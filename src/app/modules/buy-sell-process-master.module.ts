import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';

import { SharedModule } from './shared.module';
import { BuySellProcessMasterRoutingModule } from '../routing/buy-sell-process-master-routing.module';
import { AddEditAssetSaleChargeComponent } from '../component/admin/buy-sell-process-master/dialogs/add-edit-asset-sale-charge/add-edit-asset-sale-charge.component';
import { AddEditMarkUpPriceComponent } from '../component/admin/buy-sell-process-master/dialogs/add-edit-markup-price/add-edit-markup-price.component';
import { AssetSaleChargeMasterComponent } from '../component/admin/buy-sell-process-master/components/asset-sale-charge-master/asset-sale-charge-master.component';
import { MarkUpPriceMasterComponent } from '../component/admin/buy-sell-process-master/components/mark-up-price-master/mark-up-price-master.component';
import { SaleMasterPropService } from '../component/admin/buy-sell-process-master/components/sale-master/sale-master-prop.service';
import { SaleMasterComponent } from '../component/admin/buy-sell-process-master/components/sale-master/sale-master.component';

@NgModule({
  declarations: [
    MarkUpPriceMasterComponent,
    AssetSaleChargeMasterComponent,
    AddEditAssetSaleChargeComponent,
    AddEditMarkUpPriceComponent,
    SaleMasterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BuySellProcessMasterRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [
    AddEditAssetSaleChargeComponent,
    AddEditMarkUpPriceComponent,
  ],
})
export class BuySellProcessMasterModule {}
