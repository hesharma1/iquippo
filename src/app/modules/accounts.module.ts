import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RealizationComponent } from '../component/admin/accounts/realization/realization.component';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from 'src/app/modules/shared.module';
import { AccountsRoutingModule } from '../routing/accounts-routing.module';
import { RefundOnlineComponent } from '../component/admin/accounts/refund-online/refund-online.component';
import { RefundOfflineComponent } from '../component/admin/accounts/refund-offline/refund-offline.component';
import { RealizedDetailsModalComponent } from '../component/admin/accounts/realized-details-modal/realized-details-modal.component';
import { RealizedResponseModalComponent } from '../component/admin/accounts/realized-response-modal/realized-response-modal.component';
import { AddRefundInfoComponent } from '../component/admin/accounts/add-refund-info/add-refund-info.component';

@NgModule({
  declarations: [
    RealizationComponent,
    RealizedResponseModalComponent,
    RealizedDetailsModalComponent,
    RefundOnlineComponent, RefundOfflineComponent,
    AddRefundInfoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularMaterialModule,
    AccountsRoutingModule
  ],
  entryComponents: [RealizedResponseModalComponent, RealizedDetailsModalComponent, AddRefundInfoComponent]
})
export class AccountsModule { }
