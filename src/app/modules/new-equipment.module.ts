import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewEquipmentRoutingModule } from '../routing/new-equipment-routing.module';
import { SellEquipmentComponent } from '../component/customer/new-equipment/sell-equipment/sell-equipment.component';
import { AngularMaterialModule } from 'src/app/angular-material/material.module';
import { SharedModule } from './shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BasicDetailsComponent } from '../component/customer/new-equipment/basic-details/basic-details.component';
import { OffersComponent } from '../component/customer/new-equipment/offers/offers.component';
import { VisualsComponent } from '../component/customer/new-equipment/visuals/visuals.component';
import { ProductsListingComponent } from '../component/customer/new-equipment/products-listing/products-listing.component';
import { TechnicalInfoMasterUsedComponent } from '../component/admin/new-equipment-master/technical-info-master-used/technical-info-master-used.component';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [SellEquipmentComponent, BasicDetailsComponent, OffersComponent, VisualsComponent,ProductsListingComponent],
  imports: [
    CommonModule,
    NewEquipmentRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule,
    MatNativeDateModule
  ]
})
export class NewEquipmentModule { }
