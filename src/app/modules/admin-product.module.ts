import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsedProductListsComponent } from '../component/admin/product/used-product-lists/used-product-lists.component';
import { NewProductListsComponent } from '../component/admin/product/new-product-lists/new-product-lists.component';
import { AdminProductRoutingModule } from './../routing/admin-product-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { NewProductUploadComponent } from '../component/admin/product/new-product-upload/new-product-upload.component';
import { UsedProductUploadComponent } from '../component/admin/product/used-product-upload/used-product-upload.component';
import { VisualsComponent } from '../component/admin/product/used-product-upload/components/visuals/visuals.component';
import { MatTableModule } from '@angular/material/table';
import { GalleryModule } from 'ng-gallery';
import { NewVisualsComponent } from '../component/admin/product/new-product-upload/new-visuals/new-visuals.component';
// import { DateFilterPipe } from './../utility/pipes/years-picker.pipe';

@NgModule({
  declarations: [
    UsedProductListsComponent,
    NewProductListsComponent,
    UsedProductUploadComponent,
    NewProductUploadComponent,
    NewVisualsComponent,
    VisualsComponent,
    // DateFilterPipe
  ],
  imports: [
    CommonModule,
    AdminProductRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
    MatTableModule,
    GalleryModule
  ],
  entryComponents: [VisualsComponent]
})
export class AdminProductModule { }
