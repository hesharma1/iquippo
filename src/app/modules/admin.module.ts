import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './../component/admin/dashboard/dashboard.component';
import { SideBarComponent } from './../component/admin/side-bar/side-bar.component';
import { AdminProductModule } from './../modules/admin-product.module';
import { NewEquipmentMasterModule } from './../modules/new-equipment-master.module';
import { AdminRoutingModule } from './../routing/admin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { ConfirmationPopupComponent } from '../component/admin/confirmation-popup/confirmation-popup.component';
import { NgxCarouselModule } from 'ngx-light-carousel';
import { BuySellProcessMasterModule } from './../modules/buy-sell-process-master.module';
import { BannerSettingsModule } from './banner-settings.module';
import { TradeDetailsModule } from './trade-details.module';
import { ClientManagementModule } from './client-management.module';
import { CreatePartnerContactDetailsComponent } from '../component/admin/partner-management/create-partner-contact-details/create-partner-contact-details.component'
import { CreatePartnerEntityComponent } from '../component/admin/partner-management/create-partner-entity-details/create-partner-entity/create-partner-entity.component';
import { CreatePartnerEntity2Component } from '../component/admin/partner-management/create-partner-entity-details/create-partner-entity2/create-partner-entity2.component';
import { MyDemoComponent } from '../component/admin/my-demo/my-demo.component';
import { UserManagementComponent } from '../component/user-management/user-management.component';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { AddUserComponent } from '../component/add-user/add-user.component';
import { PartnerManagementBankDetailsComponent } from '../component/admin/partner-management/partner-management-bank-details/partner-management-bank-details.component'
import { TransactionComponent } from '../component/admin/transaction/transaction.component';
import { NewEquipmentLeadComponent } from '../component/admin/new-equipment-lead/new-equipment-lead.component';
import { LeadViewComponent } from '../component/admin/lead-view/lead-view.component';
import { GalleryModule } from 'ng-gallery';
import { BulkCurationComponent } from '../component/admin/bulk-upload/components/bulk-curation/bulk-curation.component';
import { BulkListingComponent } from '../component/admin/bulk-upload/components/bulk-listing/bulk-listing.component';
import { ValuationSettingsComponent } from '../component/admin/valuation-settings/valuation-settings.component';
import { EnterValuationRequestComponent } from '../component/admin/enter-valuation-request/enter-valuation-request.component';
import { CallbackRequestListComponent } from '../component/admin/callback-request-list/callback-request-list.component';
import { SubscribersListComponent } from '../component/admin/subscribers-list/subscribers-list.component';
import { EnterpriseValuationSettingsComponent } from '../component/admin/enterprise-valuation-settings/enterprise-valuation-settings.component';
import { BulkUploadListEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-list-enterprise/bulk-upload-list-enterprise.component';
import { EnterpriseInfoDialogComponent } from '../component/admin/enter-valuation-request/info-dialog/enterprise-info-dialog.component';
import { EnterpriseInvoiceComponent } from '../component/admin/enterprise-invoice/enterprise-invoice.component';
import { GenerateInvoiceDialogComponent } from '../component/admin/enterprise-invoice/generate-invoice-dialog/generate-invoice-dialog.component';
import { InvoiceFeeDetailsComponent } from '../component/admin/enterprise-invoice/invoice-fee-details/invoice-fee-details.component';
import { OfflinePaymentComponent } from 'src/app/component/admin/offline-payment/offline-payment.component';
import { UserManagementNewComponent } from '../component/admin/user-management-new/user-management-new.component';
import { ViewTransactionsComponent } from 'src/app/component/admin/enterprise-invoice/view-transactions/view-transactions.component';
import { BuyerPremiumInvoicesComponent } from '../component/admin/buyer-premium-invoices/buyer-premium-invoices.component';
import { SellerCommisionInvoicesComponent } from '../component/admin/seller-commision-invoices/seller-commision-invoices.component';
import { EMDMRInvoicesComponent } from '../component/admin/emd-mr-invoices/emd-mr-invoices.component';
import { EMDForfeitureInvoicesComponent } from '../component/admin/emd-forfeiture-invoices/emd-forfeiture-invoices.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SideBarComponent,
    ConfirmationPopupComponent,
    CreatePartnerContactDetailsComponent,
    CreatePartnerEntityComponent,
    CreatePartnerEntity2Component,
    MyDemoComponent,    
    AddUserComponent,
    PartnerManagementBankDetailsComponent,
    AddUserComponent,
    TransactionComponent,
    NewEquipmentLeadComponent,
    LeadViewComponent,
    BulkListingComponent,
    BulkCurationComponent,
    ValuationSettingsComponent,
    EnterValuationRequestComponent,
    CallbackRequestListComponent,
    SubscribersListComponent,
    EnterpriseValuationSettingsComponent,
    BulkUploadListEnterpriseComponent,
    EnterpriseInfoDialogComponent,
    EnterpriseInvoiceComponent,
    GenerateInvoiceDialogComponent,
    InvoiceFeeDetailsComponent,
    OfflinePaymentComponent,
    UserManagementNewComponent,
    ViewTransactionsComponent,
    BuyerPremiumInvoicesComponent,
    SellerCommisionInvoicesComponent,
    EMDMRInvoicesComponent,
    EMDForfeitureInvoicesComponent    
  ],
  imports: [
    CommonModule,
    AdminProductModule,
    AdminRoutingModule,
    NewEquipmentMasterModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
    NgxCarouselModule,
    BuySellProcessMasterModule,
    BannerSettingsModule,
    TradeDetailsModule,
    ClientManagementModule,
    GridModule,
    GalleryModule,
  ],
  
  entryComponents: [
    ConfirmationPopupComponent,
    EnterpriseInfoDialogComponent,
    GenerateInvoiceDialogComponent,
    InvoiceFeeDetailsComponent,
    OfflinePaymentComponent,
    ViewTransactionsComponent
  ]
})
export class AdminModule { }
