import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewEquipmentListRoutingModule } from '../routing/new-equipment-list-routing.module';
import { NewEquipmentDetailComponent } from '../component/customer/new-equipment-list/new-equipment-detail/new-equipment-detail.component';
import { NewEquipmentComponent } from '../component/customer/new-equipment-list/new-equipment/new-equipment.component';
import { NewEquipmentListingComponent } from '../component/customer/new-equipment-list/new-equipment-listing/new-equipment-listing.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { BookDemoDialogComponent } from '../component/customer/new-equipment-list/new-equipment-detail/book-demo-dialog/book-demo-dialog.component';
import { MakeEnquiryDialogComponent } from '../component/customer/new-equipment-list/new-equipment-detail/make-enquiry-dialog/make-enquiry-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCarouselModule } from 'ngx-light-carousel';
import { GalleryModule } from 'ng-gallery';
import { CompareComponent } from '../component/shared/compare/compare.component';
import { LightboxModule } from  'ng-gallery/lightbox';

@NgModule({
  declarations: [NewEquipmentComponent,NewEquipmentListingComponent,NewEquipmentDetailComponent, BookDemoDialogComponent, MakeEnquiryDialogComponent],
  imports: [
    CommonModule,
    NewEquipmentListRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule,
    NgbModule,
    NgxCarouselModule,
    GalleryModule,
    LightboxModule
  ],
  exports:[LightboxModule],
  entryComponents: [BookDemoDialogComponent, MakeEnquiryDialogComponent]
})
export class NewEquipmentListModule { }
