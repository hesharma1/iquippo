import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';

import { SharedModule } from './shared.module';
import { TradeDetailsRoutingModule } from '../routing/trade-deatils-routing.module';
import { TradeDetailsDashboardComponent } from '../component/admin/trade-details/trade-details-dashboard.component';
import { InfoDialogComponent } from '../component/admin/trade-details/info-dialog/info-dialog.component';
import { ViewBidComponent } from '../component/admin/trade-details/view-bid/view-bid.component';
import { TradeAssetDetailsComponent } from '../component/admin/trade-details/trade-asset-details/trade-asset-details.component';
import { BidActionComponent } from '../component/admin/trade-details/bid-action/bid-action.component';

@NgModule({
  declarations: [
    TradeDetailsDashboardComponent,
    InfoDialogComponent,
    ViewBidComponent,
    TradeAssetDetailsComponent,
    BidActionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TradeDetailsRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [
    InfoDialogComponent,
    BidActionComponent
  ],
})
export class TradeDetailsModule { }
