import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsedEquipmentListRoutingModule } from '../routing/used-equipment-list-routing.module';
import { OldEquipmentComponent } from '../component/customer/used-equipment-list/old-equipment/old-equipment.component';
import { UsedEquipmentListingComponent } from '../component/customer/used-equipment-list/used-equipment-listing/used-equipment-listing.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { OldEquipmentDetailsComponent } from '../component/customer/used-equipment-list/old-equipment-details/old-equipment-details.component';
import { GalleryModule } from 'ng-gallery';
import { CompareComponent } from '../component/shared/compare/compare.component';
import { LightboxModule } from 'ng-gallery/lightbox';

@NgModule({
  declarations: [OldEquipmentComponent, UsedEquipmentListingComponent, OldEquipmentDetailsComponent],
  imports: [
    CommonModule,
    UsedEquipmentListRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule,
    GalleryModule,
    LightboxModule
  ],
  exports: [LightboxModule]
})
export class UsedEquipmentListModule { }
