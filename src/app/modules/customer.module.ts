import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from "../angular-material/material.module";
import { CustomerRoutingModule } from '../routing/customer-routing.module';
import { DashboardComponent } from '../component/customer/dashboard/dashboard/dashboard.component';
import { SidebarComponent } from '../component/customer/dashboard/sidebar/sidebar.component';
import { MyWalletComponent } from '../component/customer/dashboard/my-wallet/my-wallet.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDemoComponent } from '../component/customer/dashboard/my-demo/my-demo.component';
import { PaymentComponent } from '../component/customer/payment/payment.component';
import { SharedModule } from './shared.module';
import { ProductListingComponent } from '../component/customer/dashboard/product-listing/product-listing.component';
import { AssetMasterComponent } from '../component/customer/dashboard/asset-master/asset-master.component';
import { MyEnquiriesComponent } from '../component/customer/dashboard/my-enquiries/my-enquiries.component';
import { MyAgreementsComponent } from '../component/customer/dashboard/my-agreements/my-agreements.component';
import { EnquiryChatComponent } from '../component/customer/dashboard/enquiry-chat/enquiry-chat.component';
import { TradeDetailsModule } from '../modules/customer-trade-details.module';
import { RefundDialogComponent } from '../component/customer/dashboard/my-wallet/refund-dialog/refund-dialog.component';
import { ChangePasswordComponent } from '../component/change-password/change-password.component';
import { MyInstaValuationRequestComponent } from '../component/customer/dashboard/my-insta-valuation-request/my-insta-valuation-request.component';
import { WatchlistComponent } from '../component/customer/watchlist/watchlist.component';
import { AdvancedValuationRequestComponent } from '../component/customer/dashboard/my-advanced-valuation-request/my-advanced-valuation-request.component';
import { MyAdvancedValuationActionComponent } from '../component/customer/dashboard/my-advanced-valuation-request/action/advanced-valuation-action-request.component';
import { MyAdvancedInfoDialogComponent } from '../component/customer/dashboard/my-advanced-valuation-request/info/advanced-info-dialog.component';
import { ESignTemplateComponent } from '../component/customer/e-sign-template/e-sign-template.component';
import { ESignConfirmDialogComponent } from '../component/customer/auction-e-sign/e-sign-confirm-dialog/e-sign-confirm-dialog.component';
import { PartnerDealerRegistrationModule } from './partner-dealer-registration.module';
import { ConfirmedPopup1Component } from '../component/customer/confirmed-popup/confirmed-popup1.component';
import { NumberToWordsPipe } from '../utility/pipes/convert-number';
import { MyEnterpriseValuationComponent } from 'src/app/component/customer/dashboard/my-enterprise-valuation/my-enterprise-valuation.component';

@NgModule({

  declarations: [SidebarComponent, ChangePasswordComponent, DashboardComponent,
    MyWalletComponent, MyDemoComponent, PaymentComponent, ProductListingComponent,
    AssetMasterComponent, MyEnquiriesComponent, MyAgreementsComponent, EnquiryChatComponent,
    RefundDialogComponent, MyInstaValuationRequestComponent, WatchlistComponent,
    AdvancedValuationRequestComponent, MyAdvancedValuationActionComponent,
    MyAdvancedInfoDialogComponent, ESignTemplateComponent,ConfirmedPopup1Component,
    ESignConfirmDialogComponent,NumberToWordsPipe,MyEnterpriseValuationComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    CustomerRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    TradeDetailsModule,
    PartnerDealerRegistrationModule
  ],
  entryComponents: [RefundDialogComponent,
    MyAdvancedValuationActionComponent,
    MyAdvancedInfoDialogComponent,
    ESignConfirmDialogComponent,ConfirmedPopup1Component]
})
export class CustomerModule { }
