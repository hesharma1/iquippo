import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from '../component/shared/input/input.component';
import { ButtonComponent } from '../component/shared/button/button.component';
import { SelectComponent } from '../component/shared/select/select.component';
import { RadiobuttonComponent } from '../component/shared/radiobutton/radiobutton.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/angular-material/material.module';
import { DynamicFieldDirective } from '../component/shared/dynamic-field/dynamic-field.directive';
import { DynamicFormComponent } from '../component/shared/dynamic-form/dynamic-form.component';
import { DynamicFieldComponent } from '../component/shared/dynamic-field/dynamic-field.component';
import { ProgressStepsComponent } from '../component/shared/progress-steps/progress-steps.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageCropperModalComponent } from '../component/shared/image-cropper-modal/image-cropper-modal.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCarouselModule } from 'ngx-light-carousel';
import { CompareComponent } from '../component/shared/compare/compare.component';
import { UserManagementComponent } from '../component/user-management/user-management.component';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { RouterModule } from '@angular/router';
import { TransactionRecieptModalComponent } from '../component/shared/transaction-reciept-modal/transaction-reciept-modal.component';
import { DateFilterPipe } from './../utility/pipes/years-picker.pipe';
import { MyselfdetailsComponent } from '../component/myselfdetails/myselfdetails.component';
import { CorporatedetailsComponent } from '../component/corporatedetails/corporatedetails.component';
import { BankingComponent } from '../component/banking/banking.component';
import { CompreferenceComponent } from '../component/compreference/compreference.component';
import { ConfirmAdduserComponent } from '../component/add-user/confirm-adduser/confirm-adduser.component';
import { ImageLoader } from '../utility/validations/image-loader';
import { LazyImgDirective } from '../utility/validations/lazy-Img';
import { OwlModule } from 'ngx-owl-carousel';
import { PaymentInfoDialogComponent } from '../component/admin/auction/payment-info-dialog/payment-info-dialog.component';
import { permissionDirective } from '../utility/validations/isPermisson-directive';

@NgModule({
  declarations: [InputComponent, UserManagementComponent, ButtonComponent, SelectComponent,
    RadiobuttonComponent, DynamicFieldDirective, DynamicFormComponent, DynamicFieldComponent,
    ProgressStepsComponent, ImageCropperModalComponent, CompareComponent,
    TransactionRecieptModalComponent, DateFilterPipe
    , MyselfdetailsComponent
    , CorporatedetailsComponent
    , CompreferenceComponent
    , BankingComponent
    , ConfirmAdduserComponent,
    ImageLoader,
    LazyImgDirective,
    PaymentInfoDialogComponent,
    permissionDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ImageCropperModule,
    NgxMaterialTimepickerModule,
    NgbModule,
    GridModule,
    RouterModule,
    NgxCarouselModule,
    OwlModule
  ],
  entryComponents: [
    InputComponent,
    ButtonComponent,
    SelectComponent,
    RadiobuttonComponent,
    ImageCropperModalComponent,
    TransactionRecieptModalComponent,
    PaymentInfoDialogComponent
  ],
  exports: [DynamicFieldDirective, UserManagementComponent, DynamicFormComponent, ProgressStepsComponent,
    AngularMaterialModule, FormsModule, ReactiveFormsModule, NgxFileDropModule, ImageCropperModule,
    ImageCropperModalComponent, NgxMaterialTimepickerModule, NgbModule, NgxCarouselModule, CompareComponent, DateFilterPipe,
    ImageLoader, LazyImgDirective, OwlModule, PaymentInfoDialogComponent, permissionDirective,]
})
export class SharedModule { }
