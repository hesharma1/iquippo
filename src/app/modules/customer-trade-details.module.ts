import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';

import { SharedModule } from './shared.module';
import { CustomerTradeDetailsRoutingModule } from '../routing/customer-trade-deatils-routing.module';
import { TradeDetailsDashboardComponent } from '../component/customer/dashboard/my-trade/trade-details-dashboard.component';
import { InfoDialogComponent } from '../component/customer/dashboard/my-trade/seller/info-dialog/info-dialog.component';
import { ViewBidComponent } from '../component/customer/dashboard/my-trade/seller/view-bids/view-bid.component';
import { TradeAssetDetailsComponent } from '../component/customer/dashboard/my-trade/seller/trade-asset-detail/trade-asset-details.component';
import { BidActionComponent } from '../component/customer/dashboard/my-trade/seller/bid-action/bid-action.component';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    TradeDetailsDashboardComponent,
    InfoDialogComponent,
    ViewBidComponent,
    TradeAssetDetailsComponent,
    BidActionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CustomerTradeDetailsRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
    MatSortModule
  ],
  entryComponents: [
    InfoDialogComponent,
    BidActionComponent
  ],
})
export class TradeDetailsModule { }
