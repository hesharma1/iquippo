import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewEquipmentLeadComponent } from '../component/admin/new-equipment-lead/new-equipment-lead.component';
import { NewEquipmentLeadRoutingModule } from '../routing/new-equipment-lead-routing.module';
import { AngularMaterialModule } from '../angular-material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './shared.module';
import { ActionPopupComponent } from '../component/admin/new-equipment-lead/components/action-popup/action-popup.component';



@NgModule({
  declarations: [NewEquipmentLeadComponent,ActionPopupComponent],
  imports: [
    CommonModule,
    NewEquipmentLeadRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule
  ],
  entryComponents: [ActionPopupComponent]
})
export class NewEquipmentLeadModule { }
