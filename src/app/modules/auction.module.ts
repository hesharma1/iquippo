import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuctionRoutingModule } from '../routing/auction-routing.module';
import { AuctionListingComponent } from '../component/admin/auction/auction-listing/auction-listing.component';
import { CreateAuctionComponent } from '../component/admin/auction/create-auction/create-auction.component';
import { SharedModule } from 'src/app/modules/shared.module';
import { CreateLotComponent } from '../component/admin/auction/create-lot/create-lot.component';
import { AssetsAuctionComponent } from '../component/admin/auction/assets-auction/assets-auction.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AuctionsComponent } from '../component/auctions/auctions.component';
import { AuctionDetailComponent } from '../component/auctions/auction-detail/auction-detail.component';
import { AuctionLotComponent } from '../component/auctions/auction-lot/auction-lot.component';
import { AngularMaterialModule } from '../angular-material/material.module';
import { AuctionFulfilmentComponent } from '../component/admin/auction/auction-fulfilment/auction-fulfilment.component';
import { AuctionPaymentHistoryForAdminComponent } from '../component/admin/auction/auction-payment-history-for-admin/auction-payment-history-for-admin.component';
import { GalleryModule } from 'ng-gallery';
import { LightboxModule } from  'ng-gallery/lightbox';

@NgModule({
  declarations: [AuctionListingComponent, CreateAuctionComponent, CreateLotComponent, AssetsAuctionComponent, AuctionsComponent,AuctionDetailComponent, AuctionLotComponent, AuctionFulfilmentComponent,AuctionPaymentHistoryForAdminComponent],
  imports: [
    CommonModule,
    AuctionRoutingModule,
    SharedModule,
    AngularMaterialModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    GalleryModule,
    LightboxModule
  ],
  exports: [LightboxModule],
  entryComponents: []
})
export class AuctionModule { }
