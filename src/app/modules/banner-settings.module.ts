import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule ,ReactiveFormsModule } from "@angular/forms";
import { AngularMaterialModule } from "../angular-material/material.module";
import { BannerSettingsComponent } from "../component/admin/banner-settings/banner-settings.component";
import { BannerSettingsRoutingModule } from "../routing/banner-settings-routing.module";
import { SharedModule } from "./shared.module";

@NgModule({
    declarations :[BannerSettingsComponent],
    imports: [
        CommonModule,
        BannerSettingsRoutingModule,
        AngularMaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule
      ]

})

export class BannerSettingsModule {}