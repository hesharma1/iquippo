import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsedEquipmentRoutingModule } from './../routing/used-equipment-routing.module';
import { SellEquipmentComponent } from './../component/customer/used-equipment/sell-equipment/sell-equipment.component';
import { AngularMaterialModule } from 'src/app/angular-material/material.module';
import { SharedModule } from './../modules/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BasicDetailsSectionComponent } from './../component/customer/used-equipment/basic-details-section/basic-details-section.component';
import { VaahanDetailsComponent } from './../component/customer/used-equipment/vaahan-details/vaahan-details.component';
import { BasicDetailsComponent } from './../component/customer/used-equipment/basic-details/basic-details.component';
import { VehicleDetailsComponent } from './../component/customer/used-equipment/vehicle-details/vehicle-details.component';
import { VehicleDetailsSectionComponent } from './../component/customer/used-equipment/vehicle-details-section/vehicle-details-section.component';
import { VisualsComponent } from './../component/customer/used-equipment/visuals/visuals.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import {MatMomentDateModule} from '@angular/material-moment-adapter';

@NgModule({
  declarations: [SellEquipmentComponent, BasicDetailsSectionComponent, VaahanDetailsComponent, BasicDetailsComponent, VehicleDetailsComponent, VehicleDetailsSectionComponent, VisualsComponent],
  imports: [
    CommonModule,
    UsedEquipmentRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule,
    MatMomentDateModule
  ],
  providers:[
    
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ]
})
export class UsedEquipmentModule { }
