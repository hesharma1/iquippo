import { CategorySpecListComponent } from './../component/admin/new-equipment-master/technical-info-master-new/components/category-spec-list/category-spec-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { MasterDataComponent, ModelBulkErrorListComponent } from '../component/admin/new-equipment-master/master-data/master-data.component';
import { CategoryBrandMasterComponent } from '../component/admin/new-equipment-master/category-brand-master/category-brand-master.component';
import { CreateUpdateBrandDialogComponent } from '../component/admin/new-equipment-master/category-brand-master/create-update-brand-dialog/create-update-brand-dialog.component';
import { CreateUpdateCategoryDialogComponent } from '../component/admin/new-equipment-master/category-brand-master/create-update-category-dialog/create-update-category-dialog.component';
import { AddEditCountryDialogComponent } from '../component/admin/new-equipment-master/location-master/add-edit-country/add-edit-country-dialog.component.';
import { ConfirmDialogComponent } from '../component/admin/new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';
import { LocationMasterComponent } from '../component/admin/new-equipment-master/location-master/location-master.component';
import { NewEquipmentMasterRoutingModule } from './../routing/new-equipment-master-routing.module';
import { OfferMasterComponent } from '../component/admin/new-equipment-master/offer-master/offer-master.component';
import {
  OfferMasterListsComponent,
  OfferMasterCreateComponent,
} from '../component/admin/new-equipment-master/offer-master/components';
import { TechnicalInformationMasterNewListComponent } from '../component/admin/new-equipment-master/technical-info-master-new/technical-info-master-new-list.component';
import { AddEditFieldComponent } from '../component/admin/new-equipment-master/technical-info-master-new/dialogs/add-edit-field/add-edit-field.component';
import { AddEditSpecificationComponent } from '../component/admin/new-equipment-master/technical-info-master-new/dialogs/add-edit-specification/add-edit-specification.component';
import { ViewBrandSpecDataComponent } from '../component/admin/new-equipment-master/technical-info-master-new/dialogs/view-brand-spec-data/view-brand-spec-data.component';
import { ModelSpecListComponent } from '../component/admin/new-equipment-master/technical-info-master-new/components/model-spec-list/model-spec-list.component';
import { AddEditStateDialogComponent } from '../component/admin/new-equipment-master/location-master/add-edit-state/add-edit-state-dialog.component';
import { AddEditLocationDialogComponent } from '../component/admin/new-equipment-master/location-master/add-edit-city/add-edit-city-dialog.component';
import { AddEditPinCodeDialogComponent } from '../component/admin/new-equipment-master/location-master/add-edit-pincode/add-edit-pincode-dialog.component';
import { TechnicalInfoMasterUsedComponent } from '../component/admin/new-equipment-master/technical-info-master-used/technical-info-master-used.component';
import { AddNewTechComponent } from '../component/admin/new-equipment-master/technical-info-master-used/add-new-tech/add-new-tech.component';
import { CertificateMasterComponent } from '../component/admin/new-equipment-master/certificate-master/certificate-master.component';
import { RmTaggingMasterComponent } from '../component/admin/rm-tagging-master/rm-tagging-master.component';

@NgModule({
  declarations: [
    OfferMasterComponent,
    OfferMasterListsComponent,
    OfferMasterCreateComponent,
    CategoryBrandMasterComponent,
    CreateUpdateCategoryDialogComponent,
    CreateUpdateBrandDialogComponent,
    LocationMasterComponent,
    AddEditCountryDialogComponent,
    ConfirmDialogComponent,
    AddEditStateDialogComponent,
    AddEditLocationDialogComponent,
    AddEditPinCodeDialogComponent,
    MasterDataComponent,
    ModelBulkErrorListComponent,
    TechnicalInfoMasterUsedComponent,
    TechnicalInformationMasterNewListComponent,
    AddEditFieldComponent,
    AddEditSpecificationComponent,
    ViewBrandSpecDataComponent,
    ModelSpecListComponent,
    CategorySpecListComponent,
    TechnicalInfoMasterUsedComponent,
    AddNewTechComponent,
    AddNewTechComponent,
    CertificateMasterComponent,
    RmTaggingMasterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NewEquipmentMasterRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [
    AddEditLocationDialogComponent,
    AddEditPinCodeDialogComponent,
    AddEditStateDialogComponent,
    OfferMasterCreateComponent,
    ConfirmDialogComponent,
    CreateUpdateCategoryDialogComponent,
    CreateUpdateBrandDialogComponent,
    AddEditCountryDialogComponent,
    AddEditFieldComponent,
    AddEditSpecificationComponent,
    ViewBrandSpecDataComponent,
    ModelBulkErrorListComponent
  ],
})
export class NewEquipmentMasterModule {}
