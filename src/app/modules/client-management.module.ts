import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientAgreementComponent, ESignConfirmDetailsComponent } from '../component/admin/client-agreement/client-agreement.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { ClientManagementRoutingModule } from '../routing/client-management-routing.module';
import { ConfirmationDeletePopupComponent } from '../component/admin/client-agreement/confirmation-delete-popup/confirmation-delete-popup.component';



@NgModule({
  declarations: [ClientAgreementComponent, ConfirmationDeletePopupComponent, ESignConfirmDetailsComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule,
    ClientManagementRoutingModule,

  ],
  entryComponents: [ConfirmationDeletePopupComponent, ESignConfirmDetailsComponent]
})
export class ClientManagementModule { }
