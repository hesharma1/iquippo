import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ValuationRoutingModule } from '../routing/valuation-routing.module';
import { AssetvaluatedComponent } from '../component/valuation/assetvaluated/assetvaluated.component';
import { EquipvalmadeeasyComponent } from '../component/valuation/equipvalmadeeasy/equipvalmadeeasy.component';
import { OurvaluationservicesComponent } from '../component/valuation/ourvaluationservices/ourvaluationservices.component';
import { RaiseinstavaluationenquiryComponent } from '../component/valuation/raiseinstavaluationenquiry/raiseinstavaluationenquiry.component';
import { ValuationdetailsComponent } from '../component/valuation/valuationdetails/valuationdetails.component';
import { AngularMaterialModule } from '../angular-material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './shared.module';

@NgModule({
  declarations: [
     AssetvaluatedComponent,
     EquipvalmadeeasyComponent, 
     OurvaluationservicesComponent, 
     RaiseinstavaluationenquiryComponent, 
     ValuationdetailsComponent],
  imports: [
    CommonModule,
    ValuationRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ]
})
export class ValuationModule { }
