import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/material.module';
import { SharedModule } from './shared.module';
import { LeftMenuSecondComponent } from '../component/layout/left-menu-second/left-menu-second.component';
import { PartnerDealerRegistrationRoutingModule } from '../routing/partner-dealer-registration-routing.module';
import { PartnerDealerRegistrationComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-registration.component';
import { AgreementDetailsComponent } from '../component/partner-dealer-management/agreement-details/agreement-details.component';
import { PartnerDealerCorporateDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-details/partner-dealer-corporate-details.component';
import { PartnerDealerBasicDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-basic-details/partner-dealer-basic-details.component';
import { PartnerDealerBankDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-bank-details/partner-dealer-bank-details.component';
import { PartnerDealerAgreementComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-agreement/partner-dealer-agreement.component';
import { PartnerDealerCorporateAddressComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-address/partner-dealer-corporate-address.component';
import { PartnerDealerLinkOrganizationComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-link-organization/partner-dealer-link-organization.component';
import { AgreementDetailsPopupComponent } from '../component/partner-dealer-management/agreement-details/agreement-details-popup/agreement-details-popup.component';
import { PartnerDealerCorporateNextDetailsComponent } from './../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-next-details/partner-dealer-corporate-next-details.component';
import { RequestInProgressComponent } from './../component/partner-dealer-management/partner-dealer-registration/request-in-progress/request-in-progress.component';

@NgModule({
  declarations: [LeftMenuSecondComponent,
    PartnerDealerRegistrationComponent,
    AgreementDetailsComponent,
    PartnerDealerCorporateDetailsComponent,
    PartnerDealerBasicDetailsComponent,
    PartnerDealerBankDetailsComponent,
    PartnerDealerAgreementComponent,
    PartnerDealerCorporateAddressComponent,
    PartnerDealerLinkOrganizationComponent,
    AgreementDetailsPopupComponent,
    PartnerDealerCorporateNextDetailsComponent,
    RequestInProgressComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PartnerDealerRegistrationRoutingModule
  ],
  entryComponents: [AgreementDetailsPopupComponent
  ]
})
export class PartnerDealerRegistrationModule { }
