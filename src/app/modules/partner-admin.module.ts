import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerAdminRoutingModule } from './../routing/partner-admin-routing.module';
import { SharedModule } from './shared.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PartnerAdminRoutingModule,
    SharedModule
  ]
})
export class PartnerAdminModule { }
