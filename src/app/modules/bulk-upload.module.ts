import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BulkUploadRoutingModule } from '../routing/bulk-upload-routing.module';
import { ProductDetailsComponent } from 'src/app/component/customer/bulk-upload/product-details/product-details.component';
import { ReviewAssetsComponent } from 'src/app/component/customer/bulk-upload/review-assets/review-assets.component';
import { UploadFilesComponent } from 'src/app/component/customer/bulk-upload/upload-files/upload-files.component';
import { SharedModule } from './shared.module';
import { BrandSelectionComponent } from '../component/customer/bulk-upload/brand-selection/brand-selection.component';
import { ConfirmedPopupComponent } from '../component/customer/bulk-upload/confirmed-popup/confirmed-popup.component';
import { FormsModule } from '@angular/forms';
import { BulkUploadEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-enterprise/bulk-upload-enterprise.component';


@NgModule({
  declarations: [
    UploadFilesComponent,
    ProductDetailsComponent,
    ReviewAssetsComponent,
    BrandSelectionComponent,
    ConfirmedPopupComponent,
    BulkUploadEnterpriseComponent
  ],
  imports: [
    CommonModule,
    BulkUploadRoutingModule,
    SharedModule,
    FormsModule
  ],
  entryComponents: [
    BrandSelectionComponent,
    ConfirmedPopupComponent
  ]
})
export class BulkUploadModule { }
