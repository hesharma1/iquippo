import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './component/signin/signin.component';
import { SignupComponent } from './component/signup/signup.component';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { CoderesetpasswordComponent } from './component/coderesetpassword/coderesetpassword.component';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { BasicinformationComponent } from './component/basicinformation/basicinformation.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { MyselfdetailsComponent } from './component/myselfdetails/myselfdetails.component';
import { CorporatedetailsComponent } from './component/corporatedetails/corporatedetails.component';
import { CompreferenceComponent } from './component/compreference/compreference.component';
import { BankingComponent } from './component/banking/banking.component';
import { LandingPageDashboardComponent } from './component/landing-page-dashboard/landing-page-dashboard.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { AuctionsComponent } from './component/auctions/auctions.component';
import { AuctionDetailComponent } from './component/auctions/auction-detail/auction-detail.component';
import { AuctionLotComponent } from './component/auctions/auction-lot/auction-lot.component';
import { AboutUsComponent } from './component/about-us/about-us.component';
import { ContactUsComponent } from './component/contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './component/privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './component/terms-of-use/terms-of-use.component';
import { OurValuedPartnersComponent } from './component/our-valued-partners/our-valued-partners.component';

const routes: Routes = [
  { path: 'auctions', component: AuctionsComponent },
  { path: 'auctions/auction-detail/:id/:auction_id', component: AuctionDetailComponent },
  { path: 'auctions/auction-lot', component: AuctionLotComponent },
  { path: 'login', component: SigninComponent },
  { path: 'register', component: SignupComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'passwordresetcode', component: CoderesetpasswordComponent },
  { path: 'resetpassword', component: ResetpasswordComponent },
  // { path: 'changepassword', component: ChangePasswordComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'our-valued-partners', component: OurValuedPartnersComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'terms-of-use', component: TermsOfUseComponent },
  { path: 'contact-us', component: ContactUsComponent },
  {
    path: 'basicinformation',
    component: BasicinformationComponent,
  },
  { path: 'home', component: DashboardComponent },
  { path: 'mydashboard', component: LandingPageDashboardComponent },
  {
    path: 'profile',
    component: MyselfdetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'corporatedetails',
    component: CorporatedetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'communication',
    component: CompreferenceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'bankingdetails',
    component: BankingComponent,
    canActivate: [AuthGuard],
  },
  { path: '', component: DashboardComponent },
  {
    path: 'used-equipment',
    loadChildren: () =>
      import('./modules/used-equipment.module').then(
        (m) => m.UsedEquipmentModule
      ),
  },
  {
    path: 'new-equipment',
    loadChildren: () =>
      import('./modules/new-equipment.module').then(
        (m) => m.NewEquipmentModule
      ),
  },
  {
    path: 'admin-dashboard',
    loadChildren: () =>
      import('./modules/admin.module').then((m) => m.AdminModule),canActivate: [AuthGuard],
  },
  {
    path: 'new-equipment-dashboard',
    loadChildren: () =>
      import('./modules/new-equipment-list.module').then(
        (m) => m.NewEquipmentListModule
      ),
  },
  {
    path: 'used-equipment-dashboard',
    loadChildren: () =>
      import('./modules/used-equipment-list.module').then(
        (m) => m.UsedEquipmentListModule
      ),
  },
  {
    path: 'bulk-upload',
    loadChildren: () =>
      import('./modules/bulk-upload.module').then((m) => m.BulkUploadModule),
  },
  {
    path: 'new-equipment-master',
    loadChildren: () =>
      import('./modules/new-equipment-master.module').then(
        (m) => m.NewEquipmentMasterModule
      ),
  },
  {
    path: 'partner-dealer',
    loadChildren: () =>
      import('./modules/partner-dealer-registration.module').then((m) => m.PartnerDealerRegistrationModule),
  },
  {
    path: 'customer',
    loadChildren: () =>
      import('./modules/customer.module').then(
        (m) => m.CustomerModule
      ),canActivate: [AuthGuard]
  },
  {
    path: 'valuation-request',
    loadChildren: () =>
      import('./modules/valuation.module').then(
        (m) => m.ValuationModule
      ),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule { }
