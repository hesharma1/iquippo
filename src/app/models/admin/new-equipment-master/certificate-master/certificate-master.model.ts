import { MatTableDataSource } from "@angular/material/table";

export class CertificateMasterDataModel{
    id : number = 0;
    addCertificateRequest? : AddCertificateRequest;
    addCertificateResponse : any;
    certificateDetail : any;   
    certificateDetailList?  = new MatTableDataSource<any>([]); //ray<AddGroupResponse>();
    certificateDataSource : any = [{id: 1,name: "Featured Products"},{id:2,name:"Coming Soon"},{id:3,name:"Insta Deal"},{id:4,name:"Other"}]
    actionName : string ='Save';
    addSEOInfoGroup = false;
    
}
export class AddCertificateRequest{
    name? : string;
    order : number = 0;
    certificate_desc? : string;
    certificate_logo_image? :  string;
    certificate_primary_image? : string;
    add_seo = false;
    seo_title? : string;
    seo_meta_keywords? : string;
    seo_meta_desc? : string;   
}
