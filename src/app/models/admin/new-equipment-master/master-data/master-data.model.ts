import { MatTableDataSource } from "@angular/material/table";

export class MasterDataModel{
    id : number = 0;
    addGroupRequest? = new AddGroupRequest();
    addGroupResponse? : AddGroupResponse;
    groupDetail? = new getGroupDetail();
    groupDetailDataSource?  : any;
    groupDetailList?  = new MatTableDataSource<any>([]); //ray<AddGroupResponse>();
    addCategoryMappingRequest? : AddCategoryMappingRequest;
    addCategoryMappingResponse? : AddCategoryMappingResponse;
    categoryMappingDetailDataSource?  : any;
    categoryMappingDetailList? =  new MatTableDataSource<any>([]);// Array<AddCategoryMappingResponse>();
    addBrandMappingRequest? : AddBrandMappingRequest;
    addBrandMappingResponse? : AddBrandMappingResponse;
    brandMappingDetailDataSource : any;
    brandMappingDetailList? =  new MatTableDataSource<any>([]);//= new Array<AddBrandMappingResponse>();
    addModelRequest? : AddModelRequest;
    addModelRequestArr? : AddModelRequest[];
    addModelResponse? : AddModelResponse;
    modelDetailDataSource : any;
    modelDetailList? =  new MatTableDataSource<any>([]);//= new Array<AddModelResponse>();
    brandMasterList? : any;
    groupMasterList? : any;
    categoryMasterList? : any;
    actionNameGroup : string ='Save';
    actionNameCategoryMapping : string ='Save';
    actionNameBrandMapping : string ='Save';
    actionNameModel : string ='Save';
    addSEOInfoGroup = false;
    addSEOInfoCatMap = false;
    addSEOInfoBrndMap = false;

}
export class AddGroupRequest{
    name? : string;
    description? : string;
    is_used = false;
    is_new = false;
    priority_for_use : number = 0;
    priority_for_new : number = 0;
    add_seo = false;
    seo_title? : string;
    seo_meta_keywords? : string;
    seo_meta_desc? : string;
    image_url? : string;
}
export class AddGroupResponse{
    id : number = 0;
    name? : string;
    description? : string;
    is_used = false;
    is_new = false;
    priority_for_use : number = 0;
    priority_for_new : number = 0;
    add_seo = false;
    seo_title? : string;
    seo_meta_keywords?  : string;
    seo_meta_desc? : string;
    image_url? : string

}
export class getGroupDetail
{
    id : number =0;
}
export class AddCategoryMappingRequest{
     is_used = false;
     is_new = false;
     valuation_partner_group : any;
     add_seo = false;
     seo_title?: string;
     seo_meta_keywords? : string;
     seo_meta_desc? : string;
     calculate_tcs = false;
     brand_promotion_banner = false;
     image_url? : string;
     top_banner_image? : string;
     left_banner_image? : string;
     visible_top_banner_only = false;
     visible_left_banner_only = false;
     is_top_hyperlink = false;
     is_left_hyperlink = false;
     top_hyperlink? : string;
     left_hyperlink? : string ;
     group : number= 0;
     category : number= 0;
}
export class AddCategoryMappingResponse{
     id : number = 0;
     is_used = false;
     is_new = false;
     valuation_partner_group : any;
     add_seo = false;
     seo_title? : string;
     seo_meta_keywords? : string;
     seo_meta_desc? : string;
     calculate_tcs = false;
     brand_promotion_banner = false;
     image_url? : string;
     top_banner_image? : string;
     left_banner_image? : string;
     visible_top_banner_only = false;
     visible_left_banner_only = false;
     is_top_hyperlink = false;
     is_left_hyperlink = false;
     top_hyperlink? : string;
     left_hyperlink? : string;
     group : any = 0;
     category : any = 0;     
}

export class AddBrandMappingRequest{
    name? : string;
    description? : string;
    is_used = false;
    is_new = false;
    priority_for_use : number = 0;
    priority_for_new : number = 0;
    add_seo = false;
    seo_title? : string;
    seo_meta_keywords? : string;
    seo_meta_desc? : string;
    image_url? : string;
    brand? : string;
    group : number = 0;
    category : number = 0;
    brand_logo_image? : string;	
    brand_image? : string
}
export class AddBrandMappingResponse{
    id : number = 0;
    name? : string;
    description? : string;
    is_used = false;
    is_new = false;
    priority_for_use : number = 0;
    priority_for_new : number = 0;
    add_seo = false;
    seo_title? : string;
    seo_meta_keywords? : string;
    seo_meta_desc? : string;
    image_url? : string;
    brand : any;
    category : any;
    group : any;
}
export class AddModelRequest{
    id?:number = 0;
    is_used = false;
    is_new = false;
    name? : string;
    capacity_group? : string;
    capacity : number = 0;
    capacity_unit? : string;
    sub_category? : string;
    aboutModel? : string;
    model_code? : string;
    model_desc? : string;
    risk_engine_asset_id? : string;
    mfg_asset_id? : string;
    is_attachment_applicable = false;
    is_attachment = false;
    hsn_code? : string;
    is_discontinued = false;
    year_launched : number = 0;
    year_discontinued : number = 0;
    is_registrable = false;
    image_url? : string;
    asset_risk_rating : number = 0;
    environmental_rating : number = 0;
    fuel_capacity_ltr : number = 0;
    is_off_road  = false;
    safety_rating : number = 0;
    asset_cost : number = 0;
    model_banner_image? : string;
    group : number = 0;
    category : number = 0;
    brand : number = 0;
    status : number = 0;
    image_category? :  Array<ImageCategory>[];
}
export class ImageCategory {
    category_name?: string;
}
export class AddModelResponse{
    id : number = 0;
    is_used = false;
    is_new = false;
    name? : string;
    capacity_group? : string;
    capacity : number = 0;
    capacity_unit? : string;
    sub_category? : string;
    model_code? : string;
    model_desc? : string;
    risk_engine_asset_id? : string;
    mfg_asset_id? : string;
    is_attachment_applicable? : true;
    is_attachment = false;
    hsn_code? : string;
    is_discontinued = false;
    year_launched : number = 0;
    year_discontinued : number = 0;
    is_registrable = false;
    image_url? : string;
    asset_risk_rating : number = 0;
    environmental_rating : number = 0;
    fuel_capacity_ltr : number = 0;
    is_off_road = false;
    safety_rating : number = 0;
    asset_cost : number = 0;
    model_banner_image? : string;
    group : any;
    category : any;
    brand : any;
    status : number = 0;
    image_category : Array<string> = ['',''];
}

export enum statusType {
    REJECTED = 0,
    ACTIVE = 1,
    INACTIVE = 2,
    EXPIRED = 3,
     
  }
  

