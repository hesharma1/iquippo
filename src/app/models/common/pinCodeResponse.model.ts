export class PincodeResponse{
    pincode?: pinDetails;
    message?: string;
    code?: number;
}

export class pinDetails{
    city?: string;
    pincode?: string;
    country?: string;
    state?: string;
}

    