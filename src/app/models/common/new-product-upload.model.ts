export class AdminNewProductUpload {
  trade_type?: '' ;
  variant?: '';
  product_name?:'';
  mfg_year?:'';
  relationship_manager?:'';
  selling_price?: '';
   indicative_price?: '';
   product_condition?:'';
   rate_equipment?:'';
   certificate?: '';
   document?: '' ;
   is_special_offer_banner?: '';
   offer_banner_image?: '';
   is_special_offer_text?: '';
   offer_heading?: '';
   offer_list?:'';
   upload_stamp?: '' ;
   upload_t_and_c?: '';
   is_active?:'';
   is_deleted?:'';
   is_featured?: '';
   brand?: '';
   model?: '';
   category?: '';
   country?: '';
   group?: '';
   location?: ''
   model_tech_specification?: ''
   offers?: '';
   seller?: '';
   state?: '';
   technical_info?: '';
   visuals?: ''
   role?:'';
   cognito?:'';
   info_level?:'';
   other_category?:any;
   other_brand?:any;
   other_model?:any;
   engine_power?:any;
   gross_Weight?:any;
   operating_weight?:any;
   bucket_capacity?:any;
   lifting_capacity?:any;
  }

  export class NewEquipmentVisualDetails {
    image_category?:'';
    new_equipment?:'';
    url?:'';
  }

  export class NewEquipmentOfferMaster {
    model?:0;
    country?: 0;
    locations?: [];
    is_new?: true;
    is_cash?: true;
    is_finance?: true;
    finance?: string;
    is_lease?: true;
    lease?: string;
    cash_purchases?: [
      {
        id?: 0;
        price?: 0;
        free_of_cost?: string;
        description?: string;
      }
    ];
    finance_or_leases?: [
      {
        id?: 0;
        type?: 1;
        name?: string;
        tenure?: string;
        amount?: string;
        rate?: string;
        margin?: string;
        processing_fee?: string;
        installment?: string;
        free_cost?: string;
        description?: string;
      }
    ]
  }