export class OtpSubmitRequest{
    phoneNumber?:string;
	sessionId?:string;
	otp?:string;
}
export class OtpSubmitResponse{
    phoneNumber?:string;
	sessionId?:string;
	otp?:string;
}
export class  ResponseData{
    // IsSuccess: boolean;
     statusCode?: string;
     body: any;
   }