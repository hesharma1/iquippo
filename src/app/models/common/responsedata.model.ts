export class  ResponseData{
    // IsSuccess: boolean;
     statusCode?: number;
     body: any;
   }

export class AWSforgetOtpResponse{
  AttributeName?: string
  DeliveryMedium?: string
  Destination?: string;

}

export class CountryResponse{
  prefixCode?:string;
  flag?:string;
  country?:string;
  countryCode?:string;
}