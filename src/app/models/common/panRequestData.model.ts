export class PanRequest{
    cognitoId?: string;
    panCardNo?: string;
    represent?: string;
    documentId?:string;

}
export class PanResponse{
    body? : Pandata;
}
export class Pandata{
    represent?: string;
    firstName?: string;
    lastName?: string;
    statusCode?: string;
    message?: string;
}


// Request parameter : 

// cognitoId
// PanCard no 
// represent

// response: 

// Represent
// First name   -  for individual first name and corporate company name
// last Name - for individual last name and corporate blank
// status code - 1 for invalid pan card, 1 for name miss match,1  success,1 error 
// message - based on status code
