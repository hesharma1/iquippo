export class GetUser{
    cognitoId?:string
}
export class GetUserResponse{
    UserName?: string;
    CountryCode?:string;
    PhoneNumber?: string;
    PinCode?:string;
    Email?: string;
    GivenName?: string;
    FamilyName?: string;
    Status?: string;
    DateBirth?: string;    
    PrefixCode?: string;
    Flag?: string;
    Country?: string;
    docIds?:string[]; 
}
export class VerifyDoc{
    kycFlag?: string;
	cognitoId?: string
}