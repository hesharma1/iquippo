
export class UsedEquipementModel {
    addGroupRequest?= new sellEquipment();
    addGroupResponse?: sellEquipmentResponse;
}

export class sellEquipment {
    id?: number;
    customer_type?: string;
    category?: number;
    other_category?: any;
    other_brand?: any;
    other_model?: any;
    brand?: number;
    model?: number;
    is_equipment_registered?: boolean;
    rc_number?: string;
    rm_name?: string;
    engine_number?: string;
    chassis_number?: string;
    mfg_year?: any;
    mileage?: string;
    machine_sr_number?: string;
    engine_power?: string;
    is_rc_available?: boolean;
    is_first_owned?: boolean;
    asset_address?: string;
    asset_description?: string;
    email_address?: string;
    alt_email_address?: string;
    customer_reference_number?: string;
    alt_contact_number?: string;
    is_service_log_available?: boolean;
    valuation_request?: boolean;
    location?: string;
    rate_equipment?: string;
    asset_condition?: string;
    operating_hours?: string;
    valuation_amount?: number;
    motor_operating_hours?: number;
    document?: any;
    fuel_type?: number;
    info_level?: number;
    seller?: string;
    gross_Weight?: string;
    operating_weight?: string;
    bucket_capacity?: string;
    lifting_capacity?: string;
    status?: number;
    product_name?: string;
    is_price_on_request?: boolean;
    parked_since?: any;
    parking_charge_per_day?: number;
    selling_price?: number;
    is_original_invoice?: boolean;
    book_value?: string;
    is_parking_charges_info?: boolean;
    pin_code;
    seller_type;
    hmr_kmr?:any;
}

export class sellEquipmentResponse {
    id?: number;
    message?: any;
    status_code?: any;
}

export class documentData {
    doc_type?: number;
    doc_url?: string;
    used_equipment?: number;
    new_equipment?: number;
}
export class visualData {
    visual_type?: number;
    url?: string;
    used_equipment?: number;
    new_equipment?: number;
    image_category?: number;
    is_primary?: number;
    img?: any;
    img_name?: string;
}

export class visualDataNewEquipment {
    visual_type?: number;
    url?: string;
    new_equipment?: number;
    image_category?: number;
    is_primary?: number;
    img?: any;
}

