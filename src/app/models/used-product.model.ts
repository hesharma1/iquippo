export class UsedProductModel {
    // addGroupRequest?= new sellerInformation();
    // addGroupResponse?: basicDetails;
    // addGroupResponse?: vehicleDetails;
}

export class sellerInformation {
    id?: number;
    seller_type?: number;
    name?: string;
    isd_code?: number;
    mobile?: any;
    email?: any;
    alternate_email?: any;
}
export class UsedProducts {
    relationship_manager? : any;
    id?: number;
    seller_type?: number;
    name?: string;
    isd_code?: number;
    mobile?: any;
    email?: any;
    alternate_email?: any;
    is_original_invoice?: any;
    seller?: any;
    category?: any;
    other_category?: string;
    brand?: any;
    other_brand?: string;
    model?: any;
    other_model?: string;
    is_rc_available?: any;
    is_first_owned?: any;
    mfg_year?: any;
    customer_reference_number?: any;
    pin_code?: number;
    asset_address?: any;
    alt_contact_number?: any;
    email_address?: any;
    alt_email_address?: any;
    asset_description?: any;
    rate_equipment?: any;
    asset_condition?: any
    motor_operating_hours?: any;
    mileage?: any;
    valuation_request?: any;
    chassis_number?: any;
    engine_number?: any;
    engine_power?: any;
    valuation_amount?: any;
    is_service_log_available?: any;
    registrationNumber?: any;
    gross_Weight?: any;
    operating_weight?: any;
    bucket_capacity?: any;
    lifting_capacity?: any;
    machine_sr_number?: any;
    book_value?: any;
    is_parking_charges_info?: any;
    parked_since?: any;
    parking_charge_per_day?: any;
    rm_name?: any;
    product_name?: any;
    selling_price?: any;
    reserve_price?: any;
    info_level?: any;
    status?: any;
    is_featured?: any;
    is_active?: any;
    certificate?: any;
    is_insta_sale?: any;
    is_price_on_request?: any;
    is_auction?: any;
    is_coming_soon?: any;
    is_online_bidding?: any;
    insta_sale_price?: any;
    last_date_insta_payment?: any;
    rc_number?: any;
    is_equipment_registered?: any;
    buyer_premium: any;
    buyer_premium_type: any;
    invoice_name_change_fee: any;
    hmr_kmr:any;
    last_date_bid_payment:any;
    fuel_type:any;
    is_sell:any;
    is_valuation:any;
}