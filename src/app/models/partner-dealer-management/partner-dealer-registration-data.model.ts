export class PartnerDealerRegistrationDataModel{
    selectedFormType : string = "registrationType";
    registrationTypes : any = [{id: 1,name: "Dealer"},
                               {id:2,name:"Manufacturer"},
                               {id:3,name:"Channel Partner"},
                               {id:4,name:"Valuation"}];

    registrationTypeID : number=0;
}


export class PartnerDealerRegistrationEntityModel
{
    entity_type: number = 0;
    is_msme_number: string = '';
    partnership_type:any;
    msme_number: string = '';
    company_name: string = '';
    incorporation_date: string = '';
    mobile: number = 0;
    gst_number: string = '';
    //crm_partner_id:any;
    status?:number;
    created_by: any = '';
    user:any;
    manufacture_brands?:any =  []
    first_name:any;
  }

  export class PartnerDealerRegistrationIdentityProofModel
  {
    proof_type:number =  1;
    id_number: string = '';
    status;number =  1;
    document:string = '';
    partner:number = 0;
  }

  export class PartnerDealerRegistrationTaxModel
  {
    id:any;
    registration_number:string = '';
    partner:number = 0;
    state:number = 0;
  }

  export class PartnerDealerRegistrationAddress
  {
    address: string = '';
    partner:any;
    state:number = 0;
    city:number = 0;
    pin_code:number = 0;
  }