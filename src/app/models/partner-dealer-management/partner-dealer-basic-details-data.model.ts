export class PartnerDealerBasicDetailsDataModel{
    id : string="";
    addressId : string="";
    partnerContactId : number =0;
    identityProofId : number=0;
    basicDetailsResponse : any;
    basicProfileResponse : any;
    userAddressResponse : any;
    userContactEntityResponse : any;
    userIdentityProofResponse : any;    
    userLocationResponse : any;    
    partnerDealerIdentityProofs : any;
    countryId : number = 0;
    stateId : number = 0;
    cityId : number = 0;
    pinCodeId : number = 0;
    actionName : string = "Save";
    partnerContactEntityMappingResponse:any;
    user_role?:number;
}


export class PartnerDealerBasicInfo
{
    cognito_id : string = "";
    first_name : string = "";
    last_name : string = "";
    dob:   any;
    mobile_number : string = "";
    email : string = "";
    gender:  number = 0;
    alt_email_address : string = "";
    pin_code: number =0;
    kyc_verified:number = 1;
    groups?: number[];
    user_permissions?:number[];
    relationship_manager?:number;
  pan_address_proof: boolean = false;
}
export class PartnerDealerAddress
{
    address : string = "";
    address_line1 : string = "";
    address_line2 : string = "";
    gst_no : string = "";
    // crm_address_id : string = "";
    pin_code : number = 0;
    partner_contact : number = 0;
    city : number = 0;
    state : number = 0;
    country : number = 0;
}
export class PartnerDealerIdentityProofs
{   
    id:number =0;
    id_number : string = "";
    proof_type : number =0;
    document : string = "";
    //crm_doc_url : string = "";
    //crm_doc_key : string = "";
    // crm_id_details_id : string = "";
    partner_contact : number = 0;
  document_back?: string;
}

export class PartnerDealerContactEntity
{
    partnership_type : number = 0;
    created_by : string = "";
    user : string =  "";
  status?: number;
  is_channel_partner?: boolean;
  channel_partner_type?: any;
   
}