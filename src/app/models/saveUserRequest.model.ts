export class SaveUser{    
    callcenterCognito?:string;
    PhoneNumber?: string;
    CountryCode?:string;
    PrefixCode?: string;    
    GivenName?: string;
    FamilyName?: string;
    Email?: string;
    DateBirth?: string; 
    PinCode?:string;
    Status?: string;     
    relationship_manager?:string;
}
export class SaveUserResponse{
    UserName?: string;
    CountryCode?:string;
    PhoneNumber?: string;
    PinCode?:string;
    Email?: string;
    GivenName?: string;
    FamilyName?: string;
    Status?: string;
    DateBirth?: string;     
}