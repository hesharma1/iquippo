import { AfterViewInit, Component, ChangeDetectorRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SharedService } from './services/shared-service.service';

import { CookieService } from 'ngx-cookie-service';
import { interval } from 'rxjs/observable/interval';
import { BehaviorSubject, Observable, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
// import { SignInProp } from './component/signin/signin-prop.service.component';
import { environment } from 'src/environments/environment';
import { LoaderService } from './services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'iquippo';

  UserID: string = "";
  LoginTime: number = 0;

  destroy = new Subject();
  // _oUser: SignInProp = new SignInProp();
  // private _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp);
  rxjsTimer = timer(1000, 1000);
  timer: number = 0;
  _Login: any;
  loading: boolean = false;
  constructor(
    private cookieServ: CookieService ,
    public sharedService: SharedService ,private spinner: NgxSpinnerService, private loaderService:LoaderService) 
    {
      this.ChkSessionTimeOut();
      this.loaderService.isLoading.subscribe((v) => {
        this.loading = v;
      });
    }
    ngOnInit(): void {
      // //this.spinner.show();
    }

    ChkSessionTimeOut()
    {
      // this._Login = this.rxjsTimer
      // .pipe(takeUntil(this.destroy))
      // .subscribe(val => {
      //   this.timer = val;

      //   // this.UserID = this._oUserSubject.getValue().UserID;
      //   // this.LoginTime = this._oUserSubject.getValue().LoginTime;

      //   if (this.timer >= environment.ExpiryTimeInSeconds) 
      //   {
      //     let o: SignInProp = new SignInProp();
      //     o.UserID = "";
      //     o.LoginTime = 0;
      //     this._oUserSubject.next(o);
      //     this._oUserSubject.complete();

      //     //let x = this._oUserSubject.getValue().UID;

      //     this._Login.unsubscribe();

      //     this.UserID = this._oUserSubject.getValue() != null ? this._oUserSubject.getValue().UserID : "";

      //     this.timer = 0;
      //   }
      // });
  
    }

    logout() 
    {
      // let o: SignInProp = new SignInProp();
      // o.UserID = "";
      // o.LoginTime = 0;
      // this._oUserSubject.next(o);
      // this._oUserSubject.complete();
  
      // this._Login.unsubscribe();
  
      // //this._oUserSubject.unsubscribe();
      // //this.destroy.unsubscribe();
  
      // this.UserID = this._oUserSubject.getValue() != null ? this._oUserSubject.getValue().UserID : "";
  
      // this.timer = 0;

      this.sharedService.logout();
    }

    ngAfterViewInit(){
      // setTimeout(() => {
      //   /** spinner ends after 5 seconds */
      //    
      // }, 1000);
    }
  }
