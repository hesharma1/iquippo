import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import 'core-js/es/reflect';
import { Router } from '@angular/router';
import { NotificationService } from "../toastr-notification/toastr-notification.service";
import { SharedService } from "src/app/services/shared-service.service";
import { NgxSpinnerService } from "ngx-spinner";
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(private notificationService: NotificationService, private spinner:NgxSpinnerService, private sharedService: SharedService) {}
    intercept(request: HttpRequest<any>, next: HttpHandler) {
        return next.handle(request).pipe(
            catchError(err => {
                console.log(err);
                setTimeout(() => {   
                }, 5000);

                if(err instanceof ErrorEvent)
                {
                    this.notificationService.warn('Client Side Error: ' + err?.error?.message);    
                }
                else if(err?.error?.message == "Unauthorized")
                {
                    this.sharedService.logout();
                    this.notificationService.warn('You are not authorized.');
                }
                else if(err?.error?.message == "User is not authorized to access this resource with an explicit deny")
                {
                    this.sharedService.logout();
                    this.notificationService.warn('Your session has expired, Please login again.');
                }
                else if(err?.error?.isTrusted == true)
                {
                    // this.sharedService.logout();
                    this.notificationService.warn('Deployment in Progress.');
                }
                else if(err?.error?.non_field_errors != null && err?.error?.non_field_errors.length > 0 && err?.error?.non_field_errors[0].includes("must make a unique set"))
                {
                   this.notificationService.warn('Data already exist!');
                }
                else if(err?.error != null && err?.error?.name !=null && err?.error?.name.length > 0 && err?.error?.name[0].includes("already exists"))
                {
                   this.notificationService.warn('Data already exist!');
                } 
                else if(err?.error != null && err?.error[0] != null && err?.error[0]?.name !=null && err?.error[0]?.name[0].length > 0 && err?.error[0]?.name[0].includes("already exists"))
                {
                   this.notificationService.warn('Data already exist!');
                }  
                else if(err?.error != null && err?.error != null && err?.error?.message !=null)
                {
                   this.notificationService.warn(err?.error?.message);
                   return throwError(err?.error);
                }   
                else if(err?.error?.message==undefined){
                    let allText = ''
                    if(Object.entries(err?.error).length==0)
                    {
                        this.notificationService.warn('Oops, something went wrong, please try again later!');
                    }
                    else{
                    for (let value of Object.entries(err?.error)) {
                        allText += value.toString() + " "
                    }
                    this.notificationService.warn(allText);
                    }
                }             
                else
                {
                    this.notificationService.warn('Oops, something went wrong, please try again later!');
                     
                }
                
                return throwError("Oops, something went wrong, please try again later!");
            })
        )
    }    
}