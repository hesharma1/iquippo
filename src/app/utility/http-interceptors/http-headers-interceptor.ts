import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders, HttpResponse, HttpEvent } from '@angular/common/http';
import 'core-js/es/reflect';
import { StorageDataService } from "src/app/services/storage-data.service";
import { NgxSpinnerService } from "ngx-spinner";
import { tap, catchError } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { UtilService } from "src/app/services/util.service";
import { SharedService } from "src/app/services/shared-service.service";
import { NotificationService } from "../toastr-notification/toastr-notification.service";
import { truncate } from "fs";
import { from } from "rxjs";
@Injectable()
export class HttpHeadersInterceptor implements HttpInterceptor {
    constructor(private storage: StorageDataService,public spinner:NgxSpinnerService,private util:UtilService,private shared:SharedService,public notify:NotificationService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
        return from(this.handle(request, next))
    }
    async handle(request: HttpRequest<any>, next: HttpHandler):Promise<HttpEvent<any>> {
        if (
            (request.url.toString().indexOf('getinfo') > -1) || 
            (request.url.toString().indexOf('DocumentDetailsMP2') > -1)
        || (request.url.toString().indexOf('preferences-set') > -1)  || (request.url.toString().indexOf('savebankdetails') > -1)
          || (request.url.toString().indexOf('get-user-data') > -1)
        || (request.url.toString().indexOf('save-user-data') > -1)  || (request.url.toString().indexOf('verify-kyc') > -1)
        || (request.url.toString().indexOf('xfshes258a') > -1)  
        ) {
           await this.util.checkAuthToken().then(token => {
                if (!token) {
                    this.notify.warn('Your session has expired, Please login again.',true);
                  this.shared.logout();
                }
              })    
            const modifiedReq = request.clone({
                headers: new HttpHeaders({
                    'Content-Type': 'application/json;charset=UTF-8',
                     'sessionId': this.storage.getStorageData('sessionId', false)
                })
            });
           return next.handle(modifiedReq).toPromise()
        } 
        if ((request.url.toString().indexOf('user-listing') > -1)) {// cutomer management api 
            await this.util.checkAuthToken().then(token => {
                if (!token) {
                    this.notify.warn('Your session has expired, Please login again.',true);
                  this.shared.logout();
                }
              })
            const modifiedReq = request.clone({
                headers: new HttpHeaders({
                    'Content-Type': 'application/json;charset=UTF-8',
                     'sessionId': this.storage.getStorageData('sessionId', false)
                })
            });
           return next.handle(modifiedReq).toPromise()
        }   
   
        else if(
            (request.url.toString().indexOf('pinCodeValidationMP2') > -1)
        ){
           return next.handle(request).toPromise()
        }
        else {
            let obj:any = {
                'Content-Type': 'application/json;charset=UTF-8',
                'x-api-key': environment.xApiKey,
                 //'sessionId': this.storage.getStorageData('sessionId', false)
            }
            if(this.storage.getStorageData('sessionId', false)!=undefined && this.storage.getStorageData('sessionId', false) !=''){
                
               await this.util.checkAuthToken().then(token => {
                    if (!token) {
                        this.notify.warn('Your session has expired, Please login again.',true);
                      this.shared.logout();
                    }      
                          
                })
                obj.sessionId = this.storage.getStorageData('sessionId', false);
                // obj.cognito_id = this.storage.getStorageData('cognitoId', false);
            }

            const modifiedReq = request.clone({
                headers: new HttpHeaders(obj)
            });
           return next.handle(modifiedReq).toPromise()
        }
        
    }
}
