import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationService } from './toastr-notification.service';
import { NotificationComponent } from './toastr-notification.component';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [NotificationComponent],
  imports: [
    BrowserModule
  ],
  exports:[
    NotificationComponent
  ],
  providers:[
    NotificationService
  ]
})
export class NotificationModule { }
