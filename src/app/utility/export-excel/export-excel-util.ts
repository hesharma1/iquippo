import * as XLSX from "xlsx";

export class ExportExcelUtil {
  static exportArrayToExcel(arr: any[], name: string) {
    var wb = XLSX.utils.book_new();
    var ws = XLSX.utils.json_to_sheet(arr);
    XLSX.utils.book_append_sheet(wb, ws, name);
    XLSX.writeFile(wb, `${name}.xlsx`);
  }
}
