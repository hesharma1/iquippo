import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppRouteEnum {
    Usermanagement: string = 'customer-management'; 
    UsermanagementNew: string = 'user-managememt-new'; 
    Adduser: string = 'add-user'; 
    Profile: string = 'customer/dashboard/profile';
    AdminProfile: string = 'admin-dashboard/profile';
    Register: string = 'register';
    ForgotPassword: string = 'forgotpassword';
    BasicInformation: string = 'basicinformation';
    Login: string = 'login';
    PasswordResetCode: string = 'passwordresetcode';
    Communication: string = 'customer/dashboard/communication';
    BankingDetails: string = 'customer/dashboard/bankingdetails';
    AdminBankingDetails: string = 'admin-dashboard/bankingdetails';
    CorporateDetails: string = 'customer/dashboard/corporatedetails';
    AdminCorporateDetails: string = 'admin-dashboard/corporatedetails';
    CustomerAgreement: string = 'dashboard/my-agreements';
    AdminCustomerAgreement: string = 'admin-dashboard/my-agreements';
    newEquipmentList: string = 'new-equipment-dashboard/equipment-list';
    CustomerManagementCallcenterUrl:string = '/customer/dashboard/customer-management';
    partnerBankingDetails: string = 'customer/dashboard/partner-dealer/bank-details' ;
    pan:string ="pan";
    partnerProfile: string = 'customer/dashboard/partner-dealer/basic-details' ;
    partnerRegistration: string = 'partner-dealer/registration' ;
    partnerContactEntity : string = 'partner/partner-contact-entity' ;
    partnerEntity : string = 'partner/partner-entity' ;
    aadhar:string ="aadhar";
    passport:string ="passport";
    voter:string ="voter";
    dl:string ="dl";
    indiaPhoneNumber = 10;
    otherPhoneNumber = 999999999;
    globalCookieName = '_isL';
    globalCookieValue = 'eltm';
    globalCookieTime = 30;
  }