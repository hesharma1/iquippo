import { AbstractControl, FormGroup } from '@angular/forms';

export function CheckMobile(mobileNumber: string) {
    return (formGroup: FormGroup) => {

        const mobileNumberControl = formGroup.controls[mobileNumber];
        let mobileWithCode = '+91' + mobileNumberControl.value;

        let regex = /[A-Za-z]+/g;
        if (regex.test(mobileNumberControl.value)) {
            var newval = mobileNumberControl.value.replace(/[^0-9\+]+/, "");
            mobileNumberControl.setValue(newval);
            mobileNumberControl.setErrors({ letterError: true });
        }
        else if (mobileNumberControl.value.length == "") {
            mobileNumberControl.setErrors({ emptyError: true });
        }
        else if (mobileNumberControl.value.length < 10) {
            mobileNumberControl.setErrors({ lengthError: true });
        }
        else {
            mobileNumberControl.setErrors(null);
        }
    }
}

export function CheckPasswordMatch(controlName: any, matchingControlName: any) {

    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors) {
            return;
        }

        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ notMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
};

export function numberonly(controlName: any) {
    return (formGroup: FormGroup) => {
        const cotrl = formGroup.controls[controlName];

        let regex = /[^0-9]+/;
        // console.log("outer");
        if (cotrl.value && regex.test(cotrl.value)) {
            // console.log("in");
            var newval = cotrl.value.replace(cotrl.value, "");
            cotrl.setValue(newval);
            cotrl.setErrors({ letterError: true });
        }
        else if (cotrl.hasError('letterError')) {
            cotrl.setErrors(null);
            // cotrl.setErrors({ letterError: false });
        }
    }
};



