import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import * as AWS from 'aws-sdk';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';

// import * as S3 from 'aws-sdk/clients/s3';

@Injectable({
  providedIn: 'root'
})
export class S3UploadDownloadService {

  constructor(public commonService: CommonService, private notify: NotificationService, public spinner: NgxSpinnerService) {
  }

  private getS3Bucket(): any {
    this.commonService.getTokensForUpload().subscribe(
      (response: any) => {
        if (!response && !response?.accessKeyId && !response?.secretAccessKey && !response?.sessionToken) {
          response = false;
          this.notify.error(response)
          return response;
        } else {
          const bucket = new AWS.S3({
            region: 'ap-south-1',
            accessKeyId: response?.accessKeyId,
            secretAccessKey: response?.secretAccessKey,
            sessionToken: response?.sessionToken
          });
          return bucket;
        }
      },
      err => {
        this.notify.error(err)
        return err;
      }
    )
  }

  prepareFileForUpload(file: any, imagePath: any): Promise<any> {
    let th = this.notify;
    let that = this;
    let res: any;
    return new Promise(resolve => {
      that.commonService.getTokensForUpload().subscribe((response: any) => {
        if (!response && !response?.accessKeyId && !response?.secretAccessKey && !response?.sessionToken) {
          res = false;
          return res;
        }
        else {
          var s3 = new AWS.S3({
            region: 'ap-south-1',
            accessKeyId: response?.accessKeyId,
            secretAccessKey: response?.secretAccessKey,
            sessionToken: response?.sessionToken
          });

          var params = {
            Bucket: environment.bucketName,
            Key: imagePath,// 
            Body: file,
            ACL: 'public-read'
            // ContentType: file.type
          };

          s3.upload(params)
            .on('httpUploadProgress', function (progress) {
              var percentComplete = (progress.loaded * 100) / progress.total;
              th.warn('Uploading File(' + percentComplete.toFixed(0).toString() + '%)')
              if(percentComplete.toFixed(0).toString() =="100"){
               setTimeout(() => {
                th.success('Uploading Successful(' + percentComplete.toFixed(0).toString() + '%)')
              }, 1000);
              }
            })
            .send(function (err: any, data: any) {
              if (err) {
                th.error(err);
                res = err;
                that.spinner.hide();
                return res;
              }
              else {
                res = data;
                that.spinner.hide();
                resolve(res);
              }
            }
            );
        }
      }, (err: any) => {
        th.error(err)
        res = err;
        return res;
      });
    })
  }
  prepareFileForUploadWithoutPublicAccess(file: any, imagePath: any): Promise<any> {
    let th = this.notify;
    let that = this;
    let res: any;
    return new Promise(resolve => {
      that.commonService.getTokensForUpload().subscribe((response: any) => {
        if (!response && !response?.accessKeyId && !response?.secretAccessKey && !response?.sessionToken) {
          res = false;
          return res;
        }
        else {
          var s3 = new AWS.S3({
            region: 'ap-south-1',
            accessKeyId: response?.accessKeyId,
            secretAccessKey: response?.secretAccessKey,
            sessionToken: response?.sessionToken
          });

          var params = {
            Bucket: environment.bucketName,
            Key: imagePath,// 
            Body: file,
            //ACL: 'public-read'
            // ContentType: file.type
          };

          s3.upload(params)
            .on('httpUploadProgress', function (progress) {
              var percentComplete = (progress.loaded * 100) / progress.total;
              th.warn('Uploading File(' + percentComplete.toFixed(0).toString() + '%)')
              if(percentComplete.toFixed(0).toString() =="100"){
               setTimeout(() => {
                th.success('Uploading Successful(' + percentComplete.toFixed(0).toString() + '%)')
              }, 1000);
              }
            })
            .send(function (err: any, data: any) {
              if (err) {
                th.error(err);
                res = err;
                that.spinner.hide();
                return res;
              }
              else {
                res = data;
                that.spinner.hide();
                resolve(res);
              }
            }
            );
        }
      }, (err: any) => {
        th.error(err)
        res = err;
        return res;
      });
    })
  }

  deleteFile(filePath) {
    let res: any;
    return new Observable((observer) => {
      this.commonService.getTokensForUpload().subscribe(
        (response: any) => {
          if (!response && !response?.accessKeyId && !response?.secretAccessKey && !response?.sessionToken) {
            res = false;
            return res;
          }
          else {
            var s3 = new AWS.S3({
              region: 'ap-south-1',
              accessKeyId: response?.accessKeyId,
              secretAccessKey: response?.secretAccessKey,
              sessionToken: response?.sessionToken
            });

            const params = {
              Bucket: environment.bucketName,
              Key: filePath
            };
            s3.deleteObject(params, function (err, data) {
              if (data) {
                observer.next('Successfully deleted file.');
              }
              if (err) {
                console.log('There was an error deleting your file: ', err.message);
                observer.error(err.message);
              }
            });
          }
        }, (err: any) => {
          this.notify.error(err)
          res = err;
          return res;
        });
    })
  }
}
