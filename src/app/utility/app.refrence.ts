import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ApiRouteService {
  otpRequest: string = 'phonenumberregister';
  otpcheck: string = 'otp-check';
  verifyUser: string = environment.verifyUser;
  countryDetailsUrl: string = environment.countryDetails;
  users: string = environment.users;
  usersUrl: string = environment.usersUrl;
  userGetSaveUrl: string = environment.userGetSaveUrl;
  getUser: string = environment.getUser;
  saveUser: string = environment.saveUser;
  verifydocUrl: string = environment.verifydocUser;
  verifyDoc: string = environment.verifyDoc;
  updateStatusUrl: string = environment.updateStatusUrl;
  updateStatus: string = environment.updateStatus;
  downloadImagesUrl: string = environment.downloadImageUrl;
  downloadImages: string = environment.downloadImage;
  verifyPanDetail: string = '';
  getTokens: string = environment.getTokensForUploadUrl;
  upload: string = environment.uploadTokens;
  brandMaster: string = 'master/brand-master/';
  tradeAsset: string = 'trade/assets/';
  tradeBid: string = 'trade/bids/';
  categoryMaster: string = 'master/category-master/';
  countryMaster: string = 'master/country/';
  techSpec: string = 'master/get-tech-specifications/';
  stateMaster: string = 'master/state/';
  locationMaster: string = 'master/location/';
  cityMaster: string = 'master/city/';
  offerMaster: string = 'master/offer-master/';
  newEquipmentLead: string = 'product/equipment-lead/';
  brandMasterByCategory: string = 'master/brand-master/filter-by-category/';
  modelMasterByBrand: string = 'master/model-master/filter-by-brand/';
  modelMasterByBrandCategory: string = 'master/model-master/filter-by-brand_category/';
  newEquipmentSave: string = 'product/new-equipment/';
  exportNewEquipment: string = 'product/new-equipment-bulk-export/'
  exportUsedEquipment: string = 'product/sed-equipment-bulk-export/'

  // verifyUser: string ='socialSignUpMP2Test';; // for deploy
  groupMaster: string = 'master/group-master/';
  categoryMappingMaster: string = 'master/groupcategory-mapping/';
  brandMappingMaster: string = 'master/brandgroupcategory-mapping/';
  modelMaster: string = 'master/model-master/';
  batchUpdateMaster: string = 'master/model-master/batch_update/';
  productUsedList: string = 'product/used-equipment/';
  getNewEquipmentList: string = 'product/new-equipment/';
  getHomepageBannerList: string = 'banner/homepage-banner/home-page';
  getLandingPageFilter: string = 'product/landing-page-filters-new/';
  getLandingPageFilterUsed: string = 'product/landing-page-filters-used/';
  usedProductList: string = 'product/used-equipment/';
  categoryTechSpecification: string = 'master/categorytech-specification/';
  modelTechSpecification: string = 'master/modeltech-specification/';
  getModelTechSpecList: string = 'master/modeltech-specification/category_model_brand_list/';
  getModelTechSpecification: string = 'master/modeltech-specification/category_model_brand_list/';
  modelSpecByCatBrandModel: string = 'master/modeltech-specification/get_fields_by_category_model_branch/';
  deleteModelTech: string = 'master/modeltech-specification/delete_fields_by_category_brand_model/';
  batchUpdateModelSpec: string = 'master/modeltech-specification/batch_update/';
  LeadStatus = {
    '1': 'INITIATED',
    '2': 'IN_PROGRESS',
    '3': 'COMPLETED',
    '4': 'CLOSED',
  };
  techInfoMasterUsed: string = 'master/used-tech-specification/';
  techInfoBatchUpdate: string = 'master/used-tech-specification/batch_update';
  newEquipmentVisuals: string = 'product/visuals-new-equipment/';
  newEquipmentVisualsByEquipmentId: string = 'product/visuals-new-equipment/by-equipment/';
  newEquipmentOffers: string = 'product/offers-new-equipment/';
  newEquipmentOffersByEquimentId: string = 'master/offer-master/';
  oldEquipmentVisualsByEquimentId: string = 'product/visuals-used-equipment/by-equipment/';
  oldEquipmentDocumentsByEquimentId: string = 'product/document-used-equipment/by-equipment/';
  demoNew: string = 'product/demo-for-new/';
  usedProductVisualData: string = 'product/visuals-used-equipment/';
  usedProductVisualTempData: string = 'product/visuals-temp-used-equipment/';
  oldTempEquipmentVisualsByEquimentId: string = 'product/visuals-temp-used-equipment/by-equipment/';
  usedProductVisualDataById: string = 'product/visuals-used-equipment/by-equipment/';
  visualTabsList: string = 'master/visual-master/';
  sellerList: string = 'product/seller/';
  usedProductDocumentData: string = 'product/document-used-equipment/';
  newProductDocumentData: string = 'product/document-new-equipment/';
  getDocumentsForUsedProduct: string = 'product/document-used-equipment/by-equipment/';
  getDocumentsForNewProduct: string = 'product/document-new-equipment/by-equipment/';
  individualInvoice: string = 'invoice/invoicing';
  creditNote: string = 'invoice/credit-note';
  certificateMaster: string = 'master/certificate-master/';
  newProductVisualData: string = 'product/visuals-new-equipment/';
  getVisualDetail: string = 'product/visuals-new-equipment/';
  newProdcutGetOffers: string = 'product/offers-new-equipment/';
  assetSaleChargeMaster: string = 'buysellmaster/asset-sale-charge-master/';
  markUpPriceMaster: string = 'buysellmaster/mark-up-price-master/';
  landingPageBanner: string = 'banner/new-equipment-banners/landing-page/';
  advanceValuation: string = 'valuation/advanced-valuation/'
  registrationTypes: any = [{ id: 1, name: "Dealer", value: "Dealer" },
  { id: 2, name: "Manufacturer", value: "Manufacturer" },
  { id: 3, name: "Channel Partner", value: "ChannelPartner" },
  { id: 4, name: "Valuation", value: "Valuation" },
    // { id: 5, name: "Enterprise customer", value: "Enterprise" }
  ];
  partershipTypes: any = [{ id: 1, name: "Dealer", value: "Dealer" },
  { id: 2, name: "Manufacturer", value: "Manufacturer" },
  { id: 3, name: "Channel Partner", value: "ChannelPartner" },
  { id: 4, name: "Valuation", value: "Valuation" },
  { id: 5, name: "Enterprise customer", value: "Enterprise" },
  { id: 6, name: "Admin", value: "Admin" },
  { id: 7, name: "Super Admin", value: "SuperAdmin" },

  ];
  groups: any = [
    { id: 1, name: "Customer", value: "Customer" },
    { id: 2, name: "SuperAdmin", value: "SuperAdmin" },
    { id: 3, name: "Dealer", value: "Dealer" },
    { id: 4, name: "Manufacturer", value: "Manufacturer" },
    { id: 5, name: "PartnerGuest", value: "PartnerGuest" },
    { id: 6, name: "Admin", value: "Admin" },
    { id: 7, name: "ChannelPartner", value: "ChannelPartner" },
    { id: 8, name: "EnterprisePartner", value: "EnterprisePartner" },

  ];
  productUsedEquipment: string = 'product/used-equipment/';
  productTempUsedEquipment: string = 'product/temp-used-equipment/';
  EnterpriseTempValuation: string = 'valuation/enterprise-bulk-upload/';
  bannerSettingsList: string = 'banner/new-equipment-banners/';
  bannerSettingsTypeListing: string = 'banner/new-equipment-banners/landing-page/';
  savePartnerProfile: string = '/partner/basic-profile/';
  verifyPan: string = 'partner/verify-pan/'
  agreementStatus = {
    '1': 'PENDING',
    '2': 'APPROVED',
    '3': 'EXPIRED',
    '4': 'REJECTED'
  }

  EmdChargeMaster: string = 'buysellmaster/emd-charge-master/';
  SaleMaster: string = 'buysellmaster/sale-process-master/';
  getMasterFields: string = '/master/get-fields/';
  roles = {
    admin: "Admin",
    superadmin: "SuperAdmin",
    customer: "Customer",
    rm: "ChannelPartner",
    callcenter: "Callcentre",
    dealer: "Dealer",
    manufacturer: "Manufacturer",
    enterprisePartner: "EnterprisePartner",
    guest: "Guest",
    partnerGuest: "PartnerGuest"
  }
  partnerBankDetails: string = 'partner/bank-details/batch_update';
  validUrlRegex: string = "http?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)";
  partnerIdentityProof: string = 'partner/partner-identity-proof/';
  partnerCorporateAddress: string = 'partner/partner-corporate-address/';
  partnerEntityMapping: string = 'partner/partner-entity-mapping/';
  bankMaster: string = 'master/bank-master/';
  getPartnerBankDetails: string = 'partner/bank-details/filter-by-partner/'
  partnerDealerContactEntity: string = 'partner/partner-contact-entity/';
  partnerDealerIdentityProofs: string = '/partner/user-identity-proof/';
  partnerDealerBatchIdentityProofs: string = 'partner/user-identity-proof/batch_update/';
  partnerContactLinks: string = '/partner/partner-contact-entity/partner-without-entity'
  partnerDealerBasicProfile: string = 'partner/basic-profile/';
  partnerDealerAllProfile: string = 'partner/get_partnercontact_info/';
  partnerDealerUserAddress = 'partner/user-address-details/';
  partnerDealerIdentityProof: string = 'partner/partner-identity-proof/filter-by-partner/'
  partnerGroupName: string = "PartnerGuest";
  partnerIdentityProofBatch: string = "partner/partner-identity-proof/batch_update/";
  identityProofType = {
    PAN_CARD: 1,
    AADHAR_CARD: 2,
    PASSPORT: 3,
    VOTER_ID: 4,
    DRIVING_LICENCE: 5,
    GSTIN: 6,
    UDYOG_AADHAR: 7,
    TRADE_LICENCE: 8,
  }
  partnerAgreement: string = "partner/agreement/"
  bulkUploadList: string = "product/temp-used-equipment/list_bulk_uploads/"
  bulkUploadEnterpriseList: string = "valuation/enterprise-bulk-upload/list_bulk_uploads/"
  bulkUploadCuration: string = "product/temp-used-equipment/by-bulk-transaction-id/"
  bulkUploadEnterpriseCuration: string = "valuation/enterprise-bulk-upload/by-bulk-transaction-id/"
  bulkAddToMaster: string = "bulk-add-to-master"
  bulkAddToDictionary: string = "bulk-add-to-dictionary"
  bulkDiscardRecords: string = "bulk-discard-records"
  bulkImportData: string = "bulk-import-data"
  bulkImportEnterpriseData: string = "enterprise-bulk-import-data"
  bulkUpdateValue: string = "bulk-update-values"
  deleteBulkUpload: string = "product/temp-used-equipment/"
  sellerAndCustomerList: string = "partner/basic-profile"
  partnerAgreementFilter: string = "partner/agreement/filter-by-partner/"
  partnerAgreementBankDetails: string = "partner/agreement/"
  partnerAgreementTemplate: string = "master/config/agreement_template_url/"
  partnerEntity: string = "partner/partner-entity/"
  partnerTaxDetails: string = "partner/partner-tax-detail/"
  partnerCity: string = "master/city/"
  pincode: string = "master/location/"
  permission: string = 'master/permissions-master/';

  partnerBrandLocationDetails: string = "partner/partner-brand-location/"
  bulkUploadAddToMaster: string = "product/temp-used-equipment/by-bulk-transaction-id/"
  UspList: string = "product/usp-for-new/"
  partnerContactList: string = "partner/partner-contact-entity/"
  partnerEntityList: string = "partner/partner-entity/"
  paymentsTransaction: string = 'payments/transaction/'
  iQuippoTransactions: string = "payments/iquippo-transactions/"
  paymentsBidBond: string = 'payments/bid-bond/'
  partnerProfile: string = "partner/list-sellers/"
  partnerlocationMaster: string = 'master/pincode/'
  validateBid: string = 'product/validate-bid/'
  masterSetting: string = 'buysellmaster/master-setting/'
  callBack: string = 'callback/request/'
  instaVal: string = 'valuation/insta-valuation/'
  advancedVal: string = 'valuation/advanced-valuation/'
  enterVal: string = 'valuation/enterprise-valuation/'
  resumeJob: string = 'valuation/resume-job/'
  cancelJob: string = 'valuation/cancel-valuation/'
  productMakeVisualPrimary: string = 'product/make-visual-primary/'
  paymentsWalletTransactions: string = 'payments/wallet-transactions/'
  partnerAdminIdentityProof = 'partner/partner-identity-proof/'
  valAsset: string = 'valuation/valuation-asset/'
  partnerContactRegistered = 'partner/partner-contact-entity/'
  contactCognito = environment.contactCognito
  createPartnerContact = 'save-user-data/'
  masterPincode: string = 'master/pincode/'
  rmTagging: string = 'partner/list-all-rms/'
  reassignRM: string = 'partner/reassign_rm/'
  masterUserRole: String = 'master/role-master/';
  channelPartnerType: String = 'master/channel-partner-type/';
  enterprisePermissions: String = 'role/enterprise-permissions';
  bulkUpload: string[] = ["Sr.No.*", "Category*", "Brand*", "Model*", "Engine_No", "Chassis_No", "Registration_No", "Year_of_Manufacture", "Sale_Price*", "Pin_Code", "City", "State", "Motor_Operating_Hours", "Mileage", "Product_Condition", "Machine_Serial_No", "Gross_Weight", "Operating_Weight", "Bucket_Capacity", "Engine_Power", "Lifting_Capacity", "Service_Date", "Operating_Hours", "Service_at_KMs", "Authorized_Station", "Engine_Repaired_Overhauling", "Video_Link", "is_contact_displayed_in_frontend", "Contact_Number", "Alternate_Contact_Number", "Address_Of_Asset", "Customer_Reference_No", "Variant", "Buyer_Premium", "Special_Offers", "Product_Condition(Average/Good/Excellent)", "HMR/KMR", "Last_Date_Of_Payment"];
  // bulkUploadEnterprise: string[] = ["Category*", "Brand*", "Model*", "Asset_name", "Repo_Date", "Machine_Serial_No", "Distance_to_be_Travelled", "Original_owner(Yes/No)", "Customer_seeking_Finance", "Reference_number", "Contact_Person", "Contact_Number", "Engine_No*", "Chassis_No*", "Registration_No*", "Year_of_Manufacture", "Sale_Price*", "Pin_Code*", "Motor_Operating_Hours", "Mileage", "Product_Condition", "Machine_Serial_No", "Gross_Weight", "Operating_Weight", "Bucket_Capacity", "Engine_Power", "Lifting_Capacity", "Service_Date", "Operating_Hours", "Service_at_KMs", "Authorized_Station", "Engine_Repaired_Overhauling", "Video_Link", "is_contact_displayed_in_frontend", "Contact_Number", "Alternate_Contact_Number", "Address_Of_Asset", "Customer_Reference_No", "RM_Name*", "Variant", "Price_On_Request(Yes/No)*", "Reserve_Price", "Buyer_Premium", "Special_Offers", "Rate_Equipment(Average/Good/Excellent)","Invoice_Date","Invoice_Value"];
  bulkUploadEnterprise: string[] = ["Category*", "Brand*", "Model*", "Asset_name", "Distance_to_be_Travelled", "Original_owner(Yes/No)", "Customer_seeking_Finance", "Reference_number", "Contact_Person*", "Repo_Date", "Engine_No", "Chassis_No", "Registration_No", "Year_of_Manufacture", "Pin_Code", "City", "State", "Machine_Serial_No", "Contact_Number", "Address_Of_Asset*", "Customer_Reference_No", "Invoice_Date", "Invoice_Value"];
  enquiryChat: string = 'product/enquiry-chat/'
  enquiryChatUpdateSeen: string = 'product/enquiry-chat/update_chat_seen/'
  homePageBanner = 'banner/homepage-banner/'
  BulkStatus = {
    1: "PENDING_FOR_IMPORT",
    2: "UNIDENTIFIED_MASTER"
  }
  pagination = {
    pagesize: 10,
    pagesizeoption: [5, 10, 20]
  }
  recentlyViewed = 'partner/recent-view-products'
  searchByKeyword = 'banner/search_product_by_keyword/'
  removeFromWatchList = 'partner/recent-view-products/remove-from-watchlist'
  newsletter = 'callback/newsletter'
  brandLocationPatch = 'partner/partner-brand-location/batch_update/'
  saveUpdatePartnerTax = 'partner/partner-tax-detail/batch_update/'
  auctions: string = 'auction/auctions/'
  auctionsLot: string = 'auction/lots/'
  auctionsEmd: string = 'auction/auction-emd/'
  auctionLotAssets: string = 'auction/lot-assets/'
  validateSeller = 'partner/validate-seller/'
  instaValuationFee = '/mastersettings/insta-valuation-fee/'
  assetType = 'master/asset-type/'
  advanceValuationFee = '/mastersettings/advance-valuation-fee/'
  enterpriseValuation = 'valuation/enterprise-valuation/'
  purposeMaster = '/mastersettings/purpose-master'
  auctionBrandFilter = 'brand-filters/'
  auctionCategoryFilter = 'category-filters/'
  auctionLocationFilter = 'state-filters/'
  auctionMfgYearFilter = 'mfg-year-filters/'
  getRolesByUser = 'partner/admin-permission/'
  generateInvoice = 'invoice/invoicing/generate_individual_invoice/'
  generateAnnexure = 'invoice/invoicing/generate_annexure/'
  invoiceFeeDetails = 'invoice/invoice-fee-details/'
  batchTaxUpdate = 'invoice/invoice-fee-details/tax_details_batch_update/'
  modifyInvoice = 'invoice/invoicing/'
  auctionEmd: string = 'auction/auction-emd/'
  enterpriseValSettings = 'mastersettings/enterprise-valuation-fee-master/'
  gstDetails = 'mastersettings/gst-rate-master/'
  auctionEmdPayments: string = 'auction/emd-payments/'
  auctionFulfillmentPayments: string = 'auction/fulfilment-payments/'
  eSignAuctions: string = 'auction/esign/'
  eSignCaptureWidget: string = "auction/esign/esign-capture-widget/"
  eSignStatus: string = "auction/esign/esign-status/"
  getTotalFee: string = "invoice/invoicing/get_total_fee"
  servicesmaster: string = "mastersettings/services-master/"
  gstMaster: string = "mastersettings/gst-rate-master/"
  iQuippoGst: string = "mastersettings/iquippo-document-master/"
  generateLOA: string = "auction/fulfilment/generate_loa/"
  fulfilment: string = "auction/fulfilment/"
  uploadFulfilment: string = "auction/fulfilment/upload_fulfilment/"
  cancelInvoice: string = "invoice/invoicing/cancel_invoice/"
  contactUs: string = "callback/contactus"
  updateFulfilment: string = "auction/fulfilment/update_fulfilment/"
  bulkUpdateReviseDate: string = "auction/fulfilment/bulk-update-revise-date"
  auctionLotBids: string = "auction/lot-bids/"
  buySellMasterPaymentDescription: string = "buysellmaster/payment-description/"
  viewOnlineAuction: string = "auction/integration/view-online-auction"
  paymentsUserTransactions: string = "payments/user-transactions/"
  tradeBidsGet_bid_payment_details: string = "trade/bids/get_bid_payment_details/"
  approveOnlineRefund: string = "payments/iquippo-transactions/approve_online_refund/"
  auctionEmdPaymentsInitiateTxnPayment: string = "auction/emd-payments/initiate-transaction-payment/"
  auctionEmdPaymentsInitiateWalletPayment: string = "auction/emd-payments/initiate-wallet-payment/"
  emdExistCheck: string = 'auction/auction-emd/emd-exist-check'
  auctionsContact: string = 'auction/auction-contacts/'
  newEquipmentFilters: string = 'product/new-equipment/filters'
  usedEquipmentFilters: string = 'product/used-equipment/filters'
  auctiondropdown: string = 'auction/auctions/auction-dropdown/'
  deleteAuctionRegistration  : string = 'payments/transaction/'
  rejectFulfilment : string = 'auction/fulfilment/'
  initiateTransactionTopup : string = 'auction/emd-payments/initiate-transaction-topup/'
  initiateWalletTopup: string = 'auction/emd-payments/initiate-wallet-topup/'
  notifications: string = 'notification/in-app-notification'
  updateNotificationsStatus: string = 'notification/in-app-notification/update_message_status'
}
 
