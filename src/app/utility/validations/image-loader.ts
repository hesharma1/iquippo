import {Directive, ElementRef, Input, ViewChild} from '@angular/core';

@Directive({
  selector: '[imageLoader]'
})
export class ImageLoader {
  @Input() imageLoader;
 // @ViewChild("imgdv" , { static : false } ) divView: ElementRef;
  constructor(private el:ElementRef) {
   // console.log("call...");
   // this.el.nativeElement.style.backgroundImage = "url(../../../assets/images/spinner.gif)";
    // this.el.nativeElement.style.backgroundPosition = "center";
    // this.el.nativeElement.style.backgroundRepeat = "no-repeat";
  } 
  
  ngOnInit() {
    let image = new Image();
    image.addEventListener('load', () => {
      this.el.nativeElement.backgroundImage = `url(${this.imageLoader})`;
    });
    image.src = this.imageLoader; 
  }
}