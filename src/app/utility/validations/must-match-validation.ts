import { FormGroup, Validators } from '@angular/forms';
//custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}

export function NotMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.notMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value == matchingControl.value) {
            matchingControl.setErrors({ notMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}

export function ageCheck(controlName: any) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        let controlval = formGroup.controls[controlName].value;
        let dob = new Date(controlval);
        let dobyear = dob.getFullYear();
        let dobMonth = dob.getMonth();
        let dobDate = dob.getDate();
        let today = new Date();
        let age = today.getFullYear() - dobyear;
        var m = today.getMonth() - dobMonth;
        if (m < 0 || (m === 0 && today.getDate() < dobDate)) {
            age--;
        }
        if (age < 18) {
            control.setErrors({ dobError: true });
        }
        else if(controlval==undefined || controlval==null || controlval==''){
            control.setErrors(Validators.required);
        }
        else {
            control.setErrors(null);

        }
    }
}

// export function checkAddressValidation(control: string) {
//     return (formGroup: FormGroup) => {
//         let addressControl = formGroup.controls[control];
//     //    console.log(addressControl.value);

//     let regex= /^[ A-Za-z0-9@.'%\/#&-]*$/;
//     // let regex = /[A-Za-z0-9\.\'\-\#\@\%\&\/\ ]+/g;

//     if (!regex.test(addressControl.value)) {
//          console.log("fg");
//         //  var newval= addressControl.value.replace(/^[ A-Za-z0-9@.'%\/#&-]*$/, "");
//         //  addressControl.setValue(newval);

//       }

//     }
// }


