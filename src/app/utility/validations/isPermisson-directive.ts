import { TemplateRef } from '@angular/core';
import { Directive, Input, ViewContainerRef, OnInit } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { CommonService } from 'src/app/services/common.service';
import { SharedService } from 'src/app/services/shared-service.service';

@Directive({
  selector: '[isPermission]'
})

export class permissionDirective {
  isAllow: any;
  permissionData: any = [];
  keyValues: any;
  modules: any
  moduleInfo!: { module: any; permission: any; };
  permissionData1: any;
  isPermitted: any;

  constructor(private commonService: CommonService, private sharedService: SharedService, private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef, private storage: StorageDataService) { }


  @Input() set isPermission(moduleName: any[]) {
    this.modules = this.storage.getStorageData("UserInfo", true) || [];
    if (this.modules.is_admin) {
      this.permissionData.push(this.modules?.user_permissions);
      let menuArr: Array<any> = [];
      moduleName.forEach(m => {
        let arr = m.split("_");
        let moduleInfo = { module: arr[0], permission: arr[1] };
        menuArr.push(moduleInfo);
      })
      if (menuArr && menuArr.length && menuArr.length > 1) {
        for (let i = 0; i < menuArr.length; ++i){
          let isMenuCreated:boolean = false;
          if(this.permissionData[0][menuArr[i]?.module]){
            this.permissionData[0][menuArr[i]?.module].forEach(element => {            
              if ((element?.name == menuArr[i]?.permission) && element?.allowed) {
                this.viewContainer.createEmbeddedView(this.templateRef);
                isMenuCreated = true;
              }
            });
          }
          if(isMenuCreated){
            break;
          }
        }      
      }else if (menuArr && menuArr.length) {
        menuArr.forEach(ele => {
          if(this.permissionData[0][ele.module]){
            this.permissionData[0][ele.module].forEach(element => {            
              if ((element?.name == ele?.permission) && element.allowed) {
                this.viewContainer.createEmbeddedView(this.templateRef);
              }
            });
          }
        })        
      } else {
        this.viewContainer.clear();
      }
    } else {
      if (this.modules.is_super_admin) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
      else {
        this.viewContainer.clear();
      }
    }
  }

}