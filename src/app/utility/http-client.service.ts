import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UtilService } from '../services/util.service';
import { CommonService } from '../services/common.service';
import { SharedService } from '../services/shared-service.service';

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  constructor(private httpClient: HttpClient) {
  
   }

  public HttpPostRequest(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient.post(
      apiUrl + apiServicePath,
      JSON.stringify(requestModel)
    );
  }

  public HttpPostRequestPormise(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient
      .post(apiUrl + apiServicePath, JSON.stringify(requestModel))
      .toPromise();
  }

  public HttpGetRequest(apiServicePath: string) {
    return this.httpClient.get(environment.apiUrl + apiServicePath);
  }

  public HttpGetUserRequest(apiServicePath: string, apiUrl: string) {
    return this.httpClient.get(apiUrl + apiServicePath);
  }

  public HttpPostRequestSelfUserData(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient
      .post(apiUrl + apiServicePath, JSON.stringify(requestModel))
      .toPromise();
  }

  public HttpGetRequestPinCode(apiServicePath: string) {
    return this.httpClient.get(environment.pincodeUrl + apiServicePath);
  }

  public HttpGetRequestUserDetail(apiServicePath: string) {
    return this.httpClient.get(
      environment.apiUrlGetUserDetails + apiServicePath
    );
  }

  public HttpPutRequestCommon(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient.put(
      apiUrl + apiServicePath,
      JSON.stringify(requestModel)
    );
  }

  public HttpGetRequestCommon(apiServicePath: string, apiUrl: string, queryParam?: any) {
    let params = new HttpParams();
    params = queryParam;
    return this.httpClient.get(apiUrl + apiServicePath, { params: params });
  }

  public HttpPostRequestCommon(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient.post(
      apiUrl + apiServicePath,
      JSON.stringify(requestModel)
    );
  }

  public HttpDeleteRequestCommon(apiServicePath: string, apiUrl: string) {
    return this.httpClient.delete(apiUrl + apiServicePath);
  }

  public HttpDeleteWithBody(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(requestModel),
    };

    return this.httpClient.delete(apiUrl + apiServicePath, options);
  }

  public HttpPatchRequestCommon(
    requestModel: any,
    apiServicePath: string,
    apiUrl: string
  ) {
    return this.httpClient.patch(
      apiUrl + apiServicePath,
      JSON.stringify(requestModel)
    );
  }

}
