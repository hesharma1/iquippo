import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AwsConfigService {

  constructor() { }
  public AUTH_CONFIG = {
    Auth: {
      region: environment.region,
      userPoolId: environment.userPoolId,
      userPoolWebClientId: environment.userPoolWebClientId,
      mandatorySignIn: false,
      authenticationFlowType: 'CUSTOM_AUTH'
    }
  }
  public AUTH_CONFIG_PWD = {
    Auth: {
      region: environment.region,
      userPoolId: environment.userPoolId,
      userPoolWebClientId: environment.userPoolWebClientId,
      mandatorySignIn: false,
      authenticationFlowType: 'USER_SRP_AUTH'
    }
  }
}
