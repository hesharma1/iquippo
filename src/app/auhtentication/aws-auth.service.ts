import { Injectable } from '@angular/core';
import { ClientMetadata } from 'amazon-cognito-identity-js';
import Amplify from 'aws-amplify';
import { Auth } from 'aws-amplify';
import { Observable } from 'rxjs';
import { AwsCredentials } from '../models/common/awscredential.model';
import { StorageDataService } from '../services/storage-data.service';
import { ApiRouteService } from '../utility/app.refrence';
import { AwsConfigService } from './aws-config.service';

@Injectable({
  providedIn: 'root',
})
export class AwsAuthService {
  awsCredentials = new AwsCredentials();
  self = Auth;
  
  constructor(public awsConfig: AwsConfigService,public storage:StorageDataService,public apiRouteService:ApiRouteService) { }

  awsLoginWithPassword(username: any, password: any): Promise<any> {
    let promise = new Promise((resolved, reject) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG_PWD);
      Auth.signIn(username, password)
        .then((user) => {
          if (
            user.challengeName == 'NEW_PASSWORD_REQUIRED' ||
            user.attributes.email_verified == 'False'
          ) {
            this.awsCredentials.userObj = user;
          }
          // console.log(user);
          resolved(user);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
    return promise;
  }

  awsLoginWithOtp(username: any): Promise<any> {
    let promise = new Promise((resolved, reject) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG);

      Auth.signIn(username, '')
        .then((user) => {
          //console.log(user);
          // if (
          //   user.challengeName == 'NEW_PASSWORD_REQUIRED' ||
          //   user.attributes.email_verified == 'False'
          // ) {
          //   this.awsCredentials.userObj = user;
          // }
          this.awsCredentials.userObj = user;
          // console.log(user);
          resolved(user);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
    return promise;
  }

  awsVerifyOtp(username: any, code: any): Promise<any> {
    let promise = new Promise((resolved, reject) => {
      // Amplify.configure(this.awsConfig.AUTH_CONFIG);
      // console.log(username);
      // console.log(Auth);
      // console.log(code);
      Auth.sendCustomChallengeAnswer(username, code)
        .then((user) => {
          // console.log(user);
          // if (
          //   user.challengeName == 'NEW_PASSWORD_REQUIRED' ||
          //   user.attributes.email_verified == 'False'
          // ) {
          //   this.awsCredentials.userObj = user;
          // }
          // console.log(user);
          resolved(user);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
    return promise;
  }

  awsForgotPassword(username: any) {
    return new Promise((resolved, reject) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG);
      // console.log(Auth);
      Auth.forgotPassword(username)
        .then((data) => {
          // console.log(Auth);

          // console.log(data);
          resolved(data);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  /*
  * forgot password confirmation with otp and password  
  */
  awsForgotConfirmationPassword(username: any, otp: any, newPassword: any) {
    return new Promise((resolved, reject) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG);
      Auth.forgotPasswordSubmit(username, otp, newPassword)
        .then((data: any) => {
          // console.log(data)
          resolved(data);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });

    });
  }

  async signUp(
    username: string,
    password: string,
    email: string,
    phone_number: string,
    family_name: string,
    given_name: string,
    date_of_birth: string,
    pincode: string,
    socialId: string,
    prefixCode: string,
    countryCode: string,
    groupName: string
  ) {
    return new Promise((resolved, reject) => {
      //this.awsConfig.AUTH_CONFIG.Auth['ClientMetadata'] = {'GroupName' : 'Manufacturer'}
      Amplify.configure(this.awsConfig.AUTH_CONFIG);
      Auth.signUp({
        username,
        password,
        attributes: {
          email, // optional
          phone_number, // optional - E.164 number convention
          family_name,
          given_name,
          'custom:date_of_birth': date_of_birth,
          'custom:pincode': pincode,
          'custom:socialId': socialId,
          'custom:authType': 'otp_login',
          'custom:prefixCode': prefixCode,
          'custom:countryCode': countryCode
          // other custom attributes
        },
        clientMetadata: {
          "GroupName": groupName,
        }
      })
        .then((user) => {
          // if (user.challengeName == "NEW_PASSWORD_REQUIRED" || user.attributes.email_verified == 'False') {
          //   this.awsCredentials.userObj = user;
          // }
          // console.log(user);
          resolved(user);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  awsSignOut() {
    return new Promise(async (resolved, reject) => {
      this.refreshTokenOrGetCurrentSes();
      Amplify.configure(this.awsConfig.AUTH_CONFIG);

     await Auth.signOut().then(data => {
        // console.log(data)
        resolved('data');
      })
        .catch(err => {
          reject(console.log(err));
        });
    });
  }

  refreshTokenOrGetCurrentSes() {
    Amplify.configure(this.awsConfig.AUTH_CONFIG);
    Auth.currentSession()
      .then((data: any) => {
        //  console.log(data)
        //  console.log(data.CognitoUserSession)
        //  console.log(data.idToken.jwtToken)
      })
      .catch(err => console.log(err));
  }

  async refreshToken(resolved: any, reject: any) {
    Amplify.configure(this.awsConfig.AUTH_CONFIG);

    const cognitoUser = await Auth.currentAuthenticatedUser();
    const currentSession: any = await Auth.currentSession();
    console.log({cognitoUser,currentSession})
    cognitoUser.refreshSession(currentSession.refreshToken, (err: any, session: any) => {
      if (err) {
        return reject(err)
      }
      // console.log('session', err, session);
      const { idToken, refreshToken, accessToken } = session;
      localStorage.setItem('sessionTime', session.idToken.payload.exp)
      localStorage.setItem('sessionId', session.idToken.jwtToken)
      localStorage.setItem('id_token', session.idToken.jwtToken)

      resolved(true);
      // do whatever you want to do now 
    });

  }

  /**
   * Change Password
   */
  awsChangePassword(username: any, payload: any) {
    return new Observable((observer) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG);
      Auth.currentAuthenticatedUser()
        .then(user => {
            return Auth.changePassword(user, payload.old_password, payload.new_password);
        })
        .then((data: any) => {
          observer.next(data);
        })
        .catch(err => {
          console.log(err);
          observer.error(err);
        });
    });
  }
  awsGetCurrentUser() {
    return new Observable((observer) => {
      Amplify.configure(this.awsConfig.AUTH_CONFIG);
      Auth.currentAuthenticatedUser()
        .then(user => {
          this.storage.clearStorageData("userData");
          this.storage.setStorageData('sessionTime', user?.signInUserSession?.idToken?.payload?.exp, false);
          this.storage.setStorageData('sessionId', user?.signInUserSession?.idToken?.jwtToken, false);
          this.storage.setStorageData("userData", user, true);
          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData('cognitoId', user?.signInUserSession?.idToken?.payload?.username, false);
          var roleArray = user?.signInUserSession?.idToken?.payload["cognito:groups"];
          if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
            this.storage.setStorageData('rolesArray', roleArray, false);
            if (roleArray.indexOf(this.apiRouteService.roles['superadmin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['superadmin'], false);
              this.storage.setStorageData('userRole', this.apiRouteService.roles['superadmin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['admin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['admin'], false);
              this.storage.setStorageData('userRole', this.apiRouteService.roles['admin'], false);
            }
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['callcenter'], false);
              this.storage.setStorageData('userRole', this.apiRouteService.roles['callcenter'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['rm']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['rm'], false);
              this.storage.setStorageData('userRole', this.apiRouteService.roles['rm'], false);
            }
            else {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['customer'], false);
              this.storage.setStorageData('userRole', this.apiRouteService.roles['customer'], false);
            }
          })
        .catch(err => {
          console.log(err);
          observer.error(err);
        });
    });
  }
}
