import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
//import { NotificationService } from '../utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { AppRouteEnum } from '../utility/app-constants.service';
import { ApiRouteService } from '../utility/app.refrence';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private appRouteEnum: AppRouteEnum, private apiRouteService: ApiRouteService
    //private _notificationservice: NotificationService
    ) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //  console.log(next);
    //  console.log(state);
    //  console.log(localStorage.getItem('userData'));
     let userData = JSON.parse(JSON.stringify(localStorage.getItem('userData')));
     let role =localStorage.getItem("role");
     if(state.url.includes('admin') && (role == this.apiRouteService.roles.customer)){
      this.router.navigate([`/home`]);
      return false;
     }else{
      if (localStorage.getItem('userData') != null) {
        return true;
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { returnUrl: state.url }});
        return false;
      }
     } 
  }

}
