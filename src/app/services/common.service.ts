import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public sendData = new Subject<any>();
  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

  shareData(data) {
    this.sendData.next(data);
  }

  getTokensForUpload() {
    return this.httpClientService.HttpGetUserRequest(this.apiPath.upload, this.apiPath.getTokens)
  }

  getCategoryMaster(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.categoryMaster}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.mastersBaseUrl);
  }

  getBrandMaster(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.brandMaster}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.mastersBaseUrl);
  }

  getModelMaster(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.modelMaster}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.mastersBaseUrl);
  }

  getBrandMasterByCatId(categoryId: number,payload?) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.brandMasterByCategory + categoryId+ '?limit=100', environment.mastersBaseUrl,payload);
  }
  getBrandMasterByCatIdWithSearch(categoryId: number,search?) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.brandMasterByCategory + categoryId+ '?limit=100'+'&search='+search, environment.mastersBaseUrl);
  }

  getModelMasterByBrandId(brandId: number,categoryId) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.modelMasterByBrandCategory + brandId+'/'+categoryId+ '?limit=100', environment.mastersBaseUrl);
  }
  getModelMasterByBrandIdWithSearch(brandId: number,categoryId,search) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.modelMasterByBrandCategory + brandId+'/'+categoryId+ '?limit=100'+'&search='+search, environment.mastersBaseUrl);
  }
  getModelMasterByBrandIdWithSearchNoCat(brandId: number,categoryId,search) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.modelMasterByBrand + brandId+ '?limit=100'+'&search='+search, environment.mastersBaseUrl);
  }

  getSellerList() {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.sellerList, environment.productBaseUrl);
  }

  postSellerData(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentSave, environment.productBaseUrl);
  }

  getCategoryById(categoryId: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.categoryMaster + categoryId, environment.mastersBaseUrl);
  }

  getBrandById(brandId: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.brandMaster + brandId, environment.mastersBaseUrl);
  }

  getModelById(modelId: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.modelMaster + modelId, environment.mastersBaseUrl);
  }

  postBulkUpload(payload: any) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.productTempUsedEquipment, environment.productBaseUrl);
  }
  postBulkUploadEnterprise(payload: any) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.EnterpriseTempValuation, environment.instaValBaseUrl);
  }
  postCallBack(formData: any) {
    return this.httpClientService.HttpPostRequestCommon(formData, this.apiPath.callBack, environment.callBackBaseUrl);
  }

  getCallBack(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.callBack}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.callBackBaseUrl);
  }

  getCallBackRequestList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.callBack, environment.paymentBaseUrl, payload);
  }

  addToWatchlist(request: any) {
    return this.httpClientService.HttpPostRequestCommon(request, this.apiPath.recentlyViewed, environment.partnerBaseUrl);
  }

  getToWatchlist(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.recentlyViewed}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.partnerBaseUrl);
  }

  removeFromWatchlist(queryParam: any) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.removeFromWatchList + (queryParam ? '?' + queryParam : ''), environment.partnerBaseUrl);
  }

  subscribeToNewsletter(queryParam: any) {
    return this.httpClientService.HttpPostRequestCommon(queryParam, this.apiPath.newsletter, environment.bannersBaseUrl);
  }

  getSubscribers(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.newsletter}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.bannersBaseUrl);
  }

  getSubscribersList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newsletter, environment.bannersBaseUrl, payload);
  }

  validateSeller(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.validateSeller, environment.partnerBaseUrl)
  }

  getAssetCategoryMaster(queryParams?: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.assetType}${queryParams ? '?' + queryParams : '?limit=999'}`, environment.mastersBaseUrl);
  }
  getRolesByUser(cognito: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getRolesByUser + cognito, environment.partnerBaseUrl);
  }
  getContactUs(payload: any){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.contactUs, environment.callBackBaseUrl,payload);
  }
  postContactUs(payload: any){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.contactUs, environment.callBackBaseUrl)
  }

}
