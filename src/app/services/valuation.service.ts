import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ValuationService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }
  getInstaValuation(payload:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.instaVal, environment.instaValBaseUrl,payload);
  }
  getEnterValuation(payload:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.enterVal, environment.instaValBaseUrl,payload);
  }
  getAdvancedValuation(payload:any){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.advancedVal, environment.instaValBaseUrl,payload);
  }
  postResumeJob(payload:any){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.resumeJob, environment.instaValBaseUrl);
  }
  getInstaVal(ucn:string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.instaVal}${ucn ? ucn + '/' : ''}`, environment.instaValBaseUrl);
  }
  postInstaVal(formData:any) {
    return this.httpClientService.HttpPostRequestCommon(formData,this.apiPath.instaVal, environment.instaValBaseUrl);
  }
  putInstaVal(ucn:string,formData:any){
    return this.httpClientService.HttpPutRequestCommon(formData,`${this.apiPath.instaVal}${ucn ? ucn + '/' : ''}`, environment.instaValBaseUrl);
  }
  patchInstaVal(ucn:string,request:any){
    return this.httpClientService.HttpPatchRequestCommon(request,`${this.apiPath.instaVal}${ucn ? ucn + '/' : ''}`, environment.instaValBaseUrl); 
  }
  patchAdvanceVal(ucn:string,request:any){
    return this.httpClientService.HttpPatchRequestCommon(request,`${this.apiPath.advancedVal}${ucn ? ucn + '/' : ''}`, environment.instaValBaseUrl); 
  }
  putAssetId(formData:any) {
    return this.httpClientService.HttpPutRequestCommon(formData,`${this.apiPath.productUsedList}${formData.valuation_asset ? formData.valuation_asset + '/' : ''}`, environment.productBaseUrl);
  }
  getAdvancedVal(ucn:string){
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.advancedVal}${ucn ? ucn + '/' : ''}`, environment.advancedValBaseUrl);
  }
  postAdvancedVal(formData:any){
    return this.httpClientService.HttpPostRequestCommon(formData,this.apiPath.advancedVal, environment.advancedValBaseUrl);
  }
  putAdvancedVal(ucn:string,formData:any){
    return this.httpClientService.HttpPutRequestCommon(formData,`${this.apiPath.advancedVal}${ucn ? ucn + '/' : ''}`, environment.advancedValBaseUrl);
  }
  deleteInstaVal(ucn:string){
    return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.instaVal}${ucn ? ucn + '/' : ''}`, environment.instaValBaseUrl);
  }
  saveInstaValCharges(request){
    return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.instaValuationFee,environment.valuationMaster);
  }
  updateInstaValCharges(request,id){
    return this.httpClientService.HttpPutRequestCommon(request,this.apiPath.instaValuationFee + id,environment.valuationMaster);
  }
  getInstaValuationFeeList(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.instaValuationFee,environment.valuationMaster,payload);
  }
  getInstaValuationFee(id){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.instaValuationFee + id,environment.valuationMaster);
  }
  deleteInstaValSettings(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.instaValuationFee + id,environment.valuationMaster);
  }
  saveAdvValCharges(request){
    return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.advanceValuationFee,environment.valuationMaster);
  }
  updateAdvValCharges(request,id){
    return this.httpClientService.HttpPutRequestCommon(request,this.apiPath.advanceValuationFee + id,environment.valuationMaster);
  }
  getAdvValuationFeeList(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.advanceValuationFee,environment.valuationMaster,payload);
  }
  getAdvValuationFee(id){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.advanceValuationFee + id,environment.valuationMaster);
  }
  deleteAdvValSettings(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.advanceValuationFee + id,environment.valuationMaster);
  }
  saveEnterpriseValuation(request){
    return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.enterpriseValuation,environment.instaValBaseUrl);
  }
  putEnterpriseValuation(request,id){
    return this.httpClientService.HttpPutRequestCommon(request,this.apiPath.enterpriseValuation +id,environment.instaValBaseUrl);
  }
  deleteEnterpriseValuation(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.enterpriseValuation +id,environment.instaValBaseUrl);
  }
  getEnterpriseValById(id){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.enterpriseValuation +id,environment.instaValBaseUrl);
  }
  getPurposeList(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.purposeMaster,environment.valuationMaster,payload);
  }
  getEnterpriseCustomerList(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList,environment.partnerBaseUrl,payload);
  }
  postCancelJob(payload:any){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.cancelJob, environment.instaValBaseUrl);
  }
  getIndividualInvoice(payload: any){
    // return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.individualInvoice}?${queryParams}`, environment.tradeBaseUrl, payload);
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.individualInvoice,environment.tradeBaseUrl,payload);
  }
  generateAnnexure(payload: any){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.generateAnnexure, environment.tradeBaseUrl);
  }
  getcreditNoteData(payload: any){
    // return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.individualInvoice}?${queryParams}`, environment.tradeBaseUrl, payload);
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.creditNote,environment.tradeBaseUrl,payload);
  }
  generateInvoice(payload: any){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.generateInvoice, environment.tradeBaseUrl);
  }
  putModifyInvoice(invoice_number:string,formData:any){
    return this.httpClientService.HttpPutRequestCommon(formData,this.apiPath.modifyInvoice + invoice_number + '/', environment.tradeBaseUrl);
  }
  postInvoice(formData:any){
    return this.httpClientService.HttpPostRequestCommon(formData,this.apiPath.modifyInvoice, environment.tradeBaseUrl);
  }
  putInvoice(formData:any,invoice_number){
    return this.httpClientService.HttpPutRequestCommon(formData,this.apiPath.modifyInvoice+ invoice_number+ '/', environment.tradeBaseUrl);
  }
  getInvoice(invoice_number){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.modifyInvoice+ invoice_number + '/',environment.tradeBaseUrl);
  }
  getEnterpriseValSettings(payload:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.enterpriseValSettings, environment.valuationMaster,payload);
  }
  postEnterpriseValSettings(payload:any) {
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.enterpriseValSettings, environment.valuationMaster);
  }
  getgstdetails(payload:any){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.gstDetails,environment.valuationMaster,payload);
  }
  putEnterpriseValSettings(id,payload:any) {
    return this.httpClientService.HttpPutRequestCommon(payload,this.apiPath.enterpriseValSettings + id, environment.valuationMaster);
  }
  getEnterpriseValSettingsById(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.enterpriseValSettings +id, environment.valuationMaster);
  }
  deleteEnterpriseValSettings(id:any) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.enterpriseValSettings+id, environment.valuationMaster);
  }
  getTotalFee(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.getTotalFee, environment.instaValBaseUrl);
  }
  getSaacCode(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.servicesmaster, environment.valuationMaster,payload);
  }
  getGstRate(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.gstMaster, environment.valuationMaster,payload);
  }
  getIquippoGst(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.iQuippoGst, environment.valuationMaster,payload);
  }
  postCancelInvoice(payload){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.cancelInvoice, environment.instaValBaseUrl);
  }
  invoiceFeeDetails(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.invoiceFeeDetails, environment.instaValBaseUrl,payload);
  }
  batchUpdateTax(payload){
    return this.httpClientService.HttpPatchRequestCommon(payload,this.apiPath.batchTaxUpdate, environment.instaValBaseUrl); 
  }
  generateCreditNote(payload){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.creditNote, environment.instaValBaseUrl);
  }
  getEnterpriseList(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityMapping, environment.partnerBaseUrl,payload);
  }
  getPartnerCorporateAddress(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerCorporateAddress, environment.partnerBaseUrl, payload);
}
  postAdvVal(payload){
  return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.advanceValuation, environment.instaValBaseUrl);
  }
}
