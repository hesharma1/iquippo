import { Injectable } from '@angular/core';
import { HttpClientService } from '../utility/http-client.service';
import { ApiRouteService } from '../utility/app.refrence';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AccountService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

    otpValidator(otpRequestcall: any) {
        return this.httpClientService.HttpPostRequest(otpRequestcall, this.apiPath.otpRequest, environment.apiUrl);
    }

    otpcheck(otpcheckcall: any) {
        return this.httpClientService.HttpPostRequest(otpcheckcall, this.apiPath.otpcheck, environment.apiUrl);
    }

    getDetails(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.verifyUser, environment.socialUrl);
    }

    getCountryDetails() {
        return this.httpClientService.HttpPostRequest({}, this.apiPath.countryDetailsUrl, environment.countryApiUrl);
    }

    getPinCode(code: string) {
        return this.httpClientService.HttpGetRequestPinCode(code);
    }

    getUserDetails(userId: any) {
        return this.httpClientService.HttpGetRequestUserDetail(userId);
    }

    verifyPanCardDetails(pinRequestModel: any) {
        return this.httpClientService.HttpPostRequest(pinRequestModel, this.apiPath.verifyPanDetail, environment.panCardverifyUrl);
    }

    getBankList() {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.bankMaster, environment.mastersBaseUrl);
    }
    getBankListOnboard(queryParams:string) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.bankMaster}${queryParams}`, environment.mastersBaseUrl);
    }
    getPartnerBankDetails(queryParams:string) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.getPartnerBankDetails}${queryParams}`, environment.partnerBaseUrl);
    }
    postPartnerBankDetails(payload: any) {
        return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.partnerBankDetails, environment.partnerBaseUrl);
    }

    searchOraganization(payload: any) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerIdentityProof, environment.partnerBaseUrl, payload);
    }

    getPartnerCorporateAddress(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerCorporateAddress, environment.partnerBaseUrl, payload);
    }

    mapOrganization(payload: any) {
        return this.httpClientService.HttpPostRequest(payload, this.apiPath.partnerEntityMapping, environment.partnerBaseUrl);
    }

}