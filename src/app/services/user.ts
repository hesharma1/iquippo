import { Injectable } from '@angular/core';
import { HttpClientService } from '../utility/http-client.service';
import { ApiRouteService } from '../utility/app.refrence';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

    getDetails(data: any) {
        return this.httpClientService.HttpPostRequest(data, this.apiPath.users, this.apiPath.usersUrl);
    }

    getUserDetail(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.getUser, this.apiPath.userGetSaveUrl);
    }
    
    saveUserDetail(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.saveUser, this.apiPath.userGetSaveUrl);
    }

    verifyUserDetail(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.verifyDoc, this.apiPath.verifydocUrl);
    }
    updateStatus(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.updateStatus, this.apiPath.updateStatusUrl);
    }
    updateActiveStatus(userCognito:any,userRequest: any) {
        return this.httpClientService.HttpPatchRequestCommon(userRequest, this.apiPath.savePartnerProfile + userCognito +'/' , environment.partnerBaseUrl);
    }

    downloadImage(userRequest: any) {
        return this.httpClientService.HttpPostRequest(userRequest, this.apiPath.downloadImages, this.apiPath.downloadImagesUrl)
    }
}