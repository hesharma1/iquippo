import { Injectable } from '@angular/core';
import { HttpClientService } from '../utility/http-client.service';
import { ApiRouteService } from '../utility/app.refrence';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ProductlistService {
  
  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

  getUsedProductListWithPayLoad(searchBy: any, page: number, isApprovedStatus: number, payLoad: any,cognitoId, filterQueryParam: any) {
    // let query = searchBy == undefined || searchBy == '' ? '?limit=999' + '&status__in=' + isApprovedStatus +'&ordering=-id'  : searchBy + 'limit=999' + '&status__in=' + isApprovedStatus +'&ordering=-id' ;
    let query = searchBy == undefined || searchBy == '' ?  '?page=' + page   : searchBy + '&page=' + page  ;
    if(cognitoId)
    // query = query +'&cognito_id='+cognitoId + '&'+filterQueryParam;
    query = query +'&cognito_id='+cognitoId;
    if(filterQueryParam)
    query = query + '&' + filterQueryParam;
    query = query + '&is_sell=true&is_active=true';
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + query, environment.productBaseUrl, payLoad);
  }

  getNewEquipmentListWithPayLoad(searchBy : any,page:number, isApprovedStatus : number, payLoad : any,cognitoId, filterQueryParam: any) {
    // let query= searchBy == undefined || searchBy == '' ? '?limit=999'+'&status__in='+isApprovedStatus +'&ordering=-id' : searchBy+'limit=999'+'&status__in='+isApprovedStatus +'&ordering=-id';
    let query= searchBy == undefined || searchBy == '' ? '?status__in='+isApprovedStatus+',3,5' + '&page=' + page : searchBy+'status__in='+isApprovedStatus + '&page=' + page ;
    if(cognitoId)
    query = query +'&cognito_id='+cognitoId;
    if(filterQueryParam)
    query = query + '&' + filterQueryParam;
    query = query + '&is_sell=true&is_active=true';
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentSave + query, environment.productBaseUrl, payLoad);
  }
  
  getNewEquipmentFilters(query,payload?){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentFilters + query, environment.productBaseUrl, payload);
  }

  getUsedEquipmentFilters(query,payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedEquipmentFilters + query, environment.productBaseUrl, payload);
  }

  getAllUsedEquipmentList(isApprovedStatus: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + '?limit=' + 999 + '&status__in=' + isApprovedStatus, environment.productBaseUrl);
  }

  getUsedProWithList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList, environment.productBaseUrl, payload);  
  }

  getNewProductWithList(payload:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentSave, environment.productBaseUrl,payload);
  }

  exportNewProduct(){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.exportNewEquipment, environment.productBaseUrl);
  }

  exportUsedProduct(){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.exportUsedEquipment, environment.productBaseUrl);
  }

  getNewProductList(page: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentSave + '?page=' + page, environment.productBaseUrl);
  }

  getUsedProList(page: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + '?page=' + page, environment.productBaseUrl);
  }

  getAllFilters(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getLandingPageFilter, environment.productBaseUrl,payload);
  }

  getlandingPageFilterForUsed(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getLandingPageFilterUsed, environment.productBaseUrl,payload);
  }

  getTransactionList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsTransaction, environment.paymentBaseUrl, payload);
  }

  getUserTransactionList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsUserTransactions, environment.paymentBaseUrl, payload);
  }
  
  deleteAsset(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.usedProductList + id + '/', environment.productBaseUrl);
  }
  
  deleteNewAsset(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.newEquipmentSave + id + '/', environment.productBaseUrl);
  }
}
