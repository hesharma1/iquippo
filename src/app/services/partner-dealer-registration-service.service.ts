import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class PartnerDealerRegistrationService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

    getLocations(queryParams: string = '',id : number) {
      return (id != null && id != 0) ? this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerlocationMaster}${id}`, environment.mastersBaseUrl+"/")
      : this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerlocationMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl+"/");    
    }

    getPartnerDealerBasicProfile(id : string) {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerBasicProfile}${id}`, environment.partnerBaseUrl).toPromise();
    }
    getPartnerAllInfo(id : string) {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerAllProfile}${id}`, environment.partnerBaseUrl);
    }
    getPartnerContactAllInfo(id : string) {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerAllProfile}${id}`, environment.partnerBaseUrl).toPromise();
    }
    updatePartnerStatus(basicDetailsRequest:any,requestURL:string,id:number){
      return this.httpClientService.HttpPutRequestCommon(basicDetailsRequest, `${requestURL}/${id}/`,environment.partnerBaseUrl);
    }
    patchPartnerStatus(basicDetailsRequest:any,requestURL:string,id:number){
      return this.httpClientService.HttpPatchRequestCommon(basicDetailsRequest, `${requestURL}/${id}/`,environment.partnerBaseUrl);
    }
    getPartnerDealerAddressDetails(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerUserAddress}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl).toPromise();
     }

    savePartnerProfile(request:any) {
      return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.savePartnerProfile,environment.partnerBaseUrl);
    }

    verifyPanCardDetails(pinRequestModel: any) {
      return this.httpClientService.HttpPostRequest(pinRequestModel, this.apiPath.verifyPan, environment.partnerBaseUrl);
    }

    getPartnerDealerIdentityProofs(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerIdentityProofs}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl).toPromise();
    }

    getPartnerDealerContactEntity(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerDealerContactEntity}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl).toPromise();
    }

    createUpdatePartnerDealerDetails(basicDetailsRequest : any,requestURL :string,action : string,id:any) {
       return (id==undefined || id == null || id == '') ?
              this.httpClientService.HttpPostRequestCommon(basicDetailsRequest,requestURL,environment.partnerBaseUrl):
              this.httpClientService.HttpPutRequestCommon(basicDetailsRequest, `${requestURL}${id}/`,environment.partnerBaseUrl);
    }
    updatePartnerContactRolePermission(basicDetailsRequest : any,requestURL :string,id:any) {
       return      this.httpClientService.HttpPatchRequestCommon(basicDetailsRequest,requestURL + id +'/',environment.partnerBaseUrl);
   }
    createUpdatePartnerContactEntityMapping(basicDetailsRequest : any,requestURL :string,id:any,action : string) {
      return (action === 'Save') ?
             this.httpClientService.HttpPostRequestCommon(basicDetailsRequest,requestURL,environment.partnerBaseUrl):
             this.httpClientService.HttpPatchRequestCommon(basicDetailsRequest,requestURL + id +'/',environment.partnerBaseUrl);
   }
    createUpdatePartnerDealerIdentityProofs(basicDetailsRequest : any,requestURL :string,action : string) {
        // return (action === 'Save') ?
        //        this.httpClientService.HttpPostRequestCommon(basicDetailsRequest,requestURL,environment.partnerBaseUrl):
        return this.httpClientService.HttpPatchRequestCommon(basicDetailsRequest,requestURL,environment.partnerBaseUrl);
     }
    getBrandMaster(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.modelMasterByBrand}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }
    getPartnerContactList(page:number, isApprovedStatus : number,limit:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerContactLinks+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit, environment.partnerBaseUrl);
    }
    
    getPartnerContacts() {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerContactLinks, environment.partnerBaseUrl);
    }
    getPartnerEntityListPagination(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.partnerEntityList}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl);
    }
    getPartnerEntityList(page:number, isApprovedStatus : number,limit:any,mobile:any,ordering:any,partnerType:any,entityType:any) {
      if(mobile == '') {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit +'&ordering='+ordering+'&partnership_type__in='+partnerType+'&entity_type__in='+entityType, environment.partnerBaseUrl);
      } else {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit+'&search='+mobile+'&ordering='+ordering+'&partnership_type__in='+partnerType+'&entity_type__in='+entityType, environment.partnerBaseUrl);
      }
    }
    // getPartnerEntityApprovedList(page:number, isApprovedStatus : number,limit:any,mobile:any,ordering:any,status?:any) {
    //   if(status!=undefined && status!=''){
    //   if(mobile == '') {
    //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit +'&status__in='+status+'&ordering='+ordering , environment.partnerBaseUrl);
    //   } else {
    //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit+'&search='+mobile+'&status__in='+status+'&ordering='+ordering , environment.partnerBaseUrl);
    //   }
    //   }
    //   else{
    //     if(mobile == '') {
    //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit +'&ordering='+ordering, environment.partnerBaseUrl);
    //     } else {
    //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntityList+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit+'&search='+mobile+'&ordering='+ordering, environment.partnerBaseUrl);
    //     }
    //   }
    // }
    getPartnerContactListWithoutId(page:number, limit:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity+'?page='+page+'&limit='+limit, environment.partnerBaseUrl);
    }
    getPartnerContactListWithoutIdordering(page:number, limit:any,ordering:any) {
      // var path = this.apiPath.partnerContactLinks+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit;
      // if(mobileNumber!=''){
      //   path += '?user__mobile_number__in='+mobileNumber
      // }
      // if(firstName!=''){
      //   path += '?user__first_name__icontains='+firstName
      // }
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity+'?page='+page+'&limit='+limit+'&ordering='+ordering, environment.partnerBaseUrl);
    }
    getUserManagementNewList(page:number, limit:any,ordering:any,role:any,cognito_id:any,filterKyc?:number,mobile?:string,name?:string,rm?:string,user_created_start?:any,user_created_end?:any) {
    var url = this.apiPath.partnerDealerBasicProfile+'?page='+page+'&limit='+limit+'&ordering='+ordering+'&role='+role+'&cognito_id='+cognito_id;
      if(filterKyc!=undefined && filterKyc!=0)
      {
        url += '&kyc_verified__in='+filterKyc ;
      }
      if(mobile!=undefined && mobile!='')
      url += '&mobile_number__icontains='+mobile;
      if(name !=undefined && name!=''){
        url += '&first_name__icontains='+name;
      }
      if(rm!=undefined && rm !=''){
        url += '&relationship_manager__user__cognito_id__in='+rm;
      }
      if(user_created_start !=undefined && user_created_start !=''){
        url += '&created_at__gte='+user_created_start;
      }
      if(user_created_end !=undefined && user_created_end !=''){
        url += '&created_at__lte='+user_created_end;
      }
      return this.httpClientService.HttpGetRequestCommon(url, environment.partnerBaseUrl);
    
    // return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerBasicProfile+'?page='+page+'&limit='+limit+'&ordering='+ordering+'&role='+role+'&cognito_id='+cognito_id, environment.partnerBaseUrl);
    }
    exportPartnerContactList(limit:any,ordering:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity+'?limit='+limit+'&ordering='+ordering, environment.partnerBaseUrl);
    }
    approveRejectEntity(requestModel: any, id: number) {
      return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.partnerEntityList + id + '/', environment.partnerBaseUrl);
    }
    approveRejectContactPartnerEntity(requestModel: any, id: number) {
      return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.partnerContactList + id + '/', environment.partnerBaseUrl);
    }
    deleteEntity(id: number) {
      return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.partnerEntityList + id + '/', environment.partnerBaseUrl);
    }
    deleteContactEntity(id: number) {
      return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.partnerContactList + id + '/', environment.partnerBaseUrl);
    }
    getPartnerDetailsTax(page:number, isApprovedStatus : number,limit:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerTaxDetails+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit, environment.partnerBaseUrl);
    }
    getPartnerIdentityProof(page:number, isApprovedStatus : number,limit:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerIdentityProofs+'?page='+page+'&user__cognito_id__in='+isApprovedStatus+'&limit='+limit, environment.partnerBaseUrl);
    }

    checkUserRegistered(mobileNo:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerContactRegistered+'?user__mobile_number__in='+mobileNo, environment.partnerBaseUrl);
    }
    checkPartnerEntitiyRegistered(mobileNo:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerEntity+'?mobile__in='+mobileNo, environment.partnerBaseUrl);
    }

    postUser(mobileNo:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerContactRegistered+'?user__mobile_number__in='+mobileNo, environment.partnerBaseUrl);
    }
    checkCongnitoUser(request:any) {
      return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.contactCognito,environment.partnerAdminAuthenticate);

    }
    saveNewContactProfile(request:any) {
      return this.httpClientService.HttpPostRequestCommon(request,this.apiPath.createPartnerContact,environment.partnerContact);

    }
    // getPartnerContactListWithoutId(page:number, limit:any) {
    //   return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity+'?page='+page+'&limit='+limit, environment.partnerBaseUrl);
    // }
    getRoleMastersList(url : any) {
      return this.httpClientService.HttpGetRequestCommon(url, environment.mastersBaseUrl);
    }
    getChannelPartnerList(url : any) {
      return this.httpClientService.HttpGetRequestCommon(url, environment.mastersBaseUrl);
    }
    getEnterprisePermission(url : any) {
      return this.httpClientService.HttpGetRequestCommon(url, environment.enterprisePermBaseUrl);
    }
    getRmList(url : any) {
      return this.httpClientService.HttpGetRequestCommon(url+'?partnership_type__in=3&channel_partner_type__title__in=Relationship Manager&limit=999', environment.partnerBaseUrl);
    }
    getApprovedRmList(url : any) {
      return this.httpClientService.HttpGetRequestCommon(url+'?partnership_type__in=3&channel_partner_type__title__in=Relationship Manager&limit=999&status__in=2', environment.partnerBaseUrl);
    }
    getRmListbyPartnership(url : any,partnership_type?:number) {
      if(partnership_type!=undefined && partnership_type!=0)
      return this.httpClientService.HttpGetRequestCommon(url+'?partnership_type__in=3&channel_partner_type__title__in=Relationship Manager&limit=999', environment.partnerBaseUrl);
      else
      return this.httpClientService.HttpGetRequestCommon(url+'?partnership_type__in=3&channel_partner_type__title__in=Relationship Manager&limit=999', environment.partnerBaseUrl);
    }
    getPermissionList(limit:any) {
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.permission+'?limit='+limit,environment.mastersBaseUrl);
  }
    
    /**get partner contact list for admin */
    
    searchPartnerContact(mobileNumber, firstName){
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity+'?user__mobile_number__in='+mobileNumber+'&user__first_name__icontains='+firstName, environment.partnerBaseUrl);
    }
    searchUserManagement(mobileNumber, firstName,role,cognito_id){
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerBasicProfile+'?mobile_number__icontains='+mobileNumber+'&first_name__icontains='+firstName+'&role='+role+'&cognito_id='+cognito_id, environment.partnerBaseUrl);
    }
}
