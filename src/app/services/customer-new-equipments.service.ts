import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewEquipmentService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService, private httpClient: HttpClient) { }
  /**
   * SAVE NEW EQUIPMENT OF CUSTOMER ON BASIC-DETAIL PAGE
   */
  postBasicDetail(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentSave, environment.productBaseUrl);
  }

  updateBasicDetail(requestModel: any,id:any) {
    return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.newEquipmentSave + id + '/', environment.productBaseUrl);
  }

  getProductList(queryParams: string = '') {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.getNewEquipmentList}${queryParams ? '?' + queryParams : ''}`, environment.productBaseUrl);
  }

  getBasicDetailList(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getNewEquipmentList + id, environment.productBaseUrl);
  }

  getBasicDetailListByCognito(id: any,cognitoId) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getNewEquipmentList + id + (cognitoId ?'?cognito_id='+cognitoId: ''), environment.productBaseUrl);
  }

  getVisuals(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisuals + id, environment.productBaseUrl);
  }

  getVisualsByEquipmentId(id: any, queryParam: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisualsByEquipmentId + id, environment.productBaseUrl, queryParam);
  }
  
  updateNewEquipmentOffers(requestModel: any, id: number) {
    return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.newEquipmentOffers + id + '/', environment.productBaseUrl);
  }

  getNewVisualsByEquipmentId(id: any, queryParam?: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisualsByEquipmentId + id, environment.productBaseUrl, queryParam);
  }

  postupdateNewEquipment(requestModel: any, id: number) {
    return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.getNewEquipmentList + id + '/', environment.productBaseUrl);
  }
  patchupdateNewEquipment(requestModel: any, id: number) {
    return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.getNewEquipmentList + id + '/', environment.productBaseUrl);
  }
  

  getAllOfferStates(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.offerMaster + 'get_states_with_offer/', environment.mastersBaseUrl, payload);
  }

  getOffersByEquipmentId(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentOffersByEquimentId, environment.mastersBaseUrl, payload);
  }

  postDemo(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.demoNew, environment.productBaseUrl);
  }

  postEnquiry(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentLead, environment.productBaseUrl)
  }

  postNewEquipmentDetails(requestModel: any, id: any) {
    return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.newEquipmentSave + id + '/', environment.productBaseUrl);
  }
  patchNewEquipmentDetails(requestModel: any, id: any) {
    return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.newEquipmentSave + id + '/', environment.productBaseUrl);
  }

  postvisualsDetail(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentVisuals, environment.productBaseUrl);
  }

  deleteVisual(id: number) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.newProductVisualData + id + '/', environment.productBaseUrl);
  }

  /**post new equipment visual details */
  getVisualList() {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisuals, environment.productBaseUrl);
  }

  /**post new equipment visual details */
  getUSPList(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.UspList + id + '/', environment.productBaseUrl);
  }

  /**post new equipment visual details */
  postUSPListNew(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.UspList, environment.productBaseUrl);
  }

  /**post new equipment visual details */
  postVisualDetails(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.getVisualDetail, environment.productBaseUrl);
  }
  /**post new equipment visual details */
  updateVisualDetails(requestModel: any,id:any) {
    return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.getVisualDetail + id + '/', environment.productBaseUrl);
  }

  /**get new equipment offer by Id */
  newEquipmentOffers(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newProdcutGetOffers + id, environment.productBaseUrl);
  }

  /**get new equipment offers  */
  newEquipmentOffersPost(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newProdcutGetOffers, environment.productBaseUrl);
  }

  /**get all states */
  getStateMaster(queryParams: string = '') {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
  }

  /**post new equipment visual details */
  postOffersToMaster(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.offerMaster, environment.mastersBaseUrl);
  }

  /**get new visual tabs */
  getvisualTabs() {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.visualTabsList, environment.mastersBaseUrl);
  }

  getSellerList(queryParams: any) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.sellerAndCustomerList}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl)
  }

  postOfferDetails(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentOffers, environment.productBaseUrl);
  }

  postOfferMaster(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.offerMaster, environment.mastersBaseUrl);
  }

  getBannersData(queryParams) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.landingPageBanner}${queryParams}`, environment.bannersBaseUrl)
  }

  postdocumentUploadNewEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newProductDocumentData, environment.productBaseUrl);
  }

  getDocumentList(id: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getDocumentsForNewProduct + id, environment.productBaseUrl);
  }

  getMyDemoList(payload:any) {    
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.demoNew, environment.productBaseUrl, payload);
  }

  updateDemoRequest(payload:any,id) {    
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.demoNew + id + '/', environment.productBaseUrl);
  }

  deleteMyDemoById(demoId: number) {
    return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.demoNew}${demoId}/`, environment.productBaseUrl);
  }

  getDocUploadedUsedEquipmetById(id: number) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.newEquipmentVisualsByEquipmentId}${id}`, environment.productBaseUrl)
  }

  getOffersFromMaster(queryParam: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.offerMaster + '?new_equipment__id__in=' + queryParam, environment.mastersBaseUrl);
  }

  getPincodeMaster(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.masterPincode, environment.mastersBaseUrl, payload);
  }

  updateOfferMaster(requestModel:any,id){
    return this.httpClientService.HttpPutRequestCommon(requestModel,this.apiPath.offerMaster + id, environment.mastersBaseUrl);
  }

  deleteOfferMaster(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.offerMaster + id, environment.mastersBaseUrl);
  }

  getMyEnquiryList(payload:any) {    
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentLead, environment.productBaseUrl, payload);
  }

  deleteEnquiry(id){
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.newEquipmentLead + id, environment.productBaseUrl);
  }

  getEnquiryDetails(id){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentLead + id, environment.productBaseUrl);
  }
  
  getEnquiryChatDetails(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.enquiryChat, environment.productBaseUrl, payload);
  }

  postChatDetail(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.enquiryChat, environment.productBaseUrl);
  }

  updateChatSeen(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.enquiryChatUpdateSeen, environment.productBaseUrl);
  }

  updateEnquiryDetails(payload, id){
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.newEquipmentLead + id + '/', environment.productBaseUrl);
  }
  getRecentlyViewed(cognitoId){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.recentlyViewed + '?user__cognito_id__iexact='+cognitoId + '&type__in=1&new_equipment__isnull=false', environment.partnerBaseUrl);

  }
}

