import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class AdminMasterService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

    getBrandMaster(queryParams: string) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.brandMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getCategoryTechSpecificationList(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.categoryTechSpecification, environment.mastersBaseUrl, payload);
    }

    getModelTechSpecificationList(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.getModelTechSpecList, environment.mastersBaseUrl, payload);
    }
    
    getTechInfoMasterUsedList(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.techInfoMasterUsed, environment.mastersBaseUrl, payload);
    }

    getBrandMasterList(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.brandMaster, environment.mastersBaseUrl, payload);
    }

    getBrandMasterByCatId(categoryId: number) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.brandMasterByCategory + categoryId, environment.mastersBaseUrl);
    }

    /**
     * getModelMasterByBrandId
     */
    getModelMasterByBrandId(brandId: number) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.modelMasterByBrand + brandId, environment.mastersBaseUrl);
    }


    postBrandMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.brandMaster, environment.mastersBaseUrl);
    }

    putBrandMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.brandMaster}${id}/`, environment.mastersBaseUrl);
    }

    getCategoryMaster(queryParams: string = '') {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.categoryMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getCategoryMasterList(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.categoryMaster, environment.mastersBaseUrl, payload);
    }

    postCategoryMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.categoryMaster, environment.mastersBaseUrl);
    }

    putCategoryMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.categoryMaster}${id}/`, environment.mastersBaseUrl);
    }

    getCountryMaster(queryParams: string = '') {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.countryMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    postCountryMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.countryMaster, environment.mastersBaseUrl);
    }

    putCountryMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.countryMaster}${id}/`, environment.mastersBaseUrl);
    }

    deleteCountryMaster(id: any) {
        return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.countryMaster}${id}/`, environment.mastersBaseUrl);
    }
    getStateMasterByStateid(id: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${id}/`, environment.mastersBaseUrl);
    }
    getStateMaster(queryParams: string = '') {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    postStateMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.stateMaster, environment.mastersBaseUrl);
    }

    putStateMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.stateMaster}${id}/`, environment.mastersBaseUrl);
    }

    deleteStateMaster(id: any) {
        return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.stateMaster}${id}/`, environment.mastersBaseUrl);
    }

    getLocationMaster(queryParams: string = '') {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.locationMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }
    getCityMasterById(id: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.cityMaster}${id}/`, environment.mastersBaseUrl);
    }
    getCityMaster(queryParams: string = '') {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.cityMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }
    postCityMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.cityMaster, environment.mastersBaseUrl);
    }

    putCityMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.cityMaster}${id}/`, environment.mastersBaseUrl);
    }

    deleteCityMaster(id: any) {
        return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.cityMaster}${id}/`, environment.mastersBaseUrl);
    }

    getPincodeMaster(queryParams: string = '', payload?) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.masterPincode}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl, payload);
    }
    postPincodeMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.masterPincode, environment.mastersBaseUrl);
    }
    getRmTaggingMaster(queryParams: string = '', payload?: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.rmTagging}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl, payload);
    }
    deleteRmTaggingMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpDeleteWithBody(requestModel, `${this.apiPath.rmTagging}${id}/`, environment.partnerBaseUrl);
    }
    reassignRM(requestModel: any) {
        return this.httpClientService.HttpPatchRequestCommon(requestModel,this.apiPath.reassignRM, environment.partnerBaseUrl);
    }
    postRmTaggingMaster(requestModel: any,cognitoId:string,isEdit:boolean) {
        // if(isEdit)
        // return this.httpClientService.HttpPatchRequestCommon(requestModel, `${this.apiPath.rmTagging}${cognitoId}/`, environment.partnerBaseUrl);
        // else
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.rmTagging}${cognitoId}/`, environment.partnerBaseUrl);
    }
    putPincodeMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.masterPincode}${id}/`, environment.mastersBaseUrl);
    }

    deletePincodeMaster(id: any) {
        return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.masterPincode}${id}/`, environment.mastersBaseUrl);
    }

    postLocationMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.locationMaster, environment.mastersBaseUrl);
    }

    putLocationMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.locationMaster}${id}/`, environment.mastersBaseUrl);
    }

    deleteLocationMaster(id: any) {
        return this.httpClientService.HttpDeleteRequestCommon(`${this.apiPath.locationMaster}${id}/`, environment.mastersBaseUrl);
    }

    createUpdateMasterData(createGroupRequest: any, requestURL: string, action: string, id: number) {
        return (action === 'Save') ?
            this.httpClientService.HttpPostRequestCommon(createGroupRequest, requestURL, environment.mastersBaseUrl) :
            this.httpClientService.HttpPutRequestCommon(createGroupRequest, `${requestURL}${id}/`, environment.mastersBaseUrl);
    }

    modelBatchUpdate(createGroupRequest: any, requestURL: string, action: string, id: number) {
        return this.httpClientService.HttpPatchRequestCommon(createGroupRequest, requestURL, environment.mastersBaseUrl);
    }

    getMasterData(requestURL: string) {
        return this.httpClientService.HttpGetRequestCommon(requestURL, environment.mastersBaseUrl);
    }

    deleteMasterData(requestURL: string) {
        return this.httpClientService.HttpDeleteRequestCommon(requestURL, environment.mastersBaseUrl);
    }

    createUpdateCertificateMasterData(createGroupRequest: any, requestURL: string, action: string, id: number) {
        return (action === 'Save') ?
            this.httpClientService.HttpPostRequestCommon(createGroupRequest, requestURL, environment.mastersBaseUrl) :
            this.httpClientService.HttpPutRequestCommon(createGroupRequest, `${requestURL}${id}/`, environment.mastersBaseUrl);
    }

    getCertificateMasterData(requestURL: string) {
        return this.httpClientService.HttpGetRequestCommon(requestURL, environment.mastersBaseUrl);
    }

    getMasterDataList(requestURL: string, queryParams: string) {
        return this.httpClientService.HttpGetRequestCommon(`${requestURL}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getCertificateMasterDataList(requestURL: string, queryParams: string) {
        return this.httpClientService.HttpGetRequestCommon(`${requestURL}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getCertificateMasterList(queryParams: any) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.certificateMaster, environment.mastersBaseUrl, queryParams);
    }
    deleteCertificateMasterData(requestURL: string) {
        return this.httpClientService.HttpDeleteRequestCommon(requestURL, environment.mastersBaseUrl);
    }

    //for post and put use HttpPostRequestCommon and HttpPutRequestCommon functions respectively
    postOfferMaster(requestModel: any) {
        return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.offerMaster, environment.mastersBaseUrl);
    }

    putOfferMaster(requestModel: any, id: any) {
        return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.offerMaster}${id}/`, environment.mastersBaseUrl);
    }

    getModelMaster(queryParams: string) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.modelMaster}?${queryParams}`, environment.mastersBaseUrl);
    }

    // getTradeAssetData(queryParams: string) {
    //     return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.productUsedList}?${queryParams}`, environment.productBaseUrl);
    // }

    getTradeAssetDataWithPayload(queryParams: string, payLoad: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.productUsedList}?${queryParams}`, environment.productBaseUrl, payLoad);
    }

    getTradeBidData(queryParams: string, payload?: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.tradeBid}?${queryParams}`, environment.tradeBaseUrl, payload);
    }

    getTradeAssetDataById(id: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.productUsedList}${id}`, environment.productBaseUrl);
    }

    getTradeBidDataById(id: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.tradeBid}${id}`, environment.tradeBaseUrl);
    }

    // getTradeBidByEquipmentId(queryParams: string) {
    //     return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.tradeBid}?${queryParams}`, environment.tradeBaseUrl);
    // }

    // getTradeBidByEquipmentIdWithPayLoad(queryParams: string, payload: any) {
    //     return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.tradeBid}?${queryParams}`, environment.tradeBaseUrl, payload);
    // }

    // getTradeBidDataWithPayLoad(queryParams: string, payload: any) {
    //     return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.tradeBid}?${queryParams}`, environment.tradeBaseUrl, payload);
    // }

    puttradeBid(requestModel: any, id: any) {
        return this.httpClientService.HttpPatchRequestCommon(requestModel, `${this.apiPath.tradeBid}${id}/`, environment.tradeBaseUrl);
    }

    putUsedEquipmentById(requestModel: any, id: any) {
        return this.httpClientService.HttpPatchRequestCommon(requestModel, `${this.apiPath.productUsedList}${id}/`, environment.productBaseUrl);
    }

    getCountryList() {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.countryMaster, environment.mastersBaseUrl)
    }

    getTechSpec(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.techSpec, environment.mastersBaseUrl, payload)
    }

    getTradeBidPaymentDetails(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.tradeBidsGet_bid_payment_details, environment.tradeBaseUrl, payload)
    }

    getTradeBidRecords(payload) {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.tradeBid, environment.tradeBaseUrl, payload);
    }



}
