import { EventEmitter, Inject, Injectable, Output } from '@angular/core';
import { ApiRouteService } from '../utility/app.refrence';
import { environment } from 'src/environments/environment';
import { SocialAuthService } from 'angularx-social-login';
import { AccountService } from './account';
import { VerifyUser } from '../models/verifyUserRequest.model';
import { StorageDataService } from './storage-data.service';
import { AwsAuthService } from '../auhtentication/aws-auth.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppRouteEnum } from '../utility/app-constants.service';
import { CountryResponse } from '../models/common/responsedata.model';
import { UserService } from './user';
import { CookieService } from 'ngx-cookie-service';

import { BehaviorSubject, timer, Subject } from "rxjs";
import { SignInProp } from '../../../src/app/component/signin/signin-prop.service.component'; //signin/signin-prop.service.component';
import { NotificationService } from '../utility/toastr-notification/toastr-notification.service';
import { PartnerDealerRegistrationService } from './partner-dealer-registration-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SharedService {
  private CookieValue: string = '';
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();

  isShow: boolean = false;
  queryParams: any;
  googleResponse: any;
  response: any;
  message: string = '';
  queryParamsAfterLogin: any;
  isProfile: boolean = false;
  countryList?: CountryResponse;
  socialFirstName?: string;
  socialLastName?: string;
  userFirstName?: string;
  userLastName?: string;
  assetValuated: any;

  public _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp());

  constructor(private cookieService: CookieService, private awsConfig: AwsAuthService, private apiPath: ApiRouteService, public accountService: AccountService,
    private authService: SocialAuthService, private router: Router, private storage: StorageDataService,
    private appRouteEnum: AppRouteEnum, private spinner: NgxSpinnerService, private userService: UserService,
    public awsCognito: AwsAuthService, private notify: NotificationService, private partnerDealerRegService: PartnerDealerRegistrationService) {
    //this.assetValuated = data.flag;
  }

  // redirecteToMP1(accessToken: any, refreshToken: any, deviceKey: any, idToken: any, isTimeOut: any) {
  //   this.setCookie(this.appRouteEnum.globalCookieName, this.appRouteEnum.globalCookieValue, this.appRouteEnum.globalCookieTime);
  //   const myform = document.createElement('form');
  //   myform.method = 'POST';
  //   myform.action = environment.mp1Url;
  //   myform.style.display = 'none';
  //   const mapInput = document.createElement('input');
  //   mapInput.type = 'hidden';
  //   mapInput.name = 'accessToken';
  //   mapInput.value = accessToken;
  //   myform.appendChild(mapInput);
  //   const mapInput1 = document.createElement('input');
  //   mapInput1.type = 'hidden';
  //   mapInput1.name = 'refreshToken';
  //   mapInput1.value = refreshToken;
  //   myform.appendChild(mapInput1);
  //   const mapInput2 = document.createElement('input');
  //   mapInput2.type = 'hidden';
  //   mapInput2.name = 'deviceKey';
  //   mapInput2.value = deviceKey;
  //   myform.appendChild(mapInput2);
  //   const mapInput3 = document.createElement('input');
  //   mapInput3.type = 'hidden';
  //   mapInput3.name = 'idToken';
  //   mapInput3.value = idToken;
  //   myform.appendChild(mapInput3);
  //   if (this.queryParams != null && this.queryParams != '') {
  //     const mapInput4 = document.createElement('input');
  //     mapInput4.type = 'hidden';
  //     mapInput4.name = 'src';
  //     mapInput4.value = this.queryParams.src;
  //     myform.appendChild(mapInput4);
  //   }
  //   // if (this.queryParamsAfterLogin != null && this.queryParamsAfterLogin != '') {
  //   //     const mapInputSrc = document.createElement('input');
  //   //     mapInputSrc.type = 'hidden';
  //   //     mapInputSrc.name = 'src';
  //   //     mapInputSrc.value = this.queryParamsAfterLogin.src;
  //   //     myform.appendChild(mapInputSrc);
  //   //     const mapInputRefreshToken = document.createElement('input');
  //   //     mapInputRefreshToken.type = 'hidden';
  //   //     mapInputRefreshToken.name = 'src';
  //   //     mapInputRefreshToken.value = this.queryParamsAfterLogin.refreshToken;
  //   //     myform.appendChild(mapInputRefreshToken);
  //   //     const mapInputDeviceKey = document.createElement('input');
  //   //     mapInputDeviceKey.type = 'hidden';
  //   //     mapInputDeviceKey.name = 'src';
  //   //     mapInputDeviceKey.value = this.queryParamsAfterLogin.deviceKey;
  //   //     myform.appendChild(mapInputDeviceKey);
  //   //     const mapInputCognitoId = document.createElement('input');
  //   //     mapInputCognitoId.type = 'hidden';
  //   //     mapInputCognitoId.name = 'cognitoId';
  //   //     mapInputCognitoId.value = this.queryParamsAfterLogin.cognitoId;
  //   //     myform.appendChild(mapInputCognitoId);
  //   // }
  //   document.body.appendChild(myform);
  //   if (isTimeOut) {
  //     setTimeout(function () {
  //       myform.submit();
  //     }, environment.timeoutTime);
  //   }
  //   else {
  //     myform.submit();
  //   }
  //   // 
  // }

  signInWithGoogleAndFacebook(socialProviderId: any, assetValuated: any) {
    return new Promise((resolve, reject) => {
      this.authService.initState.subscribe(value => {
        this.authService.signIn(socialProviderId).then((x) => {
          this.googleResponse = x;
          this.storage.clearStorageData('google_data');
          this.storage.setStorageData("google_data", this.googleResponse, true);
          var verifyUserRequestModel = new VerifyUser();
          verifyUserRequestModel.socialId = this.googleResponse.id;
          verifyUserRequestModel.email = this.googleResponse.email;
          this.accountService.getDetails(verifyUserRequestModel).subscribe(verifyRes => {
            // console.log(verifyRes);
            var getResponse = verifyRes as any;
            this.response = verifyRes as any;
            this.getLoggedInName.emit(true);
            var getResponseBody = getResponse.body;
            var getResponseBodyJSON = JSON.parse(getResponseBody);
            if (getResponse != null) {
              if (getResponse.statusCode == '501') {

                this.storage.setStorageData('socialId', this.googleResponse.id, true);
                if (this.queryParams != null && this.queryParams != undefined) {

                  var pro = this.queryParams.aoc;
                  var src = this.queryParams.src;
                  if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
                    this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { src: this.queryParams.src, aoc: this.queryParams.aoc }, state: { isSocialLogin: true } });
                    resolve(true);
                  }
                  else if (pro != undefined && pro != null && pro == "qtt") {
                    this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { aoc: this.queryParams.aoc }, state: { isSocialLogin: true } });
                    resolve(true);

                  }
                  else if (src != undefined && src != null) {
                    this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { src: this.queryParams.src }, state: { isSocialLogin: true } });
                    resolve(true);

                  }
                  else {
                    this.router.navigate([`./` + this.appRouteEnum.Register], { state: { isSocialLogin: true } });
                    resolve(true);

                  }
                } else {
                  this.router.navigateByUrl('/' + this.appRouteEnum.Register, { state: { isSocialLogin: true } });
                  resolve(true);

                }
                // this.router.navigate([`./register`]);
                //this.otpSection =false;
              }
              if (getResponse.statusCode == '200') {
                // var getResponseBody = getResponse.body;
                // var getResponseBodyJSON = JSON.parse(getResponse);
                if (getResponseBodyJSON != undefined && getResponseBodyJSON != null) {
                  this.awsConfig.awsLoginWithOtp(getResponseBodyJSON.cognitoId)
                    .then((OtpRes) => {
                      // if(RSA_NO_PADDING.)
                      if (OtpRes != undefined && OtpRes != null) {
                        this.awsConfig.awsVerifyOtp(OtpRes, this.googleResponse.id).then((verifyOtpRes) => {
                          //console.log(verifyOtpRes);
                          //this.sharedService.redirecteToMP1(verifyOtpRes.accessToken, verifyOtpRes.refreshToken, verifyOtpRes.deviceKey , verifyOtpRes.idToken);
                          if (verifyOtpRes != undefined && verifyOtpRes != null) {

                            // start - saving UID to cookie
                            var isActive = verifyOtpRes?.attributes["custom:status"];
                            if (isActive != undefined && isActive == "inactive") {

                              this.notify.error('You are not authorized to access the application. Please raise a callback request to get in touch with the iQuippo team.', true)
                              this.logoutInactive();
                              reject(true);

                              return;
                            }
                            var d = new Date();
                            d.setTime(d.getTime() + environment.ExpiryTimeInMS);

                            this.cookieService.set(
                              "userData",
                              this.response,
                              d
                            );
                            //this.CookieValue = this.cookieService.get("UID");
                            //alert(this.CookieValue);

                            // End - saving UID to cookie



                            this.storage.clearStorageData("userData");
                            this.storage.setStorageData("userData", verifyOtpRes, true);
                            this.storage.clearStorageData("cognitoId");
                            this.storage.setStorageData('cognitoId', verifyOtpRes.username, false);
                            this.storage.setStorageData('sessionTime', verifyOtpRes?.signInUserSession?.idToken?.payload?.exp, false);
                            this.storage.setStorageData('sessionId', verifyOtpRes?.signInUserSession?.idToken?.jwtToken, false);
                            if (this.queryParams != null && this.queryParams != "") {
                              var pro = this.queryParams.aoc;
                              if (pro != undefined && pro != null && pro == "qtt") {
                                this.isProfile = true;
                              }
                              else {
                                this.isProfile = false;
                              }
                            }
                            var address_proof_available = verifyOtpRes?.attributes["custom:pan_address_proof"];
                            if (address_proof_available != undefined && address_proof_available != null && address_proof_available != '' && this.isProfile) {
                              this.storage.clearStorageData("address_proof_available");
                              this.storage.setStorageData('address_proof_available', address_proof_available, true);
                            }
                            var roleArray = verifyOtpRes?.signInUserSession?.idToken?.payload["cognito:groups"];
                            if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
                              this.storage.setStorageData('rolesArray', roleArray, false);
                              if (roleArray.indexOf(this.apiPath.roles['superadmin']) != -1) {
                                this.storage.clearStorageData("role");
                                this.storage.setStorageData('role', this.apiPath.roles['superadmin'], false);
                              }
                              else if (roleArray.indexOf(this.apiPath.roles['admin']) != -1) {
                                this.storage.clearStorageData("role");
                                this.storage.setStorageData('role', this.apiPath.roles['admin'], false);
                              }
                              else if (roleArray.indexOf(this.apiPath.roles['callcenter']) != -1) {
                                this.storage.clearStorageData("role");
                                this.storage.setStorageData('role', this.apiPath.roles['callcenter'], false);
                              }
                              else if (roleArray.indexOf(this.apiPath.roles['rm']) != -1) {
                                this.storage.clearStorageData("role");
                                this.storage.setStorageData('role', this.apiPath.roles['rm'], false);
                              }
                              else {
                                this.storage.clearStorageData("role");
                                this.storage.setStorageData('role', this.apiPath.roles['customer'], false);
                              }
                            }
                            if (verifyOtpRes?.attributes["custom:kyc_verified"] == "Verified" && verifyOtpRes?.attributes["custom:pan_address_proof"] == "True" && verifyOtpRes?.attributes["email"] != "" && verifyOtpRes?.attributes["email"] != null && verifyOtpRes?.attributes["email"] != undefined) {
                              if (assetValuated) {
                                resolve(true);
                              }
                              else {
                                this.router.navigate(['/home']);
                              }
                              //this.redirecteToMP1(verifyOtpRes.signInUserSession.accessToken.jwtToken, verifyOtpRes.signInUserSession.refreshToken.token, verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), verifyOtpRes.signInUserSession.idToken.jwtToken, false);
                            }
                            else {
                              if (this.isProfile) {
                                if (this.storage.getStorageData('role', false) == this.apiPath.roles.customer)
                                  this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { deviceKey: verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), refreshToken: verifyOtpRes.signInUserSession.refreshToken.token, cognitoId: verifyOtpRes.username, src: this.queryParams.src, aoc: this.queryParams.aoc } });
                                else {
                                  if (assetValuated) {
                                    resolve(true);
                                  }
                                  else {
                                    this.router.navigate(['/home']);
                                  }
                                  //this.redirecteToMP1(verifyOtpRes.signInUserSession.accessToken.jwtToken, verifyOtpRes.signInUserSession.refreshToken.token, verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), verifyOtpRes.signInUserSession.idToken.jwtToken, false);
                                }
                              }
                              else {
                                if (assetValuated) {
                                  resolve(true);
                                }
                                else {
                                  this.router.navigate(['/home']);
                                }
                                //this.redirecteToMP1(verifyOtpRes.signInUserSession.accessToken.jwtToken, verifyOtpRes.signInUserSession.refreshToken.token, verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), verifyOtpRes.signInUserSession.idToken.jwtToken, false);
                              }
                            }
                          }
                        }).catch((err) => {
                          console.log(err);
                          reject(true);

                        })
                      }
                      // console.log(res);
                    }).catch((err) => {
                      console.log(err);
                      reject(true);

                      if (err.code == "NotAuthorizedException") {
                        this.message = err.message;
                      }
                    });
                }
              }

            }
          })
          //this.router.navigate([`./dashboard`]);

          // console.log(this.googleResponse);
        }).catch((err) => {
          console.log(err)
        })
      });
    })
  }

  getAllPartnerInfo() {
    let cognitoId = this.storage.getStorageData('cognitoId', false);
    //  let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    return new Observable((obs) => {
      if (cognitoId != null && cognitoId != undefined && cognitoId != '') {
        this.partnerDealerRegService.getPartnerAllInfo(cognitoId).subscribe((res: any) => {
          console.log(res);
          this.storage.setStorageData('partnerAllDetail', res, true)
          //obs.next(res);
          obs.next(res);
          obs.complete();
        }, err => {
          //return err;
          obs.error(err);
          //obs.unsubscribe();
        })
      }
    })

  }

  GetSocialUserFullName() {
    let google_data = this.storage.getStorageData("google_data", true);
    if (google_data != undefined) {
      this.socialFirstName = google_data.firstName;
      this.socialLastName = google_data.lastName;
      return `<b>Welcome ${this.socialFirstName} ${this.socialLastName}</b>, <br/>Please provide your Mobile number to complete the SignUp to iQuippo`;
    }
    else {
      return ``;
    }
  }

  download(url: string) {
    if (url == undefined || url == "" || url == null) {
      return;
    }
    if (url.includes('base64')) {
      return;
    }
    var obj = {
      source: "getsignedurl",
      documentUrl: url
    }
    this.userService.downloadImage(obj).subscribe(res => {
      var response = res as any;
      var resJson = JSON.parse(response.body);
      var win1 = window.open(resJson.url, '_blank');
      //win1?.close();
    }, err => {

    })
  }

  viewImage(url: string): Promise<any> {
    let unsignedUrl;
    return new Promise(resolve => {
      if (url == undefined || url == "" || url == null) {
        return;
      }
      if (url.includes('base64')) {
        return;
      }
      var obj = {
        source: "getsignedurl",
        documentUrl: url
      }
      this.userService.downloadImage(obj).subscribe(res => {
        var response = res as any;
        var resJson = JSON.parse(response.body);

        unsignedUrl = resJson.url;
        resolve(unsignedUrl);
      }, err => {

      })
    });
  }

  redirecteToMP1AfterLogin() {
    let userDetails = this.storage.getLocalStorageData('userData', true);
    let redirectUrl;
    let rolesArray = this.storage.getStorageData("rolesArray", false);
    rolesArray = rolesArray.split(',');
    if (rolesArray.includes('Customer')) {
      redirectUrl = environment.customerLoginUrl;
    } else {
      redirectUrl = environment.partnerLoginUrl;
    }
    const myform = document.createElement('form');
    myform.method = 'POST';
    myform.action = redirectUrl;
    myform.style.display = 'none';
    const mapInputRefreshToken = document.createElement('input');
    mapInputRefreshToken.type = 'hidden';
    mapInputRefreshToken.name = 'refToken';
    mapInputRefreshToken.value = userDetails?.signInUserSession?.refreshToken?.token;
    myform.appendChild(mapInputRefreshToken);
    const mapInputDeviceKey = document.createElement('input');
    mapInputDeviceKey.type = 'hidden';
    mapInputDeviceKey.name = 'deviceKey';
    mapInputDeviceKey.value = userDetails?.signInUserSession?.accessToken?.payload['device_key'];
    myform.appendChild(mapInputDeviceKey);
    const mapInputCognitoId = document.createElement('input');
    mapInputCognitoId.type = 'hidden';
    mapInputCognitoId.name = 'userId';
    mapInputCognitoId.value = this.storage.getLocalStorageData('cognitoId', false);
    myform.appendChild(mapInputCognitoId);
    if (rolesArray.includes('Customer')) {
      const pinCode = document.createElement('input');
      pinCode.type = 'hidden';
      pinCode.name = 'pincode';
      pinCode.value = userDetails?.attributes['custom:pincode'];
      myform.appendChild(pinCode);
      const birthDate = document.createElement('input');
      birthDate.type = 'hidden';
      birthDate.name = 'birthDate';
      birthDate.value = userDetails?.attributes['custom:date_of_birth'];
      myform.appendChild(birthDate);
      const firstName = document.createElement('input');
      firstName.type = 'hidden';
      firstName.name = 'firstName';
      firstName.value = userDetails?.attributes['given_name'];
      myform.appendChild(firstName);
      const middleName = document.createElement('input');
      middleName.type = 'hidden';
      middleName.name = 'middleName';
      middleName.value = '';
      myform.appendChild(middleName);
      const lastName = document.createElement('input');
      lastName.type = 'hidden';
      lastName.name = 'lastName';
      lastName.value = userDetails?.attributes['family_name'];
      myform.appendChild(lastName);
      const mobile = document.createElement('input');
      mobile.type = 'hidden';
      mobile.name = 'mobile';
      mobile.value = userDetails?.attributes['phone_number'];
      myform.appendChild(mobile);
      const emailId = document.createElement('input');
      emailId.type = 'hidden';
      emailId.name = 'emailId';
      emailId.value = userDetails?.attributes['email'];
      myform.appendChild(emailId);
      const role = document.createElement('input');
      role.type = 'hidden';
      role.name = 'role';
      role.value = 'customer';
      myform.appendChild(role);
    }
    document.body.appendChild(myform);
    myform.submit();
  }

  setCookie(name: any, value: any, days: any) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/; domain=.iquippo.com";
  }

  getCookie(name: any) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  removeCookie(name: any) {
    document.cookie = name + '= ""; Path=/; domain=.iquippo.com; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  updateUserName() {
    if (localStorage.getItem("userData") != null) {
      var userdata = this.storage.getLocalStorageData("userData", true);
      if (userdata != null && userdata != "" && userdata != undefined && userdata.attributes != null && userdata.attributes != "" && userdata.attributes != undefined) {
        this.userFirstName = userdata?.attributes?.given_name;
        this.userLastName = userdata?.attributes?.family_name;
      }
      else {
        this.userFirstName = userdata?.challengeParam?.userAttributes?.given_name;
        this.userLastName = userdata?.challengeParam?.userAttributes?.family_name;
      }
      // console.log("username");   
      return `${this.userFirstName} ${this.userLastName}`;
    }
    else {
      return ``;
    }

  }

  getUserFirstName() {
    if (localStorage.getItem("userData") != null) {
      var userdata = this.storage.getLocalStorageData("userData", true);
      if (userdata != null && userdata != "" && userdata != undefined && userdata.attributes != null && userdata.attributes != "" && userdata.attributes != undefined) {
        this.userFirstName = userdata?.attributes?.given_name;
        this.userLastName = userdata?.attributes?.family_name;
      }
      else {
        this.userFirstName = userdata?.challengeParam?.userAttributes?.given_name;
        this.userLastName = userdata?.challengeParam?.userAttributes?.family_name;
      }
      // console.log("username");   
      return `${this.userFirstName}`;
    }
    else {
      return ``;
    }

  }

  logout() {
    this.awsCognito.awsSignOut().then(() => {
      // console.log('qsqw');
      // this.navCtrl.setDirection('root');
      this.storage.cleanAll();
      //this.removeCookie(this.appRouteEnum.globalCookieName);
      //this.removeCookie("userData");
      //this.router.navigateByUrl(this.appRouteEnum.Login);
      window.location.href = environment.mp1LogoutUrl;
    }).catch((e: any) => {
      console.log('getting error');
    });
  }

  logoutInactive() {
    this.awsCognito.awsSignOut().then(() => {
      // console.log('qsqw');
      // this.navCtrl.setDirection('root');
      this.storage.cleanAll();
      //this.removeCookie(this.appRouteEnum.globalCookieName);
      //this.removeCookie("userData");
      //this.router.navigateByUrl(this.appRouteEnum.Login);
      setTimeout(() => {
        window.location.href = environment.mp1LogoutUrl;
      }, 2000);

    }).catch((e: any) => {
      console.log('getting error');
    });
  }

  checkImage(url) {
    return new Promise(resolve => {
      var request = new XMLHttpRequest();
      request.open("GET", url, true);
      request.send();
      request.onload = function () {
        // status = request.status;
        if (request.status == 200){
          console.log("image exists");
          resolve(url);
        } else {
          console.log("image doesn't exist");
          resolve('../../../assets/images/default-thumbnail.jpg');
        }
      }
    });
  }
}