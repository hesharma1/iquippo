import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AwsAuthService } from '../auhtentication/aws-auth.service';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';
import { AccountService } from './account';
import { StorageDataService } from './storage-data.service';

declare var Razorpay: any;

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  public userDetails: any;

  constructor(private apiPath: ApiRouteService, private httpClientService: HttpClientService, public accountService: AccountService, public awsCognito: AwsAuthService, public storage: StorageDataService) { }

  getBidBond(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsBidBond, environment.paymentBaseUrl, payload);
  }

  getGstRate(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.masterSetting, environment.paymentBaseUrl, payload);
  }

  setTradeBids(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.tradeBid, environment.tradeBaseUrl);
  }

  updateTradeBids(payload,id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.tradeBid + id + '/', environment.tradeBaseUrl);
  }

  generateOrderId(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsTransaction, environment.paymentBaseUrl);
  }

  payWithRazor(data) {
    this.userDetails = this.storage.getStorageData('userData', true);
    return new Observable((observer) => {
      const options: any = {
        key: 'rzp_test_ufUVcYm6zoemJB',
        amount: data.amount * 100, // amount should be in paise format to display Rs 1255 without decimal point
        currency: 'INR',
        name: 'iQuippo', // company name or product name
        description: data.description,  // product description
        image: '../../assets/images/logo.png', // company logo or product image
        order_id: data.order_id, // order_id created by you in backend
        // callback_url: 'http://localhost:4200/home',
        // redirect: true,
        prefill: {
          name: this.userDetails?.attributes?.given_name + this.userDetails?.attributes?.family_name,
          email: this.userDetails?.attributes?.email,
          contact: this.userDetails?.attributes?.phone_number
        },
        modal: {
          // We should prevent closing of the form when esc key is pressed.
          escape: false,
        },
        notes: {
          // include notes if any
        },
        theme: {
          color: '#0c238a'
        }
      };
      options.handler = ((response, error) => {
        options.response = response;
        observer.next(response);
        if (error) {
          observer.error(error);
        }
        // console.log(options);
        // call your backend api to verify payment signature & capture transaction
      });
      options.modal.ondismiss = (() => {
        // handle the case when user closes the form while transaction is in progress
        observer.error('Transaction cancelled.');
        // console.log('Transaction cancelled.');
      });
      const rzp = new Razorpay(options);
      rzp.open();
    })
  }

  updatePaymentStatus(id, res) {
    return this.httpClientService.HttpPatchRequestCommon(res, this.apiPath.paymentsTransaction + id + '/', environment.paymentBaseUrl);
  }

  getWalletDetails(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsWalletTransactions, environment.paymentBaseUrl, payload);
  }

  getWalletBalance(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsWalletTransactions + 'get_balance/', environment.paymentBaseUrl, payload);
  }

  generateWalletOrderId(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsWalletTransactions, environment.paymentBaseUrl);
  }

  updateWalletTransactionDetails(id, payload){
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.paymentsWalletTransactions + id + '/', environment.paymentBaseUrl);
  }

  getTransactionRefundStatus(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsWalletTransactions + 'get_refunded_amount/', environment.paymentBaseUrl, payload);
  }

  setAuctionStatus(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionEmdPayments, environment.auctionBaseUrl);
  }

  updateFulfillmentStatus(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionFulfillmentPayments, environment.auctionBaseUrl);
  }

  walletPayment(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsWalletTransactions, environment.paymentBaseUrl);
  }

  getEmdPaymentStatus(payload, id?){
    return this.httpClientService.HttpGetRequestCommon('auction/auctions/'+ id +'/emd_pay_check/', environment.auctionBaseUrl, payload);
  }

  generateAuctionOrderId(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionEmdPaymentsInitiateTxnPayment, environment.auctionBaseUrl);
  }

  auctionWalletPayment(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionEmdPaymentsInitiateWalletPayment, environment.auctionBaseUrl);
  }

  auctionTopupPayment(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.initiateTransactionTopup, environment.auctionBaseUrl);
  }

  auctionWalletTopupPayment(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.initiateWalletTopup, environment.auctionBaseUrl);
  }

  updateAuctionRegPaymentStatus(id, res){
    return this.httpClientService.HttpPatchRequestCommon(res, this.apiPath.paymentsTransaction + id + '/finalize-payment-status', environment.paymentBaseUrl);
  }
}
