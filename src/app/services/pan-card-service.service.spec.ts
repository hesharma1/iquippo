import { TestBed } from '@angular/core/testing';

import { PanCardServiceService } from './pan-card-service.service';

describe('PanCardServiceService', () => {
  let service: PanCardServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanCardServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
