import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService, private httpClient: HttpClient) { }
    /**
     * SAVE NEW EQUIPMENT OF CUSTOMER ON BASIC-DETAIL PAGE
     */
    // postBasicDetail(requestModel: any) {
    //     return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentSave, environment.productBaseUrl);
    // }

    getbannersList(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.getHomepageBannerList}${queryParams ? '?' + queryParams : ''}`, environment.bannersBaseUrl);
    }

    getStateMaster(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getCityMaster(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.cityMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }

    getSearchAllUsed(queryParams: string = '') {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
    }
    searchByKeyword(keyword:string, searchType){
      return this.httpClientService.HttpGetRequestCommon(this.apiPath.searchByKeyword + '?keyword='+keyword + '&equipment_type='+searchType, environment.bannersBaseUrl);
    }
    getNotificationsCount(queryParams: string = '',payload) {
      return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.notifications}${queryParams ? '?' + queryParams : ''}`, environment.bannersBaseUrl,payload);
    }
    updateNotificationStatus(queryParams: string = '',payload) {
      return this.httpClientService.HttpPatchRequestCommon(payload,`${this.apiPath.updateNotificationsStatus}${queryParams ? '?' + queryParams : '/'}`, environment.bannersBaseUrl);
    }
    updateNotification(id,payload) {
      return this.httpClientService.HttpPatchRequestCommon(payload,this.apiPath.notifications+ '/' + id+ '/', environment.bannersBaseUrl);
    }
  //   getBasicDetailList(id: any) {
  //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.getNewEquipmentList + id, environment.productBaseUrl);
  //   }

  //   getVisuals(id: any) {
  //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisuals + id, environment.productBaseUrl);
  //   }

  //   getVisualsByEquipmentId(id: any) {
  //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisualsByEquipmentId + id, environment.productBaseUrl);
  //   }

  //   getOffers(id: any) {
  //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentOffers + id, environment.productBaseUrl);
  //   }

  //   getOffersByEquipmentId(equipmentId: any) {
  //     return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentOffersByEquimentId + equipmentId + "/", environment.productBaseUrl);
  //   }

  //   postDemo(requestModel: any) {
  //     return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.demoNew, environment.productBaseUrl);
  //   }

  //   postEnquiry(requestModel:any){
  //     return this.httpClientService.HttpPostRequestCommon(requestModel,this.apiPath.newEquipmentLead,environment.productBaseUrl)
  //   }

  //   getOffersDetailList(id: any) {
  //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.getNewEquipmentList + id, environment.productBaseUrl);
  //   }

  //   postNewEquipmentDetails(requestModel: any, id:any) {
  //       return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.newEquipmentSave + id + '/', environment.productBaseUrl);
  //   }
    
  //   postvisualsDetail(requestModel: any) {
  //       return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentVisuals, environment.productBaseUrl);
  //   }

  //   /**post new equipment visual details */
  //   getVisualList() {
  //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.newEquipmentVisuals, environment.productBaseUrl);
  //   }

  //   /**post new equipment visual details */
  //   getUSPList(queryParams: string = '') {
  //       return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.UspList}${queryParams ? '?' + queryParams : ''}`, environment.productBaseUrl);
  //   }

  //   /**post new equipment visual details */
  //   postVisualDetails(requestModel: any) {
  //       return this.httpClientService.HttpPostRequestCommon(requestModel,this.apiPath.getVisualDetail, environment.productBaseUrl);
  //   }

  //   /**get new equipment offer by Id */
  //   newEquipmentOffers(id: any) {
  //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.newProdcutGetOffers + id, environment.productBaseUrl);
  //   }

  //   /**get new equipment offers  */
  //   newEquipmentOffersPost(requestModel: any) {
  //       return this.httpClientService.HttpPostRequestCommon(requestModel,this.apiPath.newProdcutGetOffers, environment.productBaseUrl);
  //   }

  //   /**get all states */
  //   getStateMaster(queryParams: string = '') {
  //       return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
  //   }

  //   /**post new equipment visual details */
  //   postOffersToMaster(requestModel: any) {
  //       return this.httpClientService.HttpPostRequestCommon(requestModel,this.apiPath.offerMaster, environment.mastersBaseUrl);
  //   }

  //   /**get new visual tabs */
  //   getvisualTabs(){
  //       return this.httpClientService.HttpGetRequestCommon(this.apiPath.visualTabsList, environment.mastersBaseUrl);
  //   }

  //   getSellerList(queryParams: any){
  //       return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.sellerList}${queryParams ? '?' + queryParams : ''}`, environment.productBaseUrl)
  //   }
  //   postOfferDetails(requestModel: any) {
  //       return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.newEquipmentOffers, environment.productBaseUrl);
  //   }
  //   postOfferMaster(requestModel: any) {
  //     return this.httpClientService.HttpPostRequestCommon(requestModel,this.apiPath.offerMaster, environment.mastersBaseUrl);
  // }
  // getBannersData(queryParams){
  //   return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.landingPageBanner}${queryParams}`, environment.bannersBaseUrl)

  // }

  // getMyDemoList(
  //   pageNumber = 0,
  //   pageSize = 3,
  //   sortOrder = 'asc',
  //   filter = ''
  // ) {
  //   return this.httpClient
  //     .get(`${environment.productBaseUrl + this.apiPath.demoNew}`, {
  //       params: new HttpParams()
  //         .set('page', pageNumber.toString())
  //         .set('limit', pageSize.toString())
  //         .set('ordering', sortOrder)
  //         .set('search', filter)
  //     })
  //     .pipe(map((res: any) => res));
  // }

  // deleteMyDemoById(demoId: number) {
  //   return this.httpClientService.HttpDeleteRequestCommon(
  //     `${this.apiPath.demoNew}${demoId}/`,
  //     environment.productBaseUrl
  //   );
  // }

  
}

