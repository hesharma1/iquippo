import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

  getRealizationList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.paymentsTransaction, environment.paymentBaseUrl, payload);
  }

  OnlineOfflineRefundList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.iQuippoTransactions, environment.paymentBaseUrl, payload);
  }

  realizeTransaction(id, val, payload) {
    if (val) {
      return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsTransaction + id + '/realize', environment.paymentBaseUrl);
    } else {
      return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsTransaction + id + '/cancel-realization/', environment.paymentBaseUrl);
    }
  }

  addRefundInfo(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.iQuippoTransactions + id + '/', environment.paymentBaseUrl);
  }

  approveOnlineRefund(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.approveOnlineRefund, environment.paymentBaseUrl);
  }

  approvedOfflineRefundPost(payload, id) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.paymentsTransaction + id + '/refund', environment.paymentBaseUrl);
  }

  approveOfflineRefund(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.paymentsTransaction + id + '/approve_refund', environment.paymentBaseUrl);
  }

}
