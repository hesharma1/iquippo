import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root',
})
export class NewEquipmentMasterService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService
  ) {}

  getCategoryTechSpecificationList(queryParams: string = 'limit=999') {
    return this.httpClientService.HttpGetRequestCommon(
      `${this.apiPath.categoryTechSpecification}?${queryParams}`,
      environment.mastersBaseUrl
    );
  }
  getMasterFields(queryParams: string){
    return this.httpClientService.HttpGetRequestCommon(
      `${this.apiPath.getMasterFields}?${queryParams}`,
      environment.mastersBaseUrl
    );
  }

  getModelTechSpecList(queryParams: string) {
    return this.httpClientService.HttpGetRequestCommon(
      `${this.apiPath.getModelTechSpecification}?${queryParams}`,
      environment.mastersBaseUrl
    );
  }

  updateCategoryField(requestModel: any, catSpecId: any) {
    return this.httpClientService.HttpPutRequestCommon(
      requestModel,
      `${this.apiPath.categoryTechSpecification}${catSpecId}/`,
      environment.mastersBaseUrl
    );
  }

  createCategoryField(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(
      requestModel,
      this.apiPath.categoryTechSpecification,
      environment.mastersBaseUrl
    );
  }

  deleteCategoryField(specId: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.categoryTechSpecification}${specId}/`,
      environment.mastersBaseUrl
    );
  }

  modelTechSpecificationdelete(specId: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.modelTechSpecification}${specId}/`,
      environment.mastersBaseUrl
    );
  }
  deleteModelTechSpec(catId: number, brandId: number, modelId: number) {
    return this.httpClientService.HttpDeleteWithBody(
      { brand: brandId, category: catId, model: modelId },
      `${this.apiPath.deleteModelTech}`,
      environment.mastersBaseUrl
    );
  }
  getModelTechSpecificationList() {
    return this.httpClientService.HttpGetRequestCommon(
      this.apiPath.getModelTechSpecification,
      environment.mastersBaseUrl
    );
  }

  updateModelTechSpecification(categoryId: number, fieldName: string) {
    return this.httpClientService.HttpPostRequestCommon(
      fieldName,
      this.apiPath.modelTechSpecification.replace(
        '{id}',
        categoryId.toString()
      ),
      environment.mastersBaseUrl
    );
  }

  createModelTechSpecification(modelSpecValues: any[]) {
    return this.httpClientService.HttpPostRequestCommon(
      modelSpecValues,
      this.apiPath.modelTechSpecification,
      environment.mastersBaseUrl
    );
  }

  getModelSpecByCategoryBrandModel(
    modelId: number,
    brandId: number,
    catId: number
  ) {
    return this.httpClientService.HttpGetRequestCommon(
      `${this.apiPath.modelSpecByCatBrandModel}?model=${modelId}&category=${catId}&brand=${brandId}`,
      environment.mastersBaseUrl
    );
  }

  batchUpdateModelSpec(specValues: any[]) {
    return this.httpClientService.HttpPatchRequestCommon(
      specValues,
      `${this.apiPath.batchUpdateModelSpec}`,
      environment.mastersBaseUrl
    );
  }

  deleteBannerSettingsField(bannerId: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.bannerSettingsList}${bannerId}/`,
      environment.bannersBaseUrl
    );
  }

  postBannerSettings(requestModel : any){
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.bannerSettingsList, environment.bannersBaseUrl);
  }

  putBannerSettings(requestModel : any,id){
    return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.bannerSettingsList}${id}/`, environment.bannersBaseUrl);
  }

  editBannerSettingsField(bannerId: number) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.bannerSettingsList}${bannerId}/`, environment.bannersBaseUrl);
  }
  editHomepageBannerSettingsField(bannerId: number) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.homePageBanner}${bannerId}/`, environment.bannersBaseUrl);
  }
  putHomePageBannerSettings(requestModel : any,id){
    return this.httpClientService.HttpPutRequestCommon(requestModel, `${this.apiPath.homePageBanner}${id}/`, environment.bannersBaseUrl);
  }

  postHomePageBannerSettings(requestModel : any){
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.homePageBanner, environment.bannersBaseUrl);
  }
  deleteHomePageBannerSettingsField(bannerId: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.homePageBanner}${bannerId}/`,
      environment.bannersBaseUrl
    );
  }
}
