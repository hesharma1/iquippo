import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class AuctionService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

  getAuctionList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions, environment.auctionBaseUrl, payload);
  }

  getLotAuctionByIdList(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions + id + '/lots', environment.auctionBaseUrl);
  }

  getLotsById(queryParam: any, id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions + id + '/lots/' + (queryParam ? '?' + queryParam : ''), environment.auctionBaseUrl);
  }

  deleteAuction(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.auctions + id + '/', environment.auctionBaseUrl);
  }

  createAuction(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctions, environment.auctionBaseUrl);
  }

  getAuctionByID(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions + id + '/', environment.auctionBaseUrl);
  }

  updateAuction(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.auctions + id + '/', environment.auctionBaseUrl);
  }

  getAuctionLotList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionsLot, environment.auctionBaseUrl, payload);
  }

  getAuctionLotDetails(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionsLot + id + '/', environment.auctionBaseUrl);
  }

  createAuctionLot(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionsLot, environment.auctionBaseUrl);
  }

  updateAuctionLot(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.auctionsLot + id + '/', environment.auctionBaseUrl);
  }

  createAssetLot(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionLotAssets, environment.auctionBaseUrl);
  }

  postAuctionEmd(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.auctionsEmd, environment.auctionBaseUrl);
  }

  updateAuctionEmd(payload, id) {
    return this.httpClientService.HttpPutRequestCommon(payload, this.apiPath.auctionsEmd + id + '/', environment.auctionBaseUrl);
  }

  deleteAuctionEmd(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.auctionsEmd + id + '/', environment.auctionBaseUrl);
  }

  getAuctionDetail(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions + id + '/', environment.auctionBaseUrl);
  }

  getAuctionContactDetail(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionsContact, environment.auctionBaseUrl, payload);
  }

  getBrandFilterById(id) {
    return this.httpClientService.HttpGetRequestCommon('auction/' + id + '/' + this.apiPath.auctionBrandFilter, environment.auctionBaseUrl);
  }

  getCategoryFilterById(id) {
    return this.httpClientService.HttpGetRequestCommon('auction/' + id + '/' + this.apiPath.auctionCategoryFilter, environment.auctionBaseUrl);
  }

  getLocationFilterById(id) {
    return this.httpClientService.HttpGetRequestCommon('auction/' + id + '/' + this.apiPath.auctionLocationFilter, environment.auctionBaseUrl);
  }

  getMfgYearFilterById(id) {
    return this.httpClientService.HttpGetRequestCommon('auction/' + id + '/' + this.apiPath.auctionMfgYearFilter, environment.auctionBaseUrl);
  }

  getLotsDetailByAuctionId(queryParam: any, id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctions + id + '/lots/' + (queryParam ? '?' + queryParam : ''), environment.auctionBaseUrl);
  }

  // getCustomerList(payload){
  //   return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity, environment.partnerBaseUrl, payload); 
  // }

  getAuctionEmdList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionsEmd, environment.auctionBaseUrl, payload);
  }

  getAuctionEmdListById(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionsEmd + id, environment.auctionBaseUrl);
  }

  deleteAuctionLot(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.auctionsLot + id + '/', environment.auctionBaseUrl);
  }

  getAuctionAssetsList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionLotAssets, environment.auctionBaseUrl, payload);
  }

  deleteAuctionAssetsLot(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.auctionLotAssets + id + '/', environment.auctionBaseUrl);
  }

  getCustomerList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity, environment.partnerBaseUrl, payload);
  }

  getDescription(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.buySellMasterPaymentDescription, environment.paymentBaseUrl, payload);
  }

  getEmdValues(id, payload?) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionEmd + 'by-auction/' + id + '/', environment.auctionBaseUrl, payload);
  }

  getAuctionPendingPayment(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionLotBids + id + '/amount-to-pay/', environment.auctionBaseUrl);
  }

  validateAuctionPayment(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionEmd + 'has-paid/', environment.auctionBaseUrl, payload);
  }

  getAuctionEsign(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.eSignAuctions, environment.auctionBaseUrl, payload);
  }

  postAuctionEsign(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.eSignAuctions, environment.auctionBaseUrl);
  }

  postAuctionEsignStatus(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.eSignStatus, environment.auctionBaseUrl);
  }

  eSignCaptureWidget(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.eSignCaptureWidget, environment.auctionBaseUrl);
  }

  patchAuctionEsign(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.eSignAuctions + id + '/', environment.auctionBaseUrl);
  }

  deleteESignData(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.eSignAuctions + id + '/', environment.auctionBaseUrl);
  }

  generateLOA(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.generateLOA, environment.auctionBaseUrl);
  }

  getfulfilmentPayments(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.fulfilment, environment.auctionBaseUrl, payload);
  }

  getfulfilmentPaymentsByID(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.fulfilment + id, environment.auctionBaseUrl);
  }

  getfulfilmentPaymentDetails(id) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.fulfilment + id + '/payment-details/', environment.auctionBaseUrl);
  }

  getAuctionAllfulfilmentPaymentDetails(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctionFulfillmentPayments, environment.auctionBaseUrl, payload);
  }

  uploadBulkFulfilment(payload) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.uploadFulfilment, environment.auctionBaseUrl);
  }

  updateBulkFulfilment(payload) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.updateFulfilment, environment.auctionBaseUrl);
  }

  UpdateFulfilmentPatchReq(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.fulfilment + id + '/', environment.auctionBaseUrl);
  }

  bulkUpdateReviseDate(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.bulkUpdateReviseDate, environment.auctionBaseUrl);
  }

  rejectAuctionFulfilment(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.auctionLotBids + id + '/', environment.auctionBaseUrl);
  }

  bidForAuction(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.viewOnlineAuction, environment.auctionBaseUrl, payload);
  }

  emdExistCheck(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.emdExistCheck, environment.auctionBaseUrl, payload);
  }

  auctionDropdown(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.auctiondropdown, environment.auctionBaseUrl, payload);
  }

  deleteAuctionRegistration(id) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.deleteAuctionRegistration + id + '/delete-auction-registration/', environment.paymentBaseUrl);
  }

  rejectFulfilment(payload, id) {
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.rejectFulfilment + id + '/reject/', environment.auctionBaseUrl);
  }

  UploadIssueDO(payload, id) {
    return this.httpClientService.HttpPatchRequestCommon(payload, this.apiPath.fulfilment + id + '/issue_delivery_order/', environment.auctionBaseUrl);
  }

}


