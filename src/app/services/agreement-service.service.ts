import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class AgreementServiceService {

  constructor(public httpservice: HttpClientService, private apiPath: ApiRouteService) { }

  postAgreementDetails(payload: any) {
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerAgreement, environment.partnerBaseUrl)
  }
  putAgreementDetails(payload: any) {
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerAgreement + payload.id + '/', environment.partnerBaseUrl)
  }
  getPartnerEntityparticular(cognito: any, parterType: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerEntity + '?partner_admin__cognito_id__in=' + cognito + '&partnership_type__in=' + parterType, environment.partnerBaseUrl)
  }
  getPartnerEntityTaxparticular(cognito: any, parterType: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerTaxDetails + '?partner__partner_admin__cognito_id__in=' + cognito + '&partner__partnership_type__in=' + parterType, environment.partnerBaseUrl)
  }
  /**get particular user adress on partner management */
  getPartnerEntityAddressparticular(cognito: any, parterType: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerCorporateAddress + '?partner__partner_admin__cognito_id__in=' + cognito + '&partner__partnership_type__in=' + parterType, environment.partnerBaseUrl)
  }
  /**get particular Brand location on partner management */
  getPartnerEntityBrandparticular(cognito: any, payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerBrandLocationDetails + '?partner__partner_admin__cognito_id__in=' + cognito + '&partner__partnership_type__in=' + payload, environment.partnerBaseUrl)
  }
  getPartnerEntityIdentityparticular(payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerDealerIdentityProof + payload + '/', environment.partnerBaseUrl)
  }
  deleteAgreementDetails(payload: any) {
    return this.httpservice.HttpDeleteRequestCommon(this.apiPath.partnerAgreement + payload.id + '/', environment.partnerBaseUrl)
  }
  getAgreemenTemplateUrl() {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerAgreementTemplate, environment.mastersBaseUrl)
  }
  getAgreementDetails(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.partnerAgreement}?${queryParams}`, environment.partnerBaseUrl)
  }
  getSellerList(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.sellerAndCustomerList}?${queryParams}`, environment.partnerBaseUrl)
  }
  getAgreementDetailsById(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.partnerAgreementFilter}${queryParams}`, environment.partnerBaseUrl)
  }
  getAgreementBankDetailsById(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.partnerAgreementBankDetails}${queryParams}`, environment.partnerBaseUrl)
  }
  postPartnerEntityMapping(payload: any) {
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerEntityMapping, environment.partnerBaseUrl)
  }
  // putPartnerEntityMapping(payload: any) {
  //   return this.httpservice.HttpPutRequestCommon(payload, this.apiPath.partnerEntityMapping, environment.partnerBaseUrl)
  // }
  putPartnerEntityMappingPartnerFlow(payload: any) {
    return this.httpservice.HttpPutRequestCommon(payload, this.apiPath.partnerEntityMapping + "/" + payload.id + "/", environment.partnerBaseUrl)
  }
  patchPartnerEntityMappingPartnerFlow(payload: any) {
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerEntityMapping + payload.id + "/", environment.partnerBaseUrl)
  }
  postDealerEntityDetails(payload: any) {  //register a dealer entity
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerEntity, environment.partnerBaseUrl)
  }

  putDealerEntityDetails(payload: any, id: any) {  //register a dealer entity
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerEntity + id + '/', environment.partnerBaseUrl)
  }
  postDealerIdentityProofDetails(payload: any) {  //register a dealer identity proof
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerIdentityProofBatch, environment.partnerBaseUrl)
  }
  postAdminIdentityProofDetails(payload: any) {  //register a dealer identity proof
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerAdminIdentityProof, environment.partnerBaseUrl)
  }
  putDealerIdentityProofDetails(payload: any, id: any) {  //register a dealer identity proof
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerIdentityProofBatch + id + '/', environment.partnerBaseUrl)
  }
  postDealerTaxDetails(payload: any) {  //register a dealer identity proof
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerTaxDetails, environment.partnerBaseUrl)
  }
  putDealerTaxDetails(payload: any, id: any) {  //register a dealer entity
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerTaxDetails + id + '/', environment.partnerBaseUrl)
  }
  /**get all states */
  getStateMaster(queryParams: string = '') {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.stateMaster}${queryParams ? '?' + queryParams : ''}`, environment.mastersBaseUrl);
  }
  // putPartnerEntityMapping(payload: any) {
  //   return this.httpservice.HttpPutRequestCommon(payload, this.apiPath.partnerEntityMapping + payload.partner + '/', environment.partnerBaseUrl)
  // }
  deletePartnerEntityMapping(payload: any) {
    return this.httpservice.HttpDeleteRequestCommon(this.apiPath.partnerEntityMapping + payload.partner_entity + '/', environment.partnerBaseUrl)
  }
  getPartnerEntityMapping(payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerEntityMapping + payload.partner_entity + '/', environment.partnerBaseUrl)
  }

  postDealerAddressDetails(payload: any) {
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerCorporateAddress, environment.partnerBaseUrl)
  }
  updateDealerAddressDetails(payload: any, id: any) {  //register a dealer entity
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerCorporateAddress + id + '/', environment.partnerBaseUrl)
  }
  updateBrandLocations(payload: any, id: any) {  //register a dealer entity
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.partnerBrandLocationDetails + id + '/', environment.partnerBaseUrl)
  }

  deleteBrandLocations(id: any) {
    return this.httpservice.HttpDeleteRequestCommon(this.apiPath.partnerBrandLocationDetails + id + '/', environment.partnerBaseUrl)
  }

  getPinCode(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.masterPincode}?${queryParams}`, environment.mastersBaseUrl);
  }

  getBrandMaster(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.brandMaster}?${queryParams}`, environment.mastersBaseUrl);
  }

  getCity(queryParams: string) {
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.partnerCity}?${queryParams}`, environment.mastersBaseUrl);
  }

  getMasterCity(payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerCity + payload + '/', environment.mastersBaseUrl)
  }

  getStateMasterId(payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.stateMaster+ payload + '/', environment.mastersBaseUrl);
  }
  getCityMasterId(payload: any) {
    return this.httpservice.HttpGetRequestCommon(this.apiPath.partnerCity+ payload + '/', environment.mastersBaseUrl);
  }
  postDealerbrandLocationDetails(payload: any) {
    return this.httpservice.HttpPostRequestCommon(payload, this.apiPath.partnerBrandLocationDetails, environment.partnerBaseUrl)
  }
   postUpdateDealerbrandLocation(payload: any) {
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.brandLocationPatch, environment.partnerBaseUrl)
  }
  saveUpdateDealerTaxDetails(payload: any) {  //register a dealer entity
    return this.httpservice.HttpPatchRequestCommon(payload, this.apiPath.saveUpdatePartnerTax, environment.partnerBaseUrl)
  }
}
