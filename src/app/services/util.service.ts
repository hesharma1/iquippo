import { Injectable } from '@angular/core';
import { HttpClientService } from '../utility/http-client.service';
import { ApiRouteService } from '../utility/app.refrence';
import { environment } from 'src/environments/environment';
import { AwsAuthService } from '../auhtentication/aws-auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';

@Injectable({
providedIn: 'root'
})
@Injectable({
providedIn: 'root'
})
export class UtilService {

constructor(private httpClientService: HttpClientService, public aws: AwsAuthService, public http: HttpClient) { }

public post(url: string, params?: any) {
  return new Promise((resolved, reject) => {
    this.checkAuthToken().then(token => {
      if (token) {
        // const headers = new HttpHeaders({
        //   'content-type': 'application/json',       
        //   'userId': localStorage.getItem('username') || '',
        //   'sessionId': localStorage.getItem('sessionId') || '',
        //   'correlationId': localStorage.getItem('ApplicationNumber') != undefined ? localStorage.getItem('ApplicationNumber') || '': localStorage.getItem('sessionId') || '',
        //   'channel': 'Mobile',
        //   'Access-Control-Allow-Origin': '*',
        //   'Access-Control-Allow-Methods': 'POST,OPTIONS',
        //   'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,access-control-allow-methods,access-control-allow-headers,access-control-allow-origin,userid,sessionid,correlationid,channel'
        // });

        let options = {
          // headers: headers
        }

        this.http.post(url, params, options).pipe(
          timeout(60000)
          ).subscribe((data:any) => {
          resolved(data);
        }, (err:any) => {
          // console.error(err.status);
          reject(err)
        })
      } else {
        //logout user
        let er = {
          status: 100,
          statusMsg: 'Session timeout.'
        }
        reject(er)
      }
    }).catch(e => {
      reject(e)
    })

  });
}

public get(url: string) {
  return Observable.create((observer: Observer<any>) => {
    this.checkAuthToken().then(token => {
      if (token) {
        // const headers = new HttpHeaders({
        //   'content-type': 'application/json',          
        //   'userId': localStorage.getItem('username') || '',
        //   'sessionId': localStorage.getItem('sessionId') || '',
        //   'correlationId': localStorage.getItem('ApplicationNumber') != undefined ? localStorage.getItem('ApplicationNumber') || '' : localStorage.getItem('sessionId') || '',
        //   'channel': 'Mobile',
        //   'Access-Control-Allow-Origin': '*',
        //   'Access-Control-Allow-Methods': 'GET,OPTIONS',
        //   'Access-Control-Allow-Headers': 'Content-Type'
        // });
        let options = {
          // headers: headers
        }
        this.http.get(url, options).
        pipe(timeout(60000)).subscribe(res => {
          observer.next(res)
          observer.complete();

        }, err => {
          observer.error(err)
        });
      } else {
        //logout
        let er = {
          status: 100,
          statusMsg: 'Session timeout.'
        }
        observer.error(er)

      }
    }).catch(e => {
      observer.error(e)

    })
  });

}
public delete(url: string) {
  return new Promise((resolved, reject) => {
    this.checkAuthToken().then(token => {
      // if (token) {
      //   const headers = new HttpHeaders({
      //     'content-type': 'application/json',
      //     'userId': localStorage.getItem('username') || '',
      //     'sessionId': localStorage.getItem('sessionId') || '',
      //     'correlationId': localStorage.getItem('ApplicationNumber') != undefined ? localStorage.getItem('ApplicationNumber') || '' : localStorage.getItem('sessionId') || '',
      //     'channel': 'Mobile',
      //     'Access-Control-Allow-Origin': '*',
      //     'Access-Control-Allow-Methods': 'DELETE,OPTIONS',
      //     'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,access-control-allow-methods,access-control-allow-headers,access-control-allow-origin,userid,sessionid,correlationid,channel'
      //   });
      //   let options = {
      //     headers: headers
      //   }
      //   return this.http.delete(url, options).pipe(timeout(60000)).subscribe(res => {
      //     // observer.next(res)
      //     resolved(res)

      //   }, err => {
      //     reject(err)
      //     // observer.error(err)
      //   });
      // } else {
      //   //logout
      //   let er = {
      //     status: 100,
      //     statusMsg: 'Session timeout.'
      //   }
      //   // observer.error(er)
      //   reject(er)
      // }
      if (token) {
        const headers = new HttpHeaders({
          'content-type': 'application/json',
          'userId': localStorage.getItem('username') || '',
          'sessionId': localStorage.getItem('sessionId') || '',
          'correlationId': localStorage.getItem('ApplicationNumber') != undefined ? localStorage.getItem('ApplicationNumber') || '' : localStorage.getItem('sessionId') || '',
          'channel': 'Mobile',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'PUT,OPTIONS',
          'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,access-control-allow-methods,access-control-allow-headers,access-control-allow-origin,userid,sessionid,correlationid,channel'
        });
        let options = {
          headers: headers
        }
        this.http.delete(url,options).pipe(timeout(60000)).subscribe(data => {
          resolved(data);
        }, err => {
          //console.error(err);
          reject(err)
        })
      } else {
        //logout
        let er = {
          status: 100,
          statusMsg: 'Session timeout.'
        }
        reject(er)
      }
    }).catch(e => {
      reject(e)
      // observer.error(e)
    })
  });
}



public put(url: string, params?: any) {
  return new Promise((resolved, reject) => {
    this.checkAuthToken().then(token => {
      if (token) {
        const headers = new HttpHeaders({
          'content-type': 'application/json',
          'userId': localStorage.getItem('username') || '',
          'sessionId': localStorage.getItem('sessionId') || '',
          'correlationId': localStorage.getItem('ApplicationNumber') != undefined ? localStorage.getItem('ApplicationNumber') || '' : localStorage.getItem('sessionId') || '',
          'channel': 'Mobile',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'PUT,OPTIONS',
          'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,access-control-allow-methods,access-control-allow-headers,access-control-allow-origin,userid,sessionid,correlationid,channel'
        });
        let options = {
          headers: headers
        }
        this.http.put(url, params, options).pipe(timeout(60000)).subscribe(data => {
          resolved(data);
        }, err => {
          //console.error(err);
          reject(err)
        })
      } else {
        //logout
        let er = {
          status: 100,
          statusMsg: 'Session timeout.'
        }
        reject(er)
      }
    }).catch(e => {
      reject(e)
    })

  });
}

checkAuthToken() {
    return new Promise((resolved, reject) => {
      const token = localStorage.getItem('sessionId');
      let time = localStorage.getItem('sessionTime');
      let sessionExpTime: any = time != null ? parseInt(JSON.parse(time)) : null;
      let date = new Date();
      let currentTimestamp = date.getTime();

      if ((sessionExpTime * 1000) > currentTimestamp) {
        resolved(true)
        //  console.log('testing')
      }
       else {

        if ((currentTimestamp - (sessionExpTime * 1000)) < (60 * 60 * 1 * 1000)) {
          //console.log();
          this.aws.refreshToken(resolved, reject)
        } 
        else {
          //logout
          resolved(false)
        }

      }
    });
  }
}