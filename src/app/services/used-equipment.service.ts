import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../utility/app.refrence';
import { HttpClientService } from '../utility/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class UsedEquipmentService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) { }

  getAllProducts(queryParams: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.usedProductList}?${queryParams}`, environment.productBaseUrl);
  }

  getBasicDetailList(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + id, environment.productBaseUrl);
  }

  getBasicDetailListByCognito(id: any,cognitoId) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + id + (cognitoId?'?cognito_id='+cognitoId:''), environment.productBaseUrl);
  }
  
  // getBasicDetailList(id: any) {
  //   let url = "https://97d9714effd4.ngrok.io/";
  //   return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList + id, url);
  // }

  getVisuals(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductVisualData + id, environment.productBaseUrl);
  }

  getVisualsByEquipmentId(id: any, queryParam?:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.oldEquipmentVisualsByEquimentId + id, environment.productBaseUrl, queryParam);
  }

  getTempVisualsByEquipmentId(id: any, queryParam?:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.oldTempEquipmentVisualsByEquimentId + id, environment.productBaseUrl, queryParam);
  }

  getDocumentsByEquipmentId(id: any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.oldEquipmentDocumentsByEquimentId + id, environment.productBaseUrl);
  }

  getvisualTabs() {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.visualTabsList, environment.mastersBaseUrl);
  }

  getDocumentList(id: number) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.getDocumentsForUsedProduct + id, environment.productBaseUrl);
  }

  postUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.usedProductList, environment.productBaseUrl);
  }

  postupdateUsedEquipment(requestModel: any, id: number) {
    return this.httpClientService.HttpPutRequestCommon(requestModel, this.apiPath.usedProductList + id + '/', environment.productBaseUrl);
  }

  postvisualUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.usedProductVisualData, environment.productBaseUrl);
  }

  patchvisualTempUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPatchRequestCommon(requestModel, this.apiPath.usedProductVisualTempData, environment.productBaseUrl);
  }
  postvisualTempUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.usedProductVisualTempData, environment.productBaseUrl);
  }

  setPrimaryVisualUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.productMakeVisualPrimary, environment.productBaseUrl);
  }

  deleteVisual(id:number) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.usedProductVisualData + id + '/', environment.productBaseUrl);
  }

  deleteTempVisual(id:number) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.usedProductVisualTempData + id + '/', environment.productBaseUrl);
  }

  postdocumentUploadUsedEquipment(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(requestModel, this.apiPath.usedProductDocumentData, environment.productBaseUrl);
  }

  deletedocumentUploadUsedEquipment(id: any) {
    return this.httpClientService.HttpDeleteRequestCommon(this.apiPath.usedProductDocumentData+id+ '/', environment.productBaseUrl);
  }

  getProduct(id: number) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.usedProductList}${id}`, environment.productBaseUrl);
  }

  getProductBidInstaSale(id:any) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.tradeBid+'?equipment='+id, environment.tradeBaseUrl);
  }

  getSellerList(queryParams: any){
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.sellerAndCustomerList}${queryParams ? '?' + queryParams : ''}`, environment.partnerBaseUrl)
  }

  getPartnerSellerList(payload: any){
    return this.httpClientService.HttpPostRequestCommon(payload,this.apiPath.partnerProfile, environment.partnerBaseUrl)
  }

  getDocUploadedUsedEquipmetById(id: number){
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.usedProductVisualDataById}${id}`, environment.productBaseUrl)
  }

  validateBid(payload){
    return this.httpClientService.HttpPostRequestCommon(payload, this.apiPath.validateBid, environment.productBaseUrl);
  }

  getRecentlyViewed(cognitoId){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.recentlyViewed + '?user__cognito_id__iexact='+cognitoId + '&type__in=1&used_equipment__isnull=false', environment.partnerBaseUrl);

  }

  getRM(){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerContactEntity + '?partnership_type__in=3&channel_partner_type__title__in=Relationship Manager,IDP,IEP' + '&limit=999', environment.partnerBaseUrl);
  }

  getUsedEquipment(request) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.usedProductList, environment.productBaseUrl,request);
  }

  getInvoiceCustomer(payload){
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.partnerDealerBasicProfile, environment.partnerBaseUrl, payload);
  }
  
}
