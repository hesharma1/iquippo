import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DisclaimerModalComponent } from '../../shared/disclaimer-modal/disclaimer-modal.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public subscribeForm = new FormGroup({
    email : new FormControl('',[Validators.email,Validators.required])
  })
  request: any;
  constructor(public router: Router,private commonService:CommonService, public notify: NotificationService, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  subscribe(){
    if(this.subscribeForm.valid){
      this.request = new Object();
        this.request.email = this.subscribeForm.get('email')?.value;
      this.commonService.subscribeToNewsletter(this.request).subscribe(res=>{
        this.subscribeForm.reset();
        this.notify.success('Thank you for subscribing!!');
        return;
      });
    }else{
      this.notify.error('Please enter a valid email!!');
    }
  }

  openDisclaimer(){
    const dialogRef = this.dialog.open(DisclaimerModalComponent, {
      width: '400px',
      disableClose: true
    });    
  }

}
