import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, Renderer2, HostListener } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, RouterStateSnapshot, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { environment } from 'src/environments/environment';
import { MakeACallComponent } from '../../make-a-call/make-a-call.component';
import { DashboardService } from 'src/app/services/dashboard-service';
import { BehaviorSubject, Observable, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { SignInProp } from '../../signin/signin-prop.service.component';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('authMenuButton') authMenuButton!: ElementRef;
  @ViewChild('authMenu') authMenu!: ElementRef;
  @ViewChild('notifyButton') notifyButton!: ElementRef;
  @ViewChild('notifySection') notifySection!: ElementRef;

  timedOutCloser;
  activePage = 1;
  sidebartgl: boolean = true;
  wrappertgl: boolean = true;
  isLoggedIn: boolean = false;
  userFirstName?: string;
  userLastName?: string;
  userdata: any;
  mp1HomePage?: string;
  UserID: string = "";
  LoginTime: number = 0;
  destroy = new Subject();
  _oUser: SignInProp = new SignInProp();
  //private _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp);
  rxjsTimer = timer(1000, 1000);
  timer: number = 0;
  _Login: any;
  type = '1';
  homeForm;
  categoryId: any;
  brandId: any;
  modalId: any;
  autoSuggestList;
  cognitoId;
  rolesArray;
  financePage: string = "";
  showValuation: boolean = false;
  showEquipment: boolean = false;
  selectedDropdown = '2';
  showSignIn = false;
  modalName = '';
  categoryName = '';
  brandName = '';
  count = 0;
  showNotifications = false;
  notifications: Array<any> = [];
  total_notification = 0;
  notificationAPi = false;
  showScrollIcon: boolean = false;
  selectedType = 'new';
  constructor(public awsCognito: AwsAuthService, public router: Router, public route:ActivatedRoute, public sharedService: SharedService, private storage: StorageDataService, public appRouteEnum: AppRouteEnum, private dialog: MatDialog, public fb: FormBuilder, private dashboardService: DashboardService, private cd: ChangeDetectorRef, private commonService: CommonService, private renderer: Renderer2) {

    this.renderer.listen('window', 'click', (e: Event) => {
      if (this.authMenu && this.authMenu.nativeElement) {
        if ((!this.authMenu.nativeElement.contains(e.target)) && (!this.authMenuButton.nativeElement.contains(e.target))) {
          this.showSignIn = false;
        }
      }

      if (this.notifySection && this.notifySection.nativeElement) {
        if ((!this.notifySection.nativeElement.contains(e.target)) && (!this.notifyButton.nativeElement.contains(e.target))) {
          this.showNotifications = false;
        }
      }
    });

    sharedService.getLoggedInName.subscribe((isLogin: boolean) => {
      this.ChkSessionTimeOut();
      if (isLogin) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
    this.mp1HomePage = environment.mp1HomePage;
    this.financePage = environment.financePage;
    router.events.subscribe(val => {
      let value: any = val;
      let route: any;
      if (value?.url != "") {
        route = value?.url;
      }
      if (route) {
        if (route.includes('used-equipment-dashboard') || route.includes('used-equipment')) {
          this.selectedDropdown = '2';
          this.type = '2'
          this.selectedType = 'used'
        } else {
          this.selectedDropdown = '1';
          this.type = '1'
          this.selectedType = 'new'
        }
      }

    });
  }

  ngOnInit(): void {
    this.homeForm = this.fb.group({
      searchAll: ['']
    })
    this.userdata = this.storage.getLocalStorageData("userData", true);
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    if (this.cognitoId) {
      this.getNotificationsCount();
    }
    const interval = setInterval(() => {
      this.cognitoId = this.storage.getStorageData('cognitoId', false);
      if (this.cognitoId) {
        this.getNotificationsCount();
      }
    }, 30000);
  }

  getNotificationsCount() {
    let payload = {
      "user__cognito_id__in": this.cognitoId,
      "message_new": true
    }
    this.dashboardService.getNotificationsCount('', payload).subscribe((res: any) => {
      this.count = res?.count;
    })
  }

  getdropdownVal() {
    if (this.router.url.includes('used-equipment-dashboard') || this.router.url.includes('used-equipment')) {
      this.selectedDropdown = '2';
      return '2';
    } else {
      this.selectedDropdown = '1';
      return '1';
    }
  }

  redirecttologin() {
    this.showSignIn = false;
    this.router.navigate([`./` + this.appRouteEnum.Login]);
  }

  ChkSessionTimeOut() {
    this._Login = this.rxjsTimer
      .pipe(takeUntil(this.destroy))
      .subscribe(val => {
        this.timer = val;

        var UserIDww = this.sharedService._oUserSubject.getValue().UserID;
        //.getValue().UserID;
        this.LoginTime = this.sharedService._oUserSubject.getValue().LoginTime;

        if (this.timer >= environment.ExpiryTimeInSeconds) {
          let o: SignInProp = new SignInProp();
          o.UserID = "";
          o.LoginTime = 0;
          this.sharedService._oUserSubject.next(o);
          this.sharedService._oUserSubject.complete();

          this.UserID = this.sharedService._oUserSubject.getValue() != null ? this.sharedService._oUserSubject.getValue().UserID : "";

          this.timer = 0;

          this.logout();
        }
      });

  }

  logout() {
    this.showSignIn = false;
    this.sharedService.logout();
  }

  sidebartoggle() {
    this.sidebartgl = !this.sidebartgl;
    this.wrappertgl = !this.wrappertgl;
  }

  backTohome() {
    this.sharedService.redirecteToMP1AfterLogin();
  }

  getUserData() {
    this.userdata = this.storage.getLocalStorageData("userData", true);
    return this.userdata;
  }

  /**get user role */

  getUserRoles() {
    this.showSignIn = false;
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
      this.storage.setStorageData("UserInfo", res, true);
      this.router.navigate(["/mydashboard"]);
    })
  }

  opencallpopup() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    const dialogRef = this.dialog.open(MakeACallComponent, dialogConfig);
  }

  switchValue(value) {
    this.type = value;
    if(value == '1'){
      this.selectedType =  'new';
    }else{
      this.selectedType = 'used';
    }
    this.autoSuggestList = [];
    this.categoryId = '';
    this.categoryName = '';
    this.brandId ='';
    this.brandName ='';
    this.modalId = '';
    this.modalName = '';
    this.homeForm.get('searchAll')?.setValue('');
  }

  search() {
    let string = '';
    if (this.categoryId) {
      string = string + 'category=' + this.categoryId + '&categoryName=' + this.categoryName;
    }
    if (this.brandId) {
      string = string + '&brand=' + this.brandId + '&brandName=' + this.brandName;
    }
    if (this.modalId) {
      string = string + '&model=' + this.modalId + '&modelName=' + this.modalName;
    }
    if (string) {
      if (this.type == '1') {
        this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: string } }).then(() => {
          window.location.reload();
        });
        this.cd.detectChanges();
      } else {
        this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { filterBy: string } }).then(() => {
          window.location.reload();
        });
        this.cd.detectChanges();
      }
    } else {
      let value = this.homeForm.get('searchAll')?.value;
      if (this.type == '1') {
        this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { search: value } }).then(() => {
          window.location.reload();
        });;
      } else {
        this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { search: value } }).then(() => {
          window.location.reload();
        });;
      }
    }
  }

  searchByKeyWord(keyword) {
    this.dashboardService.searchByKeyword(keyword, this.selectedType).subscribe((res: any) => {
      this.autoSuggestList = res;
    })
  }

  onautoSelect(id, type, name) {
    if (type == 'category') {
      this.categoryId = id;
      this.categoryName = name;
    } else if (type == 'brand') {
      this.brandId = id;
      this.brandName = name;
    } else if (type == 'model') {
      this.modalId = id;
      this.modalName = name;
    }
  }

  getRolesArray() {
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    if (this.rolesArray)
      this.rolesArray = this.rolesArray.split(',');
  }

  checkUserRole() {
    this.getRolesArray();
    if (this.rolesArray) {
      if (this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer')) {
        return true;
      }
      return false;
    }
    return false;
  }

  mouseEnter(trigger) {
    if (this.timedOutCloser) {
      clearTimeout(this.timedOutCloser);
    }
    trigger.openMenu();
  }

  mouseLeave(trigger) {
    this.timedOutCloser = setTimeout(() => {
      trigger.closeMenu();
    }, 50);
  }

  // showSignInFn() {
  //   if (this.showSignIn) {
  //     this.showSignIn = false;
  //   } else {
  //     setTimeout(() => {
  //       this.showSignIn = true;
  //     });
  //   }
  // }

  timeStampFilter = (timeStamp, type) => {
    switch (type) {
      case "date": { return this.filterDate(timeStamp); }
      case "time": { return this.filterTime(timeStamp); }
      default: { return '' };
    }
    return ''
  };

  filterDate = (timeStamp) => {
    let date = (timeStamp + "").split("T")[0];
    var months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    var d = new Date(date);
    var day = "" + d.getDate();
    if (day.length < 2) day = "0" + day;
    var year = d.getFullYear();
    return day + " " + months[d.getMonth()] + ", " + year;
  }

  filterTime = (timeStamp) => {
    var d = new Date(timeStamp),
      hour = "" + d.getHours(),
      min = "" + d.getMinutes();
    if (hour.length < 2) hour = "0" + hour;
    if (min.length < 2) min = "0" + min;
    return hour + ":" + min + ((Number(hour) >= 12) ? " PM" : " AM");
    ;
  }

  getNotifications() {
    if (this.showNotifications && this.cognitoId) {
      let payload = {
        "user__cognito_id__in": this.cognitoId,
        "page": this.activePage,
        "limit": 10,
        "message_seen": false,
        "ordering": "-id"
      }
      this.notificationAPi = false;
      this.dashboardService.getNotificationsCount('', payload).subscribe((res: any) => {
        if (this.activePage == 1) {
          this.notifications = [];
        }
        this.notificationAPi = true;
        this.total_notification = res?.count;
        if (!this.notifications)
          this.notifications = res?.results;
        else {
          this.notifications.push(...res?.results);
        }
        this.markAllAsRead();
      })
    }
  }

  clearAllNotifications() {
    if (this.cognitoId) {
      let payload = {
        "user": this.cognitoId,
        "message_seen": true
      }
      this.dashboardService.updateNotificationStatus('', payload).subscribe((res: any) => {
        console.log(res);
      })
    }
  }

  markAllAsRead() {
    if (this.cognitoId) {
      let payload = {
        "user": this.cognitoId,
        "message_new": false
      }
      this.dashboardService.updateNotificationStatus('', payload).subscribe((res: any) => {
        this.getNotificationsCount();
      })
    }
  }

  markNotificationAsRead(id, link) {
    if (this.cognitoId) {
      let payload = {
        "user": this.cognitoId,
        "message_read ": true
      }
      this.dashboardService.updateNotification(id, payload).subscribe((res: any) => {
        if (link) {
          this.router.navigate([link]);
          return;
        }
      })
    }
  }

  @HostListener('scroll', ['$event'])
  recursiveNotificationHit = (event: any) => {
    if (!this.notifications || this.total_notification <= this.notifications?.length)
      return;

    if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
      this.activePage = (Number(this.activePage) + 1);
      this.getNotifications();
    }
  }

  @HostListener("window:scroll")
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    if ((max / pos) * 100 < 90) {
      this.showScrollIcon = true;
    } else {
      this.showScrollIcon = false;
    }
  }

  scrollToTop() {
    window.scrollTo(0, 0);
  }

  getClass() {
    if (this.router.routerState.snapshot.url.includes('/new-equipment-dashboard') || this.router.routerState.snapshot.url.includes('/used-equipment-dashboard') || this.router.routerState.snapshot.url.includes('/new-equipment/sell-equipment') || this.router.routerState.snapshot.url.includes('/used-equipment/sell-equipment')) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    this.showSignIn = false;
  }
}