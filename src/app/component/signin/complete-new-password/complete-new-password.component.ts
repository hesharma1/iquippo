import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { dataBound } from '@syncfusion/ej2-angular-grids';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { CheckPasswordMatch } from 'src/app/utility/custom-validators/forgot-password';
@Component({
  selector: 'app-complete-new-password',
  templateUrl: './complete-new-password.component.html',
  styleUrls: ['./complete-new-password.component.css']
})
export class CompleteNewPasswordComponent implements OnInit {
  description: string = "";
  cognitoId: string = "";

  constructor(public dialogRef: MatDialogRef<CompleteNewPasswordComponent>, @Inject(MAT_DIALOG_DATA) data: any, private router: Router
    , private appRouteEnum: AppRouteEnum, private formbulider: FormBuilder) {
  }
  newUserForm: any = this.formbulider.group({
    Password: new FormControl('', [Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/)]),

    ConfirmPassword: new FormControl('', [Validators.required]),
  },
    {
      validator: [CheckPasswordMatch('Password', 'ConfirmPassword')]
    });
  fieldTextType: boolean = false;
  fieldTextType1: boolean = false;
  ngOnInit(): void {

  }
  okClick() {
    this.dialogRef.close(this.newUserForm.get('Password')?.value);
  }
  // backClick(){
  //   this.dialogRef.close();
  // }
}
