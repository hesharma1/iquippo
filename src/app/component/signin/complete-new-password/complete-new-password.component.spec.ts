import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteNewPasswordComponent } from './complete-new-password.component';




describe('CompleteNewPasswordComponent', () => {
  let component: CompleteNewPasswordComponent;
  let fixture: ComponentFixture<CompleteNewPasswordComponent>;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompleteNewPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteNewPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
