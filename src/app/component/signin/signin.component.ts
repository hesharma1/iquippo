import { Component, OnInit, ViewChild, ElementRef, Inject, } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormsModule
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debug } from 'console';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { SocialAuthService } from 'angularx-social-login';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
} from 'angularx-social-login';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AccountService } from 'src/app/services/account';
import { VerifyUser, VerifyUserResponse } from 'src/app/models/verifyUserRequest.model';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from 'src/app/services/shared-service.service';
import { environment } from 'src/environments/environment';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CompleteNewPasswordComponent } from './complete-new-password/complete-new-password.component';
import { Auth } from 'aws-amplify';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { CookieService } from 'ngx-cookie-service';

import { BehaviorSubject, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { AssetvaluatedComponent } from '../valuation/assetvaluated/assetvaluated.component';
// import { SignInProp } from '../signin/signin-prop.service.component';

//import { myFunction} from 'src/assets/js/focusonnext.js'
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
})
export class SigninComponent implements OnInit {

  //private _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp());  

  private CookieValue: string = '';
  otpSection?: boolean = true;
  isSubmitted: boolean = false;
  OtpForm: boolean = false;
  otpResponse: any;
  IsforgotPwdFormHidden: boolean = true;
  resendOTP: boolean = false;
  remainingTime: any;
  otpError?: boolean = false;
  fieldTextType: boolean = false;
  mobileInputBlur: boolean = false;
  countryList?: any;
  selectedCity?: CountryResponse;
  response: any;
  isProfile: boolean = false;
  maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  message?: string;
  googleResponse: any;
  returnUrl= '';
  @ViewChild('otpField2') otp2?: ElementRef;
  @ViewChild('otpField3') otp3?: ElementRef;
  @ViewChild('otpField4') otp4?: ElementRef;
  @ViewChild('otpField5') otp5?: ElementRef;
  @ViewChild('otpField6') otp6?: ElementRef;
  @ViewChild('otpField1') otp1?: ElementRef;
  partnerFlow: any;
  assetValuated: any;

  constructor(
    private cookieService: CookieService,
    private awsConfig: AwsAuthService,
    private fb: FormBuilder,
    private router: Router,
    private authService: SocialAuthService,
    private storage: StorageDataService,
    public accountService: AccountService,
    public httpClient: HttpClient,
    private sharedService: SharedService,
    private spinner: NgxSpinnerService,
    private notificationservice: NotificationService,
    private activatedRoute: ActivatedRoute, public appRouteEnum: AppRouteEnum, public dialog: MatDialog, public apiRouteService: ApiRouteService,
    public dialogRef: MatDialogRef<SigninComponent>,
        @Inject(MAT_DIALOG_DATA) data: any,
  ) {
    this.assetValuated = data.flag;
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
    });
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'];
    if(!this.returnUrl && this.router.url != '/login'){
      this.returnUrl = this.router.url;
    }
    // var isLoggedIn = this.sharedService.getCookie(this.appRouteEnum.globalCookieName);
    // if (isLoggedIn != null && isLoggedIn != '' && isLoggedIn != undefined && isLoggedIn == this.appRouteEnum.globalCookieValue) {
    //   window.location.href = environment.mp1HomePage;
    // }
    this.bindFlags();
    this.partnerFlow = this.sharedService.queryParams.partner;
    // let google_data=this.storage.getStorageData("google_data",true);
    // console.log(google_data);
    // if(google_data!=undefined){
    // this.otpSection =true;
    //}
    // if(this.submitOtp.value('otpField1').length == 1){
    // myFunction();
    // }
    //  this.notificationservice.success('hooray')
    //this.signinform.get('countrycode')?.setValue("+91");
     
  }

  signinform: FormGroup = this.fb.group({
    mobile: new FormControl('', [
      Validators.required,
      /*Validators.minLength(10),
      Validators.maxLength(10),*/
    ]),
    countrycode: new FormControl(''),
    password: new FormControl('', [
      Validators.required]),
  }, {
    validator: [numberonly('mobile')]
  }
  );

  submitOtp = this.fb.group({
    otpField1: new FormControl('', [Validators.required]),
    otpField2: new FormControl('', [Validators.required]),
    otpField3: new FormControl('', [Validators.required]),
    otpField4: new FormControl('', [Validators.required]),
    otpField5: new FormControl('', [Validators.required]),
    otpField6: new FormControl('', [Validators.required]),
    agree: new FormControl('', [Validators.required]),
  }, { validator: [numberonly('otpField1'), numberonly('otpField2'), numberonly('otpField3'), numberonly('otpField4'), numberonly('otpField5'), numberonly('otpField6')] }
  );

  checkPhoneField() {
    var prefixCode = this.signinform.get('countrycode')?.value.countryCode;
    var mobile = this.signinform.get('mobile')?.value;
    if (prefixCode != null && prefixCode == 'IN') {
      if (mobile.length >= 10 && mobile.length <= 14) {
        return true;
      }
      else {
        this.signinform.controls['mobile']?.setErrors({ 'incorrect': true });
        return false;
      }
    }
    else {
      return true;
    }
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.signinform.get('countrycode')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
    }, err => {
      this.notificationservice.error(err);
    })
  }
  // updateCountryCode(city:any, code:any){
  //   this.signinform.get('countrycode')?.setValue(city)
  //   this.signinform.get('mobile')?.setValue('')

  //   if(city == '+91' && code == 'IN'){
  //     this.maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  //   } else {
  //     this.maxLengthValue = this.appRouteEnum.otherPhoneNumber;
  //   }
  // }

  updateCountryCode(city: any) {
    this.signinform.get('countrycode')?.setValue(city);
    this.signinform.get('mobile')?.setValue('')

    if (city?.prefixCode == '+91' && city?.countryCode == 'IN') {
      this.maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
    } else {
      this.maxLengthValue = this.appRouteEnum.otherPhoneNumber;
    }
  }

  consoleitem(item: any) {
    //console.log(item);
  }

  otpvalidationfun() {
    if (this.submitOtp.get('otpField1')?.invalid ||
      this.submitOtp.get('otpField2')?.invalid ||
      this.submitOtp.get('otpField3')?.invalid ||
      this.submitOtp.get('otpField4')?.invalid ||
      this.submitOtp.get('otpField5')?.invalid ||
      this.submitOtp.get('otpField6')?.invalid) {
      this.otpError = true;

    }
    else if (this.submitOtp.get('otpField1')?.valid && this.submitOtp.get('otpField2')?.valid &&
      this.submitOtp.get('otpField3')?.valid && this.submitOtp.get('otpField4')?.valid &&
      this.submitOtp.get('otpField5')?.valid && this.submitOtp.get('otpField6')?.valid) {
      this.otpError = false;
    }
  }

  signup() {
    if(this.assetValuated){
      this.dialogRef.close(false);
    }
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
      var pro = this.sharedService.queryParams.aoc;
      var src = this.sharedService.queryParams.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: { src: this.sharedService.queryParams.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: {returnUrl:this.returnUrl}});
      }
    } else {
      this.router.navigate([`./` + this.appRouteEnum.Register], { queryParams: {returnUrl:this.returnUrl}});
    }
  }

  signin() {
    if(!this.signinform.valid){
      return;
    }
    //this.spinner.show();
    this.awsConfig
      .awsLoginWithPassword(
        this.signinform.get('countrycode')?.value.prefixCode + this.signinform.get('mobile')?.value,
        this.signinform.get('password')?.value,
      )
      .then((res) => {
        var isActive = res?.attributes["custom:status"];
        if(isActive!=undefined && isActive=="inactive")
        {
           
          this.notificationservice.error('You are not authorized to access the application. Please raise a callback request to get in touch with the iQuippo team.',true)
          this.sharedService.logoutInactive();
          
          return;
        }
        this.isSubmitted = true;
        this.response = res as any;
      
        if (this.response.challengeName === 'NEW_PASSWORD_REQUIRED') {
          this.message = "";
          const dialogRef = this.dialog.open(CompleteNewPasswordComponent, {
            width: '650px',
            disableClose: true
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              //this.spinner.show();
              this.message = "";
              Auth.completeNewPassword(
                res,               // the Cognito User Object
                result,       // the new password
                // OPTIONAL, the required attributes
                // {
                //   email: 'xxxx@example.com',
                //   phone_number: '1234567890'
                // }
              ).then(user => {
                // at this time the user is logged in if no MFA required
                this.message = "";
                console.log(user);
                var newRes = user as any;
                this.response = newRes;
                if (
                  this.response.signInUserSession.idToken.jwtToken != null &&
                  this.response.signInUserSession.idToken.jwtToken != ''
                ) {
                  // console.log(this.response);

                  // let obj: SignInProp = new SignInProp();
                  // obj.UserID = this.response;
                  // obj.LoginTime = 0;

                  // this.sharedService._oUserSubject = new BehaviorSubject<SignInProp>(obj);
                  // this.sharedService._oUserSubject.subscribe();

                  //this._oUserSubject = new BehaviorSubject<SignInProp>(obj);
                  //this._oUserSubject.subscribe();

                  //this._oUserSubject.next(obj);

                  // start - saving UID to cookie

                  /* var d = new Date();
                  d.setTime(d.getTime() + environment.ExpiryTimeInMS);
                  
                  this.cookieService.set(
                    "userData",
                    this.response,
                    d
                  ); */
                  //this.CookieValue = this.cookieService.get("UID");
                  //alert(this.CookieValue);

                  // End - saving UID to cookie

                  this.storage.clearStorageData("userData");
                  this.storage.setStorageData("userData", this.response, true);
                  this.storage.clearStorageData("cognitoId");
                  this.storage.setStorageData('cognitoId', this.response.username, false);
                  this.storage.setStorageData('sessionTime', this.response?.signInUserSession?.idToken?.payload?.exp, false);
                  this.storage.setStorageData('sessionId', this.response?.signInUserSession?.idToken?.jwtToken, false);
                  this.sharedService.getLoggedInName.emit(true);
                  if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
                    var pro = this.sharedService.queryParams.aoc;

                    if (pro != undefined && pro != null && pro == "qtt") {
                      this.isProfile = true;
                    }
                    else {
                      this.isProfile = false;
                    }
                  }
                  var address_proof_available = this.response?.attributes["custom:pan_address_proof"];
                  if (address_proof_available != undefined && address_proof_available != null && address_proof_available != '' && this.isProfile) {
                    this.storage.clearStorageData("address_proof_available");
                    this.storage.setStorageData('address_proof_available', address_proof_available, true);
                  }
                  var roleArray = this.response?.signInUserSession?.idToken?.payload["cognito:groups"];
                  if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
                    this.storage.setStorageData('rolesArray', roleArray, false);
                    if (roleArray.indexOf(this.apiRouteService.roles['superadmin']) != -1) {
                      this.storage.clearStorageData("role");
                      this.storage.setStorageData('role', this.apiRouteService.roles['superadmin'], false);
                    }
                    else if (roleArray.indexOf(this.apiRouteService.roles['admin']) != -1) {
                      this.storage.clearStorageData("role");
                      this.storage.setStorageData('role', this.apiRouteService.roles['admin'], false);
                    }
                    else if (roleArray.indexOf(this.apiRouteService.roles['callcenter']) != -1) {
                      this.storage.clearStorageData("role");
                      this.storage.setStorageData('role', this.apiRouteService.roles['callcenter'], false);
                    }
                    else if (roleArray.indexOf(this.apiRouteService.roles['rm']) != -1) {
                      this.storage.clearStorageData("role");
                      this.storage.setStorageData('role', this.apiRouteService.roles['rm'], false);
                    }
                    else {
                      this.storage.clearStorageData("role");
                      this.storage.setStorageData('role', this.apiRouteService.roles['customer'], false);
                    }
                  }
                  if (this.isProfile) {
                    if (this.storage.getStorageData('role', false) == this.apiRouteService.roles.customer)
                      this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { deviceKey: this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), refreshToken: this.response.signInUserSession.refreshToken.token, cognitoId: this.response.username, src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
                    else {
                      if(this.assetValuated){
                        this.dialogRef.close(true);
                      }
                      else
                      this.router.navigate(['/home']);
                      //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
                    }
                      //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
                  }
                  else {
                    if(this.assetValuated){
                      this.dialogRef.close(true);
                    }
                    else
                    this.router.navigate(['/home']);
                    //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
                  }
                }
              }).catch(e => {
                console.log(e);
              });
            }
            else {
              //console.log(result);
            }
          })
        }
        // console.log(this.response.signInUserSession.idToken.jwtToken);
        if (
          this.response.signInUserSession.idToken.jwtToken != null &&
          this.response.signInUserSession.idToken.jwtToken != ''
        ) {

          // let obj: SignInProp = new SignInProp();
          // obj.UserID = this.response;
          // obj.LoginTime = 0;

          // this.sharedService._oUserSubject = new BehaviorSubject<SignInProp>(obj);
          // this.sharedService._oUserSubject.subscribe();

          // this._oUserSubject = new BehaviorSubject<SignInProp>(obj);

          ////this._oUserSubject.next(obj);

          // console.log(this.response);

          // start - saving UID to cookie

          /* var d = new Date();
          d.setTime(d.getTime() + environment.ExpiryTimeInMS);
      
          this.cookieService.set(
            "userData",
            this.response,
            d
          ); */
          //this.CookieValue = this.cookieService.get("UID");
          //alert(this.CookieValue);

          // End - saving UID to cookie



          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData('cognitoId', this.response.username, false);
          this.storage.clearStorageData("userData");
          this.storage.setStorageData("userData", this.response, true);
          this.storage.setStorageData('sessionTime', this.response?.signInUserSession?.idToken?.payload?.exp, false);
          this.storage.setStorageData('sessionId', this.response?.signInUserSession?.idToken?.jwtToken, false);
          this.sharedService.getLoggedInName.emit(true);

          if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
            var pro = this.sharedService.queryParams.aoc;
            if (pro != undefined && pro != null && pro == "qtt") {
              this.isProfile = true;
            }
            else {
              this.isProfile = false;
            }
          }
          var address_proof_available = this.response?.attributes["custom:pan_address_proof"];
          if (address_proof_available != undefined && address_proof_available != null && address_proof_available != '' && this.isProfile) {
            this.storage.clearStorageData("address_proof_available");
            this.storage.setStorageData('address_proof_available', address_proof_available, true);
          }
          var roleArray = this.response?.signInUserSession?.idToken?.payload["cognito:groups"];
          if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
            this.storage.setStorageData('rolesArray', roleArray, false);
            if (roleArray.indexOf(this.apiRouteService.roles['superadmin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['superadmin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['admin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['admin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['callcenter']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['callcenter'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['rm']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['rm'], false);
            }
            else {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['customer'], false);
            }
          }
          if (this.partnerFlow != undefined && this.partnerFlow != null && this.partnerFlow == "true") {
            this.storage.clearStorageData("partnerDealerUserId")
            this.storage.setStorageData('partnerDealerUserId', this.response.username, true)
            this.router.navigate([`./customer/dashboard/partner-dealer/basic-details`])
          }
          else if (this.response?.attributes["custom:kyc_verified"] == "Verified" && this.response?.attributes["custom:pan_address_proof"] == "True" && this.response?.attributes["email"] != "" && this.response?.attributes["email"] != null && this.response?.attributes["email"] != undefined) {
            //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
            if(this.assetValuated){
              this.dialogRef.close(true);
            }
            else
            this.router.navigate(['/home']);
            //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
          }
          else {
            if (this.isProfile) {
              if (this.storage.getStorageData('role', false) == this.apiRouteService.roles.customer)
                this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { deviceKey: this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), refreshToken: this.response.signInUserSession.refreshToken.token, cognitoId: this.response.username, src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
              else {
                if(this.assetValuated){
                  this.dialogRef.close(true);
                }
                else
                this.router.navigate(['/home']);
                //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
              }
            }
            else {
              if(this.assetValuated){
                this.dialogRef.close(true);
              }
              else
              this.router.navigate(['/home']);
              //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
            }
          }
        }
      })
      .catch((err) => {
        if (err.message.indexOf('idToken') > -1) {
          this.message = "";
        }
        else {
          this.message = err.message;
        }
         
        // console.log(err)
      });
    // let token=this.response.idToken.jwtToken;
    // if()
    //  this.message= "login Successful";
  }

  signInWithOtp = () => {
    if(!this.signinform.get('mobile')?.value || !this.signinform.get('countrycode')?.value.prefixCode){
      return;
    }
    //this.spinner.show();

    this.message = "";
    this.awsConfig
      .awsLoginWithOtp(this.signinform.get('countrycode')?.value.prefixCode + this.signinform.get('mobile')?.value)
      .then((res) => {

        this.otpResponse = res;
        this.otpSection = false;
        this.otpError = false;
        this.resendOTP = false;
        this.remainingTime = environment.remainingTime;
        this.startCountdown(environment.resendOtpTime);
         
        //console.log(res);
      }).catch((err) => {
        console.log(err);
        this.message = err.message;
         
      });
  }

  startCountdown(seconds: number) {
    let counter = seconds;
    let minutes, sec, finalTime;
    // let elem = document.getElementById('resend');

    const interval = setInterval(() => {
      // console.log(counter);
      counter--;

      if (counter < 0) {
        clearInterval(interval);
        this.resendOTP = true;
        // console.log(this.resendOTP);
      }

      minutes = Math.floor(counter / 60);
      sec = counter - minutes * 60;
      finalTime = this.str_pad_left(minutes, '0', 2) + ':' + this.str_pad_left(sec, '0', 2);
      this.remainingTime = finalTime;

    }, 1000);
  }

  str_pad_left(str: number, pad: string, length: number) {
    return (new Array(length + 1).join(pad) + str).slice(-length);
  }

  verifyOtpForSignIN = () => {
    //this.spinner.show();

    let otp =
      this.submitOtp.get('otpField1')?.value +
      '' +
      this.submitOtp.get('otpField2')?.value +
      '' +
      '' +
      this.submitOtp.get('otpField3')?.value +
      '' +
      '' +
      this.submitOtp.get('otpField4')?.value +
      '' +
      '' +
      this.submitOtp.get('otpField5')?.value +
      '' +
      '' +
      this.submitOtp.get('otpField6')?.value;
    this.awsConfig
      .awsVerifyOtp(this.otpResponse, otp)
      .then((res) => {
        if (res.Username != '' && res.signInUserSession != null) {
          //this.router.navigate([`./dashboard`]);
          //this.redirectToPaymentHandler();
          // start - saving UID to cookie

          //  let obj: SignInProp = new SignInProp();
          //   obj.UserID = this.response;
          //   obj.LoginTime = 0;

          //   //this._oUserSubject = new BehaviorSubject<SignInProp>(obj);

          //   this.sharedService._oUserSubject = new BehaviorSubject<SignInProp>(obj);
          //   this.sharedService._oUserSubject.subscribe();

          ////this._oUserSubject.next(obj);

          /* var d = new Date();
          d.setTime(d.getTime() + environment.ExpiryTimeInMS);
          
          this.cookieService.set(
            "userData",
            this.response,
            d
          ); */
          //this.CookieValue = this.cookieService.get("UID");
          //alert(this.CookieValue);

          // End - saving UID to cookie
          var isActive = res?.attributes["custom:status"];
          if(isActive!=undefined && isActive=="inactive")
          {
             
            this.notificationservice.error('You are not authorized to access the application. Please raise a callback request to get in touch with the iQuippo team.',true)
            this.sharedService.logoutInactive();
            return;
          }

          this.sharedService.getLoggedInName.emit(true);
          this.response = res as any;
          this.storage.clearStorageData("userData");
          this.storage.setStorageData('sessionTime', this.response?.signInUserSession?.idToken?.payload?.exp, false);
          this.storage.setStorageData('sessionId', this.response?.signInUserSession?.idToken?.jwtToken, false);
          this.storage.setStorageData("userData", this.response, true);
          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData('cognitoId', this.response?.signInUserSession?.idToken?.payload?.username, false);
          if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
            var pro = this.sharedService.queryParams.aoc;
            if (pro != undefined && pro != null && pro == "qtt") {
              this.isProfile = true;
            }
            else {
              this.isProfile = false;
            }
          }
          var address_proof_available = this.response?.attributes["custom:pan_address_proof"];
          if (address_proof_available != undefined && address_proof_available != null && address_proof_available != '' && this.isProfile) {
            this.storage.clearStorageData("address_proof_available");
            this.storage.setStorageData('address_proof_available', address_proof_available, true);
          }
          var roleArray = this.response?.signInUserSession?.idToken?.payload["cognito:groups"];
          if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
            this.storage.setStorageData('rolesArray', roleArray, false);
            if (roleArray.indexOf(this.apiRouteService.roles['superadmin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['superadmin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['admin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['admin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['callcenter']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['callcenter'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['rm']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['rm'], false);
            }
            else {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['customer'], false);
            }
          }
          if (this.partnerFlow != undefined && this.partnerFlow != null && this.partnerFlow == "true") {
            this.storage.clearStorageData("partnerDealerUserId")
            this.storage.setStorageData('partnerDealerUserId', this.response?.signInUserSession?.idToken?.payload?.username, true)
            this.router.navigate([`./customer/dashboard/partner-dealer/basic-details`])
          }
          else if (this.response?.attributes["custom:kyc_verified"] == "Verified" && this.response?.attributes["custom:pan_address_proof"] == "True" && this.response?.attributes["email"] != "" && this.response?.attributes["email"] != null && this.response?.attributes["email"] != undefined) {
            if(this.assetValuated){
              this.dialogRef.close(true);
            }
            else
            this.router.navigate(['/home']);
            //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
          }
          else {
            if (this.isProfile) {
              if (this.storage.getStorageData('role', false) == this.apiRouteService.roles.customer)
                this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { deviceKey: this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), refreshToken: this.response.signInUserSession.refreshToken.token, cognitoId: this.response.signInUserSession.idToken.payload.username, src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
              else {
                if(this.assetValuated){
                  this.dialogRef.close(true);
                }
                else
                this.router.navigate(['/home']);
                //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
              }
            }
            else {
              if(this.assetValuated){
                this.dialogRef.close(true);
              }
              else
              this.router.navigate(['/home']);
              //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, false);
            }
          }
        }
        else {
          this.message = "OTP didn't matched";
          //this.spinner.hide()
        }
        // console.log(res);
      });
  };

  async signInWithGoogle() {
    //this.spinner.show();
    debugger;
    await this.sharedService.signInWithGoogleAndFacebook(GoogleLoginProvider.PROVIDER_ID, this.assetValuated);
    this.dialogRef.close(true);
  }

  async signInWithFB() {
    //this.spinner.show();
    await this.sharedService.signInWithGoogleAndFacebook(FacebookLoginProvider.PROVIDER_ID, this.assetValuated);
    this.dialogRef.close(true);
  }
  // signInWithGoogleAndFacebook(socialProviderId:string): void {

  //   this.authService.signIn(socialProviderId).then((x) => {
  //     this.googleResponse = x;
  //     this.storage.clearStorageData('google_data');
  //     this.storage.setStorageData("google_data", this.googleResponse, true);
  //     var verifyUserRequestModel = new VerifyUser();
  //     verifyUserRequestModel.socialId = this.googleResponse.id;
  //     this.accountService.getDetails(verifyUserRequestModel).subscribe(verifyRes => {
  //       // console.log(verifyRes);
  //       var getResponse = verifyRes as any;
  //       this.response = verifyRes as any;
  //       this.sharedService.getLoggedInName.emit(true);
  //        var getResponseBody = getResponse.body;
  //        var getResponseBodyJSON = JSON.parse(getResponseBody);
  //     if(getResponse !=null){
  //       if (getResponse.statusCode == '301') {

  //         this.storage.setStorageData('socialId', this.googleResponse.id, true);
  //         this.router.navigateByUrl('/register', { state: { isSocialLogin: true } });
  //         // this.router.navigate([`./register`]);
  //         //this.otpSection =false;
  //       }
  //       if (getResponse.statusCode == '300') {
  //         // var getResponseBody = getResponse.body;
  //       // var getResponseBodyJSON = JSON.parse(getResponse);
  //         if(getResponseBodyJSON !=undefined && getResponseBodyJSON!=null )
  //         {
  //         this.awsConfig.awsLoginWithOtp(getResponseBodyJSON.cognitoId)
  //         .then((OtpRes) => {
  //           // if(RSA_NO_PADDING.)
  //           if(OtpRes !=undefined && OtpRes!=null ){
  //           this.awsConfig.awsVerifyOtp(OtpRes,this.googleResponse.id).then((verifyOtpRes)=>{
  //               console.log(verifyOtpRes);
  //           //this.sharedService.redirecteToMP1(verifyOtpRes.accessToken, verifyOtpRes.refreshToken, verifyOtpRes.deviceKey , verifyOtpRes.idToken);
  //           if(verifyOtpRes!=undefined && verifyOtpRes !=null) {
  //             this.storage.setStorageData('sessionTime', verifyOtpRes?.signInUserSession?.idToken?.payload?.exp,false);
  //             this.storage.setStorageData('sessionId', verifyOtpRes?.signInUserSession?.idToken?.jwtToken,false);
  //             this.sharedService.redirecteToMP1(verifyOtpRes.signInUserSession.accessToken.jwtToken, verifyOtpRes.signInUserSession.refreshToken.token, verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), verifyOtpRes.signInUserSession.idToken.jwtToken);
  //           }
  //           }).catch((err)=>{
  //             console.log(err);
  //           })
  //           }
  //           // console.log(res);
  //         }).catch((err) => {
  //           console.log(err);
  //           if (err.code == "NotAuthorizedException") {
  //             this.message = err.message;
  //           }
  //         });
  //       }
  //       }
  //     }
  //     })
  //     //this.router.navigate([`./dashboard`]);

  //     // console.log(this.googleResponse);
  //   }).catch((err) => {
  //     console.log(err)
  //   })

  // }

  // signInWithFB(): void {
  // //  const fbLoginOptions = {
  // //   scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
  // //   return_scopes: true,
  // //   enable_profile_selector: true
  // // }
  //   this.authService
  //     .signIn(FacebookLoginProvider.PROVIDER_ID).then((x) => {
  //       this.googleResponse = x;
  //       this.storage.clearStorageData('google_data');
  //       this.storage.setStorageData("google_data", this.googleResponse, true);
  //       var verifyUserRequestModel = new VerifyUser();
  //       verifyUserRequestModel.socialId = this.googleResponse.id;
  //       this.accountService.getDetails(verifyUserRequestModel).subscribe(verifyRes => {
  //           // console.log(verifyRes);
  //           var getResponse = verifyRes as any;
  //           this.response = verifyRes as any;
  //           this.sharedService.getLoggedInName.emit(true);
  //            var getResponseBody = getResponse.body;
  //            var getResponseBodyJSON = JSON.parse(getResponseBody);

  //           if (getResponse.statusCode == '301') {

  //             this.storage.setStorageData('socialId', this.googleResponse.id, true);
  //             this.router.navigateByUrl('/register', { state: { isSocialLogin: true } });
  //             // this.router.navigate([`./register`]);
  //             //this.otpSection =false;
  //           }
  //           if (getResponse.statusCode == '300') {
  //             // var getResponseBody = getResponse.body;
  //           // var getResponseBodyJSON = JSON.parse(getResponse);
  //             this.awsConfig.awsLoginWithOtp(getResponseBodyJSON.cognitoId)
  //             .then((OtpRes) => {
  //               // if(RSA_NO_PADDING.)
  //               this.awsConfig.awsVerifyOtp(OtpRes,this.googleResponse.id).then((verifyOtpRes)=>{
  //                   console.log(verifyOtpRes);
  //               //this.sharedService.redirecteToMP1(verifyOtpRes.accessToken, verifyOtpRes.refreshToken, verifyOtpRes.deviceKey , verifyOtpRes.idToken);
  //                this.sharedService.redirecteToMP1(verifyOtpRes.signInUserSession.accessToken.jwtToken, verifyOtpRes.signInUserSession.refreshToken.token, verifyOtpRes.pool.storage.getItem(verifyOtpRes.keyPrefix + '.' + verifyOtpRes.username + '.deviceKey'), verifyOtpRes.signInUserSession.idToken.jwtToken);

  //               }).catch((err)=>{
  //                 console.log(err);
  //               })
  //               // console.log(res);
  //             }).catch((err) => {
  //               console.log(err);
  //               if (err.code == "NotAuthorizedException") {
  //                 this.message = err.message;
  //               }
  //             });
  //           }
  //         })
  //         //this.router.navigate([`./dashboard`]);

  //         // console.log(this.googleResponse);
  //       }).catch((err) => {
  //         console.log(err)
  //       })
  // }

  goToForgotPage() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
      var pro = this.sharedService.queryParams.aoc;
      var src = this.sharedService.queryParams.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { src: this.sharedService.queryParams.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword]);
      }
    } else {
      this.router.navigateByUrl('/' + this.appRouteEnum.ForgotPassword);
    }
  }

  redirectToPaymentHandler(): Observable<any> {
    const urlEncodeData = 'amount=1&currCode=608&payMethod=CC';

    let headers = new HttpHeaders();

    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');

    headers = headers.append('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');



    return this.httpClient.post('https://dev.iquippo.com/auth/nmp/sso',

      urlEncodeData, {

      headers,

      responseType: 'text'

    });

  }

  myfun(val: string) {
    // console.log(val);
    // val.
    // @ts-ignore
    // val.nextSibling.focus();
    switch (val) {
      case '1':
        if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '2':
        if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '3':
        if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        else {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        break;
      case '4':
        if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        else {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        break;
      case '5':
        if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
          this.otp6?.nativeElement.focus();
          this.otp6?.nativeElement.select();
        }
        else {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        break;
      case '6':
        if (this.submitOtp.get('otpField6')?.value != '' && this.submitOtp.get('otpField6')?.value != null && this.submitOtp.get('otpField6')?.value != undefined) {
          this.otp6?.nativeElement.focus();
        }
        else {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        break;
      case '11':
        if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
          this.otp1?.nativeElement.select();
          this.otp1?.nativeElement.focus();
        }
        break;
      case '22':
        if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
          this.otp2?.nativeElement.select();
          this.otp2?.nativeElement.focus();
        }
        break;
      case '33':
        if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
          this.otp3?.nativeElement.select();
          this.otp3?.nativeElement.focus();
        }
        break;
      case '44':
        if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
          this.otp4?.nativeElement.select();
          this.otp4?.nativeElement.focus();
        }
        break;
      case '55':
        if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
          this.otp5?.nativeElement.select();
          this.otp5?.nativeElement.focus();
        }
        break;

      default:
        break;
    }

  }
  close(){
    this.dialogRef.close(false);
  }

}
