import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { CommunicationPrefrencesDTO, MasterData } from 'src/app/shared/abstractions/user';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-compreference',
  templateUrl: './compreference.component.html',
  styleUrls: ['./compreference.component.css']
})
export class CompreferenceComponent implements OnInit {

  communicationPrefrences: CommunicationPrefrencesDTO;
  masterDatalist: MasterData;
  communicationForm: FormGroup;
  emailService: boolean = false;
  objData?: any;
  VerifiedDataobj: any;
  paramaterarray: any;
  isCallCenter: any;
  Email_services: any;
  Email_promotion: any;
  whatsapp_services: any;
  whatsapp_promotion: any;
  Sms_services: any;
  Sms_promotion: any;
  Voicecall_services: any;
  Voicecall_promotion: any;
  isOption: boolean = false;
  returnUrl='';
  userRole: any;
  role: any;
  isrm: string = '';
  constructor(private router: Router, private storage: StorageDataService, private http: HttpClient, private fb: FormBuilder,
    private spinner: NgxSpinnerService, private notify: NotificationService, private appRouteEnum: AppRouteEnum,
    public activatedRoute: ActivatedRoute,public apiRouteService : ApiRouteService) {

    this.masterDatalist = {};
    this.communicationPrefrences = {};


    this.communicationForm = this.fb.group({
      Email: new FormControl(['']),
      Email_services: new FormControl(['']),
      Email_promotion: new FormControl(['']),
      Sms: new FormControl(['']),
      Sms_services: new FormControl(['']),
      Sms_promotion: new FormControl(['']),
      Whatsapp: new FormControl(['']),
      Whatsapp_services: new FormControl(['']),
      Whatsapp_promotion: new FormControl(['']),
      Voicecall: new FormControl(['']),
      Voicecall_services: new FormControl(['']),
      Voicecall_promotion: new FormControl(['']),
      Industry: new FormControl([''], [
        Validators.required,

      ]),
      Asset: new FormControl([''], [
        Validators.required,

      ]),
      Services: new FormControl([''], [
        Validators.required,

      ]),
      Actas: new FormControl([''], [
        Validators.required,

      ]),

    });
  }

  ngOnInit(): void {
    this.userRole = this.storage.getStorageData("userRole", false);
    this.role = this.storage.getStorageData("role", false);
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
      if (params['isrequired'] == 'true') {
        this.isOption = true;
      }
      else {
        this.isOption = false;
      }
      if(params['isrm']){
        this.isrm = params['isrm'];
        //this.getRmList();
      }
    });
    //this.spinner.show();
    this.communicationPrefrences.assetClass = [];
    this.communicationPrefrences.actAs = [];
    this.communicationPrefrences.industry = [];
    this.communicationPrefrences.services = [];
    //this.paramaterarray = this.storage.getStorageData("aflparams", true);
    //this.communicationPrefrences.cognitoId=this.paramaterarray.cognitoId;//"d52258f6-2927-4bbc-9bdb-2f6f0e5d4979"
    this.isCallCenter = this.storage.getStorageData("isCallCenter", true);
    if (this.isCallCenter == "1") {
      this.communicationPrefrences.cognitoId = this.storage.getStorageData("cognitoUserId", true);
      this.communicationPrefrences.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    }
    else {
      this.communicationPrefrences.cognitoId = this.storage.getStorageData("cognitoId", false);
    }
    this.getPreferencesMasterDatafn();

  }
  addComPrefName(ev: any, name: string, type: string) {
    this.Email_services = this.communicationForm.get('Email_services')?.value;
    this.Email_promotion = this.communicationForm.get('Email_promotion')?.value;
    this.whatsapp_services = this.communicationForm.get('Whatsapp_services')?.value;
    this.whatsapp_promotion = this.communicationForm.get('Whatsapp_promotion')?.value;
    this.Sms_services = this.communicationForm.get('Sms_services')?.value;
    this.Sms_promotion = this.communicationForm.get('Sms_promotion')?.value;
    this.Voicecall_services = this.communicationForm.get('Voicecall_services')?.value;
    this.Voicecall_promotion = this.communicationForm.get('Voicecall_promotion')?.value;


  }

  //   addComPrefName(ev:any,name:string,type:string){
  //   if(name=='email'){
  //       let s=this.communicationPrefrences?.communicationPref?.email?.service;
  //       let p=this.communicationPrefrences?.communicationPref?.email?.promotion;
  //       if(type=='services'){//
  //       s=true;
  //       p=false;   
  //       }
  //     else{
  //       s=false;
  //       p=true;
  //     }
  //   }

  //   else if (name="sms")
  //     {
  //       let s=this.communicationPrefrences?.communicationPref?.sms?.service;
  //       let p=this.communicationPrefrences?.communicationPref?.sms?.promotion;
  //       if(type=='services'){//
  //       s=true;
  //       p=false;   
  //       }
  //     else{
  //       s=false;
  //       p=true;
  //     }
  //   }
  //     else if (name="whatsapp")
  //     {
  //       let s=this.communicationPrefrences?.communicationPref?.sms?.service;
  //       let p=this.communicationPrefrences?.communicationPref?.sms?.promotion;
  //       if(type=='services'){//
  //       s=true;
  //       p=false;   
  //       }
  //     else{
  //       s=false;
  //       p=true;
  //     }
  // }
  //   else 
  //     {
  //       let s=this.communicationPrefrences?.communicationPref?.sms?.service;
  //       let p=this.communicationPrefrences?.communicationPref?.sms?.promotion;
  //       if(type=='services'){//
  //       s=true;
  //       p=false;   
  //     }
  //     else{
  //       s=false;
  //       p=true;
  //     }

  //   }

  //   console.log(this.communicationPrefrences.communicationPref);
  // }

  onCheckboxChange(e: any, control: any, checkValue: string) {
    if (e.target.checkValue != '') {

      //   if(control=='Industry')
      //   {
      //     this.communicationPrefrences.industry?.push(checkValue);

      //   }
      //  else if(control=='Asset')
      //   {
      //     this.communicationPrefrences.assetClass?.push(checkValue);
      //   }
      //   else if(control=='Services')
      //   {
      //     this.communicationPrefrences.services?.push(checkValue);
      //   }
      //   else(control=='Actas')
      //   {
      //     this.communicationPrefrences.actAs?.push(checkValue);
      //   }

      //console.log(this.communicationPrefrences);
    }
  }

  onSubmit() {
    //this.spinner.show();
    this.communicationForm.markAllAsTouched();
    if (this.communicationForm.valid) {
      this.communicationPrefrences.assetClass = this.communicationForm.get('Asset')?.value;
      this.communicationPrefrences.industry = this.communicationForm.get('Industry')?.value;
      this.communicationPrefrences.actAs = this.communicationForm.get('Actas')?.value;
      this.communicationPrefrences.services = this.communicationForm.get('Services')?.value;
      //this.communicationPrefrences.crmId = this.paramaterarray.cognitoId;//"5144886a-f35b-4fec-b504-16c39921a744";
      this.communicationPrefrences.communicationPref = {
        "email": { "service": this.communicationForm.get('Email_services')?.value, "promotion": this.communicationForm.get('Email_promotion')?.value },
        "sms": { "service": this.communicationForm.get('Sms_services')?.value, "promotion": this.communicationForm.get('Sms_promotion')?.value },
        "whatsapp": { "service": this.communicationForm.get('Whatsapp_services')?.value, "promotion": this.communicationForm.get('Whatsapp_promotion')?.value },
        "voiceCall": { "service": this.communicationForm.get('Voicecall_services')?.value, "promotion": this.communicationForm.get('Voicecall_promotion')?.value }
      }
      //console.log(this.communicationPrefrences);

      // this.communicationPrefrences.username=this.paramaterarray.cognitoId;
      if (this.isCallCenter == "1") {
        this.communicationPrefrences.cognitoId = this.storage.getStorageData("cognitoUserId", true);
        this.communicationPrefrences.username = this.storage.getStorageData("cognitoUserId", true);
        this.communicationPrefrences.callcenterCognito = this.storage.getStorageData("cognitoId", false);
      }
      else {
        this.communicationPrefrences.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.communicationPrefrences.username = this.storage.getStorageData("cognitoId", false);
      }

      this.http.post(environment.communicationPref,
        JSON.stringify(this.communicationPrefrences)).subscribe((resp: any) => {
          //console.log(resp.statusCode);
          this.objData = JSON.parse(resp.body);
          if (resp.statusCode == 242 || resp.statusCode == 240) {
            this.notify.success("Preferences Saved Succesfully");
            if (this.isOption == true) {
                if(this.userRole==this.apiRouteService.roles.customer)
              {
                if(this.isrm!=undefined && this.isrm!='')
                this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { 'isrequired': true,returnUrl:this.returnUrl,isrm:this.isrm } });
                else
                this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { 'isrequired': true,returnUrl:this.returnUrl } });
              }
              else{
                if(this.isrm!=undefined && this.isrm!='')
                this.router.navigate([`./` + this.appRouteEnum.AdminBankingDetails], { queryParams: { 'isrequired': true,returnUrl:this.returnUrl,isrm:this.isrm } });
                else
                this.router.navigate([`./` + this.appRouteEnum.AdminBankingDetails], { queryParams: { 'isrequired': true,returnUrl:this.returnUrl } });
              }
              
            }
            else {
              if(this.userRole==this.apiRouteService.roles.customer)
            {
              if(this.isrm!=undefined && this.isrm!='')
            this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: {returnUrl:this.returnUrl,isrm:this.isrm}});
              else
              this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: {returnUrl:this.returnUrl}});
            }
            else{
              if(this.isrm!=undefined && this.isrm!='')
              this.router.navigate([`./` + this.appRouteEnum.AdminBankingDetails], { queryParams: {returnUrl:this.returnUrl,isrm:this.isrm}});
              else
              this.router.navigate([`./` + this.appRouteEnum.AdminBankingDetails], { queryParams: {returnUrl:this.returnUrl}});
            }
            }
          }
          else {
             
            this.notify.error("Preferences not Saved Succesfully!! Something went wrong");
            // alert("Preferences not Saved Succesfully!! Something wrong");
          }
        }, error => {
           
          console.log(error);
        })
    }
    else {
       
    }
  }

  goBack() {
    if(this.role != null && this.role != undefined && this.role == 'SuperAdmin') {
      this.router.navigate([`./` + this.appRouteEnum.AdminProfile], { queryParams: { cognitoId: this.storage.getStorageData("cognitoUserId", true), isCallCenter: 1 } });
    } else {
      this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: {returnUrl:this.returnUrl}});
    }
  }

  getPreferencesMasterDatafn() {
    if (this.isCallCenter == "1") {
      this.masterDatalist.username = this.storage.getStorageData("cognitoUserId", true);
      //this.communicationPrefrences.crmId = this.storage.getStorageData("cognitoId", true);
    }
    else {
      this.masterDatalist.username = this.storage.getStorageData("cognitoId", false);
    }
    //this.masterDatalist.username=this.paramaterarray.cognitoId;//"5144886a-f35b-4fec-b504-16c39921a744";//
    this.masterDatalist.getData = 'getData';
    //console.log('Perfrence data',JSON.stringify(this.masterDatalist));
    this.http.post(environment.communicationPref,
      JSON.stringify(this.masterDatalist))
      .subscribe((data: any) => {
        //console.log(data);

        this.objData = JSON.parse(data.body);
        console.log(this.objData);
         
        if (this.objData != null && this.objData != undefined) {
          if (this.objData?.message != null && this.objData?.message != undefined && this.objData?.message != "data not found") {
            this.communicationForm.patchValue({
              Email: ((this.objData?.message?.communcationPref_email?.promotion || this.objData?.message?.communcationPref_email?.service) ? true : false),
              Sms: ((this.objData?.message?.communcationPref_sms?.promotion || this.objData?.message?.communcationPref_sms?.service) ? true : false),
              Voicecall: ((this.objData?.message?.communcationPref_whatsapp?.promotion || this.objData?.message?.communcationPref_whatsapp?.service) ? true : false),
              Whatsapp: ((this.objData?.message?.communcationPref_voiceCall?.promotion || this.objData?.message?.communcationPref_voiceCall?.service) ? true : false),
              Email_services: this.objData?.message?.communcationPref_email?.service,
              Email_promotion: this.objData?.message?.communcationPref_email?.promotion,
              Sms_services: this.objData?.message?.communcationPref_sms?.service,
              Sms_promotion: this.objData?.message?.communcationPref_sms?.promotion,
              Whatsapp_services: this.objData?.message?.communcationPref_whatsapp?.service,
              Whatsapp_promotion: this.objData?.message?.communcationPref_whatsapp?.promotion,
              Voicecall_services: this.objData?.message?.communcationPref_voiceCall?.service,
              Voicecall_promotion: this.objData?.message?.communcationPref_voiceCall?.promotion,
              Industry: this.objData?.message?.industry,
              Asset: this.objData?.message?.assetClass,
              Services: this.objData?.message?.services,
              Actas: this.objData?.message?.actAs
            });
          }
          else {
            this.communicationForm.patchValue({
              Email: true,
              Sms: true,
              Voicecall: true,
              Whatsapp: false,
              Email_services: true,
              Email_promotion: true,
              Sms_services: true,
              Sms_promotion: true,
              Whatsapp_services: false,
              Whatsapp_promotion: false,
              Voicecall_services: true,
              Voicecall_promotion: true,
              Industry: '',
              Asset: '',
              Services: '',
              Actas: ''
            });
          }
        }
         
        // console.log(this.objData.masterData.message.communcationPref_email);
        // this.communicationForm.get('Industry').?.setValue(this.objData.masterData.message)
      });


  }

  onComPrefChange(event: any, value: any) {
    if (value == 1) {
      if (event.checked) {
        this.communicationForm.controls['Email_services'].setValue(true);
        this.communicationForm.controls['Email_promotion'].setValue(true);
      }
      else {
        this.communicationForm.controls['Email_services'].setValue(false);
        this.communicationForm.controls['Email_promotion'].setValue(false);
      }
    }
    else if (value == 2) {
      if (event.checked) {
        this.communicationForm.controls['Sms_services'].setValue(true);
        this.communicationForm.controls['Sms_promotion'].setValue(true);
      }
      else {
        this.communicationForm.controls['Sms_services'].setValue(false);
        this.communicationForm.controls['Sms_promotion'].setValue(false);
      }
    }
    else if (value == 3) {
      if (event.checked) {
        this.communicationForm.controls['Whatsapp_services'].setValue(true);
        this.communicationForm.controls['Whatsapp_promotion'].setValue(true);
      }
      else {
        this.communicationForm.controls['Whatsapp_services'].setValue(false);
        this.communicationForm.controls['Whatsapp_promotion'].setValue(false);
      }
    }
    else if (value == 4) {
      if (event.checked) {
        this.communicationForm.controls['Voicecall_services'].setValue(true);
        this.communicationForm.controls['Voicecall_promotion'].setValue(true);
      }
      else {
        this.communicationForm.controls['Voicecall_services'].setValue(false);
        this.communicationForm.controls['Voicecall_promotion'].setValue(false);
      }
    }
  }

}
