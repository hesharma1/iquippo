import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompreferenceComponent } from './compreference.component';

describe('CompreferenceComponent', () => {
  let component: CompreferenceComponent;
  let fixture: ComponentFixture<CompreferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompreferenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
