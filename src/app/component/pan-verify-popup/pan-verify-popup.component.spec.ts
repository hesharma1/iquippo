import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanVerifyPopupComponent } from './pan-verify-popup.component';

describe('PanVerifyPopupComponent', () => {
  let component: PanVerifyPopupComponent;
  let fixture: ComponentFixture<PanVerifyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanVerifyPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanVerifyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
