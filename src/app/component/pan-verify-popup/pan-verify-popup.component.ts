import { Component, OnInit,Inject } from '@angular/core';
// import { MatDialogRef } from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-pan-verify-popup',
  templateUrl: './pan-verify-popup.component.html',
  styleUrls: ['./pan-verify-popup.component.css']
})
export class PanVerifyPopupComponent implements OnInit {
  description: string;
  agreementNotApproved: boolean = false;
  constructor(public dialogRef: MatDialogRef<PanVerifyPopupComponent>,@Inject(MAT_DIALOG_DATA) data : any) { 
    this.description = data.message;
    this.agreementNotApproved = data.agreementNotApproved;
  }

  ngOnInit(): void {
  }

  onCancel(){
    this.dialogRef.close(false);
    // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId , isCallCenter: 1} });
  }

  onSuccess(){
    this.dialogRef.close(true);
    // this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
  }
  
  close(){
    this.dialogRef.close();
  }
}

