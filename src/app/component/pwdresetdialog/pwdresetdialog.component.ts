import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Router} from '@angular/router';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';

@Component({
  selector: 'app-pwdresetdialog',
  templateUrl: './pwdresetdialog.component.html',
  styleUrls: ['./pwdresetdialog.component.css']
})
export class PwdresetdialogComponent implements OnInit {

  constructor(private router: Router, private appRouteEnum: AppRouteEnum, @Inject(MAT_DIALOG_DATA) public data: any,) { }
  ngOnInit(): void {
  }


  goToLogin(){
    if (this.data != null && this.data.params != null) {
      var pro = this.data.params.aoc;
      var src = this.data.params.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.data.params.src, aoc: this.data.params.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { aoc: this.data.params.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.data.params.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.Login]);
      }
    }
    else {
      this.router.navigateByUrl('/' + this.appRouteEnum.Login);
    }
  }
}
