import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';
import { DashboardService } from 'src/app/services/dashboard-service';
import { SharedService } from 'src/app/services/shared-service.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { PaymentService } from 'src/app/services/payment.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { environment } from 'src/environments/environment';
import { SigninComponent } from '../signin/signin.component';
import { AuctionService } from 'src/app/services/auction.service';
import { ESignConfirmDialogComponent } from '../customer/auction-e-sign/e-sign-confirm-dialog/e-sign-confirm-dialog.component';
import { CustomerSearchModalComponent } from '../shared/customer-search-modal/customer-search-modal.component';
import { MakeACallComponent } from '../make-a-call/make-a-call.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  homeForm!: FormGroup;
  auctionBannerList: any = [];
  cityList: any;
  cityData: any;
  allSearchList: any;
  bannerRes;
  modelData;
  allbrandData;
  cognitoId;
  allModelList;
  modalName = '';
  categoryName = '';
  brandName = '';
  locationName = '';
  ongoingAuctionList: any = [];
  upcomingAuctionList: any = [];
  bannerList: any = [];
  newProductList: any = [];
  usedProductList: any = [];
  allCategoryList: any = [];
  AdvancedSearch: boolean = false;
  innerWidth: any
  categoryList;
  brandList;
  categoryData: any;
  brandData: any;
  modalData: any;
  modelList = [];
  categoryId: any;
  brandId: any;
  modalId: any;
  cityId: any;
  searchType = 'New';
  autoSuggestList;
  returnUrl = '';
  eSignReqData: any;
  esignStatus: any;
  currentFeaturedTab: any = 0;
  categoryTypingTimer: any;
  brandTypingTimer: any;
  modalTypingTimer: any;

  loansList = [
    { name: "loan 1" },
    { name: "loan 1" },
    { name: "loan 1" },
    { name: "loan 1" },
    { name: "loan 1" },
    { name: "loan 1" }, { name: "loan 1" }, { name: "loan 1" }
  ];
  options2 = {
    animation: {
      animationClass: 'transition', // done
      animationTime: 500,
    },
    swipe: {
      swipeable: true, // done
      swipeVelocity: .004, // done - check amount
    },
    drag: {
      draggable: true, // done
      dragMany: true, // todo
    },
    autoplay: {
      enabled: false,
      direction: 'right',
      delay: 5000,
      stopOnHover: true,
      speed: 6000,
    },
    arrows: true,
    infinite: false,
    breakpoints: [
      {
        width: 400,
        number: 1,
      },
      {
        width: 768,
        number: 2,
      },
      {
        width: 991,
        number: 3,
      },
      {
        width: 9999,
        number: 4,
      },
    ],
  }






  bannerslideOptions = {
    items: 1,
    dots: true,
    //nav: true,
    loop: false,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    autoplay: true,
    responsive: {
      300: {
        items: 1,
        nav: false
      },
      400: {
        items: 1,
        nav: false
      },
      500: {
        items: 1,
        nav: false
      },
      600: {
        items: 1,
        nav: false
      },
      700: {
        items: 1,
        nav: false
      },
      800: {
        items: 1,
        nav: false
      },
      900: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: false,
        loop: false
      }
    }
  };







  categorySlideOptions = {
    items: 4,
    dots: false,
    //nav: true,
    loop: false,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    autoplay: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };

  testimonialsSlideOptions = {
    items: 4,
    dots: false,
    //nav: true,
    loop: false,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    autoplay: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 2,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };

  loanschemeSlideOptions = {
    items: 4,
    dots: false,
    //nav: true,
    loop: false,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    autoplay: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 2,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };

  recentViewSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false, autoplay: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };
  doneTypingInterval: number = 1000;
  assistedMode: boolean = false;
  loading: boolean = true;
  constructor(public fb: FormBuilder, private dashboardService: DashboardService, private activatedRoute: ActivatedRoute, private appRouteEnum: AppRouteEnum, private sharedService: SharedService, private spinner: NgxSpinnerService, private commonService: CommonService, public router: Router, private adminMasterService: AdminMasterService, private storage: StorageDataService, public notify: NotificationService, private dialog: MatDialog, public paymentService: PaymentService, public apiPath: ApiRouteService, private auctionService: AuctionService) {

    this.homeForm = this.fb.group({
      searchAll: [''],
      category: [''],
      brand: [''],
      model: [''],
      assetLocation: [''],
    })
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
    });
  }

  ngOnInit(): void {
    if (this.returnUrl) {
      this.router.navigate([this.returnUrl]);
    }
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getAllDetails();
  }

  changeType(type) {
    this.searchType = type;
    this.autoSuggestList = [];
      this.categoryId = '';
      this.categoryName = '';
      this.brandId ='';
      this.brandName ='';
      this.modalId = '';
      this.modalName = '';
      this.homeForm.get('searchAll')?.setValue('');
  }

  getAllDetails() {
    let queryParam = ``;
    if (this.cognitoId) {
      queryParam = 'cognito_id=' + this.cognitoId;
    }
    this.dashboardService.getbannersList(queryParam).subscribe(async res => {
      let response: any = res;
      this.bannerRes = res;
      this.categoryList = response.category;
      this.newProductList = response.featured_new;
      this.usedProductList = response.featured_used;
      this.brandList = response.brand;
      this.ongoingAuctionList = response.ongoing_auctions;
      this.upcomingAuctionList = response.upcoming_auctions;
    });
  }

  getCategoryMaster() {
    this.adminMasterService.getCategoryMaster('limit=999').subscribe(
      (res: any) => {
        if (res != null) {
          var result = res.results;
          this.allCategoryList = result;
          this.categoryData = result;
        }
      });
  }

  getBrandMaster() {
    this.adminMasterService.getBrandMaster('limit=999').subscribe(
      (res: any) => {
        if (res != null) {
          this.allbrandData = res.results;
          this.brandData = res.results;
        }
      });
  }

  getModelMaster() {
    this.adminMasterService.getModelMaster('limit=999').subscribe((res: any) => {
      if (res != null) {
        this.allModelList = res.results;
        this.modelData = res.results;
      }
    })
  }

  keyUp($event, type) {
    if (type === 'category') {
      clearTimeout(this.categoryTypingTimer);
      this.categoryTypingTimer = setTimeout(this.filterValuesCategory.bind(this, $event), this.doneTypingInterval)
    } else if (type === 'brand') {
      clearTimeout(this.brandTypingTimer);
      this.brandTypingTimer = setTimeout(this.filterValuesBrand.bind(this, $event), this.doneTypingInterval)
    } else {
      clearTimeout(this.modalTypingTimer);
      this.modalTypingTimer = setTimeout(this.filterValuesModal.bind(this, $event), this.doneTypingInterval)
    }
  }

  filterValuesCategory(search: any) {
    if (search && search.target.value && search.target.value.length > 2) {
      let params = 'limit=999&' + 'search=' + search.target.value;
      this.adminMasterService.getCategoryMaster(params).subscribe(
        (res: any) => {
          if (res != null) {
            var result = res.results;
            this.allCategoryList = result;
            this.categoryData = result;
          }
        });
    }
    else if (!search.target.value) {
      this.allCategoryList = [];
    }
  }

  filterValuesBrand(search: any) {
    if (search && search.target.value && search.target.value.length > 2) {
      let params = 'limit=999&' + 'search=' + search.target.value;
      if (this.categoryId) {
        this.commonService.getBrandMasterByCatIdWithSearch(this.categoryId, search.target.value).subscribe(
          (res: any) => {
            this.allbrandData = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
            this.loading = false;
          });
      } else {
        this.adminMasterService.getBrandMaster(params).subscribe(
          (res: any) => {
            if (res != null) {
              this.allbrandData = res.results;
              this.brandData = res.results;
            }
          });
      }
    }
    else if (!search.target.value) {
      this.allbrandData = [];
    }
  }

  filterValuesModal(search: any) {
    if (search && search.target.value && search.target.value.length > 2) {
      let params = 'limit=999&' + 'search=' + search.target.value;
      if (this.brandId && this.categoryId) {
        this.commonService.getModelMasterByBrandIdWithSearch(this.brandId, this.categoryId, search.target.value).subscribe(
          (res: any) => {
            this.allModelList = res.results;
            this.modelData = res.results;
          },
          err => {
            console.error(err);
            this.loading = false;
          });
      } else if (this.brandId) {
        this.commonService.getModelMasterByBrandIdWithSearchNoCat(this.brandId, this.categoryId, search.target.value).subscribe(
          (res: any) => {
            this.allModelList = res.results;
            this.modelData = res.results;
          },
          err => {
            console.error(err);
            this.loading = false;
          });
      } else {
        this.adminMasterService.getModelMaster(params).subscribe((res: any) => {
          if (res != null) {
            this.allModelList = res.results;
            this.modelData = res.results;
          }
        });
      }
    }
    else if (!search.target.value) {
      this.allModelList = [];
    }

  }

  keyDown(type) {
    if (type === 'category') {
      clearTimeout(this.categoryTypingTimer);
    } else if (type === 'brand') {
      clearTimeout(this.brandTypingTimer);
    } else {
      clearTimeout(this.modalTypingTimer);
    }
  }

  getCityData() {
    this.dashboardService.getCityMaster('limit=999').subscribe(
      (res: any) => {
        this.cityList = res.results;
        this.cityData = res.results;
      },
      (err) => {
        console.error(err);
        this.loading = false;
      }
    );
  }

  filterValuesState(search: any) {
    if (search && search.target.value && search.target.value.length > 2) {
      let params = 'limit=999&' + 'search=' + search.target.value;
      this.dashboardService.getCityMaster(params).subscribe(
        (res: any) => {
          this.cityList = res.results;
          this.cityData = res.results;
        },
        (err) => {
          console.error(err);
          this.loading = false;
        });
    }
    else if (!search.target.value) {
      this.cityList = [];
    }
  }

  onModalSelect(modal: any) {
    this.modalId = modal.id;
    this.modalName = modal.name
  }

  onCategorySelect(category: any) {
    this.categoryId = category.id
    this.categoryName = category.display_name
  }

  onBrandSelect(brand: any) {
    this.brandId = brand.id;
    this.brandName = brand.display_name
  }

  onStateSelect(city: any) {
    this.cityId = city.id;
    this.locationName = city.name
  }


  viewAllCategories() {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`]);
  }

  viewAllBrands() {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`]);
  }

  redirectByBrand(id, name) {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: 'brand=' + id } });
  }

  redirectByCategory(id, type, name) {
    if (type == 'Buy') {
      this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { filterBy: 'category=' + id } });
    } else {
      this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: 'category=' + id } });
    }
  }

  search() {
    if (!this.categoryId && !this.brandId && !this.modalId && !this.cityId && !this.homeForm.get('searchAll')?.value) {
      this.notify.error('Please select something to search');
      this.loading = false;
      return;
    }
    let string = '';
    if (this.categoryId) {
      string = string + 'category=' + this.categoryId;
    }
    if (this.brandId) {
      string = string + '&brand=' + this.brandId;
    }
    if (this.modalId) {
      string = string + '&model=' + this.modalId;
    }
    if (this.cityId) {
      string = string + '&location=' + this.cityId;
    }
    if (string) {
      if (this.searchType == 'New') {
        this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: string } });
      } else {
        this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { filterBy: string } });
      }
    } else {
      let value = this.homeForm.get('searchAll')?.value;
      if (this.searchType == 'New') {
        this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { search: value } });
      } else {
        this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { search: value } });
      }
    }
  }

  getBannerImgByLoc(loc) {
    for (let banner of this.bannerList) {
      if (banner.position == loc) {
        return banner.img_url;
      }
    }
  }
  getPostion(title, section) {
    if (section == 1) {
      return "Jumbotron " + title;
    } else {
      return "Marketing banner " + title + " -" + section;
    }
  }

  opencallpopup(text) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = text;
    const dialogRef = this.dialog.open(MakeACallComponent, dialogConfig);
  }
  redirectToScreen(link, banner) {
    if (link == 'raise-callback') {
      this.opencallpopup(this.getPostion(banner.banner_title, banner.section));
    } else {
      if (link.includes('/product/used-equipment/') || link.includes('/product/new-equipment/')) {
        if (link.includes('/product/used-equipment/')) {
          link = link.replace("/product/used-equipment/", "");
          this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
        } else {
          link = link.replace("/product/new-equipment/", "");
          this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
        }
      } else {
        if (!link.includes('http') || !link.includes('https')) {
          link = 'https://' + link;
        }
        window.open(link, '_blank');
      }
    }
  }

  redirectToEquipmentDetail(type, id) {
    if (type == 'New') {
      this.router.navigate(['/new-equipment-dashboard/equipment-details/' + id]);
    } else {
      this.router.navigate(['/used-equipment-dashboard/equipment-details/' + id]);
    }
  }

  searchByKeyWord(keyword) {
    if (keyword && keyword.length > 2) {
      this.dashboardService.searchByKeyword(keyword,this.searchType?.toLowerCase()).subscribe((res: any) => {
        this.autoSuggestList = res;
      })
    }
    else {
      this.autoSuggestList = [];
    }
  }

  onautoSelect(id, type, name) {
    if (type == 'category') {
      this.categoryId = id;
      this.categoryName = name;
    } else if (type == 'brand') {
      this.brandId = id;
      this.brandName = name;
    } else if (type == 'model') {
      this.modalId = id;
      this.modalName = name;
    }
  }

  addToWishlist(type, id) {
    if (this.cognitoId) {
      let request = {
        new_equipment: null,
        used_equipment: null,
        type: 2,
        user: this.cognitoId
      }
      if (type == 'new') {
        request.new_equipment = id;
      } else {
        request.used_equipment = id;
      }
      this.commonService.addToWatchlist(request).subscribe((res: any) => {
        this.getAllDetails();
      });
    }

  }

  removeFromWatchlist(type, id) {
    let queryParam =
      'asset_type=' + type +
      '&asset_id=' + id +
      '&cognito_id=' + this.cognitoId
    this.commonService.removeFromWatchlist(queryParam).subscribe((res: any) => {
      this.getAllDetails();
    });
  }

  getTimeLeft(endDate) {
    const end_date = new Date(endDate);
    const current_date = new Date();
    if (current_date > end_date) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      };
    } else {
      let totalSeconds = Math.floor((end_date.getTime() - (current_date.getTime())) / 1000);
      let totalMinutes = Math.floor(totalSeconds / 60);
      let totalHours = Math.floor(totalMinutes / 60);
      let totalDays = Math.floor(totalHours / 24);

      let hours = totalHours - (totalDays * 24);
      let minutes = totalMinutes - (totalDays * 24 * 60) - (hours * 60);
      let seconds = totalSeconds - (totalDays * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

      return {
        days: totalDays,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
              this.loading = false;
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!', false);
          this.loading = false;
        }
      }
      else {
        this.openSignInModal();
      }
    });
  }

  auctionBid(id, parentId) {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          this.checkInitialPayment(id, parentId).then((res) => {
            let payload = `auction_id=${id}&user_cognito_id=${this.cognitoId}&role='Customer'`;
            window.open(environment.auctionBaseUrl + "auction/integration/view-online-auction?" + payload, '_blank');
          });
        }
      });
    } else {
      this.openSignInModal();
    }
  }

  checkInitialPayment(auctionID, parentId) {
    return new Promise((resolved, reject) => {
      let payload = {
        cognito_id: this.cognitoId
      }
      this.paymentService.getEmdPaymentStatus(payload, (parentId ? parentId : auctionID)).subscribe(
        (res: any) => {
          if (res?.redirect) {
            resolved(true);
          } else {
            this.notify.error('You have not made the requisite EMD payment to participate in this auction. Please click Register Now button to make the necessary payment.');
            this.loading = false;
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
          this.loading = false;
        }
      );
    });
  }

  checkEmdAvailability(auctionID) {
    return new Promise((resolved, reject) => {
      this.auctionService.getEmdValues(auctionID).subscribe(
        (res: any) => {
          if (res?.count) {
            resolved(true);
          } else {
            this.notify.error('Don"t have emd against this auction, Please contact admin !!');
            this.loading = false;
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
          this.loading = false;
        }
      );
    });
  }

  getTechTitle(str, unit) {
    if (str == 'enginePower') {
      return str + ' cc';
    } else if (str == 'operatingWeight') {
      return str + ' kg ';
    }
    else if (str == 'bucketCapcity') {
      return str + ' kg ';
    } else if (str == 'grossWeight') {
      return str + ' kg ';
    } else if (str == 'liftingCapcity') {
      return str + ' kg';
    } else {
      let Uom = unit == undefined || unit == null ? '' : unit
      return str + Uom;
    }
  }

  showAuctionDetails(id, auctionEngineId) {
    this.router.navigate(['/auctions/auction-detail/' + id + '/' + auctionEngineId]);
  }

  openSignInModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Admin')) {
          localStorage.setItem("userRole", 'Admin');
        }
        else if (rolesArray.includes('SuperAdmin')) {
          localStorage.setItem("userRole", 'SuperAdmin');
        }
        else if (rolesArray.includes('ChannelPartner')) {
          localStorage.setItem("userRole", 'ChannelPartner');
        }

        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
          this.storage.setStorageData("UserInfo", res, true);
        });

      }
    })
  }

  onFeaturedTabChanged(event) {
    this.currentFeaturedTab = event?.index;
  }

  viewProductListing() {
    let string = '';
    let id;
    if (this.currentFeaturedTab == 0) {
      string = 'certificate=Featured';
      this.router.navigate(['/new-equipment-dashboard/equipment-list'], { queryParams: { filterBy: string } });
    } else {
      string = 'certificate=Featured';
      this.router.navigate(['/used-equipment-dashboard/equipment-list'], { queryParams: { filterBy: string } });
    }
  }

  onRegisterNow(data) {
    if (this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        this.checkAuthentication().then((isAuthorise) => {
          if (isAuthorise) {
            const params = {
              bidder__cognito_id__in: this.cognitoId
            }
            this.auctionService.getAuctionEsign(params).subscribe(
              (res: any) => {
                this.eSignReqData = res?.results as any[];
                this.esignStatus = (res && res?.results && (res?.results.length > 0) && (res?.results[0]?.status)) ? res?.results[0]?.status : '';
                //  this.esignStatus = 'BUS';
                if (this.esignStatus != 'BUS') {
                  const dialogRef = this.dialog.open(ESignConfirmDialogComponent, {
                    width: '500px',
                    data: {
                      //auction: this.auctionDataById.id,
                      bidder: this.cognitoId,
                      returnUrl: this.router.url
                    }
                  });
                }
                else {
                  this.auctionRegister(data);
                }
              });
          }
        });
      }
      else if (rolesArray.includes('Admin') || rolesArray.includes('SuperAdmin') || rolesArray.includes('ChannelPartner')) {
        const dialogRef = this.dialog.open(CustomerSearchModalComponent, {
          width: '600px',
          data: {
            type: 'Auction',
            data: data,
            heading: "Register for Auction",
            subHeading: "Does the user already exist on iQuippo?"
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result && result?.cognitoId) {
            this.checkCustomerKYC(result?.cognitoId).then((isAuthorise) => {
              if (isAuthorise) {
                const params = {
                  bidder__cognito_id__in: result?.cognitoId
                }
                this.auctionService.getAuctionEsign(params).subscribe(
                  (res: any) => {
                    this.eSignReqData = res?.results as any[];
                    this.esignStatus = res && res?.results && res?.results.length > 0 && res?.results[0].status ? res?.results[0].status : '';
                    if (this.eSignReqData && this.eSignReqData.length > 0) {
                      //this.esignStatus = 'BUS';
                      if (this.esignStatus == 'BUS') {
                        this.assistedMode = true;
                        this.auctionRegister(data, result?.cognitoId);
                      }
                      else {
                        this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
                      }
                    }
                    else {
                      this.initiateBUForm(result.cognitoId)
                    }
                  });
              }
            });
          }
        });
      }
      else {
        this.notify.error('Please login as Customer to proceed further with asset buy!!');
        this.loading = false;
      }
    }
    else {
      this.openSignInModal();
    }
  }

  initiateBUForm(cognitoId) {
    const params = {
      role_type: 1,
      bidder: cognitoId,
    }
    this.auctionService.postAuctionEsign(params).subscribe(
      (res: any) => {
        this.notify.success('The Bidder Undertaking of this customer has been successfully initiated.');
        setTimeout(() => {
          if (res && res?.status == 'EP') {
            this.patchESignReq(res?.reference_id);
          }
          else if (res && (res?.status == 'INI' || res?.status == 'UNS' || res?.status == 'TC')) {
            this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
          }
        }, 2000);
      }, err => {
        this.loading = false;
      });
  }

  patchESignReq(referenceId) {
    const params = {
      reference_id: referenceId,
      page_url: environment.mp1HomePage + 'customer/dashboard/e-sign-template/'
    }
    this.auctionService.patchAuctionEsign(params, referenceId).subscribe(
      (res: any) => {
        this.notify.success('The mail has been successfully sent to the customer!!');
      });
  }

  checkCustomerKYC(cognitoId) {
    return new Promise((resolved, reject) => {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      let payload = {
        cognito_id: cognitoId,
        role: 'Customer'
      }
      this.commonService.validateSeller(payload).subscribe(
        (res: any) => {
          if (!res.kyc_verified || !res.pan_address_proof) {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
              width: '500px',
              disableClose: true,
              data: {
                heading: 'KYC Details Incomplete',
                message: rolesArray.includes('ChannelPartner') ? 'Please ask user to complete the KYC in their My profile section' : 'You need to complete user KYC profile with iQuippo to make payments.',
                submessage: rolesArray.includes('ChannelPartner') ? '' : 'Click Yes if you want to proceed or No if you want to do this later. ',
                isRoleChannelPartner: rolesArray.includes('ChannelPartner') ? true : false
              }
            });

            dialogRef.afterClosed().subscribe(val => {
              if (val) {
                let rolesArray = this.storage.getStorageData("rolesArray", false);
                rolesArray = rolesArray.split(',');
                if (rolesArray.includes('Admin')) {
                  localStorage.setItem("userRole", 'Admin');
                }
                else if (rolesArray.includes('SuperAdmin')) {
                  localStorage.setItem("userRole", 'SuperAdmin');
                }
                else if (rolesArray.includes('ChannelPartner')) {
                  localStorage.setItem("userRole", 'ChannelPartner');
                }
                this.cognitoId = this.storage.getStorageData("cognitoId", false);
                this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
                  this.storage.setStorageData("UserInfo", res, true);
                });

                this.router.navigate(['/admin-dashboard/customer-management']);
              }
            })
            resolved(false);
            return;
          } else {
            resolved(true);
          }
        },
        err => {
          reject(false);
        }
      );

    });
  }

  auctionRegister(data, assistedCognitoId?) {
    this.checkEmdAvailability(data?.id).then((res) => {
      let paymentDetails = {
        auction_id: data?.id,
        asset_type: 'Used',
        case: 'Auction',
        payment_type: 'Partial',
        assistedMode: this.assistedMode
      }
      if (this.assistedMode) {
        paymentDetails['assistedCognitoId'] = assistedCognitoId;
      }
      this.storage.setSessionStorageData('paymentSession', true, false);
      this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
      this.router.navigate(['/customer/payment']);
    },
      err => {
        console.log(err);
        this.loading = false;
      }
    );
  }

  onLoad() {
    this.loading = false;
  }
  getTitle(str) {
    return str.replace(" ", "<br/>");
  }
}
