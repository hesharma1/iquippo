import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { bool } from 'aws-sdk/clients/signer';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Component({
  selector: 'app-landing-page-dashboard',
  templateUrl: './landing-page-dashboard.component.html',
  styleUrls: ['./landing-page-dashboard.component.css']
})
export class LandingPageDashboardComponent implements OnInit {
  userRole: any;
  cognitoId: any;
  isChange:boolean = true;
  constructor(private router: Router, private storage: StorageDataService,public apiRouteService:ApiRouteService,private commonService:CommonService) { }
  role:any;
  rolesArray:string[] = [];
  ngOnInit(): void {
    if(this.isChange) {
    this.role = this.storage.getStorageData("role", false);
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    if(this.rolesArray.includes(this.apiRouteService.roles.rm)){
      localStorage.setItem("userRole", this.apiRouteService.roles.rm);
      this.userRole = this.apiRouteService.roles.rm;
      this.router.navigate(['/admin-dashboard']);
      this.getUserRoles();
    }
    else if(this.rolesArray.includes('Customer') || this.rolesArray.includes('Manufacturer')
    || this.rolesArray.includes('EnterprisePartner') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Guest')|| this.rolesArray.includes('PartnerGuest')
    ){
      this.router.navigate(['/customer/dashboard']);
    }else if(this.rolesArray.includes('Admin') || this.rolesArray.includes('SuperAdmin')){
      this.userRole = this.rolesArray.includes('SuperAdmin') ? this.apiRouteService.roles.superadmin: this.apiRouteService.roles.admin;
      localStorage.setItem("userRole", this.userRole);
      this.router.navigate(['/admin-dashboard']);
      if(this.role==this.apiRouteService.roles.admin)
      this.getUserRoles();
    }else{
      this.router.navigate(['/customer/dashboard']);
    }
    this.isChange = false;
  }

  }
  getUserRoles() {
    if(this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.rm) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
    this.commonService.getRolesByUser(this.cognitoId).subscribe(res =>{
      this.storage.setStorageData("UserInfo", res, true);
      this.commonService.shareData(res);
      this.router.navigate(['/admin-dashboard']);

    })
    }
  }
}
