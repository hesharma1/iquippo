import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TermsAndConditionsComponent>, public changeDetect: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  backClick(){
    this.dialogRef.close();
    this.changeDetect.detectChanges();
  }
}
