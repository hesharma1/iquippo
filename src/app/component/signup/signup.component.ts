import { Component, OnInit, ViewEncapsulation , ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validator, Validators} from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { OtpSubmitRequest, OtpSubmitResponse } from 'src/app/models/common/otpcheck.model';
import { OtpSentRequest, OtpSentResponse } from 'src/app/models/common/otpsent.model';
import { CountryResponse, ResponseData } from 'src/app/models/common/responsedata.model';
import { AccountService } from 'src/app/services/account';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatDialog } from '@angular/material/dialog';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { environment } from 'src/environments/environment';
import {numberonly} from '../../utility/custom-validators/forgot-password';
import { VerifyUser, VerifyUserResponse } from 'src/app/models/verifyUserRequest.model';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { SharedService } from 'src/app/services/shared-service.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class SignupComponent implements OnInit {
  private CookieValue: string = '';
  otpResponse? : OtpSentResponse
  otpSubmitResponse?: OtpSubmitResponse;
  isSubmitted:boolean=false;
  isSubmittedPhone:boolean=false;
  signupSocial:boolean=false;
  resendOTP: boolean = false;
  remainingTime: any;
  googleResponse: any;
  response: any;
  isCheckedTermsCondition: boolean=false;
  assetValuatedFlag: boolean=false;
  google_data:any;
  countryList?: any;
  selectedCity?:CountryResponse;
  firstName?:string;
  lastName?:string;
  isGetOtpbtnHidden:boolean=false;
  maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  fullName?: string;
  returnUrl = '';
  constructor(private cookieService: CookieService,private fb:FormBuilder,public sharedService:SharedService,public accountService:AccountService,private authService:SocialAuthService, private router: Router, private storage:StorageDataService, public dialog: MatDialog
    , private activatedRoute: ActivatedRoute,private spinner:NgxSpinnerService, private appRouteEnum: AppRouteEnum,private notificationservice:NotificationService) {
    this.otpResponse= new OtpSentResponse(); 
   }
  @ViewChild('otpField2') otp2?: ElementRef;
  @ViewChild('otpField3') otp3?: ElementRef;
  @ViewChild('otpField4') otp4?: ElementRef;
  @ViewChild('otpField5') otp5?: ElementRef;
  @ViewChild('otpField6') otp6?: ElementRef;
  @ViewChild('otpField1') otp1?: ElementRef;
  ngOnInit(): void {
    
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
      this.returnUrl = params.returnUrl;
    });
    // var isLoggedIn = this.sharedService.getCookie(this.appRouteEnum.globalCookieName);
    // if (isLoggedIn != null && isLoggedIn != '' && isLoggedIn != undefined && isLoggedIn == this.appRouteEnum.globalCookieValue) {
    //    window.location.href = environment.mp1HomePage;
    // }
    this.bindFlags();
    if(!history.state.isSocialLogin){
      this.storage.cleanAll();
    }
      let google_data=this.storage.getStorageData("google_data",true);
    // console.log(google_data);
     if(google_data!=undefined){
       this.signupSocial =true;
       
    }
     
  }
  statuscodevar?: any = '';
  otpcodevar?: any = '';
// phoneNumberResponse?: String='';
// sessionID?:string;

otpstatus: boolean = true;

getotpForm=this.fb.group({
  phone: new FormControl('',[Validators.required /*, Validators.minLength(10), Validators.maxLength(10)*/]),
  countrycode: new FormControl (),
}
,{validator: [numberonly('phone')]}
);

checkPhoneField() {
  
  var prefixCode = this.getotpForm.get('countrycode')?.value.countryCode;
  var mobile = this.getotpForm.get('phone')?.value;
  if(prefixCode != null && prefixCode == 'IN')
  {
    if(mobile.length >= 10 && mobile.length <= 14)
    {
     return true;
    }
    else{
      this.getotpForm.controls['phone']?.setErrors({'incorrect': true});
      return false;
    }
  }
  else{
    return true;
  }    
}
getotpfn(){
 
 //this.spinner.show();
  let OtpSentRequestModel =new OtpSentRequest();
  OtpSentRequestModel.phoneNumber=this.getotpForm.get('countrycode')?.value.prefixCode + this.getotpForm.get('phone')?.value;
  this.accountService.otpValidator(OtpSentRequestModel).subscribe(Response=>{
    //  console.log(Response)
    
    this.isSubmittedPhone=true;
    var getResponse= Response as ResponseData;
    var getResponseBody = getResponse.body;
    var getResponseBodyJSON = JSON.parse(getResponseBody);
    this.otpResponse= new OtpSentResponse();
    this.otpResponse.phoneNumber =OtpSentRequestModel.phoneNumber;
    this.otpResponse.sessionId=getResponseBodyJSON.session;
   // 
    this.statuscodevar = getResponse.statusCode;
    this.resendOTP = false;
    this.remainingTime = environment.remainingTime;
    this.startCountdown(environment.resendOtpTime);
    this.isGetOtpbtnHidden=true;
     
    // if(this.statuscodevar=='229'){
    //   alert();
    // };
  });
}

startCountdown(seconds:number) {
  let counter = seconds;
  let minutes, sec, finalTime;
 // let elem = document.getElementById('resend');

  const interval = setInterval(() => {
   // console.log(counter);
    counter--;
      
    if (counter < 0 ) {
      clearInterval(interval);
      this.resendOTP = true;
      // console.log(this.resendOTP);
    }

    minutes = Math.floor(counter / 60);
    sec = counter - minutes * 60;
    finalTime = this.str_pad_left(minutes,'0',2)+':'+ this.str_pad_left(sec,'0',2);
    this.remainingTime = finalTime;

  }, 1000);
}

str_pad_left(str: number ,pad: string, length: number) {
  return (new Array(length+1).join(pad)+str).slice(-length);
}

resentOtp(){
  //this.spinner.show();
  let OtpSentRequestModel =new OtpSentRequest();
  OtpSentRequestModel.phoneNumber=this.getotpForm.get('phone')?.value;
  this.accountService.otpValidator(OtpSentRequestModel).subscribe(Response=>{
    // console.log(Response)

    this.resendOTP = false;
    this.remainingTime = environment.remainingTime;
    this.startCountdown(environment.resendOtpTime);
   
  });
}

submitOtp=this.fb.group({
  otpField1: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  otpField2: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  otpField3: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  otpField4: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  otpField5: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  otpField6: new FormControl('',[Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
  agree: new FormControl('',[Validators.required]),
}
,{validator: [numberonly('otpField1'),numberonly('otpField2'),numberonly('otpField3'),numberonly('otpField4'),numberonly('otpField5'),numberonly('otpField6')]
});

submitotpfn(){
  //this.spinner.show();
  this.isSubmitted=true;
  let otpSubmitRequestModel =new OtpSubmitRequest();
  otpSubmitRequestModel.phoneNumber=this.otpResponse?.phoneNumber;
  otpSubmitRequestModel.sessionId=this.otpResponse?.sessionId;
  otpSubmitRequestModel.otp=this.submitOtp.get('otpField1')?.value + '' +this.submitOtp.get('otpField2')?.value + '' + '' +this.submitOtp.get('otpField3')?.value + '' + '' +this.submitOtp.get('otpField4')?.value + '' + '' +this.submitOtp.get('otpField5')?.value + '' + '' +this.submitOtp.get('otpField6')?.value;
 // 
  this.accountService.otpValidator(otpSubmitRequestModel).subscribe((Response:any)=>{
    // console.log(Response)
    let otpresponsecode= Response as ResponseData;
    var getResponseBody = otpresponsecode.body;
    var getResponseBodyJSON = JSON.parse(getResponseBody);
    this.otpcodevar = otpresponsecode.statusCode;
  //  
    if(otpresponsecode.statusCode==220){
      

      // start - saving UID to cookie

      var d = new Date();
      d.setTime(d.getTime() + environment.ExpiryTimeInMS);
  
      this.cookieService.set(
        "userData",
        this.response,
        d
      );
      //this.CookieValue = this.cookieService.get("UID");
      //alert(this.CookieValue);

      // End - saving UID to cookie



      localStorage.setItem("prefixCode",this.getotpForm.get('countrycode')?.value.prefixCode);
      localStorage.setItem("countryCode",this.getotpForm.get('countrycode')?.value.countryCode);
      localStorage.setItem("phone",this.getotpForm.get('phone')?.value);
      // this.storage.clearStorageData("userData");
      // this.storage.setStorageData("userData", Response, true);
      // this.storage.clearStorageData("cognitoId");
      //     this.storage.setStorageData('cognitoId', Response.username,false);
      if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
        var pro = this.sharedService.queryParams.aoc;
        var src = this.sharedService.queryParams.src;
        if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
          this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
        }
        else if (pro != undefined && pro != null && pro == "qtt") {
          this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
        }
        else if (src != undefined && src != null) {
          this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { src: this.sharedService.queryParams.src } });
        }
        else {
          this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: {returnUrl:this.returnUrl}});
        }
      } else {
        this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: {returnUrl:this.returnUrl}});
      }
    }
    if(otpresponsecode.statusCode==260){
      this.otpstatus = !this.otpstatus;    
   
    };
    if(otpresponsecode.statusCode==213){
      this.otpstatus = !this.otpstatus; 
   

    };
    if(otpresponsecode.statusCode==500){
      this.otpstatus = !this.otpstatus;   
   

    };
  });

}
termsConditions(){
  const dialogRef = this.dialog.open(TermsAndConditionsComponent, {
    width: '900px',
  });
}
bindFlags() {
  this.accountService.getCountryDetails().subscribe(response=>{
    
   var countries = response as any;
   this.countryList=countries.body;
  this.selectedCity = this.countryList[0];
   this.getotpForm.get('countrycode')?.setValue(this.countryList[0]);
  //  this.getotpForm.get('countrycode')?.setValue(this.countryList[0]);
  },err=>{
    this.notificationservice.error(err);
  })
}
updateCountryCode(city:any){
  this.getotpForm.get('countrycode')?.setValue(city);
  this.getotpForm.get('phone')?.setValue('')

  if(city?.prefixCode == '+91' && city?.countryCode == 'IN'){
    this.maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  } else {
    this.maxLengthValue = this.appRouteEnum.otherPhoneNumber;
  }
}
signinredirect(){
  if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
    var pro = this.sharedService.queryParams.aoc;
      var src = this.sharedService.queryParams.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.sharedService.queryParams.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.Login]);
      }
  } else {
    this.router.navigate([`./` + this.appRouteEnum.Login]);
  }
}
// myfun(val:string){
//   // console.log(val);
//   // val.
//   // @ts-ignore
//   // val.nextSibling.focus();
//   switch (val) {
//     case '1':
//       if(this.submitOtp.get('otpField1')?.value!=''&&this.submitOtp.get('otpField1')?.value!=null&& this.submitOtp.get('otpField1')?.value!=undefined){
//         this.otp2?.nativeElement.focus();            
//       }
//       break;
//       case '2':
//         if(this.submitOtp.get('otpField2')?.value!=''&&this.submitOtp.get('otpField2')?.value!=null&& this.submitOtp.get('otpField2')?.value!=undefined){
//           this.otp3?.nativeElement.focus();             
//         }    
//       break;
//       case '3':
//         if(this.submitOtp.get('otpField3')?.value!=''&&this.submitOtp.get('otpField3')?.value!=null&& this.submitOtp.get('otpField3')?.value!=undefined){
//           this.otp4?.nativeElement.focus();             
//         }    
//       break;
//       case '4':
//         if(this.submitOtp.get('otpField4')?.value!=''&&this.submitOtp.get('otpField4')?.value!=null&& this.submitOtp.get('otpField4')?.value!=undefined){
//           this.otp5?.nativeElement.focus();             
//         }    
//       break;
//       case '5':
//         if(this.submitOtp.get('otpField5')?.value!=''&&this.submitOtp.get('otpField5')?.value!=null&& this.submitOtp.get('otpField5')?.value!=undefined){
//           this.otp6?.nativeElement.focus();             
//         }    
//       break;
//     default:
//       break;
//   }

// }
myfun(val: string) {
  // console.log(val);
  // val.
  // @ts-ignore
  // val.nextSibling.focus();
  this.isSubmitted=false;
  
  switch (val) {
    case '1':
      if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
        this.otp2?.nativeElement.focus();
        this.otp2?.nativeElement.select();
      }
      else{
        this.otp1?.nativeElement.focus();
        this.otp1?.nativeElement.select();
      }
      break;
    case '2':
      if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
        this.otp3?.nativeElement.focus();
        this.otp3?.nativeElement.select();
      }
      else{
        this.otp1?.nativeElement.focus();
        this.otp1?.nativeElement.select();
      }
      break;
    case '3':
      if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
        this.otp4?.nativeElement.focus();
        this.otp4?.nativeElement.select();
      }
      else{
        this.otp2?.nativeElement.focus();
        this.otp2?.nativeElement.select();
      }
      break;
    case '4':
      if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
        this.otp5?.nativeElement.focus();
        this.otp5?.nativeElement.select();
      }
      else{
        this.otp3?.nativeElement.focus();
        this.otp3?.nativeElement.select();
      }
      break;
    case '5':
      if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
        this.otp6?.nativeElement.focus();
        this.otp6?.nativeElement.select();
      }
      else{
        this.otp4?.nativeElement.focus();
        this.otp4?.nativeElement.select();
      }
      break;
    case '6':
      if (this.submitOtp.get('otpField6')?.value != '' && this.submitOtp.get('otpField6')?.value != null && this.submitOtp.get('otpField6')?.value != undefined) {
        this.otp6?.nativeElement.focus();
      }
      else{
        this.otp5?.nativeElement.focus();
        this.otp5?.nativeElement.select();
      }
      break;
    case '11':
      if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
        this.otp1?.nativeElement.select();
        this.otp1?.nativeElement.focus();
      }
      break;
    case '22':
      if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
        this.otp2?.nativeElement.select();
        this.otp2?.nativeElement.focus();
      }
      break;
    case '33':
      if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
        this.otp3?.nativeElement.select();
        this.otp3?.nativeElement.focus();
      }
      break;
    case '44':
      if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
        this.otp4?.nativeElement.select();
        this.otp4?.nativeElement.focus();
      }
      break;
    case '55':
      if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
        this.otp5?.nativeElement.select();
        this.otp5?.nativeElement.focus();
      }
      break;
    
      default:
      break;
  }

}
signInWithGoogle(){
  this.sharedService.signInWithGoogleAndFacebook(GoogleLoginProvider.PROVIDER_ID,this.assetValuatedFlag); 
  
  let google_data=this.storage.getStorageData("google_data",true);
    // console.log(google_data);
     if(google_data!=undefined){
       this.signupSocial =true;
    }

}
signInWithFB(){
  this.sharedService.signInWithGoogleAndFacebook(FacebookLoginProvider.PROVIDER_ID,this.assetValuatedFlag);
  let google_data=this.storage.getStorageData("google_data",true);
    // console.log(google_data);
     if(google_data!=undefined){
       this.signupSocial =true;
    }
}
// signInWithGoogle(): void {

//   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((x) => {
//     this.googleResponse = x;
//     // 
//     this.storage.clearStorageData('google_data');
//     this.storage.setStorageData("google_data", this.googleResponse, true);
//     var verifyUserRequestModel = new VerifyUser();
//     verifyUserRequestModel.socialId = this.googleResponse.authToken;
//     this.accountService.getDetails(verifyUserRequestModel).subscribe(res => {
//     //  
//       // console.log(res);
//       var getResponse = res as VerifyUserResponse;
//       this.response = res as any;
//       this.sharedService.getLoggedInName.emit(true);
//       // var getResponseBody = getResponse.body;
//       // var getResponseBodyJSON = JSON.parse(getResponse);

//       if (getResponse.statusCode == '301') {
//         this.storage.setStorageData('socialId', this.googleResponse.authToken, true);
//         this.router.navigate([`./register`]);
//         //this.otpSection =false;
//       }
//       if (getResponse.statusCode == '300') {
//         this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken);
//       }
//     })
//     //this.router.navigate([`./dashboard`]);

//     // console.log(this.googleResponse);
//   }).catch((err) => {
//     console.log(err)
//   })

// }

// signInWithFB(): void {
//  // 
//  const fbLoginOptions = {
//   scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
//   return_scopes: true,
//   enable_profile_selector: true
// }
//   this.authService
//     .signIn(FacebookLoginProvider.PROVIDER_ID,fbLoginOptions).then((x) => {
//       this.googleResponse = x;
//       this.storage.clearStorageData('google_data');
//       // 
//       this.storage.setStorageData("google_data", this.googleResponse, true);
//       var verifyUserRequestModel = new VerifyUser();
//       verifyUserRequestModel.socialId = this.googleResponse.authToken;
//       this.accountService.getDetails(verifyUserRequestModel).subscribe(res => {
//         // 
//         //  console.log(res);
//         this.sharedService.getLoggedInName.emit(true);
//         var getResponse = res as VerifyUserResponse;
//         this.response = res as any;
//         // var getResponseBody = getResponse.body;
//         // var getResponseBodyJSON = JSON.parse(getResponse);

//         if (getResponse.statusCode == '301') {
//           this.storage.setStorageData('socialId', this.googleResponse.authToken, true);
//           this.router.navigate([`./register`]);

//           //this.otpSection =false;
//         }
//         if (getResponse.statusCode == '300') {
//           this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken);
//         }

//       })
//       //this.router.navigate([`./dashboard`]);

//       // console.log(this.googleResponse);
//     }).catch((err) => {
//       console.log(err)
//     })
// }
onTermConditionChange($event:any) {
  this.isCheckedTermsCondition = $event.checked;
  if(!this.isCheckedTermsCondition)
  this.submitOtp.get('agree')?.setErrors({ isAgree: true});
}
}