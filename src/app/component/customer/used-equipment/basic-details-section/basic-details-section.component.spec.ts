import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicDetailsSectionComponent } from './basic-details-section.component';

describe('BasicDetailsComponent', () => {
  let component: BasicDetailsSectionComponent;
  let fixture: ComponentFixture<BasicDetailsSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasicDetailsSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicDetailsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
