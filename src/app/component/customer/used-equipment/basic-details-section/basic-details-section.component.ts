import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';

// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};

@Component({
  selector: 'app-basic-details-section',
  templateUrl: './basic-details-section.component.html',
  styleUrls: ['./basic-details-section.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class BasicDetailsSectionComponent implements OnInit {
  public basicDetailsForm: FormGroup;
  public clicked: any;
  public countryList: any;
  public selectedCity?: CountryResponse;
  public maxLengthValue: number = this.appRouteEnum.indiaPhoneNumber;;
  equipmentDetails;
  stateList: any = [];
  fuelType: { [key: number]: string } = {
    1: "Diesel",
    2: "Petrol",
    3: "CNG"
  }
  todayDate = new Date();
  vaahanDetailsEditable: any = {};
  constructor(private router: Router, public accountService: AccountService, private notificationservice: NotificationService, public appRouteEnum: AppRouteEnum, public spinner: NgxSpinnerService, private agreementServiceService: AgreementServiceService,
    private fb: FormBuilder) {
    this.basicDetailsForm = this.fb.group({
      registrationNumber: new FormControl('', [Validators.maxLength(25)]),
      chassisNumber: new FormControl('', [Validators.minLength(2), Validators.maxLength(25)]),
      engineNumber: new FormControl('', [Validators.minLength(2), Validators.maxLength(25)]),
      machineSerialNumber: new FormControl('', [Validators.minLength(2), Validators.maxLength(25)]),
      enginePower: new FormControl('', [Validators.minLength(2), Validators.maxLength(10)]),
      grossWeight: new FormControl('', [Validators.minLength(2), Validators.maxLength(10)]),
      operationWeight: new FormControl('', [Validators.minLength(2), Validators.maxLength(10)]),
      bucketCapacity: new FormControl('', [Validators.minLength(2), Validators.maxLength(10)]),
      liftingCapacity: new FormControl('', [Validators.minLength(2), Validators.maxLength(10)]),
      fuelType: new FormControl(''),
      rcCopyAvailable: new FormControl('', Validators.required),
      isFirstOwned: new FormControl('', Validators.required),
      yearOfManufacture: new FormControl('', Validators.required),
      customerReferenceNo: new FormControl(''),
      //SellPrice: new FormControl(''),
      pincode: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
      address: new FormControl('', Validators.required),
      countryCode: new FormControl('+91', Validators.required),
      // email: new FormControl('', [Validators.required,Validators.email]),
      alternateEmail: new FormControl('', [Validators.email]),
      contactNumber: new FormControl('', Validators.compose([Validators.pattern('[1-9][0-9]+'),
      Validators.minLength(10), Validators.maxLength(10)])),
      description: new FormControl('')
    }, { validator: [numberonly('enginePower'), numberonly('grossWeight'), numberonly('operationWeight'), numberonly('operationWeight'), numberonly('liftingCapacity')] });
  }

  ngOnInit(): void {
    this.bindFlags();
    this.getLocationData();
    //this.equipmentDetails = localStorage.getItem('sellequipmentData');
    console.log(this.vaahanDetailsEditable)
  }


  patchValue(equipmentDetails) {
    this.equipmentDetails = equipmentDetails;
    if (this.equipmentDetails != null) {
      this.vaahanDetailsEditable.chassisNumber = this.equipmentDetails.is_chassis_number_from_vaahan ? this.equipmentDetails.is_chassis_number_from_vaahan : false;
      this.vaahanDetailsEditable.engineNumber = this.equipmentDetails.is_engine_number_from_vaahan ? this.equipmentDetails.is_engine_number_from_vaahan : false;
      this.vaahanDetailsEditable.enginePower = this.equipmentDetails.is_engine_power_from_vaahan ? this.equipmentDetails.is_engine_power_from_vaahan : false;
      this.vaahanDetailsEditable.fuelType = this.equipmentDetails.is_fuel_type_from_vaahan ? this.equipmentDetails.is_fuel_type_from_vaahan : false;
      this.vaahanDetailsEditable.registrationNumber = this.equipmentDetails.is_rc_available ? this.equipmentDetails.is_rc_available : false;
      this.vaahanDetailsEditable.grossWeight = this.equipmentDetails.grossWeight ? true : false;
      this.vaahanDetailsEditable.operationWeight = this.equipmentDetails.operationWeight ? true : false;
      this.vaahanDetailsEditable.bucketCapacity = this.equipmentDetails.bucketCapacity ? true : false;
      this.vaahanDetailsEditable.liftingCapacity = this.equipmentDetails.liftingCapacity ? true : false;
      this.vaahanDetailsEditable.equipmentRegistered = this.equipmentDetails.is_equipment_registered ? this.equipmentDetails.is_equipment_registered : false;
      if (this.equipmentDetails.is_rc_available) {
        this.basicDetailsForm.get('registrationNumber')?.setValidators(Validators.required);
        this.basicDetailsForm.get('registrationNumber')?.updateValueAndValidity();

      } else {
        this.basicDetailsForm.get('registrationNumber')?.clearValidators();
        this.basicDetailsForm.get('registrationNumber')?.updateValueAndValidity();

      }
      // if(this.vaahanDetailsEditable.equipmentRegistered) {
      //   this.basicDetailsForm.get('engineNumber')?.clearValidators();
      // this.basicDetailsForm.get('engineNumber')?.updateValueAndValidity();
      // } else {
      //   this.basicDetailsForm.get('engineNumber')?.setValidators(Validators.required);
      //   this.basicDetailsForm.get('engineNumber')?.updateValueAndValidity();
      // }
      this.basicDetailsForm.patchValue({
        registrationNumber: this.equipmentDetails.rc_number,
        chassisNumber: this.equipmentDetails.chassis_number,
        engineNumber: this.equipmentDetails.engine_number,
        machineSerialNumber: this.equipmentDetails.machine_sr_number,
        enginePower: this.equipmentDetails.engine_power,
        grossWeight: this.equipmentDetails.gross_Weight,
        operationWeight: this.equipmentDetails.operating_weight,
        bucketCapacity: this.equipmentDetails.bucket_capacity,
        liftingCapacity: this.equipmentDetails.lifting_capacity,
        fuelType: this.equipmentDetails.fuel_type,
        rcCopyAvailable: this.equipmentDetails.is_rc_available ? "1" : "2",
        isFirstOwned: this.equipmentDetails.is_first_owned ? "1" : "0",
        yearOfManufacture: this.equipmentDetails.mfg_year,
        customerReferenceNo: this.equipmentDetails.customer_reference_number,
        pincode: this.equipmentDetails.pin_code.pin_code,
        address: this.equipmentDetails.asset_address,
        countryCode: '+91',
        // email: this.equipmentDetails.email_address,
        alternateEmail: this.equipmentDetails.alt_email_address,
        contactNumber: this.equipmentDetails.alt_contact_number,
        description: this.equipmentDetails.asset_description,
        //SellPrice: this.equipmentDetails.selling_price,

      });
      if (this.equipmentDetails.mfg_year) {
        let date = new Date();
        date.setFullYear(this.equipmentDetails.mfg_year);
        this.basicDetailsForm.get('yearOfManufacture')?.setValue(date);
      } if (this.equipmentDetails.pin_code.pin_code) {
        this.validatePincode();
        this.basicDetailsForm.get('pincode')?.clearValidators()
        this.basicDetailsForm.get('pincode')?.updateValueAndValidity()
      }

    }
  }



  submit() {
    console.log('formsubmitted');
  }

  isRequiredField(field: string) {
    const form_field: any = this.basicDetailsForm.get(field);
    if (!form_field.validator) {
      return false;
    }
    const validator = form_field.validator({} as AbstractControl);
    return (validator && validator.required);
  }

  checkSellingPrice() {
    alert("have you entered selling price =" + this.basicDetailsForm.get('SellPrice')?.value)
  }

  bindFlags() {
    //this.spinner.show();
    this.accountService.getCountryDetails().pipe(finalize(() => { })).subscribe(
      (response: any) => {
        this.countryList = response.body;
        this.basicDetailsForm.get('countryCode')?.setValue(this.countryList[0].prefixCode);
        this.selectedCity = this.countryList[0];
      }, err => {
        this.notificationservice.error(err);
      }
    )
  }

  updateCountryCode(city: any) {
    this.basicDetailsForm.get('countryCode')?.setValue(city);
    this.basicDetailsForm.get('contactNumber')?.setValue('');

    if (city?.prefixCode == '+91' && city?.countryCode == 'IN') {
      this.maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
    } else {
      this.maxLengthValue = this.appRouteEnum.otherPhoneNumber;
    }
  }

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.basicDetailsForm.get('yearOfManufacture');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.basicDetailsForm?.get('yearOfManufacture')?.setValue(ctrlValue?.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }

  /*** get state data*/
  getLocationData() {
    let pincode = this.basicDetailsForm?.get('pincode')?.value;
    if (pincode && pincode.length > 4) {
      let queryParam = `search=${pincode}`;
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
          this.stateList = res.results;
          if (this.stateList.length > 0) { } else {
            this.basicDetailsForm?.get('pincode')?.setValue('')
          }
        },
        (err) => {
          console.error(err);
        }
      );
    }
  }
  validatePincode() {
    let pincode = this.basicDetailsForm?.get('pincode')?.value;
    if (this.stateList) {
      let pinvalid = this.stateList.filter(e => e.pin_code == pincode)
      if (pinvalid?.length > 0) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  onKeydownEvent(event: KeyboardEvent) {
    let value = this.basicDetailsForm?.get('bucketCapacity')?.value;
    if(value?.length>0) {
      let data = value.split(".")
      var preDecimal = data[0];
      var postDecimal = data[1];
  
      if (preDecimal?.length > 3 || postDecimal?.length > 2) {
        this.notificationservice.error("please enter 3 digits before decimal and 2 digits after decimal.");
        this.basicDetailsForm?.get('bucketCapacity')?.setErrors({ required: true });
      } else {
        this.basicDetailsForm?.get('bucketCapacity')?.setErrors(null);
      }
    }
  }


}


