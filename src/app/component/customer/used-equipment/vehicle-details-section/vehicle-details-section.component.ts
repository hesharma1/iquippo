import { Component, OnInit, ViewChild } from "@angular/core";
import { FieldConfig } from "src/app/shared/abstractions/field.interface";
import { FormControl, FormGroup, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { DynamicFormComponent } from "../../../shared/dynamic-form/dynamic-form.component";
import { environment } from 'src/environments/environment';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { sellEquipment, documentData } from 'src/app/models/sellEquipment.model';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { formatNumber } from "@angular/common";
import { finalize } from "rxjs/operators";
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { Router } from '@angular/router';
import { ValuationService } from "src/app/services/valuation.service";

@Component({
  selector: 'app-vehicle-details-section',
  templateUrl: './vehicle-details-section.component.html',
  styleUrls: ['./vehicle-details-section.component.css']
})

export class VehicleDetailsSectionComponent implements OnInit {
  todayDate = new Date();
  // @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  public vehicleDetailsForm: FormGroup;
  isInvalidfile: boolean = false;
  docType: { [key: string]: number } = {
    "EQUIPMENT_DETAIL": 1,
    "SERVICE_LOG": 2,
    "INVOICE": 7
  };
  uploadDocumentPath: Array<object> = [];
  uploadingImage: boolean = false;
  vaahanDetail: any;
  documentName: Array<any> = [];
  serviceLogName: Array<any> = [];
  invoiceName: Array<any> = [];
  docData;
  constructor(private router: Router, private notify: NotificationService,private valuationService: ValuationService,public s3: S3UploadDownloadService, private usedEquipmentService: UsedEquipmentService, private spinner: NgxSpinnerService, private fb: FormBuilder) {
    this.vehicleDetailsForm = this.fb.group({
      productCondition: new FormControl('', Validators.required),
      MotorOperatingHours: new FormControl(''),
      Mileage: new FormControl('', Validators.required),
      isValuationReport: new FormControl('', Validators.required),
      uploadDocument: new FormControl(''),
      valuationValue: new FormControl(''),
      isServiceLogs: new FormControl('', Validators.required),
      isParkingCharges: new FormControl(''),
      parkedSince: new FormControl(''),
      parkingChargePerDay: new FormControl('', Validators.compose([Validators.pattern('[1-9][0-9]+'),
      Validators.maxLength(10)])),
      bookValue: new FormControl('', Validators.compose([Validators.maxLength(10),
      Validators.pattern('[1-9][0-9]+')])),
      isOriginalInvoice: new FormControl(''),
      ValuationReportDoc: new FormControl(''),
      isPriceOnRequest: new FormControl('0'),
      sellingPrice: new FormControl('', Validators.compose([, Validators.required, Validators.pattern('[1-9][0-9]+'),
        Validators.maxLength(10)])),
      uploadServiceLogs: new FormControl(),
      OriginalInvoiceDoc: new FormControl()
    }, { validator: [numberonly('MotorOperatingHours'), numberonly('parkingChargePerDay'), numberonly('bookValue'), numberonly('sellingPrice')] });
  }

  ngOnInit(): void {
    //this.vaahanDetail = localStorage.getItem('sellequipmentData');
  }

  getDetails(equipmentDetails) {
    this.vaahanDetail = equipmentDetails;
    console.log(this.vaahanDetail)
    if (this.vaahanDetail != null) {
      //this.vaahanDetail = JSON.parse(this.vaahanDetail);
      this.getDocuments();
      this.vehicleDetailsForm.patchValue({
        //productCondition: this.vaahanDetail.rate_equipment.toString(),
        productCondition: this.vaahanDetail.asset_condition.toString(),
        MotorOperatingHours: this.vaahanDetail.motor_operating_hours,
        Mileage: this.vaahanDetail.hmr_kmr == '0.00' ? '' : this.vaahanDetail.hmr_kmr,
        isValuationReport: this.vaahanDetail.valuation_request ? '1' : '0',
        uploadDocument: this.vaahanDetail,
        valuationValue: this.vaahanDetail.valuation_amount,
        isServiceLogs: this.vaahanDetail.is_service_log_available ? '1' : '0',
        isParkingCharges: this.vaahanDetail.is_parking_charges_info ? '1' : '0',
        parkedSince: this.vaahanDetail.parked_since,
        parkingChargePerDay: this.vaahanDetail.parking_charge_per_day,
        bookValue: this.vaahanDetail.book_value,
        isOriginalInvoice: this.vaahanDetail.is_original_invoice ? '1' : '0',
        isPriceOnRequest: this.vaahanDetail.is_price_on_request ? '1' : '0',
        sellingPrice: this.vaahanDetail.selling_price
      });
    }
    if (!this.vaahanDetail.is_parking_charges_info) {
      this.vehicleDetailsForm.get('isParkingCharges')?.clearValidators();
      this.vehicleDetailsForm.get('isParkingCharges')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('parkedSince')?.clearValidators();
      this.vehicleDetailsForm.get('parkedSince')?.updateValueAndValidity();
    } 
    if (this.vaahanDetail?.is_valuation_completed) {
      this.vehicleDetailsForm.get('valuationValue')?.clearValidators();
      this.vehicleDetailsForm.get('valuationValue')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('ValuationReportDoc')?.clearValidators();
      this.vehicleDetailsForm.get('ValuationReportDoc')?.updateValueAndValidity();
    } else {
      let value = this.vehicleDetailsForm?.get('isValuationReport')?.value;
      this.updateValidations(value);
    }
    let parkingValue = this.vehicleDetailsForm?.get('isParkingCharges')?.value;
    this.updateParkingValidations(parkingValue);
  }

  submit(value: any) {

  }

  

  redirectToVal() {
    let payload= {
      "valuation_asset": this.vaahanDetail?.id
    }
    this.valuationService.postAdvVal(payload).subscribe((res:any)=>{
      this.notify.success("Thank you for showing interest in getting in touch with our Valuation experts. Our team will get in touch with you shortly.")
    })
    
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      // this.vehicleDetailsForm.get('uploadDocument')?.setValue('');
      var documentDataRequest = new documentData();
      documentDataRequest.doc_type = this.docType[tab];
      documentDataRequest.doc_url = uploaded_file.Location;
      documentDataRequest.used_equipment = this.vaahanDetail.id;
      this.uploadDocument(documentDataRequest);
      if (tab == 'EQUIPMENT_DETAIL') {
        this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      } else if (tab == 'SERVICE_LOG') {
        this.serviceLogName.push({ name: fileFormat, url: uploaded_file.Location });
      } else {
        this.invoiceName.push({ name: fileFormat, url: uploaded_file.Location });
      }
      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }

  async handleUploadInvoice(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' ||
     file.type == 'image/svg+xml' || fileExt == 'svg' || file.type == 'application/pdf' || file.type == "docx" || file.type == "doc" || file.type == "docx" || file.type == "dot" || file.type == "dotx") && file.size <= 5242880) {
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      // this.vehicleDetailsForm.get('uploadDocument')?.setValue('');
      var documentDataRequest = new documentData();
      documentDataRequest.doc_type = this.docType[tab];
      documentDataRequest.doc_url = uploaded_file.Location;
      documentDataRequest.used_equipment = this.vaahanDetail.id;
      this.uploadDocument(documentDataRequest);
        this.invoiceName.push({ name: fileFormat, url: uploaded_file.Location });
      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
    } else {
      this.notify.error("Invalid document type.")
    }
  }

  getDocuments() {
    this.usedEquipmentService.getDocumentList(this.vaahanDetail.id).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        var data = res.results;
        this.docData = data;
        if (data.length > 0) {
          for (let document of data) {
            if (document.doc_type == 1) {
              this.documentName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
              this.vehicleDetailsForm.get('ValuationReportDoc')?.clearValidators();
              this.vehicleDetailsForm.get('ValuationReportDoc')?.updateValueAndValidity();
            } else if (document.doc_type == 2) {
              this.serviceLogName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
              this.updateInvoiceValidations('0')
            } else {
              this.invoiceName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
              this.updateInvoiceValidations('0')
            }
          }
        }

      }
    );
  }

  uploadDocument(data: any) {
    //this.spinner.show();
    this.usedEquipmentService.postdocumentUploadUsedEquipment(data).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        console.log(res);
      });
  }

  enterPrice() {
    if( this.vehicleDetailsForm.get('sellingPrice')?.value < 1) {
      this.notify.error('enter a valid selling price',false)
      this.vehicleDetailsForm.get('sellingPrice')?.setValue('');
    }
  }


  isRequiredField(field: string) {
    const form_field: any = this.vehicleDetailsForm.get(field);
    if (!form_field.validator) {
      return false;
    }

    const validator = form_field.validator({} as AbstractControl);
    return (validator && validator.required);
  }

  updateValidations(value) {
    if (value == '0') {
      this.vehicleDetailsForm.get('valuationValue')?.clearValidators();
      this.vehicleDetailsForm.get('valuationValue')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('ValuationReportDoc')?.clearValidators();
      this.vehicleDetailsForm.get('ValuationReportDoc')?.updateValueAndValidity();
    }
    if (value == '1') {
      this.vehicleDetailsForm.get('valuationValue')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('valuationValue')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('ValuationReportDoc')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('ValuationReportDoc')?.updateValueAndValidity();
    }
  }

  updateInvoiceValidations(value) {
    if (value == '0') {
      this.vehicleDetailsForm.get('OriginalInvoiceDoc')?.clearValidators();
      this.vehicleDetailsForm.get('OriginalInvoiceDoc')?.updateValueAndValidity();
    }
    if (value == '1') {
      this.vehicleDetailsForm.get('OriginalInvoiceDoc')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('OriginalInvoiceDoc')?.updateValueAndValidity();
    }
  }

  updateServiceValidations(value) {
    if (value == '0') {
      this.vehicleDetailsForm.get('uploadServiceLogs')?.clearValidators();
      this.vehicleDetailsForm.get('uploadServiceLogs')?.updateValueAndValidity();
    }
    if (value == '1') {
      this.vehicleDetailsForm.get('uploadServiceLogs')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('uploadServiceLogs')?.updateValueAndValidity();
    }
  }

  updateParkingValidations(value) {
    if (value == '0') {
      this.vehicleDetailsForm.get('isParkingCharges')?.clearValidators();
      this.vehicleDetailsForm.get('isParkingCharges')?.updateValueAndValidity();
    }
    if (value == '1') {
      this.vehicleDetailsForm.get('isParkingCharges')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('isParkingCharges')?.updateValueAndValidity();
    }
  }

  test(val) {
    console.log(formatNumber(val, 'en-Us'));
  }
  resetFile(i, url) {
    let id = this.documentName[i].id;
    this.documentName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(res => {
      if (id) {
        this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {
          this.vehicleDetailsForm?.get('ValuationReportDoc')?.setValue('');
          this.resetFile(this.vehicleDetailsForm.get('isValuationReport')?.value, '')
        })
      }
    })
  }
  resetServiceFile(i, url) {
    let id = this.serviceLogName[i].id;
    this.serviceLogName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));

    this.s3.deleteFile(imgPath).subscribe(res => {
      if (id) {
        this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {
          this.vehicleDetailsForm?.get('uploadServiceLogs')?.setValue('');
          this.updateServiceValidations(this.vehicleDetailsForm.get('isServiceLogs')?.value)
        })
      }
    })
  }
  resetinvoiceFile(i, url) {
    let id = this.invoiceName[i].id;
    this.invoiceName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(res => {
      if (id) {
        this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {
          this.vehicleDetailsForm?.get('OriginalInvoiceDoc')?.setValue('');
          this.updateInvoiceValidations(this.vehicleDetailsForm.get('isOriginalInvoice')?.value)
        })
      }
    })
  }

  deleteDocument(imgPath) {
    if (this.docData) {
      let docData = this.docData.filter(e => e.doc_url == imgPath)
      if (docData && docData) {

      }
    }
    // deletedocumentUploadUsedEquipment
  }

  checkValuatioAmount() {
    if(this.vehicleDetailsForm?.get('valuationValue')?.value > 2147483647) {
      this.notify.error("Max valuation amount is 2147483647",false);
      this.vehicleDetailsForm?.get('valuationValue')?.setValue('')
    } else {
      this.vehicleDetailsForm?.get('valuationValue')?.clearValidators();
      this.vehicleDetailsForm?.get('valuationValue')?.updateValueAndValidity();
    }
  }

  parkingCharges() {
    if(this.vehicleDetailsForm?.get('parkingChargePerDay')?.value > 65535) {
      this.notify.error("max parking charges value is 65535",false);
      this.vehicleDetailsForm?.get('parkingChargePerDay')?.setValue('')
    } else {
      this.vehicleDetailsForm?.get('parkingChargePerDay')?.clearValidators();
      this.vehicleDetailsForm?.get('parkingChargePerDay')?.updateValueAndValidity();
    }
  }

  checkParkingValidation(value) {
    if(value == 1) {
      this.vehicleDetailsForm.get('parkedSince')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('parkedSince')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('parkingChargePerDay')?.setValidators(Validators.required);
      this.vehicleDetailsForm.get('parkingChargePerDay')?.updateValueAndValidity();
    } else {
      this.vehicleDetailsForm.get('parkedSince')?.clearValidators();
      this.vehicleDetailsForm.get('parkedSince')?.updateValueAndValidity();
      this.vehicleDetailsForm.get('parkingChargePerDay')?.clearValidators();
      this.vehicleDetailsForm.get('parkingChargePerDay')?.updateValueAndValidity();
    }
  }

}
