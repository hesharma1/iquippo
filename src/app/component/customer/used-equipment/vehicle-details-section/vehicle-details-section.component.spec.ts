import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDetailsSectionComponent } from './vehicle-details-section.component';

describe('VehicleDetailsSectionComponent', () => {
  let component: VehicleDetailsSectionComponent;
  let fixture: ComponentFixture<VehicleDetailsSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDetailsSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDetailsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
