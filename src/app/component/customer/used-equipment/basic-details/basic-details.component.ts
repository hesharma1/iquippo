import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { BasicDetailsSectionComponent } from '../basic-details-section/basic-details-section.component';
import { VaahanDetailsComponent } from '../vaahan-details/vaahan-details.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { sellEquipment, UsedEquipementModel } from 'src/app/models/sellEquipment.model';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css']
})
export class BasicDetailsComponent implements OnInit {
  @ViewChild(BasicDetailsSectionComponent) basicDetailsChild!: BasicDetailsSectionComponent;
  @ViewChild(VaahanDetailsComponent) vaahanDetailsChild!: VaahanDetailsComponent;

  public vaahanDetailsEditable: any;
  public vaahanDetail: any;
  public basicDetail: any;
  public cognitoId;
  public fuelType: { [key: string]: number } = {
    "Diesel": 1,
    "Petrol": 2,
    "CNG": 3
  }
  public equipmentId;
  public steps = {
    current: 1,
    total: [
      { title: 'Basic Details' },
      { title: 'Vehicle Details' },
      { title: 'Visuals' }
    ]
  }

  constructor(private router: Router, private spinner: NgxSpinnerService, private usedEquipmentService: UsedEquipmentService, private notificationservice: NotificationService, private storage: StorageDataService, private route: ActivatedRoute, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    //this.vaahanDetail = localStorage.getItem('sellequipmentData');
    this.route.params.subscribe((params: Params): void => {
      this.equipmentId = params['id'];
      this.getDetail(this.equipmentId);
    });
      // this.router.navigateByUrl('used-equipment/sell-equipment');  
      // this.usedEquipmentService.getBasicDetailList(this.equipmentId).subscribe(res => {
      //   this.vaahanDetail = res;
      //   this.getDetail(this.vaahanDetail.id);
      // });
  }

  back() {
    this.router.navigateByUrl('used-equipment/sell-equipment');
  }

  getDetail(id: number) {
    //this.spinner.show();
    this.usedEquipmentService.getProduct(id).subscribe(
      (res: any) => {
         
        this.vaahanDetail = res;
        // localStorage.setItem('sellequipmentData', JSON.stringify(res));
        this.basicDetailsChild.basicDetailsForm.patchValue({
          rcCopyAvailable: res.is_rc_available ? "1" : "0",
          isFirstOwned: res.is_first_owned ? "1" : "0",
          yearOfManufacture: res.mfg_year,
          customerReferenceNo: res.customer_reference_number,
          pincode: res.pin_code.pin_code,
          address: res.asset_address,
          countryCode: "+91",
          contactNumber: res.alt_contact_number,
          email: res.email_address ? res.email_address : "",
          alternateEmail: res.alt_email_address,
          description: res.asset_description
        });
        this.basicDetailsChild.patchValue(this.vaahanDetail);
        this.vaahanDetailsChild.getDetail(this.vaahanDetail);
      },
      (err: any) => {
      }

    );
  }

  /*
  Form Submit
  */

  next() {
    if (this.basicDetailsChild.basicDetailsForm.invalid) {
      this.basicDetailsChild.basicDetailsForm.markAllAsTouched();
      return;
    }
    if(!this.vaahanDetail.is_equipment_registered && (this.basicDetailsChild.basicDetailsForm.value.engineNumber == null || this.basicDetailsChild.basicDetailsForm.value.engineNumber == '') &&
    (this.basicDetailsChild.basicDetailsForm.value.machineSerialNumber == null || this.basicDetailsChild.basicDetailsForm.value.machineSerialNumber == '') &&
    (this.basicDetailsChild.basicDetailsForm.value.chassisNumber == null || this.basicDetailsChild.basicDetailsForm.value.chassisNumber == '')) {
      this.notificationservice.error("Kindly fill atleast one of the three- Engine no, Chassis No and Machine serial no",false);
    }else {
      this.submitData();
    }
  }

    submitData() {
      var sellEquipmentRequest = new sellEquipment();
      //console.log(this.basicDetailsChild.basicDetailsForm.value);
      console.log(this.vaahanDetailsChild.vaahanDetailsForm.value);
  
      sellEquipmentRequest.category = this.vaahanDetail.category.id;
      sellEquipmentRequest.model = this.vaahanDetail.model.id ? this.vaahanDetail.model.id : this.vaahanDetail.model;
      sellEquipmentRequest.brand = this.vaahanDetail.brand.id;
      sellEquipmentRequest.info_level = 1;
      sellEquipmentRequest.seller = this.cognitoId;
      sellEquipmentRequest.is_equipment_registered = this.vaahanDetail.is_equipment_registered;
      if (this.vaahanDetail.is_equipment_registered) {
        sellEquipmentRequest.rc_number = this.vaahanDetail.rc_number;
      }
      sellEquipmentRequest.is_rc_available = this.basicDetailsChild.basicDetailsForm.get('rcCopyAvailable')?.value == 1 ? true : false;
      sellEquipmentRequest.is_first_owned = this.basicDetailsChild.basicDetailsForm.get('isFirstOwned')?.value == 1 ? true : false;
      if (this.basicDetailsChild.basicDetailsForm.get('yearOfManufacture')?.value != "") {
        let date = this.basicDetailsChild.basicDetailsForm.get('yearOfManufacture')?.value;
        sellEquipmentRequest.mfg_year = this.datePipe.transform(date, 'yyyy');
      }
      if (this.basicDetailsChild.basicDetailsForm.get('customerReferenceNo')?.value != "") {
        sellEquipmentRequest.customer_reference_number = this.basicDetailsChild.basicDetailsForm.get('customerReferenceNo')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('contactNumber')?.value != "") {
        sellEquipmentRequest.alt_contact_number = this.basicDetailsChild.basicDetailsForm.get('contactNumber')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('email')?.value != "") {
        sellEquipmentRequest.email_address = this.basicDetailsChild.basicDetailsForm.get('email')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('alternateEmail')?.value != "") {
        sellEquipmentRequest.alt_email_address = this.basicDetailsChild.basicDetailsForm.get('alternateEmail')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('description')?.value != "") {
        sellEquipmentRequest.asset_description = this.basicDetailsChild.basicDetailsForm.get('description')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('address')?.value != "") {
        sellEquipmentRequest.asset_address = this.basicDetailsChild.basicDetailsForm.get('address')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('pincode')?.value != "") {
        sellEquipmentRequest.pin_code = this.basicDetailsChild.basicDetailsForm.get('pincode')?.value;
      }
      if (this.vaahanDetailsChild.vaahanDetailsForm.get('name')?.value != "") {
        sellEquipmentRequest.product_name = this.vaahanDetailsChild.vaahanDetailsForm.get('name')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('registrationNumber')?.value != "") {
        sellEquipmentRequest.rc_number = this.basicDetailsChild.basicDetailsForm.get('registrationNumber')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('chassisNumber')?.value != "") {
        sellEquipmentRequest.chassis_number = this.basicDetailsChild.basicDetailsForm.get('chassisNumber')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('engineNumber')?.value != "") {
        sellEquipmentRequest.engine_number = this.basicDetailsChild.basicDetailsForm.get('engineNumber')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('machineSerialNumber')?.value != "") {
        sellEquipmentRequest.machine_sr_number = this.basicDetailsChild.basicDetailsForm.get('machineSerialNumber')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('enginePower')?.value != "") {
        sellEquipmentRequest.engine_power = this.basicDetailsChild.basicDetailsForm.get('enginePower')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('fuelType')?.value != "") {
        sellEquipmentRequest.fuel_type = this.basicDetailsChild.basicDetailsForm.get('fuelType')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('grossWeight')?.value != "") {
        sellEquipmentRequest.gross_Weight = this.basicDetailsChild.basicDetailsForm.get('grossWeight')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('operationWeight')?.value != "") {
        sellEquipmentRequest.operating_weight = this.basicDetailsChild.basicDetailsForm.get('operationWeight')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('bucketCapacity')?.value != "") {
        sellEquipmentRequest.bucket_capacity = this.basicDetailsChild.basicDetailsForm.get('bucketCapacity')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('liftingCapacity')?.value != "") {
        sellEquipmentRequest.lifting_capacity = this.basicDetailsChild.basicDetailsForm.get('liftingCapacity')?.value;
      }
      if (this.basicDetailsChild.basicDetailsForm.get('SellPrice')?.value != "") {
        sellEquipmentRequest.selling_price = this.basicDetailsChild.basicDetailsForm.get('SellPrice')?.value;
      }
      this.usedEquipmentService.postupdateUsedEquipment(sellEquipmentRequest, this.equipmentId).subscribe(
        (res: any) => {
           
          //localStorage.setItem('sellequipmentData', JSON.stringify(res));
          this.router.navigate(['used-equipment/vehicle-details',this.equipmentId]);
        },
        (err) => {
  
          this.notificationservice.error(err);
        }
      );
    }
    
    // this.basicDetailsChild.submit();
    // this.vaahanDetailsChild.submit();
}
