import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, RouterStateSnapshot } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { sellEquipment, UsedEquipementModel } from 'src/app/models/sellEquipment.model';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { finalize } from 'rxjs/operators';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';
import { SigninComponent } from 'src/app/component/signin/signin.component';
import { ApiRouteService } from 'src/app/utility/app.refrence';
@Component({
  selector: 'app-sell-equipment',
  templateUrl: './sell-equipment.component.html',
  styleUrls: ['./sell-equipment.component.css']
})
export class SellEquipmentComponent implements OnInit {
  public sellEquipmentForm: FormGroup;
  brandList: any;
  categoryList: any;
  message?: string;
  masterEquipmentData: UsedEquipementModel;
  modelList: any;
  cognitoId;
  categoryId: any;
  brandId: any;
  modalId: any;
  categoryData: any;
  brandData: any;
  modalData: any;
  rolesArray;
  partnerAllDetail: any;
  agreementNotApproved: boolean = false;
  userRole: string | null = '';
  constructor(
    private router: Router,
    private usedEquipmentService: UsedEquipmentService,
    private commonService: CommonService,private dialog:MatDialog, private spinner: NgxSpinnerService, private notificationservice: NotificationService,
    private storage: StorageDataService, public notify: NotificationService, public apiRouteService: ApiRouteService, private appRouteEnum: AppRouteEnum, private sharedservice:SharedService
  ) {
    this.sellEquipmentForm = new FormGroup({
      category: new FormControl('', Validators.required),
      otherCategory: new FormControl(''),
      brand: new FormControl('', Validators.required),
      otherBrand: new FormControl(''),
      model: new FormControl('', Validators.required),
      otherModel: new FormControl(''),
      isEquipmentRegistered: new FormControl('', Validators.required),
      rcNumber: new FormControl('')
    });
    this.masterEquipmentData = new UsedEquipementModel();
  }

  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    if (this.rolesArray)
      this.rolesArray = this.rolesArray.split(',');
    this.getCategoryMasters();
    this.getPartnerAllInfo();
    
  }
  ngAfterViewInit(){
    this.userRole = localStorage.getItem("userRole");
  }

  createListing(isMultiple:boolean) {
    if (!this.cognitoId) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      
      //dialogRef.afterClosed().subscribe(result => {
      //    console.log(`Dialog result: ${result}`);
      //});
    
  
    dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = localStorage.getItem('cognitoId');
            this.rolesArray = this.storage.getStorageData("rolesArray", false);
          }
        })
      return;
    }
    let sellerType;
    if (this.rolesArray) {
      if (this.rolesArray.includes('Customer') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer')) {
        let role;
        if (this.rolesArray.includes('Dealer')) {
          role = 'Dealer';
          sellerType = 2;
        }
        else if (this.rolesArray.includes('Manufacturer')) {
          role = 'Manufacturer';
          sellerType = 2;
        }
        else if (this.rolesArray.includes('Customer')) {
          role = 'Customer';
          sellerType = 1;
        }

        let payload = {
          cognito_id: this.cognitoId,
          role: role
        }
        this.commonService.validateSeller(payload).subscribe((res: any) => {
          if (res?.partner_admin && role != 'Customer') {
            this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              // this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to complete your KYC profile with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true, isNew: false,returnUrl:this.router.url } });
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return;
            }
            if (!res.bank_details_exist) {
              // this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to fill your bank details with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { createListing: true, isNew: false } });
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return;
              
            }
            if (!res.allow) {
              if(res?.agreement_status!=undefined && res?.agreement_status=="agreement_not_approved"){
                constMsg =  res.message;
                this.agreementNotApproved = true; 
               }
               else{
                constMsg =  res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ';
                this.agreementNotApproved = false; 
              }
             // this.notify.warn(res.message, true);
             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message: constMsg,//res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                agreementNotApproved:this.agreementNotApproved
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate(['/customer/dashboard/partner-dealer/agreement-details'], { queryParams: { createListing: true, isNew: false } });
              return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return;
             
            }
          }
          else if (res.partner_admin != undefined && !res?.partner_admin && role != 'Customer') {
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              this.notify.warn('KYC and Address proof of partner admin not available.', true);
              // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true,isNew: false } });
              return;
            }
            if (!res.bank_details_exist) {

              this.notify.warn('bank details of partner admin not available.', true);
              //  this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true,isNew: false } });
              return;

            }
            if (!res.allow) {
              this.notify.warn(res.message, true);
              return;
              // if (role == 'Customer') {
              //   this.router.navigate(['/'+this.appRouteEnum.CustomerAgreement]);
              //   return;
              // }
              // else{
              // this.router.navigate(['/partner-dealer/agreement-details'], { queryParams: { createListing: true,isNew: false } });
              // return;
              // }
            }
          }
          else if (role == 'Customer') {
            if (res.kyc_verified != 2 || !res.pan_address_proof) {
              if (!res.pan_address_proof || res.kyc_verified == 1) {
               // this.notify.warn(res.message, true);
                const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                  width: '500px',
                  data: {
                    message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                  }
                });
                dialogRef.afterClosed().subscribe(result => {
                  if (result) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url } });
                    return;
                  }
                  else {
                    //console.log(result);
                    return;
                  }
                })
                return;
              } 
              else if (res.kyc_verified == 3) {
                this.notify.warn('As per our records, your KYC profile is rejected. If you have any questions, kindly raise a call back request.', true);
                return;
              }
              
            }
            if (!res.bank_details_exist) {

            //  this.notify.warn(res.message, true);
            const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url}  });
              return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return; 
            

            }
            if (!res.allow) {
              var constMsg =  '';
              if(res?.agreement_status!=undefined && res?.agreement_status=="agreement_not_approved"){
               constMsg =  res.message;
               this.agreementNotApproved = true; 
              }
              else{
               constMsg =  res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ';
               this.agreementNotApproved = false; 
             }
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message:constMsg, //res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                  agreementNotApproved:this.agreementNotApproved
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate(['/' + this.appRouteEnum.CustomerAgreement],{ queryParams: { returnUrl:this.router.url}});
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return; 
              
            }
          }

          if(isMultiple){
            this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false,returnUrl:this.router.url } })
          }
          else{
          //this.spinner.show();
          var sellEquipmentRequest = new sellEquipment();
          sellEquipmentRequest.category = this.categoryId;
          sellEquipmentRequest.model = this.modalId;
          sellEquipmentRequest.brand = this.brandId;
          sellEquipmentRequest.rc_number = this.sellEquipmentForm.get('rcNumber')?.value;
          sellEquipmentRequest.is_equipment_registered = this.sellEquipmentForm.get('isEquipmentRegistered')?.value == 1 ? true : false;
          sellEquipmentRequest.seller = this.cognitoId;
          sellEquipmentRequest.seller_type = sellerType;
          if (this.sellEquipmentForm.get('otherCategory')?.value) {
            sellEquipmentRequest.other_category = this.sellEquipmentForm.get('otherCategory')?.value;
          }
          if (this.sellEquipmentForm.get('otherBrand')?.value) {
            sellEquipmentRequest.other_brand = this.sellEquipmentForm.get('otherBrand')?.value;

          }
          if (this.sellEquipmentForm.get('otherModel')?.value) {
            sellEquipmentRequest.other_model = this.sellEquipmentForm.get('otherModel')?.value;

          }
          if(sellEquipmentRequest.category != undefined && sellEquipmentRequest.brand != undefined && sellEquipmentRequest.model != undefined) {
            this.usedEquipmentService.postUsedEquipment(sellEquipmentRequest).pipe(finalize(() => {   })).subscribe(
              (res: any) => {
                this.router.navigate(['used-equipment/basic-details', res.id]);
                //this.masterDataModel.groupDetailDataSource?.push(this.masterDataModel.addGroupResponse);
              },
              err => {
                console.error(err);
                this.notificationservice.error(err);
              }
            );
          } else {
            this.notify.error("please select correct category, brand, model",false);
          }
          }
        });
      }
      else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 1){
        this.notify.warn("You cannot perform this action as your registration request is currently under approval process. Please get in touch with us if you need further help.")
      }
      else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 2){
        this.notify.warn("Please re-login again to create listing.");
        this.sharedservice.logout();
      }
    }
  }
  createListingMultiple(isMultiple:boolean) {
    if (!this.cognitoId) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      //dialogRef.afterClosed().subscribe(result => {
      //    console.log(`Dialog result: ${result}`);
      //});
    
  
    dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = localStorage.getItem('cognitoId');
            this.rolesArray = this.storage.getStorageData("rolesArray", false);
          }
        })
      return;
    }
    let sellerType;
    if (this.rolesArray) {
      if (this.rolesArray.includes('Customer') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer')) {
        let role;
        if (this.rolesArray.includes('Dealer')) {
          role = 'Dealer';
          sellerType = 2;
        }
        else if (this.rolesArray.includes('Manufacturer')) {
          role = 'Manufacturer';
          sellerType = 2;
        }
        else{
          role = 'Customer';
          sellerType = 1;
        }

        let payload = {
          cognito_id: this.cognitoId,
          role: role
        }
        this.commonService.validateSeller(payload).subscribe((res: any) => {
          if (res?.partner_admin && role != 'Customer') {
            this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              // this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to complete your KYC profile with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true, isNew: false,returnUrl:this.router.url } });
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return;
            }
            if (!res.bank_details_exist) {
              // this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to fill your bank details with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { createListing: true, isNew: false } });
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return;
              
            }
            if (!res.allow) {
              if(res?.agreement_status!=undefined && res?.agreement_status=="agreement_not_approved"){
                constMsg =  res.message;
                this.agreementNotApproved = true; 
               }
               else{
                constMsg =  res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ';
                this.agreementNotApproved = false; 
              }
             // this.notify.warn(res.message, true);
             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message: constMsg,
                agreementNotApproved:this.agreementNotApproved
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate(['/customer/dashboard/partner-dealer/agreement-details'], { queryParams: { createListing: true, isNew: false } });
              return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return;
             
            }
          }
          else if (res.partner_admin != undefined && !res?.partner_admin && role != 'Customer') {
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              this.notify.warn('KYC and Address proof of partner admin not available.', true);
              // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true,isNew: false } });
              return;
            }
            if (!res.bank_details_exist) {

              this.notify.warn('bank details of partner admin not available.', true);
              //  this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true,isNew: false } });
              return;

            }
            if (!res.allow) {
              this.notify.warn(res.message, true);
              return;
              // if (role == 'Customer') {
              //   this.router.navigate(['/'+this.appRouteEnum.CustomerAgreement]);
              //   return;
              // }
              // else{
              // this.router.navigate(['/partner-dealer/agreement-details'], { queryParams: { createListing: true,isNew: false } });
              // return;
              // }
            }
          }
          else if (role == 'Customer') {
            if (res.kyc_verified != 2 || !res.pan_address_proof) {
              if (!res.pan_address_proof || res.kyc_verified == 1) {
               // this.notify.warn(res.message, true);
                const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                  width: '500px',
                  data: {
                    message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                  }
                });
                dialogRef.afterClosed().subscribe(result => {
                  if (result) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url } });
                    return;
                  }
                  else {
                    //console.log(result);
                    return;
                  }
                })
                return;
              } 
              else if (res.kyc_verified == 3) {
                this.notify.warn('As per our records, your KYC profile is rejected. If you have any questions, kindly raise a call back request.', true);
                return;
              }
              
            }
            if (!res.bank_details_exist) {

            //  this.notify.warn(res.message, true);
            const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url}  });
              return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return; 
            

            }
            if (!res.allow) {
              var constMsg =  '';
              if(res?.agreement_status!=undefined && res?.agreement_status=="agreement_not_approved"){
               constMsg =  res.message;
               this.agreementNotApproved = true; 
              }
              else{
               constMsg =  res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ';
               this.agreementNotApproved = false; 
             }
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message:constMsg, //res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                  agreementNotApproved:this.agreementNotApproved
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate(['/' + this.appRouteEnum.CustomerAgreement],{ queryParams: { returnUrl:this.router.url}});
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return; 
              
            }
          }

          if(isMultiple){
            localStorage.removeItem("adminBulkUpload");
            this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false,returnUrl:this.router.url } })
          }
          else{
          //this.spinner.show();
          if(this.categoryId != null && this.modalId != null && this.brandId != null) {
            var sellEquipmentRequest = new sellEquipment();
          sellEquipmentRequest.category = this.categoryId;
          sellEquipmentRequest.model = this.modalId;
          sellEquipmentRequest.brand = this.brandId;
          sellEquipmentRequest.rc_number = this.sellEquipmentForm.get('rcNumber')?.value;
          sellEquipmentRequest.is_equipment_registered = this.sellEquipmentForm.get('isEquipmentRegistered')?.value == 1 ? true : false;
          sellEquipmentRequest.seller = this.cognitoId;
          sellEquipmentRequest.seller_type = sellerType;
          if (this.sellEquipmentForm.get('otherCategory')?.value) {
            sellEquipmentRequest.other_category = this.sellEquipmentForm.get('otherCategory')?.value;
          }
          if (this.sellEquipmentForm.get('otherBrand')?.value) {
            sellEquipmentRequest.other_brand = this.sellEquipmentForm.get('otherBrand')?.value;

          }
          if (this.sellEquipmentForm.get('otherModel')?.value) {
            sellEquipmentRequest.other_model = this.sellEquipmentForm.get('otherModel')?.value;

          }
          this.usedEquipmentService.postUsedEquipment(sellEquipmentRequest).pipe(finalize(() => {   })).subscribe(
            (res: any) => {
              this.router.navigate(['used-equipment/basic-details', res.id]);
              //this.masterDataModel.groupDetailDataSource?.push(this.masterDataModel.addGroupResponse);
            },
            err => {
              console.error(err);
              this.notificationservice.error(err);
            }
          );
          } else {
            this.notify.error("please select correct category, brand, model",false);
          }
          
          }
        });
      }
      else if((this.userRole == this.apiRouteService.roles.partnerGuest || this.userRole == this.apiRouteService.roles.customer) && this.partnerAllDetail?.partner_contact_entity?.status == 1){
        this.notify.warn("You cannot perform this action as your registration request is currently under approval process. Please get in touch with us if you need further help.")
      }
      else if((this.userRole == this.apiRouteService.roles.partnerGuest || this.userRole == this.apiRouteService.roles.customer) && this.partnerAllDetail?.partner_contact_entity?.status == 2){
        this.notify.warn("Please re-login again to create listing.");
        this.sharedservice.logout();
      }
      else{
        this.notify.warn("You cannot perform this action with this role. Please switch role from dashboard.")
      }
    }
  }
  updateValidations() {
    if(this.sellEquipmentForm){
      if(this.sellEquipmentForm?.get('category')?.value == 'Other'){
        this.sellEquipmentForm.get('otherCategory')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherCategory')?.clearValidators();
        this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherCategory')?.setValue('');
      }
      if(this.sellEquipmentForm?.get('brand')?.value == 'Other'){
        this.sellEquipmentForm.get('otherBrand')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherBrand')?.clearValidators();
        this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherBrand')?.setValue('');
      }
      if(this.sellEquipmentForm?.get('model')?.value == 'Other'){
        this.sellEquipmentForm.get('otherModel')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherModel')?.clearValidators();
        this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherModel')?.setValue('');
      }
      if(this.sellEquipmentForm?.get('isEquipmentRegistered')?.value == '1'){
        this.sellEquipmentForm.get('rcNumber')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('rcNumber')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('rcNumber')?.clearValidators();
        this.sellEquipmentForm.get('rcNumber')?.updateValueAndValidity();
        this.sellEquipmentForm.get('rcNumber')?.setValue('');
      }
    }
    // if (this.sellEquipmentForm) {
    //   if (this.sellEquipmentForm.get('category')?.value == '2') {
    //     this.sellEquipmentForm.get('otherCategory')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('category')?.value == '1') {
    //     this.sellEquipmentForm.get('otherCategory')?.clearValidators();
    //     this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
    //   }
    //   if (this.sellEquipmentForm.get('brand')?.value == '1') {
    //     this.sellEquipmentForm.get('otherBrand')?.clearValidators();
    //     this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('brand')?.value == '2') {
    //     this.sellEquipmentForm.get('otherBrand')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
    //   }
    //   if (this.sellEquipmentForm.get('model')?.value == '1') {
    //     this.sellEquipmentForm.get('otherModel')?.clearValidators();
    //     this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('model')?.value == '2') {
    //     this.sellEquipmentForm.get('otherModel')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
    //   }
    //   if (this.sellEquipmentForm.get('isEquipmentRegistred')?.value == '1') {
    //     this.sellEquipmentForm.get('registationNumber')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('registationNumber')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('isEquipmentRegistred')?.value == '2') {
    //     this.sellEquipmentForm.get('registationNumber')?.clearValidators();
    //     this.sellEquipmentForm.get('registationNumber')?.updateValueAndValidity();
    //   }
    // }

  }

  getCategoryMasters() {
    let queryparams = "limit=999"
    this.commonService.getCategoryMaster(queryparams).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      err => {
        console.error(err);
      });
  }

  filterValuesCategory(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }

  filterValuesBrand(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.brandData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }
  trimSpaces(event){
    if(event.target.value)
event.target.value = event.target.value.trim()
  }
  filterValuesModal(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.modalData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }

  onModalSelect(modal: any) {
    this.modalId = modal.id;
    this.sellEquipmentForm.get('model')?.setValue(modal.name);
  }

  onCategorySelect(categoryId) {
    if (categoryId.id != this.categoryId) {
      this.sellEquipmentForm.get('model')?.setValue('');
      this.sellEquipmentForm.get('brand')?.setValue('');
      this.sellEquipmentForm.get('category')?.setValue(categoryId.name);
      this.categoryId = categoryId.id
      if (this.categoryId != 0) {
        this.commonService.getBrandMasterByCatId(this.categoryId).pipe(finalize(() => {   })).subscribe(
          (res: any) => {
            this.brandList = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.sellEquipmentForm.get('category')?.setValue(categoryId.name);
    }

  }

  onBrandSelect(brandId: any) {
    if (brandId.id != this.brandId) {
      this.brandId = brandId.id;
      this.sellEquipmentForm.get('model')?.setValue('');
      this.sellEquipmentForm.get('brand')?.setValue(brandId.display_name);
      if (this.brandId != 0) {
        this.commonService.getModelMasterByBrandId(brandId.id,this.categoryId).pipe(finalize(() => {   })).subscribe(
          (res: any) => {
            this.modelList = res.results;
            this.modalData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.sellEquipmentForm.get('brand')?.setValue(brandId.display_name);
    }
  }
  getPartnerAllInfo(){
    this.sharedservice.getAllPartnerInfo().pipe(finalize(() => {   })).subscribe(res=>{
       console.log(res);
       this.partnerAllDetail = res;
     })
   }
  // multipleUpload() {
  //   if (!this.cognitoId) {
  //     const dialogConfig = new MatDialogConfig();
  //     dialogConfig.disableClose = false;
  //     dialogConfig.autoFocus = true;
  //     dialogConfig.panelClass = 'my-class';
  //     dialogConfig.width = '900px';
  //     dialogConfig.data = {
  //       flag: true
  //     }
  //     const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
  //     //dialogRef.afterClosed().subscribe(result => {
  //     //    console.log(`Dialog result: ${result}`);
  //     //});
    
  
  //   dialogRef.afterClosed().subscribe(val => {
  //         if (val) {
  //           this.cognitoId = localStorage.getItem('cognitoId');
  //           this.rolesArray = this.storage.getStorageData("rolesArray", false);
  //         }
  //       })
  //     return;
  //   }
  //   let sellerType;
  //   if (this.rolesArray) {
  //     if (this.rolesArray.includes('Customer') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer')) {
  //       let role;
  //       if (this.rolesArray.includes('Dealer')) {
  //         role = 'Dealer';
  //         sellerType = 2;
  //       }
  //       else if (this.rolesArray.includes('Manufacturer')) {
  //         role = 'Manufacturer';
  //         sellerType = 2;
  //       }
  //       else if (this.rolesArray.includes('Customer')) {
  //         role = 'Customer';
  //         sellerType = 1;
  //       }

  //       let payload = {
  //         cognito_id: this.cognitoId,
  //         role: role
  //       }
  //       this.commonService.validateSeller(payload).subscribe((res: any) => {
  //         if (res?.partner_admin && role != 'Customer') {
  //           this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
  //           if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
  //             // this.notify.warn(res.message, true);
  //             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //               width: '650px',
  //               data: {
  //                 message: 'You need to complete your KYC profile with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //               }
  //             });
  //             dialogRef.afterClosed().subscribe(result => {
  //               if (result) {
  //                 this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true, isNew: false,returnUrl:this.router.url } });
  //                 return;
  //               }
  //               else {
  //                 //console.log(result);
  //                 return;
  //               }
  //             })
  //             return;
  //           }
  //           if (!res.bank_details_exist) {
  //             // this.notify.warn(res.message, true);
  //             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //               width: '650px',
  //               data: {
  //                 message: 'You need to fill your bank details with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //               }
  //             });
  //             dialogRef.afterClosed().subscribe(result => {
  //               if (result) {
  //                 this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { createListing: true, isNew: false,returnUrl:this.router.url } });
  //                 return;
  //               }
  //               else {
  //                 //console.log(result);
  //                 return;
  //               }
  //             })
  //             return;
              
  //           }
  //           if (!res.allow) {
  //            // this.notify.warn(res.message, true);
  //            const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //             width: '650px',
  //             data: {
  //               message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //             }
  //           });
  //           dialogRef.afterClosed().subscribe(result => {
  //             if (result) {
  //               this.router.navigate(['/customer/dashboard/partner-dealer/agreement-details'], { queryParams: { createListing: true, isNew: false,returnUrl:this.router.url } });
  //             return;
  //             }
  //             else {
  //               //console.log(result);
  //               return;
  //             }
  //           })
  //           return;
             
  //           }
  //         }
  //         else if (res.partner_admin != undefined && !res?.partner_admin && role != 'Customer') {
  //           if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
  //             this.notify.warn('KYC and Address proof of partner admin not available.', true);
  //             // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1, createListing: true,isNew: false } });
  //             return;
  //           }
  //           if (!res.bank_details_exist) {

  //             this.notify.warn('bank details of partner admin not available.', true);
  //             //  this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true,isNew: false } });
  //             return;

  //           }
  //           if (!res.allow) {
  //             this.notify.warn(res.message, true);
  //             return;
  //             // if (role == 'Customer') {
  //             //   this.router.navigate(['/'+this.appRouteEnum.CustomerAgreement]);
  //             //   return;
  //             // }
  //             // else{
  //             // this.router.navigate(['/partner-dealer/agreement-details'], { queryParams: { createListing: true,isNew: false } });
  //             // return;
  //             // }
  //           }
  //         }
  //         else if (role == 'Customer') {
  //           if (res.kyc_verified != 2 || !res.pan_address_proof) {
  //             if (!res.pan_address_proof || res.kyc_verified == 1) {
  //              // this.notify.warn(res.message, true);
  //               const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //                 width: '650px',
  //                 data: {
  //                   message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //                 }
  //               });
  //               dialogRef.afterClosed().subscribe(result => {
  //                 if (result) {
  //                   this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url } });
  //                   return;
  //                 }
  //                 else {
  //                   //console.log(result);
  //                   return;
  //                 }
  //               })
  //               return;
  //             } 
  //             else if (res.kyc_verified == 3) {
  //               this.notify.warn('As per our records, your KYC profile is rejected. If you have any questions, kindly raise a call back request.', true);
  //               return;
  //             }
              
  //           }
  //           if (!res.bank_details_exist) {

  //           //  this.notify.warn(res.message, true);
  //           const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //             width: '650px',
  //             data: {
  //               message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //             }
  //           });
  //           dialogRef.afterClosed().subscribe(result => {
  //             if (result) {
  //               this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { createListing: true, isNew: false, 'isrequired': true,returnUrl:this.router.url } });
  //             return;
  //             }
  //             else {
  //               //console.log(result);
  //               return;
  //             }
  //           })
  //           return; 
            

  //           }
  //           if (!res.allow) {
  //             //this.notify.warn(res.message, true);
  //             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
  //               width: '650px',
  //               data: {
  //                 message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
  //               }
  //             });
  //             dialogRef.afterClosed().subscribe(result => {
  //               if (result) {
  //                 this.router.navigate(['/' + this.appRouteEnum.CustomerAgreement], { queryParams: {returnUrl:this.router.url}});
  //                 return;
  //               }
  //               else {
  //                 //console.log(result);
  //                 return;
  //               }
  //             })
  //             return; 
              
  //           }
  //         }
  //         this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false,returnUrl:this.router.url } })
  //       });
  //     }
  //     else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 1){
  //       this.notify.warn("You cannot perform this action as your registration request is currently under approval process. Please get in touch with us if you need further help.")
  //     }
  //     else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 2){
  //       this.notify.warn("Please re-login again to create listing.");
  //       this.sharedservice.logout();
  //     }
  //   }
  // }

}
