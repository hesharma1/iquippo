import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaahanDetailsComponent } from './vaahan-details.component';

describe('VaahanDetailsComponent', () => {
  let component: VaahanDetailsComponent;
  let fixture: ComponentFixture<VaahanDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaahanDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaahanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
