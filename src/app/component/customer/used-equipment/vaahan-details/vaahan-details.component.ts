import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';

@Component({
  selector: 'app-vaahan-details',
  templateUrl: './vaahan-details.component.html',
  styleUrls: ['./vaahan-details.component.css']
})
export class VaahanDetailsComponent implements OnInit {
  @Input() vaahanDetailsEditable: any = {};

  public vaahanDetailsForm: FormGroup;
  vaahanDetail: any;
  fuelType: { [key: number]: string } = {
    1: "Diesel",
    2: "Petrol",
    3: "CNG"
  }

  constructor(private usedEquipmentService: UsedEquipmentService) {
    this.vaahanDetailsForm = new FormGroup({
      name: new FormControl('', Validators.required),
      registrationNumber: new FormControl('', Validators.required),
      chassisNumber: new FormControl('', Validators.required),
      engineNumber: new FormControl('', Validators.required),
      machineSerialNumber: new FormControl('', Validators.required),
      enginePower: new FormControl('', Validators.required),
      grossWeight: new FormControl('', Validators.required),
      operationWeight: new FormControl('', Validators.required),
      bucketCapacity: new FormControl('', Validators.required),
      liftingCapacity: new FormControl('', Validators.required),
      fuelType: new FormControl('')
    });
  }
  
  ngOnInit(): void {
  }

  submit() {

  }

  getDetail(equipmentDetails) {
    this.vaahanDetail = equipmentDetails;
    //this.vaahanDetail = localStorage.getItem('sellequipmentData');

    if (this.vaahanDetail != null) {
     // this.vaahanDetail = JSON.parse(this.vaahanDetail);
      this.vaahanDetailsEditable = new Object();
      this.vaahanDetailsEditable.chassisNumber = this.vaahanDetail.is_chassis_number_from_vaahan ? this.vaahanDetail.is_chassis_number_from_vaahan : false;
      this.vaahanDetailsEditable.engineNumber = this.vaahanDetail.is_engine_number_from_vaahan ? this.vaahanDetail.is_engine_number_from_vaahan : true;
      this.vaahanDetailsEditable.enginePower = this.vaahanDetail.is_engine_power_from_vaahan ? this.vaahanDetail.is_engine_power_from_vaahan : false;
      this.vaahanDetailsEditable.fuelType = this.vaahanDetail.is_fuel_type_from_vaahan ? this.vaahanDetail.is_fuel_type_from_vaahan : false;
      this.vaahanDetailsEditable.registrationNumber = this.vaahanDetail.is_rc_available ? this.vaahanDetail.is_rc_available : false;
      this.vaahanDetailsEditable.grossWeight = this.vaahanDetail.grossWeight ? true : false;
      this.vaahanDetailsEditable.operationWeight = this.vaahanDetail.operationWeight ? true : false;
      this.vaahanDetailsEditable.bucketCapacity = this.vaahanDetail.bucketCapacity ? true : false;
      this.vaahanDetailsEditable.liftingCapacity = this.vaahanDetail.liftingCapacity ? true : false;
          if (this.vaahanDetail.model.name) {
            this.vaahanDetailsForm.get('name')?.setValue((this.vaahanDetail?.category?.display_name == 'Other'?this.vaahanDetail?.other_category:this.vaahanDetail?.category?.display_name) +' ' +(this.vaahanDetail?.brand?.display_name == 'Other'?this.vaahanDetail?.other_brand:this.vaahanDetail?.brand?.display_name) +' ' + (this.vaahanDetail?.model?.name == 'Other'?this.vaahanDetail?.other_model:this.vaahanDetail?.model?.name));
          }
          if (this.vaahanDetail.rc_number) {
            this.vaahanDetailsForm.get('registrationNumber')?.setValue(this.vaahanDetail.rc_number);
          }
           if (this.vaahanDetail.chassis_number) {
            this.vaahanDetailsForm.get('chassisNumber')?.setValue(this.vaahanDetail.chassis_number);
          }
          if (this.vaahanDetail.engine_number) {
            this.vaahanDetailsForm.get('engineNumber')?.setValue(this.vaahanDetail.engine_number);
          }
          if (this.vaahanDetail.machine_sr_number) {
            this.vaahanDetailsForm.get('machineSerialNumber')?.setValue(this.vaahanDetail.machine_sr_number);
          }
          if (this.vaahanDetail.engine_power) {
            this.vaahanDetailsForm.get('enginePower')?.setValue(this.vaahanDetail.engine_power);
          }
          if (this.vaahanDetail.gross_Weight) {
            this.vaahanDetailsForm.get('grossWeight')?.setValue(this.vaahanDetail.gross_Weight);
          }
          if (this.vaahanDetail.operating_weight) {
            this.vaahanDetailsForm.get('operationWeight')?.setValue(this.vaahanDetail.operating_weight);
          }
          if (this.vaahanDetail.bucket_capacity) {
            this.vaahanDetailsForm.get('bucketCapacity')?.setValue(this.vaahanDetail.bucket_capacity);
          }
          if (this.vaahanDetail.lifting_capacity) {
            this.vaahanDetailsForm.get('liftingCapacity')?.setValue(this.vaahanDetail.lifting_capacity);
          }
          if (this.vaahanDetail.fuel_type) {
            this.vaahanDetailsForm.get('fuelType')?.setValue(this.fuelType[this.vaahanDetail.fuel_type]);
          }
    }
  }

}
