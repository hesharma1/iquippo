import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { VaahanDetailsComponent } from '../vaahan-details/vaahan-details.component';
import { VehicleDetailsSectionComponent } from '../vehicle-details-section/vehicle-details-section.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { sellEquipment, UsedEquipementModel } from 'src/app/models/sellEquipment.model';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DatePipe } from '@angular/common';
import { finalize } from 'rxjs/operators';
import { ConfirmedPopup1Component } from '../../confirmed-popup/confirmed-popup1.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css']
})
export class VehicleDetailsComponent implements OnInit {
  @ViewChild(VehicleDetailsSectionComponent) basicDetailsChild!: VehicleDetailsSectionComponent;
  @ViewChild(VaahanDetailsComponent) vaahanDetailsChild!: VaahanDetailsComponent;
  @ViewChild(VehicleDetailsSectionComponent) vehicleDetailsChild!: VehicleDetailsSectionComponent;

  public vaahanDetailsEditable: any;
  public vaahanDetail: any;
  public fuelType: { [key: string]: number } = {
    "Diesel": 1,
    "Petrol": 2,
    "CNG": 3
  }
  public formatedDate?: string;
  public cognitoId;
  public equipmentId;
  public steps = {
    current: 2,
    total: [
      { title: 'Basic Details' },
      { title: 'Vehicle Details' },
      { title: 'Visuals' }
    ]
  }
  value: any

  constructor(public dialog: MatDialog,
    private router: Router, private spinner: NgxSpinnerService, private usedEquipmentService: UsedEquipmentService, private notificationservice: NotificationService,
    private storage: StorageDataService, private route: ActivatedRoute, private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    //this.vaahanDetail = localStorage.getItem('sellequipmentData');
    this.cognitoId = localStorage.getItem('cognitoId');
    this.route.params.subscribe((params: Params): void => {
      this.equipmentId = params['id'];
      this.getDetail(this.equipmentId);
    });
        
      //this.router.navigateByUrl('used-equipment/sell-equipment');  
  }

  getDetail(id: number) {
    //this.spinner.show();
    this.usedEquipmentService.getProduct(id).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.vaahanDetail = res;
        this.vehicleDetailsChild.vehicleDetailsForm.patchValue({
         // productCondition: res.rate_equipment.toString(),
          productCondition: res.asset_condition.toString(),
          MotorOperatingHours: res.motor_operating_hours,
          Mileage: res.hmr_kmr,
          isValuationReport: res.valuation_request ? "1" : "0",
          isServiceLogs: res.is_service_log_available ? "1" : "0",
          valuationValue: res.valuation_amount,
          uploadDocument: "",
          // uploadServiceLogs:"",
          isParkingCharges: res.is_parking_charges_info?'1' : '0',
          parkedSince: res.parked_since? res.parked_since: null,
          parkingChargePerDay: res.parking_charge_per_day,
          bookValue: res.book_value,
          isOriginalInvoice: res.is_original_invoice ? '1' : '0',
          isPriceOnRequest: res.is_price_on_request ? '1' : '0',
          sellingPrice: res.selling_price
        });
        this.vehicleDetailsChild.getDetails(this.vaahanDetail);
        this.vaahanDetailsChild.getDetail(this.vaahanDetail);

      });
  }

  back() {
    this.router.navigate(['used-equipment/basic-details',this.equipmentId]);
  }

  next() {
    if (this.vehicleDetailsChild.vehicleDetailsForm.invalid) {
      this.vehicleDetailsChild.vehicleDetailsForm.markAllAsTouched();
      return;
    }
    const dialogRef = this.dialog.open(ConfirmedPopup1Component, {
      width: '500px',
      data: {
        message: this.vehicleDetailsChild.vehicleDetailsForm.get('sellingPrice')?.value,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        var sellEquipmentRequest = new sellEquipment();
  
        sellEquipmentRequest.category = this.vaahanDetail.category.id;
        sellEquipmentRequest.model = this.vaahanDetail.model.id ? this.vaahanDetail.model.id : this.vaahanDetail.model;
        sellEquipmentRequest.brand = this.vaahanDetail.brand.id;
        sellEquipmentRequest.info_level = 2;
        sellEquipmentRequest.seller = this.cognitoId;
        sellEquipmentRequest.is_equipment_registered = this.vaahanDetail.is_equipment_registered;
        if (this.vaahanDetail.is_equipment_registered) {
          sellEquipmentRequest.rc_number = this.vaahanDetail.rc_number;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('productCondition')?.value != "") {
          //sellEquipmentRequest.rate_equipment = this.vehicleDetailsChild.vehicleDetailsForm.get('productCondition')?.value;
          sellEquipmentRequest.asset_condition = this.vehicleDetailsChild.vehicleDetailsForm.get('productCondition')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('MotorOperatingHours')?.value != "") {
          sellEquipmentRequest.motor_operating_hours = this.vehicleDetailsChild.vehicleDetailsForm.get('MotorOperatingHours')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('Mileage')?.value != "") {
          sellEquipmentRequest.hmr_kmr = this.vehicleDetailsChild.vehicleDetailsForm.get('Mileage')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('isValuationReport')?.value != "") {
          sellEquipmentRequest.valuation_request = this.vehicleDetailsChild.vehicleDetailsForm.get('isValuationReport')?.value == 1 ? true : false;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('valuationValue')?.value != "") {
          sellEquipmentRequest.valuation_amount = this.vehicleDetailsChild.vehicleDetailsForm.get('valuationValue')?.value;
        }
    
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('isServiceLogs')?.value != "") {
          sellEquipmentRequest.is_service_log_available = this.vehicleDetailsChild.vehicleDetailsForm.get('isServiceLogs')?.value == 1 ? true : false;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('registrationNumber')?.value != "") {
          sellEquipmentRequest.rc_number = this.vaahanDetailsChild.vaahanDetailsForm.get('registrationNumber')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('chassisNumber')?.value != "") {
          sellEquipmentRequest.chassis_number = this.vaahanDetailsChild.vaahanDetailsForm.get('chassisNumber')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('engineNumber')?.value != "") {
          sellEquipmentRequest.engine_number = this.vaahanDetailsChild.vaahanDetailsForm.get('engineNumber')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('machineSerialNumber')?.value != "") {
          sellEquipmentRequest.machine_sr_number = this.vaahanDetailsChild.vaahanDetailsForm.get('machineSerialNumber')?.value;
        }
       
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('fuelType')?.value != "") {
          sellEquipmentRequest.fuel_type = this.fuelType[this.vaahanDetailsChild.vaahanDetailsForm.get('fuelType')?.value];
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('enginePower')?.value != "") {
          sellEquipmentRequest.engine_power = this.vaahanDetailsChild.vaahanDetailsForm.get('enginePower')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('grossWeight')?.value != "") {
          sellEquipmentRequest.gross_Weight = this.vaahanDetailsChild.vaahanDetailsForm.get('grossWeight')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('operationWeight')?.value != "") {
          sellEquipmentRequest.operating_weight = this.vaahanDetailsChild.vaahanDetailsForm.get('operationWeight')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('bucketCapacity')?.value != "") {
          sellEquipmentRequest.bucket_capacity = this.vaahanDetailsChild.vaahanDetailsForm.get('bucketCapacity')?.value;
        }
        if (this.vaahanDetailsChild.vaahanDetailsForm.get('liftingCapacity')?.value != "") {
          sellEquipmentRequest.lifting_capacity = this.vaahanDetailsChild.vaahanDetailsForm.get('liftingCapacity')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('isPriceOnRequest')?.value != "") {
          sellEquipmentRequest.is_price_on_request = this.vehicleDetailsChild.vehicleDetailsForm.get('isPriceOnRequest')?.value == 1 ? true : false;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('sellingPrice')?.value != 0) {
          sellEquipmentRequest.selling_price = this.vehicleDetailsChild.vehicleDetailsForm.get('sellingPrice')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('isParkingCharges')?.value != "") {
          sellEquipmentRequest.is_parking_charges_info = this.vehicleDetailsChild.vehicleDetailsForm.get('isParkingCharges')?.value== 1 ? true : false;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('parkedSince')?.value != "") {
          const date = this.datePipe.transform(this.vehicleDetailsChild.vehicleDetailsForm.get('parkedSince')?.value, 'YYYY-MM-dd');
          sellEquipmentRequest.parked_since = date || null;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('parkingChargePerDay')?.value != 0) {
          sellEquipmentRequest.parking_charge_per_day = this.vehicleDetailsChild.vehicleDetailsForm.get('parkingChargePerDay')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('isOriginalInvoice')?.value != "") {
          sellEquipmentRequest.is_original_invoice = this.vehicleDetailsChild.vehicleDetailsForm.get('isOriginalInvoice')?.value;
        }
        if (this.vehicleDetailsChild.vehicleDetailsForm.get('bookValue')?.value != "") {
          sellEquipmentRequest.book_value = this.vehicleDetailsChild.vehicleDetailsForm.get('bookValue')?.value;
        }
        this.usedEquipmentService.postupdateUsedEquipment(sellEquipmentRequest, this.vaahanDetail.id).pipe(finalize(() => {   })).subscribe(
          (res: any) => {
            this.router.navigate(['used-equipment/visuals',this.equipmentId]);
          },
          (err) => {
            this.notificationservice.error(err);
          }
    
        );
      }
    })
    
  }

  

}
