import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-confirmed-popup1',
  templateUrl: './confirmed-popup1.component.html',
  styleUrls: ['./confirmed-popup1.component.css']
})
export class ConfirmedPopup1Component implements OnInit {
  description:any;
  test:any = 'At what price do you want to sell your vehicle?';
  description1: string;
  constructor(public dialogRef: MatDialogRef<ConfirmedPopup1Component>,@Inject(MAT_DIALOG_DATA) data : any) {
    this.description = data.message;
    this.description1 = data.message + ' ' + 'Rupees Only';
   }
  ngOnInit(): void {
   
  }
  backClick(){
    this.dialogRef.close();
  }
}
