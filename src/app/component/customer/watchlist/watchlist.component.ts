import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  cognitoId?: any;
  productList: Array<any> = [];
  constructor(private storage: StorageDataService, private commonService: CommonService,public notify: NotificationService) { }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    let queryParam = 'user__cognito_id__iexact=' + this.cognitoId + '&type__in=2&limit=999&ordering=-id';
    this.getRecentlyViewed(queryParam);
  }

  getRecentlyViewed(queryParam) {
    this.commonService.getToWatchlist(queryParam).subscribe((res: any) => {
      console.log(res);
      this.productList = res.results;
    })
  }

  removeFromWatchlist(id, type) {
    //'asset_type='+'used'+
    let queryParam =
      'asset_type=' + type +
      '&asset_id=' + id +
      '&cognito_id=' + this.cognitoId;
    this.commonService.removeFromWatchlist(queryParam).subscribe((res: any) => {
      this.productList = [];
      this.notify.success('Asset successfully removed from Watchlist', true);
      let watchListQueryParam = 'user__cognito_id__iexact=' + this.cognitoId + '&type__in=2&limit=999&ordering=-id';
      this.getRecentlyViewed(watchListQueryParam);
    });
  }

}
