import { ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { PaymentService } from 'src/app/services/payment.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { RefundDialogComponent } from './refund-dialog/refund-dialog.component';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css']
})
export class MyWalletComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public filterForm!: FormGroup;
  public payAmount: any = null;
  public cognitoId!: string;
  public walletDetails: any;
  public walletBalance: number = 0;
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public maxDate = new Date();
  public userRole!: string;
  public listingData = [];
  constructor(public paymentService: PaymentService, public spinner: NgxSpinnerService, public storage: StorageDataService, public notify: NotificationService, private fb: FormBuilder, public changeDetect: ChangeDetectorRef, private datePipe: DatePipe, private dialog: MatDialog) {
    this.filterForm = this.fb.group({
      // type:[''],
      created_at__gte: [{ disabled: true, value: '' }],
      created_at__lte: [{ disabled: true, value: '' }]
    })
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getLocalStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getWalletDetails();
    this.getWalletBalance();
  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.total_count >  this.walletDetails.length) && ( this.walletDetails.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.page++;
         this.getWalletDetails();
        }
       }
     }
   }
 }

  getWalletDetails() {
    let detailsPayload = {
      page: this.page,
      user: this.cognitoId,
      limit: this.pageSize
    }
    if (this.filterForm.value.created_at__gte) {
      detailsPayload['created_at__gte'] = this.datePipe.transform(this.filterForm.value.created_at__gte, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    if (this.filterForm.value.created_at__lte) {
      let date:any = new Date(this.filterForm.get('created_at__lte')?.value);
      date = date.setDate(date.getDate() + 1);
      detailsPayload['created_at__lte'] = this.datePipe.transform(date, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    this.paymentService.getWalletDetails(detailsPayload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.total_count = data.count;
        this.walletDetails = data.results;
         // Changes for Card layout on Mobile devices
         if(window.innerWidth > 768){
          this.walletDetails = data.results;
          }else{
            this.scrolled = false;
            if(this.page == 1){
              this.walletDetails = [];
              this.listingData = [];
              this.listingData = data.results;
              this.walletDetails = this.listingData;
            }else{
              this.listingData = this.listingData.concat(data.results);
              this.walletDetails = this.listingData;
            }
          }
        this.changeDetect.detectChanges();
      },
      err => {
        console.log(err);
      }
    )
  }

  getWalletBalance() {
    let walletPayload = {
      user: this.cognitoId
    }
    this.paymentService.getWalletBalance(walletPayload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.walletBalance = data?.total;
        this.changeDetect.detectChanges();
      },
      err => {
        console.log(err);
      }
    )
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getWalletDetails();
  }

  applyFiter() {
    this.getWalletDetails();
  }

  resetFiter(){
    this.filterForm.reset();
    this.getWalletDetails();
  }

  generateOrderId(type: string, refundDetails?: any) {
    if ((this.payAmount && this.payAmount != 0) || (refundDetails && refundDetails?.amount != 0)) {
      if ((this.payAmount > 1000000) || (refundDetails?.amount > 1000000)) {
        this.notify.error('The system allows online payment up to 10 lacs only in a single-time. For making payments above 10 lacs, please contact our Team.');
        return;
      }
      let payload = {
        user: this.cognitoId,
        amount: refundDetails ? +(refundDetails?.amount) : (+this.payAmount),
        usage_type: (type === 'add') ? 'LM' : 'RF',
        transaction_type: (type === 'add') ? 'Cr' : 'Dr'
      }
      if (refundDetails) {
        payload['transaction_id'] = refundDetails?.transaction_id
      }
      this.paymentService.generateWalletOrderId(payload).pipe(finalize(() => { })).subscribe(
        (data: any) => {
          if (!refundDetails && (data.status == 1)) {
            this.payWithRazor(data);
            this.payAmount = null;
          } else {
            if (data.status == 2) {
              this.notify.success('Amount Refund Successfully!!');
              this.getWalletBalance();
              this.getWalletDetails();
            }
          }
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.notify.warn('Please enter the amount to be Credit or Debit and it must be greater than 0!!')
    }
  }

  payWithRazor(data) {
    data.description = "iQuippo Wallet Payment";
    this.paymentService.payWithRazor(data).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.notify.success('Money added successfully!!', true);
        if (res) {
          this.updateWalletTransaction(data, res);
        }
      },
      err => {
        this.updateWalletTransaction(data);
        this.notify.error(err);
      }
    )
  }

  /**
   * @param data : Containes info regarding order_id, txn_id etc.
   * @param res : Razorpay response
   */
  updateWalletTransaction(data, res?) {
    let payload = {
      payload: res ? JSON.stringify(res) : null,
      status: res ? 2 : 4,
    }
    if (res) {
      payload['payment_gateway_payment_id'] = res.razorpay_payment_id;
    }
    this.paymentService.updateWalletTransactionDetails(data?.id, payload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.notify.success('Wallet transaction details updated!!');
        this.getWalletBalance();
        this.getWalletDetails();
      },
      err => {
        console.log(err);
      }
    )
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Transaction Inititated'
        break;
      }
      case 2: {
        return 'Transaction Successfull'
        break;
      }
      case 3: {
        return 'Transaction Failure'
        break;
      }
      case 4: {
        return 'Transaction Cancelled'
        break;
      }
      default: {
        return 'Transaction Inititated'
        break;
      }
    }
  }

  getRefundIconStatus(wallet) {
    if ((!wallet?.is_refunded) && (wallet?.status == 2) && (wallet.transaction_type == 'Cr') && (this.walletBalance > 0)) {
      return true;
    } else {
      return false;
    }
  }

  refund(walletDetails) {
    let payload = {
      user: this.cognitoId,
      transaction_id: walletDetails?.transaction_id
    }
    this.paymentService.getTransactionRefundStatus(payload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        const makeEnquiryDialogRef = this.dialog.open(RefundDialogComponent, {
          width: '500px',
          disableClose: true
        });

        makeEnquiryDialogRef.afterClosed().subscribe(val => {
          if (val) {
            let maxRefundAmount = walletDetails?.amount - data?.refunded_amount;
            let refundAmount;
            if (maxRefundAmount >= this.walletBalance) {
              refundAmount = this.walletBalance;
            } else {
              refundAmount = maxRefundAmount;
            }
            if (val?.refund_amount && (refundAmount >= val?.refund_amount)) {
              let refundDetails = {
                transaction_id: walletDetails?.transaction_id,
                amount: val?.refund_amount
              }
              this.generateOrderId('refund', refundDetails);
            } else {
              this.notify.error('For this transaction refund amount should not be more than' + ' ' + refundAmount);
            }
          }
        })
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    )

  }

  export() {
    let payload = {
      limit: 999,
      user: this.cognitoId
    }
    this.paymentService.getWalletDetails(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Transaction Id": r?.transaction_id,
            "Amount": r?.amount,
            "Transaction Type": (r?.transaction_type == 'Cr') ? 'Credit' : 'Debit',
            "Date": this.datePipe.transform(r?.created_at, 'yyyy-MM-dd, h:mm:ss a')
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "My Wallet Transaction Details");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

}