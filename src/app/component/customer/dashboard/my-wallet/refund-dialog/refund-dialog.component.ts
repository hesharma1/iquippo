import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-refund-dialog',
  templateUrl: './refund-dialog.component.html',
  styleUrls: ['./refund-dialog.component.css']
})
export class RefundDialogComponent implements OnInit {

  refundForm: FormGroup;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<RefundDialogComponent>) { 
    this.refundForm = this.fb.group({
      refund_amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
    })
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }

  submit() {
    let payload = Object.assign({}, this.refundForm.value);
    this.dialogRef.close(payload);
  }

}
