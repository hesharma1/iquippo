import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { AuctionService } from 'src/app/services/auction.service';

@Component({
  selector: 'app-bid-action',
  templateUrl: './bid-action.component.html',
  styleUrls: ['./bid-action.component.css']
})
export class BidActionComponent implements OnInit {
  @ViewChild('imageInput') imageInput!: ElementRef;
  type: any;
  bidactionform?: FormGroup;
  invoiceActionForm?: FormGroup;
  doGenerationForm?: FormGroup;
  sellerInvoiceForm?: FormGroup;
  dateOfDeliveryForm?: FormGroup;
  modifyForm?: FormGroup;
  amount?: number;
  equipment?: any;
  buyer?: string;
  tradeId?: number;
  valuationAmount?: any;
  parkingCharges?: any;
  assetId?: string;
  assetName?: any;
  reservePrice?: any;
  yearOfMfg?: any;
  statusType?: any;
  feedback?: any = '';
  feedback_text?: any = '';
  isSelfInvoice: boolean = true;
  firstNameInvoice?: any;
  lastNameInvoice?: any;
  countryInvoice?: any;
  stateInvoice?: any;
  locationInvoice?: any;
  addressInvoice?: any;
  emailInvoice?: any;
  mobileNoInvoice?: any;
  countryList = [];
  locationList: Array<any> = [];
  states = [];
  sellerList: any = [];
  isOtherCustDetailAva: boolean = true;
  DO_DocumentName: Array<object> = [];
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  isConfirming: boolean = false;
  uploadDocument: any;
  mainData: any;
  tradeInfo: any;
  isFromFulfilment: boolean = false;
  sellerCognitoId: any;
  minDate: Date = new Date();

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<BidActionComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private adminMasterService: AdminMasterService,
    private router: Router,
    public s3: S3UploadDownloadService,
    private datePipe: DatePipe,
    public notify: NotificationService,
    private usedEquipmentService: UsedEquipmentService,
    private auctionService: AuctionService
  ) {
    if (data) {
      this.mainData = data;
      this.type = data?.type;
      this.isFromFulfilment = data?.isFromFulfilment;
      this.tradeInfo = data?.tradeInfo;
      this.amount = data?.amount;
      this.equipment = data?.equipmentId;
      this.buyer = data?.buyer;
      this.tradeId = data?.id;
      this.valuationAmount = data?.valuationAmount;
      this.reservePrice = data?.reservePrice;
      this.assetName = data?.assetName;
      this.yearOfMfg = data?.yearOfMfg;
      this.parkingCharges = data?.parkingCharges;
      this.statusType = data?.statusType;
    }    

    if(this.type === 'rejectTransaction'){
      this.bidActionForm();
    }else if(this.type === 'Invoice'){
      this.invoiceForm();
    }else if(this.type === 'DO'){
      this.doGeneration();
    }else if(this.type === 'DOD'){
      this.dodForm();
    }else if(this.type === 'SelInvoice'){
      this.sellerInvoice();
    }else if(this.type === 'Modify'){
      this.modifyForm = this.fb.group({
        bid_amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      })
    }        
  }

  ngOnInit(): void {
    if (this.type == 'rejectTransaction' && this.tradeInfo?.partial_payment?.length && !this.isFromFulfilment) {
      this.bidactionform?.controls.refund_type.setValidators([Validators.required]);
      this.bidactionform?.controls.refund_type.updateValueAndValidity();
    }

    if(this.isFromFulfilment){
      this.equipment = this.mainData?.mainData?.asset_ids[0];
    }
  }

  invoiceForm() {
    this.invoiceActionForm = new FormGroup({
      isSelfInvoice: new FormControl('1'),
      firstNameInvoice: new FormControl({ value: '', disabled: true }),
      lastNameInvoice: new FormControl({ value: '', disabled: true }),
      emailInvoice: new FormControl({ value: '', disabled: true }),
      mobileNoInvoice: new FormControl('')
    });
  }

  doGeneration() {
    this.doGenerationForm = new FormGroup({
      uploadDocument: new FormControl(''),
      creationDate: new FormControl('')
    })
  }

  sellerInvoice() {
    this.sellerInvoiceForm = new FormGroup({
      uploadDocument: new FormControl('')
    });
  }

  dodForm() {
    this.dateOfDeliveryForm = new FormGroup({
      dateOfDelivery: new FormControl('')
    })
  }

  bidActionForm(){
    this.bidactionform = this.fb.group({
      comments: ['', Validators.required],
      reason: ['', Validators.required],
      refund_type: [''],
      refund_amount: [''],
    });
  }

  getAllStatesForCountry(queryParams: string, countryId: number) {
    return this.adminMasterService.getStateMaster(queryParams).subscribe((res: any) => {
      const allStates = res.results;
      this.states = allStates.filter(l => l.country.id === countryId);
    },
      err => {
        console.log(err);
      }
    )
  }

  getAllLocationsForState(queryParams: string, stateId: number) {
    return this.adminMasterService.getLocationMaster(queryParams).subscribe(
      (res: any) => {
        const allLocations = res.results;
        this.locationList = allLocations.filter(l => l.city.state.id === stateId);
      },
      err => {
        console.log(err);
      }
    )
  }

  backClick() {
    this.dialogRef.close();
  }

  updateInvoice() {
    if (this.isFromFulfilment) {
      const putDOUpdate = {
        invoice_in_favour_of: this.sellerCognitoId,
      };

      this.auctionService.UpdateFulfilmentPatchReq(putDOUpdate, this.mainData?.mainData?.id).subscribe((res: any) => {
        this.dialogRef.close('InvoiceUpdated');
        this.notify.success("Invoice Detail Updated", true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
    else {
      this.isSelfInvoice = this.invoiceActionForm?.get('isSelfInvoice')?.value == "1" ? true : false;
      var updateInvoiceDetail;
      if (this.isSelfInvoice) {
        updateInvoiceDetail = {
          amount: this.amount,
          equipment: this.equipment,
          buyer: this.buyer,
          is_self_invoice: this.isSelfInvoice,
          invoice_first_name: null,
          invoice_last_name: null,
          invoice_email: null,
          invoice_address: null,
          invoice_mobile: null,
          invoice_pin_code: null
        }
      }
      else {
        updateInvoiceDetail = {
          amount: this.amount,
          equipment: this.equipment,
          buyer: this.buyer,
          is_self_invoice: this.isSelfInvoice,
          invoice_first_name: this.invoiceActionForm?.get('firstNameInvoice')?.value,
          invoice_last_name: this.invoiceActionForm?.get('lastNameInvoice')?.value,
          invoice_email: this.invoiceActionForm?.get('emailInvoice')?.value,
          invoice_mobile: this.invoiceActionForm?.get('mobileNoInvoice')?.value
        }
      }
      this.adminMasterService.puttradeBid(updateInvoiceDetail, this.tradeId).subscribe((data: any) => {
        this.dialogRef.close('InvoiceUpdated');
        this.notify.success("Invoice Detail Updated", true);
      });
    }
  }

  approveBid() {
    const putTradeBody = {
      equipment: this.equipment,
      bid_status: 'SP',
      deal_status: 'CP',
      offer_status: 'BA'
    };
    const putUsedBody = {
      status: 3
    }
    forkJoin([
      this.adminMasterService.puttradeBid(putTradeBody, this.tradeId),
      this.adminMasterService.putUsedEquipmentById(putUsedBody, this.equipment)
    ]).subscribe((res: any) => {
      this.dialogRef.close();
      this.notify.success("Bid Approved", true);
      this.router.navigate(['/customer/dashboard/my-trade/trade-details-dashboard']);
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  rejectTransaction() {
    if (this.isFromFulfilment) {
      this.dialogRef.close({'rejectTransaction' : true ,'reject_reason':this.bidactionform?.get('reason')?.value , 'reject_comment': this.bidactionform?.get('comments')?.value});
      // const payload = {
      //   reject_reason: this.bidactionform?.get('reason')?.value,
      //   reject_comment: this.bidactionform?.get('comments')?.value
      // };
      // this.auctionService.UpdateFulfilmentPatchReq(payload, this.mainData?.mainData?.id).subscribe((res: any) => {
      //   this.notify.success("Transaction Rejected", true);
      // }, (err: any) => {
      //   console.log(err?.message);
      // });
    }
    else {
      const putRejectTransTradeBody = {
        amount: this.amount,
        equipment: this.equipment,
        bid_status: 'CL',
        deal_status: 'CP',
        offer_status: this.tradeInfo?.trade_type == "BID" ? 'RE' : 'OR',
        rejection_reason: this.bidactionform?.get('reason')?.value,
        rejection_comment: this.bidactionform?.get('comments')?.value
      };
      if (this.tradeInfo?.partial_payment?.length) {
        if (this.bidactionform?.get('refund_type')?.value == '') {
          this.notify.error('Please select valid Refund Type !!', true);
          return;
        } else {
          putRejectTransTradeBody['refund_type'] = this.bidactionform?.get('refund_type')?.value;
        }
        if (this.bidactionform?.get('refund_type')?.value == 'PL') {
          if (this.bidactionform?.get('refund_amount')?.value > this.tradeInfo?.partial_payment[0]?.amount) {
            this.notify.error('Refund amount should be less than or equal to Partial payment amount!!', true);
            return;
          } else {
            putRejectTransTradeBody['refund_amount'] = this.bidactionform?.get('refund_amount')?.value;
          }
        }
      }
      this.adminMasterService.puttradeBid(putRejectTransTradeBody, this.tradeId).subscribe((data: any) => {
        this.dialogRef.close();
        this.notify.success("Transaction Rejected", true);
        this.router.navigate(['/customer/dashboard/my-trade/trade-details-dashboard']);
      });
    }
  }

  updateValidation(val) {
    if (val == 'PL') {
      this.bidactionform?.controls.refund_amount.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
      this.bidactionform?.controls.refund_amount.updateValueAndValidity();
    } else {
      this.bidactionform?.controls.refund_amount.setValidators([]);
      this.bidactionform?.controls.refund_amount.updateValueAndValidity();
    }
  }

  feedBackAccepted() {
    const putFeedbackObject = {
      feedback: this.feedback,
      feedback_text: this.feedback_text
    }
    if(this.isFromFulfilment){
      this.auctionService.UpdateFulfilmentPatchReq(putFeedbackObject, this.mainData?.mainData?.id).subscribe((res: any) => {
        this.dialogRef.close();
        this.notify.success("Feedback Received", true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }else{
      this.adminMasterService.puttradeBid(putFeedbackObject, this.tradeId).subscribe((data: any) => {
        this.dialogRef.close();
        this.notify.success("Feedback Received", true);
      }, (err: any) => {
        console.log(err);
      })
    }
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/" + this.equipment + '/' + tab + '/' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        if (tab == 'Seller_Invoice') {
          this.sellerInvoiceForm?.get('uploadDocument')?.setValue(uploaded_file.Location);
        }
        else {
          this.doGenerationForm?.get('uploadDocument')?.setValue(uploaded_file.Location);
        }

      }
    } else {
      this.notify.error('Selected document should be pdf and word format', true);
    }
    this.imageInput.nativeElement.value = '';
  }

  uploadSellerInvoice() {
    if (this.isFromFulfilment) {
      let uploadDocUrl = this.sellerInvoiceForm?.get('uploadDocument')?.value;
      const putDOUpdate = {
        invoice_url: uploadDocUrl,
        deal_status: 'IG'
      };
      this.auctionService.UpdateFulfilmentPatchReq(putDOUpdate, this.mainData?.mainData?.id).subscribe((res: any) => {
        this.dialogRef.close('Invoice Uploaded');
        this.notify.success('Invoice Uploaded', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
    else {
      let uploadDocUrl = this.sellerInvoiceForm?.get('uploadDocument')?.value;
      const putDOUpdate = {
        amount: this.amount,
        equipment: this.equipment,
        seller_invoice: uploadDocUrl,
        offer_status: 'IG'
      };
      this.adminMasterService.puttradeBid(putDOUpdate, this.tradeId).subscribe((res: any) => {
        this.dialogRef.close('Sales Invoice Uploaded');
        this.notify.success('Seller Invoice Uploaded', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
  }

  DODetailSubmited() {
    if (this.isFromFulfilment) {
      let uploadDocUrl = this.doGenerationForm?.get('uploadDocument')?.value;
      const customeDate = this.doGenerationForm?.get('creationDate')?.value;
      const putDOUpdate = {
        delivery_order_url: uploadDocUrl,
        delivery_order_issue_date: customeDate,
        bid_status: "CL",
        deal_status: 'DI'
      };
      this.auctionService.UploadIssueDO(putDOUpdate, this.mainData?.mainData?.id).subscribe((res: any) => {
        this.dialogRef.close('DO Issued');
        this.notify.success('DO Issued successfully !!', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
    else {
      let uploadDocUrl = this.doGenerationForm?.get('uploadDocument')?.value;
      const customeDate = this.datePipe.transform(this.doGenerationForm?.get('creationDate')?.value, 'YYYY-MM-dd');
      const putDOUpdate = {
        amount: this.amount,
        equipment: this.equipment,
        do_document: uploadDocUrl,
        do_created_at: customeDate,
        is_do_generated: true,
        offer_status: 'DG'
      };
      this.adminMasterService.puttradeBid(putDOUpdate, this.tradeId).subscribe((res: any) => {
        this.dialogRef.close('DO Issued');
        this.notify.success('DO Issued successfully !!', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
  }

  addValidations(event: any) {
    let value = event.value;
    if (value == "1") {
      this.isOtherCustDetailAva = true;
      this.invoiceActionForm?.get('firstNameInvoice')?.clearValidators();
      this.invoiceActionForm?.get('firstNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('lastNameInvoice')?.clearValidators();
      this.invoiceActionForm?.get('lastNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('emailInvoice')?.clearValidators();
      this.invoiceActionForm?.get('emailInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('mobileNoInvoice')?.clearValidators();
      this.invoiceActionForm?.get('mobileNoInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('emailInvoice')?.setValue('');
      this.invoiceActionForm?.get('mobileNoInvoice')?.setValue('');
      this.invoiceActionForm?.get('firstNameInvoice')?.setValue('');
      this.invoiceActionForm?.get('lastNameInvoice')?.setValue('');
      this.sellerList = [];
    }
    else {
      this.invoiceActionForm?.get('mobileNoInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('mobileNoInvoice')?.updateValueAndValidity();
    }
  }

  rejectDOD() {
    this.dialogRef.close();
  }

  confirmDOD() {
    if (this.isFromFulfilment) {
      const putDOUpdate = {
        delivery_date: this.dateOfDeliveryForm?.get('dateOfDelivery')?.value,
        deal_status: 'AD'
      };
      this.auctionService.UpdateFulfilmentPatchReq(putDOUpdate, this.mainData?.mainData?.id).subscribe((res: any) => {
        this.dialogRef.close();
        this.notify.success('Date of delivery confirm', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
    else {
      const customeDOD_Date = this.datePipe.transform(this.dateOfDeliveryForm?.get('dateOfDelivery')?.value, 'YYYY-MM-dd');
      const putDODUpdate = {
        amount: this.amount,
        equipment: this.equipment,
        asset_delivery_date: customeDOD_Date,
        offer_status: 'AD'
      };
      const putAssetUpdate = {
        status: 5
      };
      forkJoin([
        this.adminMasterService.puttradeBid(putDODUpdate, this.tradeId),
        this.adminMasterService.putUsedEquipmentById(putAssetUpdate, this.equipment)
      ]).subscribe((res: any) => {
        this.dialogRef.close();
        this.notify.success('Date of delivery confirm', true);
      }, (err: any) => {
        console.log(err?.message);
      });
    }
  }

  DeliveryDateSubmit() {
    this.isConfirming = true;
  }

  setLocalStorage() {
    localStorage.setItem('valuationAmount', this.valuationAmount);
    localStorage.setItem('reservePrice', this.reservePrice);
    localStorage.setItem('assetId', this.equipment);
    localStorage.setItem('assetName', this.assetName);
    localStorage.setItem('yearOfMfg', this.yearOfMfg);
    localStorage.setItem('parkingCharges', this.parkingCharges);
    localStorage.setItem('tradeStatusType', this.statusType);
    localStorage.setItem('tradeAssetId', this.equipment);
  }

  bidWithdraw() {
    const putWithDrawnBody = {
      amount: this.amount,
      equipment: this.equipment,
      bid_status: 'CL',
      deal_status: 'CP',
      offer_status: 'BW'
    };
    this.adminMasterService.puttradeBid(putWithDrawnBody, this.tradeId).subscribe((res: any) => {
      this.dialogRef.close("Withdrawn");
      this.notify.success("Bid Withdrawn", true);
    });
  }

  updateBid(value) {
    if (this.mainData?.amount > value?.bid_amount) {
      this.notify.error('You cannot place a bid lower than your last bid which was ' + this.mainData?.amount);
      return;
    }
    let payload = {
      type: 'Modify',
      bid_amount: value?.bid_amount
    }
    this.dialogRef.close(payload);
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    this.sellerList = [];

    if (event.keyCode != 8 && !pattern.test(event.key)) {
      event.preventDefault();
    }
    else if (this.invoiceActionForm?.controls['mobileNoInvoice'].value.length == 0) {
      this.sellerList = [];
      this.invoiceActionForm?.get('emailInvoice')?.setValue('');
      this.invoiceActionForm?.get('mobileNoInvoice')?.setValue('');
      this.invoiceActionForm?.get('firstNameInvoice')?.setValue('');
      this.invoiceActionForm?.get('lastNameInvoice')?.setValue('');
    }
    else if (this.invoiceActionForm?.controls['mobileNoInvoice'].value.length >= 9) {
      this.searchSeller(event);
    }
  }

  searchSeller(e) {
    let payload = {
      "mobile_number__icontains": e.target.value,
      "groups__name__in": "Customer",
      "is_active": 'true'
    }
    this.sellerList = [];
    this.usedEquipmentService.getInvoiceCustomer(payload).subscribe(
      (res: any) => {
        if (res?.results && res?.results.length) {
          this.sellerList = res.results;
          this.isOtherCustDetailAva = true;
        } else {
          this.sellerList = [];
          this.isOtherCustDetailAva = false;
        }
      },
      (error: any) => {
        if (error.message == "No such user exists with valid Agreement") {
          error.message == "This customer is not registered with us. Kindly ask him to register on our portal."
          this.isOtherCustDetailAva = false;
        }
        this.sellerList = [];
      }
    );
  }

  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      if (res.mobile_number == search) {
        this.selectSeller(res);
      }
    });
  }

  getDocumentName(str) {
    return str.slice(str.lastIndexOf("/") + 1);
  }

  selectSeller(seller) {
    this.sellerCognitoId = seller.cognito_id;
    this.invoiceActionForm?.get('emailInvoice')?.setValue(seller.email);
    this.invoiceActionForm?.get('mobileNoInvoice')?.setValue(seller.mobile_number);
    this.invoiceActionForm?.get('firstNameInvoice')?.setValue(seller.first_name);
    this.invoiceActionForm?.get('lastNameInvoice')?.setValue(seller.last_name);
  }

  deleteFile(tab, key, imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!', true);
        if (tab == 'Seller_Invoice') {
          this.sellerInvoiceForm?.get(key)?.setValue('');
        }
        else {
          this.doGenerationForm?.get(key)?.setValue('');
        }
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }
}
