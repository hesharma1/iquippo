import { AfterViewInit, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { BidActionComponent } from '../bid-action/bid-action.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatSort } from '@angular/material/sort';
import { SharedService } from 'src/app/services/shared-service.service';
import { finalize } from 'rxjs/operators';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { CommonService } from 'src/app/services/common.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { PaymentService } from 'src/app/services/payment.service';
import { SigninComponent } from 'src/app/component/signin/signin.component';

@Component({
    selector: 'app-trade-asset-details',
    templateUrl: './trade-asset-details.component.html',
    styleUrls: ['./trade-asset-details.component.css']
})
export class TradeAssetDetailsComponent implements OnInit {
    tradeApprovalData: any[] = [];
    tradeProgressData: any[] = [];
    tradeClosedData: any[] = [];
    tradeApprovedData: any[] = [];
    tradeClosedBidForBuyer: any[] = [];
    isPaymentDone: boolean = false;
    public page: number = 1;
    public pageSize: number = 10;
    isDateOfDelivery: boolean = false;
    assetStatusObject = [{
        "id": 0,
        "display_name": "DRAFT"
    }, {
        "id": 1,
        "display_name": "PENDING"
    }, {
        "id": 2,
        "display_name": "APPROVED"
    }, {
        "id": 3,
        "display_name": "Sale in Progress"
    }, {
        "id": 4,
        "display_name": "REJECTED"
    }, {
        "id": 5,
        "display_name": "SOLD"
    }];
    assetStatus?: string;
    public cognitoId?: string;
    public userRole?: string;
    public searchText!: string;
    public ordering: string = '-id';
    isFullPaymentButtonActive: boolean = false;
    showInvoice: boolean = false;
    tradeApprovalDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeProgressDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeClosedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeApprovedBuyerDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeClosedBuyerDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeApprovalScrolled: boolean = false;
    tradeProgressScrolled: boolean = false;
    tradeClosedScrolled: boolean = false;
    tradeApprovedBuyerScrolled: boolean = false;
    tradeClosedBuyerScrolled: boolean = false;
    public tradeApprovalListingData = [];
    public tradeProgressListingData = [];
    public tradeClosedListingData = [];
    public tradeApprovedBuyerListingData = [];
    public tradeClosedBuyerListingData = [];
    public selectedParentTabIndex: number = 0;
    public selectedChildTabIndex: number = 0;
    public total_count: any;
    approvalDisplayedColumns = ["id", "product_name", "tradeType", "no_of_bids", "max_bid", "pin_code", "status", "actions"];
    progressDisplayedColumns = ["id", "product_name", "tradeType", "no_of_bids", "max_bid", "pin_code", "status", "actions"];
    closedDisplayedColumns = ["id", "equipment__id", "equipment__product_name", "trade_type", "equipment__no_of_bids", "equipment__pin_code", "equipment__selling_price", "emd_details__amount", "amount", "offer_status", "bid_status", "deal_status", "equipment__status", "do_document", "seller_invoice", "actions"];
    approvedForBuyerDisplayedColumns = ["id", "equipment__id", "equipment__product_name", "equipment__pin_code", "trade_type", "amount", "offer_status", "bid_status", "deal_status", "invoice_In_Favour_Of", "delivery_Order", "full_Payment", "actions"];
    closedForBuyerDisplayedColumns = ["id", "equipment__id", "equipment__product_name", "equipment__pin_code", "trade_type", "amount", "offer_status", "bid_status", "deal_status", "Feedback", "actions"];
    @ViewChild(MatSort) sort!: MatSort;

    constructor(
        private dialog: MatDialog,
        private adminMasterService: AdminMasterService,
        private router: Router,
        public storage: StorageDataService,
        public sharedService: SharedService,
        public apiService: UsedEquipmentService,
        private spinner: NgxSpinnerService,
        public notify: NotificationService,
        private commonService: CommonService,
        private appRouteEnum: AppRouteEnum,
        public paymentService: PaymentService
    ) {

    }

    ngOnInit() {
        this.cognitoId = this.storage.getStorageData('cognitoId', false);
        this.userRole = this.storage.getStorageData('userRole', false);
        this.getData();
    }

    getApprovedTradeAssetData() {
        let queryparams = `status__in=2&bid__bid_status__in=PA&bid__trade_type__in=BID`
        let payloadClosedBuyer = {
            is_sell: true,
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize,
            seller__cognito_id__in: this.cognitoId
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeApprovalData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeApprovalDataSource.data = data.results;
                } else {
                    this.tradeApprovalScrolled = false;
                    if (this.page == 1) {
                        this.tradeApprovalDataSource.data = [];
                        this.tradeApprovalListingData = [];
                        this.tradeApprovalListingData = data.results;
                        this.tradeApprovalDataSource.data = this.tradeApprovalListingData;
                    } else {
                        this.tradeApprovalListingData = this.tradeApprovalListingData.concat(data.results);
                        this.tradeApprovalDataSource.data = this.tradeApprovalListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    getProgessedAssetData() {
        let queryparams = `status__in=3`
        let payloadClosedBuyer = {
            is_sell: true,
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize,
            seller__cognito_id__in: this.cognitoId
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeProgressData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeProgressDataSource.data = data.results;
                } else {
                    this.tradeProgressScrolled = false;
                    if (this.page == 1) {
                        this.tradeProgressDataSource.data = [];
                        this.tradeProgressListingData = [];
                        this.tradeProgressListingData = data.results;
                        this.tradeProgressDataSource.data = this.tradeProgressListingData;
                    } else {
                        this.tradeProgressListingData = this.tradeProgressListingData.concat(data.results);
                        this.tradeProgressDataSource.data = this.tradeProgressListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    getClosedTradeAssetData() {
        let queryparams = `bid_status__in=CL`
        let payloadClosedBuyer = {
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize,
            equipment__seller__cognito_id__in: this.cognitoId
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeClosedData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeClosedDataSource.data = data.results;
                } else {
                    this.tradeClosedScrolled = false;
                    if (this.page == 1) {
                        this.tradeClosedDataSource.data = [];
                        this.tradeClosedListingData = [];
                        this.tradeClosedListingData = data.results;
                        this.tradeClosedDataSource.data = this.tradeClosedListingData;
                    } else {
                        this.tradeClosedListingData = this.tradeClosedListingData.concat(data.results);
                        this.tradeClosedDataSource.data = this.tradeClosedListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    getApprovedBidForBuyer() {
        let queryparams = `bid_status__in=SP,PA`;
        const payloadApprovedBuyer = {
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize,
            cognito_id: this.cognitoId,
            role: this.userRole
        }
        if (this.searchText) {
            payloadApprovedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadApprovedBuyer).subscribe(
            (data: any) => {
                this.tradeApprovedData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeApprovedBuyerDataSource.data = data.results;

                } else {
                    this.tradeApprovedBuyerScrolled = false;
                    if (this.page == 1) {
                        this.tradeApprovedBuyerDataSource.data = [];
                        this.tradeApprovedBuyerListingData = [];
                        this.tradeApprovedBuyerListingData = data.results;
                        this.tradeApprovedBuyerDataSource.data = this.tradeApprovedBuyerListingData;
                    } else {
                        this.tradeApprovedBuyerListingData = this.tradeApprovedBuyerListingData.concat(data.results);
                        this.tradeApprovedBuyerDataSource.data = this.tradeApprovedBuyerListingData;
                    }
                }
                console.log(" this.tradeApprovedBuyerDataSource.data ", this.tradeApprovedBuyerDataSource.data);
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    getClosedBidForBuyer() {
        let queryparams = `bid_status__in=CL&deal_status__in=CP&offer_status=[BR,RE]`
        let payloadClosedBuyer = {
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize,
            cognito_id: this.cognitoId,
            role: this.userRole
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeClosedBidForBuyer = data.results;
                this.total_count = data.count;
                this.tradeClosedBuyerDataSource.data = data.results;

                if (window.innerWidth > 768) {
                    this.tradeClosedBuyerDataSource.data = data.results;
                } else {
                    this.tradeClosedBuyerScrolled = false;
                    if (this.page == 1) {
                        this.tradeClosedBuyerDataSource.data = [];
                        this.tradeClosedBuyerListingData = [];
                        this.tradeClosedBuyerListingData = data.results;
                        this.tradeClosedBuyerDataSource.data = this.tradeClosedBuyerListingData;
                    } else {
                        this.tradeClosedBuyerListingData = this.tradeClosedBuyerListingData.concat(data.results);
                        this.tradeClosedBuyerDataSource.data = this.tradeClosedBuyerListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    getData() {
        switch (this.selectedParentTabIndex) {
            case 0: {
                switch (this.selectedChildTabIndex) {
                    case 0: {
                        this.getApprovedTradeAssetData();
                        break;
                    }
                    case 1: {
                        this.getProgessedAssetData();
                        break;
                    }
                    case 2: {
                        this.getClosedTradeAssetData();
                        break;
                    }
                    default: {
                        this.getApprovedTradeAssetData();
                        break;
                    }
                }
                break;
            }
            case 1: {
                switch (this.selectedChildTabIndex) {
                    case 0: {
                        this.getApprovedBidForBuyer();
                        break;
                    }
                    case 1: {
                        this.getClosedBidForBuyer();
                        break;
                    }
                    default: {
                        this.getApprovedBidForBuyer();
                        break;
                    }
                }
                break;
            }
            default: {
                this.selectedParentTabIndex = 0;
                this.selectedChildTabIndex = 0;
                this.getData();
                break;
            }
        }
    }

    onSortColumn(event, type: any) {
        this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
        this.page = 1;
        this.pageSize = 10;
        this.getData();
        // if (type == "approvedBuyer") {
        //     this.getApprovedBidForBuyer();
        // }
        // else if (type == "closedBuyer") {
        //     this.getClosedBidForBuyer();
        // }
        // else if (type == "closedSeller") {
        //     this.getClosedTradeAssetData();
        // }
        // else if (type == "progressSeller") {
        //     this.getProgessedAssetData()
        // }
        // else if (type == "pendingSeller") {
        //     this.getApprovedTradeAssetData();
        // }
    }

    onPageChange(event: PageEvent, type) {
        this.page = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.getData();
    }

    searchInList(type: string) {
        if (this.searchText === '' || this.searchText.length > 3) {
            this.page = 1;
            this.pageSize = 10;
            this.getData();
            // if (type == "approvedBuyer") {
            //     this.getApprovedBidForBuyer();
            // }
            // else if (type == "closedBuyer") {
            //     this.getClosedBidForBuyer();
            // }
            // else if (type == "closedSeller") {
            //     this.getClosedTradeAssetData();
            // }
            // else if (type == "progressSeller") {
            //     this.getProgessedAssetData()
            // }
            // else if (type == "pendingSeller") {
            //     this.getApprovedTradeAssetData();
            // }
        }

    }

    setassetStatus(objectStatus) {
        let statusObj = this.assetStatusObject.find(x => { return x.id == objectStatus });
        return statusObj?.display_name;
    }

    moreInfoDialog(statusType: string, id: any, userType?: any) {
        const dialogRef = this.dialog.open(InfoDialogComponent, {
            width: '800px',
            data: {
                statusType: statusType,
                userType: userType,
                id: id
            }
        });
    }

    viewBidDialog(statusType: string, id: any) {
        if (statusType == 'approved') {
            this.setDataForViewBid(this.tradeApprovalData, id);
        }
        else {
            this.setDataForViewBid(this.tradeProgressData, id);
        }
        localStorage.setItem('tradeStatusType', statusType);
        localStorage.setItem('tradeAssetId', id);
        this.router.navigate(['./customer/dashboard/my-trade/view-trade-details'])
    }

    changeOnClickStatus(event: MatTabChangeEvent) {
        this.selectedChildTabIndex = event.index;
        this.page = 1;
        this.pageSize = 10;
        this.searchText = '';
        this.ordering = '-id';
        this.getData();
    }

    changeOnParentTab(event: MatTabChangeEvent) {
        this.selectedParentTabIndex = event.index;
        this.selectedChildTabIndex = 0;
        this.page = 1;
        this.pageSize = 10;
        this.searchText = '';
        this.ordering = '-id';
        this.getData();
    }

    exportToExcelAccToPage() {
        this.exportFile(this.selectedParentTabIndex, this.selectedChildTabIndex);
    }

    exportAllFiles() {
        if (this.selectedParentTabIndex == 1) {
            for (var i = 0; i < 2; i++) {
                this.exportFile(this.selectedParentTabIndex, i);
            }
        }
        else {
            for (var i = 0; i < 3; i++) {
                this.exportFile(this.selectedParentTabIndex, i);
            }
        }
    }

    allPaymentExport() {
        this.exportFile(0, 2);
    }

    exportFile(parentTabVal, childTabVal) {
        switch (parentTabVal) {
            case 0: {
                switch (childTabVal) {
                    case 0: {
                        this.exportPendingForApprovalData();
                        break;
                    }
                    case 1: {
                        this.exportSaleInProgressData();
                        break;
                    }
                    case 2: {
                        this.exportSellerClosedData();
                        break;
                    }
                    default: { break; }
                }
                break;
            }
            case 1: {
                switch (childTabVal) {
                    case 0: {
                        this.exportTradeInProgressData();
                        break;
                    }
                    case 1: {
                        this.exportBuyerClosedData();
                        break;
                    }
                    default: { break; }
                }
                break;
            }
            default: { break; }
        }
    }

    setDataForViewBid(data: any, id: number) {
        let dataObject = data.filter(e => e.id == id);
        let valuationAmount = dataObject[0].valuation_amount == null ? 0 : dataObject[0].valuation_amount;
        localStorage.setItem('valuationAmount', valuationAmount);
        let reservePrice = dataObject[0].reserve_price == null ? 0 : dataObject[0].reserve_price;
        localStorage.setItem('reservePrice', reservePrice);
        let assetId = dataObject[0].id;
        localStorage.setItem('assetId', assetId);
        let assetName = (dataObject[0]?.category?.name == 'Other' ? dataObject[0]?.other_category : dataObject[0]?.category?.display_name) + ' ' + (dataObject[0]?.brand.name == 'Other' ? dataObject[0]?.other_brand : dataObject[0]?.brand.display_name) + ' ' + (dataObject[0]?.model?.name == 'Other' ? dataObject[0]?.other_model : dataObject[0]?.model?.name);;
        localStorage.setItem('assetName', assetName);
        let yearOfMfg = dataObject[0].mfg_year == "" || dataObject[0].mfg_year == null ? 'N/A' : dataObject[0].mfg_year;
        localStorage.setItem('yearOfMfg', yearOfMfg);
        let parkingCharges = dataObject[0].parking_charge_per_day == null ? 'N/A' : dataObject[0].parking_charge_per_day;
        localStorage.setItem('parkingCharges', parkingCharges);
    }

    feedBack(type: any, objectData: any) {
        if (objectData?.feedback) {
            this.notify.success("We have already received your valuable feedback. Thank You!");
        }
        else {
            const dialogRef = this.dialog.open(BidActionComponent, {
                width: '600px',
                data: {
                    type: type,
                    amount: objectData.amount,
                    equipmentId: objectData.equipment.id,
                    buyer: this.cognitoId,
                    id: objectData.id,
                    valuationAmount: 0,
                    reservePrice: 0,
                    assetName: 0,
                    yearOfMfg: 0,
                    parkingCharges: 0,
                    statusType: 0
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                this.getData();
            })
        }
    }

    exportPendingForApprovalData() {
        let queryparams = `status__in=2&bid__bid_status__in=PA&bid__trade_type__in=BID`
        let payloadClosedBuyer = {
            is_sell: true,
            limit: 999,
            seller__cognito_id__in: this.cognitoId
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                const excelData = data?.results.map((r: any) => {
                    return {
                        "Asset ID": r?.id,
                        "Asset Name": (r?.category?.name == 'Other' ? r?.other_category : r?.category?.display_name) + ' ' + (r?.brand?.name == 'Other' ? r?.other_brand : r?.brand?.display_name) + ' ' + (r?.model?.name == 'Other' ? r?.other_model : r?.model?.name),
                        "Trade Type": r?.is_insta_sale ? 'Insta Sale' : 'Portal Sale',
                        "No. of Bids": r?.no_of_bids,
                        "Highest Bid": r?.max_bid,
                        'Asset Location': r?.pin_code?.city?.name,
                        "Asset Status": this.setassetStatus(r?.status)
                    }
                });
                ExportExcelUtil.exportArrayToExcel(excelData, "Pending for Approval Trade List");
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    exportSaleInProgressData() {
        let queryparams = `status__in=3`
        let payloadClosedBuyer = {
            is_sell: true,
            limit: 999,
            seller__cognito_id__in: this.cognitoId
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                const excelData = data?.results.map((r: any) => {
                    return {
                        "Asset ID": r?.id,
                        "Asset Name": (r?.category?.name == 'Other' ? r?.other_category : r?.category?.display_name) + ' ' + (r?.brand?.name == 'Other' ? r?.other_brand : r?.brand?.display_name) + ' ' + (r?.model?.name == 'Other' ? r?.other_model : r?.model?.name),
                        "Trade Type": r?.is_insta_sale ? 'Insta Sale' : 'Portal Sale',
                        "No. of Bids": r?.no_of_bids,
                        "Highest Bid": r?.max_bid,
                        'Asset Location': r?.pin_code?.city?.name,
                        "Asset Status": this.setassetStatus(r?.status)
                    }
                });
                ExportExcelUtil.exportArrayToExcel(excelData, "Sale In Progress Trade List");
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    exportSellerClosedData() {
        let queryparams = `bid_status__in=CL`
        let payloadClosedBuyer = {
            limit: 999,
            equipment__seller__cognito_id__in: this.cognitoId
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                const excelData = data?.results.map((r: any) => {
                    return {
                        "Ticket Id": r?.id,
                        "Asset ID": r?.equipment?.id,
                        "Asset Name": (r?.equipment?.category?.name == 'Other' ? r?.equipment?.other_category : r?.equipment?.category?.display_name) + ' ' + (r?.equipment?.brand?.name == 'Other' ? r?.equipment?.other_brand : r?.equipment?.brand?.display_name) + ' ' + (r?.equipment?.model?.name == 'Other' ? r?.equipment?.other_model : r?.equipment?.model?.name),
                        "Trade Type": r?.trade_type == "BID" ? 'Portal Sale' : 'Insta Sale',
                        "No. of Bids": r?.equipment?.no_of_bids,
                        'Asset Location': r?.equipment?.pin_code?.city?.name,
                        "Selling Price": r?.equipment?.selling_price,
                        "EMD Amount": (r?.emd_details == null) ? 0 : r?.emd_details?.amount + (r?.emd_details?.amount_type == 2) ? '%' : '',
                        "Trade Amount": r?.amount,
                        "Offer Status": r?.offer_status_display_name,
                        "Trade Status": r?.bid_status_display_name,
                        "Deal Status": r?.deal_status_display_name,
                        "Asset Status": this.setassetStatus(r?.equipment?.status),
                        "Delivery Order": r?.do_document,
                        "Sales Invoice": r?.seller_invoice
                    }
                });
                ExportExcelUtil.exportArrayToExcel(excelData, "Seller Closed Trade List");
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    exportTradeInProgressData() {
        let queryparams = `bid_status__in=SP,PA`;
        const payloadApprovedBuyer = {
            limit: 999,
            cognito_id: this.cognitoId,
            role: this.userRole
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadApprovedBuyer).subscribe(
            (data: any) => {
                const excelData = data?.results.map((r: any) => {
                    return {
                        "Ticket Id": r?.id,
                        "Asset ID": r?.equipment?.id,
                        "Asset Name": (r?.equipment?.category?.name == 'Other' ? r?.equipment?.other_category : r?.equipment?.category?.display_name) + ' ' + (r?.equipment?.brand?.name == 'Other' ? r?.equipment?.other_brand : r?.equipment?.brand?.display_name) + ' ' + (r?.equipment?.model?.name == 'Other' ? r?.equipment?.other_model : r?.equipment?.model?.name),
                        'Asset Location': r?.equipment?.pin_code?.city?.name,
                        "Trade Type": r?.trade_type == "BID" ? 'Portal Sale' : 'Insta Sale',
                        "Trade Amount": r?.amount,
                        "Offer Status": r?.offer_status_display_name,
                        "Trade Status": r?.bid_status_display_name,
                        "Deal Status": r?.deal_status_display_name,
                        "Invoice in Favour of": r?.is_self_invoice ? 'Self' : r?.invoice_first_name + r?.invoice_last_name,
                        "Delivery Order": r?.do_document,
                        // "Full Payment": r?.seller_invoice
                    }
                });
                ExportExcelUtil.exportArrayToExcel(excelData, "Trade in Progress List");
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    exportBuyerClosedData() {
        let queryparams = `bid_status__in=CL&deal_status__in=CP&offer_status=[BR,RE]`
        let payloadClosedBuyer = {
            limit: 999,
            cognito_id: this.cognitoId,
            role: this.userRole
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                const excelData = data?.results.map((r: any) => {
                    return {
                        "Ticket Id": r?.id,
                        "Asset ID": r?.equipment?.id,
                        "Asset Name": (r?.equipment?.category?.name == 'Other' ? r?.equipment?.other_category : r?.equipment?.category?.display_name) + ' ' + (r?.equipment?.brand?.name == 'Other' ? r?.equipment?.other_brand : r?.equipment?.brand?.display_name) + ' ' + (r?.equipment?.model?.name == 'Other' ? r?.equipment?.other_model : r?.equipment?.model?.name),
                        'Asset Location': r?.equipment?.pin_code?.city?.name,
                        "Trade Type": r?.trade_type == "BID" ? 'Portal Sale' : 'Insta Sale',
                        "Trade Amount": r?.amount,
                        "Offer Status": r?.offer_status_display_name,
                        "Trade Status": r?.bid_status_display_name,
                        "Deal Status": r?.deal_status_display_name
                    }
                });
                ExportExcelUtil.exportArrayToExcel(excelData, "Buyer Closed Trade List");
            },
            err => {
                console.log(err);
                this.notify.error(err?.message);
            }
        );
    }

    checkPaymentStatus(object: any) {
        let finalPayment = object?.final_payments?.length;
        let result = false;
        if (finalPayment != null && finalPayment != 0) {
            object.btnClicked = false;
            object.btnFeedback = false;
            object.showInvoice = true;
            if (object.delivery_date == null) {
                object.isDateOfDelivery = true;
            }
            result = true;
        }
        else {
            object.btnFeedback = true;
        }
        return result;
    }

    makePayment(objectData: any) {
        this.checkAuthentication().then((isAuthorise) => {
            if (isAuthorise) {
                this.validateBid(objectData);
            }
        });
    }

    checkAuthentication() {
        return new Promise((resolved, reject) => {
            if (this.cognitoId) {
                let rolesArray = this.storage.getStorageData("rolesArray", false);
                rolesArray = rolesArray.split(',');
                if (rolesArray.includes('Customer')) {
                    let payload = {
                        cognito_id: this.cognitoId,
                        role: 'Customer'
                    }
                    this.commonService.validateSeller(payload).subscribe(
                        (res: any) => {
                            if (!res.kyc_verified || !res.pan_address_proof) {
                                resolved(false);
                                this.notify.warn('Please submit KYC and Address proof', true);
                                this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                            } else {
                                resolved(true);
                            }
                        },
                        err => {
                            console.log(err);
                            reject(false);
                        }
                    );
                } else {
                    this.notify.error('Please login as Customer to proceed further with asset buy!!');
                }
            }
            else {
                const dialogConfig = new MatDialogConfig();
                dialogConfig.disableClose = false;
                dialogConfig.autoFocus = true;
                dialogConfig.panelClass = 'my-class';
                dialogConfig.width = '900px';
                dialogConfig.data = {
                    flag: true
                }
                const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
                //dialogRef.afterClosed().subscribe(result => {
                //    console.log(`Dialog result: ${result}`);
                //});


                dialogRef.afterClosed().subscribe(val => {
                    if (val) {
                        this.cognitoId = this.storage.getStorageData('cognitoId', false);
                    }
                })
            }
        });
    }

    validateBid(object: any) {
        let payload = {
            bid_id: object?.id
        }
        this.adminMasterService.getTradeBidPaymentDetails(payload).subscribe(
            (data: any) => {
                let paymentDetails = {
                    asset_id: object?.equipment.id,
                    asset_type: 'Used',
                    case: object?.trade_type == 'IS' ? 'Insta_sale' : 'Bid',
                    payment_type: 'Full'
                }
                data['bid_Id'] = object?.id;
                data['bid_amount'] = object?.amount;
                this.storage.setSessionStorageData('paymentSession', true, false);
                this.storage.setSessionStorageData('tradeBidDetails', window.btoa(JSON.stringify(data)), false);
                this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                this.router.navigate(['/customer/payment']);
            },
            err => {
                console.log(err);
                this.notify.error(err.message);
            }
        )
    }

    bidAction(type: string, element: any) {
        const dialogRef = this.dialog.open(BidActionComponent, {
            width: '500px',
            data: {
                type: type,
                amount: element.amount,
                partial_payment: element.partial_payment,
                equipmentId: element.equipment.id,
                buyer: this.cognitoId,
                id: element.id,
                valuationAmount: 0,
                reservePrice: 0,
                assetName: 0,
                yearOfMfg: 0,
                parkingCharges: 0,
                statusType: ''
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == "InvoiceUpdated") {
                element.btnClicked = true;
            }
            if (result == "Withdrawn") {
                // this.getAllTabularData();
                this.getData();
            }
            if (result == "Sales Invoice Uploaded") {
                // this.getAllTabularData();
                this.getData();
            }
            if (result && result.type == 'Modify') {
                this.modifyBid(result?.bid_amount, element)
            }
        });
    }

    bidWithdraw(element: any) {
        const putTradeBody = {
            amount: element.amount,
            equipment: element.equipment.id,
            // buyer : this.buyer,
            bid_status: 'CL',
            deal_status: 'CP',
            offer_status: 'BW'
        };
        let tradeId = element.id;
        this.adminMasterService.puttradeBid(putTradeBody, tradeId).subscribe((res: any) => {
            this.notify.success("Bid Withdrawn");
            // this.getAllTabularData();
            this.getData();
        });
    }

    modifyBid(bid_amount, bidDetails) {
        let payload = {
            bid_id: bidDetails?.id,
            amount: bid_amount,
            asset_id: bidDetails?.equipment?.id,
            cognito_id: this.cognitoId,
            source: 'Bid',
            payment_type: 1
        }
        this.apiService.validateBid(payload).pipe(finalize(() => { })).subscribe(
            (data: any) => {
                if (data?.higher_bid_available) {
                    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                        width: '500px',
                        disableClose: true,
                        data: {
                            heading: 'Higher Bid Available',
                            message: 'A Higher bid is available for the asset would you like to submit the same bid',
                        }
                    });
                    dialogRef.afterClosed().subscribe(val => {
                        if (val) {
                            if (!data?.redirect) {
                                this.updateTradeBids('Bid', false, false, bidDetails, bid_amount);
                            } else {
                                if (data.redirect) {
                                    if (data?.is_emd_available) {
                                        if (bidDetails?.partial_payment?.length) {
                                            this.updateTradeBids('Bid', true, data?.is_cooling_period, bidDetails, bid_amount);
                                        } else {
                                            let paymentDetails = {
                                                asset_id: bidDetails?.equipment?.id,
                                                asset_type: 'Used',
                                                case: 'Bid',
                                                payment_type: 'Partial',
                                                bid_id: bidDetails?.id
                                            }
                                            this.storage.setSessionStorageData('paymentSession', true, false);
                                            this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                                            this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                                            window.open("/customer/payment", '_self');
                                        }
                                    } else {
                                        this.updateTradeBids('Bid', true, data?.is_cooling_period, bidDetails, bid_amount);
                                    }
                                }
                            }
                        }
                    })
                } else {
                    if (!data?.redirect) {
                        this.updateTradeBids('Bid', false, false, bidDetails, bid_amount);
                    } else {
                        if (data.redirect) {
                            if (data?.is_emd_available) {
                                if (bidDetails?.partial_payment?.length) {
                                    this.updateTradeBids('Bid', true, data?.is_cooling_period, bidDetails, bid_amount);
                                } else {
                                    let paymentDetails = {
                                        asset_id: bidDetails?.equipment?.id,
                                        asset_type: 'Used',
                                        case: 'Bid',
                                        payment_type: 'Partial',
                                        bid_id: bidDetails?.id
                                    }
                                    this.storage.setSessionStorageData('paymentSession', true, false);
                                    this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                                    this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                                    window.open("/customer/payment", '_self');
                                }
                            } else {
                                this.updateTradeBids('Bid', true, data?.is_cooling_period, bidDetails, bid_amount);
                            }
                        }
                    }
                }
            },
            err => {
                console.log(err);
            }
        )
    }

    getOfferStatus(type, redirect, coolingPeriod) {
        if (type == 'Bid') {
            if (redirect) {
                if (coolingPeriod) {
                    return 'BR';
                } else {
                    return 'BA';
                }
            } else {
                return 'BR';
            }
        } else {
            return 'PP';
        }
    }

    getBidStatus(type, redirect, coolingPeriod) {
        if (type == 'Bid') {
            if (redirect) {
                if (coolingPeriod) {
                    return 'PA';
                } else {
                    return 'SP';
                }
            } else {
                return 'PA';
            }
        } else {
            return 'SP';
        }
    }

    getDealStatus(type, redirect, coolingPeriod) {
        if (type == 'Bid') {
            if (redirect) {
                if (coolingPeriod) {
                    return 'DP';
                } else {
                    return 'CP';
                }
            } else {
                return 'DP';
            }
        } else {
            return 'CP';
        }
    }

    updateTradeBids(type: any, redirect, coolingPeriod, bid_details, bid_amount) {
        let payload = {
            amount: bid_amount,
            equipment: bid_details?.equipment?.id,
            buyer: this.cognitoId,
            offer_status: this.getOfferStatus(type, redirect, coolingPeriod),
            trade_type: 'BID',
            bid_status: this.getBidStatus(type, redirect, coolingPeriod),
            is_cooling_period: coolingPeriod,
            deal_status: this.getDealStatus(type, redirect, coolingPeriod)
        }
        this.paymentService.updateTradeBids(payload, bid_details?.id).subscribe(
            (data: any) => {
                this.notify.success('Bid updated successfully!!', true);
                // this.getAllTabularData();
                this.getData();
            },
            err => {
                console.log(err);
            }
        )
    }

    @HostListener('window:scroll', ['$event'])
    recursiveNewsApiHit = (event) => {
        if (window.innerWidth < 768) {
            if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                this.page++;
                switch (this.selectedParentTabIndex) {
                    case 0: {
                        switch (this.selectedChildTabIndex) {
                            case 0: {
                                if ((this.total_count > this.tradeApprovalDataSource.data.length) && (this.tradeApprovalDataSource.data.length > 0)) {
                                    if (!this.tradeApprovalScrolled) {
                                        this.tradeApprovalScrolled = true;
                                        this.getApprovedTradeAssetData();
                                    }
                                }
                                break;
                            }
                            case 1: {
                                if ((this.total_count > this.tradeProgressDataSource.data.length) && (this.tradeProgressDataSource.data.length > 0)) {
                                    if (!this.tradeProgressScrolled) {
                                        this.tradeProgressScrolled = true;
                                        this.getProgessedAssetData();
                                    }
                                }
                                break;
                            }
                            case 2: {
                                if ((this.total_count > this.tradeClosedDataSource.data.length) && (this.tradeClosedDataSource.data.length > 0)) {
                                    if (!this.tradeClosedScrolled) {
                                        this.tradeClosedScrolled = true;
                                        this.getClosedTradeAssetData();
                                    }
                                }
                                break;
                            }
                            default: {
                                if ((this.total_count > this.tradeApprovalDataSource.data.length) && (this.tradeApprovalDataSource.data.length > 0)) {
                                    if (!this.tradeApprovalScrolled) {
                                        this.tradeApprovalScrolled = true;
                                        this.getApprovedTradeAssetData();
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                    case 1: {
                        switch (this.selectedChildTabIndex) {
                            case 0: {
                                if ((this.total_count > this.tradeApprovedBuyerDataSource.data.length) && (this.tradeApprovedBuyerDataSource.data.length > 0)) {
                                    if (!this.tradeApprovedBuyerScrolled) {
                                        this.tradeApprovedBuyerScrolled = true;
                                        this.getApprovedBidForBuyer();
                                    }
                                }
                                break;
                            }
                            case 1: {
                                if ((this.total_count > this.tradeClosedBuyerDataSource.data.length) && (this.tradeClosedBuyerDataSource.data.length > 0)) {
                                    if (!this.tradeClosedBuyerScrolled) {
                                        this.tradeClosedBuyerScrolled = true;
                                        this.getClosedBidForBuyer();
                                    }
                                }
                                break;
                            }
                            default: {
                                if ((this.total_count > this.tradeApprovedBuyerDataSource.data.length) && (this.tradeApprovedBuyerDataSource.data.length > 0)) {
                                    if (!this.tradeApprovedBuyerScrolled) {
                                        this.tradeApprovedBuyerScrolled = true;
                                        this.getApprovedBidForBuyer();
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                    default: {
                        this.selectedParentTabIndex = 0;
                        this.selectedChildTabIndex = 0;
                        if (!this.tradeApprovalScrolled) {
                            this.tradeApprovalScrolled = true;
                            this.getData();
                        }
                        break;
                    }
                }
            }

        }
    }


}
