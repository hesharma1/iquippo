import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductlistService } from 'src/app/services/product-list.service';
import * as _ from 'underscore';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { SharedService } from 'src/app/services/shared-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.component.html',
  styleUrls: ['./product-listing.component.css'],
})
export class ProductListingComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  page: number = 1;
  listingData = [];
  pageSize: number = 10;
  ordering: string = '-id';
  total_count: any;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  productDisplayedColumns = [
    'id',
    'image',
    'category__display_name',
    'brand__display_name',
    'model__name',
    'service_type',
    'selling_price',
    'mfg_year',
    'location',
    'status',
    'Actions',
  ];
  cognitoId;
  pageEvent!: PageEvent;
  currentRole: any;
  searchText!: string;
  filter: any;
  noRecordFound: string = "";
  public statusOptions: Array<any> = [
    { name: 'Draft', value: 0 },
    { name: 'Pending', value: 1 },
    { name: 'Approved', value: 2 },
    { name: 'Sale in progress', value: 3 },
    { name: 'Rejected', value: 4 },
    { name: 'Sold', value: 5 }
  ];

  constructor(
    private router: Router,
    private ProductlistService: ProductlistService,
    private storage: StorageDataService,
    private apiRouteService: ApiRouteService,
    public sharedService: SharedService,
    public spinner: NgxSpinnerService,
    public notify: NotificationService
  ) { }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.currentRole = this.storage.getStorageData('userRole', false);
    this.getProductList();
  }
  scrolled= false;
  // Changes for Card layout on Mobile devices
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if(window.innerWidth < 768){
      if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){
        const triggerAt: number = 900; 
        if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getProductList();
          }
        }
      }
    }
  }
  
  /**get row status chnage */
  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    }else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    }else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  actionPopup(id) {
    let state = 'preview';
    localStorage.setItem('preview', state);
    localStorage.setItem('isCustomer', 'true');
    this.router.navigate(['used-equipment-dashboard/equipment-details', id]);
  }
  
  getProductList() {
    //this.spinner.show();
    let queryParam = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.cognitoId,
      role: this.currentRole
    };
    if (this.searchText) {
      queryParam['search'] = this.searchText;
    }
    if (this.filter) {
      queryParam['status__in'] = this.filter;
    }
    this.ProductlistService.getUsedProWithList(queryParam).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if(window.innerWidth > 768){
            this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.page == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
         
          if (this.dataSource.data && this.dataSource.data.length <= 0) {
            this.noRecordFound = 'No records found';
          }
          else {
            this.noRecordFound = '';
          }
        }
      },
      err => {
        this.notify.error(err?.message)
      }
    );
  }

  statusFilter(val) {
    this.filter = val;
    this.page = 1;
    this.getProductList();
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getProductList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getProductList();
  }

  /**search list  */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.getProductList();
    }
  }

  createUpdateCategory(element) {
    this.router.navigate(['/used-equipment/basic-details', element.id]);
  }

  /**Export Function */
  export() {
    let payload = {
      is_sell: true,
      limit: 999,
      cognito_id: this.cognitoId
    }
    this.ProductlistService.getUsedProWithList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Asset ID": r?.id,
            "Asset Name": r?.product_name,
            "Price": r?.selling_price,
            "MFG Year": r?.mfg_year,
            "Location": r?.location?.city?.name,
            "Status": this.getRowstatus(r?.status)
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Used Product List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }
  deleteAsset(id){
    this.ProductlistService.deleteAsset(id).subscribe(res=>{
      this.notify.success('Asset Deleted Successfully!!');
      this.getProductList();
    });
  }

}
