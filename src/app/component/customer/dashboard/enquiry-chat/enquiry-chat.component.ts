import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { CommonService } from 'src/app/services/common.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { SigninComponent } from 'src/app/component/signin/signin.component';

@Component({
  selector: 'app-enquiry-chat',
  templateUrl: './enquiry-chat.component.html',
  styleUrls: ['./enquiry-chat.component.css']
})
export class EnquiryChatComponent implements OnInit {
  public dealForm!: FormGroup;
  public cognitoId!: any;
  public enquiryId: any;
  public userRole!: string;
  public enquiryDetails: any;
  public chatDetails!: Array<any>;
  public chatText: string = '';
  public enquiryCurrentStatus: any;
  public newEnquiryStatus: any;
  public isRole: boolean = false;
  public selectedStatus: any;
  public userRoleSpecificOptions = [1, 2, 3, 6];
  public selectedStatusOptions: Array<any> = [];
  public enquiryStatusOptions: Array<any> = [
    { name: 'Open', value: 1 },
    { name: 'In Progress', value: 2 },
    { name: 'Deal Submited', value: 3 },
    { name: 'Deal in Progress', value: 4 },
    { name: 'Purchase Completed', value: 5 },
    { name: 'Rejected', value: 6 },
    { name: 'Rejected by Buyer', value: 7 }
  ];

  constructor(private fb: FormBuilder, public route: ActivatedRoute, public router: Router, public notify: NotificationService, public spinner: NgxSpinnerService, public storage: StorageDataService, private newEquipmentService: NewEquipmentService, public usedEquipmentService: UsedEquipmentService, private commonService: CommonService, private appRouteEnum: AppRouteEnum, private dialog: MatDialog) {
    this.dealForm = this.fb.group({
      final_offer_price: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      booking_value: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
    })
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    if (this.userRole && (this.userRole != 'Customer')) {
      this.isRole = true;
    }

    this.route.queryParams.subscribe((data) => {
      this.enquiryId = data.id;
      if (this.enquiryId) {
        this.getEnquiryDetails();
        this.getChatDetails();
      } else {
        this.router.navigate(['/customer/dashboard/my-enquiries']);
      }
    })
  }

  getEnquiryDetails() {
    this.newEquipmentService.getEnquiryDetails(this.enquiryId).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.enquiryDetails = data;
        this.enquiryCurrentStatus = JSON.stringify(data.status);
        if (this.isRole) {
          this.selectedStatusOptions = this.enquiryStatusOptions.filter((opt) => { return (this.userRoleSpecificOptions.includes(opt.value) && (opt.value > this.enquiryCurrentStatus)) });
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getChatDetails() {
    let chatPayload = {
      limit: 999,
      enquiry_id__in: this.enquiryId
    }
    this.newEquipmentService.getEnquiryChatDetails(chatPayload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.chatDetails = data.results;
        this.updateChatSeen();
      },
      err => {
        console.log(err);
      }
    )
  }

  getEnquiryStatus(val) {
    let Obj = this.enquiryStatusOptions.find((enq) => { return enq.value == val });
    return Obj?.name;
  }

  postChat() {
    if (this.chatText) {
      let payload = {
        agent_type: this.userRole == 'Customer' ? 'BR' : 'SR',
        agent_comment: this.chatText,
        enquiry_id: this.enquiryId,
        agent_user: this.cognitoId
      }
      this.newEquipmentService.postChatDetail(payload).pipe(finalize(() => { })).subscribe(
        (data: any) => {
          this.getChatDetails();
          this.chatText = '';
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.notify.error('Please enter something to post!!');
    }
  }

  getEquipmentRequiredTime(val) {
    switch (val) {
      case 1: {
        return 'Immediate'
        break;
      }
      case 2: {
        return 'Within next 60 Days'
        break;
      }
      case 3: {
        return 'Not Sure'
        break;
      }
      default: {
        return 'Not Sure'
        break;
      }
    }
  }

  updateEnquiryStatus() {
    let payload: any;
    if (this.newEnquiryStatus && (this.newEnquiryStatus != this.enquiryCurrentStatus)) {
      if (this.newEnquiryStatus != 3) {
        payload = {
          status: +this.newEnquiryStatus
        }
      } else {
        if (this.dealForm.valid) {
          if (+this.dealForm.value.booking_value > +this.dealForm.value.final_offer_price) {
            this.notify.error('Booking value (Down Payment) should less than Final Offer Price');
            return;
          } else {
            payload = {
              status: +this.newEnquiryStatus,
              final_offer_price: +this.dealForm.value.final_offer_price,
              booking_value: +this.dealForm.value.booking_value
            }
          }
        } else {
          this.notify.error('Please provide valid value for Submit Deal!!');
        }
      }
      if (payload) {
        this.newEquipmentService.updateEnquiryDetails(payload, this.enquiryId).pipe(finalize(() => { })).subscribe(
          (data: any) => {
            this.enquiryDetails = data;
            this.enquiryCurrentStatus = JSON.stringify(data.status);
            this.notify.success('Enquiry Status updated!!');
            if (this.isRole) {
              this.selectedStatusOptions = this.enquiryStatusOptions.filter((opt) => { return (this.userRoleSpecificOptions.includes(opt.value) && (opt.value > this.enquiryCurrentStatus)) });
            }
          },
          err => {
            console.log(err);
          }
        )
      }
    } else {
      this.notify.error('Please select valid status for update!!');
    }
  }

  updateChatSeen() {
    if (this.chatDetails?.length) {
      let lastChatDetails: any;
      let tempArr: Array<any> = [];
      if (this.userRole != 'Customer') {
        tempArr = this.chatDetails.filter(f => { return f.agent_type == "BR" });
        lastChatDetails = tempArr[tempArr.length - 1];
      } else {
        tempArr = this.chatDetails.filter(f => { return f.agent_type == "SR" });
        lastChatDetails = tempArr[tempArr.length - 1];
      }
      if ((lastChatDetails != null) && (!lastChatDetails?.comment_seen)) {
        let payload = {
          enquiry_id: +this.enquiryId,
          cognito_id: lastChatDetails.agent_user,
          last_seen_chat_id: lastChatDetails.id,
        }
        this.newEquipmentService.updateChatSeen(payload).pipe(finalize(() => { })).subscribe(
          data => {
            console.log(data);
          },
          err => {
            console.log(err);
          }
        )
      }
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        if (this.userRole == 'Customer') {
          let payload = {
            cognito_id: this.cognitoId,
            role: this.userRole
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                // this.notify.warn('Please submit KYC and Address proof', true);
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.width = '900px';
        dialogConfig.data = {
          flag: true
        }
        const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
        //dialogRef.afterClosed().subscribe(result => {
        //    console.log(`Dialog result: ${result}`);
        //});


        dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = this.storage.getStorageData('cognitoId', false);
          }
        })
      }
    });
  }

  payBookValue() {
    this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        let payload = {
          amount: this.enquiryDetails?.booking_value,
          asset_id: this.enquiryDetails?.new_equipment?.id,
          cognito_id: this.cognitoId,
          source: 'Lead',
          payment_type: 1,
          lead_id: this.enquiryId
        }
        this.usedEquipmentService.validateBid(payload).pipe(finalize(() => { })).subscribe(
          (data: any) => {
            if (data.redirect) {
              let bookingDetails = {
                final_offer_price: this.enquiryDetails?.final_offer_price,
                bookValue: data?.bid_amount,
                enquiry_id: this.enquiryId,
                description: data?.description
              }
              let paymentDetails = {
                asset_id: this.enquiryDetails?.new_equipment?.id,
                asset_type: 'New',
                case: 'Lead',
                payment_type: 'Partial'
              }
              this.storage.setSessionStorageData('paymentSession', true, false);
              this.storage.setSessionStorageData('bookingDetails', window.btoa(JSON.stringify(bookingDetails)), false);
              this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
              // this.router.navigate(['/customer/payment']);
              window.open("/customer/payment", '_self');
            } else {
              this.notify.error(data.message);
            }
          },
          err => {
            console.log(err);
          }
        )
      }
    });
  }
}
