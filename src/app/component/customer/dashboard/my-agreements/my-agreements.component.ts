import { DatePipe } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { environment } from 'src/environments/environment';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from 'src/app/services/shared-service.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NgxSpinnerService } from 'ngx-spinner';
import { AgreementDetailsPopupComponent } from 'src/app/component/partner-dealer-management/agreement-details/agreement-details-popup/agreement-details-popup.component';
import { ConfirmationDeletePopupComponent } from 'src/app/component/admin/client-agreement/confirmation-delete-popup/confirmation-delete-popup.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatSort } from '@angular/material/sort';
import { CommonService } from 'src/app/services/common.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuctionService } from 'src/app/services/auction.service';
import { ESignConfirmDetailsComponent } from 'src/app/component/admin/client-agreement/client-agreement.component';

@Component({
  selector: 'app-my-agreements',
  templateUrl: './my-agreements.component.html',
  styleUrls: ['./my-agreements.component.css']
})
export class MyAgreementsComponent implements OnInit {
  form?: FormGroup;
  multipleAgreementRef?: FormArray;
  dataSource1: MatTableDataSource<any[]> = new MatTableDataSource<any[]>([]);
  res: any;
  showForm: boolean = false;
  isDataDisplay: boolean = false;
  listingData:any = [];
  displayedColumns = ["id", "start_date", "end_date", "seller_mobile_number", "seller_email", "agreement_letter", "status", "Action"];
  displayedEsignColumns = ["id", "start_date", "agreement_letter", "status", "Action"];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild('agreement') agreement!: ElementRef;
  @ViewChild('work_order') work_order!: ElementRef;
  @ViewChild('input') input!: ElementRef;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  action: string = "";
  role;
  cognitoId;
  agreementStatus: any;
  agreementTemplateUrl: string = 'https://iquippodocuments.s3.ap-south-1.amazonaws.com/agreement.pdf';
  sellerData: any;
  sellerList: any;
  sellerId?: number;
  isForEsign: boolean = false;
  agreementType: Array<any> = [
    { typeName: 'Bidder undertaking', value: 1 },
    { typeName: 'Seller Agreement', value: 2 },
  ];
  agreementTypeValue: any = 2;
  //eSignReqData = new MatTableDataSource<[]>();
  eSignReqData: any[] = [];
  isESignData: boolean = false;
  auctionId: any;
  isSellerAgreementCreated: boolean = true;
  isAdmin: boolean = true;
  returnUrl = '';
  constructor(private fb: FormBuilder, private router: Router, private commonService: CommonService,
    private appRouteEnum: AppRouteEnum, private datePipe: DatePipe, private s3: S3UploadDownloadService,
    private notify: NotificationService, public agreementService: AgreementServiceService,
    public sharedService: SharedService, private dialog: MatDialog, public app: ApiRouteService,
    public spinner: NgxSpinnerService, private storage: StorageDataService, public auctionService: AuctionService,
    private activatedRoute: ActivatedRoute) {
    this.form = this.fb.group({
      start_date: new FormControl(this.datePipe.transform(new Date(), 'yyyy-MM-dd'), [Validators.required]),
      end_date: new FormControl(this.datePipe.transform(new Date().setFullYear(new Date().getFullYear() + 1), 'yyyy-MM-dd'), [Validators.required]),
      agreement_letter: new FormControl('', [Validators.required]),
      commission_value: new FormControl(5000),
      enableCost: new FormControl(false),
      is_fixed_commission: new FormControl(false),
      status: new FormControl(1),
      partner_entity: new FormControl(1),
      multipleAgreement: new FormArray([
        new FormGroup({
          work_order: new FormControl('', []),
        })
      ])
    })
    this.agreementStatus = app.agreementStatus;
  }

  ngOnInit(): void {
    this.agreementTypeValue = "2";
    this.multipleAgreementRef = <FormArray>this.form?.controls['multipleAgreement'];
    this.role = localStorage.getItem("userRole");
    this.cognitoId = localStorage.getItem('cognitoId');
    // this.getAgreementTemplateUrl();
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl ? params.returnUrl : '';
      if (params['isForEsign'] == 'true') {
        this.isForEsign = true;
        this.agreementTypeValue = "1";
        this.getESignTemplate();
      }
      else {
        this.isForEsign = false;
        this.agreementTypeValue = "2";
        this.getESignTemplate();
      }
      if (params['auction']) {
        this.auctionId = params['auction'];
      }

    });
  }


  ngAfterViewInit() {
    if (this.agreementTypeValue == 2) {
      this.getOnloadAgreementDetails(this.cognitoId);
    }
  }



  addAgreement() {
    this.multipleAgreementRef?.push(
      new FormGroup({
        work_order: new FormControl('', [Validators.required]),
      })
    );
  }
  deleteImg(url: string, index?: number) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        if (index != undefined) {
          this.form?.get('multipleAgreement')?.get('' + index + '')?.get("work_order")?.setValue('');
          this.work_order.nativeElement.value = "";
        }
        else
          this.form?.get('agreement_letter')?.setValue('');
        this.agreement.nativeElement.value = "";

      },
      err => {
        this.notify.error(err);
      }
    )
  }
  removeAgreement(i: number) {
    this.multipleAgreementRef?.removeAt(i);
  }

  createUUID() {
    return uuidv4();
  }

  async handleUpload(event: any, name: string) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + name + '/' + uniqueFileName + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');
      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});
    }
  }

  async handleWorkUpload(event: any, name: string, i: number) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + name + '/' + uniqueFileName + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get('multipleAgreement')?.get('' + i + '')?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //console.log('type', file.type);
        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');
      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});

    }
  }




  onSubmit() {
    let payload = {
      cognito_id: this.cognitoId,
      role: 'Customer'
    }
    this.commonService.validateSeller(payload).subscribe((res: any) => {
      if (res.kyc_verified != 2 || !res.pan_address_proof) {
        if (!res.pan_address_proof || res.kyc_verified == 1) {
          this.notify.warn('Please submit KYC and Address proof', true);
        } else if (res.kyc_verified == 3) {
          this.notify.warn('As per our records, your KYC profile is rejected. If you have any questions, kindly raise a call back request.', true);
        }
        this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, 'isrequired': true } });
        return;
      }
      if (!res.bank_details_exist) {

        this.notify.warn(res.message, true);
        this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { 'isrequired': true } });
        return;
      }

      //this.spinner.show();
      let payload = { ...this.form?.value };
      payload.work_order = [];
      if (payload.enableCost == true) {
        payload.is_fixed_commission = true;
        payload.commission_value = 5000;
      }
      else {
        payload.is_fixed_commission = false;
        payload.commission_value = 5;
      }
      payload.start_date = this.datePipe.transform(payload.start_date, 'yyyy-MM-dd');
      payload.end_date = this.datePipe.transform(payload.end_date, 'yyyy-MM-dd');
      // payload.multipleAgreement?.forEach(element => {
      //   payload.work_order.push(element.work_order);
      // });
      payload.type = 1;
      payload.user = this.cognitoId
      this.agreementService.postAgreementDetails(payload).subscribe(res => {
        if (this.agreement != undefined)
          this.agreement.nativeElement.value = '';
        if (this.work_order != undefined)
          this.work_order.nativeElement.value = '';
        this.resetForm();
        this.isSellerAgreementCreated = false;
        this.getAgreementDetails(this.cognitoId);
      }, err => {
      })
    })
  }

  resetForm() {
    this.form?.reset();
    this.form?.get('enableCost')?.setValue(false);
    this.form?.get('status')?.setValue(1);
    this.form?.get('commission_value')?.setValue(5);
  }

  actionPopup(agreementObj: any, action: string) {

    if (action == "view") {
      const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
        width: "60%",
        data: { agreementObj: agreementObj }
      }).afterClosed()
        .subscribe(data => {
        })
    }
    else if (action == "edit") {

      this.showForm = false;
      this.form?.get('multipleAgreement')?.get('0')?.patchValue(agreementObj);
    }
    else {
      const dialogRef = this.dialog.open(ConfirmationDeletePopupComponent, {
        width: "50%",
        data: {}
      }).afterClosed()
        .subscribe(data => {
          if (data) {
            this.agreementService.deleteAgreementDetails(agreementObj).subscribe(res => {
              //   this.agreementService.deletePartnerEntityMapping(agreementObj).subscribe(res=>{
              this.getAgreementDetails(this.cognitoId);
              // })
            })
          }
        })

    }

  }

  getAgreement() {
    this.getAgreementDetails(this.cognitoId);
  }
  scrolled= false;
 // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.res.count > this.listingData.length) && (this.listingData.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.paginator.pageIndex++;
         this.getAgreementDetails(this.cognitoId);
        }
       }
     }
   }
 }
  getAgreementDetails(cognitoId: string) {
    // let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    let queryparams = cognitoId + "?limit=" + this.paginator.pageSize + "&page=" +
      (this.paginator.pageIndex + 1) + "&search=" + this.input.nativeElement.value +
      "&role=" + (this.role) + "&ordering=" + (this.sort.direction == "asc" ?
        this.sort.active : "-" + this.sort.active);
    this.agreementService.getAgreementDetailsById(queryparams).subscribe(res => {
      
// Changes for Card layout on Mobile devices
if(window.innerWidth > 768){
  this.res = res as any;
  this.dataSource1 = this.res?.results;
  }else{
    this.scrolled = false;
    if(this.paginator.pageIndex == 1){
      // this.dataSource1 = [];
      this.listingData = [];
      this.listingData = this.res?.results;
      this.dataSource1 = this.listingData;
    }else{
      this.listingData = this.listingData.concat(this.res?.results);
      this.dataSource1 = this.listingData;
    }
  }
    }, err => {
      console.log(err);
    })
  }

  getOnloadAgreementDetails(cognitoId: string) {
    // let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    let queryparams = cognitoId + "?limit=" + this.paginator.pageSize + "&page=" +
      (this.paginator.pageIndex + 1) + "&search=" + this.input.nativeElement.value +
      "&role=" + (this.role) + "&ordering=" + (this.sort.direction == "asc" ?
        this.sort.active : "-" + this.sort.active);
    this.agreementService.getAgreementDetailsById(queryparams).subscribe(res => {
      this.res = res as any;
      this.dataSource1 = this.res?.results;
      //this.res.results = [];
      if (this.res?.results && this.res?.results.length > 0) {
        this.isSellerAgreementCreated = false;
      }
      else {
        this.isSellerAgreementCreated = true;
      }
    }, err => {
      console.log(err);
    })
  }

  approve(item) {
    item.status = 2;
    this.agreementService.putAgreementDetails(item).subscribe(res => {
      this.getAgreementDetails(this.cognitoId)
    })
  }

  reject(item) {
    item.status = 4;
    this.agreementService.putAgreementDetails(item).subscribe(res => {
      this.getAgreementDetails(this.cognitoId)
    })
  }

  // getAgreementTemplateUrl() {
  //   this.agreementService.getAgreemenTemplateUrl().subscribe((res: any) => {
  //     this.agreementTemplateUrl = res?.value;

  //   }, err => {
  //     console.log(err);
  //   })
  // }

  enableCost() {
    if (this.form?.get('enableCost')?.value) {
      this.form?.get('commission_value')?.setValue(5000)
    }
    else {
      this.form?.get('commission_value')?.setValue(5)
    }
  }

  agreementTypeChange(value) {
    if (value == 1) {
      this.isSellerAgreementCreated = true;
      this.getESignTemplate();
    }
    else if (value == 2) {
      this.isSellerAgreementCreated = false;
      this.getAgreementDetails(this.cognitoId);
    }
  }

  viewBidderData(agreementObj) {
    const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
      width: "60%",
      data: {
        agreementObj: agreementObj,
        isFromBidder: true
      }
    }).afterClosed()
      .subscribe(data => {
      })
  }

  downloadESignTemplate() {
    const params = {
      role_type: 1,
      bidder: this.cognitoId,
      //bidder: "00567af9-0df6-4571-b454-e34bc9a71271",
      //auction: this.auctionId ? parseInt(this.auctionId) : 35
    }
    this.auctionService.postAuctionEsign(params).subscribe(
      (res: any) => {
        if (res.message) {
          //  this.notify.success(res.message);
          this.getESignTemplate();
        }
        else {
          this.eSignReqData = [];
          this.eSignReqData.push(res)
        }
      }, err => {
        console.error(err);
      });
  }

  getESignTemplate() {
    const params = {
      bidder__cognito_id__in: this.cognitoId
      //bidder__cognito_id__in: "00567af9-0df6-4571-b454-e34bc9a71271",
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        if (res && res.results) {
          this.eSignReqData = res.results as any[];
        }
      });
  }

  getESignWidget(refId) {
    const params = {
      bidder__cognito_id__in: this.cognitoId
      //bidder__cognito_id__in: "00567af9-0df6-4571-b454-e34bc9a71271",
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        this.eSignReqData = res.results as any[];
        this.downloadESignTemplate();
        setTimeout(() => {
          if (res.results && res.results[0] && res.results[0].status && res.results[0].status == 'EP') {
            this.patchESignReq(refId, this.eSignReqData[0].esign_ref_id);
          }
          else if (res.results && res.results[0] && res.results[0].status && (res.results[0].status == 'INI' ||
            res.results[0].status == 'UNS' || res.results[0].status == 'TC')) {
            this.eSignDetailsConfirmation(refId, this.eSignReqData[0].esign_ref_id);
            // this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
          }
        }, 2000);
      });
  }

  eSignDetailsConfirmation(url, esign_ref_id?) {
    const dialogRef = this.dialog.open(ESignConfirmDetailsComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(val => {
      if (val == true) {
        //this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
        console.log("return url", this.returnUrl);
        if (this.returnUrl != '') {
          localStorage.setItem("returnUrl", this.returnUrl);
        }
        window.open("/customer/dashboard/e-sign-template/" + esign_ref_id,
          '_blank');
      }
    })
  }

  onProceed(refId) {

    this.getESignWidget(refId);
  }

  patchESignReq(url, esign_ref_id?) {
    const params = {
      reference_id: url,
      page_url: environment.mp1HomePage + 'customer/dashboard/e-sign-template/'
    }
    this.auctionService.patchAuctionEsign(params, url).subscribe(
      (res: any) => {
        console.log("patchres", res);
        if (res.message) {
          this.notify.error(res.message);
        }
        else {
          this.eSignDetailsConfirmation(url, res.esign_ref_id);
          //this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
        }
      });

  }

  deleteEsignRecord(id) {
    this.auctionService.deleteESignData(id).subscribe(
      res => {
        if (res == null) {
          this.notify.success('Data Deleted Successfully.');
          this.getESignTemplate();
        }
      });
  }
  proceed() {
    this.router.navigate([this.returnUrl]);
  }

}


