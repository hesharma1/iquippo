import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { CookieService } from 'ngx-cookie-service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { finalize } from 'rxjs/operators';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

@Component({
  selector: 'app-asset-master',
  templateUrl: './asset-master.component.html',
  styleUrls: ['./asset-master.component.css']
})
export class AssetMasterComponent implements OnInit {

  //AssetMstrFrmGrp: FormGroup;
  public JData: any;
  public UsersJData: any;
  activePage: number = 1;
  pager: any = {};
  total_count: any;
  ChkBox: any;
  IsChkd: number = 0;
  public filterStatus: any;
  public filter: any;
  public searchText!: string;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public cognitoId:any;
  public statusOptions: Array<any> = [
    { name: 'Draft', value: 0 },
    { name: 'Pending', value: 1 },
    { name: 'Approved', value: 2 },
    { name: 'Sale in progress', value: 3 },
    { name: 'Rejected', value: 4 },
    { name: 'Sold', value: 5 }
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  listingData = [];
  constructor(private cookieService: CookieService, private ProductlistService: ProductlistService, private http: HttpClient, private productUpload: NewEquipmentService,
    private route: ActivatedRoute, private formbuilder: FormBuilder, public notify: NotificationService,private storage: StorageDataService, private router: Router,) {
    /*this.AssetMstrFrmGrp = this.formbuilder.group({
      TblAssetMstr: [''],
      TBodyAssetMstr: ['']
    });*/
  }

  ngOnInit(): void {
    //cookie session save by using expiry datetime - start

    //var CookieValue = this.cookieService.get("UID");
    //alert(CookieValue);

    //cookie session save by using expiry datetime - end
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getPage();
  }
  scrolled = false;
 // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.total_count > this.JData.length) && (this.JData.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
         this.getPage();
        }
       }
     }
   }
 }
  Chkd: boolean = false;
  ChkAllChkBoxTbl() {

    var TblLnth: number = Number(document.getElementById("TblAssetMstr") ?.getElementsByTagName("tr").length) - 1;

    if (this.IsChkd == 0) {
      this.IsChkd = 1;
      for (var i = 0; i < TblLnth; i++) {
        //document.getElementById("TBodyAssetMstr")?.children[1].children[0].children["Chk"].checked = true;

        const HChkBox = document.getElementById("TBodyAssetMstr");
        if (HChkBox) {
          HChkBox.children[i].children[0].children["Chk"].checked = true;
        }

        //var HChkBox = document.getElementById("TBodyAssetMstr"); //.children[1].children[0].children["Chk"];
        //var HChkBox_Chk = HChkBox?.children[i].children[0].children["Chk"].checked; // this.func1() : this.func2();
        //HChkBox_Chk = true;
      }
    }
    else if (this.IsChkd == 1) {
      this.IsChkd = 0;

      for (var i = 0; i < TblLnth; i++) {
        const HChkBox = document.getElementById("TBodyAssetMstr");
        if (HChkBox) {
          HChkBox.children[i].children[0].children["Chk"].checked = false;
        }
        //HChkBox.children[1].children[0].children["Chk"].checked = true;
      }
    }
  }

  BChk_Change(Chk: any) {
    if (Chk == true) {
      this.IsChkd = 0;
      //document.getElementById("TblHead").children[0].children[0].children["ChkBoxHeader"].checked = false;
      const TblHd = document.getElementById("TblHead");
      if (TblHd) {
        TblHd.children[0].children[0].children["ChkBoxHeader"].checked = false;
      }
      // this.func1() : this.func2();

    }
  }

  /*func1()
  {
    alert("Hi");
  }
  
  func2()
  {
    alert("Hi");
  }*/

  /*
  
  BtnSubmit_Click() {
      // document.getElementById("TblBody").children[1].children[0].children["Chk"].checked
  
      var aa = "";
  
      for (
        var i = 0;
        i < document.getElementById("Tbl").getElementsByTagName("tr").length - 1;
        i++
      ) {
        if (
          document.getElementById("TblBody").children[i].children[0].children[
            "Chk"
          ].checked == true
        ) {
          aa +=
            document.getElementById("TblBody").children[i].children[0].children[
              "Chk"
            ].value + ",";
        }
      }
      alert(aa);
    }
  
  */

  isAllChecked() {
    return this.JData.every(x => x.id);
  }

  setPage(page: number) {

    this.activePage = page;
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.getPage();
  }
  /**search from list */
  searchInList() {
    if (this.searchText === '' || this.searchText.length > 3) {
      this.getPage();
    }
  }

  getPage() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
      seller__cognito_id__in:this.cognitoId

    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filterStatus) {
      payload['status__in'] = this.filterStatus;
    }
    this.ProductlistService.getNewProductWithList(payload).subscribe((res: any) => {
      //if(res)
      //{
      this.total_count = res.count;

      //this.dataSource.data = res.results;
      //this.dataSource.sort = this.tableOneSort;

      
// Changes for Card layout on Mobile devices
if(window.innerWidth > 768){
  this.JData = eval(res.results);
  }else{
    this.scrolled = false;
    if(this.activePage == 1){
      this.JData = [];
      this.listingData = [];
      this.listingData = res.results;
      this.JData = this.listingData;
    }else{
      this.listingData = this.listingData.concat(res.results);
      this.JData = this.listingData;
    }
  } 
      this.pager = this.getPager(this.total_count, this.activePage);

      //this.norecordshow();
      //}

    })
  }

  /**on chnage pages pagination view */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPage();
  }

  statusFilter(val) {
    this.filterStatus = val;
    this.activePage = 1;
    this.getPage();
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    let totalPages = Math.ceil(totalItems / pageSize);
    let startPage: number; let endPage: number;
    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;

    }
    else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      }
      else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      }
      else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
    let pages = _.range(startPage, endPage + 1);

    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    }
  }

  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    }else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    }else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  actionPopup(id) {
    let state = 'preview';
    localStorage.setItem('preview', state);
    localStorage.setItem('isCustomer', 'true');
    this.router.navigate(['new-equipment-dashboard/equipment-details', id]);
  }
  export() {
    let payload = {
      limit: 999,
      seller__cognito_id__in:this.cognitoId
    }
    this.ProductlistService.getNewProductWithList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Asset ID": r?.id,
            "Category": r?.category?.display_name,
            "Brand": r?.brand?.display_name,
            "Model": r?.model?.name,
            "Price": r?.selling_price,
            "MFG Year": r?.mfg_year,
            "Status": this.getRowstatus(r?.status)
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "New Asset List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  /*
  
 setCookie(cname,cvalue,exdays) 
 {
   var d = new Date();
   d.setTime(d.getTime() + 600000);
   //(exdays*24*60*60*1000));
   var expires = "expires=" + d.toUTCString();
   //document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
   document.cookie = cname +"=" + cvalue + ";" + expires + ";domain=" + window.location.protocol + "//" + window.location.hostname + "/" + window.location.port + ";path=/";
 }
 
 getCookie(cname) 
 {
   var name = cname + "=";
   var decodedCookie = decodeURIComponent(document.cookie);
   var ca = decodedCookie.split(';');
   for(var i = 0; i < ca.length; i++) {
     var c = ca[i];
     while (c.charAt(0) == ' ') {
       c = c.substring(1);
     }
     if (c.indexOf(name) == 0) {
       return c.substring(name.length, c.length);
     }
   }
   return "";
 }
 
 checkCookie(cname) 
 {  
   //this.delete_cookie(cname);
   // window.location.href = window.location.protocol + "//" + window.location.hostname
   
   var user=this.getCookie(cname);
   if (user != "") // Get
   {
     alert("Welcome again " + user);
   } 
   else // Set
   {
     alert("No User Found");
     //window.location.href = window.location.protocol + "//" + window.location.hostname;
   }  
 }
 */
  deleteAsset(id) {
    this.ProductlistService.deleteNewAsset(id).subscribe(res => {
      this.notify.success('Asset Deleted Successfully!!');
      this.getPage();
    });
  }

}
