import { TestBed } from '@angular/core/testing';

import { AssetMasterPropService } from './asset-master-prop.service';

describe('AssetMasterPropService', () => {
  let service: AssetMasterPropService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssetMasterPropService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
