import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AssetMasterPropService implements IAssetMstrProp
{

  constructor() { }

  count: number = 0;
  next: string = '';
  previous: string = '';
  results: Result[] = []; 

}

export interface IAssetMstrProp 
{
  count:    number;
  next:     string;
  previous: string;
  results:  Result[];
}

export interface Result 
{
  id:                      number;
  tech_fields:             any[];
  primary_image:           string;
  seller_type:             number;
  trade_type:              number;
  variant:                 string;
  product_name:            string;
  fuel_type:               number;
  mfg_year:                string;
  selling_price:           string;
  indicative_price:        string;
  product_condition:       string;
  rate_equipment:          number;
  certificate:             number;
  document:                string;
  video_link:              string;
  youtube_link:            string;
  is_special_offer_banner: boolean;
  offer_banner_image:      string;
  is_special_offer_text:   boolean;
  offer_heading:           string;
  offer_list:              string;
  upload_stamp:            string;
  upload_t_and_c:          string;
  is_active:               boolean;
  is_deleted:              boolean;
  is_featured:             boolean;
  info_level:              number;
  status:                  number;
  seller:                  string;
  category:                Brand;
  brand:                   Brand;
  model:                   Model;
  group:                   string;
  country:                 string;
  state:                   string;
  location:                Location;
}

export interface Brand 
{
  id:           number;
  name:         Name;
  display_name: Name;
  status:       Status;
  status_id:    number;
  updated_at:   Date;
  created_at:   Date;
}

export interface Name 
{
  Excavator: string;
  Jcb: string;
}

export interface Status 
{
  Active: string;
}

export interface Location 
{
  city:     string;
  country:  string;
  location: string;
  pin_code: string;
}

export interface Model 
{
  id:                       number;
  image_category:           any[];
  status:                   number;
  is_used:                  boolean;
  is_new:                   boolean;
  name:                     string;
  capacity_group:           string;
  capacity:                 number;
  capacity_unit:            string;
  sub_category:             string;
  model_code:               string;
  model_desc:               string;
  risk_engine_asset_id:     string;
  mfg_asset_id:             string;
  is_attachment_applicable: boolean;
  is_attachment:            boolean;
  hsn_code:                 string;
  is_discontinued:          boolean;
  year_launched:            number;
  year_discontinued:        number;
  is_registrable:           boolean;
  image_url:                string;
  asset_risk_rating:        number;
  environmental_rating:     number;
  fuel_capacity_ltr:        number;
  is_off_road:              boolean;
  safety_rating:            number;
  asset_cost:               number;
  model_banner_image:       string;
  updated_at:               Date;
  created_at:               Date;
  group:                    Group;
  category:                 Brand;
  brand:                    Brand;
}

export interface Group 
{
  id:                number;
  name:              string;
  description:       string;
  is_used:           boolean;
  is_new:            boolean;
  priority_for_use:  number;
  priority_for_new:  number;
  add_seo:           boolean;
  seo_title:         string;
  seo_meta_keywords: string;
  seo_meta_desc:     string;
  image_url:         string;
}
