import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userRole: any;
  cognitoId: any;
  validateSeller: any;
  incompleteProfile: boolean = false;
  rolesArray;
  constructor(public router:Router,private storage:StorageDataService,private apiRouteService:ApiRouteService,private appRouteEnum:AppRouteEnum,private commonService:CommonService) { }

  ngOnInit(): void {
    this.userRole = localStorage.getItem("userRole");
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    if(!this.userRole){
      this.rolesArray = this.storage.getStorageData("rolesArray", false);
      this.rolesArray = this.rolesArray.split(',');
      this.userRole = this.rolesArray[0];
    }
    // this.validateSeller = this.storage.getLocalStorageData("validateSeller",true)
    // if(this.validateSeller != null){
    //   if((!this.validateSeller.kyc_verified && this.validateSeller.kyc_verified!=2 ) || !this.validateSeller.pan_address_proof || !this.validateSeller.bank_details_exist || !this.validateSeller.allow){
    //     this.incompleteProfile = true
    //   }
    //   }
   
  }
  ngAfterViewInit(){
    this.ValidateSeller();
  }
  ValidateSeller() {
    let payload = {
      cognito_id: this.cognitoId,
      role: this.userRole
    }
    this.commonService.validateSeller(payload).subscribe(
      (res: any) => {
        if((!res.kyc_verified && res.kyc_verified!=2 ) || !res.pan_address_proof || !res.bank_details_exist || !res.allow){
          this.incompleteProfile = true
        }
      });
  }

  redirectToPage() {
   
    if (this.userRole == this.apiRouteService.roles.customer || this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.callcenter || this.userRole == this.apiRouteService.roles.rm || this.userRole == this.apiRouteService.roles.superadmin) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
    }
    else {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
      this.router.navigate([`./customer/dashboard/partner-dealer/basic-details`]);
    }
    
  }
}
