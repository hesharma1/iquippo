import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { MatDialog } from '@angular/material/dialog';
import { TransactionRecieptModalComponent } from 'src/app/component/shared/transaction-reciept-modal/transaction-reciept-modal.component';
import { ViewAllIdsComponent } from 'src/app/component/shared/view-all-ids/view-all-ids.component';
import { PaymentInfoDialogComponent } from 'src/app/component/admin/auction/payment-info-dialog/payment-info-dialog.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auction-payment-history',
  templateUrl: './auction-payment-history.component.html',
  styleUrls: ['./auction-payment-history.component.css']
})

export class AuctionPaymentHistoryComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-payment_date';
  public total_count: any;

  public displayedColumns: string[] = ['transaction_id', 'auction_engine_auction_id', 'parent_auction_engine_auction_id', 'eligible_for_refund', 'Registered_By', 'buyer__customer_number', 'is_refunded', 'User_Name', 'Mobile', 'email', "amount", 'payment_mode', 'payment_type', 'payment_date', 'payment_status', 'Action'];

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchText!: string;
  public paymentStatus: any;
  public paymentForType: any;
  form: FormGroup;
  public statusOptions: Array<any> = [
    { name: 'Initiated', value: 1 },
    { name: 'Success', value: 2 },
    { name: 'Failure', value: 3 },
    { name: 'Cancelled', value: 4 },
    { name: 'To be Realized', value: 5 },
    { name: 'Registration Cancelled', value: 6 },

  ];
  public now: Date = new Date();
  public listingData = [];
  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute, private router: Router,
    private ProductlistService: ProductlistService, private storage: StorageDataService,
    public notify: NotificationService, private datePipe: DatePipe, private dialog: MatDialog, private fb: FormBuilder) {
    this.form = this.fb.group({
      startDate: ["", Validators.required],
      endDate: ["", Validators.required]
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    if (this.userId) {
      this.getTransactionList();
    }
  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if (!this.scrolled) {
            this.scrolled = true;
            this.page++;
            this.getTransactionList();
          }
        }
      }
    }
  }
  getTransactionList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.userId,
      role: this.userRole,
      type__in: 1,
      payment_type__in: '1,3'
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.paymentStatus) {
      payload['payment_status__in'] = this.paymentStatus;
    }

    this.ProductlistService.getTransactionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            } else {
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getTransactionList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getTransactionList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getTransactionList();
    }
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      case 6: {
        return 'Registration Cancelled'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getServiceType(val) {
    switch (val) {
      case 1: {
        return 'Auction'
        break;
      }
      case 2: {
        return 'Insta Sell'
        break;
      }
      case 3: {
        return 'Bid'
        break;
      }
      case 4: {
        return 'Lead'
        break;
      }
      case 5: {
        return 'Advanced Valuation'
        break;
      }
      case 6: {
        return 'Insta Valuation'
        break;
      }
      case 7: {
        return 'Auction EMD'
        break;
      }
      case 8: {
        return 'Auction Fulfillment'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  export() {
    let date = this.form?.get('endDate')?.value;
    date = new Date(date);
    date = date.setDate(date.getDate() + 1);
    this.form.markAllAsTouched();
    if (this.form.valid) {

      let payload = {
        limit: -1,
        cognito_id: this.userId,
        role: this.userRole,
        type__in: 1,
        payment_type__in: '1,3'
      };

      if (this.form?.get('startDate')?.value) {
        payload['payment_date__gte'] = this.datePipe.transform(this.form?.get('startDate')?.value, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
      }
      if (this.form?.get('endDate')?.value) {
        payload['payment_date__lt'] = this.datePipe.transform(date, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
      }

      this.ProductlistService.getTransactionList(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          const excelData = res.results.map((r: any) => {
            let auctionId: any;
            let subAuctionId: any[] = [];
            let emd_subauctions: any;
            let emd_lots: any[] = [];
            let lots;
            let sub_lots: any[] = [];;
            let parentAuction;
            let baton_number;
            let client_reference_id;
            let eligible_for_refund;
            if (r?.emd_details && r?.emd_details.length > 0) {
              baton_number = r?.emd_details.filter(x => x.baton_number).map(x => x.baton_number).join(", ");
              client_reference_id = r?.emd_details.filter(x => x.client_reference_id).map(x => x.client_reference_id).join(", ");
              eligible_for_refund = r?.emd_details.filter(x => x.eligible_for_refund).map(x => x.eligible_for_refund).join(", ");
              // parentAuction = r?.emd_details.filter(x => x.auction_emd?.auction?.auction_engine_auction_id).map(x => x.auction_emd?.auction?.auction_engine_auction_id).join(", ");
              parentAuction = r?.emd_details[0].auction_emd?.auction?.auction_engine_auction_id;
              // r?.emd_details.forEach((x: any) => {
              //   emd_subauctions = x.auction_emd?.emd_subauctions;
              //   emd_lots = x.auction_emd?.emd_lots;
              // });
              emd_subauctions = r?.emd_details.filter(x => x.auction_emd?.emd_subauctions).map(x => x.auction_emd?.emd_subauctions);
              emd_lots = r?.emd_details.filter(x => x.auction_emd?.emd_lots).map(x => x.auction_emd?.emd_lots);
              if (emd_subauctions[0] && emd_subauctions[0]?.length > 0) {
                emd_subauctions.forEach((sub: any) => {
                  sub.forEach(element => {
                    subAuctionId.push(element.auction?.auction_engine_auction_id);
                  });
                });
                //auctionId = emd_subauctions.filter(x => x.auction?.auction_engine_auction_id).map(x => x.auction?.auction_engine_auction_id).join(", ");
              }
              else {
                auctionId = r?.emd_details.filter(x => x.auction_emd?.auction?.auction_engine_auction_id).map(x => x.auction_emd?.auction?.auction_engine_auction_id).join(", ");
              }
              let checkEmdType = r?.emd_details.filter(x => x.auction_emd?.emd_type == 'AU')
              if (checkEmdType && checkEmdType.length > 0) {
                lots = r?.emd_details.filter(x => x.auction_emd?.lots).map(x => x.auction_emd?.lots).join(", ");
              }
              else if (emd_subauctions[0] && emd_subauctions[0]?.length > 0) {
                emd_subauctions.forEach((sub: any) => {
                  sub.forEach(element => {
                    sub_lots.push(element.lots);
                  });
                });
                //lots = emd_subauctions.filter(x => x.lots).map(x => x.lots).join(", ");
              }
              else {
                //lots = emd_lots.filter(x => x.lot?.lot_number).map(x => x.lot?.lot_number).join(", ");
                emd_lots.forEach((lots: any) => {
                  lots.forEach(element => {
                    sub_lots.push(element?.lot?.lot_number);
                  });
                });
              }
            }
            return {
              "Transaction ID": r?.transaction_id,
              "Auction ID": subAuctionId && subAuctionId.length > 0 ? subAuctionId.join(",") : auctionId ? auctionId : '',
              "Parent Auction": parentAuction ? parentAuction : '',
              "Lots": lots ? lots : sub_lots && sub_lots.length > 0 ? sub_lots.join(",") : '',
              "Registered By": r?.emd_details &&
                r?.emd_details?.length && r?.emd_details[0]?.registered_by &&
                r?.emd_details[0]?.registered_by?.user_name ?
                r?.emd_details[0]?.registered_by?.user_name
                : '-',
              "User Id": r?.buyer?.customer_number ? r?.buyer?.customer_number : '-',
              "Client Reference ID": client_reference_id,
              "Baton No": baton_number,
              "Eligible for Refund": eligible_for_refund,
              "Refunded": r?.is_refunded ? 'Yes' : 'No',
              "User Name": r?.buyer?.first_name,
              "Mobile": r?.buyer?.mobile_number,
              "Email": r?.buyer?.email,
              "Amount": r?.amount,
              "Payment Mode": r?.payment_mode,
              "Payment Type": r?.is_online_payment ? 'Online' : 'Offline',
              "Request Date": this.datePipe.transform(r?.payment_date, 'yyyy-MM-dd'),
              "Payment Status": this.getPaymentStatus(r?.payment_status),
            }
          });
          ExportExcelUtil.exportArrayToExcel(excelData, "PaymentAll");
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      );
    }
  }

  showReceipt(ele) {
    const receiptDialogRef = this.dialog.open(TransactionRecieptModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        transactionDetails: ele,
      }
    });
  }

  viewAllIds(data, type) {
    if (type == 'lots') {
      const dialogRef = this.dialog.open(ViewAllIdsComponent, {
        width: '500px',
        disableClose: true,
        data: {
          isFromAuctionPaymentHistory: true,
          ids: data,
        }
      });
    }
    else {
      const dialogRef = this.dialog.open(ViewAllIdsComponent, {
        width: '500px',
        disableClose: true,
        data: {
          isEmdDetails: true,
          ids: data,
        }
      });
    }

  }

  viewPayment(data) {
    const dialogRef = this.dialog.open(PaymentInfoDialogComponent, {
      width: '500px',
      disableClose: true,
      data: {
        realizationDetails: data,
      }
    });
  }

  statusFilter(val) {
    this.paymentStatus = val;
    this.page = 1;
    this.getTransactionList();
  }

  getMinDate() {
    const date = new Date();
    const month = date.getMonth();
    date.setMonth(date.getMonth() - 1);
    while (date.getMonth() === month) {
      date.setDate(date.getDate() - 1);
    }
    return date;
  }

  getEndDate() {
    let date = this.form?.get('startDate')?.value;
    if (date) {
      date = new Date(date);
      return date;
    }
    // else {
    //   const date = new Date();
    //   // const month = date.getMonth();
    //   // date.setMonth(date.getMonth() - 1);
    //   // while (date.getMonth() === month) {
    //   //   date.setDate(date.getDate() - 1);
    //   // }
    //   return date;
    // }
  }



}