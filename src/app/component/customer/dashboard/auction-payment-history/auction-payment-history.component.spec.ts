import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionPaymentHistoryComponent } from './auction-payment-history.component';

describe('AuctionPaymentHistoryComponent', () => {
  let component: AuctionPaymentHistoryComponent;
  let fixture: ComponentFixture<AuctionPaymentHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionPaymentHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
