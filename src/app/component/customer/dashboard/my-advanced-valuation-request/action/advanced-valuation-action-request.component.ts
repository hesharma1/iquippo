import { Component, OnInit, Inject } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-advanced-valuation-action',
    templateUrl: './advanced-valuation-action-request.component.html',
    styleUrls: ['./advanced-valuation-action-request.component.css']
})

export class MyAdvancedValuationActionComponent implements OnInit
{
    type: any;
    paymentForm? : FormGroup;
    UCN? : any;
    modeOfPaymentObject : Array<any> = [{name: 'Cash', value : 'Cash'},
    {name: 'Cheque', value : 'Cheque'},
    {name: 'Demand Draft', value : 'Demand Draft'},
    {name: 'RTGS', value : 'RTGS'},
    {name: 'NEFT', value : 'NEFT'},
    {name: 'Debit Card', value : 'Debit Card'},
    {name: 'Net Banking', value : 'Net Banking'},
{name :'Wallet', value:'Wallet'},{
    name:'Online', value:'Online'
}]
    constructor(
        public dialogRef: MatDialogRef<MyAdvancedValuationActionComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    ){
        if(data){
            this.type = data.type;
        }
    }

    ngOnInit(){
this.initializePaymentForm();
    }

    initializePaymentForm(){
        this.paymentForm = new FormGroup({
            modeOfPayment : new FormControl(''),
            creationDate: new FormControl(''),
            paymentReferenceNo : new FormControl(''),
            amount :  new FormControl(''),
            dateOfPayment : new FormControl(''),
            bankName : new FormControl(''),
            branchName : new FormControl('')
        })
    }

    backClick(){
        this.dialogRef.close(false);
    }
    cancel(){
        this.dialogRef.close(true);
    }


    // rejectBid(){
    //     const putRejectTradeBody = {
    //         status: 7
    //     };
        
    //     this.adminMasterService.puttradeBid(putRejectTradeBody, this.UCN).subscribe((data: any) => {
    //       this.dialogRef.close();
    //       alert('Bid Rejected');
    //     });
    //   }

    updateInvoice(){

    }
}