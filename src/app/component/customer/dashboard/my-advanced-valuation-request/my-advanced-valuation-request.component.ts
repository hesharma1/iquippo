import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DatePipe } from '@angular/common';
import { ProductlistService } from 'src/app/services/product-list.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
// import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
// import { AdvancedInfoDialogComponent } from './info-dialog/advanced-info-dialog.component';
import { MyAdvancedValuationActionComponent } from './action/advanced-valuation-action-request.component';
import { MyAdvancedInfoDialogComponent } from './info/advanced-info-dialog.component';
import { ConfirmationPopupComponent } from 'src/app/component/admin/confirmation-popup/confirmation-popup.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SharedService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-advanced-valuation-request',
  templateUrl: './my-advanced-valuation-request.component.html',
  styleUrls: ['./my-advanced-valuation-request.component.css']
})
export class AdvancedValuationRequestComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  public filterFormGroup?: FormGroup;
  public showForm: boolean = false;
  public searchText!: string;
  public activePage: any = 1;
  public total_count: any;
  public dataSource!: MatTableDataSource<any>;
  public ordering: string = '-created_at';
  public cognitoId?: string;
  public pageSize: number = 10;
  public filter: any = ''; //Initially records according to request in draft;
  userRole?: string;
  toDate;
  fromDate;
  public isRequestOnHold: boolean = false;
  public cancelConfirmation: boolean = false;
  public ucn: any;
  todayDate = new Date();
  // public displayedColumns: string[] = [
  //   "unique_control_number","request_type","purpose","name","job_id","asset_number","report_number","status","payment_mode","request_date","oi_amount","registration_status","rc_number","run","acc_update","asset_condn", "status", "created_at"
  // ];
  public displayedColumns: string[] = [
    "unique_control_number",
    "advanced_valuation_type",
    // "purpose",
    "valuation_asset__seller__first_name",
    "valuation_asset__id",
    "created_at",
    "status",
    "transaction_id__payment_mode",
    "job_id",
    "report_number",
    // "view_all",
    "Actions"
  ];
  public statusOptions: Array<any> = [
    { name: 'Request In Draft', value: 1 },
    { name: 'Request Initiated', value: 2 },
    { name: 'Request Submitted', value: 3 },
    { name: 'Inspection In Progress', value: 4 },
    { name: 'Request On Hold', value: 5 },
    { name: 'Request Closed', value: 6 },
    { name: 'Inspection Completed', value: 7 },
    { name: 'Report Submitted', value: 8 },
    { name: 'Invoice Generated', value: 9 },
    { name: 'Invoice Modified', value: 10 },
    { name: 'Invoice Cancelled', value: 11 },
    { name: 'Payment Completed', value: 12 },
    { name: 'Request Cancelled', value: 13 },
  ];
  public listingData = [];
  constructor(private dialog: MatDialog,
    public dialogRef: MatDialogRef<MyAdvancedInfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private ProductlistService: ProductlistService,
    private usedUploadService: UsedEquipmentService,
    private route: Router,
    private datePipe: DatePipe,
    public spinner: NgxSpinnerService,
    public valService: ValuationService,
    public storage: StorageDataService, public notify: NotificationService,
    public router: ActivatedRoute
    , public fb: FormBuilder,
    private sharedService: SharedService) {
    this.showForm = false;
    this.router.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.route.url.substring(0, this.route.url.indexOf("?"));
        this.route.navigateByUrl(url);
      }
    })
  }

  /**ng on init */
  ngOnInit(): void {
    this.filterFormGroup = this.fb.group({
      filter: [null]
    });
    this.filterFormGroup.get('filter')?.setValue('');
    this.dataSource = new MatTableDataSource();
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getPage();
    let isfrompayment = localStorage.getItem('isfrompayment');
    if (isfrompayment) {
      this.notify.success('Payment done successfully!!', true);
      localStorage.removeItem('isfrompayment');
    }
  }
  scrolled= false;
  // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
         this.getPage();
        }
       }
     }
   }
 }

  /**get data from parent component */
  GetChildData(data: boolean) {
    this.showForm = data;
  }

  /**open dailog  */
  openDialog() {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      width: '40%',
      panelClass: 'custom-modalbox'
    });

    dialogRef.afterClosed().subscribe(
      res => {
        console.log('The dialog was closed');
      },
      err => {
        console.log(err);
      }
    );
  }

  openCancelDialog(u_c_n: any) {
    const dialogRef = this.dialog.open(MyAdvancedValuationActionComponent, {
      width: '40%',
      panelClass: 'custom-modalbox'
    });

    dialogRef.afterClosed().subscribe(
      res => {
        if (res) {
          this.cancel(u_c_n);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  /**get row status chnage */
  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    } else if (status == '3') {
      return "Rejected"
    }
    return "";
  }

  /**search list  */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.activePage = 1;
      this.getPage();
    }
  }

  statusFilter(val) {
    this.filter = val;
    this.activePage = 1;
    this.getPage();
  }

  /**get list from api */
  getPage() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.cognitoId,
      role: this.userRole
    };
    //   let payload = {
    //     page: this.activePage,
    //     limit: this.pageSize,
    //     ordering: this.ordering,
    //   };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
    if(this.fromDate){
      payload['created_at__date__gte'] = this.datePipe.transform(this.fromDate,'yyyy-MM-dd');
    }
    if(this.toDate){
      payload['created_at__date__lte'] = this.datePipe.transform(this.toDate,'yyyy-MM-dd');
    }
    this.valService.getAdvancedValuation(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
         if(window.innerWidth > 768){
          this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.activePage == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }  
        }
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    )
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPage();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getPage();
  }

  /**chnage status */
  approve(item) {
    item.status = 2;
    item.category = item.category.id;
    item.brand = item.brand.id;
    item.model = item.model.id;
    item.location = item.location.id;
    item.seller = item.seller.cognito_id;
    this.usedUploadService.postupdateUsedEquipment(item, item.id).pipe(finalize(() => { })).subscribe(
      res => {
        console.log(res);
        this.getPage()
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      })
  }

  /**chnage status */
  reject(item) {
    item.status = 3;
    item.category = item.category.id;
    item.brand = item.brand.id;
    item.model = item.model.id;
    item.location = item.location.id;
    item.seller = item.seller.cognito_id;
    this.usedUploadService.postupdateUsedEquipment(item, item.id).pipe(finalize(() => { })).subscribe(
      res => {
        console.log(res);
        this.getPage()
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      })
  }

  /**edit record navigate to page */
  editRecord(editableRow) {
    localStorage.setItem('usedProductRecordById', JSON.stringify(editableRow));
    this.route.navigate(['/admin-dashboard/products/used-products-upload'], { queryParams: { toEdit: true } });
  }

  /**Export Function */
  export() {
    let payload = {
      limit: 999,
      cognito_id: this.cognitoId,
      role: this.userRole
    }
    //   let payload = {
    //     limit: 999,
    //   }
    this.valService.getAdvancedValuation(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "UCN": r?.unique_control_number,
            "Request Type": r?.request_type,
            "User Name": r?.user_name,
            "Job ID": r?.job_id,
            "Asset No.": r?.asset_number,
            "Report No.": r?.report_number? r?.report_number : '',
            "Status": r?.status_display_name ,
            "Payment Mode": r?.payment_mode,
            "Request Date": r?.created_at
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Advanced Valuation");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  setRequestType(id: any) {
    let returnRequestType;
    switch (id) {
      case 1:
        returnRequestType = "Inspection";
        break;
      case 2:
        returnRequestType = "Valuation";
        break;
    }
    return returnRequestType;
  }

  setStatusDisplayName(id: any, reason: any) {
    let statusDisplayName;
    for (var i = 0; i < this.statusOptions.length; i++) {
      if (this.statusOptions[i].value == id) {
        statusDisplayName = id == 5 ? this.statusOptions[i].name + "-" + reason : this.statusOptions[i].name;
        if (id == 5) {
          this.isRequestOnHold = true;
        }
        break;
      }
    }
    return statusDisplayName;
  }

  moreInfoDialog(ucn: string) {
    const dialogRef = this.dialog.open(MyAdvancedInfoDialogComponent, {
      data: {
        uniqueNumber: ucn
      }
    });
  }

  bidAction(type: string, unique_number: any) {

    // const dialogRef = this.dialog.open(AdvancedValuationActionComponent, {
    //   data: {
    //     type: type,
    //     UCN: unique_number
    //   }
    // });
  }

  cancelConfirmationPopUp(u_c_n: any) {
    this.ucn = u_c_n;
    this.cancelConfirmation = true;
  }

  cancel(unique_number: any) {
    let payload = {
      "unique_control_number": unique_number,
    }
    this.valService.postCancelJob(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.notify.success('Job cancelled');
        this.getPage();
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  resumeJob(unique_number: any) {
    let payload = {
      unique_control_number: unique_number,
    }
    this.valService.postResumeJob(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.notify.success('Job Resumed');
        this.getPage();
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  setJobID(jobId: any) {
    let customizeValue;
    if (jobId == null) {
      customizeValue = "";
    }
    else {
      customizeValue = jobId;
    }
    return customizeValue;
  }

  isCancelButtonActive(status: any) {
    let isCancelButton = true;
    if (!(status == 1 || status == 2)) {
      isCancelButton = false;
    }
    return isCancelButton;
  }

  isInvoiceGenerationActive(rowData: any) {
    let isActive = false;
    // if((rowData.invoice_url == null) && (rowData.status == 9 || rowData.status == 10 || rowData.status == 11))
    // {
    //   isActive = true;
    // }
    if ((rowData.invoice_url == null) && (rowData.status == 8)) {
      isActive = true;
    }
    return isActive;
  }

  generateInvoice(unique_number: any) {
    let payload = {
      "unique_control_number": unique_number,
      "type": 1
    }
    this.valService.generateInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.notify.success('Invoice Generated');
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  backClick() {
    this.cancelConfirmation = false;
  }

  downloadInvoice(url: any) {
    if (url != '') {
      this.sharedService.download(url);
    }
  }

  edit(row) {
    this.route.navigate(['/valuation-request/val-details', row.unique_control_number]);
  }
  makePayment(row) {
    let payload = {
      asset_id: row.asset_number,
      cognito_id: this.cognitoId,
      source: 'Advance_valuation',
      payment_type: 2
    }
    this.usedUploadService.validateBid(payload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        if (data.redirect) {
          if (data.valuation_fee == 0) {
            let request = {
              transaction_id: data.id,
              status: 12
            }
            this.valService.patchAdvanceVal(row.unique_control_number, request).subscribe(res => {
              this.notify.success('Request Submitted Successfully!!', true);
              window.open("/customer/dashboard/advanced-valuation-request", '_self');
              //this.router.navigate(['/customer/dashboard/advanced-valuation-request'], { queryParams: { reload: true } });
            })
          } else {
            let paymentDetails = {
              asset_id: row.asset_number,
              asset_type: 'Used',
              case: 'Advance_valuation',
              payment_type: 'Full'
            }
            this.storage.setSessionStorageData('paymentSession', true, false);
            this.storage.setSessionStorageData('AdvanceValuation', window.btoa(JSON.stringify(data)), false);
            this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
            this.storage.setSessionStorageData('uniqueControlNumber', row.unique_control_number, false);
            // this.router.navigate(['/customer/payment']);
            window.open("/customer/payment", '_self');
          }
        } else {
          this.notify.error(data.message);
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  getDate(){
    if(this.toDate){
      return this.toDate;
    }else{
      return this.todayDate;
    }
  }
}
