import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { ConfirmDialogComponent } from "src/app/component/admin/new-equipment-master/location-master/confirm-dialog/confirm-dialog.component";
import { NewEquipmentService } from "src/app/services/customer-new-equipments.service";
import { StorageDataService } from "src/app/services/storage-data.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { DatePipe } from '@angular/common';
import { _fixedSizeVirtualScrollStrategyFactory } from "@angular/cdk/scrolling";

@Component({
  selector: 'app-my-demo',
  templateUrl: './my-demo.component.html',
  styleUrls: ['./my-demo.component.css']
})

export class MyDemoComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public displayColumns: string[] = [
    "new_equipment",
    "new_equipment__category__display_name",
    "pin_code__city__name",
    "start_date",
    "start_time",
    "status",
    'actions'
  ];
  public listingData = [];
  public cognitoId?: string;
  public userRole?: string;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public searchText!: string;
  public filterStatus: any;
  public demoStatusOptions: Array<any> = [
    { name: 'Requested', value: 1 },
    { name: 'Scheduled', value: 2 },
    { name: 'Completed', value: 3 },
    { name: 'Cancelled', value: 4 }
  ];
  
  constructor(public newEquipmentService: NewEquipmentService, private dialog: MatDialog, public spinner: NgxSpinnerService, public storage: StorageDataService, public notify: NotificationService, public datePipe: DatePipe) { }

  ngOnInit() {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getDemoList();
  }
  scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.page++;
        this.getDemoList();
      }
    }
    }
  }
}
  getDemoList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.cognitoId,
      role: this.userRole
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filterStatus) {
      payload['status__in'] = this.filterStatus;
    }
    this.newEquipmentService.getMyDemoList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
          if(window.innerWidth > 768){
            this.dataSource.data = res.results;
            }else{
              this.scrolled = false;
              if(this.page == 1){
                this.dataSource.data = [];
                this.listingData = [];
                this.listingData = res.results;
                this.dataSource.data = this.listingData;
              }else{
                this.listingData = this.listingData.concat(res.results);
                this.dataSource.data = this.listingData;
              }
            }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getDemoList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getDemoList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.page = 1;
      this.pageSize = 10;
      this.getDemoList();
    }
  }

  statusFilter(val) {
    this.filterStatus = val;
    this.page = 1;
    this.getDemoList();
  }

  getDemoStatus(status) {
    if (status) {
      let Obj = this.demoStatusOptions.find((d) => { return d.value == status });
      return Obj?.name;
    }
  }

  getStatusOptions(status) {
    let selectedStatusOptions = this.demoStatusOptions.filter((opt) => { return opt.value > status });
    return selectedStatusOptions;
  }

  updateStatus(val, data) {
    let payload = {
      status: val
    }
    this.newEquipmentService.updateDemoRequest(payload, data.id).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        if (res) {
          this.getDemoList();
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  deleteField(specId: number): void {
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent)
      .afterClosed().subscribe((data) => {
        if (data) {
          this.newEquipmentService.deleteMyDemoById(specId).pipe(finalize(() => {   })).subscribe(
            (res) => {
              this.notify.success('Demo request deleted successfully !!', true);
              this.getDemoList();
            },
            err => {
              console.log(err);
            });
        }
      });
  }

  export() {
    let payload = {
      limit: 999,
      cognito_id: this.cognitoId,
      role: this.userRole
    }
    this.newEquipmentService.getMyDemoList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Asset ID": r?.new_equipment?.id,
            "Asset Name": r?.new_equipment?.product_name,
            "Location": r?.pin_code?.city?.name,
            "Email": r?.email,
            "Start Date to End Date": this.datePipe.transform(r?.start_date,'dd-MM-yyyy') + ' to ' +  this.datePipe.transform(r?.end_date,'dd-MM-yyyy'),
            "Availability on Daily Basis":  r?.in_second_half ? 'Post lunch' : 'Pre lunch',
            "Status": this.getDemoStatus(r?.status)
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "My Demo Requests");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

}