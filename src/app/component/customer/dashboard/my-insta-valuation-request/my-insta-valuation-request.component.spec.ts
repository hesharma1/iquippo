import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInstaValuationRequestComponent } from './my-insta-valuation-request.component';

describe('MyInstaValuationRequestComponent', () => {
  let component: MyInstaValuationRequestComponent;
  let fixture: ComponentFixture<MyInstaValuationRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyInstaValuationRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInstaValuationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
