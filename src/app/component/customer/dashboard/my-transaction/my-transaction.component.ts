import { AfterViewInit, ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { MatDialog } from '@angular/material/dialog';
import { TransactionRecieptModalComponent } from 'src/app/component/shared/transaction-reciept-modal/transaction-reciept-modal.component';

@Component({
  selector: 'app-my-transaction',
  templateUrl: './my-transaction.component.html',
  styleUrls: ['./my-transaction.component.css']
})

export class MyTransactionComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-transaction__payment_date';
  public total_count: any;
  public displayedColumns: string[] = ['transaction__transaction_id', 'transaction__payment_id', 'transaction__order_id', 'type','transaction__payment_mode', 'transaction__payment_type', 'transaction__unique_control_number', 'transaction_type', 'transaction__asset_id', 'transaction__used_equipment__product_name', 'amount', 'status',  'transaction__payment_date', 'transaction__updated_at', 'action'];
  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchText!: string;
  public paymentStatus: any;
  public paymentForType: any;
  public listingData = [];
  public statusOptions: Array<any> = [
    { name: 'Initiated', value: 1 },
    { name: 'Success', value: 2 },
    { name: 'Failure', value: 3 },
    { name: 'Cancelled', value: 4 },
    { name: 'To be Realized', value: 5 }
  ];
  public serviceTypeOptions: Array<any> = [
    { name: 'Auction', value: 1 },
    { name: 'Insta Sell', value: 2 },
    { name: 'Bid', value: 3 },
    { name: 'Enquiry', value: 4 },
    { name: 'Advanced Valuation', value: 5 },
    { name: 'Insta Valuation', value: 6 },
    { name: 'Auction EMD', value: 7 },
    { name: 'Auction FulFillment', value: 8 },
    { name: 'Enterprise Valuation', value: 9 }
  ];

  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute, private router: Router, private ProductlistService: ProductlistService, private storage: StorageDataService, public notify: NotificationService, private datePipe: DatePipe, private dialog: MatDialog, public changeDetect: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.changeDetect.detectChanges();
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    if (this.userId) {
      this.getTransactionList();
    }
  }
  scrolled= false;
  // Changes for Card layout on Mobile devices
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if(window.innerWidth < 768){
      if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
        if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getTransactionList();
          }
        }
      }
    }
  }
  ngAfterViewInit() {
    // Changes done on 30th July 2021 

    // this.userId = this.storage.getStorageData('cognitoId', false);
    // this.userRole = this.storage.getStorageData('userRole', false);
    // if (this.userId) {
    //   this.getTransactionList();
    // }
  }

  getTransactionList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.userId,
      role: this.userRole,
      is_wallet: false
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.paymentStatus) {
      payload['status__in'] = this.paymentStatus;
    }
    if (this.paymentForType) {
      payload['type__in'] = this.paymentForType;
    }
    this.ProductlistService.getUserTransactionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
           // Changes for Card layout on Mobile devices
          if(window.innerWidth > 768){
          this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.page == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getTransactionList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getTransactionList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.page = 1;
      this.pageSize = 10;
      this.getTransactionList();
    }
  }

  statusFilter(val) {
    this.paymentStatus = val;
    this.page = 1;
    this.getTransactionList();
  }

  serviceTypeFilter(val) {
    this.paymentForType = val;
    this.page = 1;
    this.getTransactionList();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realised'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getServiceType(val) {
    switch (val) {
      case 1: {
        return 'Auction'
        break;
      }
      case 2: {
        return 'Insta Sell'
        break;
      }
      case 3: {
        return 'Bid'
        break;
      }
      case 4: {
        return 'Lead'
        break;
      }
      case 5: {
        return 'Advanced Valuation'
        break;
      }
      case 6: {
        return 'Insta Valuation'
        break;
      }
      case 7: {
        return 'Auction EMD'
        break;
      }
      case 8: {
        return 'Auction Fulfillment'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  export() {
    let payload = {
      limit: 999,
      cognito_id: this.userId,
      role: this.userRole,
      is_wallet: false
    }
    this.ProductlistService.getUserTransactionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Transaction ID": r?.transaction?.transaction_id,
            "Payment ID": r?.transaction?.payment_id,
            "Order ID": r?.transaction?.order_id,
            "Service": this.getServiceType(r?.type),
            "Payment Mode": r?.transaction?.payment_mode,
            'Unique Control No': r?.transaction?.unique_control_number,
            "Transaction Type": r?.transaction_type_display_name,
            "Asset ID": r?.transaction?.asset_id?.id,
            "Asset Name": r?.transaction?.asset_id?.product_name,
            "Amount": r?.amount,
            "Payment Status": this.getPaymentStatus(r?.status),
            "Payment Date": this.datePipe.transform(r?.payment_date, 'dd-MM-yyyy'),
            "Transaction Date": this.datePipe.transform(r?.updated_at, 'dd-MM-yyyy hh:mm a'),
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "My Transactions");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  showReceipt(ele) {
    const receiptDialogRef = this.dialog.open(TransactionRecieptModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        transactionDetails: ele,
      }
    });
  }
}
