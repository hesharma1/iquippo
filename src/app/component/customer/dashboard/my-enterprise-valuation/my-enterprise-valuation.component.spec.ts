import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyEnterpriseValuationComponent } from './my-enterprise-valuation.component';

describe('MyEnterpriseValuationComponent', () => {
  let component: MyEnterpriseValuationComponent;
  let fixture: ComponentFixture<MyEnterpriseValuationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyEnterpriseValuationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyEnterpriseValuationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
