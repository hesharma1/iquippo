import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NewEquipmentService } from "src/app/services/customer-new-equipments.service";
import { StorageDataService } from "src/app/services/storage-data.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from 'rxjs/operators';
import { ConfirmDialogComponent } from "src/app/component/admin/new-equipment-master/location-master/confirm-dialog/confirm-dialog.component";
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-my-enquiries',
  templateUrl: './my-enquiries.component.html',
  styleUrls: ['./my-enquiries.component.css']
})
export class MyEnquiriesComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  // public displayedColumns = ["id","new_equipment__category__display_name", "new_equipment__model__name",
  //   "number_of_equipment_avail", "number_of_equipment_required", "equipment_req_in", "user__first_name", "user__mobile_number", "new_equipment__seller__first_name", "new_equipment__seller__mobile_number", "created_at", "is_offer_enquiry", "status", "Action"];

  public displayedColumns = ["id", "new_equipment__category__display_name","new_equipment__brand__display_name", "new_equipment__model__name",
    "remarks", "user__first_name", "user__mobile_number", "created_at", "is_offer_enquiry", "status", "Action"];
  public cognitoId?: string;
  public userRole?: string;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public searchText!: string;
  public filterStatus: any;
  public filterEquipmentReq: any;
  public listingData = [];
  public statusOptions: Array<any> = [
    { name: 'Open', value: 1 },
    { name: 'In Progress', value: 2 },
    { name: 'Deal Submitted', value: 3 },
    { name: 'Deal in progress', value: 4 },
    { name: 'Purchase Completed', value: 5 },
    { name: 'Rejected By Seller', value: 6 },
    { name: 'Rejected By Buyer', value: 7 }
  ];
  public equipmentRequiredOptions: Array<any> = [
    { name: 'Immediate', value: 1 },
    { name: 'Within next 60 Days', value: 2 },
    { name: 'Not Sure', value: 3 }
  ];

  constructor(private route: ActivatedRoute, public router: Router, private newEquipmentService: NewEquipmentService, public dialog: MatDialog, public app: ApiRouteService, public spinner: NgxSpinnerService, public storage: StorageDataService, public notify: NotificationService, public datePipe: DatePipe) { }

  ngOnInit() {
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getEnquiryList();
  }
  scrolled = false; 
  // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true; 
        this.page++;
         this.getEnquiryList();
        }
       }
     }
   }
 }

  getEnquiryList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      cognito_id: this.cognitoId,
      role: this.userRole
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filterStatus) {
      payload['status__in'] = this.filterStatus;
    }
    // if (this.filterEquipmentReq) {
    //   payload['equipment_req_in__in'] = this.filterEquipmentReq;
    // }
    this.newEquipmentService.getMyEnquiryList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
         if(window.innerWidth > 768){
          this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.page == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          } 
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getEnquiryList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getEnquiryList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText.length > 3) {
      this.page = 1;
      this.pageSize = 10;
      this.getEnquiryList();
    }
  }

  statusFilter(val) {
    this.filterStatus = val;
    this.page = 1;
    this.getEnquiryList();
  }

  equipmentRequiredFilter(val) {
    this.filterEquipmentReq = val;
    this.page = 1;
    this.getEnquiryList();
  }

  getEnquiryStatus(val) {
    switch (val) {
      case 1: {
        return 'Open'
        break;
      }
      case 2: {
        return 'In Progress'
        break;
      }
      case 3: {
        return 'Deal Submitted'
        break;
      }
      case 4: {
        return 'Deal in progress'
        break;
      }
      case 5: {
        return 'Purchase Completed'
        break;
      }
      case 6: {
        return 'Rejected By Seller'
        break;
      }
      case 7: {
        return 'Rejected By Buyer'
        break;
      }
      default: {
        return 'Open'
        break;
      }
    }
  }

  getEquipmentRequiredTime(val) {
    switch (val) {
      case 1: {
        return 'Immediate'
        break;
      }
      case 2: {
        return 'Within next 60 Days'
        break;
      }
      case 3: {
        return 'Not Sure'
        break;
      }
      default: {
        return 'Not Sure'
        break;
      }
    }
  }

  viewEnquiry(id) {
    this.router.navigate(['/customer/dashboard/enquiry-chat'], { queryParams: { id: id } });
  }

  deleteEnquiry(id) {
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed().subscribe((data) => {
        if (data) {
          this.newEquipmentService.deleteEnquiry(id).pipe(finalize(() => { })).subscribe(
            (res: any) => {
              this.notify.success('Enquiry successfully deleted!!');
              this.getEnquiryList();
            },
            err => {
              console.log(err);
              this.notify.error(err.message);
            }
          );
        }
      });
  }

  export() {
    let payload = {
      limit: 999,
      cognito_id: this.cognitoId
    }
    if (this.userRole) {
      payload['role'] = this.userRole;
    }
    this.newEquipmentService.getMyEnquiryList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        console.log("")
        const excelData = res.results.map((r: any) => {
          return {
            "Enquiry ID": r?.id,
            "Equipment Category": r?.new_equipment?.category?.name,
            "Equipment Brand": r?.new_equipment?.brand?.name,
            "Equipment Model": r?.new_equipment?.model?.name,
            "Description": r?.remarks,
            // "Equipment Available": r?.number_of_equipment_avail,
            // "Equipment Required": r?.number_of_equipment_required,
            // "Equipment Required In": this.getEquipmentRequiredTime(r?.equipment_req_in),
            "Buyer Name": r?.status == 5 ? (r?.user?.first_name + r?.user?.last_name) : 'NA',
            "Buyer Contact": r?.status == 5 ? (r?.user?.mobile_number) : 'NA',
            "Seller Name": r?.status == 5 ? (r?.new_equipment?.seller?.first_name + r?.new_equipment?.seller?.last_name) : 'NA',
            "Seller Contact": r?.status == 5 ? (r?.new_equipment?.seller?.mobile_number) : 'NA',
            "Request Date": this.datePipe.transform(r?.updated_at, 'yyyy-MM-dd'),
            "Enquiry Type": r?.is_offer_enquiry ? 'Offer Enquiry' : 'General Enquiry',
            "Status": this.getEnquiryStatus(r?.status),
            "Final Offer Price": r?.final_offer_price,
            "Booking Value": r?.booking_value
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "My Enquiries");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }
}
