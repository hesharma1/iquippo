import { Component, OnInit } from '@angular/core';
import { Subject } from "rxjs";
import { Router } from '@angular/router';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { CommonService } from 'src/app/services/common.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { environment } from 'src/environments/environment';
import { AuctionService } from 'src/app/services/auction.service';
import { finalize } from 'rxjs/operators';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  filteredData = new Subject<any>();
  sidebartgl: boolean = true;
  wrappertgl: boolean = true;
  isLoggedIn: boolean = false;
  userFirstName?: string;
  userLastName?: string;
  userdata: any;
  mp1HomePage?: string;
  addClass1: boolean = false;
  addClass2: boolean = false;
  addClass3: boolean = false;
  addClass4: boolean = false;
  addClass5: boolean = false;
  addClass6: boolean = false;
  addClass7: boolean = false;
  addClass8: boolean = false;
  addClass9: boolean = false;
  addClass10: boolean = false;
  addClass11: boolean = false;
  addClass12: boolean = false;
  userRole: any;
  rolesArray;
  customerMenu = [
    "Dashboard",
    "My Profile",
    "My Assets",
    "My Entity",
    "Used Equipments",
    "My Enquires",
    "My Trades",
    "My Agreements",
    "My Transactions",
    "My Insta Request",
    "My Advanced Request",
    "My Wallet",
    "Demo Request",
    "Wishlist",
    "Auction Payment History",
    "Auction Fulfilment List",
    "Bulk Upload Requests"
  ];
  dealerMenu = [
    "Dashboard",
    "My Profile",
    "My Assets",
    "New Equipments",
    "Used Equipments",
    "My Enquires",
    "My Trades",
    "My Agreements",
    "My Transactions",
    "My Valuation Request",
    "My Wallet",
    "Wishlist",
    "My Offers",
    "Demo Request",
    "Bulk Upload Requests"
  ];
  manufacturerMenu = [
    "Dashboard",
    "My Profile",
    "My Assets",
    "New Equipments",
    "Used Equipments",
    "My Enquires",
    "My Trade",
    "My Agreements",
    "My Transactions",
    "Demo Request",
    "My Wallet",
    "Wishlist",
    "My Offers",
    "Bulk Upload Requests"
  ];
  channelPartnerMenu = [
    "Dashboard",
    "My Profile",
    "My Assets",
    "New Equipments",
    "Used Equipments",
    "My Enquires",
    "My Trade",
    "Seller Agreements",
    "My Transactions",
    "My Wallet",
    "Demo Request",
    "Wishlist",
    "My Offers",
     "Customers"
  ];

  enterprisePartner = [
    "Enterprise Valuation Request",
    // "Bulk upload",
    "My Profile",
    "Enterprise Valuation Requests",

  ]

  guestMenu = [
    "My Profile",
    "My Agreements"
  ]
  callCenterMenu = [
    "Customer Management"
  ]
  cognitoId: any;
  showAgreementLink: boolean = false;
  showAgreementLinkForBidder: boolean = false;
  eSignReqData: any;
  role: any;
  constructor(public awsCognito: AwsAuthService, public router: Router, private sharedService: SharedService, private storage: StorageDataService,
    public appRouteEnum: AppRouteEnum, private apiRouteService: ApiRouteService, private commonService: CommonService,
    public auctionService: AuctionService,public notify:NotificationService) {
    this.mp1HomePage = environment.mp1HomePage;
  }

  ngOnInit(): void {
    // localStorage.removeItem("userRole");
    // localStorage.setItem("userRole", "Customer");
    //this.userRole = this.storage.getStorageData("role", false);
    this.role = this.storage.getStorageData("role", false);
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    this.rolesArray = this.rolesArray.split(',');
    if (!localStorage.getItem("userRole")) {
      // if (this.rolesArray.includes('Customer')) {
      //   this.userRole = this.apiRouteService.roles.customer;
      //   localStorage.setItem("userRole", this.userRole);
      // }
      if (this.rolesArray[0] != this.apiRouteService.roles.admin && this.rolesArray[0] != this.apiRouteService.roles.superadmin) {
        this.userRole = this.rolesArray[0];
        localStorage.setItem("userRole", this.userRole);
      }
      else {
        this.userRole = this.rolesArray[0];
        localStorage.setItem("userRole", this.userRole);
        if (this.userRole == this.apiRouteService.roles.superadmin || this.userRole == this.apiRouteService.roles.admin) {
          this.router.navigate(['/admin-dashboard'])
        }
      }
    }else {
      this.userRole = localStorage.getItem("userRole");
      localStorage.setItem("userRole", this.userRole);
      if (this.userRole == this.apiRouteService.roles.superadmin || this.userRole == this.apiRouteService.roles.admin) {
        this.router.navigate(['/admin-dashboard'])
      }
    }
    this.ValidateSeller();
    this.getESignTemplate();
    this.getUserRoles();
  }

  showsubmenu1() {
    this.addClass1 = !this.addClass1;
  }

  showsubmenu2() {
    this.addClass2 = !this.addClass2;
  }

  ValidateSeller() {
    let payload = {
      cognito_id: this.cognitoId,
      role: this.userRole
    }
    this.commonService.validateSeller(payload).subscribe(
      (res: any) => {
        this.storage.setLocalStorageData("validateSeller", res, true)
        if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof || !res.bank_details_exist) {

        }
        else {

            if (this.userRole != this.apiRouteService.roles.customer && !res.partner_admin)
            this.showAgreementLink = false;
            else
            {
              // if(this.userRole == this.apiRouteService.roles.partnerGuest){
              //   this.notify.warn("Please re-login again to continue.",true);
              //   setTimeout(() => {
              //     this.sharedService.logout();
              //   }, 2000);
              // }
              if(this.userRole != this.apiRouteService.roles.customer && this.userRole != this.apiRouteService.roles.rm)
              this.showAgreementLink = true;
            }
            if(this.userRole == this.apiRouteService.roles.customer && res.bank_details_exist && res.pan_address_proof){
              this.showAgreementLink = true;
            }
          
         
            // this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      });
  }

  getESignTemplate() {
    const params = {
      bidder__cognito_id__in: this.cognitoId
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        if (res && res.results) {
          this.eSignReqData = res.results as any[];
        }
        this.storage.setLocalStorageData("eSignData", res.results, true)

        if (res.results && res.results && res.results.length > 0)
          this.showAgreementLinkForBidder = true;
        else
          this.showAgreementLinkForBidder = false;
      });
  }

  showsubmenu3() {
    this.addClass3 = !this.addClass3;
  }

  showsubmenu4() {
    this.addClass4 = !this.addClass4;
  }

  showsubmenu5() {
    this.addClass5 = !this.addClass5;
  }

  showsubmenu6() {
    this.addClass6 = !this.addClass6;
  }

  showsubmenu7() {
    this.addClass7 = !this.addClass7;
  }

  showsubmenu8() {
    this.addClass8 = !this.addClass8;
  }

  showsubmenu9() {
    this.addClass9 = !this.addClass9;
  }

  showsubmenu10() {
    this.addClass10 = !this.addClass10;
  }

  showsubmenu11() {
    this.addClass11 = !this.addClass11;
  }

  showsubmenu12() {
    this.addClass12 = !this.addClass12;
  }

  /**get user role */

  getUserRoles() {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
        this.storage.setStorageData("UserInfo", res, true);
      })
  }

  redirecttologin() {
    this.router.navigate([`./` + this.appRouteEnum.Login]);
  }

  redirectToPage(page: string) {
    if (page == "profile") {
      if (this.userRole == this.apiRouteService.roles.customer || this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.callcenter || this.userRole == this.apiRouteService.roles.rm || this.userRole == this.apiRouteService.roles.superadmin) {
        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
      }
      else {
        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
        this.router.navigate([`./customer/dashboard/partner-dealer/basic-details`]);
      }
    }
    else if (page == "entity") {
      if (this.userRole == this.apiRouteService.roles.customer || this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.callcenter || this.userRole == this.apiRouteService.roles.rm || this.userRole == this.apiRouteService.roles.superadmin) {
        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.router.navigate([`./` + this.appRouteEnum.CorporateDetails], { queryParams: { cognitoId: this.cognitoId } });
      }
      else {
        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
        this.router.navigate([`./customer/dashboard/partner-dealer/corporate-details`]);
      }
    }
    else if (page == "agreement") {
      if (this.userRole == this.apiRouteService.roles.customer) {
        this.router.navigate([`./dashboard/my-agreements`]);
      }
      else if (this.userRole == this.apiRouteService.roles.dealer || this.userRole == this.apiRouteService.roles.manufacturer) {
        this.router.navigate([`./customer/dashboard/partner-dealer/agreement-details`]);
      }
      else {
        this.router.navigate([`./customer/dashboard/partner-dealer/agreement-details`]);
      }
    }
  }

  logout() {
    this.sharedService.logout();
    // this.awsCognito.awsSignOut().then(() => {
    //   // console.log('qsqw');
    //   // this.navCtrl.setDirection('root');
    //   this.storage.cleanAll();
    //   this.sharedService.removeCookie(this.appRouteEnum.globalCookieName);
    //   //this.router.navigateByUrl(this.appRouteEnum.Login);
    //   window.location.href = environment.mp1LogoutUrl;
    // }).catch(e => {
    //   console.log('getting error');
    // });
  }
  sidebartoggle() {
    this.sidebartgl = !this.sidebartgl;
    this.wrappertgl = !this.wrappertgl;
  }
  backTohome() {
    this.sharedService.redirecteToMP1AfterLogin();
  }
  isAccesible(option: string) {
    if (this.userRole == this.apiRouteService.roles.customer) {
      if (this.customerMenu.includes(option)) {
        return true;
      }
    } else if (this.userRole == this.apiRouteService.roles.rm) {
      if (this.channelPartnerMenu.includes(option)) {
        return true;
      }
    } else if (this.userRole == this.apiRouteService.roles.dealer) {
      if (this.dealerMenu.includes(option)) {
        return true;
      }
    } else if (this.userRole == this.apiRouteService.roles.manufacturer) {
      if (this.manufacturerMenu.includes(option)) {
        return true;
      }
    }
    else if (this.userRole == this.apiRouteService.roles.enterprisePartner) {
      if (this.enterprisePartner.includes(option)) {
        return true;
      }
    }
    else if (this.userRole == this.apiRouteService.roles.callcenter) {
      if (this.callCenterMenu.includes(option)) {
        return true;
      }
    }
    else if (this.userRole == this.apiRouteService.roles.guest || this.userRole == this.apiRouteService.roles.partnerGuest) {
      if (this.guestMenu.includes(option)) {
        return true;
      }
    }
    return false;
  }

  switchRole(role) {
    this.userRole = role;
    localStorage.removeItem("userRole");
    if (this.userRole == this.apiRouteService.roles.superadmin) {
      localStorage.setItem("userRole", role);
      //localStorage.removeItem("UserInfo");
      this.router.navigate(['/admin-dashboard']);
    } 
    else if (this.userRole == this.apiRouteService.roles.admin) {
      localStorage.setItem("userRole", role);
      this.router.navigate(['/admin-dashboard']);
    } 
    else if (this.userRole == this.apiRouteService.roles.rm) {
      localStorage.setItem("userRole", role);
      this.router.navigate(['/admin-dashboard']);      
    } 
    else {
      localStorage.setItem("userRole", role);
      this.router.navigate(['/customer/dashboard/']);
    }
  }
}
