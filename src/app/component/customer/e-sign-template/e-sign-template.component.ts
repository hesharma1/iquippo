import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { AuctionService } from 'src/app/services/auction.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
declare var esignWidgetCall: any;

@Component({
  selector: 'app-e-sign-template',
  templateUrl: './e-sign-template.component.html',
  styleUrls: ['./e-sign-template.component.css']
})
export class ESignTemplateComponent implements OnInit {
  reqData: any;
  widgetToken: any;
  widgetData: any;
  cognitoId: any;
  constructor(public auctionService: AuctionService, @Inject(MAT_DIALOG_DATA) data: any, private router: Router,
    private notify: NotificationService,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // let url = this.route.snapshot.params.id;
    // this.onProceed(url)
    //this.mysteryUrl = this.mysteryUrl.substr(1);
    this.cognitoId = localStorage.getItem('cognitoId');
    this.getESignTemplate();
  }

  getESignTemplate() {
    const params = {
      bidder__cognito_id__in: this.cognitoId
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        if (res && res.results && res.results[0]) {
          this.mycall(res.results[0]);
        }
      });
  }

  mycall(res) {
    var self = this;
    var clientData = {
      document_id: res.document_id,
      signer_id: res.signer_id,
      docket_id: res.docket_id,
      id: "iquippo-services-limited-_UAT_1",
      widget_width: "100%",
      widget_height: "100%",
      getWidgetToken: function (result) {
        self.eSignCaptureWidgetCall(res.reference_id, result);
      },
      signatureStatus: function (result) {
        self.eSignCaptureStatus(res.reference_id);
        if (result && result.status && result.status == 'success') {
          self.notify.success(
            "Congratulations!! You have successfully signed the Bidder Undertaking form. You can now participate in Auctions by paying the requisite registration amount."
          );
        }
        else if (result && result.status) {
          self.notify.warn(
            result.status
          );
        }
        // function will return signature status
      },
      errorLog: function (result) {
        self.notify.error(
          result
        );
      }
    }
    this.widgetData = clientData;
    esignWidgetCall(this.widgetData);
  }

  onProceed(url) {
    const params = {
      reference_id: url,
    }
    this.auctionService.patchAuctionEsign(params, url).subscribe(
      res => {
        this.mycall(res);
      });
  }

  eSignCaptureWidgetCall(param, widgetTokn) {
    const params = {
      reference_id: param,
      widget_token: widgetTokn,
    }
    this.auctionService.eSignCaptureWidget(params).subscribe(
      res => {
      });
  }

  eSignCaptureStatus(param) {
    const params = {
      reference_id: param,
    }
    this.auctionService.postAuctionEsignStatus(params).subscribe(
      (res: any) => {
        if (res && res.status && res.status == 'BUS') {
          let returnUrl = localStorage.getItem('returnUrl');
          
          if (returnUrl != undefined && returnUrl != null && returnUrl != 'undefined' && returnUrl != '') {
            this.router.navigate([returnUrl]);
          }
          else {
            this.router.navigate([`/dashboard/my-agreements`], { queryParams: { 'isForEsign': true } });
          }
        }
      });
  }

}
