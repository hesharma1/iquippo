import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ESignTemplateComponent } from './e-sign-template.component';

describe('ESignTemplateComponent', () => {
  let component: ESignTemplateComponent;
  let fixture: ComponentFixture<ESignTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ESignTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ESignTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
