import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NewEquipmentService } from './../../../../services/customer-new-equipments.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { AdminNewProductUpload } from '../../../../models/common/new-product-upload.model';
import { StorageDataService } from './../../../../services/storage-data.service';
import { NewEquipmentOfferMaster } from '../../../../models/common/new-product-upload.model';
import { environment } from 'src/environments/environment';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { DatePipe } from '@angular/common';
import { AnyNsRecord } from 'dns';
import { Observable } from "rxjs";
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css'],
})
export class OffersComponent implements OnInit {
  isShowOfferTypes = false;
  isOfferCash = false;
  isOfferLease = false;
  isOfferFinance = false;
  infoLevel: number = 2;
  customerId: any;
  cognitoId: any;
  offersDetailList: any;
  adminNewProductUpload: any;
  newEquipmentOfferMaster: any;
  page = 1;
  ordering: any = '1';
  searching: any = '2';
  pageSize = 1;
  basicDetailList: any;
  maxOffersArrayLength = 5;
  offersDataFinal: any[] = [];
  offersMasterFinal: any[] = [];
  offersUpdateData: any[] = [];
  stateList: any = [];
  offersMasterResponce: any;
  myDate = new Date();
  iscashTypeValue = false;
  isLeaseTypeValue = false;
  public offersForm: FormGroup = new FormGroup({
    offersArray: new FormArray([this.createOffer()]),
  });
  offersArray = this.offersForm.get('offersArray') as FormArray;
  // public offersForm!: FormGroup;
  // offersArray= this.offersForm.get('offersArray') as FormArray;
  image_path: any;
  endDate: any;
  documentName: Array<any> = [];
  documentNameTc: Array<any> = [];
  object: any = {};
  updateOffermaster: any;
  isPanAvailableChecked: boolean = false;
  countryDataSource: any;
  cityDataSource: any;
  stateDataSource: any;
  role:any;
  roles: any;
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageDataService,
    public newEquipmentService: NewEquipmentService,
    private fb: FormBuilder,
    public spinner: NgxSpinnerService,
    public s3: S3UploadDownloadService,
    private agreementServiceService: AgreementServiceService,
    private datePipe: DatePipe,
    private adminMasterService: AdminMasterService,
    private apiRouteService: ApiRouteService,
  ) { 
    this.getPreInitData();
  }

  async getPreInitData(){
    await this.renderCountryData();
  }
  ngOnInit(): void {
    this.role = this.storage.getStorageData("userRole", false);
    this.roles = this.storage.getStorageData("role", false);

    this.cognitoId = localStorage.getItem('cognitoId');
    this.getEditDeleteId();
    this.getNewEquipmentList();
    //this.getStateData();
    this.dateValidation();
    //this.addOffer();
    this.getNewOffersList();
    

  }

  /**onchnage offer type
   *
   */
  dateValidation() {
    var d = new Date();
    var curr_date = d.getDate() + 1;
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear()
    let minEndDate = curr_date + "-" + curr_month + "-" + curr_year;
    this.endDate = new Date(minEndDate);
  }

  onOfferTypeSelect(event: any) {
    this.isShowOfferTypes = event;
    if (event == 1) {
      this.isOfferCash = false;
      this.isOfferLease = false;
      this.isOfferFinance = true;
    } else if (event == 2) {
      this.isOfferCash = false;
      this.isOfferLease = true;
      this.isOfferFinance = false;
    } else {
      this.isOfferCash = true;
      this.isOfferLease = false;
      this.isOfferFinance = false;
    }
  }

  /**
   * add more offer on click
   */
  addOffer() {
    let offers = this.offersForm.get('offersArray') as FormArray;
    if (offers.length < this.maxOffersArrayLength) {
      offers.push(this.createOffer());
    }
  }

  /**
   * get state data
   */

  /**
   * offer validations
   */
  getOffersLength() {
    let offers = this.offersForm.get('offersArray') as FormArray;
    return offers.length;
  }

  /**
   * create offer form group
   */
  createOffer(): FormGroup {
    return new FormGroup({
      typeOfOffer: new FormControl(''),
      valueOfOffer: new FormControl(''),
      offerTermsAndCond: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
      offerPicture: new FormControl(''),
      dealer: new FormControl(''),
      freeOfCost: new FormControl(''),
      tenure: new FormControl(''),
      amount: new FormControl(''),
      rate: new FormControl(''),
      margin: new FormControl(''),
      processingFee: new FormControl(''),
      installment: new FormControl(''),
      description: new FormControl(''),
      PinCode: new FormControl(''),
      State: new FormControl(''),
      City: new FormControl(''),
      offerId: new FormControl(''),
      Country: new FormControl('1')
    });
  }

  updateValidations(i) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    if (arrayControl.controls[i]?.get('typeOfOffer')?.value == 3) {
      
      arrayControl.controls[i]?.get('typeOfOffer')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('valueOfOffer')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('valueOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('startDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('startDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('endDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('endDate')?.updateValueAndValidity();
     
      arrayControl.controls[i]?.get('freeOfCost')?.clearValidators();
      arrayControl.controls[i]?.get('freeOfCost')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('tenure')?.clearValidators();
      arrayControl.controls[i]?.get('tenure')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('amount')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('amount')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('rate')?.clearValidators();
      arrayControl.controls[i]?.get('rate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('margin')?.clearValidators();
      arrayControl.controls[i]?.get('margin')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('installment')?.clearValidators();
      arrayControl.controls[i]?.get('installment')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('processingFee')?.clearValidators();
      arrayControl.controls[i]?.get('processingFee')?.updateValueAndValidity();
      if(this.role == this.apiRouteService.roles.dealer) {
        arrayControl.controls[i]?.get('Country')?.clearValidators();
        arrayControl.controls[i]?.get('Country')?.updateValueAndValidity();
      } else {
        arrayControl.controls[i]?.get('Country')?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('Country')?.updateValueAndValidity();
      }
    }
    if (arrayControl.controls[i]?.get('typeOfOffer')?.value == 1 || arrayControl.controls[i]?.get('typeOfOffer')?.value == 2) {
      arrayControl.controls[i]?.get('typeOfOffer')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('valueOfOffer')?.clearValidators();
      arrayControl.controls[i]?.get('valueOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('startDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('startDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('endDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('endDate')?.updateValueAndValidity();
      // arrayControl.controls[i]?.get('Country')?.clearValidators();
      // arrayControl.controls[i]?.get('Country')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('freeOfCost')?.clearValidators();
      arrayControl.controls[i]?.get('freeOfCost')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('tenure')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('tenure')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('amount')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('amount')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('rate')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('rate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('margin')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('margin')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('installment')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('installment')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('processingFee')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('processingFee')?.updateValueAndValidity();
      if(this.role == this.apiRouteService.roles.dealer) {
        arrayControl.controls[i]?.get('Country')?.clearValidators();
        arrayControl.controls[i]?.get('Country')?.updateValueAndValidity();
      } else {
        arrayControl.controls[i]?.get('Country')?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('Country')?.updateValueAndValidity();
      }
    }
    return this.offersForm;
  }
  /**
   * remove offers
   */
  removeOffer(i: number) {
    const control = <FormArray>this.offersForm.controls['offersArray'];
    let offer = control.controls[i];
    if (offer?.get('offerId')?.value) {
      this.newEquipmentService.deleteOfferMaster(offer?.get('offerId')?.value).subscribe(res => {
        control.removeAt(i);
      });
    } else {
      control.removeAt(i);
    }
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  /*** get state data*/
  getLocationData(event, i) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    let data = arrayControl.controls[i]?.get('PinCode')?.value
    if (data && data.length > 5) {
      let queryParam = `search=${data}`;
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
           
          this.stateList = res.results;
          if (this.stateList && this.stateList.length > 0) {
            arrayControl.controls[i]?.get('Country')?.setValue(this.stateList[0].city.state?.country?.id);
          arrayControl.controls[i]?.get('State')?.setValue(this.stateList[0].city.state?.id);
          arrayControl.controls[i]?.get('City')?.setValue(this.stateList[0].city?.id);
          if(this.stateList[0].city.state?.country?.id){
            this.getStates(this.stateList[0].city.state?.country?.id)
          }
         if(this.stateList[0].city.state?.id){
          this.getCities(this.stateList[0].city.state?.id) 
         }
          }
          if ((this.stateList && this.stateList.length == 0) || data.length < 5) {
            arrayControl.controls[i]?.get('State')?.setValue('')
            arrayControl.controls[i]?.get('City')?.setValue('')
          }
        },
        (err) => {
          console.error(err);
        }
      );
    } else {
      arrayControl.controls[i]?.get('State')?.setValue('')
      arrayControl.controls[i]?.get('City')?.setValue('')
    }
  }

  confirmOffer() {
    let offers = this.offersForm.get('offersArray') as FormArray;
    let filtered = offers.value.filter(item => item.typeOfOffer == '' && offers.value.length >= 1);
    if (filtered.length == 1 && offers.value.length == 1) {
      this.router.navigate(['new-equipment/visuals', this.customerId]);
    } else {
      this.saveOffersBeforeSubmit();
    }
  }

  getNewEquipmentList() {
    //this.spinner.show();
    this.newEquipmentService.getBasicDetailList(this.customerId).subscribe(
      (res: any) => {
         
        this.basicDetailList = res;
        this.role = this.getSellerType(this.basicDetailList.seller_type);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /**upload files and video by this method */
  async handleUpload(event: any, tab: string, side: string, index: any) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    const file = event.target.files[0];
    if (
      (file.type == 'image/png' ||
        file.type == 'image/jpg' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/bmp' ||
        file.type == 'application/kswps'
      ) &&
      file.size <= 5242880
    ) {
      //this.isInvalidfile = true;
      //this.uploadingImage = true;
      //  this.isShowPanDoc = false;
      arrayControl.controls[side]?.get('offerPicture')?.setErrors(null);
      arrayControl.controls[side]?.get('offerTermsAndCond')?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // console.log('type', file.type);
        // this.offersForm
        //   .get(tab)
        //   ?.get(side)
        //   ?.setValue(reader.result);
      };
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path =
        environment.bucket_parent_folder +
        '/' +
        this.cognitoId +
        '/' +
        tab +
        '/' +
        side +
        '.' +
        fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (
        uploaded_file?.Key != undefined &&
        uploaded_file?.Key != '' &&
        uploaded_file?.Key != null
      ) {
        if (tab == 'offerPicture') {
          // arrayControl.controls[index]?.get('offerPicture')?.setValue(uploaded_file.Location);
          this.documentName[index] = { name: fileFormat, url: uploaded_file.Location };
        } else {
          // arrayControl.controls[index]?.get('offerTermsAndCond')?.setValue(uploaded_file.Location);
          let object = { name: fileFormat, url: uploaded_file.Location };
          this.documentNameTc[index] = object;
          console.log(this.documentNameTc)
        }
      }

    } else {
      // this.isShowPanDoc = true;

      if (tab == 'offerPicture') {
        arrayControl.controls[index]?.get('offerPicture')?.setValue('');
        arrayControl.controls[index]?.get('offerPicture')?.setErrors({ imgIssue: true });
      } else {
        arrayControl.controls[index]?.get('offerTermsAndCond')?.setValue('');
        arrayControl.controls[index]?.get('offerTermsAndCond')?.setErrors({ imgIssue: true });
      }
    }
  }

  resetFile(i: any) {
    this.documentName.splice(i, 1);
    //this.documentNameTc = [];
  }

  /**
   * go back
   */
  back() {
    this.router.navigate(['new-equipment/basic-details/', this.customerId]);
  }

  /**
   *  get id from active route for edit and delete
   * */
  getEditDeleteId() {
    this.route.params.subscribe((params: Params): void => {
      this.customerId = params['id'];
    });
  }

  /**
   * Get offers details for new equipment
   *
   */
  getNewOffersList() {
    //this.spinner.show();
    this.newEquipmentService.getOffersFromMaster(this.customerId).subscribe(
      (res: any) => {
         
        this.offersDetailList = res.results;
        if (this.offersDetailList.length > 0) {
          this.initializRecords(this.offersDetailList);
        }
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /**
   * set basic forms details
   * */
  initializRecords(data?: any) {
    if (this.offersDetailList.length != 0) {
      const formArray: any = new FormArray([]);
      //this.offersArray.removeAt(0)
      console.log("offerlist", this.offersDetailList);
      for (let i = 0; i < this.offersDetailList.length; i++) {
        this.getStates(this.offersDetailList[i].country[0])
        if(this.offersDetailList[i].state[0]){
          this.getCities(this.offersDetailList[i].state[0].id)
        }
        formArray.push(this.fb.group({
          startDate: this.offersDetailList[i].start_date,
          typeOfOffer: this.offersDetailList[i].type,
          endDate: this.offersDetailList[i].end_date,
          freeOfCost: this.offersDetailList[i].free_of_cost,
          description: this.offersDetailList[i].description,
          valueOfOffer: this.offersDetailList[i].amount,
          amount: this.offersDetailList[i].amount,
          rate: this.offersDetailList[i].rate,
          tenure: this.offersDetailList[i].tenure,
          margin: this.offersDetailList[i].margin,
          processingFee: this.offersDetailList[i].processing_fee,
          installment: this.offersDetailList[i].installment,
          Country: this.offersDetailList && this.offersDetailList[i].country[0] ? this.offersDetailList[i].country[0] : "",
          State: this.offersDetailList && this.offersDetailList[i].state[0] ? this.offersDetailList[i].state[0].id : "",
          City: this.offersDetailList && this.offersDetailList[i].city[0] ? this.offersDetailList[i].city[0].id : "",
          // StateId: this.offersDetailList && this.offersDetailList[i].state[0] ? this.offersDetailList[i].state[0].id : "",
          // CityId: this.offersDetailList && this.offersDetailList[i].city[0] ? this.offersDetailList[i].city[0].id : "",
          PinCode: this.offersDetailList && this.offersDetailList[i].pincode[0] ? this.offersDetailList[i].pincode[0].pin_code : "",
          locationId: this.offersDetailList && this.offersDetailList[i].state[0] ? this.offersDetailList[i].state[0].id : "",
          offerTermsAndCond: this.offersDetailList[i].t_and_c_doc,
          new_equipment: this.customerId,
          offerId: this.offersDetailList[i].id
        }));
        if (this.offersDetailList[i].offer_image != null) {
          this.documentName.push({ name: this.offersDetailList[i].offer_image, url: this.offersDetailList[i].offer_image });
        }
        if (this.offersDetailList[i].t_and_c_doc != null) {
          this.documentNameTc.push({ name: this.offersDetailList[i].t_and_c_doc, url: this.offersDetailList[i].t_and_c_doc });
        }
      }
      this.offersForm.setControl('offersArray', formArray);
    }
  }

  createRequestOffersMaster() {
    /**---form control bindings--- */
    this.offersForm.markAllAsTouched();
    if (this.offersForm.valid) {
      let locations: any = [];
      let cash_purchases: any = [];
      let cash_purchase = {};
      let leaseAndFinance = {};
      let leaseAndFinances: any = [];
      var arrayControl = this.offersForm.get('offersArray') as FormArray;
      this.newEquipmentOfferMaster = [];
      this.updateOffermaster = [];
      arrayControl.value.forEach((item, i) => {
        cash_purchase = {
          name: "Offer",
          type: item.typeOfOffer,
          tenure: item.tenure,
          rate: item.rate,
          amount: item.amount,
          margin: item.margin,
          processing_fee: item.processingFee,
          installment: item.installment,
          free_of_cost: item.freeOfCost,
          description: item.description,
          state: item.State ? [item.State] : [],
          city: item.City ? [item.City] : [],
          pincode: item.PinCode ? [item.PinCode] : [],
          t_and_c_doc: this.documentNameTc[i]?.url == undefined ? '' : this.documentNameTc[i].url,
          offer_image: this.documentName[i]?.url == undefined ? '' : this.documentName[i].url,
          start_date: this.datePipe.transform(item.startDate, 'yyyy-MM-dd'),
          end_date: this.datePipe.transform(item.endDate, 'yyyy-MM-dd'),
          new_equipment: this.customerId,
          id: item.offerId,
          country: item.Country ? [item.Country]: [],
          cognito_id:this.cognitoId
        };



        if (item.offerId) {
          this.updateOffermaster.push(cash_purchase)
        } else {
          this.newEquipmentOfferMaster.push(cash_purchase);
        }
      });
      if (this.offersForm.valid) {
        if (this.newEquipmentOfferMaster) {
          this.newEquipmentService
            .postOffersToMaster(this.newEquipmentOfferMaster)
            .subscribe((res) => {
               
              this.offersMasterResponce = res;
              var getResponse = res as ResponseData;
              // this.submit();
               
              this.router.navigate(['new-equipment/visuals', this.customerId]);
              if (getResponse.statusCode == 200 || getResponse.statusCode == 201) {
                if (getResponse.body != null) {
                   
                  this.router.navigate(['new-equipment/visuals', this.customerId]);
                }
              }
              if (getResponse.statusCode == 409) {
                 
              }
            });
        }
        if (this.updateOffermaster) {
          this.updateOffermaster.forEach(element => {
            this.newEquipmentService
              .updateOfferMaster(element, element.id)
              .subscribe((res) => {
                 
                this.router.navigate(['new-equipment/visuals', this.customerId]);
              });
          });
        }
      } else {
        return;
      }
    } else {
      return;
    }
  }

  /**
   * submit data pre final submission
   */
  saveOffersBeforeSubmit() {
    ////this.spinner.show();
    this.offersForm.markAllAsTouched();
    if (this.offersForm.valid) {
      this.createRequestOffersMaster(); //call requested permater modal
    }
  }

  /**
   * submit offers details
   *  */
  submit() {
    var arrayControl = this.offersForm.get('offersArray') as FormArray;
    //this.spinner.show();
    arrayControl.value.some((item, index) => {
      let object = {};
      object = {
        t_and_c_doc: this.documentNameTc[index]?.url == undefined ? '' : this.documentNameTc[index].url,
        offer_image: this.documentName[index]?.url == undefined ? '' : this.documentName[index].url,
        dealer: item.dealer,
        start_date: this.datePipe.transform(item.startDate, 'yyyy-MM-dd'),
        end_date: this.datePipe.transform(item.endDate, 'yyyy-MM-dd'),
        offer_master: this.offersMasterResponce.id,
        new_equipment: this.customerId,
        state: item.locationId,
      };
      this.offersDataFinal.push(object);
    });

    this.newEquipmentService
      .newEquipmentOffersPost(this.offersDataFinal)
      .subscribe((res) => {
         
        this.submitNewEquipments();
        this.router.navigate(['new-equipment/visuals', this.customerId]);
        var getResponse = res as ResponseData;
        if (getResponse.statusCode == 200) {
          if (getResponse.body != null) {
             
          }
        }
        if (getResponse.statusCode == 409) {
           
        }
      });



    //this.router.navigateByUrl('new-equipment/visuals');
    // } else {
    // }
  }

  resetFileTermCondition(i: any) {
    this.documentNameTc.splice(i, 1);
  }

  updateOffer() {
    this.offersForm.markAllAsTouched();
    if (this.offersForm.valid) {
      //this.spinner.show();
      this.offersDetailList.forEach((item, index) => {
        this.object = {};
        this.object = {
          t_and_c_doc: this.documentNameTc[index].url,
          offer_image: this.documentName[index].url,
          dealer: item.dealer,
          start_date: this.datePipe.transform(item.start_date, 'yyyy-MM-dd'),
          end_date: this.datePipe.transform(item.end_date, 'yyyy-MM-dd'),
          offer_master: item.offer_master.id,
          new_equipment: item.new_equipment,
          state: item.state,
        }
      });
      this.offersUpdateData.push(this.object);


      this.newEquipmentService
        .updateNewEquipmentOffers(this.offersDataFinal, this.customerId)
        .subscribe((res) => {
           
          this.submitNewEquipments();
          this.router.navigate(['new-equipment/visuals', this.customerId]);
          var getResponse = res as ResponseData;
          if (getResponse.statusCode == 200) {
            if (getResponse.body != null) {
               
              this.router.navigate(['new-equipment/visuals', this.customerId]);
            }
          }
          if (getResponse.statusCode == 409) {
             
          }
        });

    }

    //this.router.navigateByUrl('new-equipment/visuals');
    // } else {
    // }
  }

  /**
   * submit basic
   * */
  submitNewEquipments() {
    this.adminNewProductUpload = new AdminNewProductUpload();
    this.adminNewProductUpload.seller = this.cognitoId;
    this.adminNewProductUpload.brand = this.basicDetailList.brand.id;
    this.adminNewProductUpload.model = this.basicDetailList.model.id;
    this.adminNewProductUpload.category = this.basicDetailList.category.id;
    this.adminNewProductUpload.info_level = this.infoLevel;
    //this.spinner.show();
    this.newEquipmentService
      .postNewEquipmentDetails(this.adminNewProductUpload, this.customerId)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
        // if (getResponse.statusCode == 200) {
        //   if (getResponse.body != null) {
         
        //   }
        // }
        // if (getResponse.statusCode == 409) {
        //    
        // }
      });
    // } else {
    // }
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  getEndDate(i) {

    let array = this.offersForm?.get('offersArray') as FormArray;

    let date = array.controls[i]?.get('startDate')?.value;
    if (date) {
      date = new Date(date);
      date.setDate(date.getDate() + 1);
      return date;
    }
    else return new Date();
  }

  // onCheckBoxChange(isChecked: boolean) {
  //   if (isChecked) {
  //     this.isPanAvailableChecked = true;
  //     (<FormArray>this.offersForm.get('offersArray')).controls.forEach(control => {
  //       control.get('PinCode')?.disable();
  //       control.get('State')?.disable();
  //       control.get('City')?.disable();
  //       control.get('PinCode')?.setValue('');
  //       control.get('State')?.setValue('');
  //       control.get('City')?.setValue('');
  //       control.get('PinCode')?.clearValidators();
  //       control.get('PinCode')?.updateValueAndValidity();
  //     });

  //   } else {
  //     this.isPanAvailableChecked = false;
  //     (<FormArray>this.offersForm.get('offersArray')).controls.forEach(control => {
  //       control.get('PinCode')?.enable();
  //       control.get('State')?.enable();
  //       control.get('City')?.enable();
  //       control.get('PinCode')?.setValidators(Validators.required);
  //       control.get('PinCode')?.updateValueAndValidity();
  //     });
  //   }
  // }

  async getStates(value: any) {
    if(value != undefined || value != null)
    await this.renderStateData('country__id__in=' + value + '&limit=999')
    let country = this.countryDataSource?.filter(x => x.id == value).map(x => x.name)[0];
  }

  getCountryData(queryParams: string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams = 'limit=999') {
    return new Promise((resolved, reject) => {
    this.getCountryData(queryParams).subscribe((data: any) => {
      this.countryDataSource = data.results;
      resolved(true);
    });
  });
  }

  renderStateData(queryParams: string) {
    return new Promise((resolved, reject) => {
    this.getStateData(queryParams).subscribe((res: any) => {
      this.stateDataSource = res.results;
      resolved(true);
    });
  });
  }

  getStateData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getStateMaster(queryParams)

  }
  async getCities(value: any) {
    await this.renderCityData('state__id__in=' + value + '&limit=999')
  }

  renderCityData(queryParams: string) {
    return new Promise((resolved, reject) => {
    this.getCityData(queryParams).subscribe((res: any) => {
      this.cityDataSource = [];
      this.cityDataSource = res.results;
      resolved(true);
    });
  });
  }

  getCityData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getCityMaster(queryParams)
  }

  skip(){
      this.router.navigate(['new-equipment/visuals', this.customerId]);
  }

  getSellerType(id){
    switch (id) {
      case 1: {
        return 'Customer'
        break;
      }
      case 2: {
        return 'Dealer'
        break;
      }
      case 3: {
        return 'Manufacturer'
        break;
      }
      default: {
        return ''
        break;
      }
  }
}

}
