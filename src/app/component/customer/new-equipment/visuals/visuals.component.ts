import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { sellEquipment, visualData } from 'src/app/models/sellEquipment.model';
import { NewEquipmentService } from './../../../../services/customer-new-equipments.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { ImageCropperModalComponent } from 'src/app/component/shared/image-cropper-modal/image-cropper-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { v4 as uuidv4 } from 'uuid';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { finalize } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-visuals',
  templateUrl: './visuals.component.html',
  styleUrls: ['./visuals.component.css']
})
export class VisualsComponent implements OnInit {
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('videoInput') videoInput!: ElementRef;

  public viewingOptionsForm!: FormGroup;
  public assetId: any;
  public assetDetail: any;
  public tabsList: any;
  public assetVisualData: any;
  public cognitoID!: string;
  public docType: { [key: string]: number } = {
    "IMAGE": 1,
    "VIDEO_LINK": 2,
    "VIDEO_UPLOAD": 3
  }

  public uploadVideoAndImages: Array<object> = [];
  public files: NgxFileDropEntry[] = [];
  public firstImage: boolean = false;

  constructor(private router: Router, public s3: S3UploadDownloadService, private spinner: NgxSpinnerService,
     private notificationservice: NotificationService,private newEquipmentService: NewEquipmentService,
     private fb: FormBuilder, public dialog: MatDialog, public storage: StorageDataService, public route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.cognitoID = this.storage.getLocalStorageData('cognitoId', false);
    this.viewingOptionsForm = this.fb.group({
      imageData: this.fb.array([]),
      videosData: this.fb.array([this.newVideo()]),
    });
    this.route.params.subscribe((data) => {
      this.assetId = data.id;
      if (this.assetId) {
        this.getDetails();
      } else {
        this.back();
      }
    })
  }

  getDetails() {
    forkJoin([this.newEquipmentService.getBasicDetailList(this.assetId), this.newEquipmentService.getvisualTabs(), this.newEquipmentService.getNewVisualsByEquipmentId(this.assetId, { limit: 999 })]).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.assetDetail = res[0];
        this.tabsList = res[1].results;
        this.assetVisualData = res[2].results;
        if (this.tabsList.length) {
          this.tabsList.forEach((tab: any) => {
            this.imageFormGroup.push(this.newImage(tab));
          });
        }
        if (this.assetVisualData.length) {
          this.setVisualData(this.assetVisualData);
        }
      },
      err => {
        console.log(err?.message);
      }
    )
  }


  /**
   * Get video data array
   */
  get imageFormGroup() { return this.viewingOptionsForm.get("imageData") as FormArray; }
  get videosFormGroup() { return this.viewingOptionsForm.get("videosData") as FormArray; }
  get sourceVideoFormGroup() { return this.videosFormGroup.controls; }
  get sourceImgFormGroup() { return this.imageFormGroup.controls; }
  sourceImgFormGroup1(index) { return this.sourceImgFormGroup[index].get('images') as FormArray; }

  /**
   * New video for group
   */
  newImage(tab): FormGroup {
    return this.fb.group({
      id: [tab.id],
      label: [tab.category_name],
      imageUrl: [''],
      images: this.fb.array([]),
    })
  }

  /**
   * New video for group
   */
  imagesGroup(data: any): FormGroup {
    return this.fb.group({
      id: [data.id],
      image_category: [data.image_category],
      is_primary: [data.is_primary],
      url: [data.url],
      new_equipment: [data.new_equipment],
      visual_type: [data.visual_type]
    })
  }

  /**
   * New video for group 
   */
  newVideo(data?: any): FormGroup {
    return this.fb.group({
      // videoLink: [data ? data.videoLink : ''],
      id: [data ? data.id : ''],
      videoFile: [''],
      url: [data ? data.url : ''],
    })
  }

  /**
   * Add new video existing video data
   */
  addNewVideo() {
    this.videosFormGroup.push(this.newVideo());
  }

  /**
   * Remove video existing video data
   */
  removeVideo(index: number) {
    this.videosFormGroup.removeAt(index);
  }

  updateSelected(event, img) {
    let existingPrimaryImage:any;
    this.imageFormGroup.value.forEach(element => {
      existingPrimaryImage = element.images.find(f=> { return f.is_primary == true; } )
    });
    if (event.checked) {
      if(existingPrimaryImage){
        existingPrimaryImage.is_primary = false;
      }
      img.is_primary = true;
    }
    // Object.keys(conrolArr).forEach(key => {
    //   if (key == ImageKey) {
    //     conrolArr[key].is_primary = true;
    //   } else {
    //     conrolArr[key].is_primary = false;
    //   }
    // });
    // this.sourceImgFormGroup1(tabKey).setValue(conrolArr);
  }

  back() {
    this.router.navigate(['new-equipment/offers',this.assetId]);
  }

  preview() {
    let state = 'preview';
    localStorage.setItem('preview', state);
    this.router.navigate(['new-equipment-dashboard/equipment-details', this.assetDetail.id]);
  }

  saveAssetData(data: any) {
    this.newEquipmentService.postvisualsDetail(data).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.setVisualData(res);
        this.assetVisualData = [...res];
        this.notificationservice.success('File uploaded successfully!!');
      },
      (err) => {
        this.notificationservice.error(err);
      }
    );
  }

  async handleUpload(files: any, tab: number) {
    let visualsDataFrom: any = {};
    this.uploadVideoAndImages = [];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        var image_path = environment.bucket_parent_folder + "/marketplace/product/" + this.cognitoID + '/' + this.assetId + '/' + tab + '_' + uuidv4() + '_' + file?.name.trim();
        var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

        if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
          visualsDataFrom = {
            visual_type: 1,
            url: uploaded_file.Location,
            new_equipment: this.assetDetail.id,
            image_category: tab,
            is_primary: false
          }
          if (this.assetVisualData.length) {
            let obj = this.assetVisualData.find(f => { return f.visual_type == 1 });
            if (obj) {
              this.firstImage = true;
            }
          }
          // for (let i = 0; i < this.imageFormGroup.value.length; ++i) {
          //   if (this.imageFormGroup.value[i].images.length) {
          //     this.firstImage = true;
          //     break;
          //   }
          // }
          if (!this.firstImage && i == 0) {
            visualsDataFrom.is_primary = true;
            this.firstImage = true;
          }
          this.uploadVideoAndImages.push(visualsDataFrom);
        }
      } else {
        this.notificationservice.error("Invalid Image.")
      }
    }
    if (this.uploadVideoAndImages.length) {
      this.saveAssetData(this.uploadVideoAndImages);
    }
    this.imageInput.nativeElement.value = '';
  }

  async handleVideoUpload(event: any, count: any) {
    let visualsDataFrom: any = {};
    const file = event.target.files[0];
    if (file.type == 'video/mp4' || file.type == 'video/avi' || file.type == 'video/webm' || file.type == 'video/3gpp' || file.type == 'video/mov' || file.type == 'video/wmv'|| file.type == 'video/mkv' ) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var image_path = environment.bucket_parent_folder + "/marketplace/product/" + this.cognitoID + '/' + this.assetId + '/' + count + '_' + uuidv4() + '_' + file?.name.trim();

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        visualsDataFrom = {
          visual_type: 3,
          url: uploaded_file.Location,
          new_equipment: this.assetDetail.id,
          image_category: 1
        }
        this.saveAssetData([visualsDataFrom]);
      }
    } else {
      this.notificationservice.error("Invalid Video File.")
    }
    this.videoInput.nativeElement.value = '';
  }

  public dropped(files: NgxFileDropEntry[], tab: number) {
    this.files = files;
    let dropFiles: File[] = [];
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          dropFiles.push(file);
          if (dropFiles.length == files.length) {
            this.handleUpload(dropFiles, tab);
          }
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  cropImage(img, tab: number, index: number, imgIndex: number) {
    const dialogRef = this.dialog.open(ImageCropperModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        image: img.img_path,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.data) {
        this.sourceImgFormGroup1(index).removeAt(imgIndex);
        const imageName = 'temp_' + tab + index + '.png';
        const imageFile = new File([result.data], imageName, { type: 'image/png' });
        this.handleUpload([imageFile], tab);
      }
    });
  }

  getVideoName(str) {
    return str.slice(str.lastIndexOf("/") + 3);
  }

  setVisualData(data) {
    let videoObj = data.find(v => { return v.visual_type == 3 });
    let existingVideoObj = this.videosFormGroup.value[this.videosFormGroup.value.length - 1];
    if (videoObj && !existingVideoObj.url) {
      this.videosFormGroup.removeAt(this.videosFormGroup.value.length - 1);
    }
    data.forEach(element => {
      if (element.visual_type == 1) {
        let visualTab: any = this.sourceImgFormGroup.find((fg) => {
          return fg.value.id == element.image_category;
        })
        visualTab.get('images').push(this.imagesGroup(element));
      } else {
        if (element.visual_type == 3) {
          if (element?.url) {
            this.videosFormGroup.push(this.newVideo(element));
          }
        }
      }
    });
  }

  delete(file, index: number, imgIndex?: any) {
    let imgPath = file?.url.slice(file?.url.indexOf(environment.bucket_parent_folder));
    // let visualId = this.assetVisualData.find(f => { return file.url == f.url })?.id;
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.newEquipmentService.deleteVisual(file.id).pipe(finalize(() => {   })).subscribe(
          res => {
            this.notificationservice.success('File Successfully deleted!!')
            if (file.visual_type == 1) {
              this.sourceImgFormGroup1(index).removeAt(imgIndex);
            } else {
              this.videosFormGroup.removeAt(index);
              if (index == 0) {
                this.addNewVideo();
              }
            }
          },
          err => {
            console.log(err);
            this.notificationservice.error(err.message);
          }
        )
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }

  saveData() {
    if (this.assetVisualData?.length == 0) {
      this.notificationservice.error('Please upload at least one asset image.');
      return;
    }
    localStorage.removeItem('preview');
    //this.spinner.show();
    let sellEquipmentRequest = new sellEquipment();
    sellEquipmentRequest.status = 1;
    sellEquipmentRequest.info_level = 3;
    sellEquipmentRequest.brand = this.assetDetail.brand.id;
    sellEquipmentRequest.category = this.assetDetail.category.id;
    sellEquipmentRequest.model = this.assetDetail.model.id;
    this.newEquipmentService.patchupdateNewEquipment(sellEquipmentRequest, this.assetDetail.id).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.notificationservice.success('Asset successfully added!!', true);
        this.router.navigate(['/customer/dashboard/MyAssets']);
      },
      err => {
        console.log(err);
        this.notificationservice.error(err.message);
      }
    );
  }

}
