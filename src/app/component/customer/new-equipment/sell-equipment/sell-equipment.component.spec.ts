import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellEquipmentComponent } from './sell-equipment.component';

describe('SellEquipmentComponent', () => {
  let component: SellEquipmentComponent;
  let fixture: ComponentFixture<SellEquipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellEquipmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SellEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
