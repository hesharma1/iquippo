import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { AdminNewProductUpload } from '../../../../models/common/new-product-upload.model'
import { NgxSpinnerService } from 'ngx-spinner';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { NewEquipmentService } from './../../../../services/customer-new-equipments.service';
import { StorageDataService } from './../../../../services/storage-data.service';
import { CommonService } from './../../../../services/common.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { finalize } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';
import { SigninComponent } from 'src/app/component/signin/signin.component';
@Component({
  selector: 'app-sell-equipment',
  templateUrl: './sell-equipment.component.html',
  styleUrls: ['./sell-equipment.component.css']
})
export class SellEquipmentComponent implements OnInit {
  public sellEquipmentForm: FormGroup;
  subsCategory!: Subscription;
  subsBrand!: Subscription;
  subsModel!: Subscription;
  adminNewProductUpload: any;
  cognitoId: any;
  categoryList = [];
  brandList = [];
  modelList = [];
  SellEqipmentResult: any;
  brandId: any;
  modalId: any;
  categoryData: any;
  brandData: any;
  modalData: any;
  categoryId: any;
  rolesArray;
  role;
  partnerAllDetail: any;
  agreementNotApproved: boolean = false;
  constructor(
    private router: Router,
    private adminMasterService: AdminMasterService,
    private spinner: NgxSpinnerService,
    private storage: StorageDataService,
    private newEquipmentService: NewEquipmentService,
    private commonService: CommonService,
    private sharedservice:SharedService,
    public notify: NotificationService, private appRouteEnum: AppRouteEnum,
    private dialog:MatDialog
  ) {
    this.sellEquipmentForm = new FormGroup({
      category: new FormControl('', Validators.required),
      otherCategory: new FormControl(''),
      brand: new FormControl('', Validators.required),
      otherBrand: new FormControl(''),
      model: new FormControl('', Validators.required),
      otherModel: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    if (this.rolesArray){
      let role;
      this.rolesArray = this.rolesArray.split(',');
      if (this.rolesArray.includes('Dealer')) {
        role = 'Dealer';
      } else if (this.rolesArray.includes('Manufacturer')) {
        role = 'Manufacturer';
      } else if (this.rolesArray.includes('Customer')) {
        role = 'Customer';
      }
      this.role = role;
    }
      
    this.getCategoryMasters();
    this.getPartnerAllInfo();
  }

  createListing() {
    // let userData = localStorage.getItem('userData');
    // if (!userData) {
    //   this.router.navigate(['login']);
    //   return;
    // }
    let sellerType;
    if (!this.cognitoId) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      //dialogRef.afterClosed().subscribe(result => {
      //    console.log(`Dialog result: ${result}`);
      //});
    
  
    dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = localStorage.getItem('cognitoId');
            this.rolesArray = this.storage.getStorageData("rolesArray", false);
          }
        })
      return;
    }
    if (this.rolesArray) {
      if (this.rolesArray.includes('Customer') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer')) {
        let role;
        if (this.rolesArray.includes('Dealer')) {
          role = 'Dealer';
          sellerType = 2;
        } else if (this.rolesArray.includes('Manufacturer')) {
          role = 'Manufacturer';
          sellerType = 3;
        } else if (this.rolesArray.includes('Customer')) {
          role = 'Customer';
          sellerType = 1;
        }
        this.role = role;
        let payload = {
          cognito_id: this.cognitoId,
          role: role
        }
        this.commonService.validateSeller(payload).subscribe((res: any) => {
          console.log(res);
          if (res?.partner_admin) {
            this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              //this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to complete your KYC profile with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { isNew: true } });
              return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return; 
             
            }
            if (!res.bank_details_exist) {

              //this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: 'You need to fill your bank details with iQuippo to sell equipments.' +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { isNew: true } });
              return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return; 
             
            }
            if (!res.allow) {
             // this.notify.warn(res.message, true);
             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate(['/customer/dashboard/partner-dealer/agreement-details'], { queryParams: { isNew: true } });
                return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return; 
             
            }
          }
          else if (res.partner_admin != undefined && !res?.partner_admin) {
            if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
              this.notify.warn('KYC and Address proof of partner admin not available.', true);
              //  this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1 } });
              return;
            }
            if (!res.bank_details_exist) {

              this.notify.warn('bank details of partner admin not available.', true);
              //  this.router.navigate([`./` + this.appRouteEnum.BankingDetails]);
              return;
            }
            if (!res.allow) {
              // this.notify.warn(res.message, true);
              this.notify.warn('Valid Agreement of partner admin does not exist.', true);
              return;
            }
          }
          else if (role == 'Customer') {
            if (res.kyc_verified != 2 || !res.pan_address_proof) {
              if (!res.pan_address_proof || res.kyc_verified == 1) {
                //this.notify.warn(res.message, true);
                const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                  width: '500px',
                  data: {
                    message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                  }
                });
                dialogRef.afterClosed().subscribe(result => {
                  if (result) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, 'isrequired': true } });
                    return;
                  }
                  else {
                    //console.log(result);
                    return;
                  }
                })
                return; 
              } 
              else if (res.kyc_verified == 3) {
                this.notify.warn('As per our records, your KYC profile is rejected. If you have any questions, kindly raise a call back request.', true);
                return;
              }
              
            }
            if (!res.bank_details_exist) {

              //this.notify.warn(res.message, true);
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '500px',
                data: {
                  message: res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.router.navigate([`./` + this.appRouteEnum.BankingDetails], { queryParams: { 'isrequired': true } });
                  return;
                }
                else {
                  //console.log(result);
                  return;
                }
              })
              return; 
              
            }
            if (!res.allow) {
              var constMsg =  '';
             if(res?.agreement_status!=undefined && res?.agreement_status=="agreement_not_approved"){
              constMsg =  res.message;
              this.agreementNotApproved = true; 
             }
             else{
              constMsg =  res.message +'</br>' + 'Click Yes if you want to proceed or No to cancel this process. ';
              this.agreementNotApproved = false; 
            }
             const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
              width: '500px',
              data: {
                message:constMsg,
                agreementNotApproved:this.agreementNotApproved
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.router.navigate(['/' + this.appRouteEnum.CustomerAgreement]);
                return;
              }
              else {
                //console.log(result);
                return;
              }
            })
            return; 
             
            }
          }
          //this.spinner.show();
          //  if(this.sellEquipmentForm.valid) {
          this.adminNewProductUpload = new AdminNewProductUpload();
          this.adminNewProductUpload.category = this.sellEquipmentForm.get('category')?.value;
          this.adminNewProductUpload.brand = this.sellEquipmentForm.get('brand')?.value;
          this.adminNewProductUpload.model = this.sellEquipmentForm.get('model')?.value;
          this.adminNewProductUpload.seller = this.cognitoId;
          this.adminNewProductUpload.category = this.categoryId;
          this.adminNewProductUpload.model = this.modalId;
          this.adminNewProductUpload.brand = this.brandId;
          this.adminNewProductUpload.seller = this.cognitoId;
          this.adminNewProductUpload.seller_type = sellerType;
          if (this.sellEquipmentForm.get('otherCategory')?.value) {
            this.adminNewProductUpload.other_category = this.sellEquipmentForm.get('otherCategory')?.value;
          }
          if (this.sellEquipmentForm.get('otherBrand')?.value) {
            this.adminNewProductUpload.other_brand = this.sellEquipmentForm.get('otherBrand')?.value;

          }
          if (this.sellEquipmentForm.get('otherModel')?.value) {
            this.adminNewProductUpload.other_model = this.sellEquipmentForm.get('otherModel')?.value;

          }
          this.newEquipmentService
            .postBasicDetail(this.adminNewProductUpload)
            .subscribe((res) => {
              this.SellEqipmentResult = res;
               
              // if (getResponse.statusCode == 200) {
              //   if (getResponse.body != null) {
               
              this.router.navigate(['new-equipment/basic-details', this.SellEqipmentResult.id]);
              //   }
              // }
            });
        });
      }
      else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 1){
        this.notify.warn("You cannot perform this action as your registration request is currently under approval process. Please get in touch with us if you need further help.")
      }
      else if((this.rolesArray.includes('PartnerGuest') || this.rolesArray.includes('Customer')) && this.partnerAllDetail?.partner_contact_entity?.status == 2){
        this.notify.warn("Please re-login again to create listing.");
        this.sharedservice.logout();
      }
    }
    //  } else {}
  }
 getPartnerAllInfo(){
   this.sharedservice.getAllPartnerInfo().pipe(finalize(() => {   })).subscribe(res=>{
      console.log(res);
      this.partnerAllDetail = res;
    })
  }
  updateValidations() {
    if(this.sellEquipmentForm){
      if(this.sellEquipmentForm?.get('category')?.value == 'Other'){
        this.sellEquipmentForm.get('otherCategory')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherCategory')?.clearValidators();
        this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherCategory')?.setValue('');
        this.sellEquipmentForm.get('otherBrand')?.setValue('');
        this.sellEquipmentForm.get('otherModel')?.setValue('');
      }
      if(this.sellEquipmentForm?.get('brand')?.value == 'Other'){
        this.sellEquipmentForm.get('otherBrand')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherBrand')?.clearValidators();
        this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherBrand')?.setValue('');
        this.sellEquipmentForm.get('otherModel')?.setValue('');
      }
      if(this.sellEquipmentForm?.get('model')?.value == 'Other'){
        this.sellEquipmentForm.get('otherModel')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
      }else{
        this.sellEquipmentForm.get('otherModel')?.clearValidators();
        this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
        this.sellEquipmentForm.get('otherModel')?.setValue('');
      }
    }
    
    // if (this.sellEquipmentForm) {
    //   if (this.sellEquipmentForm.get('category')?.value == '1') {
    //     this.sellEquipmentForm.get('otherCategory')?.clearValidators();
    //     this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('category')?.value == '2') {
    //     this.sellEquipmentForm.get('otherCategory')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherCategory')?.updateValueAndValidity();
    //   }
    //   if (this.sellEquipmentForm.get('brand')?.value == '1') {
    //     this.sellEquipmentForm.get('otherBrand')?.clearValidators();
    //     this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('brand')?.value == '2') {
    //     this.sellEquipmentForm.get('otherBrand')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherBrand')?.updateValueAndValidity();
    //   }
    //   if (this.sellEquipmentForm.get('model')?.value == '1') {
    //     this.sellEquipmentForm.get('otherModel')?.clearValidators();
    //     this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
    //   } else if (this.sellEquipmentForm.get('model')?.value == '2') {
    //     this.sellEquipmentForm.get('otherModel')?.setValidators(Validators.required);
    //     this.sellEquipmentForm.get('otherModel')?.updateValueAndValidity();
    //   }
    // }
    // return this.sellEquipmentForm;
  }



  filterValuesCategory(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }
  filterValuesBrand(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.brandData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }
  trimSpaces(event){
    if(event.target.value)
event.target.value = event.target.value.trim()
  }
  filterValuesModal(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.modalData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }
  onModalSelect(modal: any) {
    this.modalId = modal;
  }
  getCategoryMasters() {
    let queryparams = "limit=999"
    this.commonService.getCategoryMaster(queryparams).subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      err => {
        console.error(err);
      });
  }

  onCategorySelect(categoryId: number) {
    this.categoryId = categoryId
    let payload = { };
      payload = {
        cognito_id : this.cognitoId,
        type:this.role == 'Dealer'? 'dealer':'manufacturer'
      }
    this.commonService.getBrandMasterByCatId(categoryId,payload).subscribe(
      (res: any) => {
        this.brandList = res.results;
        this.brandData = res.results;
      },
      err => {
        console.error(err);
      });
  }

  onBrandSelect(brandId: number) {
    this.brandId = brandId;
    console.log(this.brandId)
    this.commonService.getModelMasterByBrandId(brandId,this.categoryId).subscribe(
      (res: any) => {
        this.modelList = res.results;
        this.modalData = res.results;
      },
      err => {
        console.error(err);
      });
  }

  multipleUpload() {
    this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false } })
  }
}
