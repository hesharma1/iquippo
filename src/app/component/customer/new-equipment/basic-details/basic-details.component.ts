import { Component, OnInit, ViewChild } from '@angular/core';
import { FieldConfig } from 'src/app/shared/abstractions/field.interface';
import { DynamicFormComponent } from '../../../shared/dynamic-form/dynamic-form.component';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { StorageDataService } from './../../../../services/storage-data.service';
import { AdminNewProductUpload } from '../../../../models/common/new-product-upload.model';
import { NewEquipmentService } from './../../../../services/customer-new-equipments.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { sellEquipment, documentData } from 'src/app/models/sellEquipment.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { Moment } from 'moment';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};
@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class BasicDetailsComponent implements OnInit {
  cognitoId: any;
  public demoForm!: FormGroup;
  public arrayItems:any = [];
  infoLevel: number = 1;
  customerId: any;
  basicDetailList: any;
  adminNewProductUpload: any;
  imageName: any;
  public basicDetailsForm: FormGroup;
  @ViewChild(DynamicFormComponent) form?: DynamicFormComponent;
  basicUSPList: any;
  UspAddArray: any;
  uploadingImage: boolean = false;
  isInvalidfile: boolean = false;
  isTechfieldShow = false;
  maxYear:any;
  yearOfManufacture: any = [];
  brandLocation: any = [];
  documentName: Array<any> = [];
  videoName: Array<any> = [];
  serviceLogName: Array<any> = [];
  docType: { [key: string]: number } = {
    "EQUIPMENT_DETAIL": 1,
    "SERVICE_LOG": 2
  }
  public object:any =  {};

  constructor(
    private fb: FormBuilder,
    public s3: S3UploadDownloadService,
    private storage: StorageDataService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    public newEquipmentService: NewEquipmentService,
    private datePipe: DatePipe,
    private notificationservice: NotificationService
  ) {
    this.basicDetailsForm = new FormGroup({
      yearOfManufacture: new FormControl('', Validators.required),
      isPriceOnRequest: new FormControl('0', Validators.required),
      showroomPrice: new FormControl(''),
      brochure: new FormControl('', Validators.required),
      video: new FormControl(''),
      youtubeLink: new FormControl(''),
      fuelType: new FormControl(''),
      enginePower: new FormControl(''),
      grossWeight: new FormControl(''),
      operatingWeight: new FormControl(''),
      bucketCapcity: new FormControl(''),
      liftingCapcity: new FormControl(''),
      uspAddArray: new FormArray([]),
    });
    this.demoForm = this.fb.group({
      demoArray: this.fb.array([])
    });
  }

  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    this.UspAddArray = <FormArray>(
      this.basicDetailsForm.controls['uspAddArray']
    );
    let currentDate = new Date();
    for(let i=0; i<4; i++){
      this.yearOfManufacture.push((currentDate.getFullYear() - i).toString());
    }
    this.maxYear = new Date().toJSON().split('T')[0];
    this.getEditDeleteId();
    //this.getUSPList();
    this.getNewEquipmentList();
  }

  /** 
   * get id from active route for edit and delete
   **/
  getEditDeleteId() {
    this.route.params.subscribe((params: Params): void => {
      this.customerId = params['id'];
    });
  }

  get demoArray() {
    return this.demoForm.get("demoArray") as FormArray;
  }

  /**add new usp */
  newUspData() {
    const TaxArray = this.basicDetailsForm?.get(
      'uspAddArray'
    ) as FormArray;
    this.UspAddArray.push(this.createUspFormGroup(''));
  }

  removeUspFormGroup(id: any) {
    var i = '' + id + '';
    this.UspAddArray.removeAt(i);
  }

  /**
   * Get basic details for new equipment
   */
   getNewEquipmentList() {
    //this.spinner.show();
    this.newEquipmentService.getBasicDetailList(this.customerId).subscribe(
      (res: any) => {
         
        this.basicDetailList = res;
        this.basicUSPList = res.usp;
        this.basicDetailList.name =  (this.basicDetailList?.category?.display_name == 'Other'?this.basicDetailList?.other_category:this.basicDetailList?.category?.display_name) +' ' +(this.basicDetailList?.brand?.display_name == 'Other'?this.basicDetailList?.other_brand:this.basicDetailList?.brand?.display_name) +' ' + (this.basicDetailList?.model?.name == 'Other'?this.basicDetailList?.other_model:this.basicDetailList?.model?.name);
          this.setBasicFormDetails(this.basicDetailList);
      },
      (err) => {
        console.error(err);
      }
    );
  }


  createUspFormGroup(uspdata?:any): FormGroup  {
    if(uspdata==undefined || uspdata==null || uspdata == '' ){
      return this.fb.group({
        id: new FormControl(''),
        Usp: new FormControl(''),
      }); 
    } else {
      return this.fb.group({
        id: [uspdata.id],
        Usp: [uspdata.val],
      })
      }
  }

  onSubmit() {
    console.log(this.basicDetailsForm.value);
    this.basicDetailsForm.markAllAsTouched();

    this.submit();
  }

  /**
   * Get Usp details for new equipment
   */
  postUSPList() {
    this.bindUspDetails();
    console.log(this.UspAddArray.value)
    if (this.UspAddArray.value.length > 0) {
      this.newEquipmentService.postUSPListNew(this.brandLocation).subscribe(
        (res: any) => {
          this.router.navigate(['new-equipment/offers', this.customerId]);
        },
        (err) => {
        }
      );
    } else {
      this.router.navigate(['new-equipment/offers', this.customerId]);
    }
  }

  bindUspDetails() {
    let object: any = {};
    this.UspAddArray = <FormArray>(this.basicDetailsForm.controls['uspAddArray']);
    // this.UspAddArray.value.forEach(element => {
    //   object = {
    //     usp:element.Usp,
    //     new_equipment: this.customerId,
    //   }
    this.brandLocation = this.UspAddArray;
    // });
  }

  

  /**
   * submit basic 
   * */
  submit() {
    console.log(this.basicDetailsForm)
    if (this.basicDetailsForm.valid) {
      this.adminNewProductUpload = new AdminNewProductUpload();
      //this.spinner.show();
      this.adminNewProductUpload.seller = this.cognitoId;
      this.adminNewProductUpload.brand = this.basicDetailList?.brand?.id;
      this.adminNewProductUpload.model = this.basicDetailList?.model?.id;
      this.adminNewProductUpload.category = this.basicDetailList?.category?.id;
      this.adminNewProductUpload.fuel_type = this.basicDetailsForm.get('fuelType')?.value;
      this.adminNewProductUpload.mfg_year = this.basicDetailsForm.get('yearOfManufacture')?.value;
      this.adminNewProductUpload.document = this.documentName[0].url;
      this.adminNewProductUpload.video_link = this.basicDetailsForm.get('video')?.value;
      this.adminNewProductUpload.youtube_link = this.basicDetailsForm.get('youtubeLink')?.value;
      this.adminNewProductUpload.is_price_on_request = this.basicDetailsForm.get('isPriceOnRequest')?.value == '1'? true:false;
      if(this.basicDetailsForm.get('enginePower')?.value) {
        this.adminNewProductUpload.engine_power = this.basicDetailsForm.get('enginePower')?.value;
      }
      if(this.basicDetailsForm.get('grossWeight')?.value) {
        this.adminNewProductUpload.gross_Weight = this.basicDetailsForm.get('grossWeight')?.value;
      }
      if(this.basicDetailsForm.get('operatingWeight')?.value) {
        this.adminNewProductUpload.operating_weight = this.basicDetailsForm.get('operatingWeight')?.value;
      }
      if(this.basicDetailsForm.get('bucketCapcity')?.value) {
        this.adminNewProductUpload.bucket_capacity = this.basicDetailsForm.get('bucketCapcity')?.value;
      }
      if(this.basicDetailsForm.get('liftingCapcity')?.value) {
        this.adminNewProductUpload.lifting_capacity = this.basicDetailsForm.get('liftingCapcity')?.value;
      }
      if(this.basicDetailsForm.get('showroomPrice')?.value){
      this.adminNewProductUpload.selling_price = this.basicDetailsForm.get('showroomPrice')?.value;
      }
      this.adminNewProductUpload.tech_fields = [];
      // this.adminNewProductUpload.status =0;
      //if(this.isTechfieldShow) {
        let dynamicTech_Fields = this.demoForm.get('demoArray') as FormArray;
        for( let i=0;i<dynamicTech_Fields.value.length;i ++) {
            this.object = {};
            this.object = {
              field_name: this.arrayItems[i].title,
              field_type: 1,
              field_value: dynamicTech_Fields.value[i],
              visible_on_front: true,
              priority: 1
            }
            this.adminNewProductUpload.tech_fields.push(this.object);
        }

      //} else {
        // if (this.basicDetailsForm.get('enginePower')?.value) {
        //   let object = {};
        //   object = {
        //     field_name: 'enginePower',
        //     field_type: 1,
        //     field_value: this.basicDetailsForm.get('enginePower')?.value,
        //     visible_on_front: true,
        //     priority: 1
        //   }
        //   this.adminNewProductUpload.tech_fields.push(object);
        // }
        // if (this.basicDetailsForm.get('grossWeight')?.value) {
        //   let object = {};
        //   object = {
        //     field_name: 'grossWeight',
        //     field_type: 1,
        //     field_value: this.basicDetailsForm.get('grossWeight')?.value,
        //     visible_on_front: true,
        //     priority: 2
        //   }
        //   this.adminNewProductUpload.tech_fields.push(object);
        // }
        // if (this.basicDetailsForm.get('operatingWeight')?.value) {
        //   let object = {};
        //   object = {
        //     field_name: 'operatingWeight',
        //     field_type: 1,
        //     field_value: this.basicDetailsForm.get('operatingWeight')?.value,
        //     visible_on_front: true,
        //     priority: 3
        //   }
        //   this.adminNewProductUpload.tech_fields.push(object);
        // }
        // if (this.basicDetailsForm.get('bucketCapcity')?.value) {
        //   let object = {};
        //   object = {
        //     field_name: 'bucketCapcity',
        //     field_type: 1,
        //     field_value: this.basicDetailsForm.get('bucketCapcity')?.value,
        //     visible_on_front: true,
        //     priority: 4
        //   }
        //   this.adminNewProductUpload.tech_fields.push(object);
        // }
        // if (this.basicDetailsForm.get('liftingCapcity')?.value) {
        //   let object = {};
        //   object = {
        //     field_name: 'liftingCapcity',
        //     field_type: 1,
        //     field_value: this.basicDetailsForm.get('liftingCapcity')?.value,
        //     visible_on_front: true,
        //     priority: 5
        //   }
        //   this.adminNewProductUpload.tech_fields.push(object);
        // }
      //}
      // this.bindUspDetails();
      this.UspAddArray = <FormArray>(this.basicDetailsForm.controls['uspAddArray']);
      this.adminNewProductUpload.usp = [];
      this.UspAddArray.value.forEach(element => {
        this.adminNewProductUpload.usp.push(element.Usp);
      });
      //this.adminNewProductUpload.tech_fields = [];
      this.adminNewProductUpload.product_name = this.basicDetailList?.category?.display_name + ' ' + this.basicDetailList?.brand?.display_name + ' ' + this.basicDetailList?.model?.name;

      // this.adminNewProductUpload.usp = this.basicDetailsForm.controls['uspAddArray'].value;
      this.adminNewProductUpload.info_level = this.infoLevel;
      //this.spinner.show();
      this.newEquipmentService
        .postNewEquipmentDetails(this.adminNewProductUpload, this.customerId)
        .subscribe((res) => {
           
           
          //  let usp = this.basicDetailsForm.get('uspAddArray') as FormArray;
          // let filtered = usp.value.filter(item=>item.Usp == '' && usp.value.length == 1);
          // if(filtered.length == 1) {
          this.router.navigate(['new-equipment/offers', this.customerId]);
          // } else {

          // }
        });
    }
  }

  /**set basic forms details */
  setBasicFormDetails(formdata: any) {
    this.basicDetailsForm.get('yearOfManufacture')?.setValue(formdata?.mfg_year ? formdata?.mfg_year.toString() : '2021');
    this.basicDetailsForm.get('showroomPrice')?.setValue(formdata?.selling_price);
    this.basicDetailsForm.get('youtubeLink')?.setValue(formdata?.youtube_link);
    this.basicDetailsForm.get('fuelType')?.setValue(formdata?.fuel_type);
    this.basicDetailsForm.get('isPriceOnRequest')?.setValue(formdata?.is_price_on_request? '1': '0');
    this.basicDetailsForm.get('enginePower')?.setValue(formdata?.engine_power);
    this.basicDetailsForm.get('grossWeight')?.setValue(formdata?.gross_Weight);
    this.basicDetailsForm.get('bucketCapcity')?.setValue(formdata?.bucket_capacity);
    this.basicDetailsForm.get('liftingCapcity')?.setValue(formdata?.lifting_capacity);
    this.basicDetailsForm.get('operatingWeight')?.setValue(formdata?.operating_weight);
    this.updateValidation();
    if(formdata.document){
      this.documentName.push({ name: formdata.document, url: formdata.document });
      this.basicDetailsForm.get('brochure')?.clearValidators();
      this.basicDetailsForm.get('brochure')?.updateValueAndValidity();
    }
    if (formdata?.document)
      this.imageName = formdata?.document;
    if (formdata?.video_link)
      this.videoName.push({ name: formdata?.video_link, url: formdata?.video_link });
      this.basicDetailsForm.get('video')?.clearValidators();
      this.basicDetailsForm.get('video')?.updateValueAndValidity();
     
    if(formdata?.tech_fields?.length > 0) {
      //this.isTechfieldShow = true;
      formdata?.tech_fields.map((field:any) => {
        this.arrayItems.push({ title: field.field_name });
        this.demoArray.push(this.fb.control(field.field_value));
      });
    }
    if (formdata?.usp?.length > 0) {
      formdata?.usp.forEach((cust) => {
        this.UspAddArray.push(this.createUspFormGroup(cust));
        //console.log(this.customersControls);
      });
    } else {
      this.UspAddArray.push(this.createUspFormGroup(''));
    }
    // else {
      //this.isTechfieldShow = false;

   // }
  }

  /**back on page */
  back() {
    this.router.navigateByUrl('/customer/dashboard/MyAssets');
  }

  resetFile() {
    this.documentName = [];
  }

  resetvideoFile() {
    this.videoName = [];
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
    console.log(uploaded_file);
    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      console.log(this.documentName)
      if (tab == 'brochure') {
        this.imageName = uploaded_file.Location;
        this.basicDetailsForm.get('brochure')?.setValue(uploaded_file.Location);
      }


      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }

  async handleVideoUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if (file.type == 'video/mp4' || file.type == 'video/avi' || file.type == 'video/webm' || file.type == 'video/3gpp' || file.type == 'video/mov' || file.type == 'video/wmv') {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      this.videoName.push({ name: fileFormat, url: uploaded_file.Location });
      if (tab == 'video') {
        this.basicDetailsForm.get('video')?.setValue(uploaded_file.Location);
      }
    } else {
      this.notificationservice.error("Invalid Video File.")
    }
  }

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  updateValidation(){
    if(this.basicDetailsForm?.get('isPriceOnRequest')?.value == '0'){
      this.basicDetailsForm?.get('showroomPrice')?.clearValidators();
      this.basicDetailsForm?.get('showroomPrice')?.updateValueAndValidity();
    }else{
      this.basicDetailsForm?.get('showroomPrice')?.setValue('');
      this.basicDetailsForm?.get('showroomPrice')?.clearValidators();
      this.basicDetailsForm?.get('showroomPrice')?.updateValueAndValidity();
    }
  }
  // chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
  //   const ctrlValue = this.basicDetailsForm.get('yearOfManufacture');
  //   ctrlValue?.setValue(_moment(this.minDate));
  //   ctrlValue?.value.year(normalizedYear.year());
  //   ctrlValue?.value.month(normalizedYear.month());
  //   ctrlValue?.setValue(ctrlValue.value);
  //   this.basicDetailsForm?.get('yearOfManufacture')?.setValue(ctrlValue?.value);
  //   //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
  //   datepicker.close();
  // }
}
