import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NewEquipmentService } from './../../../../services/customer-new-equipments.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationPopupComponent } from './../../../admin/confirmation-popup/confirmation-popup.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from './../../../../services/storage-data.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';


@Component({
  selector: 'app-products-listing',
  templateUrl: './products-listing.component.html',
  styleUrls: ['./products-listing.component.css'],
})
export class ProductsListingComponent implements OnInit {
  productList: any;
  page: number = 1;
  pageCount: number = 1;
  pageSize: number = 999;
  displayedColumns: string[] = [
    // 'Select',
    'ID',
    'Image',
    'product_name',
    'Trading',
    'Type',
    'Categories',
    'Brand',
    'Model',
    'Price',
    // 'Assets',
    'Status',
    // 'Seller & Type',
    'Mfg. Year',
    'Location',
    'Actions',
  ];
  productDataSource = new MatTableDataSource<any>([]);
  cognitoId;
  roleArray;
  @ViewChild("equipmentPaginator") set categoryPaginator(pager: MatPaginator) {
    if (pager) this.productDataSource.paginator = pager;
  }

  constructor(
    private newEquipmentService: NewEquipmentService, private dialog: MatDialog,
    private router: Router, private spinner: NgxSpinnerService, private sharedService: SharedService,
    private storage: StorageDataService, private apiRouteService: ApiRouteService) { }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.roleArray = this.storage.getStorageData('rolesArray', false);
    this.getNewEquipmentList('');
  }

  nextPage() {
    console.log('next Page');
  }
  /**
   * update selected row
   */
  updateSelectedRow() {

  }

  /**
   * Get ProductList for new equipment
   *
   */
  getNewEquipmentList(data: any) {
    //this.spinner.show();
    let role;
    this.roleArray = this.roleArray.split(',');
    if (this.roleArray.includes('Customer')) {
      role = this.apiRouteService.roles.customer;
    }
    let queryParam = `page=${this.page}&limit=${this.pageSize}&cognito_id=${this.cognitoId}&role=${role}`;
    this.newEquipmentService.getProductList(queryParam).subscribe(
      async (res: any) => {
         
        this.productList = res.results;
        this.productDataSource = new MatTableDataSource<any>(this.productList)
        for (let image of this.productList) {
          if (image.primary_image !== "" && image.primary_image !== null) {
            image.primary_image = await this.sharedService.viewImage(image.primary_image);
          }
        }
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /**
   * click to edit
   */
  clickToEdit(Id: any) {
    this.router.navigate(['new-equipment/basic-details', Id]);
  }
  /**
   * click to delete
   */
  clickToDelete() {
    this.openDialog();
  }

  /**
   * open dailog
   */
  openDialog() {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      width: '40%',
      panelClass: 'custom-modalbox'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  /**get row status chnage */
  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    }else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    }else if (status == '5') {
      return "Sold"
    }
    return "";
  }

}
