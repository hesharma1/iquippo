import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AuctionService } from 'src/app/services/auction.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { CommonService } from 'src/app/services/common.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SigninComponent } from '../../signin/signin.component';
import { BidActionComponent } from '../dashboard/my-trade/seller/bid-action/bid-action.component';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-auction-fulfilment-for-user',
  templateUrl: './auction-fulfilment-for-user.component.html',
  styleUrls: ['./auction-fulfilment-for-user.component.css']
})

export class AuctionFulfilmentForUserComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public listingData = [];
  public displayedColumns: string[] = [
    "id",
    "lot_bid__lot__auction",
    "Asset_Id",
    "lot_bid__lot__lot_number",
    "lot_bid__lot__description",
    "lot_bid__amount",
    "lot_bid__lot__auction__buyers_premium",
    "lot_bid__status",
    "lot_bid__deal_status",
    "lot_bid__lot__auction__payment_date",
    "created_at",
    "lot_bid__user__relationship_manager__user__first_name",
    "Full_Payment_Details",
    "Invoice_in_Favour_of",
    "delivery_order_url",
    "invoice_url",
    "action"];

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchText!: string;
  public paymentStatus: any;
  public paymentForType: any;
  public cognitoId!: string;
  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute, private router: Router, private storage: StorageDataService, public notify: NotificationService, private dialog: MatDialog, private appRouteEnum: AppRouteEnum, public auctionService: AuctionService, public sharedService: SharedService, private commonService: CommonService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getfulfilmentPayments();

  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
 @HostListener('window:scroll', ['$event'])
 recursiveNewsApiHit = (event) => {
   if(window.innerWidth < 768){
     if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
       if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.page++;
         this.getfulfilmentPayments();
        }
       }
     }
   }
 }

  getfulfilmentPayments() {
    let userDetails = this.storage.getLocalStorageData('userData', true);

    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      //cognito_id: this.userId,
      role: this.userRole,
      lot_bid__user__cognito_id__in: this.userId
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.paymentStatus) {
      payload['payment_status__in'] = this.paymentStatus;
    }
    if (this.paymentForType) {
      payload['type__in'] = this.paymentForType;
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
          if(window.innerWidth > 768){
            this.dataSource.data = res.results;
            }else{
              this.scrolled = false;
              if(this.page == 1){
                this.dataSource.data = [];
                this.listingData = [];
                this.listingData = res.results;
                this.dataSource.data = this.listingData;
              }else{
                this.listingData = this.listingData.concat(res.results);
                this.dataSource.data = this.listingData;
              }
            }        
          }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getfulfilmentPayments();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getfulfilmentPayments();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.page = 1;
      this.pageSize = 10;
      this.getfulfilmentPayments();
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                this.notify.warn('Please submit KYC and Address proof', true);
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.width = '900px';
        dialogConfig.data = {
          flag: true
        }
        const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = this.storage.getStorageData('cognitoId', false);
          }
        })
      }
    });
  }

  ProceedFullPayment(data) {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          let paymentDetails = {
            auction_id: data?.lot_bid?.lot?.auction.id,
            asset_type: 'Used',
            case: 'Auction',
            lot_bid_id: data?.lot_bid?.id,
            payment_type: 'Full',
            fulfillment_id: data?.id
          }
          this.storage.setSessionStorageData('paymentSession', true, false);
          this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
          window.open("/customer/payment", '_self');
        }
      });
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = this.storage.getStorageData('cognitoId', false);
        }
      })
    }
  }

  setInvoicePerson(element: any) {
    let nameShow;
    if (element?.invoice_in_favour_of) {
      nameShow = element?.invoice_in_favour_of?.first_name + " " + element?.invoice_in_favour_of?.last_name;
      return nameShow;
    }
  }

  bidAction(type: string, element: any) {
    if (type == 'feedback' && element?.feedback) {
      this.notify.success("We have already received your valuable feedback. Thank You!");
    }
    const dialogRef = this.dialog.open(BidActionComponent, {
      width: '600px',
      data: {
        type: type,
        mainData: element,
        isFromFulfilment: true
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getfulfilmentPayments();
    });
  }

  export() {
    let payload = {
      page: 1,
      limit: -1,
      ordering: this.ordering,
      role: this.userRole,
      lot_bid__user__cognito_id__in: this.userId
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.paymentStatus) {
      payload['payment_status__in'] = this.paymentStatus;
    }
    if (this.paymentForType) {
      payload['type__in'] = this.paymentForType;
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          const excelData = res.results.map((r: any) => {
            return {
              "ID": r?.id,
              "Auction ID": r?.lot_bid?.lot?.auction?.auction_engine_auction_id,
              "Lot Number": r?.lot_bid?.lot?.lot_number,
              "Lot Description": r?.lot_bid?.lot?.description,
              "Asset Id": r?.asset_ids.join(", "),
              "Buyer Name": r?.lot_bid?.user?.first_name,
              "Buyer Mobile No ": r?.lot_bid?.user?.mobile_number,
              "Email": r?.lot_bid?.user?.email,
              "Bid Amount": r?.lot_bid?.amount,
              "Buyers Premium": r?.actual_buyers_premium,
              "Bid Status": r?.bid_status_display_name,
              "Deal Status": r?.deal_status_display_name,
              "Full Payment Due Date": r?.payment_due_date ? (this.datePipe.transform(r?.payment_due_date, 'yyyy-MM-dd')) :
                r?.lot_bid?.lot?.auction?.payment_date
                  ? r?.lot_bid?.lot?.auction?.payment_date : '',
              "Request Date": r?.created_at ? (this.datePipe.transform(r?.created_at, 'yyyy-MM-dd')) : '',
              "RM Name": r?.lot_bid?.user_relationship_manager_full_name,
              "Invoice_in_Favour_of": this.setInvoicePerson(r),
              "Full Payment Details": r?.fulfilment_payments.length && !r?.is_fully_paid ? 'Partial Paid' : r?.is_fully_paid ? 'Paid' : '',
              "Sales Invoice": r?.invoice_url,
              "Delivery Order": r?.delivery_order_url

            }
          });
          ExportExcelUtil.exportArrayToExcel(excelData, "Fulfilment");
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );

  }

}