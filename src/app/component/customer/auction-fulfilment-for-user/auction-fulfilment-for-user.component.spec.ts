import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionFulfilmentForUserComponent } from './auction-fulfilment-for-user.component';

describe('AuctionFulfilmentForUserComponent', () => {
  let component: AuctionFulfilmentForUserComponent;
  let fixture: ComponentFixture<AuctionFulfilmentForUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionFulfilmentForUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionFulfilmentForUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
