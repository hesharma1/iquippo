import { Component, Inject } from "@angular/core"
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { CommonService } from "src/app/services/common.service";

@Component({
  styleUrls: ["make-enquiry-dialog.component.css"],
  templateUrl: "make-enquiry-dialog.component.html"
})
export class MakeEnquiryDialogComponent {
  enquiryForm: FormGroup;
  numbers: number[] = [];
  categoryMasterList: any;
  offerData: any;
  firstAsset: any = '';

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<MakeEnquiryDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private commonService: CommonService, public spinner: NgxSpinnerService) {
    this.enquiryForm = this.fb.group({
      // category: ['', Validators.required],
      equipment_req_in: ['3', Validators.required],
      //number_of_equipment_avail: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      //number_of_equipment_required: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      remarks: ['', Validators.required],
    })
    this.numbers = new Array(10).fill(0).map((x, i) => i + 1);
  }

  ngOnInit() {
    this.offerData = this.data.offerData;
    this.getCategoryMaster();
  }

  backClick() {
    this.dialogRef.close();
  }

  getCategoryMaster() {
    this.commonService.getCategoryMaster().pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.categoryMasterList = res.results;
      },
      err => {
        console.log(err);
      }
    );
  }

  updateValidation(val) {
    this.firstAsset = val;
    if (val == 'true') {
      this.enquiryForm.controls.category.setValidators([]);
      this.enquiryForm.controls.category.updateValueAndValidity();
      this.enquiryForm.controls.number_of_equipment_avail.setValidators([]);
      this.enquiryForm.controls.number_of_equipment_avail.updateValueAndValidity();
    } else {
      this.enquiryForm.controls.category.setValidators(Validators.required);
      this.enquiryForm.controls.category.updateValueAndValidity();
      this.enquiryForm.controls.number_of_equipment_avail.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
      this.enquiryForm.controls.number_of_equipment_avail.updateValueAndValidity();
    }
  }

  submit() {
    let payload = { ...this.enquiryForm.value };
    if (this.offerData) {
      payload['offer'] = this.offerData.id;
      payload['is_offer_enquiry'] = true;
    }
    this.dialogRef.close(payload);
  }

  getOfferType(val){
    if (val == 1) {
      return 'Finance';
    } else if (val == 2) {
      return 'Lease';
    } else {
      return 'Cash';
    }
  }
}
