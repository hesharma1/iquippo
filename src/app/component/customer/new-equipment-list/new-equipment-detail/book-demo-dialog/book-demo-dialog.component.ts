import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { NewEquipmentService } from "src/app/services/customer-new-equipments.service";
import { DatePipe } from "@angular/common";
import { AgreementServiceService } from "src/app/services/agreement-service.service";

@Component({
  styleUrls: ["./book-demo-dialog.component.css"],
  templateUrl: "./book-demo-dialog.component.html"
})
export class BookDemoDialogComponent implements OnInit {

  public demoForm: FormGroup;
  public selectedLocation: any;
  public minDate: Date = new Date();
  public locationOptions: Array<any> = [];
  loggedUserData: any;
  stateList: any = [];
  isBeforeHalf: any = false;
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<BookDemoDialogComponent>, private agreementServiceService: AgreementServiceService,
    private newEquipmentService: NewEquipmentService, private spinner: NgxSpinnerService, public datePipe: DatePipe) {
    this.demoForm = fb.group({
      //state: [''],
      //city: [''],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      in_second_half: ['', Validators.required]
      //start_time: ['', Validators.required],
      //end_time: ['', Validators.required],
      //pin_code: ["", Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(6)])]
    })
  }

  ngOnInit() {

  }


  getLocation(pin) {
    if (pin && pin.length > 4) {
      this.newEquipmentService.getPincodeMaster({ pin_code__contains: pin, limit: 999 }).pipe(finalize(() => { })).subscribe(
        (data: any) => {
          if (data.results.length) {
            this.locationOptions = data.results;
          } else {
            this.locationOptions = [];
            this.demoForm.get('pin_code')?.setErrors({ wrong: true });
          }
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.demoForm.patchValue({
        country: '',
        state: '',
        city: ''
      });
    }
  }

  pinCodeChange(option) {
    if (option) {
      let Obj = this.locationOptions.find((l) => { return l.pin_code == option })
      this.demoForm.patchValue({
        state: Obj?.city?.state?.name,
        city: Obj?.city?.name
      });
    }
  }

  backClick() {
    this.dialogRef.close();
  }

  submit() {
    let payload = Object.assign({}, this.demoForm.value);
    payload.start_date = this.datePipe.transform(payload.start_date, 'yyyy-MM-dd');
    payload.end_date = this.datePipe.transform(payload.end_date, 'yyyy-MM-dd');
    delete payload.state;
    delete payload.city;
    this.dialogRef.close(payload);
  }
}
