import { Component, OnInit, HostListener, ViewChild, ElementRef, Inject, Renderer2 } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { BookDemoDialogComponent } from './book-demo-dialog/book-demo-dialog.component';
import { MakeEnquiryDialogComponent } from './make-enquiry-dialog/make-enquiry-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { Gallery, ImageItem } from "ng-gallery";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { CommonService } from 'src/app/services/common.service';
import { SigninComponent } from 'src/app/component/signin/signin.component';
import { SharedService } from 'src/app/services/shared-service.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-new-equipment-detail',
  templateUrl: './new-equipment-detail.component.html',
  styleUrls: ['./new-equipment-detail.component.css']
})
export class NewEquipmentDetailComponent implements OnInit {
  @ViewChild("emiCalc") emiCalc?: ElementRef;
  @ViewChild("offerSec") offerSec?: ElementRef;
  public emiForm: FormGroup;
  public equipmentId!: string;
  public equipmentDetails: any;
  public visualImageData: any = [];
  public visualVideoData: any;
  public allOffers = {
    cash: [],
    finance: [],
    lease: []
  };
  public offerAvailable: boolean = false;
  public isPreview: boolean = false;
  public offerArray: Array<any> = [];
  public offerForSelectedLocation: Array<any> = [];
  public statesRes: any;
  public selectedStateId!: number;
  public showData = false;
  public userDetails: any;
  public cognitoId!: string;
  public recentlyViewedList;
  public demoBooked: boolean = false;
  public showEmiCalc: boolean = false;
  public showOfferList: boolean = false;
  public panIndiaCheck: boolean = false;
  public isSameUser: boolean = false;
  public showCalculatedEmi: boolean = true;
  public result: any = {
    emi: 0,
    total: 0,
    interest: 0
  }

  recentViewSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    responsive: {

      300: {
        items: 1,
        nav: false
      },
      400: {
        items: 1,
        nav: false
      },
      500: {
        items: 1,
        nav: false
      },
      600: {
        items: 2,
        nav: false
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };

  constructor(private fb: FormBuilder, public route: ActivatedRoute, public apiService: NewEquipmentService, private dialog: MatDialog, private notify: NotificationService, private sharedService: SharedService, private storage: StorageDataService, private router: Router, private spinner: NgxSpinnerService, public gallery: Gallery, private commonService: CommonService, @Inject(DOCUMENT) private document: Document) {
    if (768 > window.innerWidth) {
      this.showCalculatedEmi = false;
    }
    this.emiForm = fb.group({
      loan_amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      rate_of_interest: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]\d*(\.\d+)?$/)])],
      payment_prefrence: ['', Validators.compose([Validators.required])],
      tenure: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
    });
  }

  ngOnInit(): void {
    let aboutPreview;
    if (localStorage.getItem('preview')) {
      aboutPreview = localStorage.getItem('preview');
    } else {
      aboutPreview = localStorage.getItem('newPreview');
    }
    this.userDetails = this.storage.getLocalStorageData('userData', true);
    this.cognitoId = this.storage.getLocalStorageData('cognitoId', false);
    if (aboutPreview == 'preview') {
      this.isPreview = true;
    }
    if (aboutPreview == 'newPreview') {
      this.isPreview = true;
    }
    this.route.params.subscribe((data) => {
      this.equipmentId = data.id;
      this.getEquipmentDetailsById(this.equipmentId);
      this.getOfferDetailsByEquipmentId();
    })
    // this.getAllOfferStates();
    if (this.cognitoId)
      this.getRecentlyViewed();
  }

  getRecentlyViewed() {
    this.apiService.getRecentlyViewed(this.cognitoId).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.recentlyViewedList = res.results;
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    )
  }

  makeEnquiry(offerData?: any) {
    if (this.userDetails && this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        const makeEnquiryDialogRef = this.dialog.open(MakeEnquiryDialogComponent, {
          width: '700px',
          disableClose: true,
          data: {
            offerData: offerData,
          }
        });

        makeEnquiryDialogRef.afterClosed().subscribe(val => {
          if (val) {
            let payload = {
              "user": this.cognitoId,
              "new_equipment": +this.equipmentId
            }
            payload = { ...payload, ...val };
            this.apiService.postEnquiry(payload).pipe(finalize(() => { })).subscribe(
              (res: any) => {
                this.notify.success("Enquiry submitted successfully with reference number " + res?.id);
              }, err => {
                if (err?.message == "non_field_errors : Enquiry already raised for this equipment.") {
                  this.notify.error("You have already submitted an enquiry on this asset!!", true);
                } else {
                  this.notify.error(err?.message);
                }
              }
            );
          }
        })
      } else {
        this.notify.error('Please login as Customer to proceed further with raise an Enquiry!!');
      }
    } else {
      this.openSignInModal();
    }
  }

  bookDemo() {
    if (this.userDetails && this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        if (!this.equipmentDetails?.requested_for_demo && !this.demoBooked) {
          const dialogRef = this.dialog.open(BookDemoDialogComponent, {
            width: '800px',
            disableClose: true
          });

          dialogRef.afterClosed().subscribe(val => {
            if (val) {
              let payload: any = {
                "customer_name": this.userDetails?.attributes?.given_name,
                "mobile_number": this.userDetails?.attributes?.phone_number,
                "email": this.userDetails?.attributes?.email,
                "user": this.cognitoId,
                "new_equipment": +this.equipmentId,
                "pin_code": this.userDetails?.attributes["custom:pincode"]
              }
              payload = { ...payload, ...val };
              this.apiService.postDemo(payload).pipe(finalize(() => { })).subscribe(
                (res: any) => {
                  this.demoBooked = true;
                  this.notify.success("Demo request raised successfully with reference number " + res?.id)
                }, err => {
                  this.notify.error(err?.message)
                }
              );
            }
          })
        } else {
          this.notify.error('You have already booked a demo for this asset!!');
        }
      } else {
        this.notify.error('Please login as Customer to proceed further with book demo!!');
      }
    } else {
      this.openSignInModal();
    }
  }

  getEquipmentDetailsById(id: string) {
    this.apiService.getBasicDetailListByCognito(id, this.cognitoId).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.equipmentDetails = res;
        if (this.equipmentDetails?.seller?.cognito_id == this.cognitoId) {
          this.isSameUser = true;
        }
        this.getVisualsByEquipmentId(id);
        this.showData = true;
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    );
  }

  getVisualsByEquipmentId(id: string) {
    this.apiService.getVisualsByEquipmentId(id, { limit: 999 }).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        let tempVisuals = res.results.filter(f => { return (f.visual_type == 1) && (f.is_primary == true) });
        res.results.forEach(f => {
          if ((f.visual_type == 1) && (f.is_primary == false)) {
            tempVisuals.push(f);
          }
        });
        if (tempVisuals.length) {
          this.visualImageData = tempVisuals.map(element =>
            new ImageItem({ src: element?.url, thumb: element?.url })
          )
        }
        if (this.equipmentDetails && this.equipmentDetails?.primary_image) {
          let Obj = this.visualImageData.find(item => { return item.data?.src == this.equipmentDetails?.primary_image })
          if (!Obj) {
            this.visualImageData.push(new ImageItem({ src: this.equipmentDetails?.primary_image, thumb: this.equipmentDetails?.primary_image }))
          }
        }
        this.visualVideoData = res.results.filter(f => { return f.visual_type == 3 });
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    );
  }

  getAllOfferStates() {
    let payload = {
      new_equipment_id: this.equipmentId
    }
    this.apiService.getAllOfferStates(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.statesRes = res;
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    );
  }

  // stateChange() {
  //   if (this.panIndiaCheck || this.selectedStateId) {
  //     this.getOfferDetailsByEquipmentId();
  //   } else {
  //     this.notify.error('Please Select state to View offers!!');
  //   }
  // }

  getOfferDetailsByEquipmentId() {
    let payload = {
      new_equipment__id__in: this.equipmentId,
    }
    // if (this.panIndiaCheck) {
    //   payload['is_pan_india'] = true;
    // } else {
    //   payload['id'] = this.selectedStateId;
    // }
    this.apiService.getOffersByEquipmentId(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        // this.showOfferList = true;
        if (res.results && res.results.length) {
          this.offerAvailable = true;
          this.allOffers.finance = res.results.filter(item => { return item.type == 1 });
          this.allOffers.lease = res.results.filter(item => { return item.type == 2 });
          this.allOffers.cash = res.results.filter(item => { return item.type == 3 });
        }
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    );
  }

  getEquipmentFuelType(val) {
    switch (val) {
      case 1: { return 'Diesel' }
      case 2: { return 'Petrol' }
      case 3: { return 'CNG' }
      default: { return 'Diesel' }
    }
  }

  backToPrevious() {
    if (localStorage.getItem('newPreview')) {
      localStorage.removeItem('newPreview');
      this.router.navigate(['/admin-dashboard/products/new-products-upload'], { queryParams: { assetId: this.equipmentId, toEdit: true } });
    } else if (localStorage.getItem('isCustomer')) {
      localStorage.removeItem('isCustomer');
      localStorage.removeItem('preview');
      this.router.navigate(['/customer/dashboard/MyAssets']);
    } else {
      localStorage.removeItem('preview');
      this.router.navigate(['/new-equipment/visuals', this.equipmentId]);
    }

  }

  getTechTitle(str, unit) {
    if (str == 'enginePower') {
      return str + ' cc ';
    } else if (str == 'operatingWeight') {
      return str + ' kg ';
    }
    else if (str == 'bucketCapcity') {
      return str + ' kg ';
    } else if (str == 'grossWeight') {
      return str + ' kg ';
    } else if (str == 'liftingCapcity') {
      return str + ' kg ';
    } else {
      let Uom = unit == undefined || unit == null ? '' : unit
      return str + ' ' + Uom;
    }
  }

  addToWishlist(val) {
    if (this.cognitoId) {
      let request = {
        new_equipment: this.equipmentId,
        used_equipment: null,
        type: val,
        user: this.cognitoId
      }
      this.commonService.addToWatchlist(request).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          this.getEquipmentDetailsById(this.equipmentId);
          if (val == 2) {
            this.notify.success('Asset added to watch list!!');
          } else {
            this.notify.success('Thanks for showing interest in the Asset!!');
          }
        },
        err => {
          console.log(err);
          this.notify.error(err?.message);
        }
      );
    }
  }

  removeFromWatchlist(id) {
    let queryParam =
      'asset_type=' + 'new' +
      '&asset_id=' + id +
      '&cognito_id=' + this.cognitoId;
    this.commonService.removeFromWatchlist(queryParam).subscribe((res: any) => {
      this.getEquipmentDetailsById(this.equipmentId);
    });
  }

  checkValue() {
    if (this.emiForm.value.payment_prefrence && this.emiForm.value.tenure) {
      if (+(this.emiForm.value.payment_prefrence) > +(this.emiForm.value.tenure)) {
        this.notify.error('Loan tenure cannot be less than Payment prefrence Option!!');
        this.emiForm.get('tenure')?.setValue('');
      }
    }
  }

  calculateEmi() {
    let payload = Object.assign({}, this.emiForm.value);

    let monthlyInterestRatio = (payload?.rate_of_interest / 100) / 12;
    let top = Math.pow((1 + monthlyInterestRatio), payload?.tenure);
    let bottom = top - 1;
    let sp = top / bottom;
    let emi = +(((payload?.loan_amount * monthlyInterestRatio) * sp).toFixed(2));
    let total = +(payload?.tenure * emi).toFixed(2);
    this.result = {
      emi: (emi * (+payload?.payment_prefrence)).toFixed(2),
      total: total,
      interest: +(total - payload?.loan_amount).toFixed(2)
    }
    this.showCalculatedEmi = true;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (767 > event.target.innerWidth) {
      this.showCalculatedEmi = false;
    } else {
      this.showCalculatedEmi = true;
    }
  }

  getActiveId() {
    if (this.allOffers?.cash?.length) {
      return 'cash';
    } else if (this.allOffers?.finance?.length) {
      return 'finance'
    } else if (this.allOffers?.lease?.length) {
      return 'lease'
    } else {
      return '';
    }
  }

  proceedToFinance() {
    if (this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      let userPayload = `userId=${this.cognitoId}&deviceKey=${this.userDetails?.signInUserSession?.accessToken?.payload['device_key']}&refToken=${this.userDetails?.signInUserSession?.refreshToken?.token}`
      if (rolesArray.includes('Customer')) {
        this.sharedService.redirecteToMP1AfterLogin();
      } else {
        window.location.href = "https://uat.iqsl.co.in/wps/portal/finance/login?" + userPayload
      }
    } else {
      window.location.href = "https://uat.iqsl.co.in/"
    }
  }

  openSignInModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        this.userDetails = this.storage.getLocalStorageData('userData', true);
        this.cognitoId = this.storage.getLocalStorageData('cognitoId', false);
      }
    })
  }

  scrollTo(type, el: HTMLElement) {
    let topGap: any = document.getElementById('header')?.offsetHeight;
    if (type == 'emiCalc') {
      this.showEmiCalc = !this.showEmiCalc;
      this.animate(document.scrollingElement || document.documentElement, "scrollTop", "", 0, (this.emiCalc?.nativeElement?.offsetTop - topGap), 100, true);
    }
    else {
      this.animate(document.scrollingElement || document.documentElement, "scrollTop", "", 0, (this.offerSec?.nativeElement?.offsetTop - topGap), 100, true);
    }
    // el.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
  }

  animate(elem, style, unit, from, to, time, prop) {
    if (!elem) {
      return;
    }
    var start = new Date().getTime(),
      timer = setInterval(function () {
        var step = Math.min(1, (new Date().getTime() - start) / time);
        if (prop) {
          elem[style] = (from + step * (to - from)) + unit;
        } else {
          elem.style[style] = (from + step * (to - from)) + unit;
        }
        if (step === 1) {
          clearInterval(timer);
        }
      }, 25);
    if (prop) {
      elem[style] = from + unit;
    } else {
      elem.style[style] = from + unit;
    }
  }

  bindBucketCapacity(product) {
    let data = product;
    var final;
    if (data != null) {
      final = data;
    }
    else {
      final = 'N/A'
    }
    return final
  }


}