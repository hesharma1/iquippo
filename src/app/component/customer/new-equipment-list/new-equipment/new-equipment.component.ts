import { Component, OnInit } from '@angular/core';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MakeACallComponent } from 'src/app/component/make-a-call/make-a-call.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
declare var $: any;

@Component({
  selector: 'app-new-equipment',
  templateUrl: './new-equipment.component.html',
  styleUrls: ['./new-equipment.component.css']
})
export class NewEquipmentComponent implements OnInit {
  options3;
  cognitoId;

  Images = ['../../../assets/images/logo.png', '../../../assets/images/default-thumbnail.jpg', '../../../assets/images/heart-wishlist-active.png'];

  SlideOptions = {
    items: 5, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    swipeable: true,
    responsive: {
      300: {
        items: 2,
        nav: true
      },
      400: {
        items: 2,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 5,
        nav: true,
        loop: false
      }
    }
  };


  FeaturedSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    swipeable: true,
    dragBeforeAnimFinish: false,

    responsive: {

      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };
  loading: boolean = true;

  //public sliderOptions = { dots: true, navigation: true, nav: true, navText: ['<', '>'], autoplay: true, loop:true, items: 1, autoHeight: true }; 
  constructor(private equipmentService: NewEquipmentService, private sharedService: SharedService, private spinner: NgxSpinnerService,
    private router: Router, private commonService: CommonService, private storage: StorageDataService,
    private dialog: MatDialog) {
    this.options3 = {
      animation: {
        animationClass: 'transition', // done
        animationTime: 500,
      },
      swipe: {
        swipeable: true, // done
        swipeVelocity: .004, // done - check amount
      },
      drag: {
        draggable: true, // done
        dragMany: true, // todo
      },
      autoplay: {
        enabled: false,
        direction: 'right',
        delay: 5000,
        stopOnHover: true,
        speed: 6000,
      },
      arrows: true,
      infinite: false,
      breakpoints: [
        {
          width: 400,
          number: 1,
        },
        {
          width: 768,
          number: 2,
        },
        {
          width: 991,
          number: 3,
        },
        {
          width: 9999,
          number: 4,
        },
      ],
    }
  }
  bannerList: any = [];
  public innerWidth: any
  categoryList;
  brandList;
  url;
  topBanner;
  leftBanner;
  rightBanner;
  bottomLeftBanner;
  bottomRightBanner;
  newProductList;
  offeringsList;
  options2 = {
    animation: {
      animationClass: 'transition', // done
      animationTime: 500,
    },
    swipe: {
      swipeable: true, // done
      swipeVelocity: .004, // done - check amount
    },
    drag: {
      draggable: true, // done
      dragMany: true, // todo
    },
    autoplay: {
      enabled: false,
      direction: 'right',
      delay: 5000,
      stopOnHover: true,
      speed: 6000,
    },
    arrows: true,
    infinite: false,
    breakpoints: [
      {
        width: 400,
        number: 1,
      },
      {
        width: 768,
        number: 2,
      },
      {
        width: 991,
        number: 3,
      },
      {
        width: 9999,
        number: 5,
      },
    ],
  }
  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getBannerImages();
  }

  getBannerImages() {
    let queryParam = `1`;
    if (this.cognitoId) {
      queryParam = queryParam + '?cognito_id=' + this.cognitoId;
    }
    this.equipmentService.getBannersData(queryParam).subscribe(async res => {
      let response: any = res;
      this.bannerList = response.banner;
      this.categoryList = response.category;
      this.brandList = response.brand;
      for (let banner of this.bannerList) {
        if (banner.position == 'T') {
          this.topBanner = banner;
        } else if (banner.position == 'L') {
          this.leftBanner = banner;
        } else if (banner.position == 'R') {
          this.rightBanner = banner;
        } else if (banner.position == 'BL') {
          this.bottomLeftBanner = banner;
        } else if (banner.position == 'BR') {
          this.bottomRightBanner = banner;
        }
      }
      this.newProductList = response.featured;
      this.offeringsList = response.offerings;
      // for(let category of this.categoryList){
      //   if(category.image_url){
      //     category.image_url = await this.sharedService.viewImage(category.image_url);
      //   }  
      // }
      // for(let brand of this.brandList){
      //      brand.brand_logo_image = await this.sharedService.viewImage(brand.brand_logo_image);
      //  }
    },
      (error) => {
        this.loading = false;
      }
    );
  }

  viewAllCategories() {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`]);
  }

  viewAllBrands() {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`]);
  }
  viewProductListing() {
    let string = '';
     string = 'certificate=Featured'  ;
      this.router.navigate(['/new-equipment-dashboard/equipment-list'], { queryParams: { filterBy: string } });
  }

  redirectToScreen(link,banner) {
    if(link){
      if(link == 'raise-callback'){
        this.opencallpopup(this.getPostion(banner.position));
      }else{
        if (link.includes('/product/used-equipment/') || link.includes('/product/new-equipment/')) {
          if (link.includes('/product/used-equipment/')) {
            link = link.replace("/product/used-equipment/", "");
            this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
          } else {
            link = link.replace("/product/new-equipment/", "");
            this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
          }
        } else {
          window.location.href = link;
        }
      }
    } 

  }

  redirectIfNoButton(label, link,banner) {
    if (!label) {
      if (link) {
        if(link == 'raise-callback'){
          this.opencallpopup(this.getPostion(banner.position));
        }else{
          this.redirectToScreen(link,banner);
        }
       
      }
    }
  }
  getPostion(pos){
    switch (pos){
      case "T" :{return 'New Equipment -Jumbotron Banner'};
      case "L" :{return 'New Equipment -Marketing Banner Left'};
      case "R" :{return 'New Equipment -Marketing Banner Right'};
      case "BL" :{return 'New Equipment -Marketing Banner Bottom Left'};
      case "BR" :{return 'New Equipment -Marketing Banner Bottom Right'};
      default:{return ''};
    }

  }

  redirectToEquipmentDetail(type, id) {
    if (type == 'New') {
      this.router.navigate(['/new-equipment-dashboard/equipment-details/' + id]);
    } else {
      this.router.navigate(['/used-equipment-dashboard/equipment-details/' + id]);
    }
  }

  addToWishlist(id) {
    if (this.cognitoId) {
      let request = {
        new_equipment: id,
        used_equipment: null,
        type: 2,
        user: this.cognitoId
      }
      this.commonService.addToWatchlist(request).subscribe((res: any) => {
        this.getBannerImages();
      });
    }

  }

  removeFromWatchlist(id) {
    let queryParam =
      'asset_type=' + 'new' +
      '&asset_id=' + id +
      '&cognito_id=' + this.cognitoId;
    this.commonService.removeFromWatchlist(queryParam).subscribe((res: any) => {
      this.getBannerImages();
    });
  }

  redirectByBrand(id) {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: 'brand=' + id } });
  }

  opencallpopup(text) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = text;
    const dialogRef = this.dialog.open(MakeACallComponent, dialogConfig);
  }

  redirectByCategory(id) {
    this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { filterBy: 'category=' + id } });
  }
  getTechTitle(str, unit) {
    if (str == 'enginePower') {
      return str + ' cc';
    } else if (str == 'operatingWeight') {
      return str + ' kg';
    }
    else if (str == 'bucketCapcity') {
      return str + ' kg';
    } else if (str == 'grossWeight') {
      return str + ' kg';
    } else if (str == 'liftingCapcity') {
      return str + ' kg';
    } else {
      let Uom = unit == undefined || unit == null ? '' : unit
      return str + Uom;
    }
  }

  onLoad() {
    this.loading = false;
  }
}
