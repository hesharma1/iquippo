import { Component, OnInit } from '@angular/core';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MakeACallComponent } from 'src/app/component/make-a-call/make-a-call.component';

@Component({
  selector: 'app-old-equipment',
  templateUrl: './old-equipment.component.html',
  styleUrls: ['./old-equipment.component.css']
})
export class OldEquipmentComponent implements OnInit {
  loading: boolean = true;

  constructor(private equipmentService: NewEquipmentService, private sharedService: SharedService, private spinner: NgxSpinnerService,
    private router: Router,private dialog: MatDialog) { }
  bannerList: any = [];
  topBanner: any;
  ngOnInit(): void {
    this.getBannerImages();
  }

  getBannerImages() {
    let queryParam = `2`;
    this.equipmentService.getBannersData(queryParam).subscribe(async res => {
      let response: any = res;
      this.bannerList = response.banner;
      this.sortBannersByPriority();
    },
      (error) => {
        this.loading = false;
      }
    );
  }

  onLoad() {
    this.loading = false;
  }

  sortBannersByPriority() {
    this.bannerList.sort((a, b) => (a.priority > b.priority) ? 1 : -1);
    this.topBanner = this.bannerList[0];
    this.bannerList.shift();
  }

  redirectToScreen(link,banner) {
    if(link){
      if(link == 'raise-callback'){
        this.opencallpopup(this.getPostion(banner.banner_title,banner.priority));
      }else{
        if (link.includes('/product/used-equipment/') || link.includes('/product/new-equipment/')) {
      if (link.includes('/product/used-equipment/')) {
        link = link.replace("/product/used-equipment/", "");
        this.router.navigate([`/used-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
      } else {
        link = link.replace("/product/new-equipment/", "");
        this.router.navigate([`/new-equipment-dashboard/equipment-list`], { queryParams: { searchby: link } });
      }
    } else {
      window.location.href = link;
    }
  }
}

  }

  redirectIfNoButton(label, link,banner) {
    if (!label) {
      if (link) {
        if(link == 'raise-callback'){
          this.opencallpopup(this.getPostion(banner.banner_title,banner.priority));
        }else{
        this.redirectToScreen(link,banner);
        }
      }
    }
  }

  getPostion(title,priority){
    return "Used Equipment - Marketing banner " +title+ " -"+ priority;
  }

  opencallpopup(text) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = text;
    const dialogRef = this.dialog.open(MakeACallComponent, dialogConfig);
  }

}
