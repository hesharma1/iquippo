import { CdkVirtualScrollViewport, ScrollDispatcher } from '@angular/cdk/scrolling';
import { Component, OnInit, ChangeDetectorRef, ViewChild, HostListener } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ThumbnailsPosition } from 'ng-gallery';
import { elementAt } from 'rxjs/operators';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';


@Component({
  selector: 'app-used-equipment-listing',
  templateUrl: './used-equipment-listing.component.html',
  styleUrls: ['./used-equipment-listing.component.css']
})
export class UsedEquipmentListingComponent implements OnInit {

  @ViewChild(CdkVirtualScrollViewport)
  virtualScroll!: CdkVirtualScrollViewport;
  productApi = false;
  productList: Array<any> = [];
  BeforeFilterProductList: Array<any> = [];
  certificateList: Array<any> = [];
  pageNumber: number = 1;
  sortInOrder : any = '0';
  sortedBy? : any;
  form!: FormGroup;
  brands: Array<any> = [];
  types: Array<any> = [];
  limitedBrand: Array<any> = [];
  limitedTypes: Array<any> = [];
  AllBrand: Array<any> = [];
  categories: Array<any> = [];
  AllCategory: Array<any> = [];
  limitedCategory: Array<any> = [];
  models: Array<any> = [];
  limitedModel: Array<any> = [];
  AllModel: Array<any> = [];
  locations: Array<any> = [];
  limitedLocation: Array<any> = [];
  AllLocation: Array<any> = [];
  startIndex: number = 0;
  endIndex: number = 5;
  showMoreBrand: boolean = true;
  showMoreCategory: boolean = true;
  showMoreModel: boolean = true;
  keyword = '';
  count =0;
  searchBrandFilter: Array<any> = [];
  searchCategoryFilter: Array<any> = [];
  searchModelFilter: Array<any> = [];
  searchYearFilter: Array<any> = [];
  searchPriceFilter: Array<any> = [];
  searchLocationFilter: Array<any> = [];
  compareList: Array<any> = [];
  years: Array<any> = [];
  categoryName
  brandName
  modelName
  locationName
  // typeOfAsset: Array<any> = [{
  //   isSelected : false,
  //   text : 'Insta Sale',
  //   type : 'Asset',
  //   count : 0
  // },{
  //   isSelected : false,
  //   text : 'Price On Request',
  //   type : 'Asset',
  //   count : 0
  // }]
  typeOfAsset: Array<any> = []
  mfgYearFilterList: Array<any> = [{
    isSelected: false,
    text: "0 - 1",
    type : "mfgYear"
  },
  {
    isSelected: false,
    text: "1 - 3",
    type : "mfgYear"
  }, {
    isSelected: false,
    text: "3 - 5",
    type : "mfgYear"
  },{
    isSelected: false,
    text: "5 - 10",
    type : "mfgYear"
  },{
    isSelected: false,
    text: "10 - Above",
    type : "mfgYear"
  }];
  siebarToggle: boolean = false;
  serverSideCategoryFilter : any;
  serverSideBrandFilter : any;
  serverSideModelFilter : any;
  serverSideLocationFilter : any;
  serverSideCertificateFilter : any;
  serverSideYearFilter : any;
  serverSidePriceFilterGrt : any;
  serverSidePriceFilterLow : any;
  serverSidePriceFilterSingle : any;
  serverSideTypeFilter : any;
  // mfgYearFilterList: Array<any> = [{
  //   isSelected: false,
  //   text: "2018"
  // }, {
  //   isSelected: false,
  //   text: "2019"
  // }, {
  //   isSelected: false,
  //   text: "2020"
  // }, {
  //   isSelected: false,
  //   text: "2021"
  // }];
  AllMFGYear: Array<any> = [];
  priceFilterList: Array<any> = [
  {
    isSelected: false,
    text: "Below - 100000",
    type : "price"
  },
  {
    isSelected: false,
    text: "Price on Request",
    type : "price"
  }, {
    isSelected: false,
    text: "100000 - 250000",
    type : "price"
  },{
    isSelected: false,
    text: "250000 - 500000",
    type : "price"
  },{
    isSelected: false,
    text: "500000 - Above",
    type : "price"
  }];
  AllPriceFilter : Array<any> = [];
  totalProduct : number = 0;
  stopCallingAPI : boolean = false;
  isApprovedStatus : number = 2; //2 is for approved
  pageLoad : number = 0;
  searchBy  : string = '';
  filterBy? : string;
  categoryByUrl? : any;
  brandByUrl? : any;
  modelByUrl? : any;
  certificateByUrl? : any;
  locationByUrl? : any;
  isApiHitSuccessful? : boolean = false;
  public searchText!: string;
  recentlyViewedList: any;
  cognitoId;
  options2;
  filterSelectClearDispName : string = "Select All";
  isCategoryMore? : boolean = true;
  isBrandMore? : boolean = true;
  isModelMore? : boolean = true;
  isYearMore? : boolean = true;
  isLocationMore? : boolean = true;
  cloneObj: any;
  breakedData: any;
  cloneBrandObj: any;
  breakedBrandObj: any;
  cloneModelObj: any;
  breakedModelObj: any;
  cloneLocationObj: any;
  breakedLocationObj: any;
  cloneYearObj: any;
  breakedYearObj: any;
  cloneCertificateObj: any;
  breakedCertificateObj: any;
  isCertificateMore: boolean = true;
  recentViewSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    responsive: {
       
      300: {
        items: 1,
        nav: false
      },
      400: {
        items: 1,
        nav: false
      },
      500: {
        items: 1,
        nav: false
      },
      600: {
        items: 2,
        nav: false
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };
  constructor(public productService : ProductlistService
    , private fb : FormBuilder
    , public adminMasterService : AdminMasterService
    , private scrollDispatcher: ScrollDispatcher
    , private cd: ChangeDetectorRef
    , private sharedService:SharedService
    , private activatedRoute: ActivatedRoute,
    public notify: NotificationService,
    public apiService: UsedEquipmentService,
    public storage: StorageDataService,
    private commonService: CommonService,
    public spinner: NgxSpinnerService,
    private router:Router) {
      this.options2 = {
        animation: {
          animationClass: 'transition', // done
          animationTime: 500,
        },
        swipe: {
          swipeable: true, // done
          swipeVelocity: .004, // done - check amount
        },
        drag: {
          draggable: true, // done
          dragMany: true, // todo
        },
        autoplay: {
          enabled: true,
          direction: 'right',
          delay: 5000,
          stopOnHover: true,
          speed: 6000,
        },
        arrows: true,
        infinite: false,
        breakpoints: [
          {
            width: 400,
            number: 1,
          },
          {
            width: 768,
            number: 2,
          },
          {
            width: 991,
            number: 3,
          },
          {
            width: 9999,
            number: 5,
          },
        ],
      }
     }

  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if ((this.totalProduct > this.productList.length) && (this.productList.length > 0) && (this.isApiHitSuccessful)){
      const triggerAt: number = 900; 
      /* perform an event when the user has scrolled over the point of 128px from the bottom */
      if (document.body.scrollHeight - Math.round(window.innerHeight + window.scrollY) <= triggerAt) {
        this.isApiHitSuccessful = false;
        this.pageNumber++;
        this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
      }
    }
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['searchby'] != '') {
        this.searchBy = params['searchby'];
        this.keyword = params['searchby'];
      }
      if(params['search'] != undefined)
      {
        this.searchText = params['search'];
        this.keyword = params['search'];
      }
      if(params['filterBy'] != '')
      {
        this.filterBy = params['filterBy'];
        if(this.filterBy?.includes("&")){
          var arr : any = [];
          arr = this.filterBy.split("&");
          arr.forEach(element => {
            let str = element.toString();
            if(str.includes("=")){
              let subArr : any = [];
              subArr = str.split("=");
              this.setUrlFilters(subArr);
            }
          });
        }
        else{
          if(this.filterBy?.includes("=")){
            let subStringArr : any = [];
            subStringArr = this.filterBy.split("=");
            this.setUrlFilters(subStringArr);
          }
        }
      }
      this.getfilter('less');
    
    });
    
    // this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
    this.AllMFGYear = JSON.parse(JSON.stringify(this.mfgYearFilterList));
    if (this.mfgYearFilterList.length > this.endIndex) {
      let object = JSON.parse(JSON.stringify(this.mfgYearFilterList));
      this.mfgYearFilterList = object.slice(this.startIndex, this.endIndex);
    }

    this.AllPriceFilter = JSON.parse(JSON.stringify(this.priceFilterList));
    if (this.priceFilterList.length > this.endIndex) {
      let object = JSON.parse(JSON.stringify(this.priceFilterList));
      this.priceFilterList = object.slice(this.startIndex, this.endIndex);
    }
    if(this.cognitoId)
    this.getRecentlyViewed();
  }

  getRecentlyViewed(){
    this.apiService.getRecentlyViewed(this.cognitoId).subscribe((res:any)=>{
      this.recentlyViewedList = res.results;
    })
  }

  setUrlFilters(object : any){
    let arr : any = [];
    arr =object;
    switch(arr[0]){
      case "category" : 
      this.categoryByUrl = arr[1];
      this.serverSideCategoryFilter = this.categoryByUrl;
      break;
      case "brand" :
      this.brandByUrl = arr[1];
      this.serverSideBrandFilter = this.brandByUrl;
      break;
      case "model" :
      this.modelByUrl = arr[1];
      this.serverSideModelFilter = this.modelByUrl;
      break;
      case "location" : 
      this.locationByUrl = arr[1];
      this.serverSideLocationFilter = this.locationByUrl;
      break;
      case "certificate" :
      this.certificateByUrl = arr[1];
      this.serverSideCertificateFilter = this.certificateByUrl;
      break;
    }
  }

  get frm() { return this.form.controls };

  getfilter(showingFilter: any) {
    this.getUsedEquipmentApiCall(this.searchBy, this.pageNumber, this.isApprovedStatus)
  }
  removeFromCompare(product){
    let index = this.compareList.indexOf(product);
    if(index >=0){
      this.compareList.splice(index, 1);
    }
  }

  checkifAddedtoCompare(product){
    let index = this.compareList.indexOf(product);
    if(index >=0){
      return false;
    }
    return true;
  }
  addToCompare(product){
    if(this.compareList.length != 0){
      if(product.category.id != this.compareList[0].category.id){
        //product.checkCompare = false;
        this.notify.error('Please select products of same category to compare');
        return;
      }
    }
      let size;
      if(window.innerWidth > 768){
        size =2
      }else{
        size=1;
      }
      if (this.compareList.length > size) {
        size = size+1;
        product.checkCompare = false;
        this.cd.detectChanges();
        this.notify.error(
          'Cannot compare more than '+ size +' products'
        );
        return;
      }
      else {
        product.checkCompare = true;
        this.compareList.push(product);
        this.cd.detectChanges();
      }
  }
  setFilters(){
    if(this.serverSideTypeFilter){
      let typeFilters = this.serverSideTypeFilter.split(",");
      this.limitedTypes.forEach((element => {
              for(let type of typeFilters){
                if(type == element.id){
                  element.isSelected =  true;
                  break;
                }
              }
            })
          );
    }
    if(this.serverSideCertificateFilter){
      let certFilters = this.serverSideCertificateFilter.split(",");
      this.typeOfAsset.forEach((element => {
        for(let type of certFilters){
          if(type == element.text){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSideYearFilter){
     // let yearFilters = this.serverSideYearFilter.split(",");
      this.mfgYearFilterList.forEach((element => {
        for(let type of this.serverSideYearFilter){
          type = type.replace(",", " - ")
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSidePriceFilterSingle){
      //let priceFilters = this.serverSidePriceFilterSingle.split(",");
      this.priceFilterList.forEach((element => {
        for(let type of this.serverSidePriceFilterSingle){
          type = type.replace(",", " - ")
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSideCategoryFilter){
      this.limitedCategory.forEach((element => {
        let cat = this.serverSideCategoryFilter.split(",");
        for(let type of cat){
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSideBrandFilter){
      this.limitedBrand.forEach((element => {
       let brand = this.serverSideBrandFilter.split(",");
        for(let type of brand){
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSideModelFilter){
      this.limitedModel.forEach((element => {
        let model = this.serverSideModelFilter.split(",");
        for(let type of model){
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
    if(this.serverSideLocationFilter){
      this.limitedLocation.forEach((element => {
        let loc = this.serverSideLocationFilter.split(",");
        for(let type of loc){
          if(type == element.id){
            element.isSelected =  true;
            break;
          }
        }
      })
    );
    }
  }
  createCustomFilters(object: any, type: string) {
    var array: Array<any> = [];
    for (var i = 0; i < object.length; i++) {
      switch (type) {
        case 'brand': 
          var brandObject: any;
          
          brandObject = {
            id: object[i].brand_id,
            isSelected: false,
            text: object[i].brand__display_name,
            type:'brand',
            count: object[i].id__count
          }
          array.push(brandObject);
          break;
        case 'category':
          var categoryObject
          
          categoryObject = {
            id: object[i].category_id,
            isSelected: false,
            text: object[i].category__display_name,
            type: 'category',
            count: object[i].id__count
          }
          array.push(categoryObject);
          break;
        case 'model':
          let modelObject = {
            id: object[i].model_id,
            isSelected: false,
            text: object[i].model__name,
            type: 'model',
            count: object[i].id__count
          }
          array.push(modelObject);
          break;
        case 'certificate' :
          let certificateObject = {
            id : object[i].certificate,
            isSelected: false,
            text: object[i].certificate__name,
            type: 'certificate',
            count : object[i].id__count 
          }
          array.push(certificateObject);
          break;
        case 'year':
        let yearObject = {
          id: object[i].mfg_year,
          isSelected: false,
          text: object[i].mfg_year,
          type: 'mfgYear'
        }
          array.push(yearObject);
          break;
        case 'location':
          if (object[i].pin_code__city_id != null && object[i].pin_code__city__name != null) {
            let locationObject = {
              id: object[i].pin_code__city_id,
              isSelected: false,
              text: object[i].pin_code__city__name,
              type: 'location',
              count: object[i].id__count
            }
            array.push(locationObject);
          }
          break;
        case 'type':
        var typeObject
          
        typeObject = {
            id: object[i].id,
            isSelected: false,
            text: object[i].name,
            type: 'type',
            count: object[i].id__count
          }
          array.push(typeObject);
          break;
      }
    }
    return array;
  }
  // setDefaultFilter(){
  //   /* for category by default selected as true */
  //   let categoryObject : Array<any> = [];
  //   this.serverSideCategoryFilter = '';
  //   this.limitedCategory.forEach(element => {
  //       element.isSelected = true;
  //         categoryObject.push(element.id);
  //     });
  //   this.serverSideCategoryFilter = categoryObject.join();
  //   /* for brand by default selected as true */
  //   let brandObject : Array<any> = [];
  //   this.serverSideBrandFilter = '';
  //     this.limitedBrand.forEach(element => {
  //       element.isSelected = true;
  //       brandObject.push(element.id);
  //     });
  //   this.serverSideBrandFilter = brandObject.join();
  // }

  applyFilter(dataObject : any){
    this.searchText = '';
    this.productList = [];
    this.pageNumber = 1;
    dataObject.isSelected = dataObject.isSelected == false ? true : false;
    switch(dataObject.type){
      case 'type' :
        let typeObject : Array<any> = [];
        this.serverSideTypeFilter = '';
        this.limitedTypes.forEach(element => {
          if(element.isSelected){
            typeObject.push(element.id);
          }
        });
        this.serverSideTypeFilter = typeObject.join();
        break;
      case 'category' :
        let categoryObject : Array<any> = [];
        this.serverSideCategoryFilter = '';
        // this.categoryName = [];
        this.categoryByUrl = '';
        this.limitedCategory.forEach(element => {
          if(element.isSelected){
            categoryObject.push(element.id);
            // this.categoryName.push(element.text)
          }
        });
        this.serverSideCategoryFilter = categoryObject.join();
        break;
      case 'brand' :
        let brandObject : Array<any> = [];
        this.serverSideBrandFilter = '';
        // this.brandName = [];
        this.brandByUrl = '';
        this.limitedBrand.forEach(element => {
          if(element.isSelected){
            brandObject.push(element.id);
            // this.brandName.push(element.text)
          }
        });
        this.serverSideBrandFilter = brandObject.join();
        break;
      case 'model' :
        let modelObject : Array<any> = [];
        this.serverSideModelFilter = '';
        // this.modelName = [];
        this.modelByUrl = '';
        this.limitedModel.forEach(element => {
          if(element.isSelected){
            modelObject.push(element.id);
            // this.modelName.push(element.text)
          }
        });
        this.serverSideModelFilter = modelObject.join();
        break;
      case 'location':
        let locationObject : Array<any> = [];
        this.serverSideLocationFilter = '';
        // this.locationName = [];
        this.locationByUrl = '';
        this.limitedLocation.forEach(element => {
          if(element.isSelected){
            locationObject.push(element.id);
            // this.locationName.push(element.text);
          }
        });
        this.serverSideLocationFilter = locationObject.join();
        break;
      case 'certificate':
        let certificateObject : Array<any> = [];
        this.serverSideCertificateFilter = '';
        this.certificateByUrl = '';
        this.typeOfAsset.forEach(element => {
          if(element.isSelected){
            certificateObject.push(element.text);
          }
        });
        this.serverSideCertificateFilter = certificateObject.join();
        break;
      case 'mfgYear' : 
        let yearObject : Array<any> = [];
        this.serverSideYearFilter = [];
        this.mfgYearFilterList.forEach(element => {
          let finalYearArray = [];
          if(element.isSelected){
            finalYearArray = element.text.split(" - ");
            if(finalYearArray[1] == "Above"){
              let data = "("+finalYearArray[0]+")";
              yearObject.push(data);
            }
            else{
              let customeString = "("+finalYearArray[0]+","+finalYearArray[1]+")";
              yearObject.push(customeString);
            }
          }
          // if(element.isSelected){
          //   yearObject.push(element.id);
          // }
        });
        // this.serverSideYearFilter = yearObject.join();
        this.serverSideYearFilter = this.serverSideYearFilter.concat(yearObject);
        break;
      case 'price' : 
        let priceObject : Array<any> = [];
        let priceObjectGrt : Array<any> = [];
        let priceObjectLwr : Array<any> = [];
        this.serverSidePriceFilterSingle =[];
        this.priceFilterList.forEach(element => {
          let finalPriceArray = [];
          if(element.isSelected){
            if(element.text != "Price on Request"){
              finalPriceArray = element.text.split(" - ");
              if(finalPriceArray[0] != "Below" && finalPriceArray[1] != "Above"){
                let customeString = "("+finalPriceArray[0]+","+finalPriceArray[1]+")";
                priceObject.push(customeString);
              }
              else if(finalPriceArray[1] == "Above"){
                let data = "("+finalPriceArray[0]+")";
                priceObject.push(data);
              }
              else if(finalPriceArray[0] == "Below"){
                let data = "("+0+","+finalPriceArray[1]+")";
                priceObject.push(data);
              }
            }
            else{
              let data = "('price_on_request')";
              priceObject.push(data);
            }
            // priceObject.push(element.id);
          }
        });
        this.serverSidePriceFilterSingle = this.serverSidePriceFilterSingle.concat(priceObject);
        break;
    }
    // this.router.navigateByUrl('/used-equipment-dashboard/equipment-list');
    this.getfilter('');
    //this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
  }

  setProductsAccordingToFilters() {
    this.productList = [];
    var data;
    var noBrandSelected = true;
    var noCategorySelected = true;
    var noModelSelected = true;
    var noYearSelected = true;
    var noPriceSelected = true;
    var noLocationSelected = true;
    let key = Object.assign(this.productList, this.BeforeFilterProductList);
    if (this.searchCategoryFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchCategoryFilter.length; i++) {
        let element = this.searchCategoryFilter[i].id;
        if (this.searchCategoryFilter[i].isSelected) {
          noCategorySelected = false;
          // let value = this.BeforeFilterProductList.filter(e => e.category.id == element);
          let value = this.productList.filter(e => e.category.id == element);
          // if(this.productList.length > 0)
          // {
          //   value = this.productList.filter(e => e.category.id == element);
          // }
          // else{
          //    value = this.BeforeFilterProductList.filter(e => e.category.id == element);
          // }
          if (value.length == 0 && this.productList.length == 0 && i == 0 && this.searchCategoryFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(value.length != 0){
          //   this.productList = this.productList.concat(value);
          // }
          else {
            data = data.concat(value);
            //this.productList = this.productList.concat(value);

          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (this.searchBrandFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchBrandFilter.length; i++) {
        let element = this.searchBrandFilter[i].id;
        if (this.searchBrandFilter[i].isSelected) {
          noBrandSelected = false;
          let value = this.productList.filter(e => e.brand.id == element);
          // if(this.productList.length > 0)
          // {
          //   value = this.productList.filter(e => e.brand.id == element);
          // }
          // else{
          //   value = this.BeforeFilterProductList.filter(e => e.brand.id == element);
          // }
          if (value.length == 0 && this.productList.length == 0 && i == 0 && this.searchBrandFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(value.length != 0){
          //   this.productList = this.productList.concat(value);
          // }
          else {
            data = data.concat(value);
          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (this.searchModelFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchModelFilter.length; i++) {
        let element = this.searchModelFilter[i].id;
        if (this.searchModelFilter[i].isSelected) {
          noModelSelected = false;
          let value = this.productList.filter(e => e.model.id == element);
          // if(this.productList.length > 0)
          // {
          //   value = this.productList.filter(e => e.model.id == element);
          // }
          // else{
          //   value = this.BeforeFilterProductList.filter(e => e.model.id == element);
          // }

          if (value.length == 0 && this.productList.length == 0 && i == 0 && this.searchModelFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(value.length != 0){
          //   this.productList = this.productList.concat(value);
          // }
          else {
            //this.productList = this.productList.concat(value);
            data = data.concat(value);
          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (this.searchLocationFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchLocationFilter.length; i++) {
        let element = this.searchLocationFilter[i].id;
        if (this.searchLocationFilter[i].isSelected) {
          noLocationSelected = false;
          var valueLocation: any = [];
          for (let i = 0; i < this.productList.length; i++) {
            if (this.productList[i].location != null) {
              if (this.productList[i].location.city != null) {
                if (this.productList[i].location.city.id == element) {
                  valueLocation.push(this.productList[i]);
                }
              }
            }
          }
          // if(this.productList.length > 0)
          // {
          //   for(let i = 0; i < this.productList.length; i++)
          //   {
          //     if(this.productList[i].location != null)
          //     {
          //       if(this.productList[i].location.city != null)
          //       {
          //         if(this.productList[i].location.city.id == element)
          //         {
          //           valueLocation.push(this.productList[i]);
          //         }
          //       }
          //     }
          //   }
          // }
          // else{
          //   for(let i = 0; i < this.BeforeFilterProductList.length; i++)
          //   {
          //     if(this.BeforeFilterProductList[i].location != null)
          //     {
          //       if(this.BeforeFilterProductList[i].location.city != null)
          //       {
          //         if(this.BeforeFilterProductList[i].location.city.id == element)
          //         {
          //           valueLocation.push(this.BeforeFilterProductList[i]);
          //         }
          //       }
          //     }
          //   }
          // }

          //let value = this.BeforeFilterProductList.filter(e => e.location.city.id == element);
          if (valueLocation.length == 0 && this.productList.length == 0 && i == 0 && this.searchLocationFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(valueLocation.length != 0){
          //   this.productList = this.productList.concat(valueLocation);
          // }
          else {
            //this.productList = this.productList.concat(valueLocation);
            data = data.concat(valueLocation);

          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (this.searchYearFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchYearFilter.length; i++) {
        let element = this.searchYearFilter[i].text;
        if (this.searchYearFilter[i].isSelected) {
          noYearSelected = false;
          let value = this.productList.filter(e => e.mfg_year == element);
          // if(this.productList.length > 0)
          // {
          //   value = this.productList.filter(e => e.mfg_year == element);
          // }
          // else{
          //   value = this.BeforeFilterProductList.filter(e => e.mfg_year == element);
          // }

          if (value.length == 0 && this.productList.length == 0 && i == 0 && this.searchYearFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(value.length != 0){
          //   this.productList = this.productList.concat(value);
          // }
          else {
            //this.productList = this.productList.concat(value);
            data = data.concat(value);

          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (this.searchPriceFilter.length > 0) {
      data = [];
      for (var i = 0; i < this.searchPriceFilter.length; i++) {
        let element = this.searchPriceFilter[i].text;
        var finalElement = [];
        if (element != "Price on Request") {
          finalElement = element.split(" - ");
        }
        if (this.searchPriceFilter[i].isSelected) {
          noPriceSelected = false;
          var value;
          if (finalElement.length == 0) {
            if (element == "Price on Request") {
              value = this.productList.filter(e => e.is_price_on_request == true);
            }
            else {
              value = this.productList.filter(e => e.selling_price == element);
            }

          }
          else if (finalElement[1] == "Above") {
            element = finalElement[0];
            value = this.productList.filter(e => e.selling_price >= element);
          }
          else if (finalElement[0] == "Below")
          {
            element = finalElement[1];
            value = this.productList.filter(e => e.selling_price <= element);
          }
          else {
            value = this.productList.filter(e => e.selling_price >= finalElement[0] && e.selling_price <= finalElement[1]);
          }
          // if(this.productList.length > 0)
          // {
          //   if(finalElement.length == 0)
          //   {
          //     if(element == "Price on Request")
          //     {
          //       value = this.productList.filter(e => e.is_price_on_request == true);
          //     }
          //     else{
          //       value = this.productList.filter(e => e.selling_price == element);
          //     }

          //   }
          //   else if(finalElement[1] == "Above")
          //   {
          //     element = finalElement[0];
          //     value = this.productList.filter(e => e.selling_price == element);
          //   }
          //   else{
          //     value = this.productList.filter(e => e.selling_price >= finalElement[0] && e.selling_price <= finalElement[1]);
          //   }
          // }
          // else{
          //   if(finalElement.length == 0)
          //   {
          //     if(element == "Price on Request")
          //     {
          //       value = this.BeforeFilterProductList.filter(e => e.is_price_on_request == true);
          //     }
          //     else{
          //       value = this.BeforeFilterProductList.filter(e => e.selling_price == element);
          //     }

          //   }
          //   else if(finalElement[1] == "Above")
          //   {
          //     element = finalElement[0];
          //     value = this.BeforeFilterProductList.filter(e => e.selling_price == element);
          //   }
          //   else{
          //     value = this.BeforeFilterProductList.filter(e => e.selling_price >= finalElement[0] && e.selling_price <= finalElement[1]);
          //   }
          // }
          if (value.length == 0 && this.productList.length == 0 && i == 0 && this.searchPriceFilter[i].isSelected == false) {
            const product = Object.assign(this.productList, this.BeforeFilterProductList);
          }
          // else if(value.length != 0){
          //   this.productList = this.productList.concat(value);
          // }
          else {
            //this.productList = this.productList.concat(value);
            value.forEach(element => {
              let index = data.findIndex(e => e.id == element.id);
              if (index == -1) {
                data = data.concat(value);
              }
            });

          }
        }
      }
      this.productList = JSON.parse(JSON.stringify(data));
    }
    if (noBrandSelected && noCategorySelected && noModelSelected && noYearSelected && noPriceSelected && noLocationSelected) {
      // if(this.categoryByUrl == undefined && this.brandByUrl == undefined && this.modelByUrl == undefined && this.locationByUrl == undefined){
      //   Object.assign(this.productList, this.BeforeFilterProductList);
      // }
      Object.assign(this.productList, this.BeforeFilterProductList);
      // else{
      //   this.productList = [];
      // }
    }
  }

  
  getAllProductApiCall(searchBy : any,pageNumber : number, isApprovedStatus : number){
    this.getUsedEquipmentApiCall(searchBy, pageNumber, isApprovedStatus)
  }

  getUsedEquipmentApiCall(searchBy:any,pageNumber : number, isApprovedStatus : number){
    //this.compareList = []
    //this.spinner.show()
    let ser = this.searchText == undefined ? '' : this.searchText;
    let payLoadData = {
      search: ser
    }
    let categoryParam = this.serverSideCategoryFilter == '' || this.serverSideCategoryFilter == undefined ? 
      'category__id__in=' : 'category__id__in='+this.serverSideCategoryFilter;
      let brandParam = this.serverSideBrandFilter == '' || this.serverSideBrandFilter == undefined ? 
      '&brand__id__in=' : '&brand__id__in='+this.serverSideBrandFilter;
      let modelParam = this.serverSideModelFilter == '' || this.serverSideModelFilter == undefined ? 
      '&model__id__in=' : '&model__id__in='+this.serverSideModelFilter;
      // let yearParam =  this.serverSideYearFilter == '' || this.serverSideYearFilter == undefined ? 
      // '&mfg_year__in=' : '&mfg_year__in='+this.serverSideYearFilter;
      let yearParam =  this.serverSideYearFilter == '' || this.serverSideYearFilter == undefined ? 
      '&mfg_year_range=[]' : '&mfg_year_range='+'['+this.serverSideYearFilter+']';
      let locationParam =  this.serverSideLocationFilter == '' || this.serverSideLocationFilter == undefined ? 
      '&pin_code__city__id__in=' : '&pin_code__city__id__in='+this.serverSideLocationFilter;
      let PriceParam =  this.serverSidePriceFilterSingle == '' || this.serverSidePriceFilterSingle == undefined ? 
      '&price_range=[]' : '&price_range='+'['+this.serverSidePriceFilterSingle+']';
      let sort = this.sortedBy == '' || this.sortedBy == undefined ?
      '' : '&ordering='+this.sortedBy;
      let certificateParam = this.serverSideCertificateFilter == '' || this.serverSideCertificateFilter == undefined ? 
      '&certificate__name__in=' : '&certificate__name__in='+this.serverSideCertificateFilter;
      let typeParam = this.serverSideTypeFilter == '' || this.serverSideTypeFilter == undefined ? 
      '&status__in=' : '&status__in='+this.serverSideTypeFilter;
      if(!this.serverSideTypeFilter){
        typeParam =  '&status__in=2,3,5';
      }
      let obj = {
      }
      if(this.serverSideCategoryFilter){
        obj['category__id__in'] = this.serverSideCategoryFilter
      }
      if(this.serverSideBrandFilter){
        obj['brand__id__in'] = this.serverSideBrandFilter
      }if(this.serverSideModelFilter){
        obj['model__id__in'] = this.serverSideModelFilter
      }if(this.serverSideYearFilter){
        obj['mfg_year_range'] ='['+this.serverSideYearFilter+']'
      }if(this.serverSidePriceFilterSingle){
        obj['price_range'] = '['+this.serverSidePriceFilterSingle+']'
      }if(this.serverSideCertificateFilter){
        obj['certificate__name__in'] = this.serverSideCertificateFilter
      }if(this.sortedBy){
        obj['ordering'] = this.sortedBy
      }
      if(this.serverSideLocationFilter){
        obj['pin_code__city__id__in'] = this.serverSideLocationFilter
      }
      if(this.serverSideTypeFilter){
        obj['status__in'] = this.serverSideTypeFilter
      }else{
        obj['status__in'] = '2,3,5'
      }
      if(this.cognitoId){
        obj['cognito_id'] = this.cognitoId
      }
      obj['is_sell'] = true;
      obj['is_active'] = true;
      if(ser){
        obj['search'] = ser
      }
      
      // let greaterPriceParam =  this.serverSidePriceFilterGrt == '' || this.serverSidePriceFilterGrt == undefined ? 
      // '&selling_price__gte=' : '&selling_price__gte='+this.serverSidePriceFilterGrt;
      // let lowerPriceParam =  this.serverSidePriceFilterLow == '' || this.serverSidePriceFilterLow == undefined ? 
      // '&selling_price__lte=' : '&selling_price__lte='+this.serverSidePriceFilterLow;
      // let PriceParam =  this.serverSidePriceFilterSingle == '' || this.serverSidePriceFilterSingle == undefined ? 
      // '&selling_price=' : '&selling_price='+this.serverSidePriceFilterSingle;

    let filterQueryParams = categoryParam + brandParam +modelParam+ yearParam + locationParam + PriceParam + sort + certificateParam +typeParam;
   this.productService.getUsedEquipmentFilters(this.searchBy?this.searchBy:'',obj).subscribe((res:any)=>{
    console.log(res);
    if (res != null) {
      this.types = []
      this.limitedTypes = []
      /* type */
      if(res.type){
        this.types = res.type;
        let typeObject = this.createCustomFilters(res.type, 'type');
        this.limitedTypes = typeObject;
      }
      /* brand */
      this.brands = res.brand;
      this.breakedBrandObj = []
      let brandObject = this.createCustomFilters(res.brand, 'brand');
      this.limitedBrand = brandObject;
      if(this.limitedBrand.length > 5){
        this.cloneBrandObj = JSON.parse(JSON.stringify(this.limitedBrand));
        this.breakedBrandObj = this.cloneBrandObj.splice(3);
        this.limitedBrand.length = 5;
      }
      /* category */
      this.categories = res.category;
      this.breakedData = []
      let categoryObject = this.createCustomFilters(res.category, 'category');
      this.limitedCategory = categoryObject;
      if(this.limitedCategory.length > 5){
        this.cloneObj = JSON.parse(JSON.stringify(this.limitedCategory));
        this.breakedData  = this.cloneObj.splice(3);
        this.limitedCategory.length = 5;
      }
      /* model */
      this.models = res.model;
      this.breakedModelObj = []
      let modelObject = this.createCustomFilters(res.model, 'model');
      this.limitedModel = modelObject;
      if(this.limitedModel.length > 5){
        this.cloneModelObj = JSON.parse(JSON.stringify(this.limitedModel));
        this.breakedModelObj = this.cloneModelObj.splice(3);
        this.limitedModel.length = 5;
      }
      /* location */
      this.locations = res.city;
      this.breakedLocationObj = []
      let locationObject = this.createCustomFilters(res.city, 'location');
      this.limitedLocation = locationObject;
      if(this.limitedLocation.length > 5){
        this.cloneLocationObj = JSON.parse(JSON.stringify(this.limitedLocation));
        this.breakedLocationObj = this.cloneLocationObj.splice(3);
        this.limitedLocation.length = 5;
      }
      this.certificateList = res.certificate;
      let certificateObject = this.createCustomFilters(res.certificate, 'certificate');
      console.log(certificateObject);
      this.breakedCertificateObj = []
      this.typeOfAsset = certificateObject;
      if(this.typeOfAsset.length > 5){
        this.cloneCertificateObj = JSON.parse(JSON.stringify(this.typeOfAsset));
        this.breakedCertificateObj = this.cloneCertificateObj.splice(3);
        this.typeOfAsset.length = 5;
      }
      /* Year */
      // this.years = res.mfg_year;
      // let yearObject = this.createCustomFilters(res.mfg_year, 'year');
      // this.mfgYearFilterList = yearObject;
      // if(this.mfgYearFilterList.length > 5){
      //   this.cloneYearObj = JSON.parse(JSON.stringify(this.mfgYearFilterList));
      //   this.breakedYearObj = this.cloneYearObj.splice(3);
      //   this.mfgYearFilterList.length = 5;
      // }|| this.categoryName?.length > 0|| this.brandName?.length > 0 || this.modelName?.length > 0 || this.locationName?.length > 0

      if(this.categoryByUrl != undefined || this.brandByUrl != undefined || this.modelByUrl != undefined || this.locationByUrl != undefined|| this.certificateByUrl != undefined )
      {
        if(this.categoryByUrl == "all" || this.brandByUrl == "all"){
          this.setFilterAccordingToAllCatAurBrand();
        }
        // else{
        //   this.setFilterAccordingToDashBoardFilter();
        // }
      }
      // else{
      //   //this.setDefaultFilter();
      // }
      if(this.serverSideTypeFilter || this.serverSideCertificateFilter || this.serverSideYearFilter || this.serverSidePriceFilterSingle || this.serverSideCategoryFilter
      || this.serverSideBrandFilter || this.serverSideModelFilter || this.serverSideLocationFilter){
        this.setFilters();
      }
      // if(this.sortInOrder != '0'){
      //   this.sortProduct(this.sortInOrder);
      // }
      // else{
      //   this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
      // }
    }

  }, (err: any) => {
    console.log(err);
  });
    this.productService.getUsedProductListWithPayLoad(searchBy, pageNumber, isApprovedStatus, payLoadData,this.cognitoId, filterQueryParams).subscribe(async(res : any) =>{
      if(this.pageNumber == 1){
        this.productList = [];
      }
      this.count = res.count;
      var getResponse = res.results;
      //this.productList = [];
      this.isApiHitSuccessful = true;
       
      this.productApi = true;
      this.totalProduct = res.count;
      for (let image of getResponse) {
        if (image.model.image_url == "https://www.youtube.com/") {
          image.model.image_url = "";
        }
      }
      getResponse.forEach(data => {
        data.checkCompare = false;
      })
      this.productList = this.productList.concat(getResponse);
      // this.sortProduct(this.sortInOrder);
      // this.BeforeFilterProductList = this.BeforeFilterProductList.concat(getResponse);
      if(this.searchBy != undefined && this.searchBy != '')
      {
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
      else if(this.searchText != undefined && this.searchText != '')
      {
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
      // else{
      //   if(this.categoryByUrl != undefined || this.brandByUrl != undefined || this.modelByUrl != undefined || this.locationByUrl != undefined)
      //   {
      //     if(this.categoryByUrl == "all" || this.brandByUrl == "all"){
      //       this.setFilterAccordingToAllCatAurBrand();
      //     }
      //     else{
      //       this.setFilterAccordingToDashBoardFilter();
      //     }
      //   }
      // }
    }, err =>{
      if(err.detail == "Invalid page.")
      {
        this.stopCallingAPI = true;
      }
      console.log(err);
      if(this.pageNumber == 1){
        this.productList = [];
      }
    })
  }
  
  setFilterAccordingToAllCatAurBrand(){
    if(this.categoryByUrl == "all")
    {
      /* for category by default selected as true */
      let categoryObject : Array<any> = [];
      this.limitedCategory.forEach(element => {
          element.isSelected = true;
            categoryObject.push(element.id);
        });
      this.serverSideCategoryFilter = categoryObject.join();
      this.limitedBrand.forEach(element =>{
        element.isSelected = false;
      });
    }
    else if(this.brandByUrl == "all")
    {
      /* for brand by default selected as true */
      let brandObject : Array<any> = [];
        this.limitedBrand.forEach(element => {
          element.isSelected = true;
          brandObject.push(element.id);
        });
      this.serverSideBrandFilter = brandObject.join();
      this.limitedCategory.forEach(element => {
        element.isSelected = false;
      });
    }
  }



  setFilterAccordingToDashBoardFilter(){
    if(this.categoryByUrl != undefined){
      let categoryObject : Array<any> = [];
      this.limitedCategory.forEach(element =>{
        if(element.id == this.categoryByUrl){
          element.isSelected = true;
          categoryObject.push(element.id);
        }
        // else{
        //   categoryObject.push(element.id);
        // }
        // this.serverSideCategoryFilter = categoryObject.join();
      });
      if(categoryObject.length == 0){
        categoryObject.push(this.categoryByUrl);
      }
      this.serverSideCategoryFilter = categoryObject.join();
      if(this.brandByUrl == undefined){
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
    }
    if(this.brandByUrl != undefined){
      let brandObject : Array<any> = [];
      this.limitedBrand.forEach(element =>{
        if(element.id == this.brandByUrl){
          element.isSelected = true;
          brandObject.push(element.id);
        }
        // else{
        //   brandObject.push(element.id);
        // }
        // this.serverSideBrandFilter = brandObject.join();
      });
      if(brandObject.length == 0){
         brandObject.push(this.brandByUrl);
      }
      this.serverSideBrandFilter = brandObject.join();
      if(this.categoryByUrl == undefined){
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
      }
    }
    if(this.modelByUrl != undefined){
      let modelObject : Array<any> = [];
      this.limitedModel.forEach(element =>{
        if(element.id == this.modelByUrl){
          element.isSelected = true;
          modelObject.push(element.id);
        }
        // else{
        //   modelObject.push(element.id);
        // }
        // this.serverSideModelFilter = modelObject.join();
      });
      if(modelObject.length == 0){
        modelObject.push(this.modelByUrl);
      }
      this.serverSideModelFilter = modelObject.join();
      if(this.categoryByUrl == undefined){
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
      }
      if(this.brandByUrl == undefined){
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
    }
    if(this.locationByUrl != undefined){
      let locationObject : Array<any> = [];
      this.limitedLocation.forEach(element =>{
        if(element.id == this.locationByUrl){
          element.isSelected = true;
          locationObject.push(element.id);
        }
        // else{
        //   locationObject.push(element.id);
        // }
        // this.serverSideLocationFilter = locationObject.join();
      });
      if(locationObject.length == 0){
        locationObject.push(this.locationByUrl);
      }
      this.serverSideLocationFilter = locationObject.join();
      if(this.categoryByUrl == undefined){
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
      }
      if(this.brandByUrl == undefined){
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
    }
    if(this.certificateByUrl != undefined){
      let certificateObject : Array<any> = [];
      this.typeOfAsset.forEach(element =>{
        if(element.id == this.certificateByUrl){
          element.isSelected = true;
          certificateObject.push(element.id);
        }
        // else{
        //   locationObject.push(element.id);
        // }
        // this.serverSideLocationFilter = locationObject.join();
      });
      if(certificateObject.length == 0){
        certificateObject.push(this.certificateByUrl);
      }
      this.serverSideLocationFilter = certificateObject.join();
      if(this.categoryByUrl == undefined){
        this.limitedCategory.forEach(element =>{
          element.isSelected = false;
        });
      }
      if(this.brandByUrl == undefined){
        this.limitedBrand.forEach(element =>{
          element.isSelected = false;
        });
      }
    }
    // if(this.categoryName != undefined && this.categoryName?.length >0){
    //   let categoryObject : Array<any> = [];
    //   for(let category of this.categoryName){
    //     this.limitedCategory.forEach(element =>{
    //       if(element.text == category){
    //         element.isSelected = true;
    //         categoryObject.push(element.id);
    //       }
    //       // else{
    //       //   categoryObject.push(element.id);
    //       // }
    //       // this.serverSideCategoryFilter = categoryObject.join();
    //     });
    //   }
      
    //   if(categoryObject.length == 0){
    //     categoryObject.push(this.categoryByUrl);
    //   }
    //   this.serverSideCategoryFilter = categoryObject.join();
    //   if(this.brandByUrl == undefined && this.brandName?.length == 0){
    //     this.limitedBrand.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    // }
    // if(this.brandName != undefined && this.brandName?.length >0){
    //   let brandObject : Array<any> = [];
    //   for(let brand of this.brandName){
    //   this.limitedBrand.forEach(element =>{
    //     if(element.text == brand){
    //       element.isSelected = true;
    //       brandObject.push(element.id);
    //     }
    //     // else{
    //     //   brandObject.push(element.id);
    //     // }
    //     // this.serverSideBrandFilter = brandObject.join();
    //   });
    // }
    //   if(brandObject.length == 0){
    //      brandObject.push(this.brandByUrl);
    //   }
    //   this.serverSideBrandFilter = brandObject.join();
    //   if(this.categoryByUrl == undefined && this.categoryName?.length == 0){
    //     this.limitedCategory.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    // }
    // if(this.modelName != undefined && this.modelName?.length >0){
    //   let modelObject : Array<any> = [];
    //   for(let model of this.modelName){
    //   this.limitedModel.forEach(element =>{
    //     if(element.text == model){
    //       element.isSelected = true;
    //       modelObject.push(element.id);
    //     }
    //     // else{
    //     //   modelObject.push(element.id);
    //     // }
    //     // this.serverSideModelFilter = modelObject.join();
    //   });   
    // }
    //   if(modelObject.length == 0){
    //     modelObject.push(this.modelByUrl);
    //   }
    //   this.serverSideModelFilter = modelObject.join();
    //   if(this.categoryByUrl == undefined && this.categoryName?.length > 0){
    //     this.limitedCategory.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    //   if(this.brandByUrl == undefined && this.brandName?.length > 0){
    //     this.limitedBrand.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    // }
    // if(this.locationName != undefined && this.locationName?.length >0){
    //   let locationObject : Array<any> = [];
    //   for(let location of this.locationName){
    //     this.limitedLocation.forEach(element =>{
    //       if(element.text == location){
    //         element.isSelected = true;
    //         locationObject.push(element.id);
    //       }
    //       // else{
    //       //   locationObject.push(element.id);
    //       // }
    //       // this.serverSideLocationFilter = locationObject.join();
    //     });
    //   }
    //   if(locationObject.length == 0){
    //     locationObject.push(this.locationByUrl);
    //   }
    //   this.serverSideLocationFilter = locationObject.join();
    //   if(this.categoryByUrl == undefined && this.categoryName?.length  == 0){
    //     this.limitedCategory.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    //   if(this.brandByUrl == undefined && this.brandName?.length  == 0){
    //     this.limitedBrand.forEach(element =>{
    //       element.isSelected = false;
    //     });
    //   }
    // }
    
  }

  setPrice(objectData: any) {
    var returnAmount;
    let isPriceOnRequest = objectData.is_price_on_request;
    let sellingPrice = objectData.selling_price;
    if (isPriceOnRequest && sellingPrice == null) {
      returnAmount = "Price On Request";
    }
    else {
      sellingPrice = sellingPrice == null ? "0" : sellingPrice;
      returnAmount = "₹ " + sellingPrice;
    }
    return returnAmount;
  }

  // sortProduct(type: string) {
  //   var nullSellingPriceArr;
  //   var clonedList;
  //   switch (type) {
  //     case '1':
  //       nullSellingPriceArr = this.commonLowestHigestSort();
  //       clonedList = this.commonLowestHigestSortForClonedList();
  //       this.productList = this.productList.sort((a, b) => parseInt(a.selling_price) - parseInt(b.selling_price));
  //       this.BeforeFilterProductList = this.BeforeFilterProductList.sort((a, b) => parseInt(a.selling_price) - parseInt(b.selling_price));
  //       this.productList = [...this.productList, ...nullSellingPriceArr];
  //       this.BeforeFilterProductList = [...this.BeforeFilterProductList, ...clonedList];
  //       break;
  //     case '2':
  //       nullSellingPriceArr = this.commonLowestHigestSort();
  //       clonedList = this.commonLowestHigestSortForClonedList();
  //       this.productList = this.productList.sort((a, b) => parseInt(b.selling_price) - parseInt(a.selling_price));
  //       this.BeforeFilterProductList = this.BeforeFilterProductList.sort((a, b) => parseInt(b.selling_price) - parseInt(a.selling_price));
  //       this.productList = [...this.productList, ...nullSellingPriceArr];
  //       this.BeforeFilterProductList = [...this.BeforeFilterProductList, ...clonedList];
  //       break;
  //     case '3':
  //       let isPriceOnReq = this.productList.filter(e => e.is_price_on_request == true);
  //       isPriceOnReq.forEach(element => {
  //         let index = this.productList.findIndex(e => e == element);
  //         if (index != -1) {
  //           this.productList.splice(index, 1);
  //         }
  //       });
  //       this.productList = [...isPriceOnReq, ...this.productList];
        
  //       let isPriceOnReqCloned = this.BeforeFilterProductList.filter(e => e.is_price_on_request == true);
  //       isPriceOnReqCloned.forEach(element => {
  //         let index = this.BeforeFilterProductList.findIndex(e => e == element);
  //         if (index != -1) {
  //           this.BeforeFilterProductList.splice(index, 1);
  //         }
  //       });
  //       this.BeforeFilterProductList = [...isPriceOnReqCloned, ...this.BeforeFilterProductList];
  //       break;
  //     case '4':
  //       let isFeatured = this.productList.filter(e => e.is_featured == true);
  //       isFeatured.forEach(element => {
  //         let index = this.productList.findIndex(e => e.element);
  //         if (index != -1) {
  //           this.productList.splice(index, 1);
  //         }
  //       });
  //       this.productList = [...isFeatured, ...this.productList];

  //       let isFeaturedCloned = this.BeforeFilterProductList.filter(e => e.is_featured == true);
  //       isFeaturedCloned.forEach(element => {
  //         let index = this.BeforeFilterProductList.findIndex(e => e.element);
  //         if (index != -1) {
  //           this.BeforeFilterProductList.splice(index, 1);
  //         }
  //       });
  //       this.BeforeFilterProductList = [...isFeaturedCloned, ...this.BeforeFilterProductList];
  //       break;
  //   }
  // }

  sortProduct(type : string){
    this.pageNumber = 1;
    switch(type){
      case '1' :
        this.sortedBy = 'selling_price'; 
        break;
      case '2' :
        this.sortedBy = '-selling_price';
        break;
      case '3' :
        this.sortedBy = 'created_at';
        break;
      case '4' :
        this.sortedBy = '-created_at';
        break;
    }
   /* Call API */
   this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
  }

  commonLowestHigestSort() {
    let allnull = this.productList.filter(e => e.is_price_on_request == true);
    allnull.forEach(element => {
      let index = this.productList.findIndex(e => e == element);
      if (index != -1) {
        this.productList.splice(index, 1);
      }
    });
    return allnull;
  }

  commonLowestHigestSortForClonedList() {
    let allnull = this.BeforeFilterProductList.filter(e => e.is_price_on_request == true);
    allnull.forEach(element => {
      let index = this.BeforeFilterProductList.findIndex(e => e == element);
      if (index != -1) {
        this.BeforeFilterProductList.splice(index, 1);
      }
    });
    return allnull;
  }

  bindBucketCapacity(product) {
    let data = product;
    var final;
    if (data != null) {
      final = data;
    }
    else {
      final = 'N/A'
    }
    return final
  }

  clearAurSelectFilter(type: any) {
    this.searchText = '';
    this.pageNumber = 1;
    this.productList = [];
    if (type != 'clear') {
      this.ifSelectAllFilters();
    } else {
      this.ifNoFilterSelected();
    }
    this.pageLoad++;
  }

  ifSelectAllFilters(){
    let typeObject : Array<any> = [];
     this.serverSideTypeFilter = '';
     this.limitedTypes.forEach(element => {
       if(!element.isSelected){
        element.isSelected = true;
       }
       typeObject.push(element.id);  
     });
     this.serverSideTypeFilter = typeObject.join();
     /* for category by default selected as true */
     let categoryObject : Array<any> = [];
     this.serverSideCategoryFilter = '';
     this.limitedCategory.forEach(element => {
       if(!element.isSelected){
        element.isSelected = true;
       }
       categoryObject.push(element.id);  
     });
     this.serverSideCategoryFilter = categoryObject.join();
     /* for brand by default selected as true */
     let brandObject : Array<any> = [];
     this.serverSideBrandFilter = '';
       this.limitedBrand.forEach(element => {
        if(!element.isSelected){
          element.isSelected = true;
        }
        brandObject.push(element.id);
     });
     this.serverSideBrandFilter = brandObject.join();
     /* for model by default selected as true */
     let modelObject : Array<any> = [];
     this.serverSideModelFilter = '';
     this.limitedModel.forEach(element => {
      if(!element.isSelected){
        element.isSelected = true;
      }
      modelObject.push(element.id);
     });
     this.serverSideModelFilter = modelObject.join();
     /* for location by default selected as true */
     let locationObject : Array<any> = [];
        this.serverSideLocationFilter = '';
        this.limitedLocation.forEach(element => {
          if(!element.isSelected){
            element.isSelected = true;
          }
          locationObject.push(element.id);
        });
        this.serverSideLocationFilter = locationObject.join();
        /* for year by default selected as true */
        let yearObject : Array<any> = [];
        this.serverSideYearFilter = '';
        this.mfgYearFilterList.forEach(element => {
          let finalYearArray = [];
          if(!element.isSelected){
            element.isSelected = true;
          }
          finalYearArray = element.text.split(" - ");
            if(finalYearArray[1] == "Above"){
              let data = "("+finalYearArray[0]+")";
              yearObject.push(data);
            }
            else{
              let customeString = "("+finalYearArray[0]+","+finalYearArray[1]+")";
              yearObject.push(customeString);
            }
          //yearObject.push(element.id);
        });
        this.serverSideYearFilter = this.serverSideYearFilter.concat(yearObject);
        //this.serverSideYearFilter = yearObject.join();
        /* for price by default selected as true */
        let priceObject : Array<any> = [];
        let priceObjectGrt : Array<any> = [];
        let priceObjectLwr : Array<any> = [];
        this.serverSidePriceFilterSingle = [];
        this.priceFilterList.forEach(element => {
          let finalPriceArray = [];
          if(!element.isSelected){
            element.isSelected = true;
          }
          if(element.text != "Price on Request"){
            finalPriceArray = element.text.split(" - ");
            if(finalPriceArray[0] != "Below" && finalPriceArray[1] != "Above"){
              let customeString = "("+finalPriceArray[0]+","+finalPriceArray[1]+")";
              priceObject.push(customeString);
            }
            else if(finalPriceArray[1] == "Above"){
              let data = "("+finalPriceArray[0]+")";
              priceObject.push(data);
            }
            else if(finalPriceArray[0] == "Below"){
              let data = "("+0+","+finalPriceArray[1]+")";
              priceObject.push(data);
            }
          }
          else{
            let data = "('price_on_request')";
            priceObject.push(data);
          }
        });
        this.serverSidePriceFilterSingle = this.serverSidePriceFilterSingle.concat(priceObject);
        
        /* Call API */
        this.getfilter('less');
        //this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
  }

  ifNoFilterSelected(){
    this.serverSideCategoryFilter= '';
    this.serverSideBrandFilter = '';
    this.serverSideModelFilter = '';
    this.serverSideLocationFilter = '';
    this.serverSideYearFilter = '';
    this.serverSidePriceFilterGrt = '';
    this.serverSidePriceFilterLow = '';
    this.serverSidePriceFilterSingle = '';
    this.serverSideCertificateFilter='';
    this.serverSideTypeFilter='';
    this.categoryName = [];
    this.brandName = [];
    this.modelName = [];
    this.locationName = [];
    this.categoryByUrl = '';
    this.brandByUrl = '';
    this.modelByUrl = '';
    this.locationByUrl = '';
    this.certificateByUrl = '';
     /* Type */
    this.limitedTypes.forEach(element =>{
      element.isSelected = false;
    });
    /* Category */
    this.limitedCategory.forEach(element =>{
      element.isSelected = false;
    });
    /* Brand */
    this.limitedBrand.forEach(element =>{
      element.isSelected = false;
    });
    /* model */
    this.limitedModel.forEach(element => {
      element.isSelected = false;
    });
    /* location */
    this.limitedLocation.forEach(element => {
      element.isSelected = false;
    });
    /* Certificate */
    this.typeOfAsset.forEach(element =>{
      element.isSelected = false;
    });
    /* Year */
    this.mfgYearFilterList.forEach(element => {
      element.isSelected = false;
    });
    /* Price */
    this.priceFilterList.forEach(element => {
      element.isSelected = false;
    });
    this.router.navigateByUrl('/used-equipment-dashboard/equipment-list');
    /* Call API */
    this.getfilter('')
    //this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
  }

  remove_compare(i){
    this.compareList.splice(i,1)
  }

  showMore(type){
    switch(type){
      case 'category' : 
        this.isCategoryMore = false;
        this.limitedCategory = [...this.limitedCategory,...this.breakedData];
        break;
      case 'brand' :
        this.isBrandMore = false;
        this.limitedBrand = [...this.limitedBrand,...this.breakedBrandObj];
        break;
      case 'model' :
        this.isModelMore = false;
        this.limitedModel = [...this.limitedModel,...this.breakedModelObj];
        break;
      case 'location' :
        this.isLocationMore = false;
        this.limitedLocation = [...this.limitedLocation,...this.breakedLocationObj];
        break;
        case 'mfgYear' :
        this.isYearMore = false;
        this.mfgYearFilterList = [...this.mfgYearFilterList,...this.breakedYearObj];
        break;
        case 'certificate' :
          this.isCertificateMore = false;
          this.typeOfAsset = [...this.typeOfAsset,...this.breakedCertificateObj];
        break;
    }
    
  }

  showLess(type){
    switch(type){
      case 'category' :
        this.isCategoryMore = true;
        this.limitedCategory.length = 5;  
        break;
      case 'brand' :
        this.isBrandMore = true;
        this.limitedBrand.length = 5;
        break;
      case 'model' :
        this.isModelMore = true;
        this.limitedModel.length = 5;
        break;
      case 'location' :
        this.isLocationMore = true;
        this.limitedLocation.length = 5;
        break;
        case 'mfgYear' :
        this.isYearMore = true;
        this.mfgYearFilterList.length = 5;
        break;
        case 'certificate' :
          this.isCertificateMore = true;
          this.typeOfAsset.length = 5;
          break;
    }
  }

  addToWishlist(id){
    if(this.cognitoId){
    let request = {
      new_equipment:null,
      used_equipment:id,
      type:2,
      user: this.cognitoId
    }
    this.commonService.addToWatchlist(request).subscribe((res:any)=>{
      this.pageNumber = 1;
      // this.productList = [];
      this.BeforeFilterProductList = [];
      //this.getfilter('less');
      
      this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
      this.getRecentlyViewed();
    });
  }
  }
  removeFromWatchlist(id){
    let queryParam = 
    'asset_type='+'used'+
    '&asset_id=' +id+
    '&cognito_id='+ this.cognitoId;
    this.commonService.removeFromWatchlist(queryParam).subscribe((res:any)=>{
      this.pageNumber = 1;
      // this.productList = [];
      this.BeforeFilterProductList = [];
      //this.getfilter('less');
      
      this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
      this.getRecentlyViewed();
    });
  }

  addRemoveFromCompare(isChecked,product){
    if(this.compareList.length != 0){
      if(product.category.id != this.compareList[0].category.id){
        this.notify.error('Please select products of same category to compare');
        return;
      }
    }
    console.log(isChecked);
    if(isChecked){
      let size;
      if(window.innerWidth > 768){
        size =2
      }else{
        size=1;
      }
      if (this.compareList.length > size) {
        size = size+1;
        product.checkCompare = false;
        this.cd.detectChanges();
        this.notify.error(
          'Cannot compare more than '+ size +' products'
        );
        return;
      }
      else {
        product.checkCompare = true;
        this.compareList.push(product);
        this.cd.detectChanges();
      }
    }else{
      let index = this.compareList.indexOf(product);
      if(index >=0){
        this.compareList.splice(index, 1);
        this.cd.detectChanges();
      }
    }
  }

  compare(){
    if(this.compareList.length > 1){
      this.storage.setSessionStorageData('compareList',this.compareList,true);
      this.storage.setSessionStorageData('type','used',false);
      this.router.navigate(['used-equipment-dashboard/compare']);
    }
  }

  sidebarshow() {
    this.siebarToggle = !this.siebarToggle;
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.pageNumber = 1;
      this.getAllProductApiCall(this.searchBy,this.pageNumber, this.isApprovedStatus);
    }
  }

  getCertificateImage(id){
    let certificate = this.certificateList.find(f => { return f.certificate == id });
    if(certificate && certificate.certificate__certificate_logo_image){
      return certificate.certificate__certificate_logo_image;
    }else{
      return '';
    }
    
  }
  getCertificateName(id){
    let certificate = this.certificateList.find(f => { return f.certificate == id });
    if(certificate && certificate.certificate__name){
      return certificate.certificate__name;
    }else{
      return '';
    }
  }
  
  getUnits(str){
    if(str == 'enginePower') {
      return ' cc ';
      } else if(str == 'operatingWeight') {
      return ' kg ';
      }
      else if(str == 'bucketCapcity') {
        return ' kg ';
      } else if(str == 'grossWeight') {
        return ' kg ';
      }else if(str == 'liftingCapcity') {
        return ' kg ';
      }
      return '';
  }
  markFiltersSelected(){
    
  }
  closeFilters(){
    this.siebarToggle = false;
  }
}
