import { Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Gallery, ImageItem } from "ng-gallery";
import { CommonService } from 'src/app/services/common.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { PaymentService } from 'src/app/services/payment.service';
import { SigninComponent } from 'src/app/component/signin/signin.component';
import { AdminMasterService } from 'src/app/services/admin-master.service';

@Component({
  selector: 'app-old-equipment-details',
  templateUrl: './old-equipment-details.component.html',
  styleUrls: ['./old-equipment-details.component.css']
})
export class OldEquipmentDetailsComponent implements OnInit {
  @ViewChild('placeBidInput') placeBidInput!: ElementRef;

  public equipmentId!: string;
  public equipmentDetails: any;
  public visualImageData: any = [];
  public visualVideoData: any;
  public isCustomer: boolean = false;
  public documents: any;
  public documentArray: any[] = [];
  public placeBid: any;
  public isPreview: boolean = false;
  public cognitoId!: string;
  public isTxnPreview: string = 'false';
  public isAdmin: any;
  public recentlyViewedList: any;
  public isSameUser: boolean = false;
  public userRole: boolean = false;
  public disabledNotify: boolean = false;
  public redirectFromAdmin: boolean = false;
  public tradeBidData: any;
  public recentViewSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    responsive: {

      300: {
        items: 1,
        nav: false
      },
      400: {
        items: 1,
        nav: false
      },
      500: {
        items: 1,
        nav: false
      },
      600: {
        items: 2,
        nav: false
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };
  constructor(public route: ActivatedRoute, public router: Router,
    public apiService: UsedEquipmentService, private dialog: MatDialog,
    private paymentService: PaymentService, public storage: StorageDataService,
    private commonService: CommonService, public notify: NotificationService,
    private spinner: NgxSpinnerService, public gallery: Gallery,
    private appRouteEnum: AppRouteEnum, public changeDetect: ChangeDetectorRef,
    private adminService: AdminMasterService) {
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('role', false);

    let aboutPreview = localStorage.getItem('preview');
    if (aboutPreview == 'preview') {
      this.isPreview = true;
    }
    let redirectFromAdmin = localStorage.getItem('redirectFromAdmin');
    if (redirectFromAdmin == 'true') {
      this.redirectFromAdmin = true;
    }
    let isCustomer = localStorage.getItem('isCustomer');
    if (isCustomer == 'true') {
      this.isCustomer = true;
    }
    this.route.queryParams.subscribe((data) => {
      this.isAdmin = data.admin;
      this.isTxnPreview = data.txnPreview;
    })

    this.route.params.subscribe((data) => {
      this.equipmentId = data.id;
      this.getDetails(this.equipmentId);
    })
    if (this.cognitoId) {
      this.getRecentlyViewed();
      let queryParams = 'buyer__cognito_id__in=' + this.cognitoId + '&equipment__id__in=' + this.equipmentId + '&bid_status__in=PA'
      this.getTradeBidData(queryParams);
    }
  }

  getTradeBidData(queryparams) {
    this.adminService.getTradeBidData(queryparams).subscribe((data: any) => {
      this.tradeBidData = data.results;
    });
  }

  getRecentlyViewed() {
    this.apiService.getRecentlyViewed(this.cognitoId).subscribe((res: any) => {
      this.recentlyViewedList = res.results;
    })
  }

  getDetails(id) {
    forkJoin([this.apiService.getBasicDetailListByCognito(id, this.cognitoId), this.apiService.getDocumentsByEquipmentId(id), this.apiService.getVisualsByEquipmentId(id, { limit: 999 })]).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.equipmentDetails = res[0];
        if (this.equipmentDetails?.seller?.cognito_id == this.cognitoId) {
          this.isSameUser = true;
        }
        this.documents = res[1].results;
        let tempVisuals = res[2].results.filter(f => { return (f.visual_type == 1) && (f.is_primary == true) });
        res[2].results.forEach(f => {
          if ((f.visual_type == 1) && (f.is_primary == false)) {
            tempVisuals.push(f);
          }
        });
        if (tempVisuals.length) {
          this.visualImageData = tempVisuals.map(element =>
            new ImageItem({ src: element?.url, thumb: element?.url })
          )
        }
        if (this.equipmentDetails && this.equipmentDetails?.primary_image) {
          let Obj = this.visualImageData.find(item => { return item.data?.src == this.equipmentDetails?.primary_image })
          if (!Obj) {
            this.visualImageData.push(new ImageItem({ src: this.equipmentDetails?.primary_image, thumb: this.equipmentDetails?.primary_image }))
          }
        }
        this.visualVideoData = res[2].results.filter(f => { return f.visual_type == 3 });

        this.documentArray = [];
        this.documents.forEach(element => {
          let doc = {
            docName: element.document_name,
            docUrl: element.doc_url
          }

          this.documentArray.push(doc);
        });
        this.changeDetect.detectChanges();
      },
      err => {
        console.log(err?.message);
      }
    )
  }

  getTechTitle(str,unit) {
    if(str == 'enginePower') {
      return str + ' cc ';
      } else if(str == 'operatingWeight') {
      return str + ' kg ';
      }
      else if(str == 'bucketCapcity') {
        return str +' kg ';
      } else if(str == 'grossWeight') {
        return str +' kg ';
      }else if(str == 'liftingCapcity') {
        return str +' kg ';
      }else {
        return str + ' ' +unit ;
      }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
                // this.notify.warn('Please submit KYC and Address proof', true);
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, returnUrl: this.router.url } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!', false);
        }
      }
      else {
        this.openSignInModal();
      }
    });
  }

  notifyMe() {
    let rolesArray = this.storage.getStorageData("rolesArray", false);
    rolesArray = rolesArray.split(',');
    if (this.cognitoId) {
      if (rolesArray.includes('Customer')) {
        let request = {
          new_equipment: null,
          used_equipment: this.equipmentId,
          type: 3,
          user: this.cognitoId
        }
        this.commonService.addToWatchlist(request).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.notify.success('Thanks for showing interest in the Asset!!', false);
            this.disabledNotify = true;
          },
          err => {
            console.log(err);
            this.notify.error(err?.message, false);
          }
        );
      } else {
        this.notify.error('Please login as Customer to proceed further Notify flow!!', false);
      }
    } else {
      this.openSignInModal();
    }
  }

  buyNow() {
    this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        let payload = {
          asset_id: this.equipmentId,
          cognito_id: this.cognitoId,
          source: 'Insta_sale',
          payment_type: 1
        }
        this.apiService.validateBid(payload).pipe(finalize(() => { })).subscribe(
          (data: any) => {
            if (!data?.redirect) {
              this.setTradeBids('Insta_sale', data?.redirect, data?.is_cooling_period);
            } else {
              if (data?.redirect) {
                if (data?.is_emd_available) {
                  let paymentDetails = {
                    asset_id: this.equipmentId,
                    asset_type: 'Used',
                    case: 'Insta_sale',
                    payment_type: 'Partial'
                  }
                  this.storage.setSessionStorageData('paymentSession', true, false);
                  this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                  this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                  this.router.navigate(['/customer/payment']);
                } else {
                  if (!data?.is_cooling_period) {
                    this.setTradeBids('Insta_sale', data?.redirect, data?.is_cooling_period);
                  }
                }
              }
            }
          },
          err => {
            console.log(err);
            this.notify.error(err.message, false);
          }
        )
      }
    });
  }

  placeBidFn() {
    this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        if (this.placeBid && this.placeBid != 0) {
          let payload = {
            amount: +this.placeBid,
            asset_id: this.equipmentId,
            cognito_id: this.cognitoId,
            source: 'Bid',
            payment_type: 1
          }
          this.apiService.validateBid(payload).pipe(finalize(() => { })).subscribe(
            (data: any) => {
              if (data?.higher_bid_available) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'Higher Bid Available',
                    message: 'A Higher bid is available for the asset would you like to submit the same bid',
                  }
                });
                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    if (!data?.redirect) {
                      this.setTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                    } else {
                      if (data?.redirect) {
                        if (data?.is_emd_available) {
                          let paymentDetails = {
                            asset_id: this.equipmentId,
                            asset_type: 'Used',
                            case: 'Bid',
                            payment_type: 'Partial'
                          }
                          this.storage.setSessionStorageData('paymentSession', true, false);
                          this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                          this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                          this.router.navigate(['/customer/payment']);
                        } else {
                          this.setTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                        }
                      }
                    }
                  }
                })
              } else {
                if (!data?.redirect) {
                  this.setTradeBids('Bid', false, false);
                } else {
                  if (data?.redirect) {
                    if (data?.is_emd_available) {
                      let paymentDetails = {
                        asset_id: this.equipmentId,
                        asset_type: 'Used',
                        case: 'Bid',
                        payment_type: 'Partial'
                      }
                      this.storage.setSessionStorageData('paymentSession', true, false);
                      this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                      this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                      this.router.navigate(['/customer/payment']);
                    } else {
                      this.setTradeBids('Bid', true, data?.is_cooling_period);
                    }
                  }
                }
              }
            },
            err => {
              console.log(err);
              this.notify.error(err.message, false);
            }
          )
        } else {
          if (this.placeBid == '' || this.placeBid == undefined || this.placeBid == 0) {
            this.notify.error('Please provide valid Bid Value and should be greater than 0!!', false);
          }
        }
      }
    });
  }

  getOfferStatus(type, redirect, coolingPeriod) {
    if (type == 'Bid') {
      if (redirect) {
        if (coolingPeriod) {
          return 'BR';
        } else {
          return 'BA';
        }
      } else {
        return 'BR';
      }
    } else {
      return 'PP';
    }
  }

  getBidStatus(type, redirect, coolingPeriod) {
    if (type == 'Bid') {
      if (redirect) {
        if (coolingPeriod) {
          return 'PA';
        } else {
          return 'SP';
        }
      } else {
        return 'PA';
      }
    } else {
      return 'SP';
    }
  }

  getDealStatus(type, redirect, coolingPeriod) {
    if (type == 'Bid') {
      if (redirect) {
        if (coolingPeriod) {
          return 'DP';
        } else {
          return 'CP';
        }
      } else {
        return 'DP';
      }
    } else {
      return 'CP';
    }
  }

  setTradeBids(type: any, redirect, coolingPeriod?) {
    let payload = {
      amount: (type == 'Insta_sale') ? this.equipmentDetails.selling_price : this.placeBid,
      equipment: this.equipmentId,
      buyer: this.cognitoId,
      offer_status: this.getOfferStatus(type, redirect, coolingPeriod),
      trade_type: (type == 'Insta_sale') ? 'IS' : 'BID',
      bid_status: this.getBidStatus(type, redirect, coolingPeriod),
      is_cooling_period: coolingPeriod,
      deal_status: this.getDealStatus(type, redirect, coolingPeriod)
    }
    this.paymentService.setTradeBids(payload).subscribe(
      (data: any) => {
        if (type == 'Bid') {
          this.notify.success('Bid submitted successfully!!', true);
          this.placeBid = '';
        }
        if (type == 'Insta_sale') {
          this.notify.success('Your asset purchase request is submitted successfully, Please make the full payment before due date.', true);
        }
        location.reload();
      },
      err => {
        console.log(err);
      }
    )
  }

  backToPrevious() {
    localStorage.removeItem('preview');
    if (this.isTxnPreview && this.isAdmin) {
      this.router.navigate(['/admin-dashboard/transactions']);
    } else if (this.isTxnPreview) {
      this.router.navigate(['/dashboard/my-transaction']);
    } else if (this.redirectFromAdmin) {
      localStorage.removeItem('redirectFromAdmin');
      this.router.navigate(['/admin-dashboard/products/used-products-upload'], { queryParams: { assetId: this.equipmentId, toEdit: true } });
    } else if (this.isCustomer) {
      localStorage.removeItem('isCustomer');
      this.router.navigate(['/customer/dashboard/used-products']);
    } else {
      this.router.navigate(['/used-equipment/visuals', this.equipmentId]);
    }
  }

  getEquipmentFuelType(val) {
    switch (val) {
      case 1: { return 'Diesel' }
      case 2: { return 'Petrol' }
      case 3: { return 'CNG' }
      default: { return 'Diesel' }
    }
  }

  addToWishlist(id) {
    if (this.cognitoId) {
      let request = {
        new_equipment: null,
        used_equipment: id,
        type: 2,
        user: this.cognitoId
      }
      this.commonService.addToWatchlist(request).pipe(finalize(() => { })).subscribe((res: any) => {
        this.getDetails(this.equipmentId);
        this.getRecentlyViewed();
      });
    } else {
      this.openSignInModal();
    }
  }

  removeFromWatchlist(id) {
    if (this.cognitoId) {
      let queryParam =
        'asset_type=' + 'used' +
        '&asset_id=' + id +
        '&cognito_id=' + this.cognitoId;
      this.commonService.removeFromWatchlist(queryParam).pipe(finalize(() => { })).subscribe((res: any) => {
        this.getDetails(this.equipmentId);
        this.getRecentlyViewed();
      });
    } else {
      this.openSignInModal();
    }
  }

  getTimeLeft(endDate) {
    const end_date = new Date(endDate);
    const current_date = new Date();
    if (current_date > end_date) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      };
    } else {
      let totalSeconds = Math.floor((end_date.getTime() - (current_date.getTime())) / 1000);
      let totalMinutes = Math.floor(totalSeconds / 60);
      let totalHours = Math.floor(totalMinutes / 60);
      let totalDays = Math.floor(totalHours / 24);

      let hours = totalHours - (totalDays * 24);
      let minutes = totalMinutes - (totalDays * 24 * 60) - (hours * 60);
      let seconds = totalSeconds - (totalDays * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

      return {
        days: totalDays,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }
  }

  getPrice(equipmentDetails) {
    if (equipmentDetails?.is_price_on_request) {
      return 'Price on Request';
    }
    else if (!equipmentDetails?.is_price_on_request && !equipmentDetails?.is_insta_sale) {
      //return '******';
      return '';
    } else if (equipmentDetails?.is_insta_sale) {
      return equipmentDetails?.selling_price;
    }
  }

  bidWithdraw() {
    this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
          width: '500px',
          disableClose: true,
          data: {
            heading: 'Withdrawn Bid',
            message: 'Are you sure you would like to withdraw this bid?'
          }
        });

        dialogRef.afterClosed().subscribe(val => {
          if (val) {
            const putWithDrawnBody = {
              amount: this.tradeBidData[0].amount,
              equipment: this.equipmentId,
              bid_status: 'CL',
              deal_status: 'CP',
              offer_status: 'BW'
            };
            this.adminService.puttradeBid(putWithDrawnBody, this.tradeBidData[0].id).subscribe((res: any) => {
              this.notify.success("Bid Withdrawn");
              dialogRef.close();
              location.reload();
            });
          }
        })
      }
    });
  }

  modifyBid() {
    this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        if (this.placeBid && this.placeBid != 0) {
          if (this.tradeBidData[0].amount > this.placeBid) {
            this.notify.error('You cannot place a bid lower than your last bid which was ' + this.tradeBidData[0].amount);
            return;
          }
          else {
            let payload = {
              bid_id: this.tradeBidData[0].id,
              amount: this.placeBid,
              asset_id: this.equipmentId,
              cognito_id: this.cognitoId,
              source: 'Bid',
              payment_type: 1
            }
            this.apiService.validateBid(payload).pipe(finalize(() => { })).subscribe(
              (data: any) => {
                if (data?.higher_bid_available) {
                  const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '500px',
                    disableClose: true,
                    data: {
                      heading: 'Higher Bid Available',
                      message: 'A Higher bid is available for the asset would you like to submit the same bid',
                    }
                  });
                  dialogRef.afterClosed().subscribe(val => {
                    if (val) {
                      if (!data?.redirect) {
                        this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                      } else {
                        if (data.redirect) {
                          if (data?.is_emd_available) {
                            if (this.tradeBidData[0]?.partial_payment?.length) {
                              this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                            } else {
                              let paymentDetails = {
                                asset_id: this.equipmentId,
                                asset_type: 'Used',
                                case: 'Bid',
                                payment_type: 'Partial',
                                bid_id: this.tradeBidData[0]?.id
                              }
                              this.storage.setSessionStorageData('paymentSession', true, false);
                              this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                              this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                              this.router.navigate(['/customer/payment']);
                            }
                          } else {
                            this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                          }
                        }
                      }
                    }
                  })
                } else {
                  if (!data?.redirect) {
                    this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                  } else {
                    if (data.redirect) {
                      if (data?.is_emd_available) {
                        if (this.tradeBidData[0]?.partial_payment?.length) {
                          this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                        } else {
                          let paymentDetails = {
                            asset_id: this.equipmentId,
                            asset_type: 'Used',
                            case: 'Bid',
                            payment_type: 'Partial',
                            bid_id: this.tradeBidData[0]?.id
                          }
                          this.storage.setSessionStorageData('paymentSession', true, false);
                          this.storage.setSessionStorageData('bidDetails', window.btoa(JSON.stringify(data)), false);
                          this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                          this.router.navigate(['/customer/payment']);
                        }
                      } else {
                        this.updateTradeBids('Bid', data?.redirect, data?.is_cooling_period);
                      }
                    }
                  }
                }
              },
              err => {
                console.log(err);
              }
            )
          }
        }
        else {
          if (this.placeBid == '' || this.placeBid == undefined || this.placeBid == 0) {
            this.notify.error('Please provide valid Bid Value and should be greater than 0!!', false);
          }
        }
      }
    });
  }

  updateTradeBids(type: any, redirect, coolingPeriod) {
    let payload = {
      amount: this.placeBid,
      equipment: this.equipmentId,
      buyer: this.cognitoId,
      offer_status: this.getOfferStatus(type, redirect, coolingPeriod),
      trade_type: 'BID',
      bid_status: this.getBidStatus(type, redirect, coolingPeriod),
      is_cooling_period: coolingPeriod,
      deal_status: this.getDealStatus(type, redirect, coolingPeriod)
    }
    this.paymentService.updateTradeBids(payload, this.tradeBidData[0]?.id).subscribe(
      (data: any) => {
        this.notify.success('Bid updated successfully!!', true);
        this.placeBid = '';
        location.reload();
      },
      err => {
        console.log(err);
      }
    )
  }

  openSignInModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        this.cognitoId = this.storage.getStorageData('cognitoId', false);
        this.userRole = this.storage.getStorageData('role', false);
      }
    })
  }
}
