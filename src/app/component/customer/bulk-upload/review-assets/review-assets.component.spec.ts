import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewAssetsComponent } from './review-assets.component';

describe('ReviewAssetsComponent', () => {
  let component: ReviewAssetsComponent;
  let fixture: ComponentFixture<ReviewAssetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewAssetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
