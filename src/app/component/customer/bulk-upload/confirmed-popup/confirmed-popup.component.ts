import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Component({
  selector: 'app-confirmed-popup',
  templateUrl: './confirmed-popup.component.html',
  styleUrls: ['./confirmed-popup.component.css']
})
export class ConfirmedPopupComponent implements OnInit {
  enterprise: any;
  userRole: string | null ='';
  constructor(public dialogRef: MatDialogRef<ConfirmedPopupComponent>,public apiRouteService: ApiRouteService, private activatedRoute: ActivatedRoute, public router: Router) { }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['enterprise'] != '') {
        this.enterprise = params['enterprise'];
      }
    });
    this.userRole = localStorage.getItem("userRole");
  }
  backClick(){
    this.dialogRef.close();
  }
  back(){
    if(this.enterprise)
    this.router.navigate(['/admin-dashboard/bulk-listing-enterprise'])
    else
    this.router.navigate(['/admin-dashboard/bulk-listing'])
  }
  backtoDb(){
    
  }
}
