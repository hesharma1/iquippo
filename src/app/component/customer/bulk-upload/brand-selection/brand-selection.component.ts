import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-brand-selection',
  templateUrl: './brand-selection.component.html',
  styleUrls: ['./brand-selection.component.css']
})
export class BrandSelectionComponent implements OnInit {

  public searchText: string = '';
  public selectedValue: any = null;
  public selectedArr: Array<any> = [];
  public dialogHeading: string = '';
  public isLoading: boolean = false;

  constructor(public dialogRef: MatDialogRef<BrandSelectionComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public cmnService: CommonService) { }

  ngOnInit(): void {
      this.dialogHeading = this.data.heading;
      this.selectedArr = this.data.listing;

    // forkJoin([this.cmnService.getCategoryMaster(), this.cmnService.getBrandMaster(), this.cmnService.getModelMaster()]).pipe(finalize(() => {
    //   this.dialogHeading = this.data.heading;
    //   this.isLoading = false;
    // })).subscribe(
    //   (res: any) => {
    //     this.setDialogeOptions(this.dialogHeading, res);
    //   },
    //   (err: any) => {
    //     console.log(err);
    //   }
    // )
  }

  backClick() {
    this.dialogRef.close();
  }

  setDialogeOptions(val, data) {
    switch (val) {
      case 'Category': {
        this.selectedArr = data[0].results;
        break;
      }
      case 'Brand': {
        this.selectedArr = data[1].results;
        break;
      }
      case 'Model': {
        this.selectedArr = data[2].results;
        break;
      }
    }
  }

  showRow(s) {
    if (s.name.includes(this.searchText) || this.searchText == '') {
      return true;
    } else {
      return false;
    }
  }

  closeDialog() {
    this.dialogRef.close({ event: 'close', data: this.selectedValue});
  }
}
