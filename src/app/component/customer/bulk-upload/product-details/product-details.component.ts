import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { BulkUploadService } from '../bulk-upload.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { CommonService } from 'src/app/services/common.service';
import { MatDialog } from '@angular/material/dialog';
import { BrandSelectionComponent } from '../brand-selection/brand-selection.component';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FlexAlignStyleBuilder } from '@angular/flex-layout';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  panelOpenState = false;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public steps = {
    current: 2,
    total: [
      { title: 'Upload Files' },
      { title: 'View Details' },
      { title: 'Completed' },
    ]
  }
  public mainData: Array<any> = [];
  public displayedColumns: string[] = ["sr_no","category","brand","model","engine_number","chassis_number","rc_number","mfg_year","selling_price","Last_Date_Of_Payment","hmr_kmr","pin_code","state","city","motor_operating_hours","mileage","product_condition","machine_sr_number","gross_Weight","operating_weight","bucket_capacity","engine_power","lifting_capacity","service_date","operating_hours","service_at_kms","authorized_station","engine_repaired","video_link","is_contact_displayed_in_frontend","contact_number","alt_contact_number","asset_address","visuals","customer_reference_number","variant","reserve_price","buyer_premium","special_offer","rate_equipment"
];
  // public displayedColumns: string[] = ['id', "category", "brand", "model", "status"];
  public dataSource;
  public dialogData: any;
  enterprise: boolean = false;
  userRole: any;

  constructor(public bulkService: BulkUploadService ,public apiPath:ApiRouteService, public storage:StorageDataService,private route:ActivatedRoute ,private datePipe: DatePipe,public notify:NotificationService ,public cmnService: CommonService, public router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.mainData = this.bulkService.getBulkUploadData();
    this.userRole = this.storage.getStorageData('userRole', false);
    // if (!this.mainData.length) {
    //   this.router.navigate(['/bulk-upload/upload-files']);
    // }
   if(this.userRole == this.apiPath.roles.superadmin || this.userRole == this.apiPath.roles.admin){
      //this.template_kit_url = "https://kyc-document-mp2.s3.ap-south-1.amazonaws.com/template/Bulk_Upload_Admin.zip";  
    }
    else{
      this.displayedColumns = ["sr_no","category","brand","model","engine_number","chassis_number","rc_number","mfg_year","selling_price","pin_code","state","city","motor_operating_hours","machine_sr_number","gross_Weight","operating_weight","bucket_capacity","engine_power","lifting_capacity","service_date","operating_hours","service_at_kms","contact_number","alt_contact_number","asset_address","visuals","customer_reference_number","product_condition"];
    }
   
    this.route.queryParams.subscribe(params=>{
      this.enterprise = params['enterprise'];
    })
    if(this.enterprise){
      this.displayedColumns = ["category","brand","model","asset_name","repo_date","distance_to_be_travelled","original_owner","customer_seeking_finance","reference_number","contact_person","contact_number","engine_number","chassis_number","rc_number","mfg_year","pin_code","state","city","machine_sr_number","contact_number","asset_address","customer_reference_number","invoice_date","invoice_value"];
    }
    this.dataSource = new MatTableDataSource<any>(this.mainData);
  }

  @HostListener('window:beforeunload', ['$event'])
  WindowBeforeUnoad($event: any) {
    $event.returnValue = 'Your data will be lost!';
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  checkDataStatus() {
    if (this.mainData) {
      this.mainData.find((x) => {
        if (x.is_uploaded == false) {
          return true;
        } else {
          return false;
        }
      })
    }
  }

  getDialogData(val, row) {
    if (!this.dialogData) {
      forkJoin([this.cmnService.getCategoryMaster(), this.cmnService.getBrandMaster(), this.cmnService.getModelMaster()]).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          this.dialogData = res;
          if (val == "Category") {
            this.openDialog(val, row, res[0]);
          } else if (val == "Brand") {
            this.openDialog(val, row, res[1]);
          } else {
            this.openDialog(val, row, res[2]);
          }
        },
        (err: any) => {
          console.log(err);
        }
      )
    } else {
      if (val == "Category") {
        this.openDialog(val, row, this.dialogData[0]);
      } else if (val == "Brand") {
        this.openDialog(val, row, this.dialogData[1]);
      } else {
        this.openDialog(val, row, this.dialogData[2]);
      }
    }
  }
  back(){
    if(this.enterprise)
    this.router.navigate(['/bulk-upload/bulk-upload-enterprise'])
    else
    this.router.navigate(['/bulk-upload/upload-files'])
  }
  openDialog(val, row, data) {
    const dialogRef = this.dialog.open(BrandSelectionComponent, {
      width: '500px',
      disableClose: true,
      data: {
        heading: val,
        listing: data.results
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.data) {
        if (val == "Category") {
          row.category = result.data;
          row.is_category_valid = true;
          this.checkRowStatus(row);
        } else if (val == "Brand") {
          row.brand = result.data;
          row.is_brand_valid = true;
          this.checkRowStatus(row);
        } else {
          row.model = result.data;
          row.is_model_valid = true;
          this.checkRowStatus(row);
        }
      }
      console.log(this.mainData);
    });
  }

  checkRowStatus(row) {
    if (row.is_category_valid && row.is_brand_valid && row.is_model_valid && row.status_code == 200) {
      row.is_uploaded = true;
    }
  }

  runImporter() {
    const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
      width: '650px',
      data: {
        message: 'Are you sure you want to submit this bulk upload request?',
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if(this.enterprise){
          this.cmnService.postBulkUploadEnterprise(this.mainData).subscribe(
            data => {
              console.log(data);
              this.router.navigate([`bulk-upload/confirmed`],{queryParams:{enterprise:true}})
              this.notify.success("Bulk Upload Successful");
            },
            err => {
              console.log(err);
              this.notify.error("Bulk Upload failed : "+ err?.message);
            }
          )
        }
        else{
        this.cmnService.postBulkUpload(this.mainData).subscribe(
          data => {
            console.log(data);
            localStorage.removeItem("adminBulkUpload");
            this.router.navigate([`bulk-upload/confirmed`])
            this.notify.success("Bulk Upload Successful");
          },
          err => {
            console.log(err);
            this.notify.error("Bulk Upload failed : " + err?.message);
          }
        )
        }
      }
      else {
      
      }
    })
    
    // this.mainData.forEach(element => {
    //   element.service_date = this.datePipe.transform(element.service_date,'yyyy-MM-dd')
    // }); 
    
  }

  confirmedDialogue() { }

}
