import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { CommonService } from '../../../../services/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as XLSX from 'xlsx';
import { BulkUploadService } from '../bulk-upload.service';
import { environment } from 'src/environments/environment';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

import {
  HttpClient
} from '@angular/common/http';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.css']
})
export class UploadFilesComponent implements OnInit, OnDestroy {
  @ViewChild('fileInput') fileInput!: ElementRef;
  cognitoId:any;
  SellerCogIdCaseAdmin : any;
  isSellerDetailOpen : boolean = false;
  public steps = {
    current: 1,
    total: [
      { title: 'Upload Files' },
      { title: 'View Details' },
      { title: 'Completed' },
    ]
  }
  public isNew: any;
  public payload:Array<any> = [];
  excelName: string="";
  zipName: string="";
  isAdminBulkUpload : any;
  sellerList: any = [];
  sellerName : any;
  SellerMobileNo : any;
  SellerEmailId : any;
  disablesubmit: boolean = false;
  sellerType: any;
  msg: any = "Please enter more than 2 characters for search.";
  searchByMobile = new FormControl('');
  pricingModel = new FormControl('',Validators.required);
  portal_sale = new FormControl(true);
  insta_sale = new FormControl(false);
  auction = new FormControl(false);
  userRole: any;
  template_kit_url:any;
  sellerTypeId: number = 1;

  constructor(public cmnService: CommonService,
    public spinner:NgxSpinnerService ,
    public route: ActivatedRoute, 
    public apiPath:ApiRouteService,
    public router:Router,
    public bulkService:BulkUploadService, 
    public http:HttpClient,
    public s3: S3UploadDownloadService,
    public storage:StorageDataService,
    private notify:NotificationService,
    private usedEquipmentService: UsedEquipmentService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.isNew = this.route.snapshot.queryParamMap.get('isNew');
    this.userRole = this.storage.getStorageData('userRole', false);
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    if(this.userRole == this.apiPath.roles.superadmin || this.userRole == this.apiPath.roles.admin){
      
      this.template_kit_url = "https://iquippo-marketplace-uat-mp2.s3.ap-south-1.amazonaws.com/assets/template/Bulk_Upload_Admin.zip";  
    }
    else{
      this.template_kit_url = "https://iquippo-marketplace-uat-mp2.s3.ap-south-1.amazonaws.com/assets/template/Bulk_Upload_Customer.zip";
    }
    if(this.storage.getStorageData("adminBulkUpload", false)==null){
      this.isAdminBulkUpload = false
    }
    else{
      this.isAdminBulkUpload = this.storage.getStorageData("adminBulkUpload", false);
    }
    if(this.userRole == this.apiPath.roles.customer){
      this.sellerTypeId =1;
    }
    else if(this.userRole == this.apiPath.roles.dealer){
      this.sellerTypeId =2;
    }
    else if(this.userRole == this.apiPath.roles.manufacturer){
      this.sellerTypeId =3;
    }
  }

  searchSeller(e) {
    if(this.sellerType!='' && this.sellerType!=undefined){
    if(this.searchByMobile.value.length>2){
      console.log(e.target.value);
    // let queryParams =
    //   'first_name__icontains=' +
    //   e.target.value +
    //   '&groups__name__iexact=Customer' +
    //   '&limit=999';
      let payload={
        "number": e.target.value,
        "source":"bulk_upload",
        "role":this.sellerType
      }
    this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
      (res: any) => {
        if(res?.data!=undefined)
        this.sellerList = res.data;
        else{
        this.sellerList = [];
      this.msg = res.message;  
      }
        console.log(this.sellerList)
      },
      (error) => {
        const msg = 'Somthing went wrong. Please try again!';
        const action = 'Error';
        this.sellerList = [];
       // this.notify.error(msg);
      }
    );
    }
    else{
      this.sellerList = [];
      this.msg ="Please enter more than 2 characters for search."
    }
    }
    else{
      this.notify.error('Please select type');
    }
  }
  keyPress(event: any) {
    const pattern = /[A-Za-z0-9\+\-\ ]/;

    // let inputChar = String.fromCharCode(event.keyCode);
    // if (event.keyCode != 8 && !pattern.test(inputChar)) {
    //   event.preventDefault();
    // }
    // else{
      this.searchSeller(event);
    // }
  }
  updateSellerType(type){
    this.sellerType=type.value; 
    if(this.sellerType.toLowerCase()==this.apiPath.roles.customer.toLowerCase())
    this.sellerTypeId = 1;
    if(this.sellerType.toLowerCase()==this.apiPath.roles.dealer.toLowerCase())
    this.sellerTypeId = 2;
  }
  onSellerSelect(search: any) {
    this.SellerCogIdCaseAdmin = search;
    this.sellerList.filter((res) => {
      if (res.cognito_id == search) {
        this.isSellerDetailOpen = true;
        this.sellerName = res.first_name + ' ' + res.last_name ;
        this.SellerEmailId = res.email;
        this.SellerMobileNo = res.mobile_number
      }
    });
  }


 onFileChangeZip(event: any, tab: string, side: string) {
    
      const file = event.target.files[0];
      
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async () => {
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
        if(fileExt=="zip"){

        
      // this.zipName=file?.name;
      var image_path = environment.bucket_zip_folder +
        '/' +
        this.cognitoId +
        '.' +
        fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      this.extractZip(event.target.files[0])
        }
        else{
          this.notify.error("Please use the correct file format for uploading.")
        }
    }
   
  }
  convert(str) {
    if(str!=undefined && str!=''){
     
    var date = new Date(str);
    date.setDate(date.getDate() + 1)
     var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
     var day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
    }
    else{
      return '';
    }
  }
  extractZip(file:any) {
    //this.spinner.show();
    let fileExtension:string = file.name.split('?')[0].split('.').pop();
    let fileName = file.name;
    fileName = this.cognitoId //get name from form for example
    let data = fileName + '.' + fileExtension;
    let object= {};
    object = {
      filename: data
    }
    this.zipName=file?.name;
    this.http.post<any>('https://ufq8f1bitc.execute-api.ap-south-1.amazonaws.com/dev/unzip-files',object).subscribe(data => {
    console.log(data);
    console.log(this.payload);
    this.payload.forEach(element => {
      element.visuals=[];
      data.filekeyList.forEach(img => {
        var imgUrl = img;
        var img_name = imgUrl.substring(imgUrl.lastIndexOf("/") + 1, imgUrl.length)
        var imgSrNo = '';
        if(img_name.substring(0,img_name.indexOf("_"))!=''){
          imgSrNo = img_name.substring(0,img_name.indexOf("_"))
        }
        else{
          imgSrNo = img_name.substring(0,img_name.indexOf("."))
        }
        if(imgSrNo==element.sr_no){
          element.visuals.push(img);
        }
      });
      console.log(this.payload);
       
    });
  })
  }
  updatePricingModel(name:any){
    if(name=="Portal")
    {
      this.portal_sale.setValue(true)
      this.insta_sale.setValue(false)
      this.auction.setValue(false)
    }
    else if(name=="insta")
    {
      this.insta_sale.setValue(true)
      this.portal_sale.setValue(false)
      this.auction.setValue(false)
    }
    else
    {
      this.auction.setValue(true)
      this.insta_sale.setValue(false)
      this.portal_sale.setValue(false)
    }
  }
  onFileChange(event: any) {
  if(this.isAdminBulkUpload == "true"){
    if(this.pricingModel.value=='')
    {
      this.notify.error("Please select Disposal Method.")
      this.fileInput.nativeElement.value = "";
      return;
    }
    if(this.sellerName=="" || this.sellerName==undefined){
      this.notify.error("Please select seller.")
      this.fileInput.nativeElement.value = "";
      return;
    }
  } 
    /* wire up file reader */
    //this.spinner.show();
    const target: DataTransfer = <DataTransfer>(event.target);
    var file=event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      if(fileExt=="xlsx" || fileExt=="xlsm" || fileExt=="xlsb" || fileExt=="xltx" ){

    this.excelName = file.name;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary',cellDates: true, dateNF: 'yyyy/mm/dd;@' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      if(wsname=="Bulk_Upload_Template"){

      
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      const data = XLSX.utils.sheet_to_json(ws,{raw:true,dateNF:'yyyy/mm/dd'}); // to get 2d array pass 2nd parameter as object {header: 1}

      //for checking header columns should same 
      let header:any = XLSX.utils.sheet_to_json(ws,{header:1})[0];

      //header?.forEach(element => {
        for (let index = 0; index < header.length; index++) {
          const element = header[index];
          if(this.apiPath.bulkUpload?.indexOf(element)==-1){
             
            this.notify.error("Please use the correct file format for uploading");
            this.fileInput.nativeElement.value = "";
            this.excelName="";
            return ;
          }
        }
    //  });
        

      if (data != null && data.length > 5) {
        if(data.length > 10 && !(this.userRole == this.apiPath.roles.superadmin || this.userRole == this.apiPath.roles.admin)){
           
          this.disablesubmit = true; 
          this.excelName ='';
          this.fileInput.nativeElement.value = "";
          this.notify.error("Maximum 5 records are allowed.");
         return ;
         
        }
        var temp = data.map((t: any) => {
          if(t.__rowNum__<2050){
            if(t["Sr.No.*"]==undefined){
              this.notify.error("Please fill the data in Sr.No. field and upload the excel again.");
              this.disablesubmit = true; 
              this.fileInput.nativeElement.value = "";
              this.excelName ='';
              return {};
            }   
            if(t["Video_Link"]!=undefined){
              var expression = /(http|https)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
              var regex = new RegExp(expression);
              var url = t["Video_Link"];
              if (url.match(regex)) {
                //alert("Successful match");
               
              }
              else{
                 this.notify.error("Please enter this url ("+url+") in Valid format. Valid Format is : https://www.google.com or http://www.google.com");
                this.disablesubmit = true; 
                this.fileInput.nativeElement.value = "";
                this.excelName ='';
                return {};
              } 
             
            }       
          if(t["Category*"]==undefined){
            this.notify.error("Please fill the data in Category field and upload the excel again.");
            this.disablesubmit = true; 
            this.fileInput.nativeElement.value = "";
            this.excelName ='';
            return {};
          }
          else if(t["Brand*"]==undefined){
            this.notify.error("Please fill the data in Brand field and upload the excel again.");
            this.disablesubmit = true; 
            this.fileInput.nativeElement.value = "";
             this.excelName ='';
            return {};
          }
          else if(t["Model*"]==undefined){
            this.notify.error("Please fill the data in Model field and upload the excel again.");
            this.disablesubmit = true; 
            this.fileInput.nativeElement.value = "";
             this.excelName ='';
            return {};
          }
         
          else if(t["Engine_No"]==undefined && t["Registration_No"]==undefined && t["Machine_Serial_No"]==undefined && t["Chassis_No"]==undefined){
            this.notify.error("Please fill the data in one of these fields (Engine_No, Registration_No, Machine_Serial_No,Chassis_No) and upload the excel again.");
            this.disablesubmit = true; 
             this.excelName ='';
             this.fileInput.nativeElement.value = "";
            return {};
          }
          else if(t["Pin_Code"]==undefined && (t["State"]==undefined || t["City"]==undefined)){
            this.notify.error("Please fill the data in Pin_Code field or (State & City) Field and upload the excel again.");
            this.disablesubmit = true; 
             this.excelName ='';
             this.fileInput.nativeElement.value = "";
            return {};
          }
       
          else if(t["Sale_Price*"]==undefined){
            this.notify.error("Please fill the data in Sale_Price field and upload the excel again.");
            this.disablesubmit = true; 
             this.excelName ='';
             this.fileInput.nativeElement.value = "";
            return {};
          }
          else{
            this.disablesubmit = false;
          var obj:any =  {
            //"id": t["Asset_ID*"],
            "sr_no":t["Sr.No.*"],
            "category": t["Category*"],
            //"other_category": t["Category*"] == "Other" ? t["Other_Category*"] : t["Category*"],
            "brand": t["Brand*"],
            //"other_brand": t["Brand*"] == "Other" ? t["Other_Brand*"] : t["Brand*"],
            "model": t["Model*"],
            "engine_number": t["Engine_No"],
            "chassis_number": t["Chassis_No"],
            "rc_number": t["Registration_No"],
            "mfg_year": t["Year_of_Manufacture"],
            "selling_price": t["Sale_Price*"],
            "pin_code": t["Pin_Code"],
            "motor_operating_hours": t["Motor_Operating_Hours"],
            "mileage": t["Mileage"],
            // "product_condition": t["Product_Condition"],
            "machine_sr_number": t["Machine_Serial_No"],
            "gross_Weight": t["Gross_Weight"],
            "operating_weight": t["Operating_Weight"],
            "bucket_capacity": t["Bucket_Capacity"],
            "engine_power": t["Engine_Power"],
            "lifting_capacity": t["Lifting_Capacity"],
            "operating_hours":t["Operating_Hours"],
             "service_at_kms":t["Service_at_KMs"],
             "authorized_station":t["Authorized_Station"]=="Yes"?true:false,
            "engine_repaired": t["Engine_Repaired_Overhauliyng"]=="Yes"?true:false,
             "video_link":t["Video_Link"],
            "is_contact_displayed_in_frontend": t["is_contact_displayed_in_frontend"]=="Yes"?true:false,
            "contact_number": t["Contact_Number"],
            "alt_contact_number": t["Alternate_Contact_Number"],
            "asset_address": t["Address_Of_Asset"],
            "seller": this.isAdminBulkUpload == "true" ? this.SellerCogIdCaseAdmin : this.cognitoId,
            "updated_by" : this.cognitoId,
            "is_online_bidding":this.portal_sale.value,
            "is_insta_sale":this.insta_sale.value,
            "is_auction":this.auction.value,
            "seller_type":this.sellerTypeId,
            "hmr_kmr":t["HMR/KMR"],
           

            // "id": t["Asset_ID*"],
            // "category": t["Category*"],
            // "other_category": t["Category*"] == "Other" ? t["Other_Category*"] : t["Category*"],
            // "brand": t["Brand*"],
            // "other_brand": t["Brand*"] == "Other" ? t["Other_Brand*"] : t["Brand*"],
            // "model": t["Model*"],
            // "other_model": t["Model*"] == "Other" ? t["Other_Model*"] : t["Model*"],
            // // "location":t["Location*"],
            // // "other_location": t["Location*"] == "Other" ? t["Other_Location*"] : t["Location*"],
            // "pin_code": t["Pin_Code"],
            // // "char_field":t["Sub_Category"],
            // "engine_number": t["Engine_No"],
            // "chassis_number": t["Chassis_No"],
            // "rc_number": t["Registration_No"],
             "customer_reference_number": t["Customer_Reference_No"],
             "variant": t["Variant"],
            // "trade_type": t["Trade_Type(SELL/NOT_AVAILABLE)*"],
            // "mfg_year": t["Manufacturing_Year*"],
            // "is_price_on_request": t["Price_On_Request(Yes/No)*"].toLowerCase()=="yes"?true:false,
            // "selling_currency": t["Currency*"],
            // "selling price": t["Sale_Price*"],
            // "country": t["Country*"],
             "state": t["State"],
             "city": t["City"],
            // "seller": this.cognitoId,
            //  "rm_name":t["RM_Name*"],
            // "motor_operating_hours": t["Motor_Operating_Hours"],
            // "mileage": t["Mileage"],
            // "product_condition": t["PrOdishauct_Condition"],
            // "machine_sr_number": t["Machine_Serial_No"],
            // "gross_Weight": t["Gross_Weight"],
            // "operating_weight": t["Operating_Weight"],
            // "bucket_capacity": t["Bucket_Capacity"],
            // "engine_power": t["Engine_Power"],
            // "lifting_capacity": t["Lifting_Capacity"],
            // // "":t["Service_Date"],
            // // "operating_hours":t["Operating_Hours"],
            // // "":t["Service_at_KMs"],
            // // "":t["Authorized_Station"],
            // "engine_repaired": t["Engine_Repaired_Overhauling"],
            // "comments": t["Comments"],
            // "is_featured": t["Featured"],
             "product_condition": t["Product_Condition(Average/Good/Excellent)"],
            "special_offer": t["Special_Offers"],
            "buyer_premium": t["Buyer_Premium"],
            // // "":t["Video_Link"],
            // "asset_status": t["Asset_Status(listed/sold)"],
            // "seller_visible": t["To_be_shown_under_PrOdishauct_Information(Yes/No)"],
            // "is_contact_displayed_in_frontend": t["Contact_Number_to_be_displayed_in_front_end(Yes/No)"],
            // "alt_contact_number": t["Alternate_Contact_Number"],
            // "is_alt_contact_displayed_in_frontend": t["Alternate_Contact_Number_To_be_displayed_in_front_end(Yes/No)"],
            // // "":t["Featured(Yes/No)"],
            // "is_active": t["Active(Yes/No)"],
            // "is_auction": t["List_for_Auctions(Yes/No)"],
            // // "":t["Auction_ID"],
            // // "":t["EMD_Amount"],
            // "valuation_request": t["Request_for_Valuation(Yes/No)"],
            // // "":t["Name_of_Agency"],
            // "parked_since": t["Parked_Since"],
            // "valuation_amount": t["Valuation_Amount"],
            // "parking_payment_details": t["Parking_Charge_Payment_To(Yard/Seller)"],
            // "parking_charge_per_day": t["Parking_Charge_Per_Day"],
            // "asset_address": t["Address_Of_Asset"],
            //  "reserve_price": t["Reserve_Price"],
            // "source": 1
          }
          if(t["Service_Date"]!=undefined && t["Service_Date"]!=''){
          obj.service_date = this.convert(t["Service_Date"]);
          }
          if(this.insta_sale.value && t["Last_Date_Of_Payment"]!=undefined && t["Last_Date_Of_Payment"]!='')
          obj.last_date_insta_payment = this.convert(t["Last_Date_Of_Payment"]);
        
          if(this.portal_sale.value && t["Last_Date_Of_Payment"]!=undefined && t["Last_Date_Of_Payment"]!='')
          obj.last_date_bid_payment = this.convert(t["Last_Date_Of_Payment"]);

          return obj;
          }
          }
          else{
            return {};
          }
        })
        this.payload = temp.filter(value => Object.keys(value).length !== 0);
         
     
      }
      else{
       
      this.excelName="";
      this.fileInput.nativeElement.value = "";
        this.notify.error("Please enter the data in the excel and then upload excel file.");
      }
      }
      else{
         
        this.excelName="";
        this.fileInput.nativeElement.value = "";
        this.notify.error("Please use the correct file format for uploading");
      }
      
    };
    }
    else{
       
      this.excelName="";
      this.fileInput.nativeElement.value = "";
      this.notify.error("Please use the correct file format for uploading.")
    }
  }

  submitData(){
   // this.cmnService.postBulkUpload(this.payload).subscribe(
   //   data => {
     if(this.isAdminBulkUpload == 'true'){
       if(this.sellerName=="" || this.sellerName==undefined){
         this.notify.error("Please select seller.")
         return;
       }
     }
        this.fileInput.nativeElement.value = "";
        this.router.navigate(['/bulk-upload/product-details']);
        this.bulkService.setBulkUploadData(this.payload);
      // },
      // err => {
      //   console.log(err);
      // }
    // )
  }

  ngOnDestroy(){
    // localStorage.removeItem("adminBulkUpload");
  }

}
