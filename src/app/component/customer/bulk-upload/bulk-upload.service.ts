import { Injectable } from '@angular/core';
import { of, Subject } from 'rxjs';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BulkUploadService {

  public bulkUploadData;

  constructor(public httpservice: HttpClientService, private apiPath: ApiRouteService) { }

  setBulkUploadData(data:any){
    this.bulkUploadData = data;
  }

  getBulkUploadData(){
    return this.bulkUploadData;
  }
  getBulkUploadList(queryParams: string){
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.bulkUploadList}?${queryParams}`, environment.productBaseUrl)
  }
  getBulkUploadEnterpriseValuationList(queryParams: string){
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.bulkUploadEnterpriseList}?${queryParams}`, environment.instaValBaseUrl)
  }
  getBulkUploadCurationById(id: string,payload:any,enterprise?:any){
    if(enterprise)
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.bulkUploadEnterpriseCuration}${id}`, environment.instaValBaseUrl,payload)
    else
    return this.httpservice.HttpGetRequestCommon(`${this.apiPath.bulkUploadCuration}${id}`, environment.productBaseUrl,payload)
  }
  deleteBulkUpload(queryParams: string){
    return this.httpservice.HttpDeleteRequestCommon(`${this.apiPath.bulkUploadCuration}${queryParams}`, environment.productBaseUrl)
  }
  deleteBulkUploadValuation(queryParams: string){
    return this.httpservice.HttpDeleteRequestCommon(`${this.apiPath.bulkUploadEnterpriseCuration}${queryParams}`, environment.advancedValBaseUrl)
  }
  addToMaster(req:any){
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkAddToMaster,environment.bulkUploadBaseUrl)
  }
  addToDictionary(req:any){
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkAddToDictionary,environment.bulkUploadBaseUrl)
  }
  discardRecords(req:any){
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkDiscardRecords,environment.bulkUploadBaseUrl)
  }
  bulkImportData(req:any,enterprise?:any){
    if(enterprise)
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkImportEnterpriseData,environment.instaValBaseUrl)
    else
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkImportData,environment.bulkUploadBaseUrl)
  }
  UpdateValue(req:any){
    return this.httpservice.HttpPostRequestCommon(req,this.apiPath.bulkUpdateValue,environment.bulkUploadBaseUrl)
  }
}
