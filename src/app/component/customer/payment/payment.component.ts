import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ElementRef, Inject, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize, map } from 'rxjs/operators';
import { SharedService } from 'src/app/services/shared-service.service';
import { PaymentService } from 'src/app/services/payment.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { forkJoin } from 'rxjs';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { AuctionService } from 'src/app/services/auction.service';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { TermsAndConditionsComponent } from 'src/app/component/signup/terms-and-conditions/terms-and-conditions.component';
import { MatDialog } from '@angular/material/dialog';
import { ViewAllIdsComponent } from '../../shared/view-all-ids/view-all-ids.component';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit, OnDestroy {
  @ViewChild('imageInput') imageInput!: ElementRef;

  public paymentForm!: FormGroup;
  public equipmentId!: string;
  public equipmentDetails: any;
  public isAgree: boolean = false;
  public cognitoId!: any;
  public isNew: boolean = false;
  public paymentDetails: any;
  public gstDetails!: any;
  public bidDetails!: any;
  public instaDetails!: any;
  public bookingDetails!: any;
  public tradeBidDetails!: any;
  public tradeInstaDetails!: any;
  public instaValuationDetails!: any;
  private unique_control_number!: any;
  public advanceValuationDetails!: any;
  public auctionDetails: any;
  public auctionLotDetails: any;
  public emdValues: Array<any> = [];
  public selectedAuctionEmd: Array<any> = [];
  public paymentMethod: any = 1;
  public walletBalance: number = 0;
  public auctionFinalAmount: any;
  public proceedClicked: boolean = false;
  public description: any;
  public paymentModeOptions = [
    { name: 'Cash', value: 'Cash' },
    { name: 'Cheque', value: 'Cheque' },
    { name: 'Demand Draft', value: 'Demand Draft' },
    { name: 'RTGS', value: 'RTGS' },
    { name: 'NEFT', value: 'NEFT' }
  ]


  constructor(private fb: FormBuilder, public route: ActivatedRoute, public router: Router, private sharedService: SharedService, public apiService: UsedEquipmentService, public notify: NotificationService, public spinner: NgxSpinnerService, public storage: StorageDataService, public paymentService: PaymentService, public newEquipmentService: NewEquipmentService, private valuationService: ValuationService, private auctionService: AuctionService, public changeDetect: ChangeDetectorRef, public s3: S3UploadDownloadService, public dialog: MatDialog,
    @Inject(DOCUMENT) private document: Document) {
    this.paymentForm = this.fb.group({
      payment_mode: ['', Validators.required],
      bank_name: ['', Validators.required],
      branch: ['', Validators.required],
      proof_of_payment: ['', [Validators.required]],
      bank_ref_no: ['', [Validators.required]],
      dd_in_favour_of: ['']
    })
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    let paymentSession = this.storage.getSessionStorageData('paymentSession', false);
    this.paymentDetails = this.storage.getSessionStorageData('paymentDetails', false) ? JSON.parse(window.atob(this.storage.getSessionStorageData('paymentDetails', false))) : null;
    if (this.paymentDetails && (paymentSession == "true")) {
      for (let x in this.paymentDetails) {
        if ((this.paymentDetails[x] == undefined) || this.paymentDetails[x] == null) {
          this.clearStorage();
          this.notify.error('Unauthorised Access!!', true);
          this.router.navigate(['/home']);
        }
      }
      this.getDetailsFor();
    } else {
      this.clearStorage();
      this.notify.error('Unauthorised Access!!', true);
      this.router.navigate(['/home']);
    }
  }

  getDetailsFor() {
    switch (this.paymentDetails?.asset_type) {
      case 'New': {
        this.isNew = true;
        this.bookingDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('bookingDetails', false)));
        this.getNewDetails();
        break;
      }
      case 'Used': {
        if (this.paymentDetails?.case != 'Auction') {
          this.getUsedDetails();
        }
        if (this.paymentDetails?.case == 'Bid') {
          if (this.paymentDetails?.payment_type == 'Partial') {
            this.bidDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('bidDetails', false)));
          } else if (this.paymentDetails?.payment_type == 'Full') {
            this.tradeBidDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('tradeBidDetails', false)));
          }
        } else if (this.paymentDetails?.case == 'Insta_sale') {
          if (this.paymentDetails?.payment_type == 'Partial') {
            this.instaDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('bidDetails', false)));
          } else if (this.paymentDetails?.payment_type == 'Full') {
            this.tradeBidDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('tradeBidDetails', false)));
          }
        } else if (this.paymentDetails?.case == 'Insta_valuation') {
          this.unique_control_number = this.storage.getSessionStorageData('uniqueControlNumber', false);
          this.instaValuationDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('InstaValuation', false)));
        } else if (this.paymentDetails?.case == 'Advance_valuation') {
          this.unique_control_number = this.storage.getSessionStorageData('uniqueControlNumber', false);
          this.advanceValuationDetails = JSON.parse(window.atob(this.storage.getSessionStorageData('AdvanceValuation', false)));
        } else if (this.paymentDetails?.case == 'Auction') {
          this.getAuctionDetails(this.paymentDetails?.auction_id);
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  getUsedDetails() {
    let gstPayload = {
      is_active: true,
      type: 1
    }
    forkJoin([this.apiService.getBasicDetailList(this.paymentDetails?.asset_id), this.paymentService.getGstRate(gstPayload)]).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.equipmentDetails = res[0];
        this.gstDetails = res[1]?.results[0];
      },
      err => {
        console.log(err);
      }
    );
  }

  getNewDetails() {
    let gstPayload = {
      is_active: true,
      type: 1
    }
    forkJoin([this.newEquipmentService.getBasicDetailList(this.paymentDetails?.asset_id), this.paymentService.getGstRate(gstPayload)]).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.equipmentDetails = res[0];
        this.gstDetails = res[1]?.results[0];
      },
      err => {
        console.log(err);
      }
    );
  }

  getStatement() {
    if (this.isNew) {
      return 'Your Booking Value for your New Asset';
    } else {
      if (this.paymentDetails?.case == 'Bid') {
        if (this.paymentDetails?.payment_type == 'Partial') {
          return 'Your Security deposit for your Bid';
        } else {
          return 'Your Final deposit for your Bid';
        }
      } else if (this.paymentDetails?.case == 'Insta_sale') {
        return 'Your Amount value for your Asset';
      } else if (this.paymentDetails?.case == 'Insta_valuation') {
        return 'Your Amount value for Insta Valuation';
      } else if (this.paymentDetails?.case == 'Advance_valuation') {
        return 'Your Amount value for Advance Valuation';
      } else if (this.paymentDetails?.case == 'Auction') {
        if (this.paymentDetails?.payment_type == 'Partial') {
          return 'Select your EMD for the Auction Registration';
        } else {
          return 'Your Final deposit for Auction Payment';
        }
      } else {
        return '';
      }
    }
  }

  getSecurityDeposit() {
    if (this.isNew) {
      return parseInt(this.bookingDetails?.bookValue);
    } else {
      if (this.equipmentDetails && (this.paymentDetails?.case == 'Bid')) {
        if (this.paymentDetails?.payment_type == 'Partial') {
          if (this.bidDetails && this.bidDetails?.is_emd_available) {
            return this.bidDetails?.emd_amount;
          } else {
            return 0;
          }
        } else {
          //return (this.tradeBidDetails?.amount_to_be_paid + this.tradeBidDetails?.buyer_premium + this.tradeBidDetails?.parking + this.tradeBidDetails?.invoice_name_change_fee);
          return (this.tradeBidDetails?.amount_to_be_paid);

        }
      } else if (this.equipmentDetails && (this.paymentDetails?.case == 'Insta_sale')) {
        if (this.paymentDetails?.payment_type == 'Partial') {
          if (this.instaDetails && this.instaDetails?.is_emd_available) {
            return this.instaDetails?.emd_amount;
          } else {
            return 0;
          }
        } else {
          return (this.tradeBidDetails?.bid_amount - this.tradeBidDetails?.amount_paid);
          // return (this.tradeBidDetails?.amount_to_be_paid );
        }
      } else if (this.paymentDetails?.case == 'Insta_valuation') {
        return parseInt(this.instaValuationDetails.valuation_fee);
      } else if (this.paymentDetails?.case == 'Advance_valuation') {
        return parseInt(this.advanceValuationDetails.valuation_fee);
      } else if (this.paymentDetails?.case == 'Auction') {
        if (this.paymentDetails?.payment_type == 'Partial') {
          if (this.selectedAuctionEmd && this.selectedAuctionEmd?.length) {
            let amount = 0;
            for (var i = 0; i < this.selectedAuctionEmd.length; ++i) {
              amount += (+this.selectedAuctionEmd[i].amount);
            }
            return amount;
          } else {
            return 0;
          }
        } else {
          return this.tradeBidDetails?.amount_to_be_paid;
        }
      } else {
        return 0;
      }
    }
  }

  getGstApplicable(type?) {
    if (this.paymentDetails?.case == 'Insta_valuation' || this.paymentDetails?.case == 'Advance_valuation') {
      let fee = 0;
      fee = this.paymentDetails?.case == 'Insta_valuation' ? this.instaValuationDetails.valuation_fee : this.advanceValuationDetails.valuation_fee;
      let gstDetails = this.paymentDetails?.case == 'Insta_valuation' ? this.instaValuationDetails : this.advanceValuationDetails;
      if (type == 'igst') {
        return parseInt((fee * (gstDetails.igst / 100)).toString());
      } else if (type == 'cgst') {
        return parseInt((fee * (gstDetails.cgst / 100)).toString());
      } else if (type == 'sgst') {
        return parseInt((fee * (gstDetails.sgst / 100)).toString());
      } else if (type == 'total') {
        let gst = 0;
        if (gstDetails.is_igst) {
          gst = gst + gstDetails.igst
        }
        if (gstDetails.is_sgst) {
          gst = gst + gstDetails.sgst
        }
        if (gstDetails.is_cgst) {
          gst = gst + gstDetails.cgst
        }
        return parseInt((fee * (gst / 100)).toString());
      }
    }
    return 0;
  }

  proceedPayment() {
    if (this.isNew) {
      this.generateOrderId();
    } else {
      if ((this.paymentDetails?.case == 'Bid') || (this.paymentDetails?.case == 'Insta_sale') || (this.paymentDetails?.case == 'Insta_valuation') || (this.paymentDetails?.case == 'Advance_valuation')) {
        this.generateOrderId();
      } else if (this.paymentDetails?.case == 'Auction') {
        if (this.paymentDetails?.payment_type == 'Partial') {
          if (this.selectedAuctionEmd.length) {
            this.validateAuctionPayment();
          } else {
            this.notify.error('Please select atleast one emd for payment!!');
          }
        } else {
          this.generateAuctionOrderId();
        }
      }
    }
  }

  getUserRole() {
    let userRole = this.storage.getLocalStorageData('userRole', false);
    switch (userRole) {
      case 'Customer': {
        return 1
        break;
      }
      case 'Dealer': {
        return 2
        break;
      }
      case 'Manufacturer': {
        return 3
        break;
      }
      default: {
        return 1
        break;
      }
    }

  }

  generateOrderId() {
    if ((this.getSecurityDeposit() + this.getGstApplicable('total')) > 1000000) {
      this.notify.error('The system allows online payment up to 10 lacs only in a single-time. For making payments above 10 lacs, please contact our Team.')
    } else {
      let payload = {
        amount: (this.getSecurityDeposit() + this.getGstApplicable('total')),
        type: this.getTransactionType(),
        asset_id: this.paymentDetails?.asset_id,
        asset_type: this.isNew ? 1 : 2,
        buyer: this.cognitoId,
        payment_type: (this.paymentDetails?.payment_type == 'Partial') ? 1 : 2,
        role: this.getUserRole()
      }
      this.paymentService.generateOrderId(payload).subscribe(
        (data: any) => {
          this.payWithRazor(data);
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  getTransactionType() {
    if (this.isNew) {
      return 4;
    } else if (this.paymentDetails?.case == 'Bid') {
      return 3;
    } else if (this.paymentDetails?.case == 'Insta_sale') {
      return 2;
    } else if (this.paymentDetails?.case == 'Insta_valuation') {
      return 6;
    } else if (this.paymentDetails?.case == 'Advance_valuation') {
      return 5;
    } else if (this.paymentDetails?.case == 'Auction') {
      return 1;
    } else {
      return '';
    }
  }

  payWithRazor(data) {
    data.description = 'iQuippo Equipment Payment';
    this.paymentService.payWithRazor(data).subscribe(
      (res: any) => {
        this.notify.success('Asset Payment Successful !!');
        this.updatePaymentStatus(data, res);
        if (this.isNew) {
          this.updateEquipmentLeadStatus(data);
        } else {
          if ((this.paymentDetails?.case == 'Bid') || (this.paymentDetails?.case == 'Insta_sale')) {
            if (this.paymentDetails?.payment_type == 'Partial') {
              if (this.paymentDetails?.bid_id) {
                this.updateTradeBids(data, this.paymentDetails?.bid_id);
              } else {
                this.setTradeBids(this.paymentDetails?.case, this.bidDetails?.is_cooling_period, data);
              }
            } else {
              this.updateTradeBids(data);
            }
          } else if (this.paymentDetails?.case == 'Insta_valuation') {
            this.setInstaValStatus(data);
          } else if (this.paymentDetails?.case == 'Advance_valuation') {
            this.setAdvanceValStatus(data);
          } else if (this.paymentDetails?.case == 'Auction') {
            if (this.paymentDetails.payment_type == 'Partial') {
              this.notify.success('Auction registration payment done successfully!!', true);
              setTimeout(() => {
                window.open("/auctions/auction-detail/" + this.auctionDetails?.id + '/' + this.auctionDetails?.auction_engine_auction_id, '_self');
              }, 1000);
            } else {
              this.updateFulfillmentStatus(data);
            }
          } else {
            this.updateTradeBids(data);
          }
        }
      },
      err => {
        this.updatePaymentStatus(data);
        this.notify.error(err);
      }
    )
  }

  setInstaValStatus(data) {
    let request = {
      transaction_id: data.id,
      status: 3
    }
    this.valuationService.patchInstaVal(this.unique_control_number, request).subscribe(res => {
      this.notify.success('Payment done successfully!!', true);
      localStorage.setItem("isfrompayment", 'true');
      setTimeout(() => {
        if (this.storage.getSessionStorageData('isFromAdmin', false)) {
          window.open("/admin-dashboard/val-request", '_self');
        } else {
          window.open("/customer/dashboard/insta-valuation-request", '_self');
        }

      }, 1000);
    })
  }

  setAdvanceValStatus(data) {
    let request = {
      transaction_id: data.id,
      status: 12
    }
    this.valuationService.patchAdvanceVal(this.unique_control_number, request).subscribe(res => {
      this.notify.success('Payment done successfully!!', true);
      localStorage.setItem("isfrompayment", 'true');
      setTimeout(() => {
        if (this.storage.getSessionStorageData('isFromAdmin', false)) {
          window.open("/admin-dashboard/adv-val-request", '_self');
        } else {
          window.open("/customer/dashboard/advanced-valuation-request", '_self');
        }
      }, 1000);
    })
  }

  updateEquipmentLeadStatus(details) {
    let payload = {
      status: 5,
      transaction: details.id
    }
    this.newEquipmentService.updateEnquiryDetails(payload, this.bookingDetails?.enquiry_id).subscribe(
      data => {
        this.notify.success('Asset booking payment done successfully!!', true);
        setTimeout(() => {
          window.open("/customer/dashboard/my-transaction", '_self');
        }, 1000);
      },
      err => {
        console.log(err);
      }
    );
  }

  updatePaymentStatus(txndetails, result?) {
    let payload = {
      payment_id: result ? result.razorpay_payment_id : '',
      payment_status: result ? 2 : 4,
    }
    if ((this.paymentDetails?.case == 'Auction') && (this.paymentDetails.payment_type == 'Partial')) {
      this.paymentService.updateAuctionRegPaymentStatus(txndetails.id, payload).subscribe(
        data => {
          // console.log(data);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.paymentService.updatePaymentStatus(txndetails.id, payload).subscribe(
        data => {
          // console.log(data);
        },
        err => {
          console.log(err);
        }
      );
    }

  }

  setTradeBids(type: any, coolingPeriod?, txnDetails?: any) {
    let payload = {
      amount: (this.paymentDetails?.case == 'Insta_sale') ? this.equipmentDetails.selling_price : this.bidDetails?.bid_amount,
      equipment: this.paymentDetails?.asset_id,
      buyer: this.cognitoId,
      offer_status: this.getOfferStatus(type, coolingPeriod),
      bid_status: this.getBidStatus(type, coolingPeriod),
      deal_status: this.getDealStatus(type, coolingPeriod),
      trade_type: (this.paymentDetails?.case == 'Insta_sale') ? 'IS' : 'BID',
      is_cooling_period: coolingPeriod,
      partial_payment: txnDetails ? [txnDetails.id] : []
    }
    this.paymentService.setTradeBids(payload).subscribe(
      (data: any) => {
        if (this.paymentDetails?.case == 'Bid') {
          this.notify.success('Bid submitted successfully!!', true);
        }
        if (this.paymentDetails?.case == 'Insta_sale') {
          this.notify.success('Asset Initial payment done successfully!!', true);
        }
        setTimeout(() => {
          window.open("/customer/dashboard/my-transaction", '_self');
        }, 1000);
      },
      err => {
        console.log(err);
      }
    )
  }

  getOfferStatus(type, coolingPeriod) {
    if (type == 'Bid') {
      if (coolingPeriod) {
        return 'BR';
      } else {
        return 'BA';
      }
    } else {
      return 'PP';
    }
  }

  getBidStatus(type, coolingPeriod) {
    if (type == 'Bid') {
      if (coolingPeriod) {
        return 'PA';
      } else {
        return 'SP';
      }
    } else {
      return 'SP';
    }
  }

  getDealStatus(type, coolingPeriod) {
    if (type == 'Bid') {
      if (coolingPeriod) {
        return 'DP';
      } else {
        return 'CP';
      }
    } else {
      return 'CP';
    }
  }

  updateTradeBids(txnDetails?: any, bid_id?: any) {
    let payload: any;
    if (bid_id) {
      payload = {
        amount: this.bidDetails?.bid_amount,
        offer_status: this.getOfferStatus('Bid', this.bidDetails?.is_cooling_period),
        bid_status: this.getBidStatus('Bid', this.bidDetails?.is_cooling_period),
        is_cooling_period: this.bidDetails?.is_cooling_period,
        deal_status: this.getDealStatus('Bid', this.bidDetails?.is_cooling_period),
        partial_payment: txnDetails ? [txnDetails.id] : []
      }
    } else {
      payload = {
        offer_status: 'FP',
        final_payments: txnDetails ? [txnDetails.id] : '',
      }
    }
    this.paymentService.updateTradeBids(payload, (bid_id ? bid_id : this.tradeBidDetails?.bid_Id)).subscribe(
      (data: any) => {
        if (bid_id) {
          this.notify.success('Bid updated successfully!!', true);
        } else {
          this.notify.success('Asset Full payment done successfully!!', true);
        }
        setTimeout(() => {
          window.open("/customer/dashboard/my-transaction", '_self');
        }, 1000);
      },
      err => {
        console.log(err);
      }
    )
  }

  onChange(e) {
    this.isAgree = e.checked;
    this.changeDetect.detectChanges();
  }

  /**
   * Auctions 
   */

  onEmdSelection(e, emd) {
    if (e.checked) {
      emd.checked = true;
      if ((emd?.emd_type == 'AU') && (emd?.auction?.id == this.paymentDetails?.auction_id)) {
        this.selectedAuctionEmd.forEach(ele => {
          ele.checked = false;
        })
        this.selectedAuctionEmd = [];
        this.selectedAuctionEmd.push(emd);
      } else {
        let Obj = this.selectedAuctionEmd.find(e => { return (e?.emd_type == 'AU') && (e?.auction?.id == this.paymentDetails?.auction_id) });
        if (Obj) {
          Obj.checked = false;
          let objIndex = this.selectedAuctionEmd.findIndex((f) => { return f?.id == Obj?.id });
          this.selectedAuctionEmd.splice(objIndex, 1);
        }
        this.selectedAuctionEmd.push(emd);
      }
    } else {
      let objIndex = this.selectedAuctionEmd.findIndex((f) => { return f?.id == emd?.id });
      this.selectedAuctionEmd.splice(objIndex, 1);
      emd.checked = false;
    }
  }

  resetEmdSelection() {
    this.selectedAuctionEmd = [];
    this.emdValues.forEach(ele => {
      ele.checked = false;
    })
  }

  getAuctionDetails(id) {
    forkJoin([this.auctionService.getAuctionDetail(id), this.auctionService.getDescription({ type__in: 5 }), this.auctionService.getLotsDetailByAuctionId(`limit=-1`, id)]).subscribe(
      (res: any) => {
        this.auctionDetails = res[0];
        this.description = res[1].results[0]?.desc;
        this.auctionLotDetails = res[2].results;
        if (this.paymentDetails?.payment_type == 'Partial') {
          if (this.paymentDetails?.assistedMode) {
            this.cognitoId = this.paymentDetails?.assistedCognitoId;
          }
          this.getAuctionEmdValues();
        } else {
          this.getAuctionPendingPayment();
        }
        if (this.auctionDetails?.is_offline_emd_available) {
          this.paymentMethod = 1;
        } else if (this.auctionDetails?.is_wallet_emd_available) {
          this.paymentMethod = 2;
          this.getWalletBalance();
        } else {
          this.paymentMethod = 3;
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  viewAllLotIds() {
    const dialogRef = this.dialog.open(ViewAllIdsComponent, {
      width: '500px',
      disableClose: true,
      data: {
        auctionLotIds: true,
        auctionId: this.auctionDetails?.auction_engine_auction_id,
        isSubauctions: (this.auctionDetails?.subauctions && this.auctionDetails?.subauctions.length) ? true : false,
        subAuctionIds: (this.auctionDetails?.subauctions && this.auctionDetails?.subauctions.length) ? this.auctionDetails?.subauctions.map(sub => { return { id: sub?.id, auction_engine_id: sub?.auction_engine_auction_id, lotIds: this.getLotIds(sub?.id)} }) : [{ id: this.auctionDetails?.id, auction_engine_id: this.auctionDetails?.auction_engine_auction_id, lotIds: this.getLotIds(this.auctionDetails?.id) }]
      }
    });
  }

  getLotIds(id) {
    if (this.auctionLotDetails && this.auctionLotDetails.length) {
      return this.auctionLotDetails
        .filter(obj => obj?.auction == id)
        .map(obj => obj?.lot_number);
    } else {
      return [];
    }
  }

  getAuctionEmdValues() {
    let payload = {
      cognito_id: this.cognitoId,
      ordering: 'emd_type'
    }
    this.auctionService.getEmdValues(this.paymentDetails?.auction_id, payload).subscribe(
      (res: any) => {
        if (res?.results && res?.results.length) {
          let auctionObj = res?.results.find(item => { return item?.emd_type == 'AU' });
          if (auctionObj?.is_already_paid) {
            if (this.paymentDetails?.assistedMode) {
              this.notify.error('User have already paid the registration amount for this Auction', true);
            } else {
              this.notify.error('You have already paid the registration amount for this Auction', true);
            }
            this.router.navigate(['/auctions/auction-detail/' + this.auctionDetails?.id + '/' + this.auctionDetails?.auction_engine_auction_id]);
            return;
          } else {
            this.emdValues = res?.results.filter((emd) => {
              return !emd?.is_already_paid;
            }).map((emd) => { return emd; })
            res?.results.forEach((emd) => {
              if (emd?.is_already_paid) {
                this.emdValues.push(emd);
              }
            })
            this.selectedAuctionEmd.push(this.emdValues[0]);
            this.selectedAuctionEmd[0].checked = true;
          }
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getAuctionPendingPayment() {
    this.auctionService.getfulfilmentPaymentDetails(this.paymentDetails?.fulfillment_id).subscribe(
      (res: any) => {
        this.tradeBidDetails = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  validateAuctionPayment() {
    let payload = {
      cognito_id: this.cognitoId,
      emd_ids: this.selectedAuctionEmd.map(e => { return e?.id })
    }
    this.auctionService.validateAuctionPayment(payload).subscribe(
      (res: any) => {
        let isPaid = false;
        let paidObjs: Array<any> = [];
        res.forEach((e) => {
          if (e?.has_paid) {
            isPaid = true;
            paidObjs.push(e?.id);
          }
        })
        if (!isPaid) {
          if (this.paymentMethod == 1) {
            if (this.paymentForm.invalid) {
              this.proceedClicked = true;
              return;
            } else {
              this.offlinePayment();
            }
          } else if (this.paymentMethod == 2) {
            if ((this.getSecurityDeposit() + this.getGstApplicable()) > this.walletBalance) {
              this.notify.error('Your wallet balance is less than payment amount. Please add balance in your wallet to proceed !!');
              return;
            } else {
              this.walletDebit();
            }
          } else {
            this.generateAuctionOrderId();
          }
        } else {
          this.notify.error(`You already have paid EMD's. Please reinitiate payment for selected EMD's.`);
          paidObjs.forEach(ele => {
            let obj = this.emdValues.find((f) => { return f?.id == ele });
            obj.checked = false;
            let objIndex = this.selectedAuctionEmd.findIndex((f) => { return f?.id == obj?.id });
            this.selectedAuctionEmd.splice(objIndex, 1);
          })
          return;
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  generateAuctionOrderId() {
    let payload;
    if (this.paymentDetails?.payment_type == 'Full') {
      if ((this.getSecurityDeposit() + this.getGstApplicable('total')) > 1000000) {
        this.notify.error('The system allows online payment up to 10 lacs only in a single-time. For making payments above 10 lacs, please contact our Team.')
      } else {
        payload = {
          amount: (this.getSecurityDeposit() + this.getGstApplicable()),
          type: this.getTransactionType(),
          buyer: this.cognitoId,
          payment_type: 2,
          role: this.getUserRole(),
          payment_mode: 'Online',
          asset_type: 2,
          is_online_payment: true,
          fulfilment: this.paymentDetails?.fulfillment_id,
          registered_by: this.paymentDetails?.assistedMode ? this.storage.getStorageData('cognitoId', false) : this.cognitoId
        }
        this.paymentService.generateOrderId(payload).subscribe(
          (data: any) => {
            this.payWithRazor(data);
          },
          err => {
            console.log(err);
          }
        )
      }
    } else {
      payload = {
        ids: this.selectedAuctionEmd.map(e => { return e?.id }),
        buyer: this.cognitoId,
        role: this.getUserRole(),
        payment_mode: 'Online',
        registered_by: this.paymentDetails?.assistedMode ? this.storage.getStorageData('cognitoId', false) : this.cognitoId
      }
      this.paymentService.generateAuctionOrderId(payload).subscribe(
        (data: any) => {
          this.payWithRazor(data);
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  offlinePayment() {
    let payload = {
      ids: this.selectedAuctionEmd.map(e => { return e?.id }),
      role: this.getUserRole(),
      buyer: this.cognitoId,
      registered_by: this.paymentDetails?.assistedMode ? this.storage.getStorageData('cognitoId', false) : this.cognitoId
    }
    if (this.paymentDetails?.payment_type == 'Full') {
      payload['fulfilment'] = this.paymentDetails?.fulfillment_id;
    }
    payload = { ...payload, ...this.paymentForm.value };
    this.paymentService.generateAuctionOrderId(payload).subscribe(
      (data: any) => {
        if (this.paymentDetails.payment_type == 'Partial') {
          this.notify.success('Auction registration payment done successfully!!', true);
          setTimeout(() => {
            window.open("/auctions/auction-detail/" + this.auctionDetails?.id + '/' + this.auctionDetails?.auction_engine_auction_id, '_self');
          }, 1000);
        } else {
          this.updateFulfillmentStatus(data);
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  walletDebit() {
    let payload = {
      ids: this.selectedAuctionEmd.map(e => { return e?.id }),
      role: this.getUserRole(),
      buyer: this.cognitoId,
      payment_mode: "Online",
      registered_by: this.paymentDetails?.assistedMode ? this.storage.getStorageData('cognitoId', false) : this.cognitoId
    }
    this.paymentService.auctionWalletPayment(payload).subscribe(
      (data: any) => {
        if (this.paymentDetails?.payment_type == 'Partial') {
          this.notify.success('Auction registration payment done successfully!!', true);
          setTimeout(() => {
            window.open("/auctions/auction-detail/" + this.auctionDetails?.id + '/' + this.auctionDetails?.auction_engine_auction_id, '_self');
          }, 1000);
        } else {
          this.updateFulfillmentStatus(null, data);
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  updateValidation(val) {
    if (val == 2) {
      this.getWalletBalance();
    }
    if (val != 1) {
      this.paymentForm.controls.bank_name.clearValidators();
      this.paymentForm.controls.bank_name.updateValueAndValidity();
      this.paymentForm.controls.branch.clearValidators();
      this.paymentForm.controls.branch.updateValueAndValidity();
      this.paymentForm.controls.proof_of_payment.clearValidators();
      this.paymentForm.controls.proof_of_payment.updateValueAndValidity();
      this.paymentForm.controls.bank_ref_no.clearValidators();
      this.paymentForm.controls.bank_ref_no.updateValueAndValidity();
      this.paymentForm.controls.dd_in_favour_of.clearValidators();
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    } else {
      this.paymentForm = this.fb.group({
        payment_mode: ['', Validators.required],
        bank_name: ['', Validators.required],
        branch: ['', Validators.required],
        proof_of_payment: ['', [Validators.required]],
        bank_ref_no: ['', [Validators.required]],
        dd_in_favour_of: ['']
      })
    }
  }

  updateFulfillmentStatus(txnDetails?: any, walletDetails?: any) {
    let payload = {
      payment: {
        transaction: txnDetails ? txnDetails?.id : null,
        wallet: walletDetails ? walletDetails?.id : null
      },
      fulfilment: this.paymentDetails?.fulfillment_id
    }
    this.paymentService.updateFulfillmentStatus(payload).subscribe(
      (data: any) => {
        this.notify.success('Auction Fulfillment payment done successfully!!', true);
        setTimeout(() => {
          window.open("/customer/dashboard/my-transaction", '_self');
        }, 1000);
      },
      err => {
        console.log(err);
      }
    )
  }

  getWalletBalance() {
    if (!this.walletBalance) {
      let walletPayload = {
        user: this.cognitoId
      }
      this.paymentService.getWalletBalance(walletPayload).pipe(finalize(() => { })).subscribe(
        (data: any) => {
          this.walletBalance = data?.total;
          this.changeDetect.detectChanges();
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  getFileName(str) {
    return str.split('/').pop();
  }

  onPaymentModeChange(val) {
    if (val == 'Cash') {
      this.paymentForm.controls.bank_name.clearValidators();
      this.paymentForm.controls.bank_name.updateValueAndValidity();
      this.paymentForm.controls.branch.clearValidators();
      this.paymentForm.controls.branch.updateValueAndValidity();
      // this.paymentForm.controls.proof_of_payment.clearValidators();
      // this.paymentForm.controls.proof_of_payment.updateValueAndValidity();
      this.paymentForm.controls.bank_ref_no.clearValidators();
      this.paymentForm.controls.bank_ref_no.updateValueAndValidity();
      this.paymentForm.controls.dd_in_favour_of.clearValidators();
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    } else {
      this.paymentForm.controls.bank_name.setValidators([Validators.required]);
      this.paymentForm.controls.bank_name.updateValueAndValidity();
      this.paymentForm.controls.branch.setValidators([Validators.required]);
      this.paymentForm.controls.branch.updateValueAndValidity();
      this.paymentForm.controls.proof_of_payment.setValidators([Validators.required]);
      this.paymentForm.controls.proof_of_payment.updateValueAndValidity();
      this.paymentForm.controls.bank_ref_no.setValidators([Validators.required]);
      this.paymentForm.controls.bank_ref_no.updateValueAndValidity();
      this.paymentForm.controls.dd_in_favour_of.clearValidators();
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    }

    if (val == 'Demand Draft') {
      this.paymentForm.controls.dd_in_favour_of.setValidators([Validators.required]);
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    }
  }

  async handleUpload(event: any) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/marketplace/payment/" + uuidv4() + '/' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.paymentForm.get('proof_of_payment')?.setValue(uploaded_file.Location);
      }
    } else {
      this.notify.error("Invalid Image.")
    }
    this.imageInput.nativeElement.value = '';
  }

  deleteFile(imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.paymentForm.get('proof_of_payment')?.setValue('');
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }

  clearStorage() {
    this.storage.cleanSessionStorageAll();
  }

  ngOnDestroy() {
    this.clearStorage();
  }

  termsConditions() {
    const dialogRef = this.dialog.open(TermsAndConditionsComponent, {
      width: '900px'
    });
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20) {
      this.document.getElementById('divPayment')?.classList.add('payment-card-fix')
    }
    else {
      this.document.getElementById('divPayment')?.classList.remove('payment-card-fix')
    }
  }

}
