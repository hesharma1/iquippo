import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { debug } from 'console';
import { AuctionService } from 'src/app/services/auction.service';
declare var esignWidgetCall: any;
declare var getWidgetToken: any;
@Component({
  selector: 'app-e-sign-confirm-dialog',
  templateUrl: './e-sign-confirm-dialog.component.html',
  styleUrls: ['./e-sign-confirm-dialog.component.css']
})
export class ESignConfirmDialogComponent implements OnInit {
  reqData: any;
  widgetToken: any;
  widgetData: any;
  constructor(public dialogRef: MatDialogRef<ESignConfirmDialogComponent>,
    public auctionService: AuctionService,
    @Inject(MAT_DIALOG_DATA) data: any, private router: Router) {
    this.reqData = data;
  }

  ngOnInit() {
  }

  backClick() {
    this.dialogRef.close(false);
  }

  onProceed() {
    // this.router.navigate([`/admin-dashboard/client-management/agreement`], { queryParams: { 'isForEsign': true, 'auction': this.reqData.auction } });
    this.router.navigate([`/dashboard/my-agreements`], { queryParams: { 'isForEsign': true, 'auction': this.reqData.auction, 'returnUrl' : this.reqData.returnUrl } });
    this.dialogRef.close();
  }
}

