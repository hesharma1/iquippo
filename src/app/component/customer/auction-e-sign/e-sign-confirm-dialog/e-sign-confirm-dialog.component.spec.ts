import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ESignConfirmDialogComponent } from './e-sign-confirm-dialog.component';

describe('ESignConfirmDialogComponent', () => {
  let component: ESignConfirmDialogComponent;
  let fixture: ComponentFixture<ESignConfirmDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ESignConfirmDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ESignConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
