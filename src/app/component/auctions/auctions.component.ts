import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { AuctionService } from 'src/app/services/auction.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { CommonService } from 'src/app/services/common.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { Router } from '@angular/router';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { ESignConfirmDialogComponent } from '../customer/auction-e-sign/e-sign-confirm-dialog/e-sign-confirm-dialog.component';
import { PaymentService } from 'src/app/services/payment.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { environment } from 'src/environments/environment';
import { SigninComponent } from '../signin/signin.component';
import { CustomerSearchModalComponent } from '../shared/customer-search-modal/customer-search-modal.component';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {
  @ViewChild('ongoing') ongoing!: ElementRef;

  public searchText!: string;
  public searchValue!: string;
  public auctionOngoinList: Array<any> = [];
  public auctionUpcomingList: Array<any> = [];
  public userRole: boolean = false;
  public cognitoId!: any;
  public auctionsSuggestions: any;
  eSignReqData: any;
  esignStatus: any;
  auctionDataById: any = {};
  loading: boolean = true;
  ongoingSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
      }
    }
  };

  upcomingSlideOptions = {
    items: 4, dots: false, nav: true,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
      }
    }
  };
  assistedMode: boolean = false;

  constructor(private auctionService: AuctionService, public spinner: NgxSpinnerService, public notify: NotificationService, public datePipe: DatePipe, private dialog: MatDialog, public storage: StorageDataService, private commonService: CommonService, private appRouteEnum: AppRouteEnum, public router: Router, public paymentService: PaymentService, public apiPath: ApiRouteService) { }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('role', false);
    this.getData();
  }

  getData() {
    let ongoingPayload = {
      limit: 999,
      starts_at__lte: (new Date()).toISOString(),
      ends_at__gte: (new Date()).toISOString(),
      parent_auction__id__isnull: true,
      auction_engine_auction_id__isnull: false
    }
    let upcomingPayload = {
      limit: 999,
      starts_at__gt: (new Date()).toISOString(),
      parent_auction__id__isnull: true,
      auction_engine_auction_id__isnull: false
    }
    if (this.searchValue) {
      ongoingPayload['search'] = this.searchValue;
      upcomingPayload['search'] = this.searchValue;
    }
    forkJoin([this.auctionService.getAuctionList(ongoingPayload), this.auctionService.getAuctionList(upcomingPayload)]).subscribe(
      (res: any) => {
        if (res) {
          this.auctionOngoinList = res[0]?.results;
          this.auctionUpcomingList = res[1]?.results;
          // this.searchText = '';          
        }
      }
    )
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 1) {
      if (this.searchText.includes(' - ')) {
        this.searchValue = this.searchText.slice(this.searchText.lastIndexOf("-") + 1).trim();
      } else {
        this.searchValue = this.searchText;
      }
      this.getData();
      this.scrollTo();
    }
  }

  scrollTo() {
    let topGap: any = document.getElementById('header')?.offsetHeight;
    this.animate(document.scrollingElement || document.documentElement, "scrollTop", "", 0, (this.ongoing?.nativeElement?.offsetTop - topGap), 1000, true);
  }

  getAuctionSuggestions() {
    if (this.searchText && this.searchText.length > 1) {
      this.searchValue = '';
      let payload = {
        page: 1,
        limit: 999,
        ordering: '-id',
        ends_at__gt: (new Date()).toISOString(),
        parent_auction__id__isnull: true
      }
      if (this.searchText) {
        payload['search'] = this.searchText;
      }

      this.auctionService.auctionDropdown(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res) {
            this.auctionsSuggestions = res.results;
          }
        },
        err => {
          this.notify.error(err.message);
        }
      );
    } else {
      this.auctionsSuggestions = [];
    }
  }

  selectAuctionId(id) {
    this.searchValue = id;
    this.getData();
    this.scrollTo();
  }

  animate(elem, style, unit, from, to, time, prop) {
    if (!elem) {
      return;
    }
    var start = new Date().getTime(),
      timer = setInterval(function () {
        var step = Math.min(1, (new Date().getTime() - start) / time);
        if (prop) {
          elem[style] = (from + step * (to - from)) + unit;
        } else {
          elem.style[style] = (from + step * (to - from)) + unit;
        }
        if (step === 1) {
          clearInterval(timer);
        }
      }, 25);
    if (prop) {
      elem[style] = from + unit;
    } else {
      elem.style[style] = from + unit;
    }
  }

  getTimeLeft(endDate) {
    const end_date = new Date(endDate);
    const current_date = new Date();
    if (current_date > end_date) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      };
    } else {
      let totalSeconds = Math.floor((end_date.getTime() - (current_date.getTime())) / 1000);
      let totalMinutes = Math.floor(totalSeconds / 60);
      let totalHours = Math.floor(totalMinutes / 60);
      let totalDays = Math.floor(totalHours / 24);

      let hours = totalHours - (totalDays * 24);
      let minutes = totalMinutes - (totalDays * 24 * 60) - (hours * 60);
      let seconds = totalSeconds - (totalDays * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

      return {
        days: totalDays,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        this.openSignInModal();
      }
    });
  }

  onRegisterNow(data) {
    if (this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        this.checkAuthentication().then((isAuthorise) => {
          if (isAuthorise) {
            const params = {
              bidder__cognito_id__in: this.cognitoId
            }
            this.auctionService.getAuctionEsign(params).subscribe(
              (res: any) => {
                this.eSignReqData = res?.results as any[];
                this.esignStatus = (res && res?.results && (res?.results.length > 0) && (res?.results[0]?.status)) ? res?.results[0]?.status : '';
                //  this.esignStatus = 'BUS';
                if (this.esignStatus != 'BUS') {
                  const dialogRef = this.dialog.open(ESignConfirmDialogComponent, {
                    width: '500px',
                    data: {
                      auction: data?.id,
                      bidder: this.cognitoId,
                      returnUrl: this.router.url
                    }
                  });
                }
                else {
                  this.auctionRegister(data);
                }
              });
          }
        });
      }
      else if (rolesArray.includes('Admin') || rolesArray.includes('SuperAdmin') || rolesArray.includes('ChannelPartner')) {
        const dialogRef = this.dialog.open(CustomerSearchModalComponent, {
          width: '600px',
          data: {
            type: 'Auction',
            data: data,
            heading: "Register for Auction",
            subHeading: "Does the user already exist on iQuippo?"
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result && result?.cognitoId) {
            this.checkCustomerKYC(result?.cognitoId).then((isAuthorise) => {
              if (isAuthorise) {
                const params = {
                  bidder__cognito_id__in: result?.cognitoId
                }
                this.auctionService.getAuctionEsign(params).subscribe(
                  (res: any) => {
                    this.eSignReqData = res?.results as any[];
                    this.esignStatus = res && res?.results && res?.results.length > 0 && res?.results[0].status ? res?.results[0].status : '';
                    if (this.eSignReqData && this.eSignReqData.length > 0) {
                      //this.esignStatus = 'BUS';
                      if (this.esignStatus == 'BUS') {
                        this.assistedMode = true;
                        this.auctionRegister(data, result?.cognitoId);
                      }
                      else {
                        this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
                      }
                    }
                    else {
                      this.initiateBUForm(result.cognitoId)
                    }
                  });
              }
            });
          }
        });
      }
      else {
        this.notify.error('Please login as Customer to proceed further with asset buy!!');
      }
    }
    else {
      this.openSignInModal();
    }
  }

  initiateBUForm(cognitoId) {
    const params = {
      role_type: 1,
      bidder: cognitoId,
    }
    this.auctionService.postAuctionEsign(params).subscribe(
      (res: any) => {
        this.notify.success('The Bidder Undertaking of this customer has been successfully initiated.');
        setTimeout(() => {
          if (res && res?.status == 'EP') {
            this.patchESignReq(res?.reference_id);
          }
          else if (res && (res?.status == 'INI' || res?.status == 'UNS' || res?.status == 'TC')) {
            this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
          }
        }, 2000);
      }, err => {
      });
  }

  patchESignReq(referenceId) {
    const params = {
      reference_id: referenceId,
      page_url: environment.mp1HomePage + 'customer/dashboard/e-sign-template/'
    }
    this.auctionService.patchAuctionEsign(params, referenceId).subscribe(
      (res: any) => {
        this.notify.success('The mail has been successfully sent to the customer!!');
      });
  }

  checkCustomerKYC(cognitoId) {
    return new Promise((resolved, reject) => {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      let payload = {
        cognito_id: cognitoId,
        role: 'Customer'
      }
      this.commonService.validateSeller(payload).subscribe(
        (res: any) => {
          if (!res.kyc_verified || !res.pan_address_proof) {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
              width: '500px',
              disableClose: true,
              data: {
                heading: 'KYC Details Incomplete',
                message: rolesArray.includes('ChannelPartner') ? 'Please ask user to complete the KYC in their My profile section' : 'You need to complete user KYC profile with iQuippo to make payments.',
                submessage: rolesArray.includes('ChannelPartner') ? '' : 'Click Yes if you want to proceed or No if you want to do this later. ',
                isRoleChannelPartner: rolesArray.includes('ChannelPartner') ? true : false
              }
            });

            dialogRef.afterClosed().subscribe(val => {
              if (val) {
                let rolesArray = this.storage.getStorageData("rolesArray", false);
                rolesArray = rolesArray.split(',');
                if (rolesArray.includes('Admin')) {
                  localStorage.setItem("userRole", 'Admin');
                }
                else if (rolesArray.includes('SuperAdmin')) {
                  localStorage.setItem("userRole", 'SuperAdmin');
                }
                else if (rolesArray.includes('ChannelPartner')) {
                  localStorage.setItem("userRole", 'ChannelPartner');
                }
                this.cognitoId = this.storage.getStorageData("cognitoId", false);
                this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
                  this.storage.setStorageData("UserInfo", res, true);
                });

                this.router.navigate(['/admin-dashboard/customer-management']);
              }
            })
            resolved(false);
            return;
          } else {
            resolved(true);
          }
        },
        err => {
          reject(false);
        }
      );

    });
  }

  auctionRegister(data, assistedCognitoId?) {
    this.checkEmdAvailability(data?.id).then((res) => {
      let paymentDetails = {
        auction_id: data?.id,
        asset_type: 'Used',
        case: 'Auction',
        payment_type: 'Partial',
        assistedMode: this.assistedMode
      }
      if (this.assistedMode) {
        paymentDetails['assistedCognitoId'] = assistedCognitoId;
      }
      this.storage.setSessionStorageData('paymentSession', true, false);
      this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
      this.router.navigate(['/customer/payment']);
    },
      err => {
        console.log(err);
      }
    );
  }

  checkInitialPayment(auctionID, parentId) {
    return new Promise((resolved, reject) => {
      let payload = {
        cognito_id: this.cognitoId
      }
      this.paymentService.getEmdPaymentStatus(payload, (parentId ? parentId : auctionID)).subscribe(
        (res: any) => {
          if (res?.redirect) {
            resolved(true);
          } else {
            this.notify.error('You have not made the requisite EMD payment to participate in this auction. Please click Register Now button to make the necessary payment.');
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
        }
      );
    });
  }

  checkEmdAvailability(auctionID) {
    return new Promise((resolved, reject) => {
      this.auctionService.getEmdValues(auctionID).subscribe(
        (res: any) => {
          if (res?.count) {
            resolved(true);
          } else {
            this.notify.error('Don"t have emd against this auction, Please contact admin !!');
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
        }
      );
    });
  }

  auctionbid(id, parentId) {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          this.checkInitialPayment(id, parentId).then((res) => {
            let payload = `auction_id=${id}&user_cognito_id=${this.cognitoId}&role='Customer'`;
            window.open(environment.auctionBaseUrl + "auction/integration/view-online-auction?" + payload, '_blank');
          });
        }
      });
    } else {
      this.openSignInModal();
    }
  }

  trackByFn(index) {
    return index;
  }

  showAuctionDetails(id, auctionEngineId) {
    this.router.navigate(['/auctions/auction-detail/' + id + '/' + auctionEngineId]);
  }

  onLoad() {
    this.loading = false;
  }

  openSignInModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Admin')) {
          localStorage.setItem("userRole", 'Admin');
        }
        else if (rolesArray.includes('SuperAdmin')) {
          localStorage.setItem("userRole", 'SuperAdmin');
        }
        else if (rolesArray.includes('ChannelPartner')) {
          localStorage.setItem("userRole", 'ChannelPartner');
        }
        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
          this.storage.setStorageData("UserInfo", res, true);
        });

      }
    })
  }

}
