import { AfterViewInit, Component, OnInit, Renderer2 } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AuctionService } from 'src/app/services/auction.service';
import { PaymentService } from 'src/app/services/payment.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { environment } from 'src/environments/environment';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { SigninComponent } from '../../signin/signin.component';
import { Gallery, ImageItem } from "ng-gallery";

@Component({
  selector: 'app-auction-lot',
  templateUrl: './auction-lot.component.html',
  styleUrls: ['./auction-lot.component.css']
})
export class AuctionLotComponent implements OnInit {
  lotAuctionData: any;
  lotAuctionId: any;
  challengeArray: any = [[]];
  isShowPreBtn: boolean = false;
  parent = document.getElementsByClassName("mySlides");
  slideIndex = 0;
  arrayUpdated: boolean = true;
  cognitoId: any;
  auctionid: any;
  parentId: any;
  currentDate: any = (new Date()).toISOString();
  isShowPlaceBid: boolean = false;
  auctionDataById: any;
  lotId: any;
  lotVisualsImageData:any = [];

  constructor(public auctionService: AuctionService, public spinner: NgxSpinnerService, private renderer: Renderer2, public paymentService: PaymentService, public apiPath: ApiRouteService, public notify: NotificationService, private appRouteEnum: AppRouteEnum, public storage: StorageDataService, private commonService: CommonService, private dialog: MatDialog, public router: Router, public gallery: Gallery, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['id']) {
        this.lotAuctionId = params['id'];
        this.lotId = params['lotid'];
        this.getLotAuctionDetail(this.lotAuctionId, this.lotId);
      }

      if (params['auctionid']) {
        this.auctionid = params['auctionid'];
        this.getAuctionDetail(this.auctionid);
      }
    });
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
  }

  getAuctionDetail(auctionid) {
    this.auctionService.getAuctionDetail(auctionid).subscribe((res: any) => {
      this.auctionDataById = res;
    }, (err: any) => {
      this.notify.error(err?.message);
    })
  }

  getLotAuctionDetail(id, lotid) {
    let lotId = 'id__in=' + lotid;
    this.auctionService.getLotsById(lotId, id).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.lotAuctionData = res?.results;
        this.arrayUpdated = true;

        if(this.lotAuctionData[0]?.assets && this.lotAuctionData[0]?.assets.length){
          this.lotAuctionData[0]?.assets.forEach((element,i) => {
            if(element?.asset && element?.asset?.visuals && element?.asset?.visuals.length){
              let tempVisuals = element?.asset?.visuals.filter(f => { return (f.visual_type == 1) && (f.is_primary == true) });
              element?.asset?.visuals.forEach(f => {
                if ((f.visual_type == 1) && (f.is_primary == false)) {
                  tempVisuals.push(f);
                }
              });
              if (tempVisuals.length) {
                this.lotVisualsImageData[i] = tempVisuals.map(ele =>
                  new ImageItem({ src: ele?.url, thumb: ele?.url })
                )
              }
            }
          });
        }
      },
      err => {
      }
    )

  }

  showSlides(n) {
    if (n == 1) {
      this.isShowPreBtn = false;
    }
    else {
      this.isShowPreBtn = true;
    }
    var i;
    if (n > this.parent.length) {
      this.slideIndex = 1;
    }
    if (n < 1) {
      this.slideIndex = this.parent.length;
    }
    for (i = 0; i < this.parent.length; i++) {
      this.renderer.setStyle(this.parent[i], 'display', 'none');
    }
    this.renderer.setStyle(this.parent[this.slideIndex - 1], 'display', 'block');
  }

  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  getChallengeArray(arr, size) {
    let array: any[] = [];
    for (let i = 0; i < arr.length; i += size) {
      array.push(arr.slice(i, i + size));
    }
    return array;
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        console.log("rolesArray", rolesArray);
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.width = '900px';
        dialogConfig.data = {
          flag: true
        }
        const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = this.storage.getStorageData('cognitoId', false);
          }
        })
      }
    });
  }

  auctionBid(id, parentId) {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          this.checkInitialPayment().then((res) => {
            let payload = `auction_id=${id}&user_cognito_id=${this.cognitoId}&role='Customer'`;
            window.open(environment.auctionBaseUrl + "auction/integration/view-online-auction?" + payload, '_blank');
          });
        }
      });
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = this.storage.getStorageData('cognitoId', false);
        }
      })
    }
  }

  checkInitialPayment() {
    return new Promise((resolved, reject) => {
      let auctionId = this.auctionid;
      let parentId = this.auctionDataById?.parent_auction
      let payload = {
        cognito_id: this.cognitoId
      }
      this.paymentService.getEmdPaymentStatus(payload, (parentId ? parentId : auctionId)).subscribe(
        (res: any) => {
          if (res?.redirect) {
            resolved(true);
          } else {
            this.notify.error('You have not made the requisite EMD payment to participate in this auction. Please click Register Now button to make the necessary payment.');
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
        }
      );
    });
  }

  auctionbid() {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          this.checkInitialPayment().then((res) => {
            let payload = `auction_id=${this.auctionid}&user_cognito_id=${this.cognitoId}&role='Customer'`;
            window.open(environment.auctionBaseUrl + "auction/integration/view-online-auction?" + payload, '_blank');
          });
        }
      });
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = this.storage.getStorageData('cognitoId', false);
        }
      })
    }
  }

  getAssetImageVisuals(imgArray){
    let tempVisuals = imgArray.filter(f => { return (f.visual_type == 1) && (f.is_primary == true) });
    let mainImageArray = [];
    imgArray.forEach(f => {
      if ((f.visual_type == 1) && (f.is_primary == false)) {
        tempVisuals.push(f);
      }
    });
    if (tempVisuals.length) {
      mainImageArray = tempVisuals.map(element =>
        new ImageItem({ src: element?.url, thumb: element?.url })
      )
    }
    return mainImageArray;
  }
}
