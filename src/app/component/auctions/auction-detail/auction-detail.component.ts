import { ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { isSelectionNode } from 'graphql';
import { forkJoin, merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuctionService } from 'src/app/services/auction.service';
import { CommonService } from 'src/app/services/common.service';
import { PaymentService } from 'src/app/services/payment.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { ESignConfirmDialogComponent } from '../../customer/auction-e-sign/e-sign-confirm-dialog/e-sign-confirm-dialog.component';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { CustomerSearchModalComponent } from '../../shared/customer-search-modal/customer-search-modal.component';
import { SigninComponent } from '../../signin/signin.component';

@Component({
  selector: 'app-auction-detail',
  templateUrl: './auction-detail.component.html',
  styleUrls: ['./auction-detail.component.css']
})
export class AuctionDetailComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild("auctionAssets") auctionAssets?: ElementRef;

  cognitoId: any;
  auctionId: any = 1;
  auctionDataById: any = {};
  subAuction: Array<any> = [];
  lotsData: Array<any> = [];
  sideFiltersArray: Array<any> = [];
  categoryFilters: Array<any> = [];
  brandFilters: Array<any> = [];
  yearFilters: Array<any> = [];
  locationFilters: Array<any> = [];
  serverSideCategoryFilter: any;
  serverSideBrandFilter: any;
  serverSideLocationFilter: any;
  serverSideYearFilter: any;
  days: any;
  hours: any;
  minutes: any;
  seconds: any;
  eSignReqData: any;
  esignStatus: any;
  showFilter: any;
  lotPageSize: any;
  totalLots: any;
  activeSubauctionId: any;
  page: number = 1;
  pageSize: number = 12;
  currentDate: any = (new Date()).toISOString();

  contactSlideOptions = {
    // items: 3,
    dots: false,
    nav: true,
    loop: false,
    responsiveClass: true,
    dragBeforeAnimFinish: false,
    autoplay: false,
    responsive: {
      300: {
        items: 1,
        nav: true
      },
      400: {
        items: 1,
        nav: true
      },
      500: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      700: {
        items: 2,
        nav: true
      },
      800: {
        items: 3,
        nav: true
      },
      900: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false
      }
    }
  };

  isFilterApply: boolean = false;
  filterlotsData: any[] = [];
  originalCategory: any;
  categoryShow: boolean = true;
  brandShow: boolean = true;
  locationShow: boolean = true;
  yearShow: boolean = true;
  originalYear: any[] = [];
  originalLocation: any[] = [];
  originalBrand: any[] = [];
  isSingleSubactionShow: boolean = false;
  signleSub: any;
  auctionContacts: any[] = [];
  loading: boolean = false;
  assistedMode: boolean = false;
  firstActiveSubauction: string = "sub0"
  constructor(private auctionService: AuctionService, public notify: NotificationService, public route: ActivatedRoute, public dialog: MatDialog, public router: Router, public storage: StorageDataService, private commonService: CommonService, private appRouteEnum: AppRouteEnum, public paymentService: PaymentService, public changeDetect: ChangeDetectorRef) {
    if (768 > window.innerWidth) {
      this.showFilter = false;
    }
  }

  ngOnInit(): void {
    localStorage.removeItem('returnUrl');
    this.cognitoId = localStorage.getItem('cognitoId');
    this.route.params.subscribe((data) => {
      this.auctionId = data.id;
    })
    localStorage.setItem('auctionId', this.auctionId);
    this.getSideFilters();
    this.getAuctionDetail();
    this.getAuctionContactDetail();
  }

  showFilters() {
    if (this.showFilter != undefined) {
      this.showFilter = !this.showFilter;
    } else {
      this.showFilter = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (767 > event.target.innerWidth) {
      this.showFilter = false;
    } else {
      this.showFilter = true;
    }
  }

  getAuctionDetail() {
    this.auctionService.getAuctionDetail(this.auctionId).subscribe((res: any) => {
      this.auctionDataById = res;
      this.subAuction = res.subauctions;
      if (this.subAuction && this.subAuction.length > 0) {
        this.activeSubauctionId = this.subAuction[0].id;
        this.getLotsDetailAccordingToSubAuction();
      }
      else {
        this.getLotsDetailAccordingToAuction(this.auctionDataById.id);
      }
    }, (err: any) => {
      this.notify.error(err?.message);
    })
  }

  getAuctionContactDetail() {
    let payload = {
      auction__in: this.auctionId
    }
    this.auctionService.getAuctionContactDetail(payload).subscribe(
      (res: any) => {
        this.auctionContacts = res?.results;
      },
      (err: any) => {
        this.notify.error(err?.message);
      }
    )
  }

  getSideFilters() {
    forkJoin([
      this.auctionService.getBrandFilterById(this.auctionId),
      this.auctionService.getCategoryFilterById(this.auctionId),
      this.auctionService.getLocationFilterById(this.auctionId),
      this.auctionService.getMfgYearFilterById(this.auctionId)
    ]).subscribe((res: any) => {
      for (var i = 0; i < res.length; i++) {
        let filter: any;
        switch (i) {
          case 0:
            filter = this.getCustomizeFilters(res[0].results, 'brand');
            this.originalBrand = filter
            this.brandFilters = filter.slice(0, 5);
            break;
          case 1:
            filter = this.getCustomizeFilters(res[1].results, 'category');
            this.originalCategory = filter
            this.categoryFilters = filter.slice(0, 5);

            break;
          case 2:
            filter = this.getCustomizeFilters(res[2].results, 'location');
            this.originalLocation = filter
            this.locationFilters = filter.slice(0, 5);
            break;
          case 3:
            filter = this.getCustomizeFilters(res[3].results, 'year');
            this.originalYear = filter
            this.yearFilters = filter.slice(0, 5);
            break;
        }
        //this.sideFiltersArray.push(filter);
      }
    }, (err: any) => {
      this.notify.error(err?.message);
    })
  }

  getLotsDetail(queryParams: any, id) {
    this.lotsData = [];
    this.auctionService.getLotsDetailByAuctionId(queryParams, id).subscribe((res: any) => {
      this.lotsData = this.lotsData.concat(res.results);
      this.totalLots = res?.count;
      // window.scrollTo(0, 0);      
    }, (err: any) => {
      this.notify.error(err?.message);
    });
  }

  getFilterLot(id) {
    let lotsData = this.lotsData.filter(x => x.auction == id);
    return lotsData;
  }

  getLotsDetailAccordingToSubAuction(id?) {
    let categoryParam = this.serverSideCategoryFilter == '' || this.serverSideCategoryFilter == undefined ?
      'lotasset__asset__category__in=' : 'lotasset__asset__category__in=' + this.serverSideCategoryFilter;
    let brandParam = this.serverSideBrandFilter == '' || this.serverSideBrandFilter == undefined ?
      '&lotasset__asset__brand__in=' : '&lotasset__asset__brand__in=' + this.serverSideBrandFilter;
    let yearParam = this.serverSideYearFilter == '' || this.serverSideYearFilter == undefined ?
      '&lotasset__asset__mfg_year__in=' : '&lotasset__asset__mfg_year__in=' + this.serverSideYearFilter;
    let locationParam = this.serverSideLocationFilter == '' || this.serverSideLocationFilter == undefined ?
      '&lotasset__asset__pin_code__city__state__in=' : '&lotasset__asset__pin_code__city__state__in=' + this.serverSideLocationFilter;
    let page = '&page=' + this.page;
    let limit = '&limit=' + this.pageSize;
    let queryParams = categoryParam + brandParam + yearParam + locationParam + page + limit;
    if (id) {
      this.getLotsDetail(queryParams, id);
    }
    else {
      this.getLotsDetail(queryParams, this.activeSubauctionId);
    }

  }

  getLotsDetailAccordingToAuction(id, event?) {
    let categoryParam = this.serverSideCategoryFilter == '' || this.serverSideCategoryFilter == undefined ?
      'lotasset__asset__category__in=' : 'lotasset__asset__category__in=' + this.serverSideCategoryFilter;
    let brandParam = this.serverSideBrandFilter == '' || this.serverSideBrandFilter == undefined ?
      '&lotasset__asset__brand__in=' : '&lotasset__asset__brand__in=' + this.serverSideBrandFilter;
    let yearParam = this.serverSideYearFilter == '' || this.serverSideYearFilter == undefined ?
      '&lotasset__asset__mfg_year__in=' : '&lotasset__asset__mfg_year__in=' + this.serverSideYearFilter;
    let locationParam = this.serverSideLocationFilter == '' || this.serverSideLocationFilter == undefined ?
      '&lotasset__asset__pin_code__city__state__in=' : '&lotasset__asset__pin_code__city__state__in=' + this.serverSideLocationFilter;
    let page = '&page=' + this.page;
    let limit = '&limit=' + this.pageSize;
    let queryParams = categoryParam + brandParam + yearParam + locationParam + page + limit;
    this.getLotsDetail(queryParams, id);
  }

  setFilter() {
    this.sideFiltersArray
  }

  getCustomizeFilters(filter, type) {
    let filterArray: any = [];
    for (var i = 0; i < filter.length; i++) {
      switch (type) {
        case 'category':
          let categoryObject: any;
          categoryObject = {
            id: filter[i].asset__category_id,
            isSelected: false,
            text: filter[i].asset__category__name,
            type: 'category',
            count: filter[i].count,
          };
          filterArray.push(categoryObject);
          break;
        case 'brand':
          let brandObject: any;
          brandObject = {
            id: filter[i].asset__brand_id,
            isSelected: false,
            text: filter[i].asset__brand__name,
            type: 'brand',
            count: filter[i].count,

          };
          filterArray.push(brandObject);
          break;
        case 'location':
          let locationObject: any;
          locationObject = {
            id: filter[i].asset__pin_code__city__state__id,
            isSelected: false,
            text: filter[i].asset__pin_code__city__state__name,
            type: 'location',
            count: filter[i].count,
          };
          filterArray.push(locationObject);
          break;
        case 'year':
          let yearObject: any;
          yearObject = {
            id: filter[i].asset__mfg_year,
            isSelected: false,
            text: filter[i].asset__mfg_year,
            type: 'year',
            count: filter[i].count,
          };
          filterArray.push(yearObject);
          break;
      }
    }
    return filterArray;
  }

  applyFilter(object: any) {
    this.isSingleSubactionShow = false;
    object.isSelected = object.isSelected == false ? true : false;
    switch (object.type) {
      case 'category':
        let categoryObject: Array<any> = [];
        this.serverSideCategoryFilter = '';
        this.categoryFilters.forEach(element => {
          if (element.isSelected) {
            categoryObject.push(element.id);
          }
        });
        this.serverSideCategoryFilter = categoryObject.join();
        break;
      case 'brand':
        let brandObject: Array<any> = [];
        this.serverSideBrandFilter = '';
        this.brandFilters.forEach(element => {
          if (element.isSelected) {
            brandObject.push(element.id);
          }
        });
        this.serverSideBrandFilter = brandObject.join();
        break;
      case 'location':
        let locationObject: Array<any> = [];
        this.serverSideLocationFilter = '';
        this.locationFilters.forEach(element => {
          if (element.isSelected) {
            locationObject.push(element.id);
          }
        });
        this.serverSideLocationFilter = locationObject.join();
        break;
      case 'year':
        let yearObject: Array<any> = [];
        this.serverSideYearFilter = '';
        this.yearFilters.forEach(element => {
          if (element.isSelected) {
            yearObject.push(element.id);
          }
        });
        this.serverSideYearFilter = yearObject.join();
        break;
    }
    if (this.subAuction && this.subAuction.length > 0) {
      this.activeSubauctionId = this.subAuction[0].id;
      // this.getLotsDetailAccordingToSubAuction();
      this.getFilterSubActionLot();
    }
    else {
      this.getLotsDetailAccordingToAuction(this.auctionDataById.id);
    }
  }

  setAuctionCountDown(endDate) {
    const end_date = new Date(endDate);
    const current_date = new Date();
    if (current_date > end_date) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      };
    } else {
      let totalSeconds = Math.floor((end_date.getTime() - (current_date.getTime())) / 1000);
      let totalMinutes = Math.floor(totalSeconds / 60);
      let totalHours = Math.floor(totalMinutes / 60);
      let totalDays = Math.floor(totalHours / 24);
      let hours = totalHours - (totalDays * 24);
      let minutes = totalMinutes - (totalDays * 24 * 60) - (hours * 60);
      let seconds = totalSeconds - (totalDays * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
      // this.changeDetect.detectChanges();
      return {
        days: totalDays,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId, returnUrl: this.router.url } });
                  }
                });
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        this.openSignInModal();
      }
    });
  }

  onRegisterNow(data) {
    if (this.cognitoId) {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        this.checkAuthentication().then((isAuthorise) => {
          if (isAuthorise) {
            const params = {
              bidder__cognito_id__in: this.cognitoId
            }
            this.auctionService.getAuctionEsign(params).subscribe(
              (res: any) => {
                this.eSignReqData = res?.results as any[];
                this.esignStatus = (res && res?.results && (res?.results.length > 0) && (res?.results[0]?.status)) ? res?.results[0]?.status : '';
                // this.esignStatus = 'BUS';
                if (this.esignStatus != 'BUS') {
                  const dialogRef = this.dialog.open(ESignConfirmDialogComponent, {
                    width: '500px',
                    data: {
                      auction: this.auctionDataById.id,
                      bidder: this.cognitoId,
                      returnUrl: this.router.url
                    }
                  });
                }
                else {
                  this.auctionRegister(data);
                }
              });
          }
        });
      }
      else if (rolesArray.includes('Admin') || rolesArray.includes('SuperAdmin') || rolesArray.includes('ChannelPartner')) {
        const dialogRef = this.dialog.open(CustomerSearchModalComponent, {
          width: '600px',
          data: {
            type: 'Auction',
            data: data,
            heading: "Register for Auction",
            subHeading: "Does the user already exist on iQuippo?"
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result && result?.cognitoId) {
            this.checkCustomerKYC(result?.cognitoId).then((isAuthorise) => {
              if (isAuthorise) {
                const params = {
                  bidder__cognito_id__in: result?.cognitoId
                }
                this.auctionService.getAuctionEsign(params).subscribe(
                  (res: any) => {
                    this.eSignReqData = res?.results as any[];
                    this.esignStatus = res && res?.results && res?.results.length > 0 && res?.results[0].status ? res?.results[0].status : '';
                    if (this.eSignReqData && this.eSignReqData.length > 0) {
                      //this.esignStatus = 'BUS';
                      if (this.esignStatus == 'BUS') {
                        this.assistedMode = true;
                        this.auctionRegister(data, result?.cognitoId);
                      }
                      else {
                        this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
                      }
                    }
                    else {
                      this.initiateBUForm(result.cognitoId)
                    }
                  });
              }
            });
          }
        });
      }
      else {
        this.notify.error('Please login as Customer to proceed further with asset buy!!');
      }
    }
    else {
      this.openSignInModal();
    }
  }

  initiateBUForm(cognitoId) {
    this.loading = true;
    const params = {
      role_type: 1,
      bidder: cognitoId,
    }
    this.auctionService.postAuctionEsign(params).subscribe(
      (res: any) => {
        this.notify.success('The Bidder Undertaking of this customer has been successfully initiated.');
        setTimeout(() => {
          if (res && res?.status == 'EP') {
            this.patchESignReq(res?.reference_id);
          }
          else if (res && (res?.status == 'INI' || res?.status == 'UNS' || res?.status == 'TC')) {
            this.notify.success('The Bidder undertaking of this customer has already been initiated. Please get the customer completed their eSign.');
          }
          this.loading = false;
        }, 2000);
      }, err => {
        this.loading = false;
      });
  }

  patchESignReq(referenceId) {
    const params = {
      reference_id: referenceId,
      page_url: environment.mp1HomePage + 'customer/dashboard/e-sign-template/'
    }
    this.auctionService.patchAuctionEsign(params, referenceId).subscribe(
      (res: any) => {
        this.notify.success('The mail has been successfully sent to the customer!!');
      });
  }

  checkCustomerKYC(cognitoId) {
    return new Promise((resolved, reject) => {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');

      let payload = {
        cognito_id: cognitoId,
        role: 'Customer'
      }
      this.commonService.validateSeller(payload).subscribe(
        (res: any) => {
          if (!res.kyc_verified || !res.pan_address_proof) {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
              width: '500px',
              disableClose: true,
              data: {
                heading: 'KYC Details Incomplete',
                message: rolesArray.includes('ChannelPartner') ? 'Please ask user to complete the KYC in their My profile section' : 'You need to complete user KYC profile with iQuippo to make payments.',
                submessage: rolesArray.includes('ChannelPartner') ? '' : 'Click Yes if you want to proceed or No if you want to do this later. ',
                isRoleChannelPartner: rolesArray.includes('ChannelPartner') ? true : false
              }
            });

            dialogRef.afterClosed().subscribe(val => {
              if (val) {
                let rolesArray = this.storage.getStorageData("rolesArray", false);
                rolesArray = rolesArray.split(',');
                if (rolesArray.includes('Admin')) {
                  localStorage.setItem("userRole", 'Admin');
                }
                else if (rolesArray.includes('SuperAdmin')) {
                  localStorage.setItem("userRole", 'SuperAdmin');
                }
                else if (rolesArray.includes('ChannelPartner')) {
                  localStorage.setItem("userRole", 'ChannelPartner');
                }
                this.cognitoId = this.storage.getStorageData("cognitoId", false);
                this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
                  this.storage.setStorageData("UserInfo", res, true);
                });

                this.router.navigate(['/admin-dashboard/customer-management']);
              }
            })
            resolved(false);
            return;
          } else {
            resolved(true);
          }
        },
        err => {
          reject(false);
        }
      );

    });
  }

  auctionRegister(data, assistedCognitoId?) {
    this.checkEmdAvailability(data?.id).then((res) => {
      let paymentDetails = {
        auction_id: data?.id,
        asset_type: 'Used',
        case: 'Auction',
        payment_type: 'Partial',
        assistedMode: this.assistedMode
      }
      if (this.assistedMode) {
        paymentDetails['assistedCognitoId'] = assistedCognitoId;
      }
      this.storage.setSessionStorageData('paymentSession', true, false);
      this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
      this.router.navigate(['/customer/payment']);
    },
      err => {
        console.log(err);
      }
    );
  }

  ViewLotDetails(id, auctionid, lotId) {
    this.router.navigate([`/auctions/auction-lot`], { queryParams: { 'id': id, 'auctionid': auctionid, 'lotid': lotId } });
  }

  checkEmdAvailability(auctionID) {
    return new Promise((resolved, reject) => {
      this.auctionService.getEmdValues(auctionID).subscribe(
        (res: any) => {
          if (res?.count) {
            resolved(true);
          } else {
            this.notify.error('Don"t have emd against this auction, Please contact admin !!');
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
        }
      );
    });
  }

  checkInitialPayment(auctionID, parentId) {
    return new Promise((resolved, reject) => {
      let payload = {
        cognito_id: this.cognitoId
      }
      this.paymentService.getEmdPaymentStatus(payload, (parentId ? parentId : auctionID)).subscribe(
        (res: any) => {
          if (res?.redirect) {
            resolved(true);
          } else {
            this.notify.error('You have not made the requisite EMD payment to participate in this auction. Please click Register Now button to make the necessary payment.');
            reject(false);
          }
        },
        err => {
          console.log(err);
          reject(false);
        }
      );
    });
  }

  auctionbid(id, parentId) {
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
        if (isAuthorise) {
          this.checkInitialPayment(id, parentId).then((res) => {
            let payload = `auction_id=${id}&user_cognito_id=${this.cognitoId}&role='Customer'`;
            window.open(environment.auctionBaseUrl + "auction/integration/view-online-auction?" + payload, '_blank');
          });
        }
      });
    } else {
      this.openSignInModal();
    }
  }

  panelChange(sub) {
    this.page = 1;
    this.pageSize = 12;
    this.activeSubauctionId = sub?.id;
    this.getLotsDetailAccordingToSubAuction();
    let topGap: any = document.getElementById('header')?.offsetHeight;
    this.animate(document.scrollingElement || document.documentElement, "scrollTop", "", 0, (this.auctionAssets?.nativeElement?.offsetTop - topGap), 100, true);
  }

  onPageChange(event: PageEvent, type, subId) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    if (type == 'subauction') {
      this.getLotsDetailAccordingToSubAuction();
    }
    else if (type == 'filterSubAuction') {
      this.getLotsDetailAccordingToSubAuction(subId)
      //this.getFilterSubActionLot();
    }
    else {
      this.getLotsDetailAccordingToAuction(this.auctionDataById.id);
    }
    let topGap: any = document.getElementById('header')?.offsetHeight;
    this.animate(document.scrollingElement || document.documentElement, "scrollTop", "", 0, (this.auctionAssets?.nativeElement?.offsetTop - topGap), 100, true);
  }

  animate(elem, style, unit, from, to, time, prop) {
    if (!elem) {
      return;
    }
    var start = new Date().getTime(),
      timer = setInterval(function () {
        var step = Math.min(1, (new Date().getTime() - start) / time);
        if (prop) {
          elem[style] = (from + step * (to - from)) + unit;
        } else {
          elem.style[style] = (from + step * (to - from)) + unit;
        }
        if (step === 1) {
          clearInterval(timer);
        }
      }, 25);
    if (prop) {
      elem[style] = from + unit;
    } else {
      elem.style[style] = from + unit;
    }
  }

  openSignInModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Admin')) {
          localStorage.setItem("userRole", 'Admin');
        }
        else if (rolesArray.includes('SuperAdmin')) {
          localStorage.setItem("userRole", 'SuperAdmin');
        }
        else if (rolesArray.includes('ChannelPartner')) {
          localStorage.setItem("userRole", 'ChannelPartner');
        }

        this.cognitoId = this.storage.getStorageData("cognitoId", false);
        this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
          this.storage.setStorageData("UserInfo", res, true);
        });
      }
    })
  }

  trackByFn(index) {
    return index;
  }

  getFilterSubActionLot() {
    if (this.serverSideCategoryFilter || this.serverSideBrandFilter || this.serverSideYearFilter ||
      this.serverSideLocationFilter) {
      this.isFilterApply = true;
    }
    else {
      this.isFilterApply = false;
    }
    for (var i = 0; i < this.subAuction.length; i++) {
      let categoryParam = this.serverSideCategoryFilter == '' || this.serverSideCategoryFilter == undefined ?
        'lotasset__asset__category__in=' : 'lotasset__asset__category__in=' + this.serverSideCategoryFilter;
      let brandParam = this.serverSideBrandFilter == '' || this.serverSideBrandFilter == undefined ?
        '&lotasset__asset__brand__in=' : '&lotasset__asset__brand__in=' + this.serverSideBrandFilter;
      let yearParam = this.serverSideYearFilter == '' || this.serverSideYearFilter == undefined ?
        '&lotasset__asset__mfg_year__in=' : '&lotasset__asset__mfg_year__in=' + this.serverSideYearFilter;
      let locationParam = this.serverSideLocationFilter == '' || this.serverSideLocationFilter == undefined ?
        '&lotasset__asset__pin_code__city__state__in=' : '&lotasset__asset__pin_code__city__state__in=' + this.serverSideLocationFilter;
      let page = '&page=' + this.page;
      let limit = '&limit=' + this.pageSize;
      let queryParams = categoryParam + brandParam + yearParam + locationParam + page + limit;
      this.getLotsDetail(queryParams, this.subAuction[i].id);
    }
  }

  showMoreLess(type) {
    if (type == 'category') {
      this.categoryShow = !this.categoryShow;
      if (this.categoryShow) {
        this.categoryFilters = this.categoryFilters.slice(0, 5);
      }
      else {
        this.categoryFilters = this.originalCategory;

      }
    }
    else if (type == 'brand') {
      this.brandShow = !this.brandShow;
      if (this.brandShow) {
        this.brandFilters = this.brandFilters.slice(0, 5);
      }
      else {
        this.brandFilters = this.originalBrand;

      }
    }
    else if (type == 'location') {
      this.locationShow = !this.locationShow;
      if (this.locationShow) {
        this.locationFilters = this.locationFilters.slice(0, 5);
      }
      else {
        this.locationFilters = this.originalLocation;

      }
    }
    else if (type == 'year') {
      this.yearShow = !this.yearShow;
      if (this.yearShow) {
        this.yearFilters = this.yearFilters.slice(0, 5);
      }
      else {
        this.yearFilters = this.originalYear;

      }
    }
  }

  clearSelectedFilter() {
    this.serverSideCategoryFilter = '';
    this.serverSideBrandFilter = '';
    this.serverSideLocationFilter = '';
    this.serverSideYearFilter = '';

    /* Category */
    this.categoryFilters.forEach(element => {
      element.isSelected = false;
    });
    /* Brand */
    this.brandFilters.forEach(element => {
      element.isSelected = false;
    });

    /* location */
    this.locationFilters.forEach(element => {
      element.isSelected = false;
    });
    /* Year */
    this.yearFilters.forEach(element => {
      element.isSelected = false;
    });
    this.getFilterSubActionLot();
  }

  showSingleSubauction(subId) {
    this.page = 1;
    this.pageSize = 12;
    this.clearAllFilters();
    this.isSingleSubactionShow = true;
    this.signleSub = this.subAuction.filter(x => x.id == subId)[0];
    this.activeSubauctionId = subId;
    this.getLotsDetailAccordingToSubAuction();
  }

  viewAllSubAuction() {
    this.page = 1;
    this.pageSize = 12;
    this.clearAllFilters();
    this.activeSubauctionId = !this.isSingleSubactionShow && this.activeSubauctionId ? this.activeSubauctionId : this.subAuction[0].id;
    this.isSingleSubactionShow = false;
    this.firstActiveSubauction = "sub0"
    this.getLotsDetailAccordingToSubAuction();
  }

  clearAllFilters() {
    this.serverSideCategoryFilter = '';
    this.serverSideBrandFilter = '';
    this.serverSideLocationFilter = '';
    this.serverSideYearFilter = '';
    this.isFilterApply = false;
    /* Category */
    this.categoryFilters.forEach(element => {
      element.isSelected = false;
    });
    /* Brand */
    this.brandFilters.forEach(element => {
      element.isSelected = false;
    });

    /* location */
    this.locationFilters.forEach(element => {
      element.isSelected = false;
    });
    /* Year */
    this.yearFilters.forEach(element => {
      element.isSelected = false;
    });
  }

}
