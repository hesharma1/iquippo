import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyselfdetailsComponent } from './myselfdetails.component';

describe('MyselfdetailsComponent', () => {
  let component: MyselfdetailsComponent;
  let fixture: ComponentFixture<MyselfdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyselfdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyselfdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
