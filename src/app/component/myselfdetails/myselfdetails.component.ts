import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared-service.service';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { AccountService } from 'src/app/services/account';
import { AddressDetails, GetUserDto, UserProfileDTO, UserResponse } from 'src/app/shared/abstractions/user';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { environment } from 'src/environments/environment';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { PanRequest, PanResponse } from 'src/app/models/common/panRequestData.model';
import { PanVerifyPopupComponent } from '../pan-verify-popup/pan-verify-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { documentVerificationComponent } from '../customer/document-verification-confirmation/document-verification-confirmation.component';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';

@Component({
  selector: 'app-myselfdetails',
  templateUrl: './myselfdetails.component.html',
  styleUrls: ['./myselfdetails.component.css']
})
export class MyselfdetailsComponent implements OnInit {
  profileForm: FormGroup
  pinCodeResponse?: PincodeResponse;
  PanRequest?: PanRequest;
  PanResponse?: PanResponse;
  initialFirstName?: string = "";
  initialLastName?: string = "";
  isDupliateEmailID: boolean = false;
  isInvalidfile: boolean = false;
  userprofileData: UserProfileDTO;
  getUser: GetUserDto;
  userDataobj: UserResponse;
  pinCodeErrorFlag = false;
  isPanCheck: boolean = false;
  isPanExist: boolean = false;
  isEditable: boolean = false;
  isPanEditable: boolean = false;
  pincodeBlur: boolean = false;
  docImge: any;
  VerifiedDataobj: any;
  proofList = ["Voter Id", "Aadhar", "Driving License", "Passport", "Ration Card", "Bank Statement", "Utility Bills", "Trade License", "GSTN & Autority Certificate", "Registration Certificate"];
  countryList?: any;
  selectedCity?: CountryResponse;
  isCallCenter: any;
  minDate: any;
  isEmailReq?: boolean = false;
  isShowPanDoc: boolean = false;
  isShowDocProof: boolean = false;
  isEdit: boolean = false;
  cognitoId: string = '';
  uploadingImage: boolean = false;
  hideAppliedForPan: boolean = false;
  initialPan?: string = '';
  isSuperAdmin?: boolean = true;
  role?: string;
  isQueryPassed: any;
  isOption: boolean = false;
  returnUrl=''
  userRole: any;
  channelPartnerType: any;
  RmList: any;
  isrm: string='';
  constructor(private router: Router,private partnerDealerRegService: PartnerDealerRegistrationService,private fb: FormBuilder, private accountService: AccountService, private appRouteEnum: AppRouteEnum,
    private http: HttpClient, private activatedRoute: ActivatedRoute, public dialog: MatDialog, public sharedService: SharedService,
    private storage: StorageDataService, private spinner: NgxSpinnerService, public notificationservice: NotificationService,
    private notify: NotificationService, private datePipe: DatePipe, private s3: S3UploadDownloadService, private route: ActivatedRoute,
    public apiRouteService: ApiRouteService) {
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
    this.profileForm = fb.group({
      Representer: ['1', Validators.required],
      Gender: ['Male', Validators.required],
      AlternateEmailId: ['', Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)],
      documentId: new FormControl(''),
      rm: new FormControl(''),
      // PanCardNo: ['',Validators.required],
      // customFile: ['',Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      Dob: ['', Validators.required],
      address_proof: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl(''),
        docNumber: new FormControl(''),
        docImages: new FormControl(''),
      }),
      Mobile: ['', Validators.required],
      EmailId: ['', [Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]],
      StreetAddress1: ['', Validators.required],
      StreetAddress2: ['', Validators.required],
      addressId: new FormControl(''),
      PinCode: ['',
        [Validators.required,
        Validators.pattern(/[1-9]{1}[0-9]{5}$/),
        Validators.minLength(6),
        Validators.maxLength(6)]
      ],
      State: ['', Validators.required],
      City: ['', Validators.required],
      countryCode: ['', Validators.required],
      applied_for_pan: new FormControl(false, [Validators.required]),
      // address_proof: {
      //   docType: ['',[Validators.required]],
      //   docNumber: ['',[Validators.required]],
      //   documentId: ['',[Validators.required]],
      //   // docImages: ['',[Validators.required]],
      //   docImages: new FormGroup({
      //     front:new FormControl('',[Validators.required]),
      //     back:new FormControl('')
      //   })
      // },
      uan: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl('', []),
          back: new FormControl('')
        })
      }),
      tradelicense: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl('', []),
          back: new FormControl('')
        })
      }),
      pan: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl('', Validators.required),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      aadhar: this.fb.group({
        docType: new FormControl('aadhar'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      },{validator: numberonly('docNumber')}),
      passport: new FormGroup({
        docType: new FormControl('passport'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      voter: new FormGroup({
        docType: new FormControl('voter'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      dl: new FormGroup({
        docType: new FormControl('dl'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        documentId: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),

    }, {
      validator: [ageCheck('Dob')]
    });
    this.userprofileData = {};
    this.userprofileData.use_pan_name = "No";
    this.getUser = {};
    this.userDataobj = new UserResponse();
  }

  ngOnInit(): void {
    //this.spinner.show();
    this.role = this.storage.getStorageData("role", false);
    this.userRole = this.storage.getStorageData("userRole", false);

    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
      if (params['isrequired'] == 'true') {
        this.isOption = true;
      }
      else {
        this.isOption = false;
      }


      if (params['src']) {
        this.storage.clearStorageData("src");
        this.storage.setStorageData("src", params['src'], true);
        // this.sharedService.queryParamsAfterLogin = params;
        // this.storage.clearStorageData("aflparams");
        // this.storage.setStorageData("aflparams", params, true);
      }
      if (params['cognitoId']) {
        if (params['isCallCenter'] || this.role == this.apiRouteService.roles.callcenter) {
          this.storage.clearStorageData("isCallCenter");
          this.storage.setStorageData("isCallCenter", 1, true);
          this.storage.clearStorageData("cognitoUserId");
          this.storage.setStorageData("cognitoUserId", params['cognitoId'], true);
        }
        else {
          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData("cognitoId", params['cognitoId'].replace(/['"]+/g, ''), false);
        }

      }
      if (params['deviceKey']) {
        this.storage.clearStorageData("deviceKey");
        this.storage.setStorageData("deviceKey", params['deviceKey'], true);
      }
      if (params['refreshToken']) {
        this.storage.clearStorageData("refreshToken");
        this.storage.setStorageData("refreshToken", params['refreshToken'], true);
      }
      if (params['aoc']) {
        if (params['aoc'] == 'qtt') {
          this.isEmailReq = true;
          this.hideAppliedForPan = true;
          // this.profileForm.get('applied_for_pan')?.setValue(false);
          this.checkEmailField();
        }
        else {
          this.isEmailReq = false;
        }
      }
      if (params['isEdit']) {
        if (params['isEdit'] == '1') {
          this.isEdit = true;
        }
        else {
          this.isEdit = false;
        }
      }
      if(params['isrm']){
        this.isrm = params['isrm'];
        this.getRmList();
      }
    });
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.bindFlags();
    this.userprofileData.customerDetails = [];
    this.userprofileData.address = []
    this.isCallCenter = this.storage.getStorageData("isCallCenter", true);

    if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
      this.userprofileData.cognitoId = this.storage.getStorageData("cognitoUserId", true);
      this.userprofileData.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    }
    else {
      this.userprofileData.cognitoId = this.storage.getStorageData("cognitoId", false);
    }
    this.getUserDetailsfn();
    //  this.checkValidationRole();

  }


  test() { 
    //console.log(this.profileForm.value);
  }
  getRmList() {
    this.partnerDealerRegService.getRmListbyPartnership(this.apiRouteService.partnerContactRegistered).subscribe(
      (res: any) => {
        console.log(res)
        this.RmList = res.results
      })
  }
  verifyPanData() {
    var panRequest = new PanRequest();
    if (this.profileForm.get('applied_for_pan')?.value == false &&
      this.profileForm?.get('pan')?.get('docNumber')?.valid &&
      (this.profileForm.get('pan')?.get('docNumber')?.value != "")) {
      if (this.initialPan?.toLowerCase() != this.profileForm.get('pan')?.get('docNumber')?.value.toLowerCase()) {
        //this.spinner.show();
        if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
          panRequest.cognitoId = this.storage.getStorageData("cognitoUserId", true);
        }
        else {
          panRequest.cognitoId = this.storage.getStorageData("cognitoId", false);
        }
        panRequest.panCardNo = this.profileForm.get('pan')?.get('docNumber')?.value;
        panRequest.documentId = this.profileForm.get('pan')?.get('documentId')?.value;
        panRequest.represent = "1";

        this.accountService.verifyPanCardDetails(panRequest).subscribe(
          resp => {
             
            this.PanResponse = resp as PanResponse;
            //console.log(this.PanResponse.body);
            if (this.PanResponse.body?.statusCode == "200") {
              let msg: any = this.PanResponse.body.message;
              this.notify.success(msg)
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.replaceVerifyPanName();

                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  this.userprofileData.use_pan_name = "No";
                }
              })
            }
            else if (this.PanResponse.body?.statusCode == "508") {
              // NamemisMatchPopup
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  //console.log(result);
                  this.replaceVerifyPanName();
                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  this.userprofileData.use_pan_name = "No";
                }
              })
            }
            else if (this.PanResponse.body?.statusCode == "513") {
              //this.profileForm.get('pan')?.get('docNumber')?.setValue("");
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg);
            }
            else if (this.PanResponse.body?.statusCode == "506" || this.PanResponse.body?.statusCode == "512") {
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg)
            }
            else {
              let msg: any = this.PanResponse?.body?.message;
              this.notify.warn(msg);
            }
          },
          error => {
            console.log(error);
          })
      }
    }
  }

  replaceVerifyPanName() {
    this.profileForm.get('FirstName')?.setValue(this.PanResponse?.body?.firstName);
    this.profileForm.get('LastName')?.setValue(this.PanResponse?.body?.lastName);
  }


  checkValidationRole() {
    this.route.params.subscribe((params: Params): void => {
      this.isQueryPassed = params['createListing'];
    });
    if (this.isQueryPassed != null && this.isQueryPassed != undefined) {
      this.profileForm.get('pan')?.get('docNumber')?.setValidators(Validators.required);
      this.profileForm.get('pan')?.get('docNumber')?.updateValueAndValidity();
      this.profileForm.get('pan')?.get('docImages')?.setValidators(Validators.required);
      this.profileForm.get('pan')?.get('docImages')?.get('front')?.updateValueAndValidity();
    }
  }

  checkaddressFun(ev: KeyboardEvent) {
    // let regex= /^[ A-Za-z0-9@.'%\/#&-]*$/;
    // if (!regex.test(ev.key)) {
    //     ev.preventDefault();
    //     //  var newval= this.profileForm.get('StreetAddress1')?.value.replace(/^[ A-Za-z0-9@.'%\/#&-]*$/, "");
    //     //  this.profileForm.get('StreetAddress1')?.setValue(newval);
    //   } 
    let k = ev.keyCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 9 || k == 32 || (k >= 48 && k <= 57));
  }

  checkPan() {
    var pan = this.profileForm.get('pan')?.get('docNumber')?.value?.toLowerCase();
    if (pan == "") {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ required: true });
    }
    else if (pan?.length != 10) {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    }
    else if (pan.length == 10 && pan.charAt(3) != 'p') {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ wrong: true });
    }
    else {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors(null);
    }
  }

  getUserDetailsfn() {
    //this.getUser.userId='002bbeaf-6d23-4766-bd9c-477703599e67';
    //var paramaterarray = this.storage.getStorageData("aflparams", true);
    var userId = '';
    if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
      userId = this.storage.getStorageData("cognitoUserId", true);
    }
    else {
      userId = this.storage.getStorageData("cognitoId", false);
    }
    this.profileForm.get('FirstName')?.disable();
    this.profileForm.get('LastName')?.disable();
    //  this.profileForm.get('Dob')?.disable();
    this.profileForm.get('countryCode')?.disable();
    this.profileForm.get('Mobile')?.disable();
    this.http.get(environment.corporateDetails + '?userId=' + userId)
      .subscribe((resp: UserResponse) => {
        this.userDataobj = resp;
        if (this.userDataobj?.basic_user_details?.cognitoId != null) {
          if (this.userDataobj?.user_details?.gender != undefined && this.userDataobj?.user_details?.gender != null && this.userDataobj?.user_details?.gender != "")
            this.profileForm.get('Gender')?.setValue(this.userDataobj?.user_details?.gender);
          // else{
          //   this.profileForm.get('Gender')?.setValue('Male');
          // }
          this.profileForm.get('AlternateEmailId')?.setValue(this.userDataobj?.user_details?.alternateEmailId);
          if (this.userDataobj?.user_details?.represent != undefined && this.userDataobj?.user_details?.represent != null && this.userDataobj?.user_details?.represent != "" && !this.isEdit) {

            this.profileForm.get('Representer')?.setValue(this.userDataobj?.user_details?.represent);
          }
          // else
          // this.profileForm.get('Representer')?.setValue('1');
          if (this.userDataobj?.user_details?.represent == "2" && !this.isEdit) {
            if (this.isEmailReq){
              if(this.userRole==this.apiRouteService.roles.customer)
              {
              this.router.navigate([`./` + this.appRouteEnum.CorporateDetails], { queryParams: { aoc: 'qtt' ,returnUrl:this.returnUrl} });
              }
              else{
              this.router.navigate([`./` + this.appRouteEnum.AdminCorporateDetails], { queryParams: { aoc: 'qtt' ,returnUrl:this.returnUrl} });
              }
            }
            else{
              if(this.userRole==this.apiRouteService.roles.customer)
              {
              this.router.navigate([`./` + this.appRouteEnum.CorporateDetails],{ queryParams: { returnUrl:this.returnUrl} });
              }
              else{
              this.router.navigate([`./` + this.appRouteEnum.AdminCorporateDetails],{ queryParams: { returnUrl:this.returnUrl} });
              }
            }
          }

          // if(this.userDataobj?.address_proof!=null) {
          //   console.log(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
          //   
          //   this.profileForm.get('address_proof')?.setValue(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
          // }
          if (this.userDataobj?.address_proof != null) {
            //console.log(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));

            // this.corporateProfileForm.get('address_proof')?.setValue(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
            this.profileForm.get('address_proof')?.get('documentId')?.setValue(this.userDataobj?.address_proof?.documentId);
            this.profileForm.get('address_proof')?.get('docType')?.setValue(this.userDataobj?.address_proof?.docType);
            this.profileForm.get('address_proof')?.get('docNumber')?.setValue(this.userDataobj?.address_proof?.docNumber);
            this.profileForm.get('address_proof')?.get('docImages')?.setValue(this.userDataobj?.address_proof?.docImages);
          }
          this.initialFirstName = this.userDataobj?.basic_user_details?.firstName;
          this.initialLastName = this.userDataobj?.basic_user_details?.lastName;
          this.profileForm.get('FirstName')?.setValue(this.userDataobj?.basic_user_details?.firstName);
          this.profileForm.get('LastName')?.setValue(this.userDataobj?.basic_user_details?.lastName);
          this.profileForm.get('Dob')?.setValue(this.userDataobj?.basic_user_details?.dateOfBirth);
          this.profileForm.get('Mobile')?.setValue(this.userDataobj?.basic_user_details?.mobileNumber); 
          if(this.isrm !='')
          this.profileForm.get('rm')?.setValue(Number(this.userDataobj?.basic_user_details?.relationship_manager)); 

          //  var cc={
          //    'prefixCode':this.userDataobj?.basic_user_details?.prefixCode,
          //    'countryCode':this.userDataobj?.basic_user_details?.countryCode
          //  }
          var cc = this.countryList?.find((x: any) => x.prefixCode == this.userDataobj?.basic_user_details?.prefixCode);
          this.selectedCity = cc;
          this.profileForm.get('countryCode')?.setValue(cc);
          this.checkIndiaPin();
          this.profileForm.get('EmailId')?.setValue(this.userDataobj?.basic_user_details?.email);
          if (this.userDataobj?.basic_user_details?.email != '' && this.userDataobj?.basic_user_details?.email != null && this.userDataobj?.basic_user_details?.email != undefined)
            this.profileForm.get('EmailId')?.disable();

          this.profileForm.get('PinCode')?.setValue(this.userDataobj?.basic_user_details?.pinCode);
          if (this.userDataobj?.basic_user_details?.applied_for_pan?.toLowerCase() == "true") {
            this.profileForm.get('applied_for_pan')?.setValue(true);
            this.profileForm?.get('pan')?.get('docNumber')?.setErrors(null);
            this.profileForm?.get('pan')?.get('docNumber')?.setValue('');
            this.profileForm?.get('pan')?.get('docNumber')?.disable();
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setValue('');
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.disable();
          }
          else {
            this.profileForm.get('applied_for_pan')?.setValue(false);
            this.profileForm?.get('pan')?.get('docNumber')?.setErrors({ required: true });
            this.profileForm?.get('pan')?.get('docNumber')?.enable();
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.enable();
          }


          this.getpinCodefn();
          if (this.userDataobj?.adress_details != undefined) {
            if (this.userDataobj?.adress_details[0]?.addressId != "" && this.userDataobj?.adress_details[0]?.addressId != null && this.userDataobj?.adress_details[0]?.addressId != undefined) {
              this.profileForm.get("addressId")?.setValue(this.userDataobj?.adress_details[0]?.addressId);
            }
            this.profileForm.get("StreetAddress1")?.setValue(this.userDataobj?.adress_details[0]?.addressLine1);
            this.profileForm.get('StreetAddress2')?.setValue(this.userDataobj?.adress_details[0]?.addressLine2);
            this.profileForm.get('City')?.setValue(this.userDataobj?.adress_details[0]?.city);
            this.profileForm.get('State')?.setValue(this.userDataobj?.adress_details[0]?.state);
          }
          if (this.userDataobj?.doc_details != undefined) {
            let panindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'pan');
            if (panindex != -1) {
              this.profileForm.get('pan')?.get('docType')?.setValue(this.userDataobj?.doc_details[panindex]?.docType)
              this.profileForm.get('pan')?.get('documentId')?.setValue(this.userDataobj?.doc_details[panindex]?.documentId)

              this.initialPan = this.userDataobj?.doc_details[panindex]?.docNumber;
              this.profileForm.get('pan')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[panindex]?.docNumber)
              if (this.userDataobj?.doc_details[panindex]?.docNumber != undefined && this.userDataobj?.doc_details[panindex]?.docNumber != "" && this.userDataobj?.doc_details[panindex]?.docNumber != null) {
                this.profileForm.get('pan')?.get('docNumber')?.disable();
                this.isPanEditable = true;
              }
              this.profileForm.get('pan')?.get('name')?.setValue(this.userDataobj?.doc_details[panindex]?.karzaRes?.result?.returnName)
              if (this.userDataobj?.doc_details[panindex]?.docUrl != undefined) {
                this.profileForm.get('pan')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[panindex]?.docUrl?.front)
              }
            }
            let aadharindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'aadhar');
            if (aadharindex != -1) {
              this.profileForm.get('aadhar')?.get('docType')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docType)
              this.profileForm.get('aadhar')?.get('documentId')?.setValue(this.userDataobj?.doc_details[aadharindex]?.documentId)
              this.profileForm.get('aadhar')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docNumber)
              if (this.userDataobj?.doc_details[aadharindex]?.docUrl != undefined) {
                this.profileForm.get('aadhar')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.front)
                this.profileForm.get('aadhar')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.back)
              }
            }
            let passportindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'passport');
            if (passportindex != -1) {
              this.profileForm.get('passport')?.get('docType')?.setValue(this.userDataobj?.doc_details[passportindex]?.docType)
              this.profileForm.get('passport')?.get('documentId')?.setValue(this.userDataobj?.doc_details[passportindex]?.documentId)
              this.profileForm.get('passport')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[passportindex]?.docNumber)
              if (this.userDataobj?.doc_details[passportindex]?.docUrl != undefined) {
                this.profileForm.get('passport')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.front)
                this.profileForm.get('passport')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.back)
              }
            }
            let voterindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'voter');
            if (aadharindex != -1) {

              this.profileForm.get('voter')?.get('docType')?.setValue(this.userDataobj?.doc_details[voterindex]?.docType)
              this.profileForm.get('voter')?.get('documentId')?.setValue(this.userDataobj?.doc_details[voterindex]?.documentId)
              this.profileForm.get('voter')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[voterindex]?.docNumber)
              if (this.userDataobj?.doc_details[voterindex]?.docUrl != undefined) {
                this.profileForm.get('voter')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.front)
                this.profileForm.get('voter')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.back)
              }
            }
            let dlindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'dl');
            if (dlindex != -1) {
              this.profileForm.get('dl')?.get('docType')?.setValue(this.userDataobj?.doc_details[dlindex]?.docType)
              this.profileForm.get('dl')?.get('documentId')?.setValue(this.userDataobj?.doc_details[dlindex]?.documentId)
              this.profileForm.get('dl')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[dlindex]?.docNumber)
              if (this.userDataobj?.doc_details[dlindex]?.docUrl != undefined) {
                this.profileForm.get('dl')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.front)
                this.profileForm.get('dl')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.back)
              }
            }
          }
          // this.profileForm.get('pan')?.setValue()
          // this.profileForm.get('PanCardNo')?.setValue(this.userDataobj?.doc_details[3]?.docNumber);
          // this.profileForm.get('customFile')?.clearValidators();
          // this.profileForm.get('customFile')?.updateValueAndValidity();

          this.isPanExist = true;
           
          // 

        }
        if (this.userDataobj?.user_details?.represent != undefined && this.userDataobj?.user_details?.represent != "" && this.userDataobj?.user_details?.represent != null) {
          //this.profileForm.disable();
          this.isEditable = true;
        }
        if (this.hideAppliedForPan) {

          if (this.profileForm.get('pan')?.get('docNumber')?.value == '') {
            this.profileForm.get('pan')?.get('docNumber')?.enable();
            this.profileForm.get('applied_for_pan')?.setValue(false);
          }
        }
        if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter || this.role == this.apiRouteService.roles.superadmin) {
          this.isEditable = false;
          if (this.profileForm.get('pan')?.get('docNumber')?.value != '' && this.role == this.apiRouteService.roles.superadmin) {
            if (this.profileForm.get('applied_for_pan')?.value)
              this.isPanEditable = false;
            this.isSuperAdmin = false;
            this.profileForm.get('pan')?.get('docNumber')?.enable();
          }

          this.profileForm.get('FirstName')?.enable();
          this.profileForm.get('LastName')?.enable();
          this.profileForm.get('countryCode')?.enable();
          this.profileForm.get('EmailId')?.enable();
          this.profileForm.get('countryCode')?.disable();
          // this.profileForm.get('Representer')?.setValue('1');
        }

      }, error => {
        // 
        console.log(error);
         

      })


  }

  checkIndiaPin() {
    let cntryCode = (this.profileForm.get('countryCode')?.value).prefixCode;
    if (cntryCode != "+91") {
      //console.log("not india");
      this.profileForm?.get('State')?.enable();
      this.profileForm?.get('City')?.enable();

    }
    else {
      //console.log("ind");
      this.profileForm?.get('State')?.disable();
      this.profileForm?.get('City')?.disable();

    }
  }

  getpinCodefn() {
    if (this.profileForm.get('PinCode')?.value != null && this.profileForm.get('PinCode')?.value != undefined && this.profileForm.get('PinCode')?.value != '' && (this.profileForm.get('countryCode')?.value).prefixCode == '+91') {
      var x = this.profileForm.get('PinCode')?.value;
      if (x > 99999 && x < 1000000) {
        this.accountService.getPinCode(this.profileForm.get('PinCode')?.value)
          .subscribe((resp) => {
            //console.log(resp);
            this.pinCodeResponse = resp as PincodeResponse;
            //console.log("my",this.pinCodeResponse);

            if (this.pinCodeResponse.code === 258) {

              this.pinCodeErrorFlag = false;
              this.profileForm.get('State')?.setValue(this.pinCodeResponse.pincode?.state);
              this.profileForm.get('City')?.setValue(this.pinCodeResponse.pincode?.city);
            }
            else if (this.pinCodeResponse.code === 259) {
              // console.log(this.pinCodeResponse.code);
              this.pinCodeErrorFlag = true;
              this.profileForm.get('State')?.setValue('');
              this.profileForm.get('City')?.setValue('');
            }
            else {
              console.log("errorrr");
              this.pinCodeErrorFlag = true;
              this.profileForm.get('State')?.setValue('');
              this.profileForm.get('City')?.setValue('');
            }
          }, error => {
            // 
            console.log(error);
            this.pinCodeErrorFlag = true;
            this.profileForm.get('State')?.setValue('');
            this.profileForm.get('City')?.setValue('');
          })
      }
    }
  }

  onGenderChange(value: any) {
    if (this.isEditable)
      return
    this.profileForm.get('Gender')?.setValue(value);
  }

  navcorporate() {
    if (this.isEditable)
      return
    if (this.isEmailReq)
    {
      this.router.navigate([`./` + this.appRouteEnum.CorporateDetails], { queryParams: { aoc: 'qtt','isrequired': this.isOption, isEdit: '1',returnUrl:this.returnUrl} } );
    }
    else
    {
      this.router.navigate([`./` + this.appRouteEnum.CorporateDetails], { queryParams: {'isrequired': this.isOption, isEdit: '1',returnUrl:this.returnUrl} } );
    }
   }


  validateAlternateEmailIdfn() {
    const value = { ...this.profileForm.getRawValue() };
    if ((value.AlternateEmailId.length > 0) && value?.EmailId == value?.AlternateEmailId) {
      this.profileForm.get('AlternateEmailId')?.setErrors({ exists: true });
      this.isDupliateEmailID = true;
    }
    else {
      if (!this.profileForm.get('AlternateEmailId')?.hasError('pattern')) {
        this.profileForm.get('AlternateEmailId')?.setErrors(null);
      }
      else {
        this.profileForm.get('AlternateEmailId')?.setErrors({ exists: false });
      }
      this.isDupliateEmailID = false;
    }
  }

  toggleEditable(event: any) {
    this.profileForm.get('applied_for_pan')?.setValue(event.checked);
    if (event.checked) {
      this.profileForm?.get('pan')?.get('docNumber')?.setErrors(null);
      this.profileForm?.get('pan')?.get('docNumber')?.setValue('');
      this.profileForm?.get('pan')?.get('docNumber')?.disable();
      this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
      this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setValue('');
      this.profileForm?.get('pan')?.get('docImages')?.get('frontView')?.setValue('');
      this.profileForm?.get('pan')?.get('docImages')?.get('front')?.disable();
    }
    else {
      this.profileForm?.get('pan')?.get('docNumber')?.setErrors({ required: true });
      this.profileForm?.get('pan')?.get('docNumber')?.enable();
      this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
      this.profileForm?.get('pan')?.get('docImages')?.get('front')?.enable();
    }
    this.profileForm.get('FirstName')?.setValue(this.initialFirstName);
    this.profileForm.get('LastName')?.setValue(this.initialLastName);
  }

  panFrontImageError = false;
  checkPanImageOnSubmit() {
    if (this.profileForm.get('pan')?.get('docNumber')?.value != '' && this.profileForm.get('pan')?.get('docImages')?.get('front')?.value == "") {
      this.panFrontImageError = true;
      this.notify.error("Please upload pan image.")
      return false;
    }
    else {
      this.panFrontImageError = false;
      return true;
    }
  }

  onSubmit() {
    if (!this.checkEmailField()) {
      this.profileForm.get('EmailId')?.markAsTouched();
      return;
    }
    if (this.profileForm.invalid) {


      return;
    }
    // if(this.profileForm.get('pan')?.get('docNumber')?.value!='' && this.profileForm.get('pan')?.get('docImages')?.get('front')?.value=="") {
    //   this.panFrontImageError= true;
    //   return;
    // }
    // else {
    //   this.panFrontImageError= false;
    // }
    if (!this.checkPanImageOnSubmit()) {
      return;
    }

    //this.spinner.show();

    //console.log(this.profileForm.value);
    if (this.profileForm.get('applied_for_pan')?.value) {
      if (this.profileForm?.get('aadhar')?.get('docNumber')?.value == "" && this.profileForm?.get('passport')?.get('docNumber')?.value == "" && this.profileForm?.get('voter')?.get('docNumber')?.value == "" && this.profileForm?.get('dl')?.get('docNumber')?.value == "") {
        this.notify.warn("If you applied for PAN, until then please enter one proof of Identity other than PAN.")
         
        return;
      }
    }
   
    const value = { ...this.profileForm.getRawValue() };
    // 
    //    let docObj={ 
    //   "pan" : {
    //     "docType" : "pan",
    //     "docNumber" : value?.PanCardNo,
    //     "name":value?.FirstName.concat(value?.LastName.toString()),
    //     "docImages":
    //      {"front" :this.docImge, 
    //      "back" : "" },
    //   },
    //   "dl" :{
    //     "docType" : "dl",
    //     "docNumber" : value?.DL,
    //     "dob":value?.dateOfBirth,
    //     "docImages" :{ "front" : " ", "back" : "" }

    //   },
    //   'voter' : {
    //     "docType" : "voter",
    //     "docNumber" : "GFD0284858",
    //     "docImages" :{ "front" : "", "back" : ""}
    //   },
    // };
    // let docObj={ 
    //     "pan" : {
    //       "docType" : "pan",
    //       "docNumber" : "dssd",
    //       "name":value?.FirstName.concat(value?.LastName.toString()),
    //       "docImages":
    //        {"front" :this.docImge, 
    //        "back" : "" },
    //     },
    //   };

    var aadharCard = this.profileForm?.get('aadhar')?.value;
    if (aadharCard != null && aadharCard != undefined) {
      if (aadharCard.docNumber != null && aadharCard.docNumber != '' && aadharCard.docNumber != undefined) {
        if ((aadharCard.docImages.front == "" || aadharCard.docImages.front.length < 0) && (aadharCard.docImages.back == "" || aadharCard.docImages.back.length < 0)) {
          this.profileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.profileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors(null);
          this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var passportCard = this.profileForm?.get('passport')?.value;
    if (passportCard != null && passportCard != undefined) {
      if (passportCard.docNumber != null && passportCard.docNumber != '' && passportCard.docNumber != undefined) {
        if ((passportCard.docImages.front == "" || passportCard.docImages.front.length < 0) && (passportCard.docImages.back == "" || passportCard.docImages.back.length < 0)) {
          this.profileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.profileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.profileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors(null);
          this.profileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }

    var voterCard = this.profileForm?.get('voter')?.value;
    if (voterCard != null && voterCard != undefined) {
      if (voterCard.docNumber != null && voterCard.docNumber != '' && voterCard.docNumber != undefined) {
        if ((voterCard.docImages.front == "" || voterCard.docImages.front.length < 0) && (voterCard.docImages.back == "" || voterCard.docImages.back.length < 0)) {
          this.profileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.profileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.profileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors(null);
          this.profileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var dlCard = this.profileForm?.get('dl')?.value;
    if (dlCard != null && dlCard != undefined) {
      if (dlCard.docNumber != null && dlCard.docNumber != '' && dlCard.docNumber != undefined) {
        if ((dlCard.docImages.front == "" || dlCard.docImages.front.length < 0) && (dlCard.docImages.back == "" || dlCard.docImages.back.length < 0)) {
          this.profileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.profileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.profileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors(null);
          this.profileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }

    this.profileForm?.get('pan')?.get('name')?.setValue(value?.FirstName.concat(' ' + value?.LastName.toString()));
    value.pan.name = value?.FirstName.concat(' ' + value?.LastName.toString());

    var docObj;
    if (this.profileForm.get('applied_for_pan')?.value) {
      docObj = {
        "aadhar": JSON.parse(JSON.stringify(this.profileForm?.get('aadhar')?.value)),
        "passport": JSON.parse(JSON.stringify(this.profileForm?.get('passport')?.value)),
        "voter": JSON.parse(JSON.stringify(this.profileForm?.get('voter')?.value)),
        "dl": JSON.parse(JSON.stringify(this.profileForm?.get('dl')?.value))
      };
    }
    else {
      docObj = {
        "pan": JSON.parse(JSON.stringify(value?.pan)),
        "aadhar": JSON.parse(JSON.stringify(this.profileForm?.get('aadhar')?.value)),
        "passport": JSON.parse(JSON.stringify(this.profileForm?.get('passport')?.value)),
        "voter": JSON.parse(JSON.stringify(this.profileForm?.get('voter')?.value)),
        "dl": JSON.parse(JSON.stringify(this.profileForm?.get('dl')?.value))
      };
    }

    this.userprofileData.doc_details = docObj;
    if (!this.profileForm.get('applied_for_pan')?.value) {
      this.userprofileData.doc_details.pan.docImages.frontView = '';
    }
    this.userprofileData.doc_details.aadhar.docImages.frontView = '';
    this.userprofileData.doc_details.aadhar.docImages.backView = '';
    this.userprofileData.doc_details.passport.docImages.frontView = '';
    this.userprofileData.doc_details.passport.docImages.backView = '';
    this.userprofileData.doc_details.voter.docImages.frontView = '';
    this.userprofileData.doc_details.voter.docImages.backView = '';
    this.userprofileData.doc_details.dl.docImages.frontView = '';
    this.userprofileData.doc_details.dl.docImages.backView = '';

    this.userprofileData.applied_for_pan = value?.applied_for_pan;

    this.userprofileData.represent = this.profileForm.get('Representer')?.value;
    this.userprofileData.gender = value?.Gender;
    var dob = this.datePipe.transform(value?.Dob, 'yyyy-MM-dd');


    this.userprofileData.customerDetails = [];
    this.userprofileData.address = [];
    if(this.isrm !=''){
      this.userprofileData.customerDetails?.push({
        'firstName': value?.FirstName,
        'lastName': value?.LastName,
        'dob': dob?.toString(),
        'mobileNumber': value?.Mobile,
        'relationship_manager': value?.rm,
        'emailId': value?.EmailId,
        'alternateEmailId': value?.AlternateEmailId
      });
    }
    else{
    this.userprofileData.customerDetails?.push({
      'firstName': value?.FirstName,
      'lastName': value?.LastName,
      'dob': dob?.toString(),
      'mobileNumber': value?.Mobile,
      'emailId': value?.EmailId,
      'alternateEmailId': value?.AlternateEmailId
    });
    }
    this.userprofileData.address?.push({
      'addressLine1': value?.StreetAddress1,
      'addressLine2': value?.StreetAddress2,
      'pincode': value?.PinCode,
      'state': value?.State,
      'city': value?.City,
      'addressId': value?.addressId,
    });

    if (this.profileForm.get('address_proof')?.get('docNumber')?.value != null &&
      this.profileForm.get('address_proof')?.get('docNumber')?.value != "" &&
      this.profileForm.get('address_proof')?.get('docNumber')?.value != undefined) {
      if (this.profileForm?.get('address_proof')?.get('docImages')?.value == "" || this.profileForm?.get('address_proof')?.get('docImages')?.value == undefined) {
         
        this.notify.warn("Please upload Address Proof image.")
        return;
      }

      this.userprofileData.address_proof = JSON.parse(JSON.stringify(value.address_proof));
    }

    //console.log("data",JSON.stringify(this.userprofileData));
    
    if((this.profileForm.get('aadhar')?.get('docNumber')?.value == '' || this.profileForm.get('aadhar')?.get('docNumber')?.value == null) &&
    (this.profileForm.get('dl')?.get('docNumber')?.value == '' || this.profileForm.get('dl')?.get('docNumber')?.value == null) &&
    (this.profileForm.get('voter')?.get('docNumber')?.value == '' || this.profileForm.get('voter')?.get('docNumber')?.value == null) &&
    (this.profileForm.get('passport')?.get('docNumber')?.value == '' || this.profileForm.get('passport')?.get('docNumber')?.value == null )){
      const dialogRef = this.dialog.open(documentVerificationComponent, {
        width: '500px',
        data: {
          message: "Kindly upload either Aadhaar, Driving License, Passport or voter card for completing KYC verification.",
        },
      });
    } else {
      this.submitProfileData();
    }

  }

  submitProfileData() {
    this.http.post(environment.kycDocumentVerifyUrl, JSON.stringify(this.userprofileData))
      .subscribe((resp: any) => {
        //console.log(resp);
        this.VerifiedDataobj = JSON.parse(JSON.stringify(resp.body));

        if (resp?.statusCode == 200) {
          // if (this.VerifiedDataobj?.pan?.isVerified==1 || (this.VerifiedDataobj?.aadhar?.isVerified==1 || this.VerifiedDataobj?.voter?.isVerified==1 || this.VerifiedDataobj?.dl?.isVerified==1 || this.VerifiedDataobj?.passport?.isVerified==1)){
          this.notify.success("User saved or verified successfully");
          if (this.isOption == true) {
            //this.router.navigate([`./communication`]);
            if(this.userRole==this.apiRouteService.roles.customer)
            {
              this.router.navigate([`./customer/dashboard/communication`], { queryParams: { 'isrequired': true ,returnUrl:this.returnUrl} });
            }
            else{
              this.router.navigate([`./admin-dashboard/communication`], { queryParams: { 'isrequired': true ,returnUrl:this.returnUrl} });
            }
             
          }
          else {
            if(this.userRole==this.apiRouteService.roles.customer)
            {
              if(this.isrm!=undefined && this.isrm!=''){
              this.router.navigate([`./customer/dashboard/communication`], { queryParams: {returnUrl:this.returnUrl,isrm:this.isrm} });
              }
              else
              this.router.navigate([`./customer/dashboard/communication`], { queryParams: {returnUrl:this.returnUrl} });
            }
            else{
              if(this.isrm!=undefined && this.isrm!=''){
                this.router.navigate([`./admin-dashboard/communication`], { queryParams: {returnUrl:this.returnUrl,isrm:this.isrm} });
              }
              else
              this.router.navigate([`./admin-dashboard/communication`], { queryParams: {returnUrl:this.returnUrl} });
            }
             
          }


          //  }
          //  else{
          //   this.notify.success("User saved or verified successfully");
          //   this.router.navigate([`./communication`]);
          //    
          //  }

        }

        else {
           
          var msg = JSON.parse(this.VerifiedDataobj);
          this.notify.error(msg?.message);
          this.userprofileData.address = [];
          this.userprofileData.customerDetails = [];
        }

        // if(this.VerifiedDataobj.pan.isVerified==1){
        //  4
        //   // alert("User  saved or verified successfully");
        //   this.notify.success("User saved or verified successfully");
        //   this.router.navigate([`./communication`]);
        // }

        // else{
        //  // alert();
        //   
        // }
      }, error => {
        // 
        console.log(error);
         

      })
  }
  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp') && file.size <= 5242880) {
      this.isInvalidfile = true;
      this.uploadingImage = true;
      //  this.isShowPanDoc = false;
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log('type', file.type);
        this.profileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue(reader.result);
      }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
        var id = this.storage.getStorageData("cognitoUserId", true);
        var image_path = environment.bucket_parent_folder + "/" + id + '/' + tab + '/' + side + '.' + fileExt;
      }
      else {
        var id = this.storage.getStorageData("cognitoId", false);
        var image_path = environment.bucket_parent_folder + "/" + id + '/' + tab + '/' + side + '.' + fileExt;
      }



      var uploaded_file = await this.s3.prepareFileForUploadWithoutPublicAccess(file, image_path);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(uploaded_file?.Key);

      }
      else {
        this.profileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue('');
        this.notify.error("Upload Failed");
      }
      // controlName=reader.result;
      setTimeout(() => {
        this.checkPanImageOnSubmit();
      }, 0);
      this.uploadingImage = false;
      //};
    } else {
      // this.isShowPanDoc = true;
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({ imgIssue: true });

    }
  }

  handleUploadAddress(event: any) {
    const file = event.target.files[0];
    if (file.size <= 5242880 && (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp')) {
      this.isInvalidfile = true;
      this.isShowDocProof = false;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log(reader.result);
        this.profileForm.get('address_proof')?.get('docImages')?.setValue(reader.result);
        // controlName=reader.result;

      };
    } else {
      this.isShowDocProof = true;
      this.profileForm.get('address_proof')?.get('docImages')?.setValue('');
    }
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.profileForm.get('countryCode')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
      this.checkIndiaPin();
    }, err => {
      this.notificationservice.error(err);
       
    })
  }

  updateCountryCode(city: any) {
    this.profileForm.get('countryCode')?.setValue(city)
  }

  checkEmailField() {
    if (this.isEmailReq) {
      let email = this.profileForm?.get('EmailId')?.value;
      if (!email) {
        this.profileForm.get('EmailId')?.setErrors({ required: true });
        return false;
      }
      else if (this.profileForm.get('EmailId')?.hasError('pattern')) {
        this.profileForm.get('EmailId')?.setErrors(null);
        this.profileForm.get('EmailId')?.setErrors({ pattern: true });
        return false;
      }
      else {
        this.profileForm.get('EmailId')?.setErrors(null);
        return true;
      }
    }
    else if (this.profileForm.get('EmailId')?.hasError('pattern')) {
      this.profileForm.get('EmailId')?.setErrors(null);
      this.profileForm.get('EmailId')?.setErrors({ pattern: true });
      return false;
    }
    else {
      this.profileForm.get('EmailId')?.setErrors(null);
      return true;
    }
  }
}

