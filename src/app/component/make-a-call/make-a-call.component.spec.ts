import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeACallComponent } from './make-a-call.component';

describe('MakeACallComponent', () => {
  let component: MakeACallComponent;
  let fixture: ComponentFixture<MakeACallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakeACallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeACallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
