import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { CommonService } from 'src/app/services/common.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-make-a-call',
  templateUrl: './make-a-call.component.html',
  styleUrls: ['./make-a-call.component.css']
})
export class MakeACallComponent implements OnInit {

  newForm: FormGroup;
  formControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  stateList: any = [];
  loggedUserData: any;
  cognitoId?: string;
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<MakeACallComponent>,
    private cmnService: CommonService, private agreementServiceService: AgreementServiceService, public notify: NotificationService, public spinner: NgxSpinnerService) {
    this.newForm = new FormGroup({
      user: new FormControl(null),
      first_name: new FormControl(null),
      last_name: new FormControl(null),
      contact_number: new FormControl(null, Validators.compose([Validators.pattern('[1-9+][0-9]+'), Validators.minLength(10), Validators.maxLength(10), Validators.required])),
      email_address: new FormControl(null, [Validators.email]),
      address: new FormControl(null),
      pin_code: new FormControl(null),
      city: new FormControl(null),
      state: new FormControl(null),
      comment: new FormControl(null),
      location: new FormControl(null),
      request_from: new FormControl(null)
    })
    this.newForm.patchValue({
      request_from :data
    });

  }

  ngOnInit(): void {
    this.loggedUserData = localStorage.getItem('userData');
    this.loggedUserData = JSON.parse(this.loggedUserData);
    console.log(this.loggedUserData);

    let cogId = localStorage.getItem('cognitoId');
    if (cogId != '' && cogId != null) {
      this.cognitoId = cogId;
    }
    this.newForm?.get('user')?.setValue(this.cognitoId);
    this.newForm?.get('first_name')?.setValue(this.loggedUserData.attributes.given_name);
    this.newForm?.get('last_name')?.setValue(this.loggedUserData.attributes.family_name);
    this.newForm?.get('email_address')?.setValue(this.loggedUserData.attributes.email);
    this.newForm?.get('contact_number')?.setValue(this.trimPrefix(this.loggedUserData.attributes.phone_number, '+91'));
    this.newForm?.get('address')?.setValue(this.loggedUserData.attributes.address);
    this.newForm?.get('pin_code')?.setValue(this.loggedUserData.attributes["custom:pincode"]);
    this.getLocationData();
  }


  submit() {
    console.log("new form value", this.newForm.value);
    this.cmnService.postCallBack(this.newForm.value).pipe(finalize(() => { })).subscribe(
      resp => {
        this.notify.success('Thanks for submitting your request. We will get back to you shortly. ')
      },
      err => {
        console.log(err?.message);
        this.notify.error(err?.message);
      }
    )
  }

  getLocationData() {
    let pincode = this.newForm.get('pin_code')?.value;
    if (pincode && pincode.length > 4) {
      let queryParam = `search=${pincode}`;
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
          this.stateList = res.results;
          if (this.stateList && this.stateList.length > 0) {
            this.newForm?.get('city')?.setValue(this.stateList[0].city?.name);
            this.newForm?.get('state')?.setValue(this.stateList[0].city.state?.name);
            this.newForm?.get('location')?.setValue(this.stateList[0].id);
          }
        },
        (err) => {
          console.error(err);
        }
      );
    }
  }

  getErrorMessage() {
    if (this.formControl.hasError('required')) {
      return 'Required field';
    }
    else if (this.formControl.hasError('email')) {
      return 'Not a valid email';
    }
    return 'Not a valid email';
  }

  backClick() {
    this.dialogRef.close();
  }

  trimPrefix(str, prefix) {
    if (str.startsWith('+')) {
      return str.slice(prefix.length);
    } else {
      return str
    }
  }

}
