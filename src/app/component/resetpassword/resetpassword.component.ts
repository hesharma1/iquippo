import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PwdresetdialogComponent } from '../pwdresetdialog/pwdresetdialog.component';
import { FormBuilder, Validators } from '@angular/forms';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { StorageDataService } from "../../services/storage-data.service";
import { CheckPasswordMatch } from '../../utility/custom-validators/forgot-password';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  constructor(public dialog: MatDialog, private fb: FormBuilder, private awsConfig: AwsAuthService,
    private storageDataService: StorageDataService) { }

  ngOnInit(): void {
  }
  resetpassForm = this.fb.group({
    Password: ['', [Validators.required]],
    confirmPassword: ['', [Validators.required]],
  }, {
    validator: [CheckPasswordMatch('Password', 'confirmPassword')]
  }
  )


  onSubmit() {
    this.resetpassForm.markAllAsTouched();
    let mobile = this.storageDataService.getStorageData('mobile', false);
    let forgetotp = this.storageDataService.getStorageData('forgetOTP', false);
    let newPass = this.resetpassForm.get('Password')?.value;
    if (this.resetpassForm.valid) {
      this.awsConfig.awsForgotConfirmationPassword(mobile, forgetotp, newPass)
        .then((resp) => {
          // console.log("final", resp);
          this.dialog.open(PwdresetdialogComponent, {
            disableClose: false,
            width: '500px',
          });
        })
        .catch(err => {
          console.log(err);
        })
    }
    //  console.log(this.resetpassForm.value);
  }

}
