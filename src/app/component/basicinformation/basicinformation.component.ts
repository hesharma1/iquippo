import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validator,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { Scope } from 'graphql-modules';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { v4 as uuidv4 } from 'uuid';
import { MustMatch, ageCheck } from 'src/app/utility/validations/must-match-validation';
import { AccountService } from 'src/app/services/account';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { SharedService } from 'src/app/services/shared-service.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { CookieService } from 'ngx-cookie-service';

import { BehaviorSubject, Observable, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from 'src/app/services/loader.service';
// import { SignInProp } from '../signin/signin-prop.service.component';


@Component({
  selector: 'app-basicinformation',
  templateUrl: './basicinformation.component.html',
  styleUrls: ['./basicinformation.component.css'],
})
export class BasicinformationComponent implements OnInit {


  // private _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp());  


  private CookieValue: string = '';
  response: any;
  isSubmitted: boolean = false;
  fieldTextType: boolean = false;
  minAge: any;
  pinCodeErrorFlag = false;
  pinCodeResponse?: PincodeResponse;
  socialId?: string;
  fieldTextType1: boolean = false;
  firstnameBlur: boolean = false;
  lastnameBlur: boolean = false;
  pincodeBlur: boolean = false;
  newDOB: any;
  minDate: any;
  returnUrl= '';
  constructor(
    private cookieService: CookieService,
    private awsConfig: AwsAuthService,
    private fb: FormBuilder,
    private router: Router,
    private storage: StorageDataService,
    private datePipe: DatePipe,
    private accountService: AccountService,
    public sharedService: SharedService,
    private appRouteEnum: AppRouteEnum,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private apiRouteService: ApiRouteService,
    private loaderService: LoaderService
  ) {
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
  }
  message?: string;
  isProfile: boolean = false;

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
      this.returnUrl = params.returnUrl;
    });
    var phone = localStorage.getItem('phone');
    var prefixCode = localStorage.getItem('prefixCode');
    var countryCode = localStorage.getItem('countryCode');
    if (phone != null && phone != undefined)
      this.basicinfoform.get('mobile')?.setValue(prefixCode + phone);
    let google_data = this.storage.getStorageData("google_data", true);
    // console.log(google_data);
    if (google_data != undefined) {
      this.basicinfoform.get('firstname')?.setValue(google_data?.firstName);
      this.basicinfoform.get('lastname')?.setValue(google_data?.lastName);
      this.basicinfoform.get('Email')?.setValue(google_data?.email);

    }

  }

  basicinfoform: FormGroup = this.fb.group({
    mobile: new FormControl('', [
      Validators.required,
      //Validators.minLength(10),
      //Validators.maxLength(14),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/),
    ]),
    confirmpassword: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(256)]),
    lastname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(256)]),
    dob: new FormControl('', [Validators.required]),
    pincode: new FormControl('', [
      Validators.required,
      Validators.pattern(/[1-9]{1}[0-9]{5}$/),
      Validators.minLength(6),
      Validators.maxLength(6),
    ]),
    Email: new FormControl('', [Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]),
    //agree: new FormControl('', [Validators.required]),
    agree: new FormControl(''),
  },
    {
      validator: [MustMatch('password', 'confirmpassword'), ageCheck('dob')]
    });

  creatAccount() {
    //this.spinner.show();
    // this.awsConfig.
    // let dobvar:Date= this.basicinfoform.get('dob')?.value;
    // console.log(dobvar.getFullYear(),dobvar.getMonth());
    // this.getpinCodefn();
    this.isSubmitted = true;
    this.basicinfoform.markAllAsTouched();
    var dobdetail = this.checkDobField();

    if (!dobdetail) {

      return;
    }
    var emailField = this.checkEmailField();
    if (!emailField) {

      return;
    }
    // if (this.basicinfoform?.get('agree')?.value == false) {
    //   return;
    // }
    this.loaderService.isLoading.next(true);
    var prefixCode = localStorage.getItem('prefixCode');
    if (prefixCode == null || prefixCode == undefined) {
      prefixCode = '';
    }
    var countryCode = localStorage.getItem('countryCode');
    if (countryCode == null || countryCode == undefined) {
      countryCode = '';
    }

    this.sharedService.isShow = true;
    var username = uuidv4();
    this.socialId = this.storage.getStorageData('socialId', true);
    if (this.socialId == undefined || this.socialId == null) {
      this.socialId = '';
    }
    var dob = this.basicinfoform.get('dob')?.value;
    this.newDOB = this.datePipe.transform(dob, 'yyyy-MM-dd');
    var a = this.awsConfig
      .signUp(
        username,
        this.basicinfoform.get('password')?.value,
        this.basicinfoform.get('Email')?.value,
        this.basicinfoform.get('mobile')?.value,
        this.basicinfoform.get('lastname')?.value,
        this.basicinfoform.get('firstname')?.value,
        this.newDOB,
        this.basicinfoform.get('pincode')?.value,
        this.socialId,
        prefixCode,
        countryCode,
        'Customer'
      )
      .then((res) => {
        this.response = res as any;
        // console.log(this.response.signInUserSession.idToken.jwtToken);
        if (this.response.userConfirmed) {
          this.storage.clearStorageData('username');
          this.storage.setStorageData("username", this.response.user.username, true);
          // this.storage.setStorageData("basicData",this.response.user.username,true);
          setTimeout(() => {
            this.signin();
          }, 2000);

          //this.router.navigate([`./dashboard`]);
        }
      })
      .catch((err) => {

        console.log(err);

        this.message = err.message;
        this.loaderService.isLoading.next(false);
      });
    //this.router.navigate([`./basicinformation`]);
  }

  checkEmailField() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
      var pro = this.sharedService.queryParams.aoc;
      if (pro != undefined && pro != null && pro == "qtt") {
        let email = this.basicinfoform?.get('Email')?.value;
        if (!email) {
          this.basicinfoform.get('Email')?.setErrors({ required: true });
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }
  }
  checkDobField() {
    let dob = this.basicinfoform?.get('dob')?.value;
    if (!dob) {
      this.basicinfoform.get('dob')?.setErrors({ required: true });
      return false;
    }
    else {
      return true;
    }
  }
  consoleitem(item: any) {
    //console.log(item);
  }
  // pincode resp
  // success code- 258,
  // pin not found errorcode- 259
  // other error- 5xx

  getpinCodefn() {
    if (localStorage.getItem('countryCode') != "IN") {
      this.pinCodeErrorFlag = false;
      this.pincodeBlur = false;
      if (this.basicinfoform.get('pincode')?.value != "" && this.basicinfoform.get('pincode')?.value != undefined && this.basicinfoform.get('pincode')?.value != null)
        this.basicinfoform.get('pincode')?.setErrors(null);
      return;
    }

    this.accountService.getPinCode(this.basicinfoform.get('pincode')?.value)
      .subscribe((resp: any) => {
        // console.log(resp);
        this.pinCodeResponse = resp as PincodeResponse;
        //console.log("my",this.pinCodeResponse.pincode);

        if (this.pinCodeResponse.code === 258) {
          // console.log(this.pinCodeResponse.code);
          this.pinCodeErrorFlag = false;
        }
        else if (this.pinCodeResponse.code === 259) {
          // console.log(this.pinCodeResponse.code);
          this.pinCodeErrorFlag = true;
        }
        else {
          console.log("errorrr");
          this.pinCodeErrorFlag = true;
        }
      }, (error: any) => {
        console.log(error);
        this.pinCodeErrorFlag = true;

      })
  }

  signin() {
    this.awsConfig
      .awsLoginWithPassword(
        this.basicinfoform.get('mobile')?.value,
        this.basicinfoform.get('password')?.value
      )
      .then((res) => {
        this.isSubmitted = true;
        this.response = res as any;
        // console.log(this.response.signInUserSession.idToken.jwtToken);
        if (
          this.response.signInUserSession.idToken.jwtToken != null &&
          this.response.signInUserSession.idToken.jwtToken != ''
        ) {
          // console.log(this.response);

          // let obj: SignInProp = new SignInProp();
          // obj.UserID = this.response;
          // obj.LoginTime = 0;

          // this._oUserSubject = new BehaviorSubject<SignInProp>(obj);

          // this._oUserSubject.next(obj);

          //this.UserID = this._oUserSubject.getValue().UserID;



          // start - saving UID to cookie

          /*var d = new Date();
          d.setTime(d.getTime() + environment.ExpiryTimeInMS);
          
          this.cookieService.set(
            "userData",
            this.response,
            d
          ); */
          //this.CookieValue = this.cookieService.get("UID");
          //alert(this.CookieValue);

          // End - saving UID to cookie

          this.storage.clearStorageData("userData");
          this.storage.setStorageData("userData", this.response, true);
          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData('cognitoId', this.response.username, false);
          this.storage.setStorageData('sessionTime', this.response?.signInUserSession?.idToken?.payload?.exp, false);
          this.storage.setStorageData('sessionId', this.response?.signInUserSession?.idToken?.jwtToken, false);
          if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
            var pro = this.sharedService.queryParams.aoc;
            if (pro != undefined && pro != null && pro == "qtt") {
              this.isProfile = true;
            }
            else {
              this.isProfile = false;
            }
          }
          var address_proof_available = this.response?.attributes["custom:pan_address_proof"];
          if (address_proof_available != undefined && address_proof_available != null && address_proof_available != '' && this.isProfile) {
            this.storage.clearStorageData("address_proof_available");
            this.storage.setStorageData('address_proof_available', address_proof_available, true);
          }
          var roleArray = this.response?.signInUserSession?.idToken?.payload["cognito:groups"];
          if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
            this.storage.setStorageData('rolesArray', roleArray, false);
            if (roleArray.indexOf(this.apiRouteService.roles['superadmin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['superadmin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['admin']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['admin'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['callcenter']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['callcenter'], false);
            }
            else if (roleArray.indexOf(this.apiRouteService.roles['rm']) != -1) {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['rm'], false);
            }
            else {
              this.storage.clearStorageData("role");
              this.storage.setStorageData('role', this.apiRouteService.roles['customer'], false);
            }
          }
          if (this.isProfile) {
            this.loaderService.isLoading.next(false);
            this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.response.username, src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
            // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { deviceKey: this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), refreshToken: this.response.signInUserSession.refreshToken.token, cognitoId: this.response.username, src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
          }
          else {
            // this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId:this.response.username} });
            this.loaderService.isLoading.next(false);
            this.router.navigate(['/home'], { queryParams: {returnUrl:this.returnUrl}});
            //this.sharedService.redirecteToMP1(this.response.signInUserSession.accessToken.jwtToken, this.response.signInUserSession.refreshToken.token, this.response.pool.storage.getItem(this.response.keyPrefix + '.' + this.response.username + '.deviceKey'), this.response.signInUserSession.idToken.jwtToken, true);
          }
        }
      })
      .catch((err) => {
        this.message = err.message;
        // console.log(err)
      });
    // let token=this.response.idToken.jwtToken;

    // if()
    //  this.message= "login Successful";
  }

}
