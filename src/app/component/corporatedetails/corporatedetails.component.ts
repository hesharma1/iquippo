import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { AccountService } from 'src/app/services/account';
import { AddressDetails, GetUserDto, UserProfileDTO, UserResponse } from 'src/app/shared/abstractions/user';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { MatSlideToggle, MatSlideToggleChange } from '@angular/material/slide-toggle';
import { PanRequest, PanResponse } from 'src/app/models/common/panRequestData.model';
import { DatePipe } from '@angular/common';
import { PanVerifyPopupComponent } from '../pan-verify-popup/pan-verify-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { documentVerificationComponent } from '../customer/document-verification-confirmation/document-verification-confirmation.component';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';

@Component({
  selector: 'app-corporatedetails',
  templateUrl: './corporatedetails.component.html',
  styleUrls: ['./corporatedetails.component.css']
})
export class CorporatedetailsComponent implements OnInit {
  isOption: boolean = false;
  base64textString: any;
  corporateProfileForm: FormGroup;
  isInvalidfile: boolean = false;
  pinCodeResponse?: PincodeResponse;
  userprofileData: UserProfileDTO;
  getUser: GetUserDto;
  deletedAddress: string[] = [];
  userDataobj: any;
  pinCodeErrorFlag = false;
  docImge: any;
  VerifiedDataobj: any;
  countryList?: any;
  selectedCity?: CountryResponse;
  selectedData = [];
  isPanExist: boolean = false;
  isPanCheck: boolean = false;
  isShowPanDoc: boolean = false;
  isShowDocProof: boolean = false;
  returnUrl=''
  paramaterarray: any;
  isCallCenter: any;
  AddressFormArray: any;
  initialPan: string = '';
  proofList = ["Voter Id", "Aadhar", "Driving License", "Passport", "Ration Card", "Bank Statement",
    "Utility Bills", "Trade License", "GSTN & Autority Certificate",
    "Registration Certificate"];
  PanResponse?: PanResponse;
  initialFirstName: any;
  initialLastName: any;
  isEmailReq?: boolean = false;
  isEditable: boolean = false;
  isPanEditable: boolean = false;
  isEdit: boolean = false;
  cognitoId: string = "";
  uploadingImage: boolean = false;
  isEntityEdittable: boolean = true;
  hideAppliedForPan: boolean = false;
  isSuperAdmin: boolean = true;
  role?: string;
  minDate: any = new Date();
  constructor(private storage: StorageDataService, public sharedService: SharedService, private router: Router, public dialog: MatDialog, private datePipe: DatePipe, private fb: FormBuilder, private accountService: AccountService, private httpClient: HttpClient, private notify: NotificationService, private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute, private appRouteEnum: AppRouteEnum, private s3: S3UploadDownloadService, private apiRouteService: ApiRouteService) {
    this.corporateProfileForm = this.fb.group({
      Representer: ['2'],
      //Gender:['M'],
      AddressArray: new FormArray([]),
      RelationshipEntity: ['Proprietor', Validators.required],
      EntityType: ['Proprietorship', Validators.required],
      // PanCardNo: ['',Validators.required],
      address_proof: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl(''),
        docNumber: new FormControl(''),
        docImages: new FormControl(''),
      }),
      enableMSME: [true],
      MSMENumber: ['', Validators.required],
      documentId: new FormControl(''),
      CompanyName: ['', Validators.required],
      // customFile: ['',Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      DateoOfIncorporation: ['', Validators.required],
      Mobile: ['', Validators.required],
      EmailId: ['', [Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]],
      // StreetAddress1: ['', Validators.required],
      // StreetAddress2: ['', Validators.required],
      // PinCode: ['', Validators.required],
      // State: ['', Validators.required],
      // City:['', Validators.required],
      // GSTNo:['', Validators.required],
      countryCode: ['', Validators.required],
      applied_for_pan: new FormControl(false, [Validators.required]),
      pan: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('pan'),
        docNumber: new FormControl('', Validators.required),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      aadhar: this.fb.group({
        documentId: new FormControl(''),
        docType: new FormControl('aadhar'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }, { validator: numberonly('docNumber')}),
      passport: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('passport'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      voter: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('voter'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      dl: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('dl'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      uan: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('uan'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      tradelicense: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl('tradelicense'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
    });
    this.userprofileData = {};
    this.userprofileData.use_pan_name = "No";
    this.getUser = {};

  }

  test() {
    //.log(this.corporateProfileForm.value);
  }

  verifyPanData() {
    var panRequest = new PanRequest();
    if (this.corporateProfileForm.get('applied_for_pan')?.value == false &&
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.valid &&
      (this.corporateProfileForm.get('pan')?.get('docNumber')?.value != "")) {

      if (this.initialPan.toLowerCase() != this.corporateProfileForm.get('pan')?.get('docNumber')?.value.toLowerCase()) {
        //this.spinner.show();
        if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
          panRequest.cognitoId = this.storage.getStorageData("cognitoUserId", true);
        }
        else {
          panRequest.cognitoId = this.storage.getStorageData("cognitoId", false);
        }
        panRequest.panCardNo = this.corporateProfileForm.get('pan')?.get('docNumber')?.value;
        panRequest.documentId = this.corporateProfileForm.get('pan')?.get('documentId')?.value;
        panRequest.represent = "2";

        this.accountService.verifyPanCardDetails(panRequest)
          .subscribe(resp => {
             
            this.PanResponse = resp as PanResponse;
            //console.log(this.PanResponse.body);
            if (this.PanResponse.body?.statusCode == "200") {
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
                if (result) {
                  //console.log(result);
                  this.replaceVerifyPanName();
                  let msg: any = this.PanResponse?.body?.message;
                  this.notify.success(msg);
                  if (this.corporateProfileForm?.get('EntityType')?.value == 'Proprietorship') {
                    this.corporateProfileForm.get('FirstName')?.disable();
                    this.corporateProfileForm.get('LastName')?.disable();
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors(null);
                    }
                  }
                  else {
                    this.corporateProfileForm.get('CompanyName')?.disable();
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      if (this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.value == '')
                        this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors({ required: true });
                    }
                  }
                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  if (this.corporateProfileForm?.get('EntityType')?.value == 'Proprietorship') {
                    // this.corporateProfileForm.get('FirstName')?.enable(); 
                    // this.corporateProfileForm.get('LastName')?.enable(); 
                    this.corporateProfileForm.get('FirstName')?.setValue(this.initialFirstName);
                    this.corporateProfileForm.get('LastName')?.setValue(this.initialLastName);
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors(null);
                    }
                  }
                  else {
                    this.corporateProfileForm.get('CompanyName')?.enable();
                    //this.corporateProfileForm.get('CompanyName')?.setValue('');

                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      if (this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.value == '')
                        this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors({ required: true });
                    }
                  }
                  this.userprofileData.use_pan_name = "No";
                }
              })

            }
            else if (this.PanResponse.body?.statusCode == "508") {
              // NamemisMatchPopup
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
                if (result) {
                  //console.log(result);
                  this.replaceVerifyPanName();
                  if (this.corporateProfileForm?.get('EntityType')?.value == 'Proprietorship') {
                    // this.corporateProfileForm.get('FirstName')?.disable(); 
                    // this.corporateProfileForm.get('LastName')?.disable(); 
                    // this.corporateProfileForm.get('CompanyName')?.enable();
                    // this.corporateProfileForm.get('CompanyName')?.setValue('');
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors(null);
                    }
                  }
                  else {
                    this.corporateProfileForm.get('CompanyName')?.disable();
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      if (this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.value == '')
                        this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors({ required: true });
                    }
                  }
                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  if (this.corporateProfileForm?.get('EntityType')?.value == 'Proprietorship') {
                    // this.corporateProfileForm.get('FirstName')?.enable(); 
                    // this.corporateProfileForm.get('LastName')?.enable(); 
                    this.corporateProfileForm.get('FirstName')?.setValue(this.initialFirstName);
                    this.corporateProfileForm.get('LastName')?.setValue(this.initialLastName);
                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors(null);
                    }
                  }
                  else {
                    this.corporateProfileForm.get('CompanyName')?.enable();
                    // this.corporateProfileForm.get('CompanyName')?.setValue('');

                    for (let index = 0; index < this.corporateProfileForm?.get('AddressArray')?.value.length; index++) {
                      var b = '' + index + '';
                      if (this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.value == '')
                        this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('GSTNo')?.setErrors({ required: true });
                    }
                  }
                  this.userprofileData.use_pan_name = "No";
                }
              })
            }
            else if (this.PanResponse.body?.statusCode == "513") {
              this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue("");
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg);
            }
            else if (this.PanResponse.body?.statusCode == "506" || this.PanResponse.body?.statusCode == "512") {
              this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue("");
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg);
            }
            else {
              let msg: any = this.PanResponse?.body?.message;
              this.notify.warn(msg);
            }

          },
            error => {
              console.log(error);
              this.notify.warn(error);
            })
      }
    }
  }

  replaceVerifyPanName() {
    if (this.corporateProfileForm?.get('EntityType')?.value == 'Proprietorship') {
      this.corporateProfileForm.get('FirstName')?.setValue(this.PanResponse?.body?.firstName);
      this.corporateProfileForm.get('LastName')?.setValue(this.PanResponse?.body?.lastName);
    }
    else {
      this.corporateProfileForm.get('CompanyName')?.setValue(this.PanResponse?.body?.firstName);
    }
  }

  ngOnInit(): void {
    this.role = this.storage.getStorageData("role", false);
    this.userprofileData.corporateDetails = [];
    this.userprofileData.address = [];
    this.selectedData = [];
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
      if (params['isrequired'] == 'true') {
        this.isOption = true;
      }
      else {
        this.isOption = false;
      }
});
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['aoc']) {
        if (params['aoc'] == 'qtt') {
          this.isEmailReq = true;
          this.hideAppliedForPan = true;

          this.checkEmailField();
        }
        else {
          this.isEmailReq = false;
        }
      }
      if (params['isEdit']) {
        if (params['isEdit'] == '1') {
          this.isEdit = true;
        }
        else {
          this.isEdit = false;
        }
      }
    });
    // this.userprofileData.cognitoId="3e7472d7-d2c0-486c-bfe5-af18818a0753";
    this.bindFlags();
    //this.paramaterarray = this.storage.getStorageData("aflparams", true);
    //this.userprofileData.cognitoId=this.paramaterarray.cognitoId;//"d52258f6-2927-4bbc-9bdb-2f6f0e5d4979"
    this.isCallCenter = this.storage.getStorageData("isCallCenter", true);
    if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
      this.userprofileData.cognitoId = this.storage.getStorageData("cognitoUserId", true);
      this.userprofileData.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    }
    else {
      this.userprofileData.cognitoId = this.storage.getStorageData("cognitoId", false);
    }
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    // let userId='002bbeaf-6d23-4766-bd9c-477703599e67';
    //   this.accountService.getUserDetails(userId)
    //   .subscribe((resp)=>{
    //    console.log(resp);


    // },error=>{

    //   console.log(error);


    // });
    this.AddressFormArray = <FormArray>this.corporateProfileForm.controls['AddressArray'];

    this.getUserDetailsfn();

  }

  // onChange(ob: MatSlideToggleChange) {
  //   console.log(ob.checked);
  //   let matSlideToggle: MatSlideToggle = ob.source;	
  //   console.log(matSlideToggle.color);
  //   console.log(matSlideToggle.required);
  // } 
  msmeCheckbox(e: MatSlideToggleChange) {
    // console.log(e)
    // console.log(e.checked);
    // let matSlideToggle: MatSlideToggle = e.source;	
    // console.log(matSlideToggle.color);
    // console.log(matSlideToggle.required);
  }

  getUserDetailsfn() {
    var userId = "";
    if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
      userId = this.storage.getStorageData("cognitoUserId", true);
    }
    else {
      userId = this.storage.getStorageData("cognitoId", false);
    }
    if (this.corporateProfileForm.get('EntityType')?.value == 'Proprietorship') {
      this.corporateProfileForm.get('FirstName')?.disable();
      this.corporateProfileForm.get('LastName')?.disable();
    }

    this.httpClient.get(environment.corporateDetails + '?userId=' + userId)
      .subscribe((resp: UserResponse) => {
        //console.log(resp);
        this.userDataobj = resp;
        //  console.log( this.userDataobj.adress_details[0].addressLine1);

        if (this.userDataobj.basic_user_details.cognitoId != null) {
          // cognitoId: "3e7472d7-d2c0-486c-bfe5-af18818a0753"
          // companyName: "Rajesh Kumar"
          // createdAt: "2020-12-31T15:15:17.584170"
          // dateOfIncorporation: "Fri May 08 1992 00:00:00 GMT+0530 (India Standard Time)"
          // entityType: "Private Ltd.Co"
          // mobileNumber: "9625398989"
          // msme: "12121212121212"
          // relationshipWithEntity: "Employee"
          // represent: "2"
          //var date = dateFormat(new Date("Thu Oct 14 2010 00:00:00 GMT 0530 (India Standard Time)"), 'dd/mm/yyyy');



          //console.log("before",this.corporateProfileForm);
          this.corporateProfileForm?.get('CompanyName')?.setValue(this.userDataobj?.user_details?.companyName);
          if (this.corporateProfileForm.get('EntityType')?.value == 'Proprietorship') {
            this.initialFirstName = this.userDataobj?.basic_user_details?.firstName;
            this.initialLastName = this.userDataobj?.basic_user_details?.lastName;
            this.corporateProfileForm.get('FirstName')?.setValue(this.userDataobj?.basic_user_details?.firstName);
            this.corporateProfileForm.get('LastName')?.setValue(this.userDataobj?.basic_user_details?.lastName);
          }
          this.corporateProfileForm?.get('DateoOfIncorporation')?.setValue(this.userDataobj?.user_details?.dateOfIncorporation);
          this.corporateProfileForm?.get('MSMENumber')?.setValue(this.userDataobj?.user_details?.msme);
          this.corporateProfileForm.get('EmailId')?.setValue(this.userDataobj?.basic_user_details?.email);
          if (this.userDataobj?.basic_user_details?.email != '' && this.userDataobj?.basic_user_details?.email != null && this.userDataobj?.basic_user_details?.email != undefined)
            this.corporateProfileForm.get('EmailId')?.disable();
          //console.log(this.corporateProfileForm);

          var cc = this.countryList?.find((x: any) => x.prefixCode == this.userDataobj?.basic_user_details?.prefixCode);
          this.selectedCity = cc;
          this.corporateProfileForm.get('countryCode')?.setValue(cc);
          // this.checkIndiaPin();

          // this.corporateProfileForm?.get('applied_for_pan')?.setValue(this.userDataobj?.user_details?.applied_for_pan);  // also update msme radio button
          if (this.userDataobj?.basic_user_details?.applied_for_pan?.toLowerCase() == "true") {
            this.corporateProfileForm.get('applied_for_pan')?.setValue(true);
            this.corporateProfileForm?.get('pan')?.get('docNumber')?.setErrors(null);
            this.corporateProfileForm?.get('pan')?.get('docNumber')?.setValue('');
            this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
            this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
            this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setValue('');
            this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.disable();
          }
          else {
            this.corporateProfileForm.get('applied_for_pan')?.setValue(false);
            this.corporateProfileForm?.get('pan')?.get('docNumber')?.setErrors({ required: true });
            this.corporateProfileForm?.get('pan')?.get('docNumber')?.enable();
            this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
            this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.enable();
          }


          if (this.userDataobj?.user_details?.msme != "" && this.userDataobj?.user_details?.msme != undefined && this.userDataobj?.user_details?.msme != null) {
            this.corporateProfileForm.get('MSMENumber')?.setValue(this.userDataobj?.user_details?.msme);
            this.corporateProfileForm?.get('enableMSME')?.setValue(true);
            this.corporateProfileForm?.get('MSMENumber')?.setErrors(null);
            this.corporateProfileForm?.get('MSMENumber')?.setErrors(null);
            this.corporateProfileForm?.get('enableMSME')?.setErrors(null);
          }
          else {
            this.corporateProfileForm.get('MSMENumber')?.setValue('');
            this.corporateProfileForm?.get('enableMSME')?.setValue(false);
            // this.corporateProfileForm?.get('MSMENumber')?.setErrors(null);
            this.corporateProfileForm?.get('MSMENumber')?.disable();
            this.corporateProfileForm?.get('enableMSME')?.setErrors(null);
          }
          if (this.userDataobj?.user_details?.represent == "1" && !this.isEdit) {
            if (this.isEmailReq)
              this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { aoc: 'qtt' } });
            else
              this.router.navigate([`./` + this.appRouteEnum.Profile]);
          }

          if (this.userDataobj?.address_proof != null) {
            //console.log(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
            // this.corporateProfileForm.get('address_proof')?.setValue(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
            this.corporateProfileForm.get('address_proof')?.get('documentId')?.setValue(this.userDataobj?.address_proof?.documentId);
            this.corporateProfileForm.get('address_proof')?.get('docType')?.setValue(this.userDataobj?.address_proof?.docType);
            this.corporateProfileForm.get('address_proof')?.get('docNumber')?.setValue(this.userDataobj?.address_proof?.docNumber);
            this.corporateProfileForm.get('address_proof')?.get('docImages')?.setValue(this.userDataobj?.address_proof?.docImages);
          }
          this.corporateProfileForm?.get('Mobile')?.setValue(this.userDataobj?.basic_user_details?.mobileNumber);
          if (this.userDataobj?.basic_user_details?.mobileNumber != null && this.userDataobj?.basic_user_details?.mobileNumber != '' && this.userDataobj?.basic_user_details?.mobileNumber != undefined) {
            this.corporateProfileForm?.get('Mobile')?.disable();
            this.corporateProfileForm?.get('countryCode')?.disable();
          }
          if (this.userDataobj?.user_details?.entityType != undefined)
            this.corporateProfileForm?.get('EntityType')?.setValue(this.userDataobj?.user_details?.entityType);
          if (this.userDataobj?.user_details?.relationshipWithEntity != undefined)
            this.corporateProfileForm?.get('RelationshipEntity')?.setValue(this.userDataobj?.user_details?.relationshipWithEntity)
          // if(this.userDataobj?.adress_details.length>0){
          // this.corporateProfileForm?.get('PinCode')?.setValue(this.userDataobj?.adress_details[0]?.pincode);
          // this.corporateProfileForm?.get("StreetAddress1")?.setValue(this.userDataobj?.adress_details[0]?.addressLine1);
          // this.corporateProfileForm?.get('StreetAddress2')?.setValue(this.userDataobj?.adress_details[0]?.addressLine2);
          // this.corporateProfileForm?.get('City')?.setValue(this.userDataobj?.adress_details[0]?.city);
          // this.corporateProfileForm?.get('State')?.setValue(this.userDataobj?.adress_details[0]?.state);
          // this.corporateProfileForm?.get('GSTNo')?.setValue(this.userDataobj?.adress_details[0]?.gstNo);
          // }
          // else{
          // this.userDataobj?.adress_details.forEach((element:any,i:number) => {
          //    this.userprofileData?.address?.push(element);
          //  });
          if (this.userDataobj?.adress_details.length == 0) {

            this.addNewAddress();
            this.corporateProfileForm.get('AddressArray')?.get('0')?.get('PinCode')?.setValue(this.userDataobj?.basic_user_details?.pinCode);
            this.getpinCodefn('0');
          }
          //}
          // this.AddressFormArray=<FormArray>this.corporateProfileForm.controls['AddressArray'];
          this.userDataobj?.adress_details.forEach((Add: AddressDetails) => {
            this.AddressFormArray.push(this.createAddressFormGroup(Add));
            //console.log(this.AddressFormArray);
          });

          if (this.userDataobj?.doc_details != undefined) {
            let panindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'pan');
            if (panindex != -1) {
              this.corporateProfileForm.get('pan')?.get('docType')?.setValue(this.userDataobj?.doc_details[panindex]?.docType)
              this.corporateProfileForm.get('pan')?.get('documentId')?.setValue(this.userDataobj?.doc_details[panindex]?.documentId)
              this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[panindex]?.docNumber)
              this.initialPan = this.userDataobj?.doc_details[panindex]?.docNumber;
              if (this.userDataobj?.doc_details[panindex]?.docNumber != undefined && this.userDataobj?.doc_details[panindex]?.docNumber != "" && this.userDataobj?.doc_details[panindex]?.docNumber != null) {
                this.corporateProfileForm.get('pan')?.get('docNumber')?.disable();
                this.isPanEditable = true;
              }
              //  this.corporateProfileForm.get('pan')?.get('name')?.setValue(this.userDataobj?.doc_details[panindex]?.karzaRes?.result?.returnName)
              if (this.userDataobj?.doc_details[panindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('pan')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[panindex]?.docUrl?.front)
              }
            }
            let aadharindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'aadhar');
            if (aadharindex != -1) {
              this.corporateProfileForm.get('aadhar')?.get('docType')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docType)
              this.corporateProfileForm.get('aadhar')?.get('documentId')?.setValue(this.userDataobj?.doc_details[aadharindex]?.documentId)
              this.corporateProfileForm.get('aadhar')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docNumber)
              if (this.userDataobj?.doc_details[aadharindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('aadhar')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.front)
                this.corporateProfileForm.get('aadhar')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.back)
              }
            }
            let passportindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'passport');
            if (passportindex != -1) {
              this.corporateProfileForm.get('passport')?.get('docType')?.setValue(this.userDataobj?.doc_details[passportindex]?.docType)
              this.corporateProfileForm.get('passport')?.get('documentId')?.setValue(this.userDataobj?.doc_details[passportindex]?.documentId)

              this.corporateProfileForm.get('passport')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[passportindex]?.docNumber)
              if (this.userDataobj?.doc_details[passportindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('passport')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.front)
                this.corporateProfileForm.get('passport')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.back)
              }
            }
            let voterindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'voter');
            if (aadharindex != -1) {
              this.corporateProfileForm.get('voter')?.get('docType')?.setValue(this.userDataobj?.doc_details[voterindex]?.docType)
              this.corporateProfileForm.get('voter')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[voterindex]?.docNumber)
              this.corporateProfileForm.get('voter')?.get('documentId')?.setValue(this.userDataobj?.doc_details[voterindex]?.documentId)
              if (this.userDataobj?.doc_details[voterindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('voter')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.front)
                this.corporateProfileForm.get('voter')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.back)
              }
            }
            let dlindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'dl');
            if (dlindex != -1) {
              this.corporateProfileForm.get('dl')?.get('docType')?.setValue(this.userDataobj?.doc_details[dlindex]?.docType)
              this.corporateProfileForm.get('dl')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[dlindex]?.docNumber)
              this.corporateProfileForm.get('dl')?.get('documentId')?.setValue(this.userDataobj?.doc_details[dlindex]?.documentId)
              if (this.userDataobj?.doc_details[dlindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('dl')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.front)
                this.corporateProfileForm.get('dl')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.back)
              }
            }
            let uanindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'uan');
            if (aadharindex != -1) {
              this.corporateProfileForm.get('uan')?.get('docType')?.setValue(this.userDataobj?.doc_details[uanindex]?.docType)
              this.corporateProfileForm.get('uan')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[uanindex]?.docNumber)
              this.corporateProfileForm.get('uan')?.get('documentId')?.setValue(this.userDataobj?.doc_details[uanindex]?.documentId)
              if (this.userDataobj?.doc_details[uanindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('uan')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[uanindex]?.docUrl?.front)
                this.corporateProfileForm.get('uan')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[uanindex]?.docUrl?.back)
              }
            }
            let tradelicenseindex = this.userDataobj?.doc_details?.findIndex((x: any) => x.docType == 'tradelicense');
            if (aadharindex != -1) {
              this.corporateProfileForm.get('tradelicense')?.get('docType')?.setValue(this.userDataobj?.doc_details[tradelicenseindex]?.docType)
              this.corporateProfileForm.get('tradelicense')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[tradelicenseindex]?.docNumber)
              this.corporateProfileForm.get('tradelicense')?.get('documentId')?.setValue(this.userDataobj?.doc_details[tradelicenseindex]?.documentId)
              if (this.userDataobj?.doc_details[tradelicenseindex]?.docUrl != undefined) {
                this.corporateProfileForm.get('tradelicense')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[tradelicenseindex]?.docUrl?.front)
                this.corporateProfileForm.get('tradelicense')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[tradelicenseindex]?.docUrl?.back)
              }
            }
          }
          // this.corporateProfileForm?.get('PanCardNo')?.setValue(this.userDataobj?.doc_details[0]?.docNumber);

          this.corporateProfileForm?.get('customFile')?.clearValidators();
          this.corporateProfileForm?.get('customFile')?.updateValueAndValidity();
          // this.docImge=this.userDataobj.doc_details[0].docUrl;
          this.isPanExist = true;


        }

        if (this.userDataobj?.user_details?.represent != undefined && this.userDataobj?.user_details?.represent != "" && this.userDataobj?.user_details?.represent != null) {
          //this.corporateProfileForm.disable();
          this.isEditable = true;
        }
        if (this.hideAppliedForPan) {

          if (this.corporateProfileForm.get('pan')?.get('docNumber')?.value == '') {
            this.corporateProfileForm.get('pan')?.get('docNumber')?.enable();
            this.corporateProfileForm.get('applied_for_pan')?.setValue(false);
          }
        }
        if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter || this.role == this.apiRouteService.roles.superadmin) {
          this.isEditable = false;
          if (this.corporateProfileForm.get('pan')?.get('docNumber')?.value != '' && this.role == this.apiRouteService.roles.superadmin) {
            if (this.corporateProfileForm.get('applied_for_pan')?.value)
              this.isPanEditable = false;

            this.isSuperAdmin = false;
            this.corporateProfileForm.get('pan')?.get('docImages')?.get('front')?.enable();
            this.corporateProfileForm.get('pan')?.get('docNumber')?.enable();
          }

          this.isEntityEdittable = false;

          this.corporateProfileForm.get('FirstName')?.enable();
          this.corporateProfileForm.get('LastName')?.enable();
          this.corporateProfileForm.get('CompanyName')?.enable();
          // this.corporateProfileForm?.get('MSMENumber')?.enable(); 
          // this.corporateProfileForm?.get('enableMSME')?.setValue(true); 
          this.corporateProfileForm?.get('countryCode')?.disable();
          this.corporateProfileForm.get('EmailId')?.enable();


        }

      }, error => {
        console.log(error);
      })


  }

  getpinCodefn(i: string) {
    var b = '' + i + '';
    var x = this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('PinCode')?.value;
    //console.log(x);
    if (x > 99999 && x < 1000000) {
      this.accountService.getPinCode(this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('PinCode')?.value)
        .subscribe((resp) => {
          //console.log(resp);
          this.pinCodeResponse = resp as PincodeResponse;
          //console.log("my", this.pinCodeResponse);

          if (this.pinCodeResponse.code === 258) {
            this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('pinCodeErrorFlag')?.setValue(false);

            // this.pinCodeErrorFlag = false;
            this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue(this.pinCodeResponse.pincode?.state);
            this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue(this.pinCodeResponse.pincode?.city);
          }
          else if (this.pinCodeResponse.code === 259) {
            // console.log(this.pinCodeResponse.code);
            this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('pinCodeErrorFlag')?.setValue(true);
            //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue('');
            //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue('');
          }
          else {
            console.log("errorrr");
            this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('pinCodeErrorFlag')?.setValue(true);
            //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue('');
            //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue('');
          }
        }, error => {
          console.log(error);
          this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('pinCodeErrorFlag')?.setValue(true);
          this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue('');
          this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue('');
        })
    }
  }

  navselfDetails() {
    if (this.isEditable)
      return
    if (this.isEmailReq)
      this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { aoc: 'qtt','isrequired': this.isOption, isEdit: '1' } });
    else
      this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { 'isrequired': this.isOption,isEdit: '1' } });
  }

  createAddressFormGroup(cust?: AddressDetails) {
    //console.log("cust",cust);
    if (cust != undefined) {
      if (this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship') {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(cust?.addressLine1, Validators.required),
          StreetAddress2: new FormControl(cust?.addressLine2, Validators.required),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo, Validators.required),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6)
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false)
        });
      }
      else {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(cust?.addressLine1, Validators.required),
          StreetAddress2: new FormControl(cust?.addressLine2, Validators.required),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6)
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false)
        });
      }
    }
    else {
      if (this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship') {
        return new FormGroup({
          'StreetAddress1': new FormControl('', Validators.required),
          'addressId': new FormControl(''),
          'StreetAddress2': new FormControl('', Validators.required),
          'PinCode': new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6)
          ]),
          'State': new FormControl('', Validators.required),
          'City': new FormControl('', Validators.required),
          'GSTNo': new FormControl('', Validators.required),
          'pinCodeErrorFlag': new FormControl(false)
        })
      }
      {
        return new FormGroup({
          'StreetAddress1': new FormControl('', Validators.required),
          'addressId': new FormControl(''),
          'StreetAddress2': new FormControl('', Validators.required),
          'PinCode': new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6)
          ]),
          'State': new FormControl('', Validators.required),
          'City': new FormControl('', Validators.required),
          'GSTNo': new FormControl(''),
          'pinCodeErrorFlag': new FormControl(false)
        })
      }
    }
  }

  addNewAddress() {
    //this.userDataobj?.adress_details.push({});
    const AddressArray = this.corporateProfileForm?.get('AddressArray') as FormArray;
    this.AddressFormArray.push(this.createAddressFormGroup());
  }

  removeAddressFormGroup(id: any) {
    var i = '' + id + '';
    var addressId = this.corporateProfileForm?.get('AddressArray')?.get(i)?.get('addressId')?.value;
    this.AddressFormArray.removeAt(i);
    if (addressId != "") {
      this.deletedAddress.push(addressId);
    }
  }
  // toggleEditable(event:any) {
  //   if ( event.target.checked) {
  //       this.isPanCheck = true;
  //  }
  // }
  onEntityTypeChange(value: any) {
    if (this.isEditable)
      return
    this.corporateProfileForm?.get('EntityType')?.setValue(value);
    if (value == "Proprietorship")
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue("Proprietor");
    if (value == "Partnership" || value == "LLP")
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue("Partner");
    if (value == "HUF")
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue("Karta");
    if (value == "Private Ltd.Co." || value == "Public Ltd.Co.")
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue("Director");
    if (value == "Trust" || value == "Society")
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue("Employee");

    this.checkPan();
    this.verifyPanData();

  }

  onRelationshipEntityChange(value: any) {
    if (this.isEditable)
      return
    this.corporateProfileForm?.get('RelationshipEntity')?.setValue(value);

  }

  checkPan() {
    // this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors( {partnership: null,
    //   wrong: null,llp: null,privateError: null,publicError: null,trust: null,
    //   society: null,huf: null});
    this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    var pan = this.corporateProfileForm.get('pan')?.get('docNumber')?.value?.toLowerCase();
    var entityValue = this.corporateProfileForm.get('EntityType')?.value;
    // if(pan?.length==10 && pan.charAt(3)=='p'){
    //   this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ wrong: true });
    // }
    if (pan == "") {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ required: true });
    }
    else if (pan?.length != 10) {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    }
    else if (pan?.length == 10 && entityValue == "Proprietorship" && pan.charAt(3) != 'p') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ proprietorship: true });
    }
    else if (pan?.length == 10 && entityValue == "Partnership" && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ partnership: true });
    }
    else if (pan?.length == 10 && entityValue == "LLP" && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ llp: true });
    }
    else if (pan?.length == 10 && entityValue == "Private Ltd.Co." && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ privateError: true });
    }
    else if (pan?.length == 10 && entityValue == "Public Ltd.Co." && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ publicError: true });
    }
    else if (pan?.length == 10 && entityValue == "Trust" && pan.charAt(3) != 't') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ trust: true });
    }
    else if (pan?.length == 10 && entityValue == "Society" && pan.charAt(3) != 'a') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ society: true });
    }
    else if (pan?.length == 10 && entityValue == "HUF" && pan.charAt(3) != 'h') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ huf: true });
    }
    else {
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ partnership: false });
      // this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors( {partnership: null,
      //   wrong: null,llp: null,privateError: null,publicError: null,trust: null,
      //   society: null,huf: null});
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors(null);

    }
  }

  enableMSME() {
    if (this.isEditable)
      return

    if (this.corporateProfileForm?.get('enableMSME')?.value) {
      this.corporateProfileForm?.get('MSMENumber')?.setErrors(null);
      this.corporateProfileForm?.get('MSMENumber')?.setValue('');
      this.corporateProfileForm?.get('MSMENumber')?.disable();
    }
    else {
      this.corporateProfileForm?.get('MSMENumber')?.setErrors({ required: true });
      this.corporateProfileForm?.get('MSMENumber')?.enable();
    }
  }

  panFrontImageError = false;
  checkPanImageOnSubmit() {
    if (this.corporateProfileForm.get('pan')?.get('docNumber')?.value != '' && this.corporateProfileForm.get('pan')?.get('docImages')?.get('front')?.value == "") {
      this.panFrontImageError = true;
      this.notify.error("Please upload pan image.")
      return false;
    }
    else {
      this.panFrontImageError = false;
      return true;
    }
  }

  onSubmit() {
    if (!this.checkEmailField()) {
      this.corporateProfileForm.get('EmailId')?.markAsTouched();
      return;
    }
    // if(this.isPanExist)
    // {
    //   this.docImge="";
    // }
    // if(this.corporateProfileForm.invalid){
    //   return;
    // }
    // else{
    // }
    if (!this.checkPanImageOnSubmit()) {
      return;
    }
    //this.spinner.show();
    //console.log(this.corporateProfileForm.value);
    if (this.corporateProfileForm.get('applied_for_pan')?.value) {
      if (this.corporateProfileForm?.get('aadhar')?.get('docNumber')?.value == "" && this.corporateProfileForm?.get('passport')?.get('docNumber')?.value == "" && this.corporateProfileForm?.get('voter')?.get('docNumber')?.value == "" && this.corporateProfileForm?.get('dl')?.get('docNumber')?.value == "" && this.corporateProfileForm?.get('uan')?.get('docNumber')?.value == "" && this.corporateProfileForm?.get('tradelicense')?.get('docNumber')?.value == "") {
        this.notify.warn("If you applied for PAN, until then please enter one proof of Identity other than PAN.")
         
        return;
      }
    }
    //console.log(this.corporateProfileForm?.value);
    const value = { ...this.corporateProfileForm.getRawValue() };
    // let docObj={
    //     "pan" : {
    //     "docType" : "pan",
    //     "docNumber" : value?.PanCardNo,
    //     "name":value?.CompanyName,
    //     "docImages":
    //      {"front" :this.docImge,
    //      "back" : "" },
    //   },
    // };
    var aadharCard = this.corporateProfileForm?.get('aadhar')?.value;
    if (aadharCard != null && aadharCard != undefined) {
      if (aadharCard?.docNumber != null && aadharCard?.docNumber != '' && aadharCard?.docNumber != undefined) {
        if ((aadharCard?.docImages?.front == "" || aadharCard?.docImages?.front?.length < 0) && (aadharCard?.docImages?.back == "" || aadharCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var passportCard = this.corporateProfileForm?.get('passport')?.value;
    if (passportCard != null && passportCard != undefined) {
      if (passportCard?.docNumber != null && passportCard?.docNumber != '' && passportCard?.docNumber != undefined) {
        if ((passportCard?.docImages?.front == "" || passportCard?.docImages?.front?.length < 0) && (passportCard?.docImages?.back == "" || passportCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var voterCard = this.corporateProfileForm?.get('voter')?.value;
    if (voterCard != null && voterCard != undefined) {
      if (voterCard?.docNumber != null && voterCard?.docNumber != '' && voterCard?.docNumber != undefined) {
        if ((voterCard?.docImages?.front == "" || voterCard?.docImages?.front?.length < 0) && (voterCard?.docImages?.back == "" || voterCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var dlCard = this.corporateProfileForm?.get('dl')?.value;
    if (dlCard != null && dlCard != undefined) {
      if (dlCard?.docNumber != null && dlCard?.docNumber != '' && dlCard?.docNumber != undefined) {
        if ((dlCard?.docImages?.front == "" || dlCard?.docImages?.front?.length < 0) && (dlCard?.docImages?.back == "" || dlCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var uanCard = this.corporateProfileForm?.get('uan')?.value;
    if (uanCard != null && uanCard != undefined) {
      if (uanCard?.docNumber != null && uanCard?.docNumber != '' && uanCard?.docNumber != undefined) {
        if ((uanCard?.docImages?.front == "" || uanCard?.docImages?.front?.length < 0) && (uanCard?.docImages?.back == "" || uanCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('uan')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('uan')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('uan')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('uan')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }
    var tradelicenseCard = this.corporateProfileForm?.get('tradelicense')?.value;
    if (tradelicenseCard != null && tradelicenseCard != undefined) {
      if (tradelicenseCard?.docNumber != null && tradelicenseCard?.docNumber != '' && tradelicenseCard?.docNumber != undefined) {
        if ((tradelicenseCard?.docImages?.front == "" || tradelicenseCard?.docImages?.front?.length < 0) && (tradelicenseCard?.docImages?.back == "" || tradelicenseCard?.docImages?.back?.length < 0)) {
          this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('front')?.setErrors({ required: true });
          this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('back')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('front')?.setErrors(null);
          this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('back')?.setErrors(null);
        }
      }
    }

    if (value?.EntityType == "Proprietorship") {
      this.corporateProfileForm?.get('pan')?.get('name')?.setValue(value?.FirstName.concat(' ' + value?.LastName.toString()));
      value.pan.name = value?.FirstName.concat(' ' + value?.LastName.toString());
    }
    else {
      this.corporateProfileForm?.get('pan')?.get('name')?.setValue(value?.CompanyName);
      value.pan.name = value?.CompanyName;
    }
    var docObj;
    if (this.corporateProfileForm.get('applied_for_pan')?.value) {
      docObj = {
        "aadhar": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('aadhar')?.value)),
        "passport": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('passport')?.value)),
        "voter": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('voter')?.value)),
        "dl": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('dl')?.value)),
        "uan": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('uan')?.value)),
        "tradelicense": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('tradelicense')?.value))
      };
    }
    else {
      docObj = {
        "pan": JSON.parse(JSON.stringify(value?.pan)),
        "aadhar": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('aadhar')?.value)),
        "passport": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('passport')?.value)),
        "voter": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('voter')?.value)),
        "dl": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('dl')?.value)),
        "uan": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('uan')?.value)),
        "tradelicense": JSON.parse(JSON.stringify(this.corporateProfileForm?.get('tradelicense')?.value))
      };
    }
    this.userprofileData.doc_details = docObj;
    if (!this.corporateProfileForm.get('applied_for_pan')?.value) {
      this.userprofileData.doc_details.pan.docImages.frontView = '';
    }
    this.userprofileData.doc_details.aadhar.docImages.frontView = '';
    this.userprofileData.doc_details.aadhar.docImages.backView = '';
    this.userprofileData.doc_details.passport.docImages.frontView = '';
    this.userprofileData.doc_details.passport.docImages.backView = '';
    this.userprofileData.doc_details.voter.docImages.frontView = '';
    this.userprofileData.doc_details.voter.docImages.backView = '';
    this.userprofileData.doc_details.dl.docImages.frontView = '';
    this.userprofileData.doc_details.dl.docImages.backView = '';
    this.userprofileData.doc_details.uan.docImages.frontView = '';
    this.userprofileData.doc_details.uan.docImages.backView = '';
    this.userprofileData.doc_details.tradelicense.docImages.frontView = '';
    this.userprofileData.doc_details.tradelicense.docImages.backView = '';

    this.userprofileData.applied_for_pan = value?.applied_for_pan;
    this.userprofileData.represent = value?.Representer;
    this.userprofileData.entityType = value?.EntityType;
    this.userprofileData.relationshipWithEntity = value?.RelationshipEntity;
    this.userprofileData.doc_details = docObj;
    this.userprofileData.deletedAddress = this.deletedAddress;
    // let date = new Date(value?.DateoOfIncorporation);

    // let dd = date.getDate();
    // let mm = date.getMonth() + 1;
    // let yyyy = date.getFullYear();
    // var docDate=dd + "/" + mm + "/" + yyyy;

    // console.log(dd + "/" + mm + "/" + yyyy);
    //this.userprofileData.gstNo=value?.GSTNo;
    var dob = value?.DateoOfIncorporation;
    var docDate = this.datePipe.transform(dob, 'yyyy-MM-dd');
    this.userprofileData.corporateDetails = [];
    this.userprofileData.address = [];

    if (this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship') {
      this.userprofileData.corporateDetails?.push({
        'companyName': value?.CompanyName,
        'dateOfIncorporation': docDate?.toString(),
        'mobileNumber': value?.Mobile,
        'emailId': value?.EmailId,
        'msme': value?.MSMENumber
      });
    }
    else {
      this.userprofileData.corporateDetails?.push({
        'companyName': value?.CompanyName,
        'firstName': value?.FirstName,
        'lastName': value?.LastName,
        'dateOfIncorporation': docDate?.toString(),
        'mobileNumber': value?.Mobile,
        'emailId': value?.EmailId,
        'msme': value?.MSMENumber
      });
    }
    for (let index = 0; index < value.AddressArray.length; index++) {
      this.userprofileData.address?.push({
        'addressId': value.AddressArray[index]?.addressId,
        'addressLine1': value.AddressArray[index]?.StreetAddress1,
        'addressLine2': value.AddressArray[index]?.StreetAddress2,
        'pincode': value.AddressArray[index]?.PinCode,
        'state': value.AddressArray[index]?.State,
        'city': value.AddressArray[index]?.City,
        'gstNo': value.AddressArray[index]?.GSTNo
      });
    }
    // if(this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!=null || 
    //  this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!="" || 
    //  this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!=undefined) {
    //   this.userprofileData.address_proof= JSON.parse(JSON.stringify(value.address_proof));
    //  }
    if (this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value != null &&
      this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value != "" &&
      this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value != undefined) {
      if (this.corporateProfileForm?.get('address_proof')?.get('docImages')?.value == "" || this.corporateProfileForm?.get('address_proof')?.get('docImages')?.value == undefined) {
         
        this.notify.warn("Please upload Address Proof image.")
        return;
      }

      this.userprofileData.address_proof = JSON.parse(JSON.stringify(value.address_proof));
    }
    //console.log("data",JSON.stringify(this.userprofileData));
    if((this.corporateProfileForm.get('aadhar')?.get('docNumber')?.value == '' || this.corporateProfileForm.get('aadhar')?.get('docNumber')?.value == null) &&
    (this.corporateProfileForm.get('dl')?.get('docNumber')?.value == '' || this.corporateProfileForm.get('dl')?.get('docNumber')?.value == null) &&
    (this.corporateProfileForm.get('voter')?.get('docNumber')?.value == '' || this.corporateProfileForm.get('voter')?.get('docNumber')?.value == null) &&
    (this.corporateProfileForm.get('passport')?.get('docNumber')?.value == '' || this.corporateProfileForm.get('passport')?.get('docNumber')?.value == null )){
      const dialogRef = this.dialog.open(documentVerificationComponent, {
        width: '500px',
        data: {
          message: "Kindly upload either Aadhaar, Driving License, Passport or voter card for completing KYC verification.",
        },
      });
    } else {
      this.submitCorporatedDetails();
    }


  }

  submitCorporatedDetails() {
    this.httpClient.post(environment.kycDocumentVerifyUrl,
      JSON.stringify(this.userprofileData)).subscribe((resp: any) => {
        //console.log(resp);
        this.VerifiedDataobj = JSON.parse(JSON.stringify(resp.body));

        if (resp?.statusCode == 200) {
          // if (this.VerifiedDataobj?.pan?.isVerified==1 || (this.VerifiedDataobj?.aadhar?.isVerified==1 || this.VerifiedDataobj?.voter?.isVerified==1 || this.VerifiedDataobj?.dl?.isVerified==1 || this.VerifiedDataobj?.passport?.isVerified==1)){
          this.notify.success("Corporate data saved or verified successfully");
          this.router.navigate([`./customer/dashboard/communication`]);
           

          //  }
          //  else{
          //   this.notify.success("User saved or verified successfully");
          //   this.router.navigate([`./communication`]);
          //    
          //  }

        }

        else {
           
          var msg = JSON.parse(this.VerifiedDataobj);
          this.notify.error(msg?.message);
          this.userprofileData.address = [];
          this.userprofileData.corporateDetails = [];
        }

        // if(this.VerifiedDataobj?.code==200){
        //   if (this.VerifiedDataobj?.pan?.isVerified==1 || (this.VerifiedDataobj?.aadhar?.isVerified==1 || this.VerifiedDataobj?.voter?.isVerified==1 || this.VerifiedDataobj?.dl?.isVerified==1 || this.VerifiedDataobj?.uan?.isVerified==1 || this.VerifiedDataobj?.tradelicense?.isVerified==1 || this.VerifiedDataobj?.passport?.isVerified==1)){
        //     this.notify.success("Corporate user saved or verified successfully");
        //     this.router.navigate([`./communication`]);
        //      

        //   }
        //   else{
        //     this.notify.error("please enter a valid PAN");
        //      
        //   }
        // }

        // else if(this.VerifiedDataobj?.pan?.isVerified==undefined){
        //    
        //   this.notify.error("please enter a valid PAN");
        // }


      }, error => {
         
        console.log(error);
        //   alert(error.message.toSting());

      })
  }

  checkaddressFun(ev: KeyboardEvent) {
    let k = ev.keyCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 9 || k == 32 || (k >= 48 && k <= 57));
  }

  toggleEditable(event: any) {
    this.corporateProfileForm.get('applied_for_pan')?.setValue(event.checked);
    if (event.checked) {
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.setErrors(null);
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.setValue('');
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setValue('');
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('frontView')?.setValue('');
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.disable();

    }
    else {
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.setErrors({ required: true });
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.enable();
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
      this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.enable();

    }
    if (this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship') {
      this.corporateProfileForm.get('CompanyName')?.setValue('');
      this.corporateProfileForm?.get('CompanyName')?.enable();
    }
    // this.corporateProfileForm.get('LastName')?.setValue(this.initialLastName);
  }

  //   handleUpload(event:any) {
  //     const file = event.target.files[0];
  //     if(file.size > 5242880) {
  //      this.isInvalidfile=true;
  //      return;
  //     }

  //     const reader = new FileReader();
  //     reader.readAsDataURL(file);
  //     reader.onload = () => {
  //         console.log(reader.result);
  //         this.docImge=reader.result;
  //     };



  // }
  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if (file.size <= 5242880 && (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp')) {
      this.isInvalidfile = true;
      //  this.isShowPanDoc = false;
      this.uploadingImage = true;
      this.corporateProfileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log('type', file.type);
        this.corporateProfileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue(reader.result);
      }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      // var image_path = environment.bucket_parent_folder + "/" + this.cognitoId + '/' + tab + '/' + side + '.' + fileExt;
      if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
        var id = this.storage.getStorageData("cognitoUserId", true);
        var image_path = environment.bucket_parent_folder + "/" + id + '/' + tab + '/' + side + '.' + fileExt;
      }
      else {
        var id = this.storage.getStorageData("cognitoId", false);
        var image_path = environment.bucket_parent_folder + "/" + id + '/' + tab + '/' + side + '.' + fileExt;
      }

      var uploaded_file = await this.s3.prepareFileForUploadWithoutPublicAccess(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {

        this.corporateProfileForm.get(tab)?.get('docImages')?.get(side)?.setValue(uploaded_file?.Key);
      }
      else {
        this.corporateProfileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue('');
        this.notify.error("Upload Failed");
      }
      // controlName=reader.result;
      setTimeout(() => {
        this.checkPanImageOnSubmit();
      }, 0);
      this.uploadingImage = false;
      // };
    } else {
      // this.isShowPanDoc = true;
      this.corporateProfileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      this.corporateProfileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({ imgIssue: true });
    }
  }

  handleUploadAddress(event: any) {
    const file = event.target.files[0];
    if (file.size <= 5242880 && (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp')) {
      this.isInvalidfile = true;
      this.isShowDocProof = false;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log(reader.result);
        this.corporateProfileForm.get('address_proof')?.get('docImages')?.setValue(reader.result);
        // controlName=reader.result;

      };
    } else {
      this.isShowDocProof = true;
      this.corporateProfileForm.get('address_proof')?.get('docImages')?.setValue('');
    }
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.corporateProfileForm.get('countryCode')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
    }, err => {
      this.notify.error(err);
       
    })
  }

  updateCountryCode(city: any) {
    this.corporateProfileForm.get('countryCode')?.setValue(city)
  }

  checkEmailField() {
    if (this.isEmailReq) {
      let email = this.corporateProfileForm?.get('EmailId')?.value;
      if (!email) {
        this.corporateProfileForm.get('EmailId')?.setErrors({ required: true });
        return false;
      }
      else if (this.corporateProfileForm.get('EmailId')?.hasError('pattern')) {
        this.corporateProfileForm.get('EmailId')?.setErrors(null);
        this.corporateProfileForm.get('EmailId')?.setErrors({ pattern: true });
        return false;
      }
      else {
        this.corporateProfileForm.get('EmailId')?.setErrors(null);
        return true;
      }
    }
    else if (this.corporateProfileForm.get('EmailId')?.hasError('pattern')) {
      this.corporateProfileForm.get('EmailId')?.setErrors(null);
      this.corporateProfileForm.get('EmailId')?.setErrors({ pattern: true });
      return false;
    }
    else {
      this.corporateProfileForm.get('EmailId')?.setErrors(null);
      return true;
    }
  }

}
