import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerCorporateAddressComponent } from './partner-dealer-corporate-address.component';


describe('PartnerDealerCorporateDetailsComponent', () => {
  let component: PartnerDealerCorporateAddressComponent;
  let fixture: ComponentFixture<PartnerDealerCorporateAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerCorporateAddressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerCorporateAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
