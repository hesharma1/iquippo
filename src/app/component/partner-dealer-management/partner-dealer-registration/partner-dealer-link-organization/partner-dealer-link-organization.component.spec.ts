import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerLinkOrganizationComponent } from './partner-dealer-link-organization.component';

describe('PartnerDealerLinkOrganizationComponent', () => {
  let component: PartnerDealerLinkOrganizationComponent;
  let fixture: ComponentFixture<PartnerDealerLinkOrganizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerLinkOrganizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerLinkOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
