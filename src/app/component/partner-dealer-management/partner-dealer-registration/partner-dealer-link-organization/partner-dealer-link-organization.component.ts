import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AccountService } from 'src/app/services/account';
import { finalize } from 'rxjs/operators';
import { SharedService } from 'src/app/services/shared-service.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';

@Component({
  selector: 'app-partner-dealer-link-organization',
  templateUrl: './partner-dealer-link-organization.component.html',
  styleUrls: ['./partner-dealer-link-organization.component.css']
})
export class PartnerDealerLinkOrganizationComponent implements OnInit {

  public searchInput: string="";
  public isOrganization: any = null;
  public organizationDetails: any;
  public organizationAddress: any;
  public isProgress = false;
  constructor(private spinner: NgxSpinnerService, public partnerDealerRegistrationService:PartnerDealerRegistrationService,public appRouteEnum:AppRouteEnum ,public sharedService:SharedService , public storage: StorageDataService, private router: Router, private notify: NotificationService, public accountService: AccountService) { }

  ngOnInit(): void {
     
  }

  searchOrg() {
    if(this.searchInput!=''){
    //this.spinner.show();
    let payload = {
      proof_type__in: 1,
      id_number__in: this.searchInput,
      partner__status__in: 2
    }
    this.accountService.searchOraganization(payload).subscribe(
      (data: any) => {
        if (data.results.length) {
          this.organizationDetails = data.results[0];
          this.getOrgAddress();
        } else {
          this.isOrganization = false;
           
        }
      },
      err => {
         
        console.log(err);
        this.notify.error(err.error.message);
      }
    )
    }
  }

  getOrgAddress() {
    let payload = {
      partner__id__in: this.organizationDetails?.partner?.id
    }
    this.accountService.getPartnerCorporateAddress(payload).pipe((finalize(() => {   }))).subscribe(
      (data: any) => {
        this.organizationAddress = data.results[0];
        this.isOrganization = true;
      },
      err => {
        console.log(err);
        this.notify.error(err.statusText);
      }
    )
  }
  updatePartnerContactStatus(){
    
    
    let id = this.storage.getStorageData("partnerDealerPartnerId", true)
    if(id!=''){
    this.partnerDealerRegistrationService.updatePartnerStatus({status:1},this.appRouteEnum.partnerContactEntity,id).subscribe(res=>{
      console.log(res)
    },err=>{
      console.log(err);
    })
    }
  }
  mapOrg() {
    //this.spinner.show();
    let payload = {
      partner : this.organizationDetails.partner.id,
      partner_contact: this.storage.getStorageData("partnerDealerPartnerId", true),
       status: 1
    }
    this.accountService.mapOrganization(payload).pipe((finalize(() => {   }))).subscribe(
      (data: any) => {
        console.log(data);
        this.updatePartnerContactStatus();
        this.notify.success('Your linking request is proccessed succesfully!!',true);
        this.router.navigate([`./customer/dashboard/partner-dealer/partner-request`]);

      },
      err => {
        console.log(err)
      }
    )
  }
}
