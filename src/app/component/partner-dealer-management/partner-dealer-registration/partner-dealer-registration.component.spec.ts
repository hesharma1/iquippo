import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerDealerComponent } from './partner-dealer.component';

describe('PartnerDealerComponent', () => {
  let component: PartnerDealerComponent;
  let fixture: ComponentFixture<PartnerDealerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
