import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerAgreementComponent } from './partner-dealer-agreement.component';


describe('PartnerDealerCorporateDetailsComponent', () => {
  let component: PartnerDealerAgreementComponent;
  let fixture: ComponentFixture<PartnerDealerAgreementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerAgreementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
