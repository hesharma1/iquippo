import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerCorporateNextOthersComponent } from './partner-dealer-corporate-next-others.component';


describe('PartnerDealerCorporateNextOthersComponent', () => {
  let component: PartnerDealerCorporateNextOthersComponent;
  let fixture: ComponentFixture<PartnerDealerCorporateNextOthersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerCorporateNextOthersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerCorporateNextOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
