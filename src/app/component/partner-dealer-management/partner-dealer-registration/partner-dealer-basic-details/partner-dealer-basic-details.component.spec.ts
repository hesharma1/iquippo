import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerBasicDetailsComponent } from './partner-dealer-basic-details.component';

describe('PartnerDealerBasicDetailsComponent', () => {
  let component: PartnerDealerBasicDetailsComponent;
  let fixture: ComponentFixture<PartnerDealerBasicDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerBasicDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerBasicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
