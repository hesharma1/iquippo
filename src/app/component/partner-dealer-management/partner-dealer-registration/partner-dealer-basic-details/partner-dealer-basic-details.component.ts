import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared-service.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { AccountService } from 'src/app/services/account';
import { GetUserDto, UserProfileDTO, UserResponse } from 'src/app/shared/abstractions/user';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { environment } from 'src/environments/environment';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { PanRequest, PanResponse } from 'src/app/models/common/panRequestData.model';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { PartnerDealerAddress, PartnerDealerBasicDetailsDataModel, PartnerDealerBasicInfo, PartnerDealerContactEntity, PartnerDealerIdentityProofs } from 'src/app/models/partner-dealer-management/partner-dealer-basic-details-data.model';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { HttpClient } from '@angular/common/http';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { documentVerificationComponent } from 'src/app/component/customer/document-verification-confirmation/document-verification-confirmation.component';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';

@Component({
  selector: 'app-partner-dealer-basic-details',
  templateUrl: './partner-dealer-basic-details.component.html',
  styleUrls: ['./partner-dealer-basic-details.component.css']
})
export class PartnerDealerBasicDetailsComponent implements OnInit {
  basicDetailsDataModel: PartnerDealerBasicDetailsDataModel;
  profileForm: FormGroup
  pinCodeResponse?: PincodeResponse;
  PanRequest?: any;
  PanResponse?: PanResponse;
  initialFirstName?: string = "";
  initialLastName?: string = "";
  isDupliateEmailID: boolean = false;
  isInvalidfile: boolean = false;
  userprofileData: UserProfileDTO;
  getUser: GetUserDto;
  userDataobj: any;
  pinCodeErrorFlag = false;
  isPanCheck: boolean = false;
  isPanExist: boolean = false;
  isEditable: boolean = false;
  isPanEditable: boolean = false;
  pincodeBlur: boolean = false;
  docImge: any;
  VerifiedDataobj: any;
  proofList = ["Voter Id", "Aadhar", "Driving License", "Passport", "Ration Card", "Bank Statement",
    "Utility Bills", "Trade License", "GSTN & Autority Certificate",
    "Registration Certificate"];
  countryList?: any;
  selectedCity?: CountryResponse;
  minDate: any;
  isEmailReq?: boolean = false;
  isShowPanDoc: boolean = false;
  isShowDocProof: boolean = false;
  isEdit: boolean = false;
  cognitoId: string = '';
  uploadingImage: boolean = false;
  hideAppliedForPan: boolean = false;
  initialPan?: string = '';
  isSuperAdmin?: boolean = true;
  role?: string;
  userId: string = '';
  partnership_type: number = 0;
  partnerDealerIdentityProofs: Array<PartnerDealerIdentityProofs>;
  isCallCenter: string = "";
  isSaveOrUpdate: string = "Update";
  rolesArray: any;
  partnerAllDetail: any;
  isOption
  constructor(private router: Router, private http: HttpClient, private appRouteEnum: AppRouteEnum, private fb: FormBuilder, private accountService: AccountService,
    public dialog: MatDialog, public sharedService: SharedService,
    private storage: StorageDataService, private spinner: NgxSpinnerService, public notificationservice: NotificationService,
    private notify: NotificationService, private datePipe: DatePipe, private s3: S3UploadDownloadService, private apiRouteService: ApiRouteService,
    private partnerDealerRegService: PartnerDealerRegistrationService, private activatedRoute: ActivatedRoute) {
    this.basicDetailsDataModel = new PartnerDealerBasicDetailsDataModel();
    this.profileForm = fb.group({
      Representer: ['1', Validators.required],
      Gender: ['1', Validators.required],
      AlternateEmailId: ['', Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)],
      documentId: new FormControl(''),
      // PanCardNo: ['',Validators.required],
      // customFile: ['',Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      Dob: ['', Validators.required],
      address_proof: new FormGroup({
        documentId: new FormControl(''),
        docType: new FormControl(''),
        docNumber: new FormControl(''),
        docImages: new FormControl(''),
      }),
      Mobile: ['', Validators.required],
      EmailId: ['', [Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]],
      StreetAddress1: ['', Validators.required],
      StreetAddress2: ['', Validators.required],
      addressId: new FormControl(''),
      PinCode: ['',
        [Validators.required,
        Validators.pattern(/[1-9]{1}[0-9]{5}$/),
        Validators.minLength(6),
        Validators.maxLength(6)]
      ],
      State: ['', Validators.required],
      City: ['', Validators.required],
      countryCode: ['', Validators.required],
      applied_for_pan: new FormControl(false, [Validators.required]),

      uan: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl('', []),
          back: new FormControl('')
        })
      }),
      tradelicense: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl('', []),
          back: new FormControl('')
        })
      }),
      pan: new FormGroup({
        docType: new FormControl('pan'),
        documentId: new FormControl(''),
        docNumber: new FormControl('', Validators.required),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      aadhar: this.fb.group({
        docType: new FormControl('aadhar'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      },{validator: numberonly('docNumber')}),
      passport: new FormGroup({
        docType: new FormControl('passport'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      voter: new FormGroup({
        docType: new FormControl('voter'),
        docNumber: new FormControl(''),
        documentId: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),
      dl: new FormGroup({
        docType: new FormControl('dl'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        documentId: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl('')
        })
      }),

    }, {
      validator: [ageCheck('Dob')]
    });
    this.userprofileData = {};
    this.userprofileData.use_pan_name = "No";
    this.getUser = {};
    this.partnerDealerIdentityProofs = new Array<PartnerDealerIdentityProofs>();
  }

  test() {
    //console.log(this.profileForm.value);
  }


  verifyPanData() {
    var panRequest: any = {};
    if (this.profileForm?.get('pan')?.get('docNumber')?.valid &&
      (this.profileForm.get('pan')?.get('docNumber')?.value != "")) {
      if (this.initialPan?.toLowerCase() != this.profileForm.get('pan')?.get('docNumber')?.value.toLowerCase()) {
        //  //this.spinner.show();        
        panRequest.cognitoId = this.basicDetailsDataModel.basicProfileResponse.cognito_id;

        panRequest.panCardNo = this.profileForm.get('pan')?.get('docNumber')?.value;
        panRequest.documentId = this.profileForm.get('pan')?.get('documentId')?.value;
        panRequest.source = "Partner";
        panRequest.represent = "1";
        panRequest.name = this.profileForm.get('FirstName')?.value + ' ' + this.profileForm.get('LastName')?.value;
        this.partnerDealerRegService.verifyPanCardDetails(panRequest)
          .subscribe(resp => {
             
            this.PanResponse = resp as PanResponse;
            //console.log(this.PanResponse.body);
            if (this.PanResponse.body?.statusCode == "200") {
              let msg: any = this.PanResponse.body.message;
              this.notify.success(msg)
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  this.replaceVerifyPanName();

                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  this.userprofileData.use_pan_name = "No";
                }
              })

            }
            else if (this.PanResponse.body?.statusCode == "508") {
              // NamemisMatchPopup
              const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                width: '650px',
                data: {
                  message: 'Do you want to use your PAN Name?',
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  //console.log(result);
                  this.replaceVerifyPanName();
                  this.userprofileData.use_pan_name = "Yes";
                }
                else {
                  //console.log(result);
                  this.userprofileData.use_pan_name = "No";
                }
              })
            }
            else if (this.PanResponse.body?.statusCode == "513") {
              //this.profileForm.get('pan')?.get('docNumber')?.setValue("");
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg);
            }
            else if (this.PanResponse.body?.statusCode == "506" || this.PanResponse.body?.statusCode == "512") {
              let msg: any = this.PanResponse.body.message;
              this.notify.warn(msg)
            }
            else {
              let msg: any = this.PanResponse?.body?.message;
              this.notify.warn(msg);
            }
          },
            error => {
              console.log(error);
            })
      }
    }

  }

  replaceVerifyPanName() {
    this.profileForm.get('FirstName')?.setValue(this.PanResponse?.body?.firstName);
    this.profileForm.get('LastName')?.setValue(this.PanResponse?.body?.lastName);
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['isrequired'] == 'true') {
        this.isOption = true;
      }
      else {
        this.isOption = false;
      }
    });
     
    this.bindFlags();
    if (this.storage.getStorageData('partnerDealerUserId', true) != null) {
      this.userId = this.storage.getStorageData('partnerDealerUserId', true).replace(/['"]+/g, '');
    }
    if (this.storage.getStorageData('partnerDealerUserId', true) != null) {
      this.partnership_type = Number(this.storage.getStorageData('registrationTypeID', true));
    }
    this.getAllPartnerInfo();

    this.disableControls();
    this.getUserBasicDetails(this.userId);
    this.cognitoId = this.storage.getStorageData('cognitoId', false).replace(/['"]+/g, '');
    this.rolesArray = this.storage.getStorageData('rolesArray', false);
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
  }

  checkaddressFun(ev: KeyboardEvent) {
    // let regex= /^[ A-Za-z0-9@.'%\/#&-]*$/;
    // if (!regex.test(ev.key)) {
    //     ev.preventDefault();
    //     //  var newval= this.profileForm.get('StreetAddress1')?.value.replace(/^[ A-Za-z0-9@.'%\/#&-]*$/, "");
    //     //  this.profileForm.get('StreetAddress1')?.setValue(newval);
    //   } 
    let k = ev.keyCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 9 || k == 32 || (k >= 48 && k <= 57));
  }
  getAllPartnerInfo() {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    //  let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    this.partnerDealerRegService.getPartnerAllInfo(this.cognitoId).subscribe((res: any) => {
      console.log(res);
      this.storage.setStorageData('partnerAllDetail', res, true)
      this.partnerAllDetail = res;

    }, err => {
      console.log(err);
    })
  }
  checkPan() {
    var pan = this.profileForm.get('pan')?.get('docNumber')?.value?.toLowerCase();
    if (pan == "") {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ required: true });
    }
    else if (pan?.length != 10) {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    }
    else if (pan.length == 10 && pan.charAt(3) != 'p') {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors({ wrong: true });
    }
    else {
      this.profileForm.get('pan')?.get('docNumber')?.setErrors(null);
    }
  }


  checkIndiaPin() {
    let cntryCode = (this.profileForm.get('countryCode')?.value).prefixCode;
    if (cntryCode != "+91") {
      //console.log("not india");
      this.profileForm?.get('State')?.enable();
      this.profileForm?.get('City')?.enable();
    }
    else {
      //console.log("ind");
      this.profileForm?.get('State')?.disable();
      this.profileForm?.get('City')?.disable();
    }
  }

  getpinCodefn() {
    if (this.profileForm.get('PinCode')?.value != null && this.profileForm.get('PinCode')?.value != undefined && this.profileForm.get('PinCode')?.value != '' && (this.profileForm.get('countryCode')?.value).prefixCode == '+91') {
      var x = this.profileForm.get('PinCode')?.value;
      if (x > 99999 && x < 1000000) {
        this.getSearchedLocation("search=" + this.profileForm.get('PinCode')?.value, 0);
        // this.accountService.getPinCode(this.profileForm.get('PinCode')?.value)
        //   .subscribe((resp) => {
        //     //console.log(resp);
        //     this.pinCodeResponse = resp as PincodeResponse;
        //     //console.log("my",this.pinCodeResponse);

        //     if (this.pinCodeResponse.code === 258) {

        //       this.pinCodeErrorFlag = false;
        //       this.profileForm.get('State')?.setValue(this.pinCodeResponse.pincode?.state);
        //       this.profileForm.get('City')?.setValue(this.pinCodeResponse.pincode?.city);
        //     }
        //     else if (this.pinCodeResponse.code === 259) {
        //       // console.log(this.pinCodeResponse.code);
        //       this.pinCodeErrorFlag = true;
        //       this.profileForm.get('State')?.setValue('');
        //       this.profileForm.get('City')?.setValue('');
        //     }
        //     else {
        //       console.log("errorrr");
        //       this.pinCodeErrorFlag = true;
        //       this.profileForm.get('State')?.setValue('');
        //       this.profileForm.get('City')?.setValue('');
        //     }
        //   }, error => {
        //     
        //     console.log(error);
        //     this.pinCodeErrorFlag = true;
        //     this.profileForm.get('State')?.setValue('');
        //     this.profileForm.get('City')?.setValue('');
        //   })
      }
    }
  }
  onGenderChange(value: any) {
    this.profileForm.get('Gender')?.setValue(value);
  }



  validateAlternateEmailIdfn() {
    const value = { ...this.profileForm.getRawValue() };
    if ((value.AlternateEmailId.length > 0) && value?.EmailId == value?.AlternateEmailId) {
      this.profileForm.get('AlternateEmailId')?.setErrors({ exists: true });
      this.isDupliateEmailID = true;
    }
    else {
      if (!this.profileForm.get('AlternateEmailId')?.hasError('pattern')) {
        this.profileForm.get('AlternateEmailId')?.setErrors(null);
      }
      else {
        this.profileForm.get('AlternateEmailId')?.setErrors({ exists: false });
      }
      this.isDupliateEmailID = false;
    }
  }


  panFrontImageError = false;
  checkPanImageOnSubmit() {
    if (this.profileForm.get('pan')?.get('docNumber')?.value != '' && this.profileForm.get('pan')?.get('docImages')?.get('front')?.value == "") {
      this.panFrontImageError = true;
      this.notify.error("Please upload pan image.")
      return false;
    }
    else {
      this.panFrontImageError = false;
      return true;
    }
  }


  onSubmit() {
   

    if (this.profileForm.valid) {
     
      // validate pan card details
      if (!this.checkPanImageOnSubmit()) {
        return;
      }
      if (!this.checkEmailField()) {
        this.profileForm.get('EmailId')?.markAsTouched();
        return;
      }
      if((this.profileForm.get('aadhar')?.get('docNumber')?.value == '' || this.profileForm.get('aadhar')?.get('docNumber')?.value == null) &&
      (this.profileForm.get('dl')?.get('docNumber')?.value == '' || this.profileForm.get('dl')?.get('docNumber')?.value == null) &&
      (this.profileForm.get('voter')?.get('docNumber')?.value == '' || this.profileForm.get('voter')?.get('docNumber')?.value == null) &&
      (this.profileForm.get('passport')?.get('docNumber')?.value == '' || this.profileForm.get('passport')?.get('docNumber')?.value == null)){
        const dialogRef = this.dialog.open(documentVerificationComponent, {
          width: '500px',
          data: {
            message: "Kindly upload either Aadhaar, Driving License, Passport or voter card for completing KYC verification.",
          },
        });
      } else {
        this.submitProfileDetails();
      }
    }
    else {
      return;
    }

  }

  submitProfileDetails() {
    const formValues = { ...this.profileForm.getRawValue() };
    let address = new PartnerDealerAddress();
    let partnerDealerIdentityProofs = new Array<PartnerDealerIdentityProofs>()
    let basic_info = new PartnerDealerBasicInfo();
    basic_info.first_name = formValues.FirstName;
    basic_info.last_name = formValues.LastName;
    basic_info.dob = this.datePipe.transform(formValues.Dob, 'yyyy-MM-dd');
    basic_info.mobile_number = formValues.Mobile;
    basic_info.email = formValues.EmailId;
    basic_info.gender = formValues.Gender;
    basic_info.pin_code = this.profileForm.get('PinCode')?.value;
    basic_info.alt_email_address = formValues.AlternateEmailId;
    basic_info.cognito_id = this.cognitoId;
    if(this.userId==undefined || this.userId == null || this.userId == ''){
    if (this.rolesArray.includes(this.apiRouteService.roles.customer))
      basic_info.groups = [1, 5];
    else
      basic_info.groups = [5];
    }
    if (this.userprofileData.use_pan_name == "Yes")
      basic_info.kyc_verified = 2;
    if (this.userprofileData.use_pan_name == "No")
      basic_info.kyc_verified = 1;
    if (this.profileForm.get('aadhar')?.get('docNumber')?.value != '' || this.profileForm.get('passport')?.get('docNumber')?.value != '' || this.profileForm.get('voter')?.get('docNumber')?.value != '' || this.profileForm.get('dl')?.get('docNumber')?.value != '')
      basic_info.pan_address_proof = true;

    address.address = formValues.StreetAddress1;
    address.address_line1 = formValues.StreetAddress2;
    address.address_line2 = "";
    address.gst_no = "";
    // address.crm_address_id = "";
    address.pin_code = this.profileForm.get('PinCode')?.value;
    address.city = this.basicDetailsDataModel.cityId;
    address.state = this.basicDetailsDataModel.stateId;
    address.country = this.basicDetailsDataModel.countryId;
    this.partnerDealerRegService.createUpdatePartnerDealerDetails(basic_info, this.apiRouteService.partnerDealerBasicProfile, this.isSaveOrUpdate, this.userId).subscribe(
      res => {
        if (res != null) {
          this.basicDetailsDataModel.basicDetailsResponse = res as any;
          // let contactEntity = new PartnerDealerContactEntity();
          // contactEntity.partnership_type = this.partnership_type;
          // contactEntity.user = this.userId;

          // if (this.storage.getStorageData('isBecomeSrcPartnerWithiQuippo', true) != null) {
          //   contactEntity.is_channel_partner = this.storage.getStorageData('isBecomeSrcPartnerWithiQuippo', true);
          // }            
          
          if (this.basicDetailsDataModel.actionName === 'Save') {
            // this.saveContactEntity(contactEntity, address);
            address.partner_contact = this?.partnerAllDetail?.partner_contact_entity?.id// .userContactEntityResponse.id;
            this.saveAddress(address);
  
          }
          else if (this.basicDetailsDataModel.actionName === 'Update')
          //else
          {
            address.partner_contact = this.basicDetailsDataModel.partnerContactId;
            this.saveAddress(address);
          }
        }
      });
  }

  setIdentityProofs(docNumber: string, type: number, doc: string, partnerContactId: number, id: number,doc_back?:string) {
    let partnerDealerIdentityProof = new PartnerDealerIdentityProofs()
    partnerDealerIdentityProof.id_number = docNumber;
    partnerDealerIdentityProof.proof_type = type
    partnerDealerIdentityProof.document = doc;
    if(doc_back!='')
    partnerDealerIdentityProof.document_back = doc_back;
    partnerDealerIdentityProof.partner_contact = partnerContactId
    partnerDealerIdentityProof.id = id;
    this.partnerDealerIdentityProofs.push(partnerDealerIdentityProof);
  }

  saveIdentityProofs(partnerContactId: number) {
    this.partnerDealerIdentityProofs = []
    var panCard = this.profileForm?.get('pan')?.value;
    if (panCard != null && panCard != undefined) {
      if (panCard.docNumber != null && panCard.docNumber != '' && panCard.docNumber != undefined) {
        if ((panCard.docImages.front == "" || panCard.docImages.front.length < 0) && (panCard.docImages.back == "" || panCard.docImages.back.length < 0)) {
          this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
           
          return;
        }
        else {
          this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
          this.setIdentityProofs(panCard.docNumber, this.apiRouteService.identityProofType.PAN_CARD, this.profileForm?.get('pan')?.get('docImages')?.get('front')?.value, partnerContactId, panCard.documentId,this.profileForm?.get('pan')?.get('docImages')?.get('back')?.value);
        }
      }

    }

    var aadharCard = this.profileForm?.get('aadhar')?.value;
    if (aadharCard != null && aadharCard != undefined) {
      if (aadharCard.docNumber != null && aadharCard.docNumber != '' && aadharCard.docNumber != undefined) {
        if(aadharCard?.docImages?.front ==''){
          this.profileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors({required: true});
        }
        else
        this.profileForm?.get('aadhar')?.get('docImages')?.get('front')?.setErrors(null);
        
        if(aadharCard?.docImages?.back ==''){
        this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors({required: true});
        return;
        }
        else
        this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.setErrors(null);
        this.setIdentityProofs(aadharCard.docNumber, this.apiRouteService.identityProofType.AADHAR_CARD, this.profileForm?.get('aadhar')?.get('docImages')?.get('front')?.value, partnerContactId, aadharCard.documentId,this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.value);
         //this.setIdentityProofs(aadharCard.docNumber,this.apiRouteService.identityProofType.AADHAR_CARD, this.profileForm?.get('aadhar')?.get('docImages')?.get('back')?.value,partnerContactId,aadharCard.documentId);
      }
    }
    var passportCard = this.profileForm?.get('passport')?.value;
    if (passportCard != null && passportCard != undefined) {
      if (passportCard.docNumber != null && passportCard.docNumber != '' && passportCard.docNumber != undefined) {
        if(passportCard?.docImages?.front ==''){
          this.profileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors({required: true});
        }
        else
        this.profileForm?.get('passport')?.get('docImages')?.get('front')?.setErrors(null);
        
        if(passportCard?.docImages?.back ==''){
        this.profileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors({required: true});
        return;
        }
        else
        this.profileForm?.get('passport')?.get('docImages')?.get('back')?.setErrors(null);
        
        this.setIdentityProofs(passportCard.docNumber, this.apiRouteService.identityProofType.PASSPORT, this.profileForm?.get('passport')?.get('docImages')?.get('front')?.value, partnerContactId, passportCard.documentId,this.profileForm?.get('passport')?.get('docImages')?.get('back')?.value);
         //this.setIdentityProofs(passportCard.docNumber,this.apiRouteService.identityProofType.PASSPORT, this.profileForm?.get('passport')?.get('docImages')?.get('back')?.value,partnerContactId,passportCard.documentId);
      }
    }

    var voterCard = this.profileForm?.get('voter')?.value;
    if (voterCard != null && voterCard != undefined) {
      if (voterCard.docNumber != null && voterCard.docNumber != '' && voterCard.docNumber != undefined) {
        if(voterCard?.docImages?.front ==''){
          this.profileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors({required: true});
        }
        else
        this.profileForm?.get('voter')?.get('docImages')?.get('front')?.setErrors(null);
        
        if(voterCard?.docImages?.back ==''){
        this.profileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors({required: true});
        return;
        }
        else
        this.profileForm?.get('voter')?.get('docImages')?.get('back')?.setErrors(null);
        
        this.setIdentityProofs(voterCard.docNumber, this.apiRouteService.identityProofType.VOTER_ID, this.profileForm?.get('voter')?.get('docImages')?.get('front')?.value, partnerContactId, voterCard.documentId,this.profileForm?.get('voter')?.get('docImages')?.get('back')?.value);
       // this.setIdentityProofs(voterCard.docNumber,this.apiRouteService.identityProofType.VOTER_ID, this.profileForm?.get('voter')?.get('docImages')?.get('back')?.value,partnerContactId,voterCard.documentId);

      }
    }
    var dlCard = this.profileForm?.get('dl')?.value;
    if (dlCard != null && dlCard != undefined) {
      if (dlCard.docNumber != null && dlCard.docNumber != '' && dlCard.docNumber != undefined) {
        if(dlCard?.docImages?.front ==''){
          this.profileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors({required: true});
        }
        else
        this.profileForm?.get('dl')?.get('docImages')?.get('front')?.setErrors(null);
        
        if(dlCard?.docImages?.back ==''){
        this.profileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors({required: true});
        return;
        }
        else
        this.profileForm?.get('dl')?.get('docImages')?.get('back')?.setErrors(null);
        this.setIdentityProofs(dlCard.docNumber, this.apiRouteService.identityProofType.DRIVING_LICENCE, this.profileForm?.get('dl')?.get('docImages')?.get('front')?.value, partnerContactId, dlCard.documentId,this.profileForm?.get('dl')?.get('docImages')?.get('back')?.value);
        //this.setIdentityProofs(dlCard.docNumber,this.apiRouteService.identityProofType.DRIVING_LICENCE, this.profileForm?.get('dl')?.get('docImages')?.get('back')?.value,partnerContactId,dlCard.documentId);
      }
    }



    this.partnerDealerRegService.createUpdatePartnerDealerIdentityProofs(this.partnerDealerIdentityProofs, this.apiRouteService.partnerDealerBatchIdentityProofs, this.basicDetailsDataModel.actionName).subscribe(
      res => {
        if (res != null) {
          this.storage.setStorageData('partnerDealerPartnerId', partnerContactId, true)
          if (this.partnerAllDetail?.mapping?.id != null) {
            if (this.partnerAllDetail?.mapping?.partner?.partner_admin?.cognito_id == this.cognitoId)
              this.router.navigate([`./customer/dashboard/partner-dealer/corporate-details`]);
            else {
              this.router.navigate([`./home`]);
            }
          }
          else
            this.router.navigate([`./customer/dashboard/partner-dealer/link-organization`]);

        }
      });

  }


  saveContactEntity(contactEntity: any, address: any) {
    this.partnerDealerRegService.createUpdatePartnerDealerDetails(contactEntity, this.apiRouteService.partnerDealerContactEntity, "Update", this.basicDetailsDataModel.id).subscribe(
      res => {
        if (res != null) {
          this.basicDetailsDataModel.userContactEntityResponse = res as any;
          address.partner_contact = this.basicDetailsDataModel.userContactEntityResponse.id;
          this.saveAddress(address);

        }
      });
  }

  saveAddress(addressRequest: any) {
    this.partnerDealerRegService.createUpdatePartnerDealerDetails(addressRequest, this.apiRouteService.partnerDealerUserAddress, this.basicDetailsDataModel.actionName, this.basicDetailsDataModel.addressId).subscribe(
      res => {
        if (res != null) {
          this.saveIdentityProofs(addressRequest.partner_contact);
        }
      });
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp') && file.size <= 5242880) {
      this.isInvalidfile = true;
      this.uploadingImage = true;
      //  this.isShowPanDoc = false;
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log('type', file.type);
        this.profileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue(reader.result);
      }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path = environment.bucket_parent_folder + "/"+this.cognitoId+"/BasicDetails/" + tab + '/' + side + '.' + fileExt;

      var uploaded_file = await this.s3.prepareFileForUploadWithoutPublicAccess(file, image_path);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(uploaded_file?.Location);
      }
      else {
        this.profileForm.get(tab)?.get('docImages')?.get(side + 'View')?.setValue('');
        this.notify.error("Upload Failed");
      }
      // controlName=reader.result;
      setTimeout(() => {
        this.checkPanImageOnSubmit();
      }, 0);
      this.uploadingImage = false;
      //};
    } else {
      // this.isShowPanDoc = true;
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({ imgIssue: true });

    }
  }


  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.profileForm.get('countryCode')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
      // this.checkIndiaPin();
    }, err => {
      this.notificationservice.error(err);
       
    })
  }

  updateCountryCode(city: any) {
    this.profileForm.get('countryCode')?.setValue(city)
  }

  checkEmailField() {
    if (this.isEmailReq) {
      let email = this.profileForm?.get('EmailId')?.value;
      if (!email) {
        this.profileForm.get('EmailId')?.setErrors({ required: true });
        return false;
      }
      else if (this.profileForm.get('EmailId')?.hasError('pattern')) {
        this.profileForm.get('EmailId')?.setErrors(null);
        this.profileForm.get('EmailId')?.setErrors({ pattern: true });
        return false;
      }
      else {
        this.profileForm.get('EmailId')?.setErrors(null);
        return true;
      }
    }
    else if (this.profileForm.get('EmailId')?.hasError('pattern')) {
      this.profileForm.get('EmailId')?.setErrors(null);
      this.profileForm.get('EmailId')?.setErrors({ pattern: true });
      return false;
    }
    else {
      this.profileForm.get('EmailId')?.setErrors(null);
      return true;
    }
  }
  //for getting location master data
  getSearchedLocation(query: string, id: number) {
    this.profileForm.get('State')?.setValue("");
    this.profileForm.get('City')?.setValue("");

    this.partnerDealerRegService.getLocations(query, id).subscribe(
      (res: any) => {
        if (res != null && res.results != null && res.results.length > 0) {
          this.basicDetailsDataModel.userLocationResponse = res.results[0] as any;
        }
        else if (res != null && res.results != null && res.results.length == 0) {
          this.pinCodeErrorFlag = true;
          return;
        }
        else if (res != null) {
          this.basicDetailsDataModel.userLocationResponse = res as any;
        }

        this.basicDetailsDataModel.pinCodeId = this.basicDetailsDataModel.userLocationResponse.id;
        this.profileForm.get('State')?.setValue(this.basicDetailsDataModel.userLocationResponse.city.state.name);
        this.profileForm.get('City')?.setValue(this.basicDetailsDataModel.userLocationResponse.city.name);
        this.profileForm.get('PinCode')?.setValue(this.basicDetailsDataModel.userLocationResponse.pin_code);
        this.basicDetailsDataModel.countryId = this.basicDetailsDataModel.userLocationResponse.city.state.country.id;
        this.basicDetailsDataModel.stateId = this.basicDetailsDataModel.userLocationResponse.city.state.id;
        this.basicDetailsDataModel.cityId = this.basicDetailsDataModel.userLocationResponse.city.id;
        if ((this.profileForm.get('State')?.value == null || this.profileForm.get('State')?.value == "") ||
          (this.profileForm.get('City')?.value == null || this.profileForm.get('City')?.value == "") ||
          (this.profileForm.get('PinCode')?.value == null || this.profileForm.get('PinCode')?.value == "")) {
          this.pinCodeErrorFlag = true;
        }
        else {
          this.pinCodeErrorFlag = false;
        }

      });
  }

  //for getting state master data
  getUserBasicDetails(userId: string) {
    this.partnerDealerRegService.getPartnerDealerBasicProfile(userId).then(
      (res: any) => {
        if (res != null) {
          this.basicDetailsDataModel.basicProfileResponse = res as any;
          this.bindUserDetails();
          this.partnerDealerRegService.getPartnerDealerContactEntity("user__cognito_id__in=" + userId.toString()).then(
            (res: any) => {
              if (res != null && res.results.length > 0) {

                this.basicDetailsDataModel.userContactEntityResponse = res.results[0] as any;
                this.storage.clearStorageData("registrationTypeID");
                this.storage.setStorageData("registrationTypeID", this.basicDetailsDataModel.userContactEntityResponse.partnership_type, true);
                this.partnerDealerRegService.getPartnerDealerAddressDetails("partner_contact__user__cognito_id__in=" + this.basicDetailsDataModel.userContactEntityResponse.user.cognito_id.toString()).then(
                  (res: any) => {
                    if (res != null) {
                      this.basicDetailsDataModel.userAddressResponse = res.results[0] as any;
                      this.bindUserDetails();
                      this.partnerDealerRegService.getPartnerDealerIdentityProofs("partner_contact__user__cognito_id__in=" + this.basicDetailsDataModel.userContactEntityResponse.user.cognito_id.toString()).then(
                        (res: any) => {

                          // if(res != null && res.results.length > 0){                      
                          this.basicDetailsDataModel.userIdentityProofResponse = res.results as any;
                          this.bindUserDetails();
                          if(this.basicDetailsDataModel.userAddressResponse)
                          if(this.basicDetailsDataModel?.userIdentityProofResponse?.length!=0)
                          this.basicDetailsDataModel.actionName = "Update";
                          // if(this.partnerAllDetail?.mapping?.status==2){
                          //   if(this.profileForm?.get('pan')?.get('docNumber')?.value!='')
                          //   this.profileForm?.get('pan')?.get('docNumber')?.disable();
                          //   if(this.profileForm?.get('aadhar')?.get('docNumber')?.value!='')
                          //   this.profileForm?.get('aadhar')?.get('docNumber')?.disable();
                          //   if(this.profileForm?.get('passport')?.get('docNumber')?.value!='')
                          //   this.profileForm?.get('passport')?.get('docNumber')?.disable();
                          //   if(this.profileForm?.get('voter')?.get('docNumber')?.value!='')
                          //   this.profileForm?.get('voter')?.get('docNumber')?.disable();
                          //   if(this.profileForm?.get('dl')?.get('docNumber')?.value!='')
                          //   this.profileForm?.get('dl')?.get('docNumber')?.disable();
                          // }
                          //}
                          //else{
                          //  this.getUserDetailsfn();
                          //}
                        });
                       
                    }
                    // else{
                    //   this.getUserDetailsfn();
                    // }
                  });
              }
              else {
                if (this.partnership_type == undefined || this.partnership_type == 0)
                  this.router.navigate(['./' + this.appRouteEnum.partnerRegistration])
                this.getUserDetailsfn();
              }
            });

        }
      }, err => {
        console.log(err)
        //this.getUserDetailsfn();
      });
  }

  bindUserDetails() {
    if (this.basicDetailsDataModel.basicProfileResponse != null) {
      this.profileForm.get('FirstName')?.setValue(this.basicDetailsDataModel.basicProfileResponse.first_name);
      this.profileForm.get('LastName')?.setValue(this.basicDetailsDataModel.basicProfileResponse.last_name);
      this.profileForm.get('Dob')?.setValue(this.basicDetailsDataModel.basicProfileResponse.dob);
      this.profileForm.get('Mobile')?.setValue(this.basicDetailsDataModel.basicProfileResponse.mobile_number.slice(this.basicDetailsDataModel.basicProfileResponse.mobile_number.length - 10));
      this.profileForm.get('EmailId')?.setValue(this.basicDetailsDataModel.basicProfileResponse.email);
      this.profileForm.get('AlternateEmailId')?.setValue(this.basicDetailsDataModel.basicProfileResponse.alt_email_address);
      this.getSearchedLocation('', this.basicDetailsDataModel.basicProfileResponse.pin_code.pin_code);

    }

    if (this.basicDetailsDataModel.userAddressResponse != null) {
      this.profileForm.get("StreetAddress1")?.setValue(this.basicDetailsDataModel.userAddressResponse.address);
      this.profileForm.get('StreetAddress2')?.setValue(this.basicDetailsDataModel.userAddressResponse.address_line1);
      this.profileForm.get('City')?.setValue(this.basicDetailsDataModel.userAddressResponse.city);
      this.profileForm.get('State')?.setValue(this.basicDetailsDataModel.userAddressResponse.state);
      this.getSearchedLocation('', this.basicDetailsDataModel.userAddressResponse.pin_code);
      this.basicDetailsDataModel.addressId = this.basicDetailsDataModel.userAddressResponse.id;
      this.basicDetailsDataModel.partnerContactId = this.basicDetailsDataModel.userAddressResponse.partner_contact;
    }

    if (this.basicDetailsDataModel.userIdentityProofResponse != null && this.basicDetailsDataModel.userIdentityProofResponse.length > 0) {
      let panDetails = this.basicDetailsDataModel.userIdentityProofResponse.filter(x => x.proof_type == this.apiRouteService.identityProofType.PAN_CARD); // for pan card
      if (panDetails != null && panDetails.length > 0) {
        this.basicDetailsDataModel.identityProofId = panDetails[0].id;
        this.profileForm.get('pan')?.get('docType')?.setValue("pan")
        this.profileForm.get('pan')?.get('docNumber')?.setValue(panDetails[0].id_number)
        this.profileForm.get('pan')?.get('documentId')?.setValue(panDetails[0].id)
        if (panDetails[0].document != undefined) {
           this.profileForm.get('pan')?.get('docImages')?.get('front')?.setValue(panDetails[0].document)
          this.profileForm.get('pan')?.get('docImages')?.get('frontView')?.setValue(panDetails[0].document)
        }
      }
      let aadharDetails = this.basicDetailsDataModel.userIdentityProofResponse.filter(x => x.proof_type == this.apiRouteService.identityProofType.AADHAR_CARD); // for aadhar card
      if (aadharDetails != null && aadharDetails.length > 0) {
        aadharDetails.forEach(aadhaar => {
          this.profileForm.get('aadhar')?.get('docType')?.setValue("aadhar")
          this.profileForm.get('aadhar')?.get('documentId')?.setValue(aadhaar.id)
          this.profileForm.get('aadhar')?.get('docNumber')?.setValue(aadhaar.id_number)
          if (aadhaar.document != null) {
               this.profileForm.get('aadhar')?.get('docImages')?.get('front')?.setValue(aadhaar.document)
              this.profileForm.get('aadhar')?.get('docImages')?.get('frontView')?.setValue(aadhaar.document)
          }
              if (aadhaar.document_back){
               this.profileForm.get('aadhar')?.get('docImages')?.get('back')?.setValue(aadhaar.document_back)
              this.profileForm.get('aadhar')?.get('docImages')?.get('backView')?.setValue(aadhaar.document_back)
              }
        });
      }
      let passportDetails = this.basicDetailsDataModel.userIdentityProofResponse.filter(x => x.proof_type == this.apiRouteService.identityProofType.PASSPORT); // for passport
      if (passportDetails != null && passportDetails.length > 0) {
        passportDetails.forEach(passport => {
          this.profileForm.get('passport')?.get('docType')?.setValue("passport")
          this.profileForm.get('passport')?.get('documentId')?.setValue(passport.id)
          this.profileForm.get('passport')?.get('docNumber')?.setValue(passport.id_number)
          if (passport.document != null) {
              this.profileForm.get('passport')?.get('docImages')?.get('front')?.setValue(passport.document)
              this.profileForm.get('passport')?.get('docImages')?.get('frontView')?.setValue(passport.document)
            } if (passport.document_back){
              this.profileForm.get('passport')?.get('docImages')?.get('back')?.setValue(passport.document_back)
              this.profileForm.get('passport')?.get('docImages')?.get('backView')?.setValue(passport.document_back)
            }
        });
      }
      let voterDetails = this.basicDetailsDataModel.userIdentityProofResponse.filter(x => x.proof_type == this.apiRouteService.identityProofType.VOTER_ID); // for voter id card
      if (voterDetails != null && voterDetails.length > 0) {
        voterDetails.forEach(voter => {
          this.profileForm.get('voter')?.get('docType')?.setValue("voter")
          this.profileForm.get('voter')?.get('documentId')?.setValue(voter.id)
          this.profileForm.get('voter')?.get('docNumber')?.setValue(voter.id_number)
          if (voter.document != null) {
              this.profileForm.get('voter')?.get('docImages')?.get('front')?.setValue(voter.document)
              this.profileForm.get('voter')?.get('docImages')?.get('frontView')?.setValue(voter.document)
           } if (voter.document_back){
              this.profileForm.get('voter')?.get('docImages')?.get('back')?.setValue(voter.document_back)
              this.profileForm.get('voter')?.get('docImages')?.get('backView')?.setValue(voter.document_back)
          }
        });
      }
      let dlDetails = this.basicDetailsDataModel.userIdentityProofResponse.filter(x => x.proof_type == this.apiRouteService.identityProofType.DRIVING_LICENCE); // for driving licence
      if (dlDetails != null && dlDetails.length > 0) {
        dlDetails.forEach(dl => {
          this.profileForm.get('dl')?.get('docType')?.setValue("dl")
          this.profileForm.get('dl')?.get('documentId')?.setValue(dl.id)
          this.profileForm.get('dl')?.get('docNumber')?.setValue(dl.id_number)
          if (dl.document != null) {
              this.profileForm.get('dl')?.get('docImages')?.get('front')?.setValue(dl.document)
           } this.profileForm.get('dl')?.get('docImages')?.get('frontView')?.setValue(dl.document)
            if (dl.document_back){
              this.profileForm.get('dl')?.get('docImages')?.get('back')?.setValue(dl.document_back)
              this.profileForm.get('dl')?.get('docImages')?.get('backView')?.setValue(dl.document_back)
              
          }
        });
      }
    }

  }

  getUserDetailsfn() {
    this.isSaveOrUpdate = "Update";
    //this.getUser.userId='002bbeaf-6d23-4766-bd9c-477703599e67';
    //var paramaterarray = this.storage.getStorageData("aflparams", true);
    var userId = '';
    if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter) {
      userId = this.storage.getStorageData("cognitoUserId", true);
    }
    else {
      userId = this.storage.getStorageData("cognitoId", false).replace(/['"]+/g, '');
    }
    this.profileForm.get('FirstName')?.disable();
    this.profileForm.get('LastName')?.disable();
    //  this.profileForm.get('Dob')?.disable();
    this.profileForm.get('countryCode')?.disable();
    this.profileForm.get('Mobile')?.disable();
    this.http.get(environment.corporateDetails + '?userId=' + userId)
      .subscribe((resp: UserResponse) => {
        this.userDataobj = resp;
        if (this.userDataobj?.basic_user_details?.cognitoId != null) {
          if (this.userDataobj?.user_details?.gender != undefined && this.userDataobj?.user_details?.gender != null && this.userDataobj?.user_details?.gender != "")
            this.profileForm.get('Gender')?.setValue(this.userDataobj?.user_details?.gender);
          // if(this.userDataobj?.user_details?.gender=="Male")  
          // this.profileForm.get('Gender')?.setValue(1);
          // else if (this.userDataobj?.user_details?.gender=="Female")  
          // this.profileForm.get('Gender')?.setValue(2);
          // else
          // this.profileForm.get('Gender')?.setValue(3);
          // else{
          //   this.profileForm.get('Gender')?.setValue('Male');
          // }
          this.profileForm.get('AlternateEmailId')?.setValue(this.userDataobj?.user_details?.alternateEmailId);
          if (this.userDataobj?.user_details?.represent != undefined && this.userDataobj?.user_details?.represent != null && this.userDataobj?.user_details?.represent != "" && !this.isEdit) {
            this.profileForm.get('Representer')?.setValue(this.userDataobj?.user_details?.represent);
          }
          // else
          // this.profileForm.get('Representer')?.setValue('1');
          // if (this.userDataobj?.user_details?.represent == "2" && !this.isEdit) {
          //   if (this.isEmailReq)
          //     this.router.navigate([`./` + this.appRouteEnum.CorporateDetails], { queryParams: { aoc: 'qtt' } });
          //   else
          //     this.router.navigate([`./` + this.appRouteEnum.CorporateDetails]);
          // }

          // if(this.userDataobj?.address_proof!=null) {
          //   console.log(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
          //   this.profileForm.get('address_proof')?.setValue(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
          // }
          if (this.userDataobj?.address_proof != null) {
            //console.log(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
            // this.corporateProfileForm.get('address_proof')?.setValue(JSON.parse(JSON.stringify(this.userDataobj?.address_proof)));
            this.profileForm.get('address_proof')?.get('documentId')?.setValue(this.userDataobj?.address_proof?.documentId);
            this.profileForm.get('address_proof')?.get('docType')?.setValue(this.userDataobj?.address_proof?.docType);
            this.profileForm.get('address_proof')?.get('docNumber')?.setValue(this.userDataobj?.address_proof?.docNumber);
            this.profileForm.get('address_proof')?.get('docImages')?.setValue(this.userDataobj?.address_proof?.docImages);
          }
          this.initialFirstName = this.userDataobj?.basic_user_details?.firstName;
          this.initialLastName = this.userDataobj?.basic_user_details?.lastName;
          this.profileForm.get('FirstName')?.setValue(this.userDataobj?.basic_user_details?.firstName);
          this.profileForm.get('LastName')?.setValue(this.userDataobj?.basic_user_details?.lastName);
          this.profileForm.get('Dob')?.setValue(this.userDataobj?.basic_user_details?.dateOfBirth);
          this.profileForm.get('Mobile')?.setValue(this.userDataobj?.basic_user_details?.mobileNumber);
          //  var cc={
          //    'prefixCode':this.userDataobj?.basic_user_details?.prefixCode,
          //    'countryCode':this.userDataobj?.basic_user_details?.countryCode
          //  }
          var cc = this.countryList?.find((x: any) => x.prefixCode == this.userDataobj?.basic_user_details?.prefixCode);
          this.selectedCity = cc;
          this.profileForm.get('countryCode')?.setValue(cc);
          this.checkIndiaPin();
          this.profileForm.get('EmailId')?.setValue(this.userDataobj?.basic_user_details?.email);
          if (this.userDataobj?.basic_user_details?.email != '' && this.userDataobj?.basic_user_details?.email != null && this.userDataobj?.basic_user_details?.email != undefined)
            this.profileForm.get('EmailId')?.disable();
          this.profileForm.get('PinCode')?.setValue(this.userDataobj?.basic_user_details?.pinCode);
          if (this.userDataobj?.basic_user_details?.applied_for_pan?.toLowerCase() == "true") {
            this.profileForm.get('applied_for_pan')?.setValue(true);
            this.profileForm?.get('pan')?.get('docNumber')?.setErrors(null);
            this.profileForm?.get('pan')?.get('docNumber')?.setValue('');
            this.profileForm?.get('pan')?.get('docNumber')?.disable();
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors(null);
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setValue('');
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.disable();
          }
          else {
            this.profileForm.get('applied_for_pan')?.setValue(false);
            this.profileForm?.get('pan')?.get('docNumber')?.setErrors({ required: true });
            this.profileForm?.get('pan')?.get('docNumber')?.enable();
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.setErrors({ required: true });
            this.profileForm?.get('pan')?.get('docImages')?.get('front')?.enable();
          }


          this.getpinCodefn();
          if (this.userDataobj?.adress_details != undefined) {
            if (this.userDataobj?.adress_details[0]?.addressId != "" && this.userDataobj?.adress_details[0]?.addressId != null && this.userDataobj?.adress_details[0]?.addressId != undefined) {
              this.profileForm.get("addressId")?.setValue(this.userDataobj?.adress_details[0]?.addressId);
            }
            this.profileForm.get("StreetAddress1")?.setValue(this.userDataobj?.adress_details[0]?.addressLine1);
            this.profileForm.get('StreetAddress2')?.setValue(this.userDataobj?.adress_details[0]?.addressLine2);
            this.profileForm.get('City')?.setValue(this.userDataobj?.adress_details[0]?.city);
            this.profileForm.get('State')?.setValue(this.userDataobj?.adress_details[0]?.state);
          }
          if (this.userDataobj?.doc_details != undefined) {
            let panindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'pan');
            if (panindex != -1) {
              this.profileForm.get('pan')?.get('docType')?.setValue(this.userDataobj?.doc_details[panindex]?.docType)
              this.profileForm.get('pan')?.get('documentId')?.setValue("")
              this.initialPan = this.userDataobj?.doc_details[panindex]?.docNumber;
              this.profileForm.get('pan')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[panindex]?.docNumber)
              if (this.userDataobj?.doc_details[panindex]?.docNumber != undefined && this.userDataobj?.doc_details[panindex]?.docNumber != "" && this.userDataobj?.doc_details[panindex]?.docNumber != null) {
                this.profileForm.get('pan')?.get('docNumber')?.disable();
                this.isPanEditable = true;
              }
              this.profileForm.get('pan')?.get('name')?.setValue(this.userDataobj?.doc_details[panindex]?.karzaRes?.result?.returnName)
              if (this.userDataobj?.doc_details[panindex]?.docUrl != undefined) {
                this.profileForm.get('pan')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[panindex]?.docUrl?.front)
              }
            }
            let aadharindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'aadhar');
            if (aadharindex != -1) {
              this.profileForm.get('aadhar')?.get('docType')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docType)
              this.profileForm.get('aadhar')?.get('documentId')?.setValue("")
              this.profileForm.get('aadhar')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docNumber)
              if (this.userDataobj?.doc_details[aadharindex]?.docUrl != undefined) {
                this.profileForm.get('aadhar')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.front)
                this.profileForm.get('aadhar')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[aadharindex]?.docUrl?.back)
              }
            }
            let passportindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'passport');
            if (passportindex != -1) {
              this.profileForm.get('passport')?.get('docType')?.setValue(this.userDataobj?.doc_details[passportindex]?.docType)
              this.profileForm.get('passport')?.get('documentId')?.setValue("")
              this.profileForm.get('passport')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[passportindex]?.docNumber)
              if (this.userDataobj?.doc_details[passportindex]?.docUrl != undefined) {
                this.profileForm.get('passport')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.front)
                this.profileForm.get('passport')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[passportindex]?.docUrl?.back)
              }
            }
            let voterindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'voter');
            if (aadharindex != -1) {
              this.profileForm.get('voter')?.get('docType')?.setValue(this.userDataobj?.doc_details[voterindex]?.docType)
              this.profileForm.get('voter')?.get('documentId')?.setValue("")
              this.profileForm.get('voter')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[voterindex]?.docNumber)
              if (this.userDataobj?.doc_details[voterindex]?.docUrl != undefined) {
                this.profileForm.get('voter')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.front)
                this.profileForm.get('voter')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[voterindex]?.docUrl?.back)
              }
            }
            let dlindex = this.userDataobj?.doc_details?.findIndex(x => x.docType == 'dl');
            if (dlindex != -1) {
              this.profileForm.get('dl')?.get('docType')?.setValue(this.userDataobj?.doc_details[dlindex]?.docType)
              this.profileForm.get('dl')?.get('documentId')?.setValue("")
              this.profileForm.get('dl')?.get('docNumber')?.setValue(this.userDataobj?.doc_details[dlindex]?.docNumber)
              if (this.userDataobj?.doc_details[dlindex]?.docUrl != undefined) {
                this.profileForm.get('dl')?.get('docImages')?.get('front')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.front)
                this.profileForm.get('dl')?.get('docImages')?.get('back')?.setValue(this.userDataobj?.doc_details[dlindex]?.docUrl?.back)
              }
            }
          }
          // this.profileForm.get('pan')?.setValue()
          // this.profileForm.get('PanCardNo')?.setValue(this.userDataobj?.doc_details[3]?.docNumber);
          // this.profileForm.get('customFile')?.clearValidators();
          // this.profileForm.get('customFile')?.updateValueAndValidity();

          this.isPanExist = true;
           
        }
        if (this.userDataobj?.user_details?.represent != undefined && this.userDataobj?.user_details?.represent != "" && this.userDataobj?.user_details?.represent != null) {
          //this.profileForm.disable();
          this.isEditable = true;
        }
        if (this.hideAppliedForPan) {

          if (this.profileForm.get('pan')?.get('docNumber')?.value == '') {
            this.profileForm.get('pan')?.get('docNumber')?.enable();
            this.profileForm.get('applied_for_pan')?.setValue(false);
          }
        }
        if (this.isCallCenter == "1" || this.role == this.apiRouteService.roles.callcenter || this.role == this.apiRouteService.roles.superadmin) {
          this.isEditable = false;
          if (this.profileForm.get('pan')?.get('docNumber')?.value != '' && this.role == this.apiRouteService.roles.superadmin) {
            if (this.profileForm.get('applied_for_pan')?.value)
              this.isPanEditable = false;
            this.isSuperAdmin = false;
            this.profileForm.get('pan')?.get('docNumber')?.enable();
          }

          this.profileForm.get('FirstName')?.enable();
          this.profileForm.get('LastName')?.enable();
          this.profileForm.get('countryCode')?.enable();
          this.profileForm.get('EmailId')?.enable();
          this.profileForm.get('countryCode')?.disable();
          // this.profileForm.get('Representer')?.setValue('1');
        }
         
      }, error => {
        console.log(error);
         

      })


  }


  disableControls() {
    this.profileForm.get('FirstName')?.disable();
    this.profileForm.get('LastName')?.disable();
    this.profileForm.get('Mobile')?.disable();
    this.profileForm.get('EmailId')?.disable();
    this.profileForm.get('State')?.disable();
    this.profileForm.get('City')?.disable();
    this.profileForm.get('countryCode')?.disable();
    if (this.profileForm.get('passport')?.get('docNumber')?.value != '') {
      this.profileForm.get('passport')?.get('docNumber')?.disable
    }
  }
}

