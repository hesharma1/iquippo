import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AccountService } from 'src/app/services/account';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Bankdetails } from 'src/app/shared/abstractions/user';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-partner-dealer-bank-details',
  templateUrl: './partner-dealer-bank-details.component.html',
  styleUrls: ['./partner-dealer-bank-details.component.css']
})
export class PartnerDealerBankDetailsComponent implements OnInit {

  public bankingForm: FormGroup;
  public steps = {
    current: 3,
    total: [
      { title: 'Profile' },
      { title: 'Preference' },
      { title: 'Banking' },
      { title: 'Terms & Conditions' },
    ]
  }
  public toEdit: any;
  public bankList: Array<{ id: string; bankName: string }> = [];
  public congnitoId: string = '';
  objData: any;
  masterData: any;
  partnerEntity: any;
  partnerAllDetail: any;
  entityDetails:any
  PartnerType: any;
  manuFactBrands: any = [];
  @ViewChild('cancelledCheque') cancelledCheque!: ElementRef;
  documentName: Array<any> = [];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public storage: StorageDataService,public appRouteEnum:AppRouteEnum,
     private router: Router, private notify: NotificationService, public _Activatedroute: ActivatedRoute, private agreementServiceService: AgreementServiceService,
     private s3: S3UploadDownloadService, public accountService: AccountService,public partnerDealerRegistrationService:PartnerDealerRegistrationService) {

    this.bankingForm = this.fb.group({
      bankDetailArray: this.fb.array([])
    });
  }

   ngOnInit()
  {
    this.toEdit = this._Activatedroute.snapshot.queryParamMap.get('toEdit');
     this.getAllPartnerInfo();
    // this.addBankFormGroup();
    this.congnitoId = this.storage.getStorageData("cognitoId", false);
    this.PartnerType = this.storage.getStorageData('registrationTypeID', false);
    
  }
  getAllPartnerInfo() {
    this.congnitoId = this.storage.getStorageData('cognitoId', false);
     // queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    this.partnerDealerRegistrationService.getPartnerAllInfo(this.congnitoId).subscribe((res: any) => {
      console.log(res);
        this.storage.setStorageData('partnerAllDetail', res, true)
        this.partnerAllDetail = res;
        this.getBankListfn();
        this.getEntityDetails();
     }, err => {
      console.log(err);
    })
  }
  get bankDetailsArray() {
    return this.bankingForm.controls.bankDetailArray as FormArray;
  }

  getEntityDetails() {
    //this.spinner.show();
    this.agreementServiceService.getPartnerEntityparticular(this.congnitoId, this.PartnerType).subscribe(
      (res: any) => {

        this.entityDetails = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  getBankListfn() {
    this.spinner.show();
    this.accountService.getBankListOnboard("?limit=999&ordering=bank_name").pipe((finalize(() => { this.spinner.hide(); }))).subscribe(
      (data: any) => {
        this.bankList = data.results;
    
      this.partnerEntity = JSON.parse(this.storage.getStorageData("partnerEntityCreate",false))
      this.partnerAllDetail = JSON.parse(this.storage.getStorageData("partnerAllDetail",false))
      if(this.partnerEntity != null && this.partnerEntity != undefined && this.partnerEntity != '') {
        this.accountService.getPartnerBankDetails(this.partnerEntity.id).subscribe((res:any)=>{
          this.objData=res.results;
        if(this.objData.length>0){
        this.objData.forEach( (cust :Bankdetails) => {
          this.bankDetailsArray.push(this.createBankFormGroup(cust));

          //console.log(this.customersControls);
        });
        }
        else{
          this.addBankFormGroup();
        }
      
      })
      }  
      else if(this.partnerAllDetail != null && this.partnerAllDetail != undefined && this.partnerAllDetail != '') {
        this.partnerAllDetail = JSON.parse(this.storage.getStorageData("partnerAllDetail",false))
        this.accountService.getPartnerBankDetails(this.partnerAllDetail?.mapping?.partner.id).subscribe((res:any)=>{
          console.log(res);
          this.objData=res.results;
        if(this.objData.length>0){
        this.objData.forEach( (cust :Bankdetails) => {
          this.bankDetailsArray.push(this.createBankFormGroup(cust));
          //console.log(this.customersControls);
        });
        }
        else{
          this.addBankFormGroup();
        }
      
      })
      }
    }
    )
  }

  addBankFormGroup() {
    this.bankDetailsArray.push(this.createBankFormGroup());
  }

  
  createBankFormGroup(bankObj?:any): FormGroup {
    if(bankObj==undefined || bankObj==null ){
    return this.fb.group({
      id: [''],
      bank: ['', Validators.compose([Validators.required])],
      branch: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      account_holder_name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      account_number: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
      ifsc_code: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      account_type: ['', Validators.compose([Validators.required])],
      cancelled_cheque: ['', Validators.compose([Validators.required])],
      partner:this.partnerEntity?.id == null ? this.partnerAllDetail?.mapping?.partner.id :  this.partnerEntity.id,
      //crm_bank_id: ""
    })
    }
    else{
      return this.fb.group({
        id: [bankObj.id],
        bank: [bankObj.bank, Validators.compose([Validators.required])],
        branch: [bankObj.branch, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_holder_name: [bankObj.account_holder_name, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_number: [bankObj.account_number, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
        ifsc_code: [bankObj.ifsc_code, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_type: [bankObj.account_type, Validators.compose([Validators.required])],
        cancelled_cheque: [bankObj.cancelled_cheque, Validators.compose([Validators.required])],
        partner: bankObj.partner,
        //bank_id: bankObj.crm_partner_bank_id
      })
    }
  }

  removeBankFormGroup(id: any) {
     var i = '' + id + '';
     var bankId = this.bankingForm?.get('customersArray')?.get(i)?.get('UserBankId')?.value;
     this.bankDetailsArray.removeAt(id);
     //this.DeletedBankList.push(bankId);
  }

  async uploadCheque(e: any, index: number, bank: any) {
    const file = e.target.files[0];
    if (file && (file.size <= 5242880) && (file.type == 'image/png' || file.type == 'image/jpg' || 
    file.type == 'image/jpeg' || file.type == 'image/bmp')) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => { }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + "/" + this.congnitoId + '/' + 'partnerDealer-bank-details' + (index + 1) + '.' + fileExt;
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        bank.controls.cancelled_cheque.setValue(uploaded_file?.Location);
        this.documentName[index] = { name: fileFormat, url: uploaded_file.Location };

      }
      else {
        bank.controls.cancelled_cheque.setValue('');
        this.notify.error("Upload Failed");
      }
    } else {
      this.notify.error("Invalid Image.")
      if (bank.controls.cancelled_cheque.value == '') {
        bank.controls.cancelled_cheque.setErrors({
          required: true
        })
      }
    }
}

deleteCheque(value,bank,i) {
  this.s3.deleteFile(value).subscribe(
    data => {
      this.notify.success('File Successfully deleted!!');
      bank.controls.cancelled_cheque.setValue('');
      this.documentName[i] = '';
      this.cancelledCheque.nativeElement.value = "";
    },
    err => {
      console.log(err);
      this.notify.error(err);
    }
  )
}

  saveBankDetailsfn() {
    this.spinner.show();
    
    let payload = this.bankingForm.value.bankDetailArray;
    this.accountService.postPartnerBankDetails(payload).pipe((finalize(() => { this.spinner.hide(); }))).subscribe(
      (data: any) => {
        console.log(data);
        this.notify.success("Bank Details Saved !!");
        var partnership_type = Number(this.storage.getStorageData('registrationTypeID',true));
        if(partnership_type==1 || partnership_type==2) {
          this.router.navigate([`./customer/dashboard/partner-dealer/agreement-details`])
        }
        else{
          this.saveOnSubmitBrands();
          this.router.navigate([`./`+this.appRouteEnum.partnerRegistration]);
          // this.router.navigate([`./dashboard`])
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  saveOnSubmitBrands() {
    //this.spinner.show();
    let object = {};
    if (this.entityDetails && this.entityDetails.length > 0 && this.entityDetails[0].manufacture_brands) {
      this.entityDetails[0].manufacture_brands?.forEach(element => {
        this.manuFactBrands.push(element.id);
      });
    }
    object = {
      entity_type: this.entityDetails[0].entity_type,
      is_msme_number: this.entityDetails[0].is_msme_number,
      msme_number: this.entityDetails[0].msme_number,
      company_name: this.entityDetails[0].company_name,
      incorporation_date: this.entityDetails[0].incorporation_date,
      mobile: this.entityDetails[0].mobile,
      status: 1,
      created_by: this.entityDetails[0].created_by,
      partner_admin: this.congnitoId,
      manufacture_brands: this.manuFactBrands,
    };
    this.agreementServiceService
      .putDealerEntityDetails(object, this.entityDetails[0].id)
      .subscribe((res) => {
      });
  }
}
