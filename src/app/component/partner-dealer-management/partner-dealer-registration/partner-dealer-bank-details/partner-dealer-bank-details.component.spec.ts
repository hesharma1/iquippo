import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerDealerBankDetailsComponent } from './partner-dealer-bank-details.component';

describe('PartnerDealerBankDetailsComponent', () => {
  let component: PartnerDealerBankDetailsComponent;
  let fixture: ComponentFixture<PartnerDealerBankDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerBankDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerBankDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
