import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInProgressComponent } from './request-in-progress.component';

describe('RequestInProgressComponent', () => {
  let component: RequestInProgressComponent;
  let fixture: ComponentFixture<RequestInProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestInProgressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
