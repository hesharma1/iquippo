import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-request-in-progress',
  templateUrl: './request-in-progress.component.html',
  styleUrls: ['./request-in-progress.component.css']
})
export class RequestInProgressComponent implements OnInit {

  constructor(public sharedService:SharedService ) { }

  ngOnInit(): void {
  }

}
