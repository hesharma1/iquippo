import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerCorporateNextDetailsComponent } from './partner-dealer-corporate-next-details.component';


describe('PartnerDealerCorporateNextDetailsComponent', () => {
  let component: PartnerDealerCorporateNextDetailsComponent;
  let fixture: ComponentFixture<PartnerDealerCorporateNextDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerCorporateNextDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerCorporateNextDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
