import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/app/services/shared-service.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { AddressDetails } from 'src/app/shared/abstractions/user';
import { PartnerDealerRegistrationAddress } from 'src/app/models/partner-dealer-management/partner-dealer-registration-data.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { AccountService } from 'src/app/services/account';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { Observable } from 'rxjs';
import { MatOption } from '@angular/material/core';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-partner-dealer-corporate-next-details',
  templateUrl: './partner-dealer-corporate-next-details.component.html',
  styleUrls: ['./partner-dealer-corporate-next-details.component.css'],
})
export class PartnerDealerCorporateNextDetailsComponent implements OnInit {
  @ViewChild('subAllSelected') subAllSelected !: MatOption;
  corporateProfileForm: FormGroup;
  cognitoId: any;
  AddressFormArray: any;
  TaxFormArray: any;
  BrandLocationsArray: any;
  stateList: any = [];
  partnerDealerRegistrationAddress: any;
  pinCodeResponse?: PincodeResponse;
  partnerId: any;

  brandData: any;
  cityData: any = [];
  getUserAddress: any;
  userId: any;
  partnerEntityBrandLocation: any = [];
  PartnerType: any;
  stateListData: any = [];
  entityDetails: any;
  modelList: any;
  getBrandLocations: any;
  partnerBrands: any;
  cityListData: any = [];
  isOption: boolean = false;
  selectedCityData: any = [];
  manuFactBrands:any = [];
  isNewBrandName = false;
  someArray:any = [];
  isCheckArray:any = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private adminMasterService: AdminMasterService,
    private storage: StorageDataService,
    public sharedService: SharedService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private agreementServiceService: AgreementServiceService,
    private accountService: AccountService,
    private route: ActivatedRoute,
    private router: Router,
    private notify: NotificationService
  ) {
    this.corporateProfileForm = this.fb.group({
      //Gender:['M'],
      StreetAddress1: [''],
      PinCode: ['', Validators.required],
      State: ['', Validators.required],
      City: ['', Validators.required],
      BrandLocationArray: new FormArray([]),
      Brand: [''],
    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['isrequired'] == 'true') {
        this.isOption = true;
      }
      else {
        this.isOption = false;
      }
    });
    if (
      this.storage.getStorageData('cognitoId', false) != null &&
      this.storage.getStorageData('cognitoId', false) != undefined
    ) {
      this.cognitoId = this.storage.getStorageData('cognitoId', false);
    }
    if (
      this.storage.getStorageData('partnerDealerPartnerId', true) != null &&
      this.storage.getStorageData('partnerDealerPartnerId', true) != undefined
    ) {
      this.userId = this.storage.getStorageData('partnerDealerPartnerId', true);
    }
    if (
      this.storage.getStorageData('registrationTypeID', false) != null &&
      this.storage.getStorageData('registrationTypeID', false) != undefined
    ) {
      this.PartnerType = this.storage.getStorageData(
        'registrationTypeID',
        false
      );
    }
    if (
      this.storage.getStorageData('partnerEntityCreate', false) != null &&
      this.storage.getStorageData('partnerEntityCreate', false) != undefined
    ) {
      this.entityDetails = JSON.parse(
        this.storage.getStorageData('partnerEntityCreate', false)
      );
    }

    // this.BrandLocationsArray = <FormArray>(
    //   this.corporateProfileForm.controls['BrandLocationArray']
    // );
    //this.ValidationCheck();
    // this.getCity('');
    this.getStateData();
    this.getBrandData();
    //  this.getCityData('limit=999');
    this.renderCityData('limit=9999');
    this.getuserAddressDetails();
    this.getEntityBrandLocationDetails();
    this.getBrandsMapping();
    this.route.params.subscribe((params: Params): void => {
      this.partnerId = params['id'];
    });
  }

  checkOtherBrand(value) {
    if(value == 'Other' || value == 'other') {
      this.isNewBrandName = true;
    } else {
      this.isNewBrandName = false;
    }
  }

  renderCityData(queryParams: string) {
    ////this.spinner.show();
    this.getCityData(queryParams).subscribe((res: any) => {
      this.cityData = res.results;
      if (this.cityData && this.cityData.length > 0) {
        console.log("cityData", this.cityData);
        //   
      }
    })
  }

  getCityData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getCityMaster(queryParams)
  }

  getBrandsMapping() {
    // //this.spinner.show();
    this.agreementServiceService.getPartnerEntityparticular(this.cognitoId, this.PartnerType).subscribe(
      (res: any) => {
        //   
        this.partnerBrands = res.results;
        if (this.partnerBrands && this.partnerBrands.length > 0 && this.partnerBrands[0].manufacture_brands) {
          if (this.partnerBrands != null && this.partnerBrands != undefined) {
            this.partnerBrands[0].manufacture_brands?.forEach(element => {
              this.manuFactBrands.push(element.id);
            });
          }
        }
      },
      (err) => {
        console.error(err);
      }
    );
  }

  getBrandData() {
    this.agreementServiceService
      .getBrandMaster('limit=999')
      .subscribe((data: any) => {
        this.brandData = data.results;
      });
  }

  // getCityData(queryParams: string) {
  //   this.adminMasterService
  //     .getCityMaster(queryParams).subscribe((data: any) => {
  //       this.cityData = data.results;
  //       console.log("cityData", this.cityData);
  //     });

  // }


  // ValidationCheck(i) {
  //   if(this.PartnerType == 1) {
  //     arrayControl.controls[i]?.get('typeOfOffer')?.setValidators(Validators.required);
  //     arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
  //     this.corporateProfileForm?.get(
  //   }
  // }

  getCity(stateId) {
    let queryPram = "limit=999&state__id__in=" + stateId;
    this.agreementServiceService.getCity(queryPram).subscribe((data: any) => {
      this.cityData = data.results;
    });
  }

  getCriteria(stateId) {
    this.cityListData = this.cityData.filter(x => x.state.id == stateId);
    return this.cityListData;
    // let queryPram = "limit=999&state__id__in=" + stateId;
    // this.agreementServiceService.getCity(queryPram).subscribe((data: any) => {
    //   this.cityData = data.results;
    //   console.log("citydata", this.cityData);
    //   return this.cityData;
    // });
    // if (this.selectedCityData && this.selectedCityData.length > 0) {
    //   this.cityListData = this.selectedCityData.filter(x => x.state.id == stateId);
    // }
    // else {
    //   this.cityListData = this.cityData.filter(x => x.state.id == stateId);
    // }

    this.cityListData = this.cityData.filter(x => x.state.id == stateId);
    return this.cityListData;
  }

  // delete(rowdata) {
  //   const actions = "success!!"
  //   this.agreementServiceService.deleteBrandLocation(id).subscribe(
  //     (res: any) => {
  //        
  //       this._snackBar.open('Entity deleted successfully', actions, {
  //         duration: 8000,
  //       });
  //       this.getPartnerEntityList();
  //     },
  //     (err) => {
  //       console.error(err);
  //     }
  //   );

  // }

  createAddressFormGroup(cust?: AddressDetails) {
    if (cust != undefined) {
      if (
        this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
      ) {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(
            cust?.addressLine1,
            Validators.required
          ),
          StreetAddress2: new FormControl(
            cust?.addressLine2,
            Validators.required
          ),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo, Validators.required),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      } else {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(
            cust?.addressLine1,
            Validators.required
          ),
          StreetAddress2: new FormControl(
            cust?.addressLine2,
            Validators.required
          ),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
    } else {
      if (
        this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
      ) {
        return new FormGroup({
          StreetAddress1: new FormControl('', Validators.required),
          addressId: new FormControl(''),
          StreetAddress2: new FormControl('', Validators.required),
          PinCode: new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl('', Validators.required),
          City: new FormControl('', Validators.required),
          GSTNo: new FormControl('', Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
      {
        return new FormGroup({
          StreetAddress1: new FormControl('', Validators.required),
          addressId: new FormControl(''),
          StreetAddress2: new FormControl('', Validators.required),
          PinCode: new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl('', Validators.required),
          City: new FormControl('', Validators.required),
          GSTNo: new FormControl(''),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
    }
  }

  newBrandLocations() {
    const TaxArray = this.corporateProfileForm?.get(
      'BrandLocationArray'
    ) as FormArray;
    this.BrandLocationsArray.push(this.createBrandLocationFormGroup(''));
  }

  createBrandLocationFormGroup(locationobj?: any): FormGroup {
    if (locationobj == undefined || locationobj == null || locationobj == '') {
      return this.fb.group({
        id: new FormControl(''),
        brandDeal: new FormControl(''),
        brandName: new FormControl(''),
        brandState: new FormControl(''),
        brandCity: new FormControl(''),
      });
    } else {
      // let queryPram = "limit=999&state__id__in=" + locationobj.city.state.id;
      // this.agreementServiceService.getCity(queryPram).subscribe((data: any) => {
      //   this.selectedCityData = data.results;
      //   console.log("selectedCityData", this.selectedCityData);
      // });
      return this.fb.group({
        id: [locationobj.id],
        brandDeal: [locationobj.brand.id, Validators.compose([Validators.required])],
        brandName: [locationobj.brand.name, Validators.compose([Validators.required, Validators.maxLength(50)])],
        brandState: [locationobj.city[0].state.id, Validators.compose([Validators.required, Validators.maxLength(50)])],
        brandCity: [locationobj.city.id, Validators.compose([Validators.required, Validators.maxLength(50)])],
      })
    }
  }

  // initializRecords(partnerEntityBrandLocation: any) {
  //   const formArray = new FormArray([]);
  //   for (let i = 0; i < partnerEntityBrandLocation.length; i++) {
  //     formArray.push(
  //       this.fb.group({
  //         id: partnerEntityBrandLocation[i].id,
  //         brandDeal: partnerEntityBrandLocation[i].brand.id,
  //         brandCity: partnerEntityBrandLocation[i].city.id,
  //         brandName: partnerEntityBrandLocation[i].brand.name,
  //         brandState: partnerEntityBrandLocation[i].city.state.id,
  //       })
  //     );
  //   }
  //   this.corporateProfileForm.setControl('BrandLocationArray', formArray);
  // }

  /**get entity details */
  getuserAddressDetails() {
    // //this.spinner.show();
    this.agreementServiceService
      .getPartnerEntityAddressparticular(this.cognitoId, this.PartnerType)
      .subscribe(
        (res: any) => {
          //    
          this.getUserAddress = res.results;
          if (this.getUserAddress.length > 0) {
            this.patchAddressValues(this.getUserAddress[0]);
          }
        },
        (err) => {
          console.error(err);
        }
      );
  }

  patchAddressValues(entityData) {
    this.corporateProfileForm
      ?.get('StreetAddress1')
      ?.setValue(entityData.address);
    this.corporateProfileForm?.get('PinCode')?.setValue(entityData.pin_code.pin_code);
    this.corporateProfileForm?.get('State')?.setValue(entityData.pin_code.city.state.name);
    this.corporateProfileForm?.get('City')?.setValue(entityData.pin_code.city.name);
  }

  /**get entity brand locations details */
  getEntityBrandLocationDetails() {
    // //this.spinner.show();

    this.agreementServiceService
      .getPartnerEntityBrandparticular(this.cognitoId, this.PartnerType)
      .subscribe(
        (res: any) => {
          //  
          this.getBrandLocations = res.results;
          if (this.getBrandLocations.length > 0) {
            this.getBrandLocations.forEach((cust, index) => {
              this.BrandLocationsArray = <FormArray>(
                this.corporateProfileForm.controls['BrandLocationArray']
              );
              //corporateProfileForm.controls['BrandLocationArray']?.value[k]?.brandState
              this.BrandLocationsArray.push(this.createBrandLocationFormGroup(cust));
              let array:any = [];
              let data = cust?.city.forEach((element) => {
                array.push(element.id)
              });
              this.someArray.push(array)
              console.log(this.someArray)
            });
          } else {
            this.BrandLocationsArray = <FormArray>(
              this.corporateProfileForm.controls['BrandLocationArray']
            );
            this.BrandLocationsArray.push(this.createBrandLocationFormGroup(''));
            this.isCheckArray[0] = false;

          }
        },
        (err) => {
          console.error(err);
        }
      );
  }

  onBrandSelect() {
    this.agreementServiceService.getBrandMaster('limit=999').subscribe(
      (res: any) => {
        this.modelList = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  removeBrandFormGroup(index, id: any) {
    var i = '' + index + '';
    this.BrandLocationsArray.removeAt(i);
    if(id != null && id != undefined && id != '') {
      this.agreementServiceService.deleteBrandLocations(id).subscribe((res) => {
        console.log("res", res);
      });
    }
  }

  /*** get state data*/
  getStateData() {
    this.agreementServiceService.getStateMaster("limit=999").subscribe(
      (res: any) => {
        this.stateListData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  getLocationData() {
    let pincode = this.corporateProfileForm?.get('PinCode')?.value;
    let queryParam = `search=${pincode}`;
    if (pincode.length > 5) {
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
          this.stateList = res.results;
          if(this.stateList.length >0) {
            this.corporateProfileForm
            ?.get('City')
            ?.setValue(this.stateList[0].city?.name);
          this.corporateProfileForm
            ?.get('State')
            ?.setValue(this.stateList[0].city.state?.name);
          } else {
            this.corporateProfileForm?.get('PinCode')?.setErrors({'incorrect': true});
            this.notify.error("please enter valid pincode",false)
            this.corporateProfileForm
            ?.get('City')
            ?.setValue('');
          this.corporateProfileForm
            ?.get('State')
            ?.setValue('');
          }
        },
        (err) => {
        }
      );
    } else {
      this.corporateProfileForm?.get('City')?.setValue('');
      this.corporateProfileForm?.get('State')?.setValue('');
    }
  }

  onSubmit() {
    this.checkValidation();
    this.corporateProfileForm?.markAllAsTouched();
    if (this.corporateProfileForm.valid) {
      // //this.spinner.show();
      this.partnerDealerRegistrationAddress = new PartnerDealerRegistrationAddress();
      this.partnerDealerRegistrationAddress.address = this.corporateProfileForm?.get(
        'StreetAddress1'
      )?.value;
      this.partnerDealerRegistrationAddress.state = this.stateList[0].city.state?.id;
      this.partnerDealerRegistrationAddress.partner = parseInt(this.partnerId);
      this.partnerDealerRegistrationAddress.city = this.stateList[0].city?.id;
      this.partnerDealerRegistrationAddress.pin_code = this.corporateProfileForm?.get('PinCode')?.value;
      this.agreementServiceService
        .postDealerAddressDetails(this.partnerDealerRegistrationAddress)
        .subscribe((res) => {
          var getResponse = res as ResponseData;
          this.router.navigate(['customer/dashboard/partner-dealer/bank-details']);
        });
    }
  }

  /**on tav chnage */
  onTabChanged(event: any) { }

  checkaddressFun(ev: KeyboardEvent) {
    let k = ev.keyCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 9 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

 

  confirm() {
    if (this.PartnerType != 2 && this.PartnerType != 3) {
      if (this.getUserAddress && this.getUserAddress.length <= 0) {
        this.submitBrandLocation();
      } else {
        this.updateBrandLocation();
      }
    } else if (this.PartnerType == 2) {
      this.saveOnSubmitBrands();
    } else if(this.PartnerType == 3) {
      if (this.getUserAddress && this.getUserAddress.length <= 0) {
        this.onSubmit();
      } else {
        this.updateDealer();
      }
    }else {
     
    }

  }

  submitBrandLocation() {
    this.checkValidation();
    this.corporateProfileForm?.markAllAsTouched();
    this.checkDuplicateEnteries();
    if (this.corporateProfileForm?.valid) {
      //  //this.spinner.show();
      let object: any = {};
      let brandLocation: any = [];
      this.BrandLocationsArray = <FormArray>(
        this.corporateProfileForm.controls['BrandLocationArray']
      );
      if (this.BrandLocationsArray && this.BrandLocationsArray.length > 0 && this.BrandLocationsArray.value && this.BrandLocationsArray.value.length > 0) {
        this.BrandLocationsArray.value.forEach((element) => {
          let brandCity = element.brandCity ? element.brandCity.filter(x => x != 0 && x != undefined) : [];
          object = {
            id: '',
            partner: parseInt(this.partnerId),
            brand: element.brandDeal,
            city: brandCity,
          };
          brandLocation.push(object);
        });
      }
      this.agreementServiceService.postUpdateDealerbrandLocation(brandLocation).subscribe((res) => {
        //    
        this.onSubmit();
        var getResponse = res as ResponseData;
      });

    }
  }

  /**update dealer */
  updateDealer() {
    this.checkValidation();
    this.corporateProfileForm?.markAllAsTouched();
    if (this.corporateProfileForm.valid) {
      ////this.spinner.show();
      this.partnerDealerRegistrationAddress = new PartnerDealerRegistrationAddress();
      this.partnerDealerRegistrationAddress.address = this.corporateProfileForm?.get(
        'StreetAddress1'
      )?.value;
      this.partnerDealerRegistrationAddress.state = this.getUserAddress[0].pin_code.city.state.id;
      //this.partnerDealerRegistrationAddress.partner = parseInt(this.partnerId);
      this.partnerDealerRegistrationAddress.city = this.getUserAddress[0].pin_code.city.id;
      this.partnerDealerRegistrationAddress.pin_code = this.corporateProfileForm?.get('PinCode')?.value;
      this.agreementServiceService.updateDealerAddressDetails(this.partnerDealerRegistrationAddress, this.getUserAddress[0].id)
        .subscribe((res) => {
          this.router.navigate(['customer/dashboard/partner-dealer/bank-details']);
          var getResponse = res as ResponseData;
        });
    }
  }

  updateBrandLocation() {
    this.checkValidation();
    this.corporateProfileForm?.markAllAsTouched();
    this.checkDuplicateEnteries();
    if (this.corporateProfileForm?.valid) {
      // //this.spinner.show();
      let object: any = {};
      let brandLocation: any = [];
      this.BrandLocationsArray = <FormArray>(
        this.corporateProfileForm.controls['BrandLocationArray']
      );
      if (this.BrandLocationsArray && this.BrandLocationsArray.length > 0) {
        this.BrandLocationsArray.value.forEach((element) => {
          let brandCity = element.brandCity ? element.brandCity.filter(x => x != 0 && x != undefined) : [];
          object = {
            partner: parseInt(this.partnerId),
            id: element.id == null ? null : element.id,
            brand: element.brandDeal,
            city: brandCity,
          };
          brandLocation.push(object);

        });
      }
      this.agreementServiceService
        .postUpdateDealerbrandLocation(brandLocation)
        .subscribe((res) => {
          //  
          var getResponse = res as ResponseData;
          this.updateDealer();
        });
    }
  }

  saveOnSubmitBrands() {
    this.checkValidation();
    this.corporateProfileForm?.markAllAsTouched();
    if (this.corporateProfileForm?.valid) {
      ////this.spinner.show();
      let object = {};
      object = {
        entity_type: this.entityDetails.entity_type,
        is_msme_number: this.entityDetails.is_msme_number,
        msme_number: this.entityDetails.msme_number,
        company_name: this.entityDetails.company_name,
        incorporation_date: this.entityDetails.incorporation_date,
        mobile: this.entityDetails.mobile,
        status: this.entityDetails.status,
        created_by: this.entityDetails.created_by,
        partner_admin: this.cognitoId,
        manufacture_brands: this.corporateProfileForm?.get('Brand')?.value,
      };
      this.agreementServiceService
        .putDealerEntityDetails(object, this.partnerId)
        .subscribe((res) => {
          var getResponse = res as ResponseData;

        });
    }
    if (this.getUserAddress && this.getUserAddress.length <= 0) {
      this.onSubmit();
    } else {
      this.updateDealer();
    }
  }

  checkValidation() {
    if (this.PartnerType == 1) {
      this.corporateProfileForm
        .get('StreetAddress1')
        ?.setValidators(Validators.required);
      this.corporateProfileForm.get('StreetAddress1')?.updateValueAndValidity();
      this.corporateProfileForm
        .get('PinCode')
        ?.setValidators(Validators.required);
      this.corporateProfileForm.get('PinCode')?.updateValueAndValidity();
      this.corporateProfileForm.get('Brand')?.clearValidators();
      this.corporateProfileForm.get('Brand')?.updateValueAndValidity();
      var arrayControl = this.corporateProfileForm?.controls[
        'BrandLocationArray'
      ] as FormArray;
      for (let i = 0; i < arrayControl.value.length; i++) {
        arrayControl.controls[i]
          ?.get('brandDeal')
          ?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('brandDeal')?.updateValueAndValidity();
        arrayControl.controls[i]
          ?.get('brandState')
          ?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('brandState')?.updateValueAndValidity();
        arrayControl.controls[i]
          ?.get('brandCity')
          ?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('brandCity')?.updateValueAndValidity();
      }
    } else if (this.PartnerType == 2) {
      this.corporateProfileForm.get('StreetAddress1')?.clearValidators();
      this.corporateProfileForm.get('StreetAddress1')?.updateValueAndValidity();
      this.corporateProfileForm.get('PinCode')?.clearValidators();
      this.corporateProfileForm.get('PinCode')?.updateValueAndValidity();
      this.corporateProfileForm.get('Brand')?.clearValidators();
      this.corporateProfileForm.get('Brand')?.updateValueAndValidity();
      var arrayControl = this.corporateProfileForm?.controls[
        'BrandLocationArray'
      ] as FormArray;
      for (let i = 0; i < arrayControl.value.length; i++) {
        arrayControl.controls[i]?.get('brandDeal')?.clearValidators();
        arrayControl.controls[i]?.get('brandDeal')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandState')?.clearValidators();
        arrayControl.controls[i]?.get('brandState')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandCity')?.clearValidators();
        arrayControl.controls[i]?.get('brandCity')?.updateValueAndValidity();
      }
    } else if(this.PartnerType == 3){
      this.corporateProfileForm.get('StreetAddress1')?.setValidators(Validators.required);
      this.corporateProfileForm.get('StreetAddress1')?.updateValueAndValidity();
      this.corporateProfileForm.get('PinCode')?.setValidators(Validators.required);
      this.corporateProfileForm.get('PinCode')?.updateValueAndValidity();
      this.corporateProfileForm.get('Brand')?.clearValidators();
      this.corporateProfileForm.get('Brand')?.updateValueAndValidity();
      var arrayControl = this.corporateProfileForm?.controls[
        'BrandLocationArray'
      ] as FormArray;
      for (let i = 0; i < arrayControl.value.length; i++) {
        arrayControl.controls[i]
          ?.get('brandDeal')
          ?.clearValidators();
        arrayControl.controls[i]?.get('brandDeal')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandState')?.clearValidators();
        arrayControl.controls[i]?.get('brandState')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandCity')?.clearValidators();
        arrayControl.controls[i]?.get('brandCity')?.updateValueAndValidity();
      }
    }else {
      this.corporateProfileForm.get('StreetAddress1')?.clearValidators();
      this.corporateProfileForm.get('StreetAddress1')?.updateValueAndValidity();
      this.corporateProfileForm.get('PinCode')?.clearValidators();
      this.corporateProfileForm.get('PinCode')?.updateValueAndValidity();
      this.corporateProfileForm.get('Brand')?.clearValidators();
      this.corporateProfileForm.get('Brand')?.updateValueAndValidity();
      var arrayControl = this.corporateProfileForm?.controls[
        'BrandLocationArray'
      ] as FormArray;
      for (let i = 0; i < arrayControl.value.length; i++) {
        arrayControl.controls[i]
          ?.get('brandDeal')
          ?.setValidators(Validators.required);
        arrayControl.controls[i]?.get('brandDeal')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandState')?.clearValidators();
        arrayControl.controls[i]?.get('brandState')?.updateValueAndValidity();
        arrayControl.controls[i]?.get('brandCity')?.clearValidators();
        arrayControl.controls[i]?.get('brandCity')?.updateValueAndValidity();
      }
    }
  }

  toggleSubAllSelection(i,value) {
    this.isCheckArray[i] = [];
    let taskListArrays = this.corporateProfileForm.get('BrandLocationArray') as FormArray;
    if (value.checked) {
      taskListArrays.controls[i].patchValue({"brandCity":this.getCriteria(this.corporateProfileForm.controls['BrandLocationArray']?.value[i]?.brandState) ? this.getCriteria(this.corporateProfileForm.controls['BrandLocationArray']?.value[i]?.brandState).map(item => item.id) : ""});
      this.isCheckArray[i] = true;
      console.log(this.isCheckArray)

    } else {
      taskListArrays.controls[i].patchValue({"brandCity": []});
    }
  }

  toggleEverySelection(l:any):any{ 

    this.isCheckArray[l] = [];
   if(this.corporateProfileForm.controls['BrandLocationArray']?.value[l].brandCity.length == this.cityListData.length) {
    this.isCheckArray[l] = true;
    console.log("true",this.isCheckArray)
   } else if(this.corporateProfileForm.controls['BrandLocationArray']?.value[l].brandCity.length != this.cityListData.length) {
    this.isCheckArray[l] = false
    console.log("false",this.isCheckArray)
   } else {}
 }

 checkDuplicateEnteries() {
  this.BrandLocationsArray = <FormArray>(
    this.corporateProfileForm.controls['BrandLocationArray']
  );
  var arrayControl = this.corporateProfileForm?.controls[
    'BrandLocationArray'
  ] as FormArray;
  if( this.BrandLocationsArray.value.length >1)
    this.BrandLocationsArray.value.forEach((data,index) => {
      if(index != 0 && data.brandDeal == this.BrandLocationsArray.value[0].brandDeal && this.BrandLocationsArray.value[0].brandState == data.brandState) {
        this.notify.error("duplicate enteries encountered in brand location");
        arrayControl.controls[index]?.get('brandDeal')?.setValue('');
        arrayControl.controls[index]?.get('brandName')?.setValue('');
        arrayControl.controls[index]?.get('brandState')?.setValue('');
        arrayControl.controls[index]?.get('brandCity')?.setValue('');
      }
    });
     
}
}
