import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, Renderer2  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { OtpSubmitRequest, OtpSubmitResponse } from 'src/app/models/common/otpcheck.model';
import { OtpSentRequest, OtpSentResponse } from 'src/app/models/common/otpsent.model';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { CountryResponse, ResponseData } from 'src/app/models/common/responsedata.model';
import { AccountService } from 'src/app/services/account';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ageCheck, MustMatch } from 'src/app/utility/validations/must-match-validation';
import { environment } from 'src/environments/environment';
import { TermsAndConditionsComponent } from '../../signup/terms-and-conditions/terms-and-conditions.component';
import { v4 as uuidv4 } from 'uuid';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { bool } from 'aws-sdk/clients/signer';
import { PartnerDealerBasicInfo, PartnerDealerContactEntity } from 'src/app/models/partner-dealer-management/partner-dealer-basic-details-data.model';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { CookieService } from 'ngx-cookie-service';

import { BehaviorSubject, Observable, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from 'src/app/services/loader.service';
// import { SignInProp } from '../../signin/signin-prop.service.component';

@Component({
  selector: 'app-partner-dealer-registration',
  templateUrl: './partner-dealer-registration.component.html',
  styleUrls: ['./partner-dealer-registration.component.css']
})

export class PartnerDealerRegistrationComponent implements OnInit {
  // private _oUserSubject: BehaviorSubject<SignInProp> = new BehaviorSubject<SignInProp>(new SignInProp());  

  private CookieValue: string = '';
  frmRegistrationType: any = this.formbuilder.group({
    becomeSouthPartner: new FormControl(''),
    tearmAndConditions: new FormControl('', Validators.requiredTrue)
  });
  basicinfoform: FormGroup = this.fb.group({
    mobile: new FormControl('', [
      Validators.required,
      //Validators.minLength(10),
      //Validators.maxLength(14),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/),
    ]),
    confirmpassword: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(256)]),
    lastname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(256)]),
    dob: new FormControl('', [Validators.required]),
    pincode: new FormControl('', [
      Validators.required,
      Validators.pattern(/[1-9]{1}[0-9]{5}$/),
      Validators.minLength(6),
      Validators.maxLength(6),
    ]),
    Email: new FormControl('', [Validators.email]),
    // agree: new FormControl('', [Validators.required]),
  },
    {
      validator: [MustMatch('password', 'confirmpassword'), ageCheck('dob')]
    });

  @ViewChild('otpField2') otp2?: ElementRef;
  @ViewChild('otpField3') otp3?: ElementRef;
  @ViewChild('otpField4') otp4?: ElementRef;
  @ViewChild('otpField5') otp5?: ElementRef;
  @ViewChild('otpField6') otp6?: ElementRef;
  @ViewChild('otpField1') otp1?: ElementRef;

  isSubmitted: boolean = false;
  maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  statuscodevar?: any = '';
  isGetOtpbtnHidden: boolean = false;
  isCheckedTermsCondition: boolean = false;
  isBecomeSrcPartnerWithiQuippo: boolean = false;
  selectedFormType: string = 'registrationType'
  registrationTypes: any;
  otpResponse?: OtpSentResponse
  otpSubmitResponse?: OtpSubmitResponse;
  isSubmittedPhone: boolean = false;
  signupSocial: boolean = false;
  resendOTP: boolean = false;
  remainingTime: any;
  googleResponse: any;
  response: any;
  countryList?: any;
  selectedCity?: CountryResponse;
  firstName?: string;
  lastName?: string;
  minDate: any;
  fullName?: string;
  otpcodevar?: any = '';
  otpstatus: boolean = true;
  registrationTypeID: number = 1;
  registrationTypeName: string = "";
  registrationTypeValue: string = "";
  partnerGroupName: string = this.app.partnerGroupName;
  socialId?: string;
  newDOB: any;
  pinCodeErrorFlag = false;
  pinCodeResponse?: PincodeResponse;
  message?: string;
  isProfile: boolean = false;
  pincodeBlur: boolean = false;
  phone: string = '';
  prefixCode: string = '';
  countryCode: string = '';
  userPinCode: number = 0;
  userDetails: any;
  cognitoId: any;
  partnership_type: number = 1;


  constructor(private cookieService: CookieService, private formbuilder: FormBuilder, private notify: NotificationService, private appRouteEnum: AppRouteEnum, public dialog: MatDialog,
    private fb: FormBuilder, public sharedService: SharedService,private renderer: Renderer2,
    private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, public accountService: AccountService, private notificationservice: NotificationService,
    private router: Router, private storage: StorageDataService, private awsConfig: AwsAuthService, private datePipe: DatePipe, public app: ApiRouteService, private partnerDealerRegService: PartnerDealerRegistrationService,private loaderService: LoaderService) {
    this.otpResponse = new OtpSentResponse();
    this.registrationTypes = this.app.registrationTypes;
    this.getRegistrationType();
  }


  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
    });
    // var isLoggedIn = this.sharedService.getCookie(this.appRouteEnum.globalCookieName);
    // if (isLoggedIn != null && isLoggedIn != '' && isLoggedIn != undefined && isLoggedIn == this.appRouteEnum.globalCookieValue) {
    //   window.location.href = environment.mp1HomePage;
    // }
    if (this.storage.getStorageData('userData', true) != null) {
      this.userDetails = this.storage.getStorageData('userData', true);
    }
    // if (this.storage.getStorageData('partnerDealerUserId', true) != null) {
    //   this.userId = this.storage.getStorageData('partnerDealerUserId', true).replace(/['"]+/g, '');
    // }
    if (this.storage.getStorageData('partnerDealerUserId', true) != null) {
      this.partnership_type = Number(this.storage.getStorageData('registrationTypeID', true));
    }
    if (this.storage.getStorageData('cognitoId', false) != null) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.getAllPartnerInfo();
    }
    this.bindFlags();

     
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
  }
  getAllPartnerInfo() {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    //  let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    this.partnerDealerRegService.getPartnerAllInfo(this.cognitoId).subscribe((res: any) => {
      console.log(res);
      if (res?.mapping?.status == 1 && res?.partner_contact_entity?.status==1) {
        this.selectedFormType = "registrationUnderProcess";
      }
      else if(res?.mapping?.status == 1 && res?.partner_contact_entity?.status==2) {
        this.storage.cleanLocalStorageAll();
        this.notify.warn('Please Re-Login first to continue.',true);
        
        this.router.navigate(['login'], { queryParams: { returnUrl: this.router.routerState.snapshot.url }});
      }
      else if(res?.mapping?.status == 2 && res?.partner_contact_entity?.status==2) {
        this.router.navigate([`./home`]);
      }
      else if (res?.partner_contact_entity?.id != undefined) {
        this.storage.clearStorageData("registrationTypeID")
        this.storage.setStorageData("registrationTypeID", res?.partner_contact_entity?.partnership_type, true);
        this.storage.clearStorageData("partnerDealerUserId")
        this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
        this.storage.setStorageData('partnerAllDetail', res, true)

        this.router.navigate([`/customer/dashboard/partner-dealer/basic-details`]);
      }

    }, err => {
      console.log(err);
    })
  }
  selectRegistrationType(event: any, registrationTypeID: number) {
    const elements: Element[] = Array.from(event.currentTarget.parentElement.children);
    elements.forEach((el: Element) => {
      el.classList.remove("active");
    });
    event.currentTarget.classList.add("active");
    this.registrationTypeID = registrationTypeID;
    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })
    // Scroll 100px to the right
    this.getRegistrationType();
  }

  getRegistrationType() {
    this.registrationTypeName = this.registrationTypes.filter((x) => x.id == this.registrationTypeID)[0].name;
    this.registrationTypeValue = this.registrationTypes.filter((x) => x.id == this.registrationTypeID)[0].value;
  }

  moveNext(eventName: any, type: string) {
    this.storage.clearStorageData("registrationTypeID")
    this.storage.setStorageData("registrationTypeID", this.registrationTypeID, true);
    if (type == 'RegistrationType') {
      if (this.frmRegistrationType.valid) {
        this.getRegistrationType();
        if (this.frmRegistrationType?.get('becomeSouthPartner')?.value) {
          this.isBecomeSrcPartnerWithiQuippo = true;
        }
        // add logic here for already logged in registration
        if (this.userDetails != undefined) {
          this.storage.clearStorageData("partnerDealerUserId")
          this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
          this.router.navigate([`./customer/dashboard/partner-dealer/basic-details`]);
        }
        else
          this.selectedFormType = 'registrationInfo'
      }
    }
    else if (type == 'RegistrationInfo') {
      if (eventName === 'continueRegInfo') {
        this.selectedFormType = 'registrationDetails'
      }
    }
  }

  openTermAndConditions() {
    const dialogRef = this.dialog.open(TermsAndConditionsComponent, {
      width: '650px',
      // data: {
      //   message: 'Do you want to use your PAN Name?',
      // },
    });
    dialogRef.afterClosed().subscribe((result) => { })
  }

  getotpForm = this.fb.group({
    phone: new FormControl('', [Validators.required /*, Validators.minLength(10), Validators.maxLength(10)*/]),
    countrycode: new FormControl(),
  }
    , { validator: [numberonly('phone')] }
  );

  checkPhoneField() {
    var prefixCode = this.getotpForm.get('countrycode')?.value.countryCode;
    var mobile = this.getotpForm.get('phone')?.value;
    if (prefixCode != null && prefixCode == 'IN') {
      if (mobile.length >= 10 && mobile.length <= 14) {
        return true;
      }
      else {
        this.getotpForm.controls['phone']?.setErrors({ 'incorrect': true });
        return false;
      }
    }
    else {
      return true;
    }
  }
  getotpfn() {
    //this.spinner.show();
    let OtpSentRequestModel = new OtpSentRequest();
    OtpSentRequestModel.phoneNumber = this.getotpForm.get('countrycode')?.value.prefixCode + this.getotpForm.get('phone')?.value;  
      //OtpSentRequestModel.phoneNumber = this.getotpForm.get('phone')?.value;
    this.accountService.otpValidator(OtpSentRequestModel).subscribe(Response => {
      //  console.log(Response)

      this.isSubmittedPhone = true;
      var getResponse = Response as ResponseData;
      var getResponseBody = getResponse.body;
      var getResponseBodyJSON = JSON.parse(getResponseBody);
      this.otpResponse = new OtpSentResponse();
      this.otpResponse.phoneNumber = OtpSentRequestModel.phoneNumber;
      this.otpResponse.sessionId = getResponseBodyJSON.session;

      this.statuscodevar = getResponse.statusCode;
      this.resendOTP = false;
      this.remainingTime = environment.remainingTime;
      this.startCountdown(environment.resendOtpTime);
      this.isGetOtpbtnHidden = true;
       
      // if(this.statuscodevar=='229'){
      //   alert();
      // };
    });
  }
  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {

      var countries = response as any;
      this.countryList = countries.body;
      this.selectedCity = this.countryList[0];
      this.getotpForm.get('countrycode')?.setValue(this.countryList[0]);
      //  this.getotpForm.get('countrycode')?.setValue(this.countryList[0]);
    }, err => {
      this.notificationservice.error(err);
    })
  }
  startCountdown(seconds: number) {
    let counter = seconds;
    let minutes, sec, finalTime;
    // let elem = document.getElementById('resend');

    const interval = setInterval(() => {
      // console.log(counter);
      counter--;

      if (counter < 0) {
        clearInterval(interval);
        this.resendOTP = true;
        // console.log(this.resendOTP);
      }

      minutes = Math.floor(counter / 60);
      sec = counter - minutes * 60;
      finalTime = this.str_pad_left(minutes, '0', 2) + ':' + this.str_pad_left(sec, '0', 2);
      this.remainingTime = finalTime;

    }, 1000);
  }

  str_pad_left(str: number, pad: string, length: number) {
    return (new Array(length + 1).join(pad) + str).slice(-length);
  }

  resentOtp() {
    //this.spinner.show();
    let OtpSentRequestModel = new OtpSentRequest();
    OtpSentRequestModel.phoneNumber = this.getotpForm.get('phone')?.value;
    this.accountService.otpValidator(OtpSentRequestModel).subscribe(Response => {
      // console.log(Response)

      this.resendOTP = false;
      this.remainingTime = environment.remainingTime;
      this.startCountdown(environment.resendOtpTime);
       
    });
  }

  submitOtp = this.fb.group({
    otpField1: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    otpField2: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    otpField3: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    otpField4: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    otpField5: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    otpField6: new FormControl('', [Validators.required, Validators.maxLength(1), Validators.minLength(1)]),
    agree: new FormControl('', [Validators.required]),
  }
    , {
      validator: [numberonly('otpField1'), numberonly('otpField2'), numberonly('otpField3'), numberonly('otpField4'), numberonly('otpField5'), numberonly('otpField6')]
    });

  submitotpfn() {
    //this.spinner.show();
    // this.isSubmitted = true;
    let otpSubmitRequestModel = new OtpSubmitRequest();
    otpSubmitRequestModel.phoneNumber = this.otpResponse?.phoneNumber;
    otpSubmitRequestModel.sessionId = this.otpResponse?.sessionId;
    otpSubmitRequestModel.otp = this.submitOtp.get('otpField1')?.value + '' + this.submitOtp.get('otpField2')?.value + '' + '' + this.submitOtp.get('otpField3')?.value + '' + '' + this.submitOtp.get('otpField4')?.value + '' + '' + this.submitOtp.get('otpField5')?.value + '' + '' + this.submitOtp.get('otpField6')?.value;

    this.accountService.otpValidator(otpSubmitRequestModel).subscribe((Response: any) => {
      // console.log(Response)
       
      let otpresponsecode = Response as ResponseData;
      var getResponseBody = otpresponsecode.body;
      var getResponseBodyJSON = JSON.parse(getResponseBody);
      this.otpcodevar = otpresponsecode.statusCode;

      if (otpresponsecode.statusCode == 220) {

        //  let obj: SignInProp = new SignInProp();
        //   obj.UserID = this.response;
        //   obj.LoginTime = 0;

        //   this._oUserSubject = new BehaviorSubject<SignInProp>(obj);

        //   this._oUserSubject.next(obj);

        // start - saving UID to cookie

        /*var d = new Date();
        d.setTime(d.getTime() + environment.ExpiryTimeInMS);
        
        this.cookieService.set(
          "userData",
          this.response,
          d
        ); */
        //this.CookieValue = this.cookieService.get("UID");
        //alert(this.CookieValue);

        // End - saving UID to cookie

        // this.storage.clearStorageData("userData");
        // this.storage.setStorageData("userData", Response, true);
        // this.storage.clearStorageData("cognitoId");
        //     this.storage.setStorageData('cognitoId', Response.username,false);

        if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
          var pro = this.sharedService.queryParams.aoc;
          var src = this.sharedService.queryParams.src;

          this.phone = this.getotpForm.get('phone')?.value;
          this.prefixCode = this.getotpForm.get('countrycode')?.value.prefixCode;
          this.countryCode = this.getotpForm.get('countrycode')?.value.countryCode;
          if (this.phone != null && this.phone != undefined)
            this.basicinfoform.get('mobile')?.setValue(this.prefixCode + this.phone);
          this.storage.setStorageData("registrationTypeID", this.registrationTypeID, true);
          this.storage.setStorageData("isBecomeSrcPartnerWithiQuippo", this.isBecomeSrcPartnerWithiQuippo, true)
          this.selectedFormType = 'registrationDetails'

          // if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
          //   this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
          // }
          // else if (pro != undefined && pro != null && pro == "qtt") {
          //   this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
          // }
          // else if (src != undefined && src != null) {
          //   this.router.navigate([`./` + this.appRouteEnum.BasicInformation], { queryParams: { src: this.sharedService.queryParams.src } });
          // }
          // else {
          //   this.router.navigate([`./` + this.appRouteEnum.BasicInformation]);
          // }
        } else {
          this.selectedFormType = 'registrationDetails'
          //this.router.navigate([`./` + this.appRouteEnum.BasicInformation]);
        }
      }
      if (otpresponsecode.statusCode == 260) {
        this.otpstatus = !this.otpstatus;
         
      };
      if (otpresponsecode.statusCode == 213) {
        this.otpstatus = !this.otpstatus;
         

      };
      if (otpresponsecode.statusCode == 500) {
        this.otpstatus = !this.otpstatus;
         

      };
    });

  }
  redirecttologin() {
    this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { partner: 'true' } });
  }
  myfun(val: string) {
    // console.log(val);
    // val.
    // @ts-ignore
    // val.nextSibling.focus();
    this.isSubmitted = false;
    switch (val) {
      case '1':
        if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '2':
        if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '3':
        if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        else {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        break;
      case '4':
        if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        else {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        break;
      case '5':
        if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
          this.otp6?.nativeElement.focus();
          this.otp6?.nativeElement.select();
        }
        else {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        break;
      case '6':
        if (this.submitOtp.get('otpField6')?.value != '' && this.submitOtp.get('otpField6')?.value != null && this.submitOtp.get('otpField6')?.value != undefined) {
          this.otp6?.nativeElement.focus();
        }
        else {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        break;
      case '11':
        if (this.submitOtp.get('otpField1')?.value != '' && this.submitOtp.get('otpField1')?.value != null && this.submitOtp.get('otpField1')?.value != undefined) {
          this.otp1?.nativeElement.select();
          this.otp1?.nativeElement.focus();
        }
        break;
      case '22':
        if (this.submitOtp.get('otpField2')?.value != '' && this.submitOtp.get('otpField2')?.value != null && this.submitOtp.get('otpField2')?.value != undefined) {
          this.otp2?.nativeElement.select();
          this.otp2?.nativeElement.focus();
        }
        break;
      case '33':
        if (this.submitOtp.get('otpField3')?.value != '' && this.submitOtp.get('otpField3')?.value != null && this.submitOtp.get('otpField3')?.value != undefined) {
          this.otp3?.nativeElement.select();
          this.otp3?.nativeElement.focus();
        }
        break;
      case '44':
        if (this.submitOtp.get('otpField4')?.value != '' && this.submitOtp.get('otpField4')?.value != null && this.submitOtp.get('otpField4')?.value != undefined) {
          this.otp4?.nativeElement.select();
          this.otp4?.nativeElement.focus();
        }
        break;
      case '55':
        if (this.submitOtp.get('otpField5')?.value != '' && this.submitOtp.get('otpField5')?.value != null && this.submitOtp.get('otpField5')?.value != undefined) {
          this.otp5?.nativeElement.select();
          this.otp5?.nativeElement.focus();
        }
        break;

      default:
        break;
    }

  }
  onTermConditionChange($event: any) {
    this.isCheckedTermsCondition = $event.checked;
    if (!this.isCheckedTermsCondition)
      this.submitOtp.get('agree')?.setErrors({ isAgree: true });
  }
  termsConditions() {
    const dialogRef = this.dialog.open(TermsAndConditionsComponent, {
      width: '900px',
    });
  }



  creatAccount() {
    //this.spinner.show();
    
    this.basicinfoform.markAllAsTouched();
    if(this.basicinfoform?.invalid){
      return;
    }
   
    var dobdetail = this.checkDobField();

    if (!dobdetail) {
       
      return;
    }
    var emailField = this.checkEmailField();
    if (!emailField) {
       
      return;
    }
    this.isSubmitted = true;
    this.loaderService.isLoading.next(true);
    // if (this.basicinfoform?.get('agree')?.value == false) {
    //   this.spinner.hide();
    //   return;
    // }
    if (this.prefixCode == null || this.prefixCode == undefined) {
      this.prefixCode = '';
    }

    if (this.countryCode == null || this.countryCode == undefined) {
      this.countryCode = '';
    }

    this.sharedService.isShow = true;
    var username = uuidv4();
    this.socialId = '';

    var dob = this.basicinfoform.get('dob')?.value;
    this.newDOB = this.datePipe.transform(dob, 'yyyy-MM-dd');

    var a = this.awsConfig
      .signUp(
        username,
        this.basicinfoform.get('password')?.value,
        this.basicinfoform.get('Email')?.value,
        this.basicinfoform.get('mobile')?.value,
        this.basicinfoform.get('lastname')?.value,
        this.basicinfoform.get('firstname')?.value,
        this.newDOB,
        this.basicinfoform.get('pincode')?.value,
        this.socialId,
        this.prefixCode,
        this.countryCode,
        this.partnerGroupName
      )
      .then((res) => {
        this.response = res as any;
        // console.log(this.response.signInUserSession.idToken.jwtToken);
        if (this.response.userConfirmed) {
          this.storage.clearStorageData('username');
          this.storage.setStorageData("username", this.response.user.username, true);
          this.storage.clearStorageData('cognitoId');
          this.storage.setStorageData("cognitoId", this.response.user.username, true);
          // this.storage.setStorageData("basicData",this.response.user.username,true);

          // create request for user basic profile
          let basic_info = new PartnerDealerBasicInfo();
          basic_info.cognito_id = this.response.user.username;
          basic_info.first_name = this.basicinfoform.get('firstname')?.value;
          basic_info.last_name = this.basicinfoform.get('lastname')?.value;
          basic_info.dob = this.newDOB;
          basic_info.mobile_number = this.basicinfoform.get('mobile')?.value.slice(this.basicinfoform.get('mobile')?.value.length - 10);
          basic_info.email = this.basicinfoform.get('Email')?.value;
          basic_info.gender = 1;
          basic_info.pin_code = this.basicinfoform.get('pincode')?.value;
          basic_info.kyc_verified = 1;
          basic_info.groups=[5];
          setTimeout(() => {
            this.signin(basic_info);
          }, 2000);

          //this.router.navigate([`./dashboard`]);
        }
      })
      .catch((err) => {

        console.log(err);
        this.isSubmitted = false;
        this.message = err.message;
        this.loaderService.isLoading.next(false);
      });
    //this.router.navigate([`./basicinformation`]);
  }

  
  checkEmailField() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
      var pro = this.sharedService.queryParams.aoc;
      if (pro != undefined && pro != null && pro == "qtt") {
        let email = this.basicinfoform?.get('Email')?.value;
        if (!email) {
          this.basicinfoform.get('Email')?.setErrors({ required: true });
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }
  }
  checkDobField() {
    let dob = this.basicinfoform?.get('dob')?.value;
    if (!dob) {
      this.basicinfoform.get('dob')?.setErrors({ required: true });
      return false;
    }
    else {
      return true;
    }
  }
  consoleitem(item: any) {
    //console.log(item);
  }
  // pincode resp
  // success code- 258,
  // pin not found errorcode- 259
  // other error- 5xx

  getpinCodefn() {

    if (this.basicinfoform.get('pincode')?.value != undefined && this.basicinfoform.get('pincode')?.value != null && this.basicinfoform.get('pincode')?.value != "") {
      this.getSearchedLocation("search=" + this.basicinfoform.get('pincode')?.value, 0)
    }

    // if(localStorage.getItem('countryCode')!="IN")
    // {
    //   this.pinCodeErrorFlag= false;
    //   this.pincodeBlur=false;
    //   if(this.basicinfoform.get('pincode')?.value!="" && this.basicinfoform.get('pincode')?.value !=undefined && this.basicinfoform.get('pincode')?.value!=null)
    //   this.basicinfoform.get('pincode')?.setErrors(null);
    //   return;
    // }


  }
  signin(basic_info: any) {
    this.awsConfig
      .awsLoginWithPassword(
        this.basicinfoform.get('mobile')?.value,
        this.basicinfoform.get('password')?.value
      )
      .then((res) => {
        this.isSubmitted = true;
        this.response = res as any;
        // console.log(this.response.signInUserSession.idToken.jwtToken);
        if (
          this.response.signInUserSession.idToken.jwtToken != null &&
          this.response.signInUserSession.idToken.jwtToken != ''
        ) {
          // console.log(this.response);
          this.storage.clearStorageData("userData");
          this.storage.setStorageData("userData", this.response, true);
          this.storage.clearStorageData("cognitoId");
          this.storage.setStorageData('cognitoId', this.response.username, false);
          this.storage.clearStorageData("userData");
          this.storage.setStorageData("userData", this.response, true);
          this.storage.setStorageData('sessionTime', this.response?.signInUserSession?.idToken?.payload?.exp, false);
          this.storage.setStorageData('sessionId', this.response?.signInUserSession?.idToken?.jwtToken, false);
          var roleArray = this.response?.signInUserSession?.idToken?.payload["cognito:groups"];
          if (roleArray != undefined && roleArray != null && roleArray.length > 0) {
            this.storage.clearStorageData('rolesArray');
            this.storage.setStorageData('rolesArray', roleArray, false);
          }
          this.loaderService.isLoading.next(false);
          this.partnerDealerRegService.createUpdatePartnerDealerDetails(basic_info, this.app.partnerDealerBasicProfile, 'Save', "").subscribe(
            res => {
              if (res != null) {
                let basicDetailsResponse = res as any;
                this.storage.setStorageData('partnerDealerUserId', basicDetailsResponse.cognito_id, true)
                if(this.storage.getStorageData('registrationTypeID', true)!=null)
                this.partnership_type = Number(this.storage.getStorageData('registrationTypeID', true));
                let contactEntity = new PartnerDealerContactEntity();
                contactEntity.partnership_type = this.partnership_type;
                contactEntity.user = basicDetailsResponse.cognito_id;
                if (this.storage.getStorageData('isBecomeSrcPartnerWithiQuippo', true) != null) {
                  contactEntity.is_channel_partner = this.storage.getStorageData('isBecomeSrcPartnerWithiQuippo', true);
                }   
                this.partnerDealerRegService.createUpdatePartnerDealerDetails(contactEntity, this.app.partnerDealerContactEntity, "Save", basicDetailsResponse.id).subscribe(
                  res => {
                    if (res != null) {
                      this.router.navigate([`/customer/dashboard/partner-dealer/basic-details`]);
                    }
                  });
                // this.router.navigate([`./home`]);
                
              }
            },err=>{
              console.log(err);
              this.isSubmitted = false;
            }
            );
            
        }
      })
      .catch((err) => {
        this.message = err.message;
        this.isSubmitted = false;
        // console.log(err)
      });

    // let token=this.response.idToken.jwtToken;

    // if()
    //  this.message= "login Successful";
  }
  //for getting location master data
  getSearchedLocation(query: string, id: number) {

    this.pinCodeErrorFlag = true;

    this.partnerDealerRegService.getLocations(query, id).subscribe(
      (res: any) => {
        if (res != null && res.results != null && res.results.length > 0) {
          this.pinCodeErrorFlag = false;
          this.userPinCode = res.results[0].id;
        }
         
      });

  }

  
   
  
}
