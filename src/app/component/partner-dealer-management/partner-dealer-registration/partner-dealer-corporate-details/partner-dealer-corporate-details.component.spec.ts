import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PartnerDealerCorporateDetailsComponent } from './partner-dealer-corporate-details.component';


describe('PartnerDealerCorporateDetailsComponent', () => {
  let component: PartnerDealerCorporateDetailsComponent;
  let fixture: ComponentFixture<PartnerDealerCorporateDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerDealerCorporateDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDealerCorporateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
