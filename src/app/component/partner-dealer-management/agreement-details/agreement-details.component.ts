import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { environment } from 'src/environments/environment';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from 'src/app/services/shared-service.service';
import { MatPaginator } from '@angular/material/paginator';
import { AgreementDetailsPopupComponent } from './agreement-details-popup/agreement-details-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-agreement-details',
  templateUrl: './agreement-details.component.html',
  styleUrls: ['./agreement-details.component.css']
})
export class AgreementDetailsComponent implements OnInit {
  form?: FormGroup;
  multipleAgreementRef?: FormArray;
  dataSource1 = new MatTableDataSource<[]>();
  res: any;
  displayedColumns = ["id","start_date", "end_date","seller_mobile_number","seller_email", "agreement_letter", "status", "Action"];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('agreement') agreement!: ElementRef;
  @ViewChild('work_order') work_order!: ElementRef;
  // @ViewChild('fileInput') fileInput!: ElementRef;
  agreementStatus: any;
  // agreementTemplateUrl: string = '';
  agreementTemplateUrl: string = 'https://iquippodocuments.s3.ap-south-1.amazonaws.com/agreement.pdf';
  cognitoId: any;
  userRole: any;
  isNew: string = "true";
  partnerAllDetail: any;
  rolesArray: any;
  constructor(private fb: FormBuilder,private commonService:CommonService,private route: ActivatedRoute, private partnerDealerRegistrationService: PartnerDealerRegistrationService, private datePipe: DatePipe, private s3: S3UploadDownloadService, private notify: NotificationService,
     public agreementService: AgreementServiceService, public sharedService: SharedService, private dialog: MatDialog,private appRouteEnum: AppRouteEnum,
      public app: ApiRouteService, public spinner: NgxSpinnerService, private storage: StorageDataService, private router: Router) {
    this.form = this.fb.group({
      start_date: new FormControl(this.datePipe.transform(new Date(), 'yyyy-MM-dd'), [Validators.required]),
      end_date: new FormControl(this.datePipe.transform(new Date().setFullYear(new Date().getFullYear() + 1), 'yyyy-MM-dd'), [Validators.required]),
      agreement_letter: new FormControl('', [Validators.required]),
      commission_value: new FormControl(5000),
      enableCost: new FormControl(false),
      is_fixed_commission: new FormControl(false),
      status: new FormControl(1),
      partner_entity: new FormControl(1),
      multipleAgreement: new FormArray([
        new FormGroup({
          work_order: new FormControl('', []),
        })
      ])
    })
    this.agreementStatus = app.agreementStatus;
  }
  addAgreement() {
    this.multipleAgreementRef?.push(
      new FormGroup({
        work_order: new FormControl('', [Validators.required]),
      })
    );
  }
  finish(){
  if(this.isNew=="false")
  {
    this.router.navigate([`./used-equipment/sell-equipment`]);
  }
  else if(this.isNew=="true")
    this.router.navigate([`./new-equipment/sell-equipment`]);
  else
  this.router.navigate([`./`+this.appRouteEnum.partnerRegistration]);
  }
  removeAgreement(i: number) {
    this.multipleAgreementRef?.removeAt(i);
  }
  createUUID() {
    return uuidv4();
  }

  async handleUpload(event: any, name: string) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + uniqueFileName + '/' + name + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //console.log('type', file.type);
        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');
      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});

    }
  }
  deleteImg(url:string,index?:number) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        console.log(data);
        if(index!=undefined){
          this.form?.get('multipleAgreement')?.get(''+index+'')?.get("work_order")?.setValue('');
          this.work_order.nativeElement.value = "";
        }
        else
        this.form?.get('agreement_letter')?.setValue('');
        this.agreement.nativeElement.value = "";
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }
  async handleWorkUpload(event: any, name: string, i: number) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + uniqueFileName + '/' + name + i + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get('multipleAgreement')?.get('' + i + '')?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //console.log('type', file.type);
        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');
      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});

    }
  }
  ngOnInit(): void {
    this.multipleAgreementRef = <FormArray>this.form?.controls['multipleAgreement'];
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    this.route.queryParams.subscribe((data) => {
      this.isNew = data.isNew;
      console.log(this.isNew);
    })
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    if(this.rolesArray)
    this.rolesArray = this.rolesArray.split(',');
  
    this.userRole = this.storage.getStorageData("userRole", false);
    // this.getAgreementTemplateUrl();
    this.getAllPartnerInfo();
  }
  ngAfterViewInit() {
    this.getAgreementDetails(this.cognitoId);
  }
  getAllPartnerInfo() {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    //  let queryparams = congnitoId+"?limit="+this.paginator.pageSize+"&page="+(this.paginator.pageIndex+1);
    this.partnerDealerRegistrationService.getPartnerAllInfo(this.cognitoId).subscribe((res: any) => {
      console.log(res);
        this.storage.setStorageData('partnerAllDetail', res, true)
        this.partnerAllDetail = res;
       
    }, err => {
      console.log(err);
    })
  }
  getAgreement() {
    this.getAgreementDetails(this.cognitoId);
  }
  onSubmit() {
     if (this.rolesArray) {
      if (this.rolesArray.includes('Customer') || this.rolesArray.includes('Dealer') || this.rolesArray.includes('Manufacturer') || this.rolesArray.includes('PartnerGuest')  ) {
        let role;
        if (this.rolesArray.includes('Dealer')) {
          role = 'Dealer';
        } 
        else if (this.rolesArray.includes('Manufacturer')) {
          role = 'Manufacturer';
        } 
        else if (this.rolesArray.includes('Customer')) {
          role = 'Customer';
        }
        else if (this.rolesArray.includes('PartnerGuest')) {
          role = 'PartnerGuest';
        }
        let payload = {
          cognito_id: this.cognitoId,
          role: role
        }
        this.commonService.validateSeller(payload).subscribe((res: any) => {
          if (!res.kyc_verified || !res.pan_address_proof) {
            this.notify.warn('Please submit KYC and Address proof', true);
            this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
            this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { createListing: true,isNew: false } });
            return;
          }
          if (!res.bank_details_exist) {
              this.notify.warn('Please submit your bank details', true);
              this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { createListing: true,isNew: false } });
              return;
          }
         
          else{
            //this.spinner.show();
    let payload = { ...this.form?.value };
    payload.work_order = [];
    if(payload.enableCost==true){
    payload.is_fixed_commission = true;
    payload.commission_value=5000;
    }
    else{
    payload.is_fixed_commission = false;
    payload.commission_value=5; 
   }
    payload.start_date = this.datePipe.transform(payload.start_date, 'yyyy-MM-dd');
    payload.end_date = this.datePipe.transform(payload.end_date, 'yyyy-MM-dd');
    // payload.multipleAgreement?.forEach(element => {
    //   payload.work_order.push(element.work_order);
    // });
    payload.type = this.userRole == 'Customer' ? 1 : 2;
    payload.user = this.cognitoId
    // });
    console.log(payload)
    this.agreementService.postAgreementDetails(payload).subscribe(async res => {
      console.log(res);
   
      if(this.res?.results.length==0){
      await this.updatePartnerEntityStatus();
     // await this.updatePartnerContactStatus();
      let partnerAllDetail = JSON.parse(this.storage.getStorageData("partnerAllDetail",false));
      await this.updateEntityMappingStatus(partnerAllDetail?.mapping?.id);
      }
      this.form?.reset();
      this.form?.get('enableCost')?.setValue(false);
      this.agreement.nativeElement.value = "";
      this.form?.get('status')?.setValue(1);
      this.form?.get('commission_value')?.setValue(5);
      this.getAgreementDetails(this.cognitoId);
       

    }, err => {
      console.log(err);
       
    })
          }
          
        
        });
  } 
      else {
        //  this.notify.warn('Please complete your profile first.',true);
        this.notify.warn('Please add any one address proof',true);
        this.router.navigate(['customer/dashboard/partner-dealer/basic-details']);
      }
    }
    
  }
  updatePartnerContactStatus(){
    
    
    let id = this.partnerAllDetail?.partner_contact_entity?.id
    
    this.partnerDealerRegistrationService.updatePartnerStatus({status:2},this.appRouteEnum.partnerContactEntity,id).subscribe(res=>{
      console.log(res)
    },err=>{
      console.log(err);
    })
  }
  updatePartnerEntityStatus(){
    let id:number=0;
    let partner_entity = JSON.parse(this.storage.getStorageData("partnerEntityCreate",false));
    let partner_entity_id = partner_entity?.id;
    if(partner_entity_id!=undefined && partner_entity_id!=null && partner_entity_id!=''){
    this.partnerDealerRegistrationService.patchPartnerStatus({status:1},this.appRouteEnum.partnerEntity,partner_entity_id).subscribe(res=>{
      console.log(res);
    },err=>{
      console.log(err)
    })
    }
    else{
      let partnerAllDetail = JSON.parse(this.storage.getStorageData("partnerAllDetail",false));
      this.partnerDealerRegistrationService.patchPartnerStatus({status:1},this.appRouteEnum.partnerEntity,partnerAllDetail?.mapping?.partner?.id).subscribe(res=>{
        console.log(res);
      },err=>{
        console.log(err)
      })
    }
  }
  updateEntityMappingStatus(mappingId: any) {
    var payloadEntityMapping: any = {};
    payloadEntityMapping.status = 1;
    // payloadEntityMapping.partner = entityId;
    // let partner_contact_id = localStorage.getItem("partnerDealerPartnerId");
    // payloadEntityMapping.partner_contact = partner_contact_id;
    payloadEntityMapping.id = mappingId;
    this.agreementService.patchPartnerEntityMappingPartnerFlow(payloadEntityMapping).subscribe((res: any) => {
      console.log(res);
      this.storage.setStorageData("entityMappingId", res.id, false);
       
    }, err => {
      console.log(err);
    })
  }
  actionPopup(agreementObj: any) {
    const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
      width: "60%",
      data: { agreementObj: agreementObj }
    }).afterClosed()
      .subscribe(data => {
        // if(data){
        //   console.log(data);
        //   var payload=data?.leadObj;
        //   if(data?.action==3){
        //     payload.response_to_customer= data?.response_to_customer;
        //     payload.enquiry_closure_date= data?.enquiry_closure_date;
        //     payload.status= data?.action;
        //   }
        //   if(data?.action==1 || data?.action==2){
        //     payload.in_progress_comment= data?.response_to_customer;
        //     if(data?.action==1){
        //       data.action = data?.action + 1
        //     }
        //     payload.status= data?.action;
        //   }
        //   else{
        //     payload.status = data?.action;
        //   }

        //   this.newEquipmentLeadService.saveStatus(payload,data.id);
        // }
      })
  }
  getAgreementDetails(congnitoId: string) {
    let queryparams = congnitoId + "?limit=" + this.paginator.pageSize + "&page=" + (this.paginator.pageIndex + 1) + "&ordering=" + (this.sort.direction == "asc" ? this.sort.active : "-" + this.sort.active);
    this.agreementService.getAgreementDetailsById(queryparams).subscribe(res => {
      console.log(res);
      this.res = res as any;
      this.dataSource1 = this.res?.results;
      console.log("ds", this.dataSource1);
    }, err => {
      console.log(err);
    })
  }
  // getAgreementTemplateUrl() {
  //   this.agreementService.getAgreemenTemplateUrl().subscribe((res: any) => {
  //     console.log(res);
  //     this.agreementTemplateUrl = res?.value;

  //   }, err => {
  //     console.log(err);
  //   })
  // }
  enableCost() {
    if (this.form?.get('enableCost')?.value) {
      this.form?.get('commission_value')?.setValue(5000)
    }
    else {
      this.form?.get('commission_value')?.setValue(5)
    }
  }

}
