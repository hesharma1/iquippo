import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Component({
  selector: 'app-agreement-details-popup',
  templateUrl: './agreement-details-popup.component.html',
  styleUrls: ['./agreement-details-popup.component.css']
})
export class AgreementDetailsPopupComponent implements OnInit {
  actionObj: any;
  agreementStatus: any;
  userRole: any;
  constructor(public dialogRef: MatDialogRef<AgreementDetailsPopupComponent>, @Inject(MAT_DIALOG_DATA) data: any, private fb: FormBuilder, public sharedService: SharedService, public app: ApiRouteService,private storage:StorageDataService) {
    this.actionObj = data;
    this.agreementStatus = app.agreementStatus;
    this.userRole = this.storage.getStorageData('userRole', false);
  }

  ngOnInit(): void {
  }
  checkEmptyObject(obj){
    if (obj==undefined || Object.entries(obj).length === 0) {
      return false;
    }
    return true;
  }
}
