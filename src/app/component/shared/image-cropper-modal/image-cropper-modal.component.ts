import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-cropper-modal',
  templateUrl: './image-cropper-modal.component.html',
  styleUrls: ['./image-cropper-modal.component.css']
})
export class ImageCropperModalComponent implements OnInit {

  public imageFile!: any;
  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public aspectRatio: any = 1/1;
  public originalImage:string = '';
  public showCropper = false;
  constructor(public dialogRef: MatDialogRef<ImageCropperModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.originalImage = this.data.image;
  }

  closeDialog() {
    this.originalImage = '';
    this.dialogRef.close({ event: 'close', data: this.base64ToFile(this.croppedImage) });
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log(this.croppedImage)
  }

  base64ToFile(base64Image: any): Blob {
    const split = base64Image.split(',');
    const type = split[0].replace('data:', '').replace(';base64', '');
    const byteString = atob(split[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i += 1) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type });
  }

}
