import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { AuctionService } from 'src/app/services/auction.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-customer-search-modal',
  templateUrl: './customer-search-modal.component.html',
  styleUrls: ['./customer-search-modal.component.css']
})

export class CustomerSearchModalComponent implements OnInit {
  actionForm?: FormGroup;
  sellerList: any = [];
  isOtherCustDetailAva: boolean = true;
  mainData: any;
  sellerCognitoId: any;
  isNotSelectedUser: boolean = true;
  firstName: string = '';
  lastName: string = '';
  mobileNo: string = '';
  email: string = '';
  constructor(
    public dialogRef: MatDialogRef<CustomerSearchModalComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    public notify: NotificationService,
    private usedEquipmentService: UsedEquipmentService,
    private auctionService: AuctionService,
    public router: Router
  ) {
    if (data) {
      this.mainData = data;
    }
    this.getActionForm();
  }

  ngOnInit(): void {

  }

  getActionForm() {
    this.actionForm = new FormGroup({
      isSelfInvoice: new FormControl('2'),
      firstName: new FormControl({ value: '', disabled: true }),
      lastName: new FormControl({ value: '', disabled: true }),
      email: new FormControl({ value: '', disabled: true }),
      mobileNo: new FormControl('', Validators.required)
    });
  }

  backClick() {
    this.dialogRef.close();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    this.sellerList = [];

    if (event.keyCode != 8 && !pattern.test(event.key)) {
      event.preventDefault();
    }
    else if (this.actionForm?.controls['mobileNo'].value.length == 0) {
      this.sellerList = [];
      this.actionForm?.get('email')?.setValue('');
      this.actionForm?.get('mobileNo')?.setValue('');
      this.actionForm?.get('firstName')?.setValue('');
      this.actionForm?.get('lastName')?.setValue('');
      this.firstName = '';
      this.lastName = '';
      this.mobileNo = '';
      this.email = '';
      this.isNotSelectedUser = true;
    }
    else if (this.actionForm?.controls['mobileNo'].value.length >= 9) {
      this.searchSeller(event);
    }
  }

  searchSeller(e) {
    let payload = {
      "mobile_number__icontains": e.target.value,
      "groups__name__in": "Customer",
      "is_active": 'true'
    }
    this.sellerList = [];

    this.usedEquipmentService.getInvoiceCustomer(payload).subscribe(
      (res: any) => {
        if (res?.results && res?.results.length) {
          this.sellerList = res.results;
          this.isOtherCustDetailAva = true;
        } else {
          this.sellerList = [];
          this.isOtherCustDetailAva = false;
        }
      },
      (error: any) => {
        if (error.message == "No such user exists with valid Agreement") {
          error.message == "This customer is not registered with us. Kindly ask him to register on our portal."
          this.isOtherCustDetailAva = false;
        }
        this.sellerList = [];

      }
    );
  }

  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      if (res.mobile_number == search) {
        this.selectSeller(res);
      }
    });
  }

  selectSeller(seller) {
    this.sellerCognitoId = seller.cognito_id;
    this.isNotSelectedUser = false;
    this.actionForm?.get('email')?.setValue(seller.email);
    this.actionForm?.get('mobileNo')?.setValue(seller.mobile_number);
    this.actionForm?.get('firstName')?.setValue(seller.first_name);
    this.actionForm?.get('lastName')?.setValue(seller.last_name);
    this.firstName = seller.first_name;
    this.lastName = seller.last_name;
    this.mobileNo = seller.mobile_number;
    this.email = seller.email;
  }

  onSubmit() {
    if (this.mainData.type == 'Auction') {
      this.dialogRef.close({ 'cognitoId': this.sellerCognitoId });
    }
  }

  CheckUserExistOrNot(event: any) {
    let value = event.value;
    if (value == "1") {
      this.dialogRef.close();
      //this.router.navigate(['/admin-dashboard/customer-management']);
      this.router.navigate(['/admin-dashboard/user-management-new']);

    }
  }
}