import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerSearchModalComponent } from './customer-search-modal.component';

describe('CustomerSearchModalComponent', () => {
  let component: CustomerSearchModalComponent;
  let fixture: ComponentFixture<CustomerSearchModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerSearchModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerSearchModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
