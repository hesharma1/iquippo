import { Component, OnInit } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css']
})
export class CompareComponent implements OnInit {

  constructor( private storage: StorageDataService, private router:Router) { }
  compareList: Array<any> = [];
  dynamicTechArray:Array<any> = [];
  type;
  techFields
  ngOnInit(): void {
    this.compareList = this.storage.getSessionStorageData('compareList',true);
    this.type = this.storage.getSessionStorageData('type',false);
    //this.getTechFieldsArray();
    console.log(this.compareList);
    this.getTechHeader();
    if(this.compareList &&  this.compareList?.length < 2){
      this.redirectToListing();
    }
  }
  getTechFieldValue(equipmentDetails,header){
    if(header == 'Gross Weight'){
      if(equipmentDetails.gross_Weight){
        return equipmentDetails.gross_Weight + ' kg';
      }else{
        return  'N/A'
      }
      
    }else if(header == 'Operating Weight'){
      if(equipmentDetails.operating_weight){
      return equipmentDetails.operating_weight +' kg '; 
    }else{
      return  'N/A'
    }
    }else if(header == 'Bucket Capacity'){
      if(equipmentDetails.bucket_capacity){
      return equipmentDetails.bucket_capacity +' kg';
    }else{
      return  'N/A'
    }
    }else if(header == 'Lifting Capacity'){
      if(equipmentDetails.lifting_capacity){
      return equipmentDetails.lifting_capacity+' kg ';
    }else{
      return  'N/A'
    }
    }else{
      for(let tech of equipmentDetails?.tech_fields){
        if(tech.field_name == header){
          if(tech.field_unit){
            return tech.field_value + ' ' + tech.field_unit; 
          }
          return tech.field_value;
        }
      }
      return 'N/A'
    }
  }
  delete(i){
    this.compareList.splice(i,1)
    this.storage.setSessionStorageData('compareList',this.compareList,true);
    if(this.compareList &&  this.compareList?.length < 2){
      this.redirectToListing();
    }
  }
  getTechTitle(str,unit) {
    if(str == 'enginePower') {
      return str + ' cc';
      } else if(str == 'operatingWeight') {
      return str + ' kg';
      }
      else if(str == 'bucketCapcity') {
        return str +' kg ';
      } else if(str == 'grossWeight') {
        return str +' kg ';
      }else if(str == 'liftingCapcity') {
        return str +' kg ';
      }else {
        return str + ' ' +unit ;
      }
  }

  getTechHeader() {
    this.compareList.forEach(obj1 => {
      if(obj1?.tech_fields){
        obj1.tech_fields.forEach(obj2 => {
          if(!this.dynamicTechArray.includes(obj2.field_name)){
            this.dynamicTechArray.push(obj2.field_name)
          }
          console.log(this.dynamicTechArray)
        });
      }
      if(obj1?.gross_Weight){
        if(!this.dynamicTechArray.includes('Gross Weight')){
          this.dynamicTechArray.push('Gross Weight')
        }
      }
      if(obj1?.operating_weight){
        if(!this.dynamicTechArray.includes('Operating Weight')){
          this.dynamicTechArray.push('Operating Weight')
        }
      }
      if(obj1?.bucket_capacity){
        if(!this.dynamicTechArray.includes('Bucket Capacity')){
          this.dynamicTechArray.push('Bucket Capacity')
        }
      }
      if(obj1?.lifting_capacity){
        if(!this.dynamicTechArray.includes('Lifting Capacity')){
          this.dynamicTechArray.push('Lifting Capacity')
        }
      }
    });
  }

 
  getEquipmentFuelType(val) {
    switch (val) {
      case 1: { return 'Diesel' }
      case 2: { return 'Petrol' }
      case 3: { return 'CNG' }
      default: { return 'Diesel' }
    }
  }
  redirectToListing(){
    if(this.type== 'new'){
      this.router.navigate(['/new-equipment-dashboard/equipment-list']);
    }else{
      this.router.navigate([`/used-equipment-dashboard/equipment-list`]);
    }
  }
  checkWidth(){
    return window.innerWidth > 768
  }
}
