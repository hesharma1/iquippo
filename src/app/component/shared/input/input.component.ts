import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FieldConfig } from "../../../shared/abstractions/field.interface";
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  public field: FieldConfig;
  public group: FormGroup;
  constructor() { }

  ngOnInit(): void {
  }

}
