import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { FieldConfig, Validator } from "../../../shared/abstractions/field.interface";

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  @Input() fields: FieldConfig[] = [];
  @Output() submit: EventEmitter<any> = new EventEmitter<any>();
  form!: FormGroup;

  ngOnInit(): void {
    this.createControl();
  }

  get value() {
    return this.form.value;
    }

  createControl() {
    let form = {};
    let abc = this.fields;
    console.log(JSON.stringify(abc))
    abc.forEach(
      field => (form = { ...form, [this.getName(field)]: new FormControl("") })
    );
    this.form = this.fb.group(form);
    console.log(this.fields);
  }

  getName(field:any){
    return field.name;
  }

  // bindValidations(validations: any) {
  //   if (validations.length > 0) {
  //   let validList = [];
  //   validations.forEach(valid => {
  //   validList.push(valid.validator);
  //   });
  //   return Validators.compose(validList);
  //   }
  //   return null;
  //   }

  onSubmit() {
      // event.preventDefault();
      // event.stopPropagation();
      if (this.form.valid) {
      this.submit.emit(this.form.value);
      } else {
      this.validateAllFormFields(this.form);
      }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if(control)
    control.markAsTouched({ onlySelf: true });
    });
    }

}
