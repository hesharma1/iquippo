import { Component, OnInit, Input } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-progress-steps',
  templateUrl: './progress-steps.component.html',
  styleUrls: ['./progress-steps.component.css']
})
export class ProgressStepsComponent implements OnInit {

  @Input() steps:any;
  public someWidth:any;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
  }

  ngOnChanges(){
    this.someWidth = this.sanitizer.bypassSecurityTrustStyle(`calc(100% / ${this.steps.total.length - 1})`);
  }

}
