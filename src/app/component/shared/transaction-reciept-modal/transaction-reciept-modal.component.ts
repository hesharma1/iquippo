import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-transaction-reciept-modal',
  templateUrl: './transaction-reciept-modal.component.html',
  styleUrls: ['./transaction-reciept-modal.component.css']
})
export class TransactionRecieptModalComponent implements OnInit {

  public transactionDetails:any;

  constructor(public dialogRef: MatDialogRef<TransactionRecieptModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.transactionDetails = this.data.transactionDetails;
  }

  backClick() {
    this.dialogRef.close();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realised'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getServiceType(val) {
    switch (val) {
      case 1: {
        return 'Auction'
        break;
      }
      case 2: {
        return 'Insta Sell'
        break;
      }
      case 3: {
        return 'Bid'
        break;
      }
      case 4: {
        return 'Lead'
        break;
      }
      case 5: {
        return 'Advanced Valuation'
        break;
      }
      case 6: {
        return 'Insta Valuation'
        break;
      }
      case 7: {
        return 'Auction EMD'
        break;
      }
      case 8: {
        return 'Auction Fulfillment'
        break;
      }
      case 9: {
        return 'Enterprise Valuation'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getFileName(str) {
    return str.slice(str.lastIndexOf("/") + 1);
  }

}
