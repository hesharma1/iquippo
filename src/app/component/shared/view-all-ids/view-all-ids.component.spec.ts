import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllIdsComponent } from './view-all-ids.component';

describe('ViewAllIdsComponent', () => {
  let component: ViewAllIdsComponent;
  let fixture: ComponentFixture<ViewAllIdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAllIdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllIdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
