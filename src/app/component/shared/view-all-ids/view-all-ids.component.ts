import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuctionService } from 'src/app/services/auction.service';

@Component({
  selector: 'app-view-all-ids',
  templateUrl: './view-all-ids.component.html',
  styleUrls: ['./view-all-ids.component.css']
})
export class ViewAllIdsComponent implements OnInit {

  idsData: any;
  ids: any;
  emdDetailsData: any[] = [];
  auctionLotsData: Array<any> = [];

  constructor(public dialogRef: MatDialogRef<ViewAllIdsComponent>, @Inject(MAT_DIALOG_DATA) data: any, public auctionService: AuctionService) {
    this.idsData = data;
  }

  ngOnInit(): void {
    if (this.idsData.isSubAuctionIds) {
      this.ids = this.idsData.ids.filter(x => x.auction.auction_engine_auction_id).map(x => x.auction.auction_engine_auction_id).join(",");
    } else if (this.idsData.isFromAuctionPaymentHistory) {
      this.ids = this.idsData.ids.filter(x => x.lots).map(x => x.lots).join(",");
    } else if (this.idsData.isUCN) {
      this.ids = this.idsData.ids.toString();
    } else if (this.idsData.isLotIds) {
      this.ids = this.idsData.ids.filter(x => x.lot.lot_number).map(x => x.lot.lot_number).join(",");
    } else if (this.idsData?.auctionLotIds) {
      this.ids = this.idsData.subAuctionIds;      
    }
  }

  close() {
    this.dialogRef.close();
  }
}
