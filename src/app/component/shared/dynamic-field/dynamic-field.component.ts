import {
  ComponentFactoryResolver, ComponentRef, Component, Input, OnInit, ViewContainerRef
  } from "@angular/core";
  import { FormGroup } from "@angular/forms";
  import { FieldConfig } from "../../../shared/abstractions/field.interface";
  import { InputComponent } from "../input/input.component";
  import { SelectComponent } from "../select/select.component";
  import { RadiobuttonComponent } from "../radiobutton/radiobutton.component";


@Component({
  selector: 'app-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.css']
})
export class DynamicFieldComponent implements OnInit {
  componentMapper:any;
    

  @Input() field:FieldConfig;
  @Input() group: FormGroup;

  componentRef: any;
  constructor(private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef) { }

  ngOnInit(): void {
    this.componentMapper={
      "input": InputComponent,
      "select": SelectComponent,
      "radiobutton": RadiobuttonComponent
    };
    const component = this.componentMapper[this.field.type];
    const factory = this.resolver.resolveComponentFactory <any>( 
      component
    );
    this.componentRef = this.container.createComponent(factory);
    this.componentRef.instance.field = this.field;
    this.componentRef.instance.group = this.group;
  }

}
