import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validator, FormControl, Validators } from '@angular/forms';
import { CheckMobile } from '../../utility/custom-validators/forgot-password';
import { AWSforgetOtpResponse, CountryResponse } from '../../models/common/responsedata.model';
import { OtpSentRequest } from 'src/app/models/common/otpsent.model';
import { AccountService } from 'src/app/services/account';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from "../../services/storage-data.service";
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  countryList?: any;
  selectedCity?: CountryResponse;
  maxLength = this.appRouteEnum.indiaPhoneNumber;
  awsOtpResp?: AWSforgetOtpResponse;

  constructor(private fb: FormBuilder, private accountService: AccountService, private awsConfig: AwsAuthService, private router: Router, private storageDataService: StorageDataService, private appRouteEnum: AppRouteEnum, private activatedRoute: ActivatedRoute, private sharedService: SharedService, public notificationservice: NotificationService, private spinner: NgxSpinnerService) { }
      
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
    });
    this.bindFlags();
  }

  forgotPassForm = this.fb.group({
    countryCode: ['', [Validators.required]],
    mobile: ['', [Validators.required]]
  }, 
  {
    validator: [CheckMobile('mobile')]
  })
  
  sendOTPFun() {
    this.forgotPassForm.markAllAsTouched();
    if (this.forgotPassForm.valid) {
      this.sendOTP();
    }
  }

  sendOTP() {    
    //this.spinner.show();
    let phoneNumber = this.forgotPassForm.get('countryCode')?.value.prefixCode + +this.forgotPassForm.get('mobile')?.value;
    this.storageDataService.setStorageData('mobile', phoneNumber, false);
    this.awsConfig.awsForgotPassword(phoneNumber)
      .then((resp) => {
        this.awsOtpResp = resp as AWSforgetOtpResponse;
        if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
          var pro = this.sharedService.queryParams.aoc;
          var src = this.sharedService.queryParams.src;
          if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
            this.router.navigate([`./` + this.appRouteEnum.PasswordResetCode], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
          }
          else if (pro != undefined && pro != null && pro == "qtt") {
            this.router.navigate([`./` + this.appRouteEnum.PasswordResetCode], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
          }
          else if (src != undefined && src != null) {
            this.router.navigate([`./` + this.appRouteEnum.PasswordResetCode], { queryParams: { src: this.sharedService.queryParams.src } });
          }
          else {
            this.router.navigate([`./` + this.appRouteEnum.PasswordResetCode]);
          }
        } else {
          this.router.navigate([`./` + this.appRouteEnum.PasswordResetCode]);
        }
      })
      .catch(err => {
        console.log("some error in getting otp");
         
      })
  }

  goToLogin() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
      var pro = this.sharedService.queryParams.aoc;
      var src = this.sharedService.queryParams.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.Login], { queryParams: { src: this.sharedService.queryParams.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.Login]);
      }
    } else {
      this.router.navigate([`./` + this.appRouteEnum.Login]);
    }
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.forgotPassForm.get('countryCode')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
    }, err => {
      this.notificationservice.error(err);
    })
  }

  updateCountryCode(city: any) {
    this.forgotPassForm.get('countryCode')?.setValue(city);
    this.forgotPassForm.get('mobile')?.setValue('')

    if (city?.prefixCode == '+91' && city?.countryCode == 'IN') {
      this.maxLength = this.appRouteEnum.indiaPhoneNumber;
    } else {
      this.maxLength = this.appRouteEnum.otherPhoneNumber
    }
  }
}
