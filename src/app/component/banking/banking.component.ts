import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { formContainer } from 'aws-amplify';
import { debug } from 'console';
import { NgxSpinnerService } from 'ngx-spinner';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Bankdetails, BankListData, SaveBankDetails } from 'src/app/shared/abstractions/user';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-banking',
  templateUrl: './banking.component.html',
  styleUrls: ['./banking.component.css']
})
export class BankingComponent implements OnInit {
  bankingForm?: FormGroup;
  SaveBankDetails: SaveBankDetails;
  BankList: BankListData;
  DeletedBankList: string[] = [];
  bankCout: number = 0;
  customersControls: any;
  selectedData: [{ value: string; text: string }] = [{
    value: "",
    text: ""
  }];
  isOtherBank: boolean = false;
  objData: any;
  paramaterarray: any;
  isCallCenter: any;
  masterData: any;
  public congnitoId: string = '';
  isOption: boolean = false;
  returnUrl = '';
  userRole: any;
  isrm: string = '';
  @ViewChild('cancelledCheque') cancelledCheque!: ElementRef;

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, private storage: StorageDataService, private http: HttpClient, private sharedService: SharedService
    , private router: Router, private appRouteEnum: AppRouteEnum, public apiRouteService: ApiRouteService, private notify: NotificationService, private activatedRoute: ActivatedRoute,private s3:S3UploadDownloadService) {

    this.SaveBankDetails = {};
    this.BankList = {};
    this.bankingForm = this.fb.group({
      customersArray: new FormArray([])
    });
  }
  deleteCheque(value,bank) {
    this.s3.deleteFile(value).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        bank.controls.cancelled_cheque.setValue('');
        this.cancelledCheque.nativeElement.value = "";
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }
  ngOnInit(): void {
    this.userRole = this.storage.getStorageData("userRole", false);
    this.congnitoId = this.storage.getStorageData("cognitoId", false);
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
      if (params['isrequired'] == 'true') {
        this.isOption = true;
        // const formarray = this.bankingForm?.get('customersArray') as FormArray;
        // for (let index = 0; index <formarray.length;index++ ) {
        //   this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.setErrors({
        //       required: true
        //     })
        // }
        // this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.setErrors({
        //   required: true
        // })
      }
      else {
        this.isOption = false;
      }
      if (params['isrm']) {
        this.isrm = params['isrm'];

      }
    });
    this.isOtherBank = false;
    this.bankingForm = this.fb.group({
      customersArray: new FormArray([])
    });
    //this.paramaterarray = this.storage.getStorageData("aflparams", true);
    this.isCallCenter = this.storage.getStorageData("isCallCenter", true);
    if (this.isCallCenter == "1") {
      this.BankList.cognitoId = this.storage.getStorageData("cognitoUserId", true);
      this.BankList.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    }
    else {
      this.BankList.cognitoId = this.storage.getStorageData("cognitoId", false);
    }
    //this.BankList.cognitoId=this.paramaterarray.cognitoId;
    // this.BankList.cognitoId="d52258f6-2927-4bbc-9bdb-2f6f0e5d4979";
    this.getBankListfn(this.isOption);
    this.customersControls = <FormArray>this.bankingForm.controls['customersArray'];
    this.SaveBankDetails.bankdetails = [];
    this.SaveBankDetails.deletedItem = [];
  }

  accountTypeChangefn(value: any, name: any) {
    var i = '' + name + '';
    this.bankingForm?.get('customersArray')?.get(i)?.get('AccountType')?.setValue(value);
    this.validateFields(name);
  }
  async uploadCheque(e: any, index: number, bank: any) {
    const file = e.target.files[0];
    if (file && (file.size <= 5242880) && (file.type == 'image/png' || file.type == 'image/jpg' || 
    file.type == 'image/jpeg' || file.type == 'image/bmp')) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => { }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + "/" + this.congnitoId + '/' + 'partnerDealer-bank-details' + (index + 1) + '.' + fileExt;
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        // bank.controls.cancelled_cheque.setValue(uploaded_file?.Location);
        this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.setValue(uploaded_file?.Location);
      }
      else {
        // bank.controls.cancelled_cheque.setValue('');
        this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.setValue('');
        this.notify.error("Upload Failed");
      }
    } else {
      this.notify.error("Invalid Image.")
      if ( this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.value == '') {
        this.bankingForm?.get('customersArray')?.get('' + index + '')?.get('cancelled_cheque')?.setErrors({
          required: true
        })
      }
    }
}
  getBankListfn(isOption:boolean) {
    //this.BankList.username="d52258f6-2927-4bbc-9bdb-2f6f0e5d4979";
    //this.BankList.username=this.paramaterarray.cognitoId;
    if (this.isCallCenter == "1") {
      this.BankList.username = this.storage.getStorageData("cognitoUserId", true);
    }
    else {
      this.BankList.username = this.storage.getStorageData("cognitoId", false);
    }
    this.BankList.getData = 'getData';
    //console.log('bank data',JSON.stringify(this.BankList));

    this.http.post(environment.bankingDetails,
      JSON.stringify(this.BankList))
      .subscribe((data: any) => {
        //console.log(data);
        //this.spinner.hide()
        this.objData = JSON.parse(data.body);
        console.log(this.objData)
        if (this.objData.message.length > 0) {
          this.masterData = this.objData.message;
          for (var b = 0; b < this.objData.message.length; b++) {
            this.selectedData.push({
              value: this.objData.message[b].bankId,
              text: this.objData.message[b].bankName
            });
          }
        }
        else {
          this.addBankFormGroup();
        }
        this.objData?.message.forEach((cust: Bankdetails) => {
          this.customersControls.push(this.createCustomerFormGroup(cust,isOption));
          //console.log(this.customersControls);
        });

        var a = this.bankingForm?.get('customersArray');
      });
  }

  selectedValue(event: MatSelectChange, name: any) {
    var i = '' + name + '';
    if (event.source.triggerValue == 'Others') {
      // this.isOtherBank=true; 
      this.bankingForm?.get('customersArray')?.get(i)?.get('BankName')?.setValue('1000');
      //this.bankingForm?.get('customersArray')?.get(i)?.get('otherBank')?.value;
      //  // event.source.value.form.otherBankflag=true;

    }
    else {
      this.isOtherBank = false;
      this.bankingForm?.get('customersArray')?.get(i)?.get('Bank')?.setValue(event.source.triggerValue);
      // this.selectedData.push({
      //   value: event.value,
      //   text: event.source.triggerValue
      // });
      this.validateFields(name);
    }
  }

  validateFields(name: any) {
    var i = '' + name + '';
    if (this.bankingForm?.get('customersArray')?.get(i)?.get('BankName')?.value != "") {
      if (this.bankingForm?.get('customersArray')?.get(i)?.get('BranchName')?.value == "")
        this.bankingForm?.get('customersArray')?.get(i)?.get('BranchName')?.setErrors({ required: true });
      if (this.bankingForm?.get('customersArray')?.get(i)?.get('AccountHolderName')?.value == "")
        this.bankingForm?.get('customersArray')?.get(i)?.get('AccountHolderName')?.setErrors({ required: true });
      if (this.bankingForm?.get('customersArray')?.get(i)?.get('AccountNo')?.value == "")
        this.bankingForm?.get('customersArray')?.get(i)?.get('AccountNo')?.setErrors({ required: true });
      if (this.bankingForm?.get('customersArray')?.get(i)?.get('IFSCSWIFTCode')?.value == "")
        this.bankingForm?.get('customersArray')?.get(i)?.get('IFSCSWIFTCode')?.setErrors({ required: true });
      if (this.bankingForm?.get('customersArray')?.get(i)?.get('AccountType')?.value == "")
        this.bankingForm?.get('customersArray')?.get(i)?.get('AccountType')?.setErrors({ required: true });
    }
    else {
      this.bankingForm?.get('customersArray')?.get(i)?.get('BranchName')?.setErrors(null);
      this.bankingForm?.get('customersArray')?.get(i)?.get('AccountHolderName')?.setErrors(null);
      this.bankingForm?.get('customersArray')?.get(i)?.get('AccountNo')?.setErrors(null);
      this.bankingForm?.get('customersArray')?.get(i)?.get('IFSCSWIFTCode')?.setErrors(null);
      this.bankingForm?.get('customersArray')?.get(i)?.get('AccountType')?.setErrors(null);
    }
  }

  createCustomerFormGroup(cust: Bankdetails,isOption:boolean) {
    if(cust.cancelled_cheque!=null && cust.cancelled_cheque!=''){
      this.isOption=true;
    }

    if(isOption)
    return this.fb.group({
      Bank: new FormControl(cust.bankName),
      BankName: new FormControl(cust.bankId),
      BranchName: new FormControl(cust.branch),
      AccountHolderName: new FormControl(cust.accountHolder),
      AccountNo: new FormControl(cust.accountNumber),
      IFSCSWIFTCode: new FormControl(cust.ifsc),
      AccountType: new FormControl(cust.accountType),
      cancelled_cheque:new FormControl(cust.cancelled_cheque,Validators.required),
      otherBank: new FormControl(cust.otherBank),
      UserBankId: new FormControl(cust.userBankId),
    });
    else
    return this.fb.group({
      Bank: new FormControl(cust.bankName),
      BankName: new FormControl(cust.bankId),
      BranchName: new FormControl(cust.branch),
      AccountHolderName: new FormControl(cust.accountHolder),
      AccountNo: new FormControl(cust.accountNumber),
      IFSCSWIFTCode: new FormControl(cust.ifsc),
      AccountType: new FormControl(cust.accountType),
      cancelled_cheque:new FormControl(cust.cancelled_cheque),
      otherBank: new FormControl(cust.otherBank),
      UserBankId: new FormControl(cust.userBankId),
    });
  }

  saveBankDetailsfn() {
    //this.spinner.show();
    const value = { ...this.bankingForm?.value };

    if (this.bankingForm?.value.otherBank !== null && this.isOtherBank == true) {

      this.selectedData.push({
        value: this.bankingForm?.value.otherBank,
        text: "0"
      });

    }

    //this.SaveBankDetails.username=this.paramaterarray.cognitoId;
    if (this.isCallCenter == "1") {
      this.SaveBankDetails.username = this.storage.getStorageData("cognitoUserId", true);
      this.SaveBankDetails.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    }
    else {
      this.SaveBankDetails.username = this.storage.getStorageData("cognitoId", false);
    }
    this.SaveBankDetails.bankdetails = [];
    for (let i = 0; i < value.customersArray.length; i++) {
      this.SaveBankDetails.bankdetails?.push(
        {

          'bankName': value.customersArray[i].Bank,
          'bankId': value.customersArray[i].BankName,
          'branch': value.customersArray[i].BranchName,
          'accountHolder': value.customersArray[i].AccountHolderName,
          'accountNumber': value.customersArray[i].AccountNo,
          'ifsc': value.customersArray[i].IFSCSWIFTCode,
          'accountType': value.customersArray[i].AccountType,
          'cancelled_cheque': value.customersArray[i].cancelled_cheque,
          'userBankId': value.customersArray[i].UserBankId
        }

      );
    }
    this.SaveBankDetails.deletedItem = this.DeletedBankList;
    //console.log('bankData',this.SaveBankDetails);
    this.http.post(environment.bankingDetails, JSON.stringify(this.SaveBankDetails)).subscribe(
      (response) => {
        this.objData = JSON.parse(JSON.stringify(response));

        if (this.objData.statusCode == 240) {
          this.notify.success("Bank Details Saved !!");

          if (this.isCallCenter == "1") 
          {
            this.storage.clearStorageData("isCallCenter");
            this.storage.clearStorageData("cognitoUserId");
            if (this.isOption) {
              if (this.userRole == this.apiRouteService.roles.customer) 
              {
                if (this.isrm != undefined && this.isrm != ''){
                  this.router.navigate([`./` + this.appRouteEnum.CustomerAgreement], { queryParams: { returnUrl: this.returnUrl, isrm: this.isrm } });
                }
                else{
                  this.router.navigate([`./` + this.appRouteEnum.CustomerAgreement], { queryParams: { returnUrl: this.returnUrl } });
                }

              }
              else {
                if (this.isrm != undefined && this.isrm != '')
                {
                  this.router.navigate([`./` + this.appRouteEnum.AdminCustomerAgreement], { queryParams: { returnUrl: this.returnUrl, isrm: this.isrm } });
                }
                else{
                  this.router.navigate([`./` + this.appRouteEnum.AdminCustomerAgreement], { queryParams: { returnUrl: this.returnUrl } });
                }
              }
            }
            else {
              if (this.isrm != undefined && this.isrm != ''){
                this.router.navigate([`/admin-dashboard/user-management-new`]);
              }
              else
              {
                this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
              }
            }
            // this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
          }
          else {
            // this.sharedService.redirecteToMP1AfterLogin();
            if (this.isOption) {
              if (this.userRole == this.apiRouteService.roles.customer) {
                if (this.isrm != undefined && this.isrm != ''){
                  this.router.navigate([`./` + this.appRouteEnum.CustomerAgreement], { queryParams: { returnUrl: this.returnUrl, isrm: this.isrm } });
                }
                  else
                  {
                  this.router.navigate([`./` + this.appRouteEnum.CustomerAgreement], { queryParams: { returnUrl: this.returnUrl } });
                  }
                }
              else {
                if (this.isrm != undefined && this.isrm != '')
                  {
                    this.router.navigate([`./` + this.appRouteEnum.AdminCustomerAgreement], { queryParams: { returnUrl: this.returnUrl, isrm: this.isrm } });
                  }
                  else
                  {
                  this.router.navigate([`./` + this.appRouteEnum.AdminCustomerAgreement], { queryParams: { returnUrl: this.returnUrl } });
                  }
                }
            }else{
              if(this.returnUrl){
                this.router.navigate([this.returnUrl]);
              }else{
                this.router.navigate([`./home`]);
              }
            }
          }
        }
        else if ((this.objData.statusCode == 520)) {
          this.notify.error("user not found ");
          if (this.isCallCenter == "1") {
            this.storage.clearStorageData("isCallCenter");
            this.storage.clearStorageData("cognitoUserId");
            if (this.isrm != undefined && this.isrm != '')
            {
              this.router.navigate([`/admin-dashboard/user-management-new`]);
            }
            else
            {
              this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
            }
          }
          else {
            this.router.navigate([`./home`]);
            //this.sharedService.redirecteToMP1AfterLogin();
          }
        }
        //  
      },

      (error) => {
        console.log(error)
        //this.spinner.hide()
      }
    )
    // 
  }

  skipfn() {
    // this.spinner.show();
    // if (this.isCallCenter == "1") {
    //   this.storage.clearStorageData("isCallCenter");
    //   this.storage.clearStorageData("cognitoUserId");
    //   this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
    // }
    // else {
    //   this.sharedService.redirecteToMP1AfterLogin();
    // }
    this.router.navigate([`./`], { queryParams: { returnUrl: this.returnUrl } })
  }


  addBankFormGroup() {
    const customersArray = this.bankingForm?.get('customersArray') as FormArray;
    this.customersControls.push(this.createBankFormGroup());
  }

  createBankFormGroup(): FormGroup {
    if(this.isOption)
    return new FormGroup({
      'Bank': new FormControl(''),
      'BankName': new FormControl('', Validators.required),
      'BranchName': new FormControl(''),
      'AccountHolderName': new FormControl(''),
      'AccountNo': new FormControl(''),
      'IFSCSWIFTCode': new FormControl(''),
      'AccountType': new FormControl(''),
      'otherBank': new FormControl(''),
      'otherBankflag': new FormControl(false),
      'UserBankId': new FormControl(''),
      //'customersArray': new FormArray([])
      'cancelled_cheque':new FormControl('',Validators.required),
    })
    else
    return new FormGroup({
      'Bank': new FormControl(''),
      'BankName': new FormControl('', Validators.required),
      'BranchName': new FormControl(''),
      'AccountHolderName': new FormControl(''),
      'AccountNo': new FormControl(''),
      'IFSCSWIFTCode': new FormControl(''),
      'AccountType': new FormControl(''),
      'otherBank': new FormControl(''),
      'otherBankflag': new FormControl(false),
      'UserBankId': new FormControl(''),
      //'customersArray': new FormArray([])
      'cancelled_cheque':new FormControl(''),
    })
  }

  removeBankFormGroup(id: any) {
    var i = '' + id + '';
    var bankId = this.bankingForm?.get('customersArray')?.get(i)?.get('UserBankId')?.value;
    this.customersControls.removeAt(i);
    this.DeletedBankList.push(bankId);
  }
}
