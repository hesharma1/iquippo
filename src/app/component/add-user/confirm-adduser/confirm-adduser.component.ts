import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { dataBound } from '@syncfusion/ej2-angular-grids';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';

@Component({
  selector: 'app-confirm-adduser',
  templateUrl: './confirm-adduser.component.html',
  styleUrls: ['./confirm-adduser.component.css']
})
export class ConfirmAdduserComponent implements OnInit {
  description: string = "";
  cognitoId: string = "";
  isrm: any;
  constructor(public dialogRef: MatDialogRef<ConfirmAdduserComponent>, private route: ActivatedRoute, @Inject(MAT_DIALOG_DATA) data: any, private router: Router
    , private appRouteEnum: AppRouteEnum) {
    this.cognitoId = data.cognitoId;
    this.description = data.message;
  }

  ngOnInit(): void {
    this.route
    .queryParams
    .subscribe(params => {            
      this.isrm = params['isrm'];
    });
  }

  okClick() {
    this.dialogRef.close();
    if(this.isrm!=undefined && this.isrm!=''){
      this.router.navigate([`./` + this.appRouteEnum.AdminProfile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1,isrm:this.isrm } });
    }
    else
    this.router.navigate([`./` + this.appRouteEnum.AdminProfile], { queryParams: { cognitoId: this.cognitoId, isCallCenter: 1 } });
  }
  
  backClick() {
    this.dialogRef.close();
    if(this.isrm!=undefined && this.isrm!=''){
      this.router.navigate([`/admin-dashboard/user-management-new` ]);
    }
    else{
    this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
    }
  }
}
