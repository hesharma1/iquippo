import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmAdduserComponent } from './confirm-adduser.component';




describe('ConfirmAdduserComponent', () => {
  let component: ConfirmAdduserComponent;
  let fixture: ComponentFixture<ConfirmAdduserComponent>;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmAdduserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmAdduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
