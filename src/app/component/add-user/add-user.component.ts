import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, Validator } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user';
import { GetUser, GetUserResponse } from 'src/app/models/getUserRequest.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { SharedService } from 'src/app/services/shared-service.service';
import { SaveUser } from 'src/app/models/saveUserRequest.model';
import { AccountService } from 'src/app/services/account';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmAdduserComponent } from './confirm-adduser/confirm-adduser.component';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { numberonly } from '../../utility/custom-validators/forgot-password';
import { DatePipe } from '@angular/common';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent implements OnInit {
  public getUserResponse = new GetUserResponse();
  public userName: string = "";
  public message: string = "";
  public preFix?: string = "";
  public Code?: string = "";
  minAge: any;
  pinCodeResponse?: PincodeResponse;
  pinCodeErrorFlag = false;
  pincodeBlur: boolean = false;
  countryList?: any;
  selectedCity?: CountryResponse;
  maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
  minDate: any;
  isrm: any;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private notificationservice: NotificationService, private storage: StorageDataService, private router: Router, public userService: UserService, public sharedService: SharedService, private accountService: AccountService, public dialog: MatDialog, private datePipe: DatePipe, private appRouteEnum: AppRouteEnum, private spinner: NgxSpinnerService) {
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
  }

  ngOnInit(): void {
    this.route
       .queryParams
       .subscribe(params => {            
         this.isrm = params['isrm'];
       });
    // this.getuser();
    this.adduserform.controls['status'].setValue('active');

    this.bindFlags();
  }

  adduserform: FormGroup = this.fb.group({
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    countrycode: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),
    dob: new FormControl('', [Validators.required]),
    Email: new FormControl('', [Validators.required, Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]),
    pincode: new FormControl('', [Validators.pattern(/[1-9]{1}[0-9]{5}$/), Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
    status: new FormControl('', [Validators.required])
  },
    {
      validator: [ageCheck('dob'), numberonly('mobile')]
    })

  checkPhoneField() {
    var prefixCode = this.Code;
    var mobile = this.adduserform.get('mobile')?.value;
    if (prefixCode != null && prefixCode == 'IN') {
      if (mobile.length >= 10 && mobile.length <= 14) {
        return true;
      }
      else {
        this.adduserform.controls['mobile']?.setErrors({ 'incorrect': true });
        return false;
      }
    }
    else {
      return true;
    }
  }

  adduser() {
    //this.spinner.show();
    var saveUserRequest = new SaveUser();
    //var paramaterarray = this.storage.getStorageData("aflparams", true);
    saveUserRequest.callcenterCognito = this.storage.getStorageData("cognitoId", false);
    saveUserRequest.PhoneNumber = this.adduserform.get('mobile')?.value;
    saveUserRequest.CountryCode = this.Code;
    saveUserRequest.PrefixCode = this.preFix;
    saveUserRequest.GivenName = this.adduserform.get('firstname')?.value;
    saveUserRequest.FamilyName = this.adduserform.get('lastname')?.value;
    saveUserRequest.Email = this.adduserform.get('Email')?.value;
    if(this.isrm!=undefined && this.isrm!=''){
      saveUserRequest.relationship_manager = this.isrm
    }
    saveUserRequest.DateBirth = this.adduserform.get('dob')?.value;
    saveUserRequest.PinCode = this.adduserform.get('pincode')?.value;
    saveUserRequest.Status = this.adduserform.get('status')?.value;
    this.userService.saveUserDetail(saveUserRequest).subscribe(response => {
      var getResponse = response as ResponseData;
      if (getResponse.statusCode == 200) {
        if (getResponse.body != null) {
           

          // this.getUserResponse = getResponse.body as GetUserResponse;
          this.message = "";
          const dialogRef = this.dialog.open(ConfirmAdduserComponent, {
            width: '650px',
            disableClose: true,
            data: { message: 'Do you want to add the User Profile and KYC details as well?', cognitoId: getResponse.body.UserName }

          });
        }
      }
      if (getResponse.statusCode == 409) {
         

        /* const dialogRef = this.dialog.open(ConfirmAdduserComponent, {
           width: '650px',
           data:{message :'User could not be added due to some unexpected error. Please check with Administrator'}
         });*/
        this.message = getResponse.body.message;
        // this.message="User could not be added due to some unexpected error. Please check with Administrator";
        this.notificationservice.error(this.message);
      }

    }, err => {
       
      console.log(err);
    });
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.selectedCity = this.countryList[0];
      this.Code = this.selectedCity?.countryCode;
      this.preFix = this.selectedCity?.prefixCode;
      this.adduserform.get('countrycode')?.setValue(this.countryList[0]);
    }, err => {
      this.notificationservice.error(err);
    })
  }

  updateCountryCode(city: any, code: any) {
    this.Code = code;
    this.preFix = city;
    this.adduserform.get('countrycode')?.setValue(city)
    this.adduserform.get('mobile')?.setValue('')

    if (city == '+91' && code == 'IN') {
      this.maxLengthValue = this.appRouteEnum.indiaPhoneNumber;
    } else {
      this.maxLengthValue = this.appRouteEnum.otherPhoneNumber;
    }
  }

  backusernav() {
    if(this.isrm!=undefined && this.isrm!=''){
      this.router.navigate([`/admin-dashboard/user-management-new` ]);
    }
    else{
    this.router.navigate([`./` + this.appRouteEnum.Usermanagement]);
    }
  }

  getuser() {
    var getUserRequest = new GetUser();
    getUserRequest.cognitoId = this.userName;
    this.userService.getUserDetail(getUserRequest).subscribe(response => {
      var getResponse = response as ResponseData;
      if (getResponse.statusCode == 200) {
        if (getResponse.body != null) {
          this.getUserResponse = getResponse.body as GetUserResponse;
          this.adduserform.get('firstname')?.setValue(this.getUserResponse.GivenName);
          this.adduserform.get('lastname')?.setValue(this.getUserResponse.FamilyName);
          this.adduserform.get('cntrycode')?.setValue(this.getUserResponse.PrefixCode);
          this.adduserform.get('mobile')?.setValue(this.getUserResponse.PhoneNumber);
          this.adduserform.get('Email')?.setValue(this.getUserResponse.Email);
          this.adduserform.get('pincode')?.setValue(this.getUserResponse.PinCode);
          this.adduserform.get('status')?.setValue(this.getUserResponse.Status);
          this.adduserform.get('dob')?.setValue(this.getUserResponse.DateBirth);
        }
      }
    });
  }

  checkEmailField() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != "") {
      var pro = this.sharedService.queryParams.aoc;
      if (pro != undefined && pro != null && pro == "qtt") {
        let email = this.adduserform?.get('Email')?.value;
        if (!email) {
          this.adduserform.get('Email')?.setErrors({ required: true });
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }
    else {
      return true;
    }
  }

  getpinCodefn() {
    this.accountService.getPinCode(this.adduserform.get('pincode')?.value)
      .subscribe((resp: any) => {
        // console.log(resp);
        this.pinCodeResponse = resp as PincodeResponse;
        // console.log("my",this.pinCodeResponse.pincode);

        if (this.pinCodeResponse.code === 258) {
          // console.log(this.pinCodeResponse.code);
          this.pinCodeErrorFlag = false;
        }
        else if (this.pinCodeResponse.code === 259) {
          // console.log(this.pinCodeResponse.code);
          this.pinCodeErrorFlag = true;
        }
        else {
          console.log("errorrr");
          this.pinCodeErrorFlag = true;
        }
      }, (error: any) => {
        console.log(error);
        this.pinCodeErrorFlag = true;

      })
  }
}
