import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user';
import { GetUser, GetUserResponse, VerifyDoc } from 'src/app/models/getUserRequest.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SharedService } from 'src/app/services/shared-service.service';
@Component({
  selector: 'app-verifyuser',
  templateUrl: './verifyuser.component.html',
  styleUrls: ['./verifyuser.component.css']
})
export class VerifyuserComponent implements OnInit {
  description: string = "";
  public userName: string = "";
  public docIds?: string[] = [];
  
  constructor(public dialogRef: MatDialogRef<VerifyuserComponent>, @Inject(MAT_DIALOG_DATA) data: any, public userService: UserService, private router: Router , private notificationservice: NotificationService , private spinner: NgxSpinnerService, private sharedService: SharedService ) {
    this.userName = data.id
  }

  ngOnInit(): void {

    //this.spinner.show();
    this.getuser();
  }

  okClick() {
    var getUserRequest = new VerifyDoc();
    getUserRequest.cognitoId = this.userName;
    getUserRequest.kycFlag = 'Verified';
    this.userService.verifyUserDetail(getUserRequest).subscribe(response => {
      var getResponse = response as ResponseData;

      if (getResponse.statusCode == 200) {
        this.dialogRef.close();
        this.description = 'Success';
        this.notificationservice.success(this.description);
        // this.router.navigate([`./user-management`]);
      }
      else {
        this.description = 'Failed';
        this.notificationservice.error(this.description);
      }

    });
  }

  close() {
    this.dialogRef.close();
  }

  backClick() {
    var getUserRequest = new VerifyDoc();
    getUserRequest.cognitoId = this.userName;
    getUserRequest.kycFlag = 'Rejected';
    this.userService.verifyUserDetail(getUserRequest).subscribe(response => {
      var getResponse = response as ResponseData;

      if (getResponse.statusCode == 200) {
        this.dialogRef.close();
        this.description = 'Success';
        this.notificationservice.success(this.description);
        // this.router.navigate([`./user-management`]);
      }
      else {
        this.description = 'Failed';
        this.notificationservice.error(this.description);
      }

    });
  }

  getuser() {
    var getUserRequest = new GetUser();
    getUserRequest.cognitoId = this.userName;
    this.userService.getUserDetail(getUserRequest).subscribe(response => {
      var getResponse = response as ResponseData;
      if (getResponse.statusCode == 200) {
        if (getResponse.body != null) {
           
          var getUserResponse = getResponse.body as GetUserResponse;
          this.docIds = getUserResponse.docIds;
        }
      }

      else {
         
        this.notificationservice.error(getResponse.body.message);
      }

    });

  }
}
