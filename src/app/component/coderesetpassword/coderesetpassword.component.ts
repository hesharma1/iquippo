import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'
import { StorageDataService } from "../../services/storage-data.service";
import { CheckPasswordMatch, numberonly } from '../../utility/custom-validators/forgot-password';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { MatDialog } from '@angular/material/dialog';
import { PwdresetdialogComponent } from '../pwdresetdialog/pwdresetdialog.component';
import { environment } from 'src/environments/environment';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-coderesetpassword',
  templateUrl: './coderesetpassword.component.html',
  styleUrls: ['./coderesetpassword.component.css']
})
export class CoderesetpasswordComponent implements OnInit {

  constructor(private fb: FormBuilder, private router: Router, public dialog: MatDialog,
    private storageDataService: StorageDataService, private awsConfig: AwsAuthService,
    private appRouteEnum: AppRouteEnum, private activatedRoute: ActivatedRoute, private sharedService: SharedService, private spinner: NgxSpinnerService) { }

  // otpErrorFlag= false;

  @ViewChild('otp2') otp2?: ElementRef;
  @ViewChild('otp3') otp3?: ElementRef;
  @ViewChild('otp4') otp4?: ElementRef;
  @ViewChild('otp5') otp5?: ElementRef;
  @ViewChild('otp6') otp6?: ElementRef;
  @ViewChild('otp1') otp1?: ElementRef;
  fieldTextType: boolean = false;
  fieldTextType1: boolean = false;
  resendOTP: boolean = false;
  remainingTime: any;
  message: string = '';
  isSubmitted: boolean = false;
  otpError?: boolean = false;

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sharedService.queryParams = params;
    });
    this.resendOTP = false;
    this.remainingTime = environment.remainingTime;
    this.startCountdown(environment.resendOtpTime);
     
  }

  startCountdown(seconds: number) {
    let counter = seconds;
    let minutes, sec, finalTime;
    // let elem = document.getElementById('resend');

    const interval = setInterval(() => {
      // console.log(counter);
      counter--;

      if (counter < 0) {
        clearInterval(interval);
        this.resendOTP = true;
        // console.log(this.resendOTP);
      }

      minutes = Math.floor(counter / 60);
      sec = counter - minutes * 60;
      finalTime = this.str_pad_left(minutes, '0', 2) + ':' + this.str_pad_left(sec, '0', 2);
      this.remainingTime = finalTime;

    }, 1000);
  }

  str_pad_left(str: number, pad: string, length: number) {
    return (new Array(length + 1).join(pad) + str).slice(-length);
  }

  resetForm = this.fb.group({
    otp1: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    otp2: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    otp3: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    otp4: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    otp5: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    otp6: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
    Password: ['', [Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/)]],
    confirmPassword: ['', [Validators.required]],
  }, {
    validator: [CheckPasswordMatch('Password', 'confirmPassword'), numberonly('otp1'), numberonly('otp2'), numberonly('otp3'), numberonly('otp4'), numberonly('otp5'), numberonly('otp6')]
  }
  )

  resetPassFun() {
    //this.spinner.show();
    // console.log("egerger");
    this.resetForm.markAllAsTouched();
    this.otpvalidationfun();
    let mobile = this.storageDataService.getStorageData('mobile', false);
    let forgetOTP = `${this.resetForm.get('otp1')?.value}${this.resetForm.get('otp2')?.value}${this.resetForm.get('otp3')?.value}${this.resetForm.get('otp4')?.value}${this.resetForm.get('otp5')?.value}${this.resetForm.get('otp6')?.value}`;
    let newPass = this.resetForm.get('Password')?.value;

    if (this.resetForm.valid) {
      this.otpError = false;
      this.awsConfig.awsForgotConfirmationPassword(mobile, forgetOTP, newPass)
        .then((resp) => {
           
          this.message = "";
          // console.log("final", resp);
          this.dialog.open(PwdresetdialogComponent, {
            disableClose: false,
            width: '500px',
            data: {
              params: (this.sharedService != null && this.sharedService != undefined) ? this.sharedService.queryParams : null
            }
          });
        })
        .catch(err => {
          console.log(err);
          if (err.code = "CodeMismatchException") {
            this.message = "OTP didn't match";
             
          }

        })
    }
    else { }
  }

  otpvalidationfun() {
    if (this.resetForm.get('otp1')?.invalid ||
      this.resetForm.get('otp2')?.invalid ||
      this.resetForm.get('otp3')?.invalid ||
      this.resetForm.get('otp4')?.invalid ||
      this.resetForm.get('otp5')?.invalid ||
      this.resetForm.get('otp6')?.invalid) {
      this.otpError = true;
      //  console.log("1",this.resetForm.get('otp1')?.invalid);
      //  console.log("2",this.resetForm.get('otp2')?.invalid);
      //  console.log("3",this.resetForm.get('otp3')?.invalid);
      //  console.log("4",this.resetForm.get('otp4')?.invalid);
      //  console.log("5",this.resetForm.get('otp5')?.hasError('required'),
      //  this.resetForm.get('otp5')?.hasError('pattern'),
      //  this.resetForm.get('otp5')?.hasError('letterError'));

      //  console.log("6",this.resetForm.get('otp6')?.hasError('required'),
      //  this.resetForm.get('otp6')?.hasError('pattern'),
      //  this.resetForm.get('otp6')?.hasError('letterError'));
    }
    else if (this.resetForm.get('otp1')?.valid && this.resetForm.get('otp2')?.valid &&
      this.resetForm.get('otp3')?.valid && this.resetForm.get('otp4')?.valid &&
      this.resetForm.get('otp5')?.valid && this.resetForm.get('otp6')?.valid) {
      this.otpError = false;
    }
  }

  //   myfun(val:string){
  //     // console.log(val);
  //     // val.
  //     // @ts-ignore
  //     // val.nextSibling.focus();
  //     switch (val) {
  //       case '1':
  //         if(this.resetForm.get('otp1')?.value!=''&&this.resetForm.get('otp1')?.value!=null&& this.resetForm.get('otp1')?.value!=undefined){
  //           this.otp2?.nativeElement.focus();
  //         this.otp2?.nativeElement.select();            
  //         }
  //         break;
  //         case '2':
  //           if(this.resetForm.get('otp2')?.value!=''&&this.resetForm.get('otp2')?.value!=null&& this.resetForm.get('otp2')?.value!=undefined){
  //             this.otp3?.nativeElement.focus();
  //             this.otp3?.nativeElement.select();             
  //           }    
  //         break;
  //         case '3':
  //           if(this.resetForm.get('otp3')?.value!=''&&this.resetForm.get('otp3')?.value!=null&& this.resetForm.get('otp3')?.value!=undefined){
  //             this.otp4?.nativeElement.focus();
  //             this.otp4?.nativeElement.select();             
  //           }    
  //         break;
  //         case '4':
  //           if(this.resetForm.get('otp4')?.value!=''&&this.resetForm.get('otp4')?.value!=null&& this.resetForm.get('otp4')?.value!=undefined){
  //             this.otp5?.nativeElement.focus();
  //             this.otp5?.nativeElement.select();             
  //           }    
  //         break;
  //         case '5':
  //           if(this.resetForm.get('otp5')?.value!=''&&this.resetForm.get('otp5')?.value!=null&& this.resetForm.get('otp5')?.value!=undefined){
  //             this.otp6?.nativeElement.focus();
  //             this.otp6?.nativeElement.select();             
  //           }    
  //         break;
  //       default:
  //         break;
  //     }
  //     // if(this.resetForm.get('otp1')?.valid && this.resetForm.get('otp2')?.valid && 
  //     // this.resetForm.get('otp3')?.valid && this.resetForm.get('otp4')?.valid&&
  //     // this.resetForm.get('otp5')?.valid && this.resetForm.get('otp6')?.valid){
  //     //                 this.otpError= false;
  //     // }
  //     // this.otp3?.nativeElement.focus();
  // // this.otp3?.nativeElement.select();
  //   }
  myfun(val: string) {
    // console.log(val);
    // val.
    // @ts-ignore
    // val.nextSibling.focus();
    if (this.isSubmitted) {
      this.otpvalidationfun();
    }
    switch (val) {
      case '1':
        if (this.resetForm.get('otp1')?.value != '' && this.resetForm.get('otp1')?.value != null && this.resetForm.get('otp1')?.value != undefined) {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '2':
        if (this.resetForm.get('otp2')?.value != '' && this.resetForm.get('otp2')?.value != null && this.resetForm.get('otp2')?.value != undefined) {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        else {
          this.otp1?.nativeElement.focus();
          this.otp1?.nativeElement.select();
        }
        break;
      case '3':
        if (this.resetForm.get('otp3')?.value != '' && this.resetForm.get('otp3')?.value != null && this.resetForm.get('otp3')?.value != undefined) {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        else {
          this.otp2?.nativeElement.focus();
          this.otp2?.nativeElement.select();
        }
        break;
      case '4':
        if (this.resetForm.get('otp4')?.value != '' && this.resetForm.get('otp4')?.value != null && this.resetForm.get('otp4')?.value != undefined) {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        else {
          this.otp3?.nativeElement.focus();
          this.otp3?.nativeElement.select();
        }
        break;
      case '5':
        if (this.resetForm.get('otp5')?.value != '' && this.resetForm.get('otp5')?.value != null && this.resetForm.get('otp5')?.value != undefined) {
          this.otp6?.nativeElement.focus();
          this.otp6?.nativeElement.select();
        }
        else {
          this.otp4?.nativeElement.focus();
          this.otp4?.nativeElement.select();
        }
        break;
      case '6':
        if (this.resetForm.get('otp6')?.value != '' && this.resetForm.get('otp6')?.value != null && this.resetForm.get('otp6')?.value != undefined) {
          this.otp6?.nativeElement.focus();
        }
        else {
          this.otp5?.nativeElement.focus();
          this.otp5?.nativeElement.select();
        }
        break;
      case '11':
        if (this.resetForm.get('otp1')?.value != '' && this.resetForm.get('otp1')?.value != null && this.resetForm.get('otp1')?.value != undefined) {
          this.otp1?.nativeElement.select();
          this.otp1?.nativeElement.focus();
        }
        break;
      case '22':
        if (this.resetForm.get('otp2')?.value != '' && this.resetForm.get('otp2')?.value != null && this.resetForm.get('otp2')?.value != undefined) {
          this.otp2?.nativeElement.select();
          this.otp2?.nativeElement.focus();
        }
        break;
      case '33':
        if (this.resetForm.get('otp3')?.value != '' && this.resetForm.get('otp3')?.value != null && this.resetForm.get('otp3')?.value != undefined) {
          this.otp3?.nativeElement.select();
          this.otp3?.nativeElement.focus();
        }
        break;
      case '44':
        if (this.resetForm.get('otp4')?.value != '' && this.resetForm.get('otp4')?.value != null && this.resetForm.get('otp4')?.value != undefined) {
          this.otp4?.nativeElement.select();
          this.otp4?.nativeElement.focus();
        }
        break;
      case '55':
        if (this.resetForm.get('otp5')?.value != '' && this.resetForm.get('otp5')?.value != null && this.resetForm.get('otp5')?.value != undefined) {
          this.otp5?.nativeElement.select();
          this.otp5?.nativeElement.focus();
        }
        break;

      default:
        break;
    }

  }

  gotoResetPass() {
    if (this.sharedService != null && this.sharedService != undefined && this.sharedService.queryParams != null && this.sharedService.queryParams != undefined) {
      var pro = this.sharedService.queryParams.aoc;
      var src = this.sharedService.queryParams.src;
      if (pro != undefined && pro != null && pro == "qtt" && src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { src: this.sharedService.queryParams.src, aoc: this.sharedService.queryParams.aoc } });
      }
      else if (pro != undefined && pro != null && pro == "qtt") {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { aoc: this.sharedService.queryParams.aoc } });
      }
      else if (src != undefined && src != null) {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword], { queryParams: { src: this.sharedService.queryParams.src } });
      }
      else {
        this.router.navigate([`./` + this.appRouteEnum.ForgotPassword]);
      }
    } else {
      this.router.navigate([`./` + this.appRouteEnum.ForgotPassword]);
    }
  }

  sendOTP() {
    let phoneNumber = this.storageDataService.getStorageData('mobile', false);

    this.awsConfig.awsForgotPassword(phoneNumber)
      .then((resp) => {
        this.resendOTP = false;
        this.remainingTime = environment.remainingTime;
        this.startCountdown(environment.resendOtpTime);
      })
      .catch(err => {
        console.log("some error in getting otp");

      })
  }

}
