import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoderesetpasswordComponent } from './coderesetpassword.component';

describe('CoderesetpasswordComponent', () => {
  let component: CoderesetpasswordComponent;
  let fixture: ComponentFixture<CoderesetpasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoderesetpasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoderesetpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
