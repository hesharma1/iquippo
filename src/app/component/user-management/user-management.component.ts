import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { FilterSettingsModel } from '@syncfusion/ej2-angular-grids';
import { UserService } from 'src/app/services/user';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { GridComponent } from '@syncfusion/ej2-angular-grids'
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { VerifyuserComponent } from 'src/app/component/add-user/verify-user/verifyuser.component';
import { MatDialog } from '@angular/material/dialog';
import { ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
export interface PeriodicElement {
  UserName: string;
  firstName: string;
  email: string;
  phoneNumber: string;
  kyc_verified: string;
  state: string;
  city: string;
  pan_address_proof?: string;
  Groups: string[];
  status:string;
}

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})

export class UserManagementComponent implements OnInit {
  @ViewChild('grid') public grid?: GridComponent;

  role?: string;
  searchData: any = {
    'source': '',
    'mobileNumber': '',
    'firstName': ''
  };
  filterUserForm: any = this.formbulider.group({
    Mobile: '',
    Name: '',
  });
  public data: PeriodicElement[] = [];
  pageSettings: PageSettingsModel = { pageSize: 10 };
  public toolbarOptions?: ToolbarItems[] = ['ExcelExport'];
  filterOptions: FilterSettingsModel = {
    type: 'Menu'
  };

  constructor(private router: Router, public userService: UserService, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private sharedService: SharedService, private storage: StorageDataService, public dialog: MatDialog, private appRouteEnum: AppRouteEnum, private formbulider: FormBuilder,
    private notificationservice: NotificationService, private apiRouteService: ApiRouteService) {

  }
  
  ngOnInit(): void {
    this.role = this.storage.getStorageData("userRole", false);
    if (!(this.role == this.apiRouteService.roles.superadmin || this.role == this.apiRouteService.roles.admin)) {
      this.toolbarOptions = [];
    }
    if (this.role == this.apiRouteService.roles.customer) {
      this.sharedService.redirecteToMP1AfterLogin();
    }
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['src']) {
        this.storage.clearStorageData("src");
        this.storage.setStorageData("src", params['src'], true);
        // this.sharedService.queryParamsAfterLogin = params;
        // this.storage.clearStorageData("aflparams");
        // this.storage.setStorageData("aflparams", params, true);
      }
      if (params['cognitoId']) {
        this.storage.clearStorageData("cognitoId");
        this.storage.setStorageData("cognitoId", params['cognitoId'].replace(/['"]+/g, ''), true);
      }
      if (params['deviceKey']) {
        this.storage.clearStorageData("deviceKey");
        this.storage.setStorageData("deviceKey", params['deviceKey'], true);
      }
      if (params['refreshToken']) {
        this.storage.clearStorageData("refreshToken");
        this.storage.setStorageData("refreshToken", params['refreshToken'], true);
      }
    });
    //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': ''
    };
    this.getusersfn(this.searchData);
    this.initializeFilterFormGroup();

  }
  
  togglestatus(userCog:any,status:any) {
     var payload =  {
        "cognitoId": userCog,
        "status":status=="active"?"inactive":"active"
      }
      this.userService.updateStatus(payload).subscribe((res:any)=>{
        if(res?.statusCode==200){
          this.notificationservice.success(res?.body?.message);
        this.getusersfn(this.searchData);
        }
      },err=>{
        console.log(err);
      })
  }
  verifyusernav(id: any) {
    const dialogRef = this.dialog.open(VerifyuserComponent, {
      width: '500px',
      data: { id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getusersfn(this.searchData);
    });
  }

  toolbarClick(args: ClickEventArgs): void {
    if (args.item.id === 'Grid_excelexport') { // 'Grid_excelexport' -> Grid component id + _ + toolbar item name
      this.grid?.excelExport();
    }
  }

  addusernav() {
    this.router.navigate([`./` + this.appRouteEnum.Adduser]);
  }

  editusernav(e: any, userName: any) {
    this.router.navigate([`./` + this.appRouteEnum.AdminProfile], { queryParams: { cognitoId: userName, isCallCenter: 1 } });
    //this.router.navigate([`./`  + this.appRouteEnum.Adduser], { queryParams: { userName: userName } });
  }

  dataBound() {
    this.grid?.autoFitColumns(['email', 'phoneNumber']);
  }


  getusersfn(data: any) {
    this.userService.getDetails(data).subscribe(response => {
      var getResponse = response as ResponseData;
      if (getResponse.statusCode == 200) {
        if (getResponse.body != null) {
          var getResponseBodyJSON = JSON.parse(getResponse.body);
          this.data = getResponseBodyJSON as PeriodicElement[];
           
        }
      }
      else {
        var getResponseBodyJSON = JSON.parse(getResponse.body);
        this.notificationservice.error(getResponseBodyJSON?.message);
         
      }
    });
  }

  filterData() {
    //this.spinner.show();
    var formData = this.filterUserForm.value;
    if (formData != undefined) {
      this.searchData = {
        'source': 'filter',
        'mobileNumber': formData.Mobile,
        'firstName': formData.Name
      };
      this.getusersfn(this.searchData);
    }
  }

  resetData() {
    //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': ''
    };
    this.grid?.clearFiltering();
    this.getusersfn(this.searchData);
    this.initializeFilterFormGroup();
  }
  
  initializeFilterFormGroup() {
    this.filterUserForm.setValue({
      Mobile: '',
      Name: ''
    });
  }

  getStatus(value) {
    if(value == undefined) {
      return value;
    }
    if(value == 'true' || value == 'True' || value == true) {
      return 'Yes';
    } else {
      return 'No'
    }
  }
}


