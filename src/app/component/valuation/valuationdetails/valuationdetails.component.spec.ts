import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuationdetailsComponent } from './valuationdetails.component';

describe('ValuationdetailsComponent', () => {
  let component: ValuationdetailsComponent;
  let fixture: ComponentFixture<ValuationdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValuationdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuationdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
