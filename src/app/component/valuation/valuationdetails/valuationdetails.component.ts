import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { validate } from 'graphql';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SigninComponent } from '../../signin/signin.component';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};
@Component({
  selector: 'app-valuationdetails',
  templateUrl: './valuationdetails.component.html',
  styleUrls: ['./valuationdetails.component.css'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },
  //   { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  // ],
})
export class ValuationdetailsComponent implements OnInit {
  country_data: any = [];
  valdetailsform: FormGroup = new FormGroup({});
  model_name;
  cognitoId;
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  documentName: Array<any> = [];
  invoiceDocumentName:  Array<any> = [];
  imageName: any;
  stateList: any = [];
  countries : string[] = [];
  todayDate = new Date();
  yearOfManufacture:any = null;
  constructor(private fb:FormBuilder, private router: Router,public s3: S3UploadDownloadService,private dialog: MatDialog,
     private route: ActivatedRoute, private valuationService: ValuationService, private agreementServiceService: AgreementServiceService, 
     private masterAdminService: AdminMasterService,private datePipe: DatePipe,public notify: NotificationService,
     public apiService: UsedEquipmentService,private spinner: NgxSpinnerService,private storage:StorageDataService) {
    
    this.valdetailsform= this.fb.group({
      valuation_on_behalf: new FormControl("0"),
      customer_name: new FormControl('',[Validators.pattern('^[a-zA-Z ]*$')]),
      rc_number: new FormControl('',[Validators.maxLength(25)]),
      machine_sr_number: new FormControl('',[Validators.minLength(2), Validators.maxLength(25)]),
      chassis_number: new FormControl('',[Validators.minLength(2), Validators.maxLength(25)]),
      engine_number: new FormControl('',[Validators.minLength(2), Validators.maxLength(25)]),
      manu_year: new FormControl(null),
      country: new FormControl('1', Validators.required ),
      pin_code: new FormControl('',[Validators.required,Validators.maxLength(6)]),
      pin: new FormControl('',[Validators.required,Validators.maxLength(6)]),
      state: new FormControl({value :'', disabled: true}, Validators.required),
      city: new FormControl({value : '', disabled: true}, Validators.required),
      address: new FormControl(''),
      is_invoice_available: new FormControl(true),
      invoice_date: new FormControl(null),
      invoice_value: new FormControl(null),
      invoice_document: new FormControl(''),
      is_site_person: new FormControl("1"),
      contact_person: new FormControl('', [Validators.pattern('^[a-zA-Z ]*$')]),
      contact_number: new FormControl('',Validators.compose([
      Validators.minLength(10), Validators.maxLength(10)])),
      // contact_number: new FormControl('',[Validators.pattern('/^[6-9]\d{9}$/'),
      // Validators.minLength(10), Validators.maxLength(10)]),
      remarks: new FormControl(''),
      for_sale: new FormControl(false),
      valuation_asset: new FormControl(''),
      valuation_type: new FormControl(2),
      advanced_valuation_type: new FormControl(1),
      rc_upload: new FormControl(''),
      rc_image: new FormControl(''),
      seller: new FormControl(''),
      state_id: new FormControl(''),
      product_name:new FormControl(''),
    }, {
      validator: [numberonly('invoice_value')]})
   }
   unique_control_number;
   get_response: any;
  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    this.route.params.subscribe((params: Params): void =>{
      this.unique_control_number = params['unique_control_number'];
      this.getDetail(this.unique_control_number);
    })
    this.masterAdminService.getCountryList().subscribe((result:any)=>{
      this.country_data = result.results;
    });
    
  }

  setValidationOnInvoiceFields()
  {
    //let value = event.value;
    let value = this.valdetailsform?.get("is_invoice_available")?.value;
    if(value == "1")
    {
      this.valdetailsform?.get('invoice_date')?.setValidators([Validators.required]);
      this.valdetailsform?.get('invoice_date')?.updateValueAndValidity();
      this.valdetailsform?.get('invoice_value')?.setValidators([Validators.required]);
      this.valdetailsform?.get('invoice_value')?.updateValueAndValidity();
      this.valdetailsform?.get('invoice_document')?.setValidators([Validators.required]);
      this.valdetailsform?.get('invoice_document')?.updateValueAndValidity();
    }
    else{
      this.valdetailsform?.get('invoice_date')?.clearValidators();
      this.valdetailsform?.get('invoice_date')?.updateValueAndValidity();
      this.valdetailsform?.get('invoice_value')?.clearValidators();
      this.valdetailsform?.get('invoice_value')?.updateValueAndValidity();
      this.valdetailsform?.get('invoice_document')?.clearValidators();
      this.valdetailsform?.get('invoice_document')?.updateValueAndValidity();
    }
  }

  setSiteContactPersonValidation(){
    //let value = event.value;
    let value = this.valdetailsform?.get("is_site_person")?.value;
    if(value == "0"){
      this.valdetailsform?.get('contact_person')?.setValidators([Validators.required]);
      this.valdetailsform?.get('contact_person')?.updateValueAndValidity();
      this.valdetailsform?.get('contact_number')?.setValidators([Validators.required]);
      this.valdetailsform?.get('contact_number')?.updateValueAndValidity();
    }
    else{
      this.valdetailsform?.get('contact_person')?.clearValidators();
      this.valdetailsform?.get('contact_person')?.updateValueAndValidity();
      this.valdetailsform?.get('contact_number')?.clearValidators();
      this.valdetailsform?.get('contact_number')?.updateValueAndValidity();
    }
  }

  addCustomerNameValidation(){
    //let value = event.value;
    let value = this.valdetailsform?.get("is_site_person")?.value;
    if(value == "0"){
      this.valdetailsform?.get('customer_name')?.setValidators([Validators.required]);
      this.valdetailsform?.get('customer_name')?.updateValueAndValidity();
    }
    else if(value == "1"){
      this.valdetailsform?.get('customer_name')?.clearValidators();
      this.valdetailsform?.get('customer_name')?.updateValueAndValidity();
    }
  }
  getDetail(unc: string) {
    this.valuationService.getAdvancedVal(unc).subscribe((res: any) => {
      this.get_response = res;
        this.valdetailsform.patchValue({
          brand: res?.valuation_asset?.brand.id,
          model: res?.valuation_asset?.model.id,
          category: res?.valuation_asset?.category.id,
          rc_number: res?.valuation_asset?.rc_number?res.valuation_asset?.rc_number:undefined,
          engine_number: res?.valuation_asset?.engine_number,
          chassis_number: res?.valuation_asset?.chassis_number,
          machine_sr_number: res?.valuation_asset?.machine_sr_number,
          valuation_asset: res?.valuation_asset?.id,
          pin_code: res?.valuation_asset?.pin_code?.pin_code,
          pin: res.valuation_asset?.pin_code?.pin_code,
          city: res.valuation_asset?.pin_code?.city?.name,
          state: res.valuation_asset?.pin_code?.city?.state?.name,
          // address: res?.valuation_asset?.address,
          address: res?.valuation_asset?.asset_address,
          // country: res?.valuation_asset?.city?.state?.country?.id,
          country: res?.valuation_asset?.country ? res?.valuation_asset?.country : 1,
          is_invoice_available: res?.valuation_asset?.is_invoice_available == false ? "0" : "1",
          invoice_date: res?.valuation_asset?.invoice_date,
          invoice_value: res?.valuation_asset?.invoice_value,
          invoice_document: res?.valuation_asset?.invoice_document,
          is_site_person: res.is_site_person == false ? "0" : "1",
          contact_person: res.contact_person,
          contact_number: res.contact_number,
          remarks: res?.remarks,
          for_sale: res?.for_sale,
          customer_name: res?.customer_name,
          valuation_on_behalf: res?.valuation_on_behalf == true ? "1" : "0",
          seller: this.cognitoId
        })
        // let onBehalf = res?.valuation_on_behalf == true ? "1" : "0";
        // let sitePerson = res?.is_site_person == false ? "0" : "1";
        // let invoiceAva = res?.valuation_asset?.is_invoice_available == false ? "0" : "1";
        // this.addCustomerNameValidation(onBehalf);
        if(this.get_response.valuation_asset.mfg_year){
          let date =  new Date();
          date.setFullYear(this.get_response.valuation_asset.mfg_year);
          this.yearOfManufacture = date;
          this.valdetailsform.get('manu_year')?.setValue(date);
        }
        this.setSiteContactPersonValidation();
        this.setValidationOnInvoiceFields();
        this.model_name =  (this.get_response.valuation_asset?.category?.display_name == 'Other'?
        this.get_response.valuation_asset?.other_category:this.get_response.valuation_asset?.category?.display_name) +' ' +
        (this.get_response.valuation_asset?.brand?.display_name == 'Other'?this.get_response.valuation_asset?.other_brand:
        this.get_response.valuation_asset.brand?.display_name) +' ' + (this.get_response.valuation_asset?.model?.name == 'Other'?
        this.get_response.valuation_asset?.other_model:this.get_response.valuation_asset?.model?.name);
      
      //this.model_name = this.get_response?.valuation_asset?.category?.display_name + ' ' + this.get_response.valuation_asset?.brand?.display_name + ' ' + this.get_response.valuation_asset?.model?.name;
    })
    this.behalf_validator();
    this.updateValidations();
    this.addCustomerNameValidation();
  }
  save_and_close() {
    if(!(this.valdetailsform.value.chassis_number || this.valdetailsform.value.engine_number || this.valdetailsform.value.machine_sr_number)){
      this.notify.error("Please enter either of Engine Number, Chassis Number or Serial Number")
      return;
    }
    var valuationRequest = {
      //category: null,
      //brand: null,
      //model: null,
      is_valuation: true,
      advanced_valuation_type: 1,
      valuation_on_behalf: null,
      customer_name: null,
      is_site_person: null,
      contact_person: null,
      contact_number: null,
      remarks: null,
      for_sale: null,
      status: 2,
      seller: this.cognitoId
    }
    
    //valuationRequest.category = this.get_response.valuation_asset.category.id;
    //valuationRequest.model = this.get_response.valuation_asset.model.id;
    //valuationRequest.brand = this.get_response.valuation_asset?.brand.id;
    valuationRequest.advanced_valuation_type = this.valdetailsform.get('advanced_valuation_type')?.value;
    valuationRequest.valuation_on_behalf = this.valdetailsform.get('valuation_on_behalf')?.value;
    valuationRequest.customer_name = this.valdetailsform.get('customer_name')?.value;
    valuationRequest.is_site_person = this.valdetailsform.get('is_site_person')?.value;
    valuationRequest.contact_person = this.valdetailsform.get('contact_person')?.value;
    valuationRequest.contact_number = this.valdetailsform.get('contact_number')?.value;
    valuationRequest.remarks = this.valdetailsform.get('remarks')?.value;
    valuationRequest.for_sale = this.valdetailsform.get('for_sale')?.value;

    var valAssetRequest = {
      is_valuation: true,
      category: null,
      model: null,
      brand: null,
      rc_number: undefined,
      seller: null,
      valuation_type: 2,
      other_category: undefined,
      other_brand: undefined,
      other_model: undefined,
      mfg_year: '',
      is_invoice_available: true,
      asset_address: null,
      pin_code: undefined,
      state: undefined,
      country: undefined,
      rc_document: undefined,
      invoice_date: '',
      invoice_value: undefined,
      invoice_document: undefined,
      // status: 2,
      status: 1,
      valuation_asset: undefined,
      product_name: null,
      chassis_number: null,
      machine_sr_number: null,
      engine_number: null,
      is_sell: null
    }
    valAssetRequest.chassis_number = this.valdetailsform.get('chassis_number')?.value;
    valAssetRequest.engine_number = this.valdetailsform.get('engine_number')?.value;
    valAssetRequest.machine_sr_number = this.valdetailsform.get('machine_sr_number')?.value;
    valAssetRequest.category = this.get_response.valuation_asset.category.id;
    valAssetRequest.model = this.get_response.valuation_asset.model.id;
    valAssetRequest.brand = this.get_response.valuation_asset.brand.id;
    valAssetRequest.rc_number = this.valdetailsform.get('rc_number')?.value;
    valAssetRequest.seller = this.cognitoId;
    valAssetRequest.valuation_type = this.valdetailsform.get('valuation_type')?.value;
    valAssetRequest.other_category = this.valdetailsform.get('other_category')?.value;
    valAssetRequest.other_brand = this.valdetailsform.get('other_brand')?.value;
    valAssetRequest.other_model = this.valdetailsform.get('other_model')?.value;
    valAssetRequest.is_sell = this.valdetailsform.get('for_sale')?.value;
    if (this.valdetailsform.get('manu_year')?.value != "") {
      let date = new Date();
      date = this.valdetailsform.get('manu_year')?.value!;
      valAssetRequest.mfg_year = this.datePipe.transform(date, 'yyyy')!;
    }
    if (this.valdetailsform.get('invoice_date')?.value != "") {
      let date = new Date();
      date = this.valdetailsform.get('invoice_date')?.value!;
      valAssetRequest.invoice_date = this.datePipe.transform(date, 'yyyy-MM-dd')!;
    }
    // valAssetRequest.mfg_year = this.valdetailsform.get('manu_year')?.value;
    valAssetRequest.is_invoice_available = this.valdetailsform.get('is_invoice_available')?.value;
    //valAssetRequest.asset_address = this.valdetailsform.get('asset_address')?.value;
    valAssetRequest.asset_address = this.valdetailsform.get('address')?.value;
    valAssetRequest.pin_code = this.valdetailsform.get('pin_code')?.value;
    valAssetRequest.state = this.valdetailsform.get('state_id')?.value;
    valAssetRequest.country = this.valdetailsform.get('country')?.value;
    valAssetRequest.rc_document =this.documentName.length > 0 ? this.documentName[this.documentName.length - 1].url : "";
    valAssetRequest.invoice_value = this.valdetailsform.get('invoice_value')?.value;
    valAssetRequest.invoice_document = this.valdetailsform.get('invoice_document')?.value ? this.valdetailsform.get('invoice_document')?.value : "";
    valAssetRequest.valuation_asset = this.valdetailsform.get('valuation_asset')?.value;
    this.valdetailsform.get('product_name')?.setValue(this.model_name);
    valAssetRequest.product_name = this.valdetailsform.get('product_name')?.value;
    valAssetRequest.is_valuation = true;
    console.log(valAssetRequest);

    
    this.valuationService.putAssetId(valAssetRequest).subscribe((res: any) => {
      let assetId = res.id;
      this.valuationService.putAdvancedVal(this.unique_control_number, valuationRequest).subscribe((res: any) => {
        if (this.cognitoId) {
          let isAuthorise = true;
          if (isAuthorise) {
            let payload = {
              asset_id: assetId,
              cognito_id: this.cognitoId,
              source: 'Advance_valuation',
              payment_type: 2
            }
            this.apiService.validateBid(payload).pipe(finalize(() => {   })).subscribe(
              (data: any) => {
                if (data.redirect) {
                  if(data.valuation_fee == 0){
                    let request = {
                      transaction_id: data.id,
                      status: 12
                    }
                    this.valuationService.patchAdvanceVal(this.unique_control_number, request).subscribe(res => {
                      this.notify.success('Request Submitted Successfully!!', true);
                      if(this.storage.getSessionStorageData('isFromAdmin', false)){
                        sessionStorage.removeItem('isFromAdmin');
                        window.open("/admin-dashboard/adv-val-request", '_self');
                      }else{
                      window.open("/customer/dashboard/advanced-valuation-request", '_self');
                      }
                      //this.router.navigate(['/customer/dashboard/advanced-valuation-request'], { queryParams: { reload: true } });
                    })
                  }else{
                    let paymentDetails = {
                      asset_id: assetId,
                      asset_type: 'Used',
                      case: 'Advance_valuation',
                      payment_type: 'Full'
                    }
                    this.storage.setSessionStorageData('paymentSession', true, false);
                    this.storage.setSessionStorageData('AdvanceValuation', window.btoa(JSON.stringify(data)), false);
                    this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                    this.storage.setSessionStorageData('uniqueControlNumber',this.unique_control_number,false);
                   // this.router.navigate(['/customer/payment']);
                   window.open("/customer/payment", '_self');
                  }
                } else {
                  this.notify.error(data.message);
                }
              },
              err => {
                console.log(err);
              }
            )
          }
        } else {
          const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    //dialogRef.afterClosed().subscribe(result => {
    //    console.log(`Dialog result: ${result}`);
    //});
  

  dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = localStorage.getItem('cognitoId');
        }
      })
        }
      })
    }) 
  }
  behalf_validator(){
    if(this.valdetailsform.get('valuation_on_behalf')?.value == '0'){
      this.valdetailsform.get('customer_name')?.setValidators(Validators.required);
      this.valdetailsform.get('customer_name')?.updateValueAndValidity();
    }
    else if(this.valdetailsform.get('valuation_on_behalf')?.value == '1'){
      this.valdetailsform.get('customer_name')?.clearValidators();
      this.valdetailsform.get('customer_name')?.updateValueAndValidity();
    }
  }
  adv_val_ins(){
    this.valdetailsform.get('advanced_valuation_type')?.setValue(1);
  }
  adv_val_ass(){
    this.valdetailsform.get('advanced_valuation_type')?.setValue(2);
  }
  updateValidations(){
    if(this.valdetailsform){
      if(this.valdetailsform.get('is_site_person')?.value == '1'){
        this.valdetailsform.get('contact_person')?.clearValidators();
        this.valdetailsform.get('contact_person')?.updateValueAndValidity();
        this.valdetailsform.get('contact_number')?.clearValidators();
        this.valdetailsform.get('contact_number')?.updateValueAndValidity();
      }
    }
  }
  invoice_update(){
    if(this.valdetailsform){
      if(this.valdetailsform?.get('is_invoice_available')?.value == 0)
      this.valdetailsform?.get('invoice_date')?.setValue(null);
      this.valdetailsform?.get('invoice_value')?.setValue(null);
      this.valdetailsform?.get('invoice_document')?.setValue(null);
      this.invoiceDocumentName = [];
    }
  }
  resetFile(name, url) {
    if(name == 'rc_image'){
      this.documentName = [];
      this.valdetailsform?.get('rc_image')?.setValue(null);
    }else if(name == 'invoice_document'){
      this.invoiceDocumentName = [];
      this.valdetailsform?.get('invoice_document')?.setValue(null);
    }
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
  this.s3.deleteFile(imgPath).subscribe(res => {
  })
  }
  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/'+ this.unique_control_number + "_" + "rc_upload" + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
    console.log(uploaded_file);
    
    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      this.valdetailsform.get('rc_image')?.setValue( uploaded_file.Location);
      console.log(this.documentName)
      if (tab == 'rc_upload') {
        this.imageName = uploaded_file.Location;
        
      }


      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }
  async invoiceUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' + this.unique_control_number + "_" + "invoice_upload" + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
    console.log(uploaded_file);
    
    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      this.invoiceDocumentName.push({ name: fileFormat, url: uploaded_file.Location });
      console.log(this.invoiceDocumentName)
      this.valdetailsform.get('invoice_document')?.setValue( uploaded_file.Location);
      if (tab == 'invoice_document') {
        this.imageName = uploaded_file.Location;
        
      }



      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }
  getLocationData() {
    
    if(this.valdetailsform.get('country')?.value==1){
    let pincode = this.valdetailsform.get('pin')?.value;
    if (pincode && pincode.length > 5) {
      let queryParam = `pin_code__contains=${pincode}`;
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
          this.stateList = res.results;
          this.valdetailsform?.get('city')?.setValue(this.stateList[0].city?.name);
          
          this.valdetailsform?.get('pin_code')?.setValue(this.stateList[0].pin_code);
          this.valdetailsform?.get('state')?.setValue(this.stateList[0].city.state?.name);
          this.valdetailsform?.get('state_id')?.setValue(this.stateList[0].city.state?.id);
        },
        (err) => {
          console.error(err);
        }
      );
    }
  }
}
countryChange(){
  
    this.valdetailsform?.get('pin')?.setValue('');
    this.valdetailsform?.get('state')?.setValue('');
    this.valdetailsform?.get('city')?.setValue('');
  }
  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.valdetailsform.get('manu_year');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.valdetailsform?.get('manu_year')?.setValue(ctrlValue?.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }
  invoiceYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.valdetailsform.get('invoice_date');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.valdetailsform?.get('invoice_date')?.setValue(ctrlValue?.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }
  getFileName(keyname, str) {
    return str.split(keyname).pop();
  }
  deleteFile(key, imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.valdetailsform.get(key)?.setValue('');
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }
  closeDatePicker(elem: MatDatepicker<any>, e) {
    let _d  = e;
          this.yearOfManufacture = _d;
    elem.close();
  }
}

