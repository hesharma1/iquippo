import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurvaluationservicesComponent } from './ourvaluationservices.component';

describe('OurvaluationservicesComponent', () => {
  let component: OurvaluationservicesComponent;
  let fixture: ComponentFixture<OurvaluationservicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurvaluationservicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurvaluationservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
