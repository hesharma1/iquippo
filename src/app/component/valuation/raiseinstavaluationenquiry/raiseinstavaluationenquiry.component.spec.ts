import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaiseinstavaluationenquiryComponent } from './raiseinstavaluationenquiry.component';

describe('RaiseinstavaluationenquiryComponent', () => {
  let component: RaiseinstavaluationenquiryComponent;
  let fixture: ComponentFixture<RaiseinstavaluationenquiryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaiseinstavaluationenquiryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaiseinstavaluationenquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
