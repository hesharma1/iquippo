import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { ValuationService } from 'src/app/services/valuation.service';
const moment = _moment;
import { Moment } from 'moment';
import { DatePipe } from '@angular/common';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SigninComponent } from '../../signin/signin.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};
@Component({
  selector: 'app-raiseinstavaluationenquiry',
  templateUrl: './raiseinstavaluationenquiry.component.html',
  styleUrls: ['./raiseinstavaluationenquiry.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class RaiseinstavaluationenquiryComponent implements OnInit {

  raiseinstaform: FormGroup;
  todayDate = new Date();
  model_name;
  cognitoId;
  constructor(private router: Router, private route: ActivatedRoute,private storage:StorageDataService,private dialog: MatDialog,
    private valuationService: ValuationService,private datePipe: DatePipe,public notify: NotificationService,
    public apiService: UsedEquipmentService,private spinner: NgxSpinnerService,private formbuilder: FormBuilder) {
    this.raiseinstaform = this.formbuilder.group({
      brand: new FormControl(null, Validators.required),
      model: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      rc_number: new FormControl(null, [Validators.maxLength(25)]),
      engine_number: new FormControl(null, [Validators.minLength(2), Validators.maxLength(25)]),
      chassis_number: new FormControl(null, [Validators.minLength(2), Validators.maxLength(25)]),
      machine_sr_number: new FormControl(null, [Validators.minLength(2), Validators.maxLength(25)]),
      mfg_year: new FormControl(null),
      hmr_kmr: new FormControl(null, [Validators.pattern('[1-9][0-9]+'),Validators.minLength(1), Validators.maxLength(15), Validators.required]),
      is_invoice_available: new FormControl(1, Validators.required),
      is_equipment_accidentally_damaged: new FormControl(1, Validators.required),
      asset_condition: new FormControl('EX', Validators.required),
      valuation_asset: new FormControl(null),
      status: new FormControl(1),
      product_name:new FormControl(''),
      is_valuation: new FormControl(true)
    }, {
      validator: [numberonly('hmr_kmr')]
    })
  }
  unique_control_number;
  get_response: any;
  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.route.params.subscribe((params: Params): void => {
      this.unique_control_number = params['unique_control_number'];
      this.getDetail(this.unique_control_number);
    });

  }

  save_and_close() {
    if(!(this.raiseinstaform.value.rc_number || this.raiseinstaform.value.chassis_number || this.raiseinstaform.value.engine_number || this.raiseinstaform.value.machine_sr_number)){
      this.notify.error("Please enter either of Engine Number, Chassis Number or Serial Number")
      return;
    }
    if(!this.raiseinstaform.valid){
      this.raiseinstaform.markAllAsTouched();
      return;
    }
    let request = {
      status : 2
    }
    this.valuationService.putInstaVal(this.unique_control_number, request).subscribe((res: any) => {
      let assetId = res.valuation_asset.id;
      if (this.raiseinstaform.get('mfg_year')?.value != "") {
        let date = this.raiseinstaform.get('mfg_year')?.value;
        this.raiseinstaform.get('mfg_year')?.setValue(this.datePipe.transform(date, 'yyyy'));
      }
      this.raiseinstaform.get('product_name')?.setValue(this.model_name);
      this.raiseinstaform.get('is_invoice_available')?.setValue(this.raiseinstaform.get('is_invoice_available')?.value == '1'?true:false);
      this.raiseinstaform.get('is_equipment_accidentally_damaged')?.setValue(this.raiseinstaform.get('is_equipment_accidentally_damaged')?.value == '1'?true:false);  
      this.valuationService.putAssetId(this.raiseinstaform.value).subscribe((res: any) => {
        if (this.cognitoId) {
          let isAuthorise = true;
          if (isAuthorise) {
            let payload = {
              asset_id: assetId,
              cognito_id: this.cognitoId,
              source: 'Insta_valuation',
              payment_type: 2
            }
            this.apiService.validateBid(payload).pipe(finalize(() => {   })).subscribe(
              (data: any) => {
                if (data.redirect) {
                  if(data.valuation_fee == 0){
                    let request = {
                      transaction_id: data.id,
                      status: 3
                    }
                    this.valuationService.patchInstaVal(this.unique_control_number, request).subscribe(res => {
                      this.notify.success('Request Submitted Successfully!!', true);
                      if(this.storage.getSessionStorageData('isFromAdmin', false)){
                        sessionStorage.removeItem('isFromAdmin');
                        window.open("/admin-dashboard/val-request", '_self');
                      }else{
                      window.open("/customer/dashboard/insta-valuation-request", '_self');
                      }
                      //this.router.navigate(['/customer/dashboard/insta-valuation-request'], { queryParams: { reload: true } });
                    })
                  }else{
                    let paymentDetails = {
                      asset_id: assetId,
                      asset_type: 'Used',
                      case: 'Insta_valuation',
                      payment_type: 'Full'
                    }
                    this.storage.setSessionStorageData('paymentSession', true, false);
                    this.storage.setSessionStorageData('InstaValuation', window.btoa(JSON.stringify(data)), false);
                    this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
                    this.storage.setSessionStorageData('uniqueControlNumber',this.unique_control_number,false);
                    //this.router.navigate(['/customer/payment']);
                    window.open("/customer/payment", '_self');
                  }
                } else {
                  this.notify.error(data.message);
                }
              },
              err => {
                console.log(err);
              }
            )
          }
        } else {
          const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    //dialogRef.afterClosed().subscribe(result => {
    //    console.log(`Dialog result: ${result}`);
    //});
  

  dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = localStorage.getItem('cognitoId');
        }
      })
        }
      })
    })
  }
  getDetail(unc: string) {
    this.valuationService.getInstaVal(unc).subscribe((res: any) => {
      this.get_response = res;
        this.raiseinstaform.patchValue({
          brand: res.valuation_asset.brand.id,
          model: res.valuation_asset.model.id,
          category: res.valuation_asset.category.id,
          engine_number: res.valuation_asset.engine_number,
          chassis_number: res.valuation_asset.chassis_number,
          machine_sr_number: res.valuation_asset.machine_sr_number,
          valuation_asset: res.valuation_asset.id,
          hmr_kmr: parseInt(res.valuation_asset.hmr_kmr),
          rc_number: res.valuation_asset.rc_number?res.valuation_asset.rc_number:undefined,
          asset_condition: res.valuation_asset.asset_condition,
          is_invoice_available:res.valuation_asset.is_invoice_available?'1':'2',
          is_equipment_accidentally_damaged:res.valuation_asset.is_equipment_accidentally_damaged?'1':'2'
        })
        this.model_name =  (this.get_response.valuation_asset?.category?.display_name == 'Other'?
        this.get_response.valuation_asset?.other_category:this.get_response.valuation_asset?.category?.display_name) +' ' +
        (this.get_response.valuation_asset?.brand?.display_name == 'Other'?this.get_response.valuation_asset?.other_brand:
        this.get_response.valuation_asset.brand?.display_name) +' ' + (this.get_response.valuation_asset?.model?.name == 'Other'?
        this.get_response.valuation_asset?.other_model:this.get_response.valuation_asset?.model?.name);
      //this.model_name = this.get_response.valuation_asset?.category?.display_name + ' ' + this.get_response.valuation_asset?.brand?.display_name + ' ' + this.get_response.valuation_asset?.model?.name;
      if(this.get_response.valuation_asset.mfg_year){
        let date =  new Date();
        date.setFullYear(this.get_response.valuation_asset.mfg_year);
        this.raiseinstaform.get('mfg_year')?.setValue(date);
      }
    })
  }
  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.raiseinstaform.get('mfg_year');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.raiseinstaform?.get('mfg_year')?.setValue(ctrlValue?.value);
    datepicker.close();
  }
}
