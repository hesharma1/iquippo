import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipvalmadeeasyComponent } from './equipvalmadeeasy.component';

describe('EquipvalmadeeasyComponent', () => {
  let component: EquipvalmadeeasyComponent;
  let fixture: ComponentFixture<EquipvalmadeeasyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipvalmadeeasyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipvalmadeeasyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
