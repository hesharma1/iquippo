import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { UsedEquipementModel, sellEquipment } from 'src/app/models/sellEquipment.model';
import { CommonService } from 'src/app/services/common.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SigninComponent } from '../../signin/signin.component';

@Component({
  selector: 'app-assetvaluated',
  templateUrl: './assetvaluated.component.html',
  styleUrls: ['./assetvaluated.component.css']
})
export class AssetvaluatedComponent implements OnInit {

  public sellEquipmentForm: FormGroup;
  brandList: any;
  categoryList: any;
  message?: string;
  masterEquipmentData: UsedEquipementModel;
  modelList: any;
  cognitoId;
  categoryId: any;
  brandId: any;
  modalId: any;
  categoryData: any;
  brandData: any;
  modalData: any;
  constructor(
    private router: Router,
    private usedEquipmentService: UsedEquipmentService, private dialog: MatDialog,
    private commonService: CommonService, private spinner: NgxSpinnerService, private notificationservice: NotificationService,
    private storage: StorageDataService, public notify: NotificationService, private appRouteEnum: AppRouteEnum,
    private valuationService: ValuationService
  ) {
    this.sellEquipmentForm = new FormGroup({
      category: new FormControl('', Validators.required),
      other_category: new FormControl(''),
      brand: new FormControl('', Validators.required),
      other_brand: new FormControl(''),
      model: new FormControl('', Validators.required),
      other_model: new FormControl(''),
      is_equipment_registered: new FormControl('', Validators.required),
      rc_number: new FormControl(''),
    });
    this.masterEquipmentData = new UsedEquipementModel();
  }
  ngOnInit(): void {
    this.cognitoId = localStorage.getItem('cognitoId');
    this.getCategoryMasters();
    if(localStorage.getItem('InstaVal')){
      let InstaValDetails:any = localStorage.getItem('InstaVal');
      InstaValDetails = JSON.parse(InstaValDetails);
      localStorage.removeItem('InstaVal');
      this.sellEquipmentForm.patchValue({
        category: InstaValDetails.formValue.category,
        other_category: InstaValDetails.formValue.other_category,
        brand: InstaValDetails.formValue.brand,
        other_brand: InstaValDetails.formValue.other_brand,
        model: InstaValDetails.formValue.model,
        other_model: InstaValDetails.formValue.other_model,
        is_equipment_registered: InstaValDetails.formValue.is_equipment_registered,
        rc_number: InstaValDetails.formValue.rc_number,  
      })
      this.categoryId = InstaValDetails.category,
      this.brandId= InstaValDetails.brand,
      this.modalId =InstaValDetails.model
      if (this.categoryId != 0) {
        this.commonService.getBrandMasterByCatId(this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.brandList = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
          });
      }
      if (this.brandId != 0) {
        this.commonService.getModelMasterByBrandId(this.brandId,this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.modelList = res.results;
            this.modalData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    }
   
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      let rolesArray = this.storage.getStorageData("rolesArray", false);
      rolesArray = rolesArray.split(',');
      if (rolesArray.includes('Customer')) {
        let payload = {
          cognito_id: this.cognitoId,
          role: 'Customer'
        }
        this.commonService.validateSeller(payload).subscribe(
          (res: any) => {
            if (!res.kyc_verified || !res.pan_address_proof) {
              //this.notify.warn('Please submit KYC and Address proof', true);
              const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '500px',
                disableClose: true,
                data: {
                  heading: 'KYC Details Incomplete',
                  message: 'You need to complete your KYC profile with iQuippo to make payments.',
                  submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                }
              });

              dialogRef.afterClosed().subscribe(val => {
                if (val) {
                  let req = {
                    "formValue" : this.sellEquipmentForm.value,
                    "category" : this.categoryId,
                    "brand" : this.brandId,
                    "model" :  this.modalId
                  }
                  localStorage.setItem('InstaVal',JSON.stringify(req))
                  this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId,returnUrl:this.router.url} });
                }
              })
              resolved(false);
              return;
            } else {
              resolved(true);
            }
          },
          err => {
            console.log(err);
            reject(false);
          }
        );
      } else {
        this.notify.error('Please login as Customer to proceed further with asset valuation!!', false);
      }
    });
  }

  submit() {
    //localStorage.removeItem('sellequipmentData')
    if (this.cognitoId) {
      this.checkAuthentication().then((isAuthorise) => {
      if (isAuthorise) {
        if(localStorage.getItem('InstaVal')){
          localStorage.removeItem('InstaVal');
        }
        //this.spinner.show();
        this.sellEquipmentForm.value;

        var sellEquipmentRequest = {
          category: null,
          model: null,
          brand: null,
          rc_number: undefined,
          is_equipment_registered: false,
          seller: null,
          valuation_type: 1,
          other_category: undefined,
          other_brand: undefined,
          other_model: undefined
        }
        sellEquipmentRequest.category = this.categoryId;
        sellEquipmentRequest.model = this.modalId;
        sellEquipmentRequest.brand = this.brandId;
        if (this.sellEquipmentForm.get('is_equipment_registered')?.value == 1) {
          sellEquipmentRequest.rc_number = this.sellEquipmentForm.get('rc_number')?.value;
        }
        sellEquipmentRequest.is_equipment_registered = this.sellEquipmentForm.get('is_equipment_registered')?.value == 1 ? true : false;
        sellEquipmentRequest.seller = this.cognitoId;
        sellEquipmentRequest.valuation_type = 1;
        if (this.sellEquipmentForm.get('other_category')?.value !== '') {
          sellEquipmentRequest.other_category = this.sellEquipmentForm.get('other_category')?.value;
        }
        if (this.sellEquipmentForm.get('other_brand')?.value !== '') {
          sellEquipmentRequest.other_brand = this.sellEquipmentForm.get('other_brand')?.value;

        }
        if (this.sellEquipmentForm.get('other_model')?.value !== '') {
          sellEquipmentRequest.other_model = this.sellEquipmentForm.get('other_model')?.value;

        }
        sellEquipmentRequest;
        this.valuationService.postInstaVal(sellEquipmentRequest).subscribe((resp: any) => {

          this.router.navigate(['/valuation-request/raise-insta-val', resp.unique_control_number]);
        })



      
      }
    });
    } else {
      // const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      //   width: '500px',
      //   disableClose: true,
      //   data: {
      //     heading: 'User not logged in',
      //     message: 'Please login to continue',
      //     submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
      //   }
      // });
      const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      flag: true
    }
    const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
    //dialogRef.afterClosed().subscribe(result => {
    //    console.log(`Dialog result: ${result}`);
    //});
  

  dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = localStorage.getItem('cognitoId');
        }
      })
    }
    // this.usedEquipmentService.postUsedEquipment(sellEquipmentRequest).pipe(finalize(()=>{   })).subscribe(
    //   (res: any) => {
    //     this.router.navigate(['/valuation-request/raise-insta-val']);
    //     //this.masterDataModel.groupDetailDataSource?.push(this.masterDataModel.addGroupResponse);
    //   },
    //   err => {
    //     console.error(err);
    //     this.notificationservice.error(err);
    //   }
    // );
  }

  updateValidations() {
    if(this.sellEquipmentForm){
      if(this.sellEquipmentForm?.get('category')?.value == 'Other'){
        this.sellEquipmentForm.get('other_category')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('other_category')?.updateValueAndValidity();
      } else if (this.sellEquipmentForm.get('category')?.value != 'Other') {
        this.sellEquipmentForm.get('other_category')?.clearValidators();
        this.sellEquipmentForm.get('other_category')?.updateValueAndValidity();
        this.sellEquipmentForm.get('other_category')?.setValue('');
      }
      if (this.sellEquipmentForm.get('brand')?.value != 'Other') {
        this.sellEquipmentForm.get('other_brand')?.clearValidators();
        this.sellEquipmentForm.get('other_brand')?.updateValueAndValidity();
      } else if (this.sellEquipmentForm.get('brand')?.value == 'Other') {
        this.sellEquipmentForm.get('other_brand')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('other_brand')?.updateValueAndValidity();
      }
      if (this.sellEquipmentForm.get('model')?.value != 'Other') {
        this.sellEquipmentForm.get('other_model')?.clearValidators();
        this.sellEquipmentForm.get('other_model')?.updateValueAndValidity();
      } else if (this.sellEquipmentForm.get('model')?.value == 'Other') {
        this.sellEquipmentForm.get('other_model')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('other_model')?.updateValueAndValidity();
      }
      if (this.sellEquipmentForm.get('is_equipment_registered')?.value == '1') {
        this.sellEquipmentForm.get('rc_number')?.setValidators(Validators.required);
        this.sellEquipmentForm.get('rc_number')?.updateValueAndValidity();
      } else {
        this.sellEquipmentForm.get('rc_number')?.clearValidators();
        this.sellEquipmentForm.get('rc_number')?.updateValueAndValidity();
      }
    }
  }

  getCategoryMasters() {
    let queryparams = "limit=999"
    this.commonService.getCategoryMaster(queryparams).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      err => {
        console.error(err);
      });
  }
  trimSpaces(event){
    if(event.target.value)
event.target.value = event.target.value.trim()
  }
  filterValuesCategory(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }

  filterValuesBrand(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.brandData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }

  filterValuesModal(search: any) {
    if(!search.target.value) return search.target.value;
    search.target.value = search.target.value.replace(/^\s+/g, '');
    let data = search.target.value
    let filtered = this.modalData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }

  onModalSelect(modal: any) {
    this.modalId = modal.id;
    this.sellEquipmentForm.get('model')?.setValue(modal.name);
  }

  onCategorySelect(categoryId) {
    if (categoryId.id != this.categoryId) {
      this.sellEquipmentForm.get('model')?.setValue('');
      this.sellEquipmentForm.get('brand')?.setValue('');
      this.sellEquipmentForm.get('category')?.setValue(categoryId.display_name);
      this.categoryId = categoryId.id
      if (this.categoryId != 0) {
        this.commonService.getBrandMasterByCatId(this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.brandList = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.sellEquipmentForm.get('category')?.setValue(categoryId.display_name);
    }

  }

  onBrandSelect(brandId: any) {
    if (brandId.id != this.brandId) {
      this.brandId = brandId.id;
      this.sellEquipmentForm.get('model')?.setValue('');
      this.sellEquipmentForm.get('brand')?.setValue(brandId.display_name);
      if (this.brandId != 0) {
        this.commonService.getModelMasterByBrandId(brandId.id,this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.modelList = res.results;
            this.modalData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.sellEquipmentForm.get('brand')?.setValue(brandId.display_name);
    }
  }

  multipleUpload() {
    this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false } })
  }

}
