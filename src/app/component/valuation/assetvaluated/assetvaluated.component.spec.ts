import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetvaluatedComponent } from './assetvaluated.component';

describe('AssetvaluatedComponent', () => {
  let component: AssetvaluatedComponent;
  let fixture: ComponentFixture<AssetvaluatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssetvaluatedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetvaluatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
