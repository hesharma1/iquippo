import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurValuedPartnersComponent } from './our-valued-partners.component';

describe('OurValuedPartnersComponent', () => {
  let component: OurValuedPartnersComponent;
  let fixture: ComponentFixture<OurValuedPartnersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurValuedPartnersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurValuedPartnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
