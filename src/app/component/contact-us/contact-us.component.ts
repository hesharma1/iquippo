import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { String } from 'aws-sdk/clients/acm';
import { CountryResponse } from 'src/app/models/common/responsedata.model';
import { AccountService } from 'src/app/services/account';
import { CommonService } from 'src/app/services/common.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  formControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  loggedUserData: any;
  cognitoId?: String;
  countryList?: any;
  selectedCity?: CountryResponse;
  constructor(private cmnService: CommonService, public accountService: AccountService, private notificationservice: NotificationService,
    ) {
    this.contactUsForm = new FormGroup({
      full_name: new FormControl(null,Validators.required),
      email: new FormControl(null,Validators.email),
      country_code: new FormControl(null,Validators.required),
      location: new FormControl(null,Validators.required),
      comments: new FormControl(null,Validators.required),
      mobile_number: new FormControl(null,Validators.compose([Validators.pattern('[1-9+][0-9]+'), Validators.minLength(10), Validators.maxLength(13),Validators.required]))
    })
   }

  ngOnInit(): void {
    this.loggedUserData = localStorage.getItem('userData');
    this.loggedUserData = JSON.parse(this.loggedUserData);
    console.log(this.loggedUserData);
    this.bindFlags();
    
    let cogId = localStorage.getItem('cognitoId');
    if (cogId != '' && cogId != null) {
      this.cognitoId = cogId;
    }
    this.contactUsForm?.get('full_name')?.setValue(this.loggedUserData.attributes.given_name + this.loggedUserData.attributes.family_name);
    this.contactUsForm?.get('email')?.setValue(this.loggedUserData.attributes.email);
      let phone_number = this.loggedUserData.attributes.phone_number;
      phone_number = phone_number.substring(4,phone_number?.length)
    this.contactUsForm?.get('mobile_number')?.setValue(phone_number);
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(response => {
      var countries = response as any;
      this.countryList = countries.body;
      this.contactUsForm.get('country_code')?.setValue(this.countryList[0]);
      this.selectedCity = this.countryList[0];
    }, err => {
      this.notificationservice.error(err);
    })
  }

  updateCountryCode(city: any) {
    this.contactUsForm.get('country_code')?.setValue(city);
    this.contactUsForm.get('mobile_number')?.setValue('')
  }

  send(){
    this.contactUsForm.get('country_code')?.setValue(this.contactUsForm.get('country_code')?.value.prefixCode);
    this.cmnService.postContactUs(this.contactUsForm.value).subscribe((res):any=>{
      this.notificationservice.success('We will contact you soon!')
    }, err => {
      this.notificationservice.error(err);
    })
  }

}
