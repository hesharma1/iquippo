import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValuationService } from 'src/app/services/valuation.service';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css']
})
export class GenerateReportComponent implements OnInit {
  formControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  public newForm: FormGroup;
  reportdata;
  // mfg_year:string;
  // invoice_avail:string;
  // registration_status:string;
  // acc_dmg:string;
  // ucn: string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<GenerateReportComponent>, private valService: ValuationService,private fb: FormBuilder) {
    this.newForm = this.fb.group({
      valuation_min_amount: new FormControl('',Validators.required),
      valuation_max_amount: new FormControl('',Validators.required),
      range: new FormControl('',Validators.required),
      status: new FormControl(5)
    }, {
      validator: [numberonly('valuation_min_amount'),numberonly('valuation_max_amount')]})
    this.reportdata = data;
    // this.ucn = data.ucn;
    // this.mfg_year = data.mfg_year;
    // this.invoice_avail = data.is_original_invoice ? "Yes" : "No";
    // this.registration_status = data.is_rc_available ? "Registered":"Not Registered";
    // this.acc_dmg = data.is_equipment_accidentally_damaged ? "Yes" : "No";
  }

  ngOnInit(): void {
    
  }

  getwords(){
    if( this.newForm.get('valuation_min_amount')?.value && this.newForm.get('valuation_max_amount')?.value && this.newForm.get('range')?.value){
      return Number(this.newForm.get('valuation_min_amount')?.value) * Number(this.newForm.get('range')?.value) + ' - ' + Number(this.newForm.get('valuation_max_amount')?.value) * Number(this.newForm.get('range')?.value)
    }
    return "";
  }
  submit(){
    let request = {
      valuation_min_amount :  Number(this.newForm.get('valuation_min_amount')?.value) *  Number(this.newForm.get('range')?.value),
      valuation_max_amount :  Number(this.newForm.get('valuation_max_amount')?.value) *  Number(this.newForm.get('range')?.value),
      status : 5
    }
    this.valService.putInstaVal(this.reportdata.ucn,request).subscribe((res:any)=>{
      this.dialogRef.close('true');
    })
  }

  backClick(){
    this.dialogRef.close('false');
  }

  getAssetCondition(condition) {
    if (condition == 'GD') {
      return "Good"
    } else if (condition == 'EX') {
      return "Excellent"
    } else if (condition == 'AVG') {
      return "Average"
    } else if (condition == 'SCR') {
      return "Scrap"
    }
      else if (condition == 'POOR'){
        return "Poor"
      }
      else if (condition == 'VGD'){
        return "Very Good"
      }
    return "";
  }

  getMinValue(){
    return Number(this.newForm.get('valuation_min_amount')?.value) +1;
  }
  getMaxValue(){
    return Number(this.newForm.get('valuation_max_amount')?.value)
  }
  checkMinValue(){
    if(this.newForm.get('valuation_min_amount')?.value && this.newForm.get('valuation_max_amount')?.value){
      if(Number(this.newForm.get('valuation_min_amount')?.value) > Number(this.newForm.get('valuation_max_amount')?.value)){
        return true;
      } 
      return false; 
    }
    return false;
  }
}