import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-confirmation-delete-popup',
  templateUrl: './confirmation-delete-popup.component.html',
  styleUrls: ['./confirmation-delete-popup.component.css']
})
export class ConfirmationDeletePopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dialogRef.close(false);
  }

  accept(){
    this.dialogRef.close(true);
  }
  
}
