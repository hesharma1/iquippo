import { DatePipe } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ageCheck } from 'src/app/utility/validations/must-match-validation';
import { environment } from 'src/environments/environment';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from 'src/app/services/shared-service.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NgxSpinnerService } from 'ngx-spinner';
import { AgreementDetailsPopupComponent } from '../../partner-dealer-management/agreement-details/agreement-details-popup/agreement-details-popup.component';
import { ConfirmationDeletePopupComponent } from './confirmation-delete-popup/confirmation-delete-popup.component';
import { CommonService } from 'src/app/services/common.service';
import { MatSort } from '@angular/material/sort';
import { AuctionService } from 'src/app/services/auction.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-agreement',
  templateUrl: './client-agreement.component.html',
  styleUrls: ['./client-agreement.component.css']
})
export class ClientAgreementComponent implements OnInit {
  form?: FormGroup;
  multipleAgreementRef?: FormArray;
  dataSource1 = new MatTableDataSource<[]>();
  res: any;
  showForm: boolean = true;
  public searchText!: string;
  isDataDisplay: boolean = false;
  displayedColumns = ["id", "seller_first_name", "seller_mobile_number", "seller_email", "start_date", "end_date", "seller_location", "agreement_letter", "status", "Action"];
  displayedEsignColumns = ["id", "start_date", "agreement_letter", "status", "Action"];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild('agreement') agreement!: ElementRef;
  @ViewChild('work_order') work_order!: ElementRef;
  @ViewChild('input') input!: ElementRef;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  action: string = "";

  agreementStatus: any;
  // agreementTemplateUrl: string = '';
  agreementTemplateUrl: string = 'https://iquippodocuments.s3.ap-south-1.amazonaws.com/agreement.pdf';
  cognitoId: any;
  role;
  listingData: any = [];
  sellerData: any;
  sellerList: any;
  sellerId?: number;
  isForEsign: boolean = false;
  agreementType: Array<any> = [
    // { typeName: 'Bidder under taking', value: 1 },
    { typeName: 'Seller Agreement', value: 2 },
  ];
  agreementTypeValue: number = 2;
  //eSignReqData = new MatTableDataSource<[]>();
  eSignReqData: any[] = [];
  isESignData: boolean = false;
  auctionId: any;
  isSellerAgreementCreated: boolean = true;
  isAdmin: boolean = true;
  isDisplaySellerAgreement: boolean = true;
  public agreementsStatus: Array<any> = [
    { name: 'PENDING', value: '1' },
    { name: 'APPROVED', value: '2' },
    { name: 'EXPIRED', value: "3" },
    { name: 'REJECTED', value: '4' },
  ];
  agreementStatusFilter: any;
  isrm: any;

  constructor(private fb: FormBuilder, private datePipe: DatePipe, private s3: S3UploadDownloadService, private notify: NotificationService, public agreementService: AgreementServiceService, public sharedService: SharedService, private dialog: MatDialog, public app: ApiRouteService, public spinner: NgxSpinnerService, private commonService: CommonService, private router: Router, private apiRouteService: ApiRouteService, public auctionService: AuctionService, private activatedRoute: ActivatedRoute) {

    this.form = fb.group({
      start_date: [this.datePipe.transform(new Date(), 'yyyy-MM-dd'), Validators.required],
      end_date: [this.datePipe.transform(new Date().setFullYear(new Date().getFullYear() + 1), 'yyyy-MM-dd'), Validators.required],
      agreement_letter: ['', Validators.required],
      commission_value: [5],
      enableCost: [false],
      is_buyer_premium: [false],
      is_invoice_name_change_fee: [false],
      is_fixed_commission: [false],
      status: [1],
      seller: [{}, Validators.required],
      sellerSearchText: [""],
      sellerType: ["", Validators.required],
      id: [""],
      emd_to_iquippo: [true],
      full_payment_to_iquippo : [true],
      // partner_entity: [1],
      multipleAgreement: new FormArray([
        new FormGroup({
          work_order: new FormControl(''),
        })
      ])
    },
      { validator: [numberonly('commission_value')] }
    )
    this.agreementStatus = app.agreementStatus;
  }

  ngOnInit(): void {
    this.multipleAgreementRef = <FormArray>this.form?.controls['multipleAgreement'];
    this.role = localStorage.getItem("userRole");
    if (this.role != undefined && (this.role == this.apiRouteService.roles.superadmin || this.role == this.apiRouteService.roles.admin)) {
      this.isAdmin = false;
    }
    this.cognitoId = localStorage.getItem('cognitoId');
    // this.getAgreementTemplateUrl();
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['isForEsign'] == 'true') {
        this.isForEsign = true;
        this.agreementTypeValue = 1;
        this.getESignTemplate();
      }
      else {
        this.isForEsign = false;
        this.agreementTypeValue = 2;
      }
      if (params['auction']) {
        this.auctionId = params['auction'];
      }
      if (params['isrm']) {
        this.isrm = params['isrm'];
      }

    });
  }

  ngAfterViewInit() {
    if (this.agreementTypeValue == 2) {
      this.getonLoadAgreementDetails();
    }
  }

  addAgreement() {
    this.multipleAgreementRef?.push(
      new FormGroup({
        work_order: new FormControl('', [Validators.required]),
      })
    );
  }

  addNew() {
    this.showForm = !this.showForm;
    this.form?.get('commission_value')?.setValue('5');
    this.form?.get('sellerType')?.enable();
    this.form?.get('sellerSearchText')?.enable();
  }

  getSellerList() {
    var queryParams = 'limit=999';
    this.agreementService.getSellerList(queryParams).subscribe((res: any) => {
      this.sellerList = res.results;
    }, err => {

    })
  }

  removeAgreement(i: number) {
    this.multipleAgreementRef?.removeAt(i);
  }

  createUUID() {
    return uuidv4();
  }

  async handleUpload(event: any, name: string) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + name + '/' + uniqueFileName + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //console.log('type', file.type);
        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');
      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});

    }
  }

  async handleWorkUpload(event: any, name: string, i: number) {
    const file = event.target.files[0];
    var fileFormat = (file.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileType == 'svg' || file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      //  this.isInvalidfile=true;

      //  this.isShowPanDoc = false;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
      let uniqueFileName = this.cognitoId//this.createUUID();
      const reader = new FileReader();
      var image_path = environment.bucket_parent_folder + '/' + name + '/' + uniqueFileName + '.' + fileType;
      reader.readAsDataURL(file);
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Location != undefined && uploaded_file?.Location != '' && uploaded_file?.Location != null) {
        this.form?.get('multipleAgreement')?.get('' + i + '')?.get(name)?.setValue(uploaded_file?.Location);
      }
      else {
        this.notify.error('Upload Failed');
      }
      reader.onload = () => {

        //console.log('type', file.type);
        //  this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue(reader.result);
        // controlName=reader.result;
        //  setTimeout(() => {
        //   this.checkPanImageOnSubmit();
        //  },0);
      };
    } else {
      this.notify.error('Selected document should be image, pdf and word format');

      // this.isShowPanDoc = true;
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setValue('');
      // this.profileForm.get(tab)?.get('docImages')?.get(side)?.setErrors({imgIssue:true});

    }
  }

  agreementTypeChange(value) {
    if (value == 1) {
      this.getESignTemplate();
    }
    else if (value == 2) {
      this.getAgreementDetails();
    }
  }

  onSubmit() {
    let payload = { ...this.form?.getRawValue() };
    payload.work_order = [];
    if (payload.enableCost == true) {
      payload.is_fixed_commission = true;
      payload.commission_value = this.form?.get('commission_value')?.value;
    }
    else {
      payload.is_fixed_commission = false;
      payload.commission_value = this.form?.get('commission_value')?.value;
    }
    payload.start_date = this.datePipe.transform(payload.start_date, 'yyyy-MM-dd');
    payload.end_date = this.datePipe.transform(payload.end_date, 'yyyy-MM-dd');
    payload.multipleAgreement?.forEach(element => {
      payload.work_order.push(element.work_order);
    });

    if (payload.sellerType == 'Customer')
      payload.type = 1;
    if (payload.sellerType == 'Dealer')
      payload.type = 2;
    if (payload.sellerType == 'Manufacturer')
      payload.type = 2;

    payload.user = this.sellerId;

    if (payload.id == "" || payload.id == null) {
      this.agreementService.postAgreementDetails(payload).subscribe(res => {
        this.notify.success('User Agreement created Successfully')
        this.getAgreementDetails();
        this.resetForm();
        this.showForm = true;
      }, err => {
      })
    }
    else {
      this.agreementService.putAgreementDetails(payload).subscribe(res => {
        this.notify.success('User Agreement Updated Successfully')
        this.getAgreementDetails();
        this.resetForm();
        this.showForm = true;
      }, err => {
        console.log(err);
      })
    }
  }

  resetForm() {
    this.form?.reset();
    this.form?.patchValue({
      start_date: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      end_date: this.datePipe.transform(new Date().setFullYear(new Date().getFullYear() + 1), 'yyyy-MM-dd'),
      enableCost: false,
      is_buyer_premium: false,
      is_invoice_name_change_fee: false,
      status: 1,
      commission_value: 5,
      emd_to_iquippo: true,
      full_payment_to_iquippo: true
    })
    // this.form?.get('enableCost')?.setValue(false);
    // this.form?.get('is_buyer_premium')?.setValue(false);
    // this.form?.get('is_invoice_name_change_fee')?.setValue(false);
    // this.form?.get('status')?.setValue(1);
    // this.form?.get('commission_value')?.setValue(5);
  }

  actionPopup(agreementObj: any, action: string) {
    if (action == "view") {
      this.agreementService.getAgreementBankDetailsById(agreementObj.id).subscribe((res:any)=>{
        agreementObj.bankObj = res.bank_details;
        const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
          width: "60%",
          data: {
            agreementObj: agreementObj,
            isFromBidder: false
          }
        }).afterClosed()
          .subscribe(data => {
          })
      },err=>{
        console.log(err);
      })
    
    }
    else if (action == "edit") {
      this.showForm = false;
      if(agreementObj?.work_order.length){
        this.multipleAgreementRef?.controls.pop();
        agreementObj.work_order?.forEach(element => {
          this.multipleAgreementRef?.push(
            new FormGroup({
              work_order: new FormControl(element.doc),
            })
          );
        });
      }
      let type = '';
      if (agreementObj.type == '1')
        type = 'Customer';
      if (agreementObj.type == '2')
        type = 'Dealer';
      if (agreementObj.type == '3')
        type = 'Manufacturer';

      this.form?.patchValue({
        seller: agreementObj?.user_data,
        start_date: agreementObj.start_date,
        end_date: agreementObj.end_date,
        agreement_letter: agreementObj.agreement_letter,
        enableCost: agreementObj.is_fixed_commission,
        is_buyer_premium: agreementObj.is_buyer_premium,
        is_invoice_name_change_fee: agreementObj.is_invoice_name_change_fee,
        status: agreementObj.status,
        sellerType: type,
        commission_value: agreementObj.commission_value,
        sellerSearchText: agreementObj.user_data.first_name,
        id: agreementObj.id,
        user: agreementObj.user,
        emd_to_iquippo: agreementObj.emd_to_iquippo,
        full_payment_to_iquippo : agreementObj.full_payment_to_iquippo
      })
      // this.form?.get('seller')?.patchValue(agreementObj?.user_data);
      // this.form?.get('start_date')?.patchValue(agreementObj.start_date);
      // this.form?.get('end_date')?.patchValue(agreementObj.end_date);
      // this.form?.get('agreement_letter')?.patchValue(agreementObj.agreement_letter);
      // this.form?.get('enableCost')?.patchValue(agreementObj.is_fixed_commission);
      // this.form?.get('is_buyer_premium')?.patchValue(agreementObj.is_buyer_premium);
      // this.form?.get('is_invoice_name_change_fee')?.patchValue(agreementObj.is_invoice_name_change_fee);
      // this.form?.get('status')?.patchValue(agreementObj.status);
      // this.form?.get('sellerType')?.patchValue(type);
      // this.form?.get('commission_value')?.patchValue(agreementObj.commission_value);
      // this.form?.get('sellerSearchText')?.patchValue(agreementObj.user_data.first_name);
      // this.form?.get('id')?.patchValue(agreementObj.id);
      // this.form?.get('user')?.patchValue(agreementObj.user);

      this.sellerId = agreementObj.user;
      this.form?.get('sellerType')?.disable();
      this.form?.get('sellerSearchText')?.disable();
    }
    else {
      const dialogRef = this.dialog.open(ConfirmationDeletePopupComponent, {
        width: "50%",
        data: {}
      }).afterClosed()
        .subscribe(data => {
          if (data) {
            this.agreementService.deleteAgreementDetails(agreementObj).subscribe(res => {
              this.getAgreementDetails();
            })
          }
        }
      )
    }
  }

  isLessthanCurrentDate(enddate: any) {
    var currDate = new Date();
    let endDate = new Date(enddate);
    if (endDate < currDate) {
      return false;
    }
    else {
      return true
    }
  }

  viewBidderData(agreementObj) {
    const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
      width: "60%",
      data: {
        agreementObj: agreementObj,
        isFromBidder: true
      }
    }).afterClosed()
      .subscribe(data => {
      })
  }

  search() {
    let text = this.input.nativeElement.value;
    if (text === '' || text.length > 0) {
      this.getAgreementDetails();
    }
  }

  searchByStatus(val) {
    this.agreementStatusFilter = val;
    this.getAgreementDetails();
  }
  scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.res?.count > this.listingData?.length) && (this.listingData?.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.paginator.pageIndex++;
        this.getAgreementDetails();
        }
      }
      }
    }
  }

  getAgreementDetails() {
    let pageSize = this.paginator && this.paginator.pageSize ? this.paginator.pageSize : 10;
    let pageIndex = this.paginator && this.paginator.pageIndex ? (this.paginator.pageIndex + 1) : 1;
    let searchValue = this.input && this.input.nativeElement && this.input.nativeElement.value ? this.input.nativeElement.value : '';
    let agreementStatus = this.agreementStatusFilter ? '&status__in=' + this.agreementStatusFilter : '';
    let queryparams = "limit=" + pageSize + "&page=" + pageIndex + "&search=" + searchValue + "&ordering=" +
      (!this.sort ? '-id' : (this.sort && this.sort.direction == "asc") ? this.sort.active : "-" + this.sort.active)
      + agreementStatus;
    if (this.isrm != undefined && this.isrm != '') {
      queryparams += "&user__relationship_manager__user__cognito_id__in=" + this.cognitoId
    }
    this.agreementService.getAgreementDetails(queryparams).subscribe(res => {
      this.res = res as any;
     // Changes for Card layout on Mobile devices
if(window.innerWidth > 768){
  this.res = res as any;
  this.dataSource1 = this.res?.results;
  }else{
    this.scrolled = false;
    if(this.paginator.pageIndex == 1){
      // this.dataSource1 = [];
      this.listingData = [];
      this.listingData = this.res?.results;
      this.dataSource1 = this.listingData;
    }else{
      this.listingData = this.listingData.concat(this.res?.results);
      this.dataSource1 = this.listingData;
    }
  
  }

    }, err => {
      console.log(err);
    })
  }

  getonLoadAgreementDetails() {
    let pageSize = this.paginator && this.paginator.pageSize ? this.paginator.pageSize : 10;
    let pageIndex = this.paginator && this.paginator.pageIndex ? (this.paginator.pageIndex + 1) : 1;
    let searchValue = this.input && this.input.nativeElement && this.input.nativeElement.value ? this.input.nativeElement.value : '';
    let queryparams = "limit=" + pageSize + "&page=" + pageIndex + "&search=" + searchValue + "&ordering=" +
      (!this.sort ? '-id' : (this.sort && this.sort.direction == "asc") ? this.sort.active : "-" + this.sort.active);
    if (this.isrm != undefined && this.isrm != '') {
      queryparams += "&role=" + this.role + "&cognito_id=" + this.cognitoId;
    }
    this.agreementService.getAgreementDetails(queryparams).subscribe(res => {
      this.res = res as any;
      // Changes for Card layout on Mobile devices
      if (window.innerWidth > 768) {
        this.res = res as any;
        this.dataSource1 = this.res?.results;
      } else {
        if (this.paginator.pageIndex == 1) {
          // this.dataSource1 = [];
          this.listingData = [];
          this.listingData = this.res?.results;
          this.dataSource1 = this.listingData;
        } else {
          this.listingData = this.listingData.concat(this.res?.results);
          this.dataSource1 = this.listingData;
        }
      }

      if (this.res?.results && this.res?.results.length > 0) {
        this.isSellerAgreementCreated = false;
      }
      else {
        this.isSellerAgreementCreated = true;
      }
    }, err => {
      console.log(err);
    })
  }

  approve(item) {
    item.status = 2;
    let temp_workorder: string[] = [];
    item.work_order.forEach(element => {
      temp_workorder.push(element.doc);
    });
    item.work_order = temp_workorder;
    this.agreementService.putAgreementDetails(item).subscribe(res => {
      this.notify.success('Your status has been updated successfully.')
      this.getAgreementDetails()
    }, err => {
      this.getAgreementDetails();
    })
  }

  filterValuesSeller(search: any) {
    if (this.form?.get('sellerType')?.value != undefined && this.form?.get('sellerType')?.value != null && this.form?.get('sellerType')?.value != '') {
      let data = search.target.value.toLowerCase();
      // let filtered = this.sellerList;
      // let filteredData: any
      // filteredData = filtered.filter(
      //   item => item.first_name.toLowerCase().includes(data.toLowerCase())
      // );
      // if (filteredData.length > 0) {
      //   this.sellerList = filteredData
      // }
      let queryParams = 'agreement_search=true&limit=999&groups__name__in=' + this.form?.get('sellerType')?.value
      if (data.length > 0)
        queryParams += '&mobile_number__icontains=' + data

      if (this.isrm != undefined && this.isrm != '') {
        queryParams += "&relationship_manager__user__cognito_id__in=" + this.cognitoId;
      }
      this.agreementService.getSellerList(queryParams).subscribe((res: any) => {
        this.sellerList = res.results;
      }, err => {

      })
    }
    else {
      this.notify.warn("Please select Seller type.")
    }
  }

  deleteImg(url: string, index?: number) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        if (index != undefined) {
          this.form?.get('multipleAgreement')?.get('' + index + '')?.get("work_order")?.setValue('');
          this.work_order.nativeElement.value = "";
        }
        else
          this.form?.get('agreement_letter')?.setValue('');
        this.agreement.nativeElement.value = "";
      },
      err => {
        this.notify.error(err);
      }
    )
  }

  onSellerSelect(seller: any) {
    this.sellerId = seller.cognito_id;
    this.form?.get('seller')?.setValue(seller);
    // this.commonService.getModelMasterBySellerId(sellerId).subscribe(
    //   (res: any) => {
    //     this.sellerList = res.results;
    //     //this.modalData = res.results;
    //   },
    //   err => {
    //     console.error(err);
    //   });
  }

  reject(item) {
    item.status = 4;
    let temp_workorder: string[] = [];
    item.work_order.forEach(element => {
      temp_workorder.push(element.doc);
    });
    item.work_order = temp_workorder;
    this.agreementService.putAgreementDetails(item).subscribe(res => {
      this.notify.success('Your status has been updated successfully.')
      this.getAgreementDetails()
    })
  }

  // getAgreementTemplateUrl() {
  //   this.agreementService.getAgreemenTemplateUrl().subscribe((res: any) => {
  //     this.agreementTemplateUrl = res?.value;

  //   }, err => {
  //   })
  // }

  enableCost() {
    if (this.form?.get('enableCost')?.value) {
      this.form?.get('commission_value')?.setValue(5000)
    }
    else {
      this.form?.get('commission_value')?.setValue(5)
    }
  }

  downloadESignTemplate() {
    const params = {
      role_type: 1,
      bidder: this.cognitoId,
      //bidder: "00567af9-0df6-4571-b454-e34bc9a71271",
      auction: this.auctionId ? parseInt(this.auctionId) : 35

    }

    this.auctionService.postAuctionEsign(params).subscribe(
      (res: any) => {
        if (res.message) {
          this.notify.success(res.message);
          this.getESignTemplate();
        }
        else {
          this.eSignReqData = [];
          this.eSignReqData.push(res)
        }
      });
  }

  getESignTemplate() {
    const params = {
      bidder__cognito_id__in: this.cognitoId
      //bidder__cognito_id__in: "00567af9-0df6-4571-b454-e34bc9a71271",
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        if (res && res.results) {
          this.eSignReqData = res.results as any[];
        }
      });
  }

  getESignWidget(refId) {
    const params = {
      bidder__cognito_id__in: this.cognitoId
      //bidder__cognito_id__in: "00567af9-0df6-4571-b454-e34bc9a71271",
    }
    this.auctionService.getAuctionEsign(params).subscribe(
      (res: any) => {
        this.eSignReqData = res.results as any[];
        if (res.results && res.results[0] && res.results[0].status && res.results[0].status == 'EP') {
          this.patchESignReq(refId);
        }
        else if (res.results && res.results[0] && res.results[0].status && (res.results[0].status == 'INI' ||
          res.results[0].status == 'UNS' || res.results[0].status == 'TC')) {
          this.eSignDetailsConfirmation(refId);
          // this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
        }
      });
  }

  eSignDetailsConfirmation(url) {
    const dialogRef = this.dialog.open(ESignConfirmDetailsComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(val => {
      if (val == true) {
        //    this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
        window.open("/customer/dashboard/e-sign-template/" + url,
          '_blank');
      }
    })
  }

  onProceed(refId) {
    this.getESignWidget(refId);
  }

  patchESignReq(url) {
    const params = {
      reference_id: url,
      page_url: environment.mp1HomePage + 'customer/dashboard/e-sign-template/'
    }
    this.auctionService.patchAuctionEsign(params, url).subscribe(
      (res: any) => {
        if (res.message) {
          this.notify.error(res.message);
        }
        else {
          this.eSignDetailsConfirmation(url);
          //this.router.navigate(['/customer/dashboard/e-sign-template/' + url]);
        }
      });
  }

  deleteEsignRecord(id) {
    this.auctionService.deleteESignData(id).subscribe(
      res => {
        if (res == null) {
          this.notify.success('Data Deleted Successfully.');
          this.getESignTemplate();
        }
      });
  }
}


@Component({
  selector: 'esign-confirm-dialog.component',
  template: `
    <div class="popup">
      <div class="popup-hdr">
        Confirm
        <button mat-button (click)="close()" class="close-btn"><img src="../../../../../../assets/images/Close.svg" alt=""></button>
      </div>
      <div class="popup-body">
        <div>Bidder undertaking form e-signing is a one-time activity. 
          Please ensure that you have filled proper details before proceeding.</div> 
          <div><span>Click Yes to continue or click No to cancel</span></div>
          <div class="text-right dialog-actions-container">
            <button class="default-btn-outline plr-md-40 mr-3" [mat-dialog-close]="false" mat-button>No</button>
            <button class="default-btn plr-md-40" [mat-dialog-close]="true" mat-button >Yes</button>
          </div>
      </div>
    </div>`,
  styleUrls: ['./client-agreement.component.css']
})

export class ESignConfirmDetailsComponent {

  constructor(public dialogRef: MatDialogRef<ESignConfirmDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder) { }

  close() {
    this.dialogRef.close();
  }

}