import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { ViewReportComponent } from '../view-report/view-report.component';
import { SharedService } from 'src/app/services/shared-service.service';
import { DatePipe } from '@angular/common';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-valuation-request',
  templateUrl: './valuation-request.component.html',
  styleUrls: ['./valuation-request.component.css']
})
export class ValuationRequestComponent implements OnInit {@ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
@ViewChild(MatSort, { static: true }) sort!: MatSort;

public searchText!: string;
public activePage: any = 1;
public total_count: any;
public cognitoId?: string;
public dataSource!: MatTableDataSource<any>;
public ordering: string = "-created_at"
public pageSize: number = 10;
public filter: any;
public displayedColumns: string[] = [
  "unique_control_number","valuation_asset__seller__first_name","valuation_asset__seller__mobile_number","valuation_asset__seller__email",
  "valuation_asset__category__display_name","valuation_asset__brand__display_name","valuation_asset__model__name","valuation_asset__mfg_year",
  "valuation_asset__is_invoice_available","valuation_asset__is_equipment_registered","valuation_asset__rc_number","hmr_kmr","valuation_asset__is_equipment_accidentally_damaged",
  "valuation_asset__asset_condition", "status", "created_at", "actions"
];
public statusOptions: Array<any> = [
  { name: 'Request in Draft', value: 1 },
  { name: 'Request Submitted', value: 2 },
  { name: 'Payment Done', value: 3 },
  { name: 'Payment Failed', value: 4 },
  { name: 'Report Generated', value: 5 },
  { name: 'Invoice Generated', value: 6 }
];
public listingData = [];

constructor(private dialog: MatDialog,
  public spinner: NgxSpinnerService,
  public apiService: UsedEquipmentService,
  private router: Router,
  public valService: ValuationService,
  public storage: StorageDataService, public notify: NotificationService,
  private sharedService: SharedService,
  private datePipe: DatePipe) {
}

/**ng on init */
ngOnInit(): void {
  this.dataSource = new MatTableDataSource();
  this.cognitoId = this.storage.getStorageData('cognitoId', false);
  let isfrompayment =localStorage.getItem('isfrompayment');
     if(isfrompayment)
     {
      this.notify.success('Payment done successfully!!', true);
      localStorage.removeItem('isfrompayment');
     }
  this.getPage();
}
scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
        this.getPage();
        }
      }
    }
  }
}

/**get row status chnage */
getRowstatus(status) {
  if (status == '1') {
    return "Request in Draft"
  } else if (status == '2') {
    return "Request Submitted"
  } else if (status == '3') {
    return "Payment Done"
  } else if (status == '4') {
    return "Payment Cancelled"
  }
  else if (status == '5') {
    return "Report Generated"
  }else if (status == '6') {
    return "Invoice Generated"
  }
  return "";
}

getAssetCondition(condition) {
  if (condition == 'GD') {
    return "Good"
  } else if (condition == 'EX') {
    return "Excellent"
  } else if (condition == 'AVG') {
    return "Average"
  } else if (condition == 'SCR') {
    return "Scrap"
  }
    else if (condition == 'POOR'){
      return "Poor"
    }
    else if (condition == 'VGD'){
      return "Very Good"
    }
  return "";
}

/**search list  */
searchInList() {
  if (this.searchText === '' || this.searchText?.length > 3) {
    this.activePage = 1;
    this.getPage();
  }
}

statusFilter(val) {
  this.filter = val;
  this.activePage = 1;
  this.getPage();
}

/**get list from api */
getPage() {
  let payload = {
    page: this.activePage,
    limit: this.pageSize,
    ordering: this.ordering,
  };
  if (this.searchText) {
    payload['search'] = this.searchText;
  }
  if (this.filter) {
    payload['status__in'] = this.filter;
  }
  this.valService.getInstaValuation(payload).pipe(finalize(() => {  })).subscribe(
    (res: any) => {
      if (res) {
        this.scrolled = false;
        this.total_count = res.count;
       // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.dataSource.data = res.results;
        }else{
          if(this.activePage == 1){
            this.dataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.dataSource.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.dataSource.data = this.listingData;
          }
        } 
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

/**on chnage page from paginations */
onPageChange(event: PageEvent) {
  this.activePage = event.pageIndex + 1;
  this.pageSize = event.pageSize;
  this.getPage();
}

onSortColumn(event) {
  this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPage();
}

openviewpopup(row:any){
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.panelClass = 'my-class'; 
  dialogConfig.width = '900px';
  dialogConfig.data = {
    category:row.valuation_asset?.category.display_name,
    brand:row?.valuation_asset?.brand.display_name,
    model:row?.valuation_asset?.model.name,
    ucn: row.unique_control_number,
    mfg_year: row.valuation_asset.mfg_year,
    is_original_invoice: row.valuation_asset.is_original_invoice,
    is_equipment_registered: row.valuation_asset.is_equipment_registered,
    reg_number:row?.valuation_asset?.rc_number,
    asset_condition:row?.valuation_asset?.asset_condition,
    is_equipment_accidentally_damaged: row.valuation_asset.is_equipment_accidentally_damaged,
    valuation_amount: row.valuation_amount,
      valuation_min_amount: row.valuation_min_amount,
      valuation_max_amount: row.valuation_max_amount
  };
  const dialogRef = this.dialog.open(ViewReportComponent, dialogConfig);
}

opencallpopup(row:any){
  
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.panelClass = 'my-class'; 
  dialogConfig.width = '900px';
  dialogConfig.data = {
    category:row.valuation_asset?.category.display_name,
    brand:row?.valuation_asset?.brand.display_name,
    model:row?.valuation_asset?.model.name,
    ucn: row.unique_control_number,
    mfg_year: row.valuation_asset.mfg_year,
    is_original_invoice: row.valuation_asset.is_original_invoice,
    is_equipment_registered: row.valuation_asset.is_equipment_registered,
    reg_number:row?.valuation_asset?.rc_number,
    asset_condition:row?.valuation_asset?.asset_condition,
    is_equipment_accidentally_damaged: row.valuation_asset.is_equipment_accidentally_damaged
  };
  
  const dialogRef = this.dialog.open(GenerateReportComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(result => {
     console.log(`Dialog result: ${result}`);
     if(result == 'true'){
      // this.getPage();
      this.generateInvoice(row.unique_control_number,false);
     }
     
  });
}

edit(row) {
  this.storage.setSessionStorageData('isFromAdmin',true,false);
  this.router.navigate(['/valuation-request/raise-insta-val', row.unique_control_number]);
}
makePayment(row) {
  let payload = {
    asset_id: row.valuation_asset.id,
    cognito_id: this.cognitoId,
    source: 'Insta_valuation',
    payment_type: 2
  }
  this.apiService.validateBid(payload).pipe(finalize(() => { })).subscribe(
    (data: any) => {
      if (data.redirect) {
        if (data.valuation_fee == 0) {
          let request = {
            transaction_id: data.id,
            status: 3
          }
          this.valService.patchInstaVal(row.unique_control_number, request).subscribe(res => {
            this.notify.success('Request Submitted Successfully!!', true);
           // window.open("/customer/dashboard/insta-valuation-request", '_self');
            //this.router.navigate(['/customer/dashboard/insta-valuation-request'], { queryParams: { reload: true } });
          })
        } else {
          let paymentDetails = {
            asset_id: row.valuation_asset.id,
            asset_type: 'Used',
            case: 'Insta_valuation',
            payment_type: 'Full'
          }
          this.storage.setSessionStorageData('paymentSession', true, false);
          this.storage.setSessionStorageData('isFromAdmin',true,false);
          this.storage.setSessionStorageData('InstaValuation', window.btoa(JSON.stringify(data)), false);
          this.storage.setSessionStorageData('paymentDetails', window.btoa(JSON.stringify(paymentDetails)), false);
          this.storage.setSessionStorageData('uniqueControlNumber', row.unique_control_number, false);
          //this.router.navigate(['/customer/payment']);
          window.open("/customer/payment", '_self');
        }
      } else {
        this.notify.error(data.message);
      }
    },
    err => {
      console.log(err);
    }
  )
}

/**Export Function */
export(string) {
  let payload = {
    limit: 999
  }
  if(string != 'all'){
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
  }
  this.valService.getInstaValuation(payload).pipe(finalize(() => {  })).subscribe(
    (res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "UCN": r?.unique_control_number,
          "Name": r?.valuation_asset?.seller.first_name,
          "Mobile":r?.valuation_asset?.seller.mobile_number,
          "Email":r?.valuation_asset?.seller.email,
          "Category":r?.valuation_asset?.category.display_name == 'Other' ? r?.valuation_asset?.other_category: r?.valuation_asset?.category.display_name,
          "Brand": r?.valuation_asset?.brand.display_name == 'Other' ? r?.valuation_asset?.other_brand: r?.valuation_asset?.brand.display_name,
          "Model": r?.valuation_asset?.model.name == 'Other'? r?.valuation_asset?.other_model:r?.valuation_asset?.model.name,
          "Manufacturing Year": r?.valuation_asset?.mfg_year,
          "Invoice Availability":r?.valuation_asset?.is_original_invoice ? "Yes":"No",
          "Registration Status":r?.valuation_asset?.is_rc_available ? "Yes":"No",
          "Registration number":r?.valuation_asset?.rc_number,
          "HMR/KMR":r?.valuation_asset?.hmr_kmr,
          "Accident Update":r?.valuation_asset?.is_equipment_accidentally_damaged ? "Yes":"No",
          "Asset Condition":this.getAssetCondition(r?.valuation_asset?.asset_condition),
          "Status": this.getRowstatus(r?.status),
          "Created At": this.datePipe.transform(r?.created_at, 'dd-mm-yyyy, hh:mm a'),
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Insta Valuation List");
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}
generateInvoice(ucn,flag){
  let payload = {
    "unique_control_number":ucn,
    "type":2
  }
  this.valService.generateInvoice(payload).pipe(finalize(() => {  })).subscribe(
    (res: any) => {
      if(flag){
      this.notify.success('Invoice Generated Successfully!')
      this.getPage();
    }
      else {
        this.notify.success('Report and Invoice Generated Successfully!')
        this.getPage();
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
      this.getPage();
    }
  )
}
downloadInvoice(url : any){
  if(url != ''){
    this.sharedService.download(url);
  }
}
}
