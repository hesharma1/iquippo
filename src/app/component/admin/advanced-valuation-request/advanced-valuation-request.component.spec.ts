import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedValuationRequestComponent } from './advanced-valuation-request.component';

describe('AdvancedValuationRequestComponent', () => {
  let component: AdvancedValuationRequestComponent;
  let fixture: ComponentFixture<AdvancedValuationRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvancedValuationRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedValuationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
