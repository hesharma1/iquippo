import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {
  ImageCategory,
  AddBrandMappingRequest,
  AddBrandMappingResponse,
  AddCategoryMappingRequest,
  AddCategoryMappingResponse,
  AddGroupRequest,
  AddGroupResponse,
  AddModelRequest,
  AddModelResponse,
  MasterDataModel,
  statusType,
} from 'src/app/models/admin/new-equipment-master/master-data/master-data.model';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { CommonService } from './../../../../services/common.service';
import * as XLSX from 'xlsx';
import * as _moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import { SharedService } from 'src/app/services/shared-service.service';
import { DatePipe } from '@angular/common';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../location-master/confirm-dialog/confirm-dialog.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class MasterDataComponent implements OnInit {
  listingData = [];
  listingDataCat = [];
  listingDataBrand = [];
  listingDataModel = [];
  scrolled = false;
  scrolledCat =  false;
  scrolledBrand = false;
  scrolledModel = false;
  currentTab =0;
  //Group table column names
  tblGroupColums = ['id', 'name', 'description', 'Actions'];

  // Category mapping table column names
  tblCategoryMappingColumns = [
    'id',
    'category__display_name',
    'is_used',
    'group__name',
    'category__status',
    'Actions',
  ];

  // Brand Mapping table column names
  tblBrandMappingColumns = [
    'id',
    'brand__display_name',
    'category__display_name',
    'is_used',
    'group__name',
    'Actions',
  ];

  // Model mapping table column names
  tblModelColumns = [
    'id',
    'name',
    'brand__display_name',
    'category__display_name',
    'is_used',
    'group__name',
    'status',
    'Actions',
  ];

  modelStatusType: Array<any> = [
    // { typeName: 'REJECTED', value: 0 },
    { typeName: 'ACTIVE', value: 1 },
    { typeName: 'INACTIVE', value: 2 },
    // { typeName: 'EXPIRED', value: 3 },
  ];

  statusType: typeof
    statusType = statusType;
  modelStatus: number = 1;
  masterDataModel: MasterDataModel;
  breadCrumText: string = "Product Group";
  savedLocation = '';
  //isAboutModel = false;
  isValuationPartnerGrpIdVisible = false;
  isShowDate = false;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  isShowInvalidRecords: boolean = false;
  // Group form controls
  addNewGroupForm: any = this.formbuilder.group({
    groupName: new FormControl('', Validators.required),
    groupDesc: new FormControl(''),
    usedEquipment: new FormControl(''),
    newEquipment: new FormControl(''),
    usedPriority: new FormControl(''),
    newPriority: new FormControl(''),
    seoInfo: new FormControl(''),
    gSeoTitle: new FormControl(''),
    gSeoMetaKey: new FormControl(''),
    gSeoMetaDes: new FormControl(''),
  });

  // Category mapping form controls
  addNewCategoryForm: any = this.formbuilder.group({
    groupList: new FormControl('', Validators.required),
    categoryName: new FormControl('', Validators.required),
    categoryUsedEquip: new FormControl(''),
    categoryNewEquip: new FormControl(''),
    seo_info_cat: new FormControl(''),
    valuationPartnerGrpId: new FormControl('', [Validators.max(14), Validators.min(1)]),
    tcs_calculate: new FormControl(''),
    cSeoTitle: new FormControl(''),
    cSeoMetaKey: new FormControl(''),
    cSeoMetaDes: new FormControl(''),
  });

  // Brand Mapping form controls
  addNewBrandForm: any = this.formbuilder.group({
    brandGroupName: new FormControl('', Validators.required),
    brandCategoryName: new FormControl('', Validators.required),
    brandName: new FormControl('', Validators.required),
    brandNewEquip: new FormControl(''),
    brandUsedEquip: new FormControl(''),
    usedPriority: new FormControl(''),
    newPriority: new FormControl(''),
    seoInfo: new FormControl(''),
    bSeoTitle: new FormControl(''),
    bSeoMetaKey: new FormControl(''),
    bSeoMetaDes: new FormControl(''),
  });

  // Model form controls
  addNewModelForm: any = this.formbuilder.group({
    modelGroupName: new FormControl('', Validators.required),
    modelCategoryName: new FormControl('', Validators.required),
    modelUsedEquip: new FormControl(''),
    modelNewEquip: new FormControl(''),
    modelBrandName: new FormControl('', Validators.required),
    modelName: new FormControl('', Validators.required),
    aboutModel: new FormControl(''),
    modelStatus: new FormControl(''),
    capacityGroup: new FormControl(''),
    subCategory: new FormControl(''),
    modelCode: new FormControl(''),
    modelDesc: new FormControl(''),
    capacity: new FormControl(''),
    capacityUnit: new FormControl(''),
    riskEngineAssetID: new FormControl(''),
    mfgAssetID: new FormControl(''),
    isAttachmentApplicable: new FormControl('', Validators.required),
    isAttachment: new FormControl('', Validators.required),
    hsnCode: new FormControl(''),
    isDiscontinued: new FormControl('', Validators.required),
    yearLaunched: new FormControl(''),
    yearDiscontinued: new FormControl(''),
    isRegistrable: new FormControl('', Validators.required),
    assetRiskRating: new FormControl('', Validators.max(65535)),
    environmentalRating: new FormControl('', Validators.max(65535)),
    fuelCapacityLtr: new FormControl(''),
    isOffRoad: new FormControl('', Validators.required),
    safetyRating: new FormControl('', Validators.max(65535)),
    assetCost: new FormControl(''),
    imgURL: new FormControl('', Validators.pattern(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/)),
  });

  @ViewChild('groupPaginator') groupPaginator!: MatPaginator; //paginator used in group
  @ViewChild('categoryMappingPaginator') categoryMappingPaginator!: MatPaginator; //paginator used in category mapping
  @ViewChild('brandMappingPaginator') brandMappingPaginator!: MatPaginator; //paginator used in brand mapping

  @ViewChild('modelPaginator') modelPaginator!: MatPaginator; //paginator used in model
  @ViewChild('fileInputGroupImg') fileInputGroupImg!: ElementRef; //file inputs used in group
  @ViewChild('fileInputCategoryMap') fileInputCategoryMap!: ElementRef; //file inputs used in category mapping

  @ViewChild('fileInputBrndMapLogo') fileInputBrndMapLogo!: ElementRef; //file inputs used in brand mapping
  @ViewChild('fileInputBrndMapImg') fileInputBrndMapImg!: ElementRef; //file inputs used in brand mapping

  @ViewChild('fileInputModelImg') fileInputModelImg!: ElementRef; //file inputs used in model mapping

  @ViewChild('fileInputBulkInsert') fileInputBulkInsert!: ElementRef; //file inputs used in model bulk insert
  @ViewChild('fileInputBulkUpdate') fileInputBulkUpdate!: ElementRef; //file inputs used in model bulk update
  categoryList: any;
  brandCategories: any;
  modelCategories: any;
  categoryData: any;
  categoryId: any;
  brandList: any;
  brandData: any;
  brandId: any;
  modelList: any;
  modalData: any;
  groupData: any;
  selectedCategoryMapping: any;
  selectedBrandMappingCategory: any;
  selectedBrandMappingGroup: any;
  selectedBrandMappingBrand: any;
  modalMappingCategory: any;
  modalMappingBrand: any;
  modalMappingModal: any;
  groupImageName: string = '';
  modelImageName: string = '';
  groupImageUrl: any;
  modelImageUrl: string | undefined;
  yearDiscontinued: number = 0;
  yearLaunched: number = 0;
  isRegistrable: boolean = false;
  isAttachmentApplicable: boolean | undefined;
  isAttachment: boolean = false;
  isDiscontinued: boolean = false;
  isOffRoad: boolean = false;
  brandCategoryName: string = '';
  modelCategoryName: string = '';
  usedcheckValue: number = 0;
  newcheckValue: number = 0;
  public ordering: string = '-id';
  brandFilterList: any;
  ModelbrandName: string = '';
  selectedGroupMapping: any;
  modalMappingGroup: any;
  groupPageSize: any;
  catPageSize: any;
  modelPageSize: any;
  brandPageSize: any;
  brandFilterData: any;
  todayDate = new Date();
  bulkUploadData: any[] = [];
  errorRecordList: any[] = [];
  brandCategoriesData: any;
  modelCategoriesData: any;
  isShowGroupForm: boolean = false;
  isShowCategoryForm: boolean = false;
  isShowBrandForm: boolean = false;
  isShowModelForm: boolean = false;
  categoryImg: any;
  constructor(
    private formbuilder: FormBuilder,
    private adminMasterService: AdminMasterService,
    private apiPath: ApiRouteService,
    public s3: S3UploadDownloadService,
    private notify: NotificationService,
    private commonService: CommonService,
    private sharedService: SharedService,
    private datePipe: DatePipe,
    private dialog: MatDialog

  ) {
    this.masterDataModel = new MasterDataModel();
    this.masterDataModel.modelDetailList = new MatTableDataSource<any>([]);
  }

  // angular event
  ngOnInit(): void {
    this.isAttachmentApplicable = false;
    this.getGroupList('ordering=-id', ''); //for getting Group records
    this.getCategoryMappingList('ordering=-id', ''); //for getting Categry mapping records
    this.getBrandMappingList('ordering=-id', ''); //for getting Brand Mapping records
    this.getModelList('ordering=-id', ''); //for getting Model records
    this.getGroupMaster('limit=999'); //for getting all Group masters
    //this.getCategoryMaster(); //for getting Category master records
    this.getBrandMaster(); //for getting Brand master records
    this.getCategoryMasters();
  }

  // angular event
  ngAfterViewInit() {
    //for group pagination
    //this.masterDataModel.groupDetailList = new MatTableDataSource<any>([]);
    //this.masterDataModel.groupDetailList.paginator = this.groupPaginator;
    this.masterDataModel.groupDetailList = new MatTableDataSource<any>(
      []
    );
    this.masterDataModel.groupDetailList.paginator = this.categoryMappingPaginator;
    this.groupPaginator.page.subscribe((e: any) => {
      this.groupPageSize = e.pageSize;
      this.getGroupList(this.getQueryParams('Group', this.groupPageSize), '');
    });
    //for category mapping pagination
    this.masterDataModel.categoryMappingDetailList = new MatTableDataSource<any>(
      []
    );
    this.masterDataModel.categoryMappingDetailList.paginator = this.categoryMappingPaginator;
    this.categoryMappingPaginator.page.subscribe((e: any) => {
      this.catPageSize = e.pageSize;
      this.getCategoryMappingList(this.getQueryParams('Category', this.catPageSize), '');
    });

    //for brand mapping pagination
    this.masterDataModel.brandMappingDetailList = new MatTableDataSource<any>(
      []
    );
    this.masterDataModel.brandMappingDetailList.paginator = this.brandMappingPaginator;
    this.brandMappingPaginator.page.subscribe((e: any) => {
      this.brandPageSize = e.pageSize;
      this.getBrandMappingList(this.getQueryParams('Brand', this.brandPageSize), '');
    });

    //for model pagination
    this.masterDataModel.modelDetailList = new MatTableDataSource<any>([]);
    this.masterDataModel.modelDetailList.paginator = this.modelPaginator;
    this.modelPaginator.page.subscribe((e: any) => {
      this.modelPageSize = e.pageSize;
      this.getModelList(this.getQueryParams('Model', this.modelPageSize), '');
    });
  }

  getQueryParams(type: string, pageSize: any): string {
    let queryParam = '';
    if (type === 'Group')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.groupPaginator.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.groupPaginator.pageIndex + 1}&ordering=-id`;
      }
    else if (type === 'Category')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.categoryMappingPaginator.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.categoryMappingPaginator.pageIndex + 1}&ordering=-id`;
      }

    else if (type === 'Brand')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.brandMappingPaginator.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.brandMappingPaginator.pageIndex + 1}&ordering=-id`;
      }
    else if (type === 'Model')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.modelPaginator.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.modelPaginator.pageIndex + 1}&ordering=-id`;
      }

    return queryParam;
  }

  //for getting group data
  getGroupList(queryParams: string, value: string, toExport?: boolean) {
    this.adminMasterService
      .getMasterDataList(this.apiPath.groupMaster, queryParams)
      .subscribe(async (res: any) => {
        if (res != null) {
          // for(let result of res.results){
          //   if(result.image_url !='' || result.image_ur!= undefined|| result.image_ur!= null){
          //     result.image_url = await this.sharedService.viewImage(result.image_url);
          //   }
          // }
          if (toExport) {
            return this.exportGroupData(res.results);
          }
          this.groupPaginator.length = res.count;
          if(window.innerWidth > 768){
            this.masterDataModel.groupDetailList = new MatTableDataSource<any>(
              res.results
            );
            this.masterDataModel.groupDetailList.filterPredicate = (
              data: any,
              filter
            ) => {
              const dataStr = JSON.stringify(data).toLowerCase();
              return dataStr.indexOf(filter) != -1;
            };
            this.masterDataModel.groupDetailDataSource = res.results as Array<AddGroupResponse>;
            this.masterDataModel.groupDetailList.filter = value;
          }else{
            this.scrolled = false;
              if(this.groupPaginator.pageIndex == 0){
                this.listingData = [];
                this.listingData = res.results;
              }else{
                this.listingData = this.listingData.concat(res.results);
                //this.dataList.results = this.listingData;
              }
              this.masterDataModel.groupDetailList = new MatTableDataSource<any>(
                this.listingData
              );
              this.masterDataModel.groupDetailList.filterPredicate = (
                data: any,
                filter
              ) => {
                const dataStr = JSON.stringify(data).toLowerCase();
                return dataStr.indexOf(filter) != -1;
              };
              this.masterDataModel.groupDetailDataSource = this.listingData as Array<AddGroupResponse>;
              this.masterDataModel.groupDetailList.filter = value;
            }
        }
      });
  }


  loadCategoryByGroup(event, type) {
    let setValue = this.masterDataModel.groupMasterList.filter(x => event.option.value === x.name).map(x => x.id).join(",");
    let queryParams = 'limit=999&ordering=-id&group__id__in=' + setValue + '';
    this.adminMasterService
      .getMasterDataList(this.apiPath.categoryMappingMaster, queryParams)
      .subscribe(async (res: any) => {
        let arr = [];
        arr = res.results;
        this.brandCategories = [];
        this.modelCategories = [];
        this.brandCategoriesData = [];
        this.modelCategoriesData = [];
        this.brandCategoryName = '';
        this.modelCategoryName = '';
        if (type == 'BrandCategory') {
          arr.forEach((data: any) => {
            this.brandCategories.push(data.category);
          });
          arr.forEach((data: any) => {
            this.brandCategoriesData.push(data.category);
          });
        }
        else {
          arr.forEach((data: any) => {
            this.modelCategories.push(data.category);
          });
          arr.forEach((data: any) => {
            this.modelCategoriesData.push(data.category);
          });
        }
      });
  }

  //for getting category mapping data
  getCategoryMappingList(
    queryParams: string,
    value: string,
    toExport?: boolean
  ) {
    this.adminMasterService
      .getMasterDataList(this.apiPath.categoryMappingMaster, queryParams)
      .subscribe(async (res: any) => {
        if (res != null) {
          if (toExport) {
            return this.exportCategoryMappingData(res.results);
          }
          this.categoryMappingPaginator.length = res.count;
          if(window.innerWidth > 768){
            this.masterDataModel.categoryMappingDetailList = new MatTableDataSource<any>(
              res.results
            );
            this.masterDataModel.categoryMappingDetailList.filterPredicate = (
              data: any,
              filter
            ) => {
              const dataStr = JSON.stringify(data).toLowerCase();
              return dataStr.indexOf(filter) != -1;
            };
            this.masterDataModel.categoryMappingDetailDataSource = res.results as Array<AddCategoryMappingResponse>;
            
            this.masterDataModel.categoryMappingDetailList.filter = value;
          }else{
            this.scrolledCat = false;
              if(this.categoryMappingPaginator.pageIndex == 0){
                this.listingDataCat = [];
                this.listingDataCat = res.results;
              }else{
                this.listingDataCat = this.listingDataCat.concat(res.results);
                //this.dataList.results = this.listingData;
              }
              this.masterDataModel.categoryMappingDetailList = new MatTableDataSource<any>(
                this.listingDataCat
              );
              this.masterDataModel.categoryMappingDetailList.filterPredicate = (
                data: any,
                filter
              ) => {
                const dataStr = JSON.stringify(data).toLowerCase();
                return dataStr.indexOf(filter) != -1;
              };
              this.masterDataModel.categoryMappingDetailDataSource = this.listingDataCat as Array<AddCategoryMappingResponse>;
              
              this.masterDataModel.categoryMappingDetailList.filter = value;
            }
        }
      });
  }

  //for getting brand mapping data
  getBrandMappingList(queryParams: string, value: string, toExport?: boolean) {
    this.adminMasterService
      .getMasterDataList(this.apiPath.brandMappingMaster, queryParams)
      .subscribe(async (res: any) => {
        if (res != null) {
          if (toExport) {
            return this.exportBrandMappingData(res.results);
          }
          
          this.brandMappingPaginator.length = res.count;
          if(window.innerWidth > 768){
            this.masterDataModel.brandMappingDetailList = new MatTableDataSource<any>(
              res.results
            );
            this.masterDataModel.brandMappingDetailList.filterPredicate = (
              data: any,
              filter
            ) => {
              const dataStr = JSON.stringify(data).toLowerCase();
              return dataStr.indexOf(filter) != -1;
            };
            this.masterDataModel.brandMappingDetailDataSource = res.results as Array<AddBrandMappingResponse>;
            this.masterDataModel.brandMappingDetailList.filter = value;
          }else{
            this.scrolledBrand = false;
              if(this.brandMappingPaginator.pageIndex == 0){
                this.listingDataBrand = [];
                this.listingDataBrand = res.results;
              }else{
                this.listingDataBrand = this.listingDataBrand.concat(res.results);
                //this.dataList.results = this.listingData;
              }
              this.masterDataModel.brandMappingDetailList = new MatTableDataSource<any>(
                this.listingDataBrand
              );
              this.masterDataModel.brandMappingDetailList.filterPredicate = (
                data: any,
                filter
              ) => {
                const dataStr = JSON.stringify(data).toLowerCase();
                return dataStr.indexOf(filter) != -1;
              };
              this.masterDataModel.brandMappingDetailDataSource =  this.listingDataBrand as Array<AddBrandMappingResponse>;
              this.masterDataModel.brandMappingDetailList.filter = value;
            }
        }
      });
  }

  //for getting model data
  getModelList(queryParams: string, value: string, toExport?: boolean) {
    this.adminMasterService
      .getMasterDataList(this.apiPath.modelMaster, queryParams)
      .subscribe(async (res: any) => {
        if (res != null) {
          if (toExport) {
            return this.exportModelMappingData(res.results);
          }
          // for(let result of res.results){
          //   if(result.image_url !='' || result.image_ur!= undefined|| result.image_ur!= null || !result.image_url.includes('s3.ap-south-1')){
          //     result.image_url = await this.sharedService.viewImage(result.image_url);
          //   }
          // }
          this.modelPaginator.length = res.count;
          if(window.innerWidth > 768){
            
          this.masterDataModel.modelDetailList = new MatTableDataSource<any>(
            res.results
          );
          this.masterDataModel.modelDetailList.filterPredicate = (
            data: any,
            filter
          ) => {
            const dataStr = JSON.stringify(data).toLowerCase();
            return dataStr.indexOf(filter) != -1;
          };
          this.masterDataModel.modelDetailDataSource = res.results as Array<AddModelResponse>;
         
          this.masterDataModel.modelDetailList.filter = value;
          }else{
            this.scrolledModel = false;
              if(this.modelPaginator.pageIndex == 0){
                this.listingDataModel = [];
                this.listingDataModel = res.results;
              }else{
                this.listingDataModel = this.listingDataModel.concat(res.results);
                //this.dataList.results = this.listingData;
              }
              this.masterDataModel.modelDetailList = new MatTableDataSource<any>(
                this.listingDataModel
              );
              this.masterDataModel.modelDetailList.filterPredicate = (
                data: any,
                filter
              ) => {
                const dataStr = JSON.stringify(data).toLowerCase();
                return dataStr.indexOf(filter) != -1;
              };
              this.masterDataModel.modelDetailDataSource = this.listingDataModel as Array<AddModelResponse>;
             
              this.masterDataModel.modelDetailList.filter = value;
            }
        }
      });
  }

  //for getting all group data
  getGroupMaster(queryParams: string) {
    this.adminMasterService
      .getMasterDataList(this.apiPath.groupMaster, queryParams)
      .subscribe((res: any) => {
        if (res != null) {
          this.masterDataModel.groupMasterList = res.results as any;
          this.groupData = res.results;
        }
      });
  }

  filterValuesGroup(search: any) {
    let data = search.target.value;
    let filtered = this.groupData;
    let filteredData: any;
    if (filtered) {
      filteredData = filtered.filter((item) =>
        item.name.toLowerCase().includes(data.toLowerCase())
      );
      if (filteredData.length > 0) {
        this.masterDataModel.groupMasterList = filteredData;
      }
    }
  }


  filterValuesCategory(search: any, type) {
    if (type === 'CatMap') {
      let data = search.target.value;
      let filtered = this.categoryData;
      let filteredData: any;
      if (filtered) {
        filteredData = filtered.filter(x => x.display_name.toLowerCase().includes(data.toLowerCase()));
        if (filteredData.length > 0) {
          this.categoryList = filteredData;
        }
      }
    }
    else if (type === 'BrandMap') {
      let data = search.target.value;
      let filtered = this.brandCategoriesData;
      let filteredData: any;
      if (filtered) {
        filteredData = filtered.filter(x => x.display_name.toLowerCase().includes(data.toLowerCase()));
        if (filteredData.length > 0) {
          this.brandCategories = filteredData;
        }
      }
    }
    else {
      let data = search.target.value;
      let filtered = this.modelCategoriesData;
      let filteredData: any;
      if (filtered) {
        filteredData = filtered.filter(x => x.display_name.toLowerCase().includes(data.toLowerCase()));
        if (filteredData.length > 0) {
          this.modelCategories = filteredData;
        }
      }
    }
  }

  onCategoryProductMapping(event) {
    this.selectedCategoryMapping = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
  }

  onGroupProductMapping(event) {
    this.selectedGroupMapping = this.masterDataModel.groupMasterList.filter(x => event.option.value === x.name).map(x => x.id).join(",");
  }

  onBrandMappingGroup(event) {
    this.selectedBrandMappingGroup = this.masterDataModel.groupMasterList.filter(x => event.option.value === x.name).map(x => x.id).join(",");
  }

  onBrandMappingCategory(event) {
    this.selectedBrandMappingCategory = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
  }

  onBrandMappingBrand(event) {
    this.selectedBrandMappingBrand = this.brandList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
  }
  onModalMappingCategory(event) {
    this.modalMappingCategory = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
  }

  onModelMappingGroup(event) {
    this.modalMappingGroup = this.masterDataModel.groupMasterList.filter(x => event.option.value === x.name).map(x => x.id).join(",");
  }

  onModalMappingBrand(event) {
    this.modalMappingBrand = this.brandList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
  }

  onModalSelect(id) {
    this.modalMappingModal = id;
  }

  filterValuesBrand(search: any, type) {
    if (type === 'Brand') {
      let data = search.target.value;
      let filtered = this.brandData;
      let filteredData: any;
      if (filtered) {
        filteredData = filtered.filter((item) =>
          item.display_name.toLowerCase().includes(data.toLowerCase())
        );
        if (filteredData.length > 0) {
          this.brandList = filteredData;
        }
      }
    }
    else {
      let data = search.target.value;
      let filtered = this.brandFilterData;
      let filteredData: any;
      if (filtered) {
        filteredData = filtered.filter((item) =>
          item.display_name.toLowerCase().includes(data.toLowerCase())
        );
        if (filteredData.length > 0) {
          this.brandFilterList = filteredData;
        }
      }



    }
  }

  getCategoryMasters() {
    let queryparams = 'limit=999&ordering=-id';
    this.commonService.getCategoryMaster(queryparams).subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  onCategorySelect(event) {
    this.categoryId = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
    this.commonService.getBrandMaster('').subscribe(
      (res: any) => {
        this.brandList = res.results;
        this.brandData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  onTabChange(event: any) {
    this.breadCrumText = event.tab.textLabel;
    this.notify.clear();
    this.currentTab = event?.index;
  }

  filterBrandByCategory(event) {
    this.categoryId = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
    this.commonService.getBrandMasterByCatId(this.categoryId).subscribe(
      (res: any) => {
        this.brandFilterList = res.results;
        this.brandFilterData = res.results;
        this.ModelbrandName = '';
      },
      (err) => {
        console.error(err);
      }
    );
  }

  onBrandSelect(event) {
    this.brandId = this.brandList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");;
    this.commonService.getModelMasterByBrandId(this.brandId,this.categoryId).subscribe(
      (res: any) => {
        this.modelList = res.results;
        this.modalData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  filterValuesModal(search: any) {
    let data = search.target.value;
    let filtered = this.modalData;
    let filteredData: any;
    if (filtered) {
      filteredData = filtered.filter((item) =>
        item.name.toLowerCase().includes(data.toLowerCase())
      );
      if (filteredData.length > 0) {
        this.modelList = filteredData;
      }
    }
  }

  // //for getting category master data
  // getCategoryMaster() {
  //   this.adminMasterService.getCategoryMaster('').subscribe(
  //     (res: any) => {
  //       if (res != null) {
  //         this.masterDataModel.categoryMasterList = res.results as any;
  //       }
  //     });
  // }

  //for getting brand master data
  getBrandMaster() {
    this.adminMasterService.getBrandMaster('limit=999&ordering=-id&status__in=1').subscribe((res: any) => {
      if (res != null) {
        this.masterDataModel.brandMasterList = res.results as any;
        this.brandList = res.results as any;
        this.brandData = res.results as any;
      }
    });
  }

  onCheckboxSearch(isChecked: boolean, type: string) {

    if (type === 'UsedEquipment') {
      if (isChecked) {
        this.usedcheckValue = 1;
      }
      else {
        this.usedcheckValue = 0;
      }
    }
    if (type === 'NewEquipment') {
      if (isChecked) {
        this.newcheckValue = 1;
      }
      else {
        this.newcheckValue = 0;
      }
    }
    if (this.usedcheckValue == 0 && this.newcheckValue == 0) {
      this.getCategoryMappingList('limit=999&ordering=-id&search=', '');
    }
    else {
      this.getCategoryMappingList('limit=999&ordering=-id&is_new=' + this.newcheckValue + '&is_used=' + this.usedcheckValue + '', '');
    }

  }

  //common function for handling different checkbox events
  onCheckBoxChange(isChecked: boolean, type: string) {

    if (type === 'Group') {
      if (isChecked) {
        this.masterDataModel.addSEOInfoGroup = true;
        this.addNewGroupForm.patchValue({
          gSeoTitle: '',
          gSeoMetaKey: '',
          gSeoMetaDes: '',
        });
        this.addNewGroupForm
          .get('gSeoTitle')
          .setValidators([Validators.required]);
        this.addNewGroupForm.get('gSeoTitle').updateValueAndValidity();
        this.addNewGroupForm
          .get('gSeoMetaKey')
          .setValidators([Validators.required]);
        this.addNewGroupForm.get('gSeoMetaKey').updateValueAndValidity();
        this.addNewGroupForm
          .get('gSeoMetaDes')
          .setValidators([Validators.required]);
        this.addNewGroupForm.get('gSeoMetaDes').updateValueAndValidity();
      } else {
        this.masterDataModel.addSEOInfoGroup = false;
        this.addNewGroupForm.patchValue({
          gSeoTitle: '',
          gSeoMetaKey: '',
          gSeoMetaDes: '',
        });
        this.addNewGroupForm.get('gSeoTitle').clearValidators();
        this.addNewGroupForm.get('gSeoTitle').updateValueAndValidity();
        this.addNewGroupForm.get('gSeoMetaKey').clearValidators();
        this.addNewGroupForm.get('gSeoMetaKey').updateValueAndValidity();
        this.addNewGroupForm.get('gSeoMetaDes').clearValidators();
        this.addNewGroupForm.get('gSeoMetaDes').updateValueAndValidity();
      }
    }

    if (type === 'Category') {
      if (isChecked) {
        this.masterDataModel.addSEOInfoCatMap = true;
        this.addNewCategoryForm.patchValue({
          cSeoTitle: '',
          cSeoMetaKey: '',
          cSeoMetaDes: '',
        });
        this.addNewCategoryForm
          .get('cSeoTitle')
          .setValidators([Validators.required]);
        this.addNewCategoryForm.get('cSeoTitle').updateValueAndValidity();
        this.addNewCategoryForm
          .get('cSeoMetaKey')
          .setValidators([Validators.required]);
        this.addNewCategoryForm.get('cSeoMetaKey').updateValueAndValidity();
        this.addNewCategoryForm
          .get('cSeoMetaDes')
          .setValidators([Validators.required]);
        this.addNewCategoryForm.get('cSeoMetaDes').updateValueAndValidity();
      } else {

        this.masterDataModel.addSEOInfoCatMap = false;
        this.addNewCategoryForm.patchValue({
          cSeoTitle: '',
          cSeoMetaKey: '',
          cSeoMetaDes: '',
        });
        this.addNewCategoryForm.get('cSeoTitle').clearValidators();
        this.addNewCategoryForm.get('cSeoTitle').updateValueAndValidity();
        this.addNewCategoryForm.get('cSeoMetaKey').clearValidators();
        this.addNewCategoryForm.get('cSeoMetaKey').updateValueAndValidity();
        this.addNewCategoryForm.get('cSeoMetaDes').clearValidators();
        this.addNewCategoryForm.get('cSeoMetaDes').updateValueAndValidity();
      }
    }

    if (type === 'Brand') {
      if (isChecked) {
        this.masterDataModel.addSEOInfoBrndMap = true;
        this.addNewBrandForm.patchValue({
          bSeoTitle: '',
          bSeoMetaKey: '',
          bSeoMetaDes: '',
        });
        this.addNewBrandForm
          .get('bSeoTitle')
          .setValidators([Validators.required]);
        this.addNewBrandForm.get('bSeoTitle').updateValueAndValidity();
        this.addNewBrandForm
          .get('bSeoMetaKey')
          .setValidators([Validators.required]);
        this.addNewBrandForm.get('bSeoMetaKey').updateValueAndValidity();
        this.addNewBrandForm
          .get('bSeoMetaDes')
          .setValidators([Validators.required]);
        this.addNewBrandForm.get('bSeoMetaDes').updateValueAndValidity();
      }
      else {
        this.masterDataModel.addSEOInfoBrndMap = false;
        this.addNewBrandForm.patchValue({
          bSeoTitle: '',
          bSeoMetaKey: '',
          bSeoMetaDes: '',
        });
        this.addNewBrandForm.get('bSeoTitle').clearValidators();
        this.addNewBrandForm.get('bSeoTitle').updateValueAndValidity();
        this.addNewBrandForm.get('bSeoMetaKey').clearValidators();
        this.addNewBrandForm.get('bSeoMetaKey').updateValueAndValidity();
        this.addNewBrandForm.get('bSeoMetaDes').clearValidators();
        this.addNewBrandForm.get('bSeoMetaDes').updateValueAndValidity();
      }
    }

    /*if (type === 'Model') {
      if (isChecked) {
        this.isAboutModel = true;
        this.addNewModelForm.patchValue({
          aboutModel: '',
        });
      } else {
        this.isAboutModel = false;
        this.addNewModelForm.patchValue({
          aboutModel: '',
        });
      }
    }*/

    if (type === 'ValuationPartnerGroupID') {
      if (isChecked) {
        this.isValuationPartnerGrpIdVisible = true;
        this.addNewCategoryForm.patchValue({
          valuationPartnerGrpId: '',
        });
      } else {
        this.isValuationPartnerGrpIdVisible = false;
        this.addNewCategoryForm.patchValue({
          valuationPartnerGrpId: '',
        });
      }
    }
  }

  onSortColumn(type, event) {
    if (type === 'Group') {
      let sorting = (event.direction == "asc") ? event.active : ("-" + event.active);
      this.ordering = 'limit=' + this.groupPageSize + '&ordering=' + sorting;
      this.getGroupList(this.ordering, '');
    }
    if (type === 'Category') {
      let sorting = (event.direction == "asc") ? event.active : ("-" + event.active);
      this.ordering = 'limit=' + this.catPageSize + '&ordering=' + sorting;
      this.getCategoryMappingList(this.ordering, '');
    }
    if (type === 'Brand') {
      let sorting = (event.direction == "asc") ? event.active : ("-" + event.active);
      this.ordering = 'limit=' + this.brandPageSize + '&ordering=' + sorting;
      this.getBrandMappingList(this.ordering, '');
    }
    if (type === 'Model') {
      let sorting = (event.direction == "asc") ? event.active : ("-" + event.active);
      this.ordering = 'limit=' + this.modelPageSize + '&ordering=' + sorting;
      this.getModelList(this.ordering, '');
    }
  }

  //common function for save/update data
  saveData(type: string) {
    if (type === 'Group') {
      //for group
      this.addGroup();
    } else if (type === 'Category') {
      //for category mapping
      this.addCategoryMapping();
    } else if (type === 'Brand') {
      //for brand mapping
      this.addBrand();
    } else if (type === 'Model') {
      //for model
      this.addModel(null);
    }
  }

  // add/update group data
  addGroup() {
    this.addNewGroupForm.markAllAsTouched();
    if (this.addNewGroupForm.valid) {
      const formData = this.addNewGroupForm.value;
      if (formData.usedEquipment || formData.newEquipment) {
        let fileName: string = 'Group_' + Math.random();
        this.imageUpload(
          this.addNewGroupForm,
          this.fileInputGroupImg,
          'MasterData',
          fileName
        ).then((res) => {
          this.masterDataModel.addGroupRequest = new AddGroupRequest();
          this.masterDataModel.addGroupRequest.name = formData.groupName;
          this.masterDataModel.addGroupRequest.description = formData.groupDesc;
          this.masterDataModel.addGroupRequest.is_used =
            formData.usedEquipment == null || formData.usedEquipment === ''
              ? false
              : formData.usedEquipment;
          this.masterDataModel.addGroupRequest.is_new =
            formData.newEquipment == null || formData.newEquipment === ''
              ? false
              : formData.newEquipment;
          this.masterDataModel.addGroupRequest.priority_for_use =
            formData.usedPriority == null || formData.usedPriority === ''
              ? 0
              : formData.usedPriority;
          this.masterDataModel.addGroupRequest.priority_for_new =
            formData.newPriority == null || formData.newPriority === ''
              ? 0
              : formData.newPriority;
          this.masterDataModel.addGroupRequest.add_seo =
            formData.seoInfo == null || formData.seoInfo === ''
              ? false
              : formData.seoInfo;
          this.masterDataModel.addGroupRequest.seo_title = formData.gSeoTitle;
          this.masterDataModel.addGroupRequest.seo_meta_keywords =
            formData.gSeoMetaKey;
          this.masterDataModel.addGroupRequest.seo_meta_desc =
            formData.gSeoMetaDes;
          this.masterDataModel.addGroupRequest.image_url = this.savedLocation ? this.savedLocation : this.groupImageUrl ? this.groupImageUrl : '';
          this.adminMasterService
            .createUpdateMasterData(
              this.masterDataModel.addGroupRequest,
              this.apiPath.groupMaster,
              this.masterDataModel.actionNameGroup,
              this.masterDataModel.id
            )
            .subscribe((res) => {
              if (res != null) {
                this.masterDataModel.addGroupResponse = res as AddGroupResponse;
                let queryParam = this.masterDataModel.actionNameGroup == 'Update' ? `page=${this.groupPaginator.pageIndex + 1}&ordering=-id` : 'ordering=-id';
                this.getGroupList(queryParam, '');
                this.getGroupMaster('limit=999');
                this.addNewGroupForm.reset();
                this.groupImageName = "";
                this.groupImageUrl = "";
                this.masterDataModel.id = 0;
                this.notify.success(
                  'Data ' +
                  this.masterDataModel.actionNameGroup +
                  'd Successfully.'
                );
                this.isShowGroupForm = false;
                this.masterDataModel.actionNameGroup = 'Save';
              }
            });
        });
      } else {
        this.notify.error(
          'Please select used equipment or new equipment checkbox!'
        );
      }
    }
  }

  // add update category mapping data
  addCategoryMapping() {
    this.addNewCategoryForm.markAllAsTouched();
    if (this.addNewCategoryForm.valid) {
      const formData = this.addNewCategoryForm.value;
      if (!formData.categoryUsedEquip && !formData.categoryNewEquip) {
        this.notify.error(
          'Please select used equipment or new equipment checkbox!'
        );
      }

      else if (this.selectedGroupMapping == undefined) {
        this.notify.error(
          'Please select group name from list!'
        );
      }
      else if (this.selectedCategoryMapping == undefined) {
        this.notify.error(
          'Please select category from list!'
        );
      }

      else {
        let fileName: string = 'CatMap_' + Math.random();
        this.masterDataModel.addCategoryMappingRequest = new AddCategoryMappingRequest();
        this.masterDataModel.addCategoryMappingRequest.is_used =
          formData.categoryUsedEquip == null ||
            formData.categoryUsedEquip === ''
            ? false
            : formData.categoryUsedEquip;
        this.masterDataModel.addCategoryMappingRequest.is_new =
          formData.categoryNewEquip == null ||
            formData.categoryNewEquip === ''
            ? false
            : formData.categoryNewEquip;
        this.masterDataModel.addCategoryMappingRequest.valuation_partner_group =
          formData.valuationPartnerGrpId;
        this.masterDataModel.addCategoryMappingRequest.add_seo =
          formData.seo_info_cat == null || formData.seo_info_cat === ''
            ? false
            : formData.seo_info_cat;
        this.masterDataModel.addCategoryMappingRequest.seo_title =
          formData.cSeoTitle;
        this.masterDataModel.addCategoryMappingRequest.seo_meta_keywords =
          formData.cSeoMetaKey;
        this.masterDataModel.addCategoryMappingRequest.seo_meta_desc =
          formData.cSeoMetaDes;
        this.masterDataModel.addCategoryMappingRequest.calculate_tcs =
          formData.tcs_calculate == null || formData.tcs_calculate === ''
            ? false
            : formData.tcs_calculate;
        this.masterDataModel.addCategoryMappingRequest.brand_promotion_banner = false;
        this.masterDataModel.addCategoryMappingRequest.top_banner_image = '';
        this.masterDataModel.addCategoryMappingRequest.left_banner_image = '';
        this.masterDataModel.addCategoryMappingRequest.visible_top_banner_only = false;
        this.masterDataModel.addCategoryMappingRequest.visible_left_banner_only = false;
        this.masterDataModel.addCategoryMappingRequest.is_top_hyperlink = false;
        this.masterDataModel.addCategoryMappingRequest.is_left_hyperlink = false;
        this.masterDataModel.addCategoryMappingRequest.top_hyperlink = '';
        this.masterDataModel.addCategoryMappingRequest.left_hyperlink = '';
        this.masterDataModel.addCategoryMappingRequest.group =
          this.selectedGroupMapping;
        this.masterDataModel.addCategoryMappingRequest.category =
          this.selectedCategoryMapping;
        if (this.savedLocation != null && this.savedLocation != '') {
          this.masterDataModel.addCategoryMappingRequest.image_url = this.savedLocation;
        }
        this.adminMasterService
          .createUpdateMasterData(
            this.masterDataModel.addCategoryMappingRequest,
            this.apiPath.categoryMappingMaster,
            this.masterDataModel.actionNameCategoryMapping,
            this.masterDataModel.id
          )
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.addCategoryMappingResponse = res as AddCategoryMappingResponse;
              let queryParam = this.masterDataModel.actionNameCategoryMapping == 'Update' ? `page=${this.categoryMappingPaginator.pageIndex + 1}&ordering=-id` : 'ordering=-id';
              this.getCategoryMappingList(queryParam, '');
              this.addNewCategoryForm.reset();
              this.masterDataModel.id = 0;
              this.selectedCategoryMapping = undefined;
              this.selectedGroupMapping = undefined;
              this.notify.success(
                'Data ' +
                this.masterDataModel.actionNameCategoryMapping +
                'd Successfully.'
              );
              this.isShowCategoryForm = false;
              this.masterDataModel.actionNameCategoryMapping = 'Save';
            }
          });
      }

    }
  }

  // add/update brand mapping data
  addBrand() {
    this.addNewBrandForm.markAllAsTouched();
    if (this.addNewBrandForm.valid) {
      const formData = this.addNewBrandForm.value;
      if (!formData.brandUsedEquip && !formData.brandNewEquip) {
        this.notify.error(
          'Please select used equipment or new equipment checkbox!'
        );
      }
      else if (this.selectedBrandMappingGroup == undefined) {
        this.notify.error(
          'Please select group name from list!'
        );
      }
      else if (this.selectedBrandMappingCategory == undefined) {
        this.notify.error(
          'Please select category from list!'
        );
      }
      else if (this.selectedBrandMappingBrand == undefined) {
        this.notify.error(
          'Please select brand name from list!'
        );
      }
      else {
        this.masterDataModel.addBrandMappingRequest = new AddBrandMappingRequest();
        this.masterDataModel.addBrandMappingRequest.group =
          this.selectedBrandMappingGroup;
        this.masterDataModel.addBrandMappingRequest.category =
          this.selectedBrandMappingCategory;
        this.masterDataModel.addBrandMappingRequest.brand =
          this.selectedBrandMappingBrand;
        this.masterDataModel.addBrandMappingRequest.is_used =
          formData.brandUsedEquip == null || formData.brandUsedEquip === ''
            ? false
            : formData.brandUsedEquip;
        this.masterDataModel.addBrandMappingRequest.is_new =
          formData.brandNewEquip == null || formData.brandNewEquip === ''
            ? false
            : formData.brandNewEquip;
        this.masterDataModel.addBrandMappingRequest.priority_for_use =
          formData.usedPriority == null || formData.usedPriority === ''
            ? 0
            : formData.usedPriority;
        this.masterDataModel.addBrandMappingRequest.priority_for_new =
          formData.newPriority == null || formData.newPriority === ''
            ? 0
            : formData.newPriority;
        this.masterDataModel.addBrandMappingRequest.add_seo =
          formData.seoInfo == null || formData.seoInfo === ''
            ? false
            : formData.seoInfo;
        this.masterDataModel.addBrandMappingRequest.seo_title =
          formData.bSeoTitle;
        this.masterDataModel.addBrandMappingRequest.seo_meta_keywords =
          formData.bSeoMetaKey;
        this.masterDataModel.addBrandMappingRequest.seo_meta_desc =
          formData.bSeoMetaDes;
        this.adminMasterService
          .createUpdateMasterData(
            this.masterDataModel.addBrandMappingRequest,
            this.apiPath.brandMappingMaster,
            this.masterDataModel.actionNameBrandMapping,
            this.masterDataModel.id
          )
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.addBrandMappingResponse = res as AddBrandMappingResponse;
              let queryParam = this.masterDataModel.actionNameBrandMapping == 'Update' ? `page=${this.brandMappingPaginator.pageIndex + 1}&ordering=-id` : 'ordering=-id';
              this.getBrandMappingList(queryParam, '');
              this.addNewBrandForm.reset();
              this.masterDataModel.id = 0;
              this.selectedBrandMappingCategory = undefined;
              this.selectedBrandMappingBrand = undefined;
              this.selectedBrandMappingGroup = undefined;
              this.notify.success(
                'Data ' +
                this.masterDataModel.actionNameBrandMapping +
                'd Successfully.'
              );
              this.isShowBrandForm = false;
              this.masterDataModel.actionNameBrandMapping = 'Save';
            }
          });
      }
    }
  }

  // add/update model data
  addModel(bulkModelData: any) {
    this.addNewModelForm.markAllAsTouched();
    if (this.addNewModelForm.valid) {
      const formData = this.addNewModelForm.value;
      if (!formData.modelUsedEquip && !formData.modelNewEquip) {
        this.notify.error(
          'Please select used equipment or new equipment checkbox!'
        );
      }
      else if (this.modalMappingGroup == undefined) {
        this.notify.error(
          'Please select group name from list!'
        );
      }
      else if (this.modalMappingCategory == undefined) {
        this.notify.error(
          'Please select category from list!'
        );
      }
      else if (this.modalMappingBrand == undefined) {
        this.notify.error(
          'Please select brand name from list!'
        );
      }
      else {
        let fileName: string = 'Model_' + Math.random();
        this.imageUpload(
          this.addNewModelForm,
          this.fileInputModelImg,
          'MasterData',
          fileName
        ).then((res) => {

          this.masterDataModel.addModelRequestArr = new Array<AddModelRequest>();
          this.masterDataModel.addModelRequest = new AddModelRequest();
          delete this.masterDataModel.addModelRequest.id;
          this.masterDataModel.addModelRequest.group = parseInt(this.modalMappingGroup);
          this.masterDataModel.addModelRequest.category = parseInt(this.modalMappingCategory);
          this.masterDataModel.addModelRequest.is_used = formData.modelUsedEquip == null || formData.modelUsedEquip === '' ? false
            : formData.modelUsedEquip;
          this.masterDataModel.addModelRequest.is_new =
            formData.modelNewEquip == null || formData.modelNewEquip === ''
              ? false
              : formData.modelNewEquip;
          this.masterDataModel.addModelRequest.brand =
            parseInt(this.modalMappingBrand);
          this.masterDataModel.addModelRequest.name = formData.modelName;
          //this.masterDataModel.addModelRequest.aboutModel = this.modalMappingModal;
          this.masterDataModel.addModelRequest.status =
            this.modelStatus;
          this.masterDataModel.addModelRequest.capacity_group =
            formData.capacityGroup;
          this.masterDataModel.addModelRequest.sub_category =
            formData.subCategory;
          this.masterDataModel.addModelRequest.model_code =
            formData.modelCode;
          this.masterDataModel.addModelRequest.model_desc =
            formData.modelDesc;
          this.masterDataModel.addModelRequest.capacity =
            formData.capacity == null || formData.capacity === ''
              ? 0
              : formData.capacity;
          this.masterDataModel.addModelRequest.capacity_unit =
            formData.capacityUnit;
          this.masterDataModel.addModelRequest.risk_engine_asset_id =
            formData.riskEngineAssetID;
          this.masterDataModel.addModelRequest.mfg_asset_id =
            formData.mfgAssetID;
          this.masterDataModel.addModelRequest.is_attachment_applicable =
            formData.isAttachmentApplicable;
          this.masterDataModel.addModelRequest.is_attachment =
            formData.isAttachment;
          this.masterDataModel.addModelRequest.hsn_code = formData.hsnCode;
          this.masterDataModel.addModelRequest.is_discontinued =
            formData.isDiscontinued;
          this.masterDataModel.addModelRequest.year_launched =
            formData.yearLaunched == null || formData.yearLaunched === ''
              ? 0
              : Number(this.datePipe.transform(formData.yearLaunched, 'yyyy'));
          this.masterDataModel.addModelRequest.year_discontinued =
            formData.yearDiscontinued == null || formData.yearDiscontinued === '' ? 0 :
              formData.isDiscontinued == false ? 0 : Number(this.datePipe.transform(formData.yearDiscontinued, 'yyyy'));
          this.masterDataModel.addModelRequest.is_registrable =
            formData.isRegistrable;
          this.masterDataModel.addModelRequest.asset_risk_rating =
            formData.assetRiskRating == null ||
              formData.assetRiskRating === ''
              ? 0
              : formData.assetRiskRating;
          this.masterDataModel.addModelRequest.environmental_rating =
            formData.environmentalRating == null ||
              formData.environmentalRating === ''
              ? 0
              : formData.environmentalRating;
          this.masterDataModel.addModelRequest.fuel_capacity_ltr =
            formData.fuelCapacityLtr == null ||
              formData.fuelCapacityLtr === ''
              ? 0
              : formData.fuelCapacityLtr;
          this.masterDataModel.addModelRequest.is_off_road =
            formData.isOffRoad;
          this.masterDataModel.addModelRequest.safety_rating =
            formData.safetyRating == null || formData.safetyRating === ''
              ? 0
              : formData.safetyRating;
          this.masterDataModel.addModelRequest.asset_cost =
            formData.assetCost == null || formData.assetCost === ''
              ? 0
              : formData.assetCost;
          //this.masterDataModel.addModelRequest.image_url = formData.imgURL;

          this.masterDataModel.addModelRequest.image_url = this.savedLocation ? this.savedLocation : this.modelImageUrl ? this.modelImageUrl : '';

          this.masterDataModel.addModelRequest.image_category?.push(
            (new ImageCategory().category_name = formData.modelName)
          );
          this.masterDataModel.addModelRequestArr.push(
            this.masterDataModel.addModelRequest
          );

          this.adminMasterService
            .createUpdateMasterData(
              this.masterDataModel.actionNameModel == 'Save'
                ? this.masterDataModel.addModelRequestArr
                : this.masterDataModel.addModelRequest,
              this.apiPath.modelMaster,
              this.masterDataModel.actionNameModel,
              this.masterDataModel.id
            )
            .subscribe((res) => {
              if (res != null) {
                this.masterDataModel.addModelResponse = res as AddModelResponse;
                let queryParam = this.masterDataModel.actionNameModel == 'Update' ? `page=${this.modelPaginator.pageIndex + 1}&ordering=-id` : 'ordering=-id';
                this.getModelList(queryParam, '');
                this.addNewModelForm.reset();
                this.fileInputModelImg.nativeElement.value = "";
                this.masterDataModel.id = 0;
                this.modalMappingBrand = undefined;
                this.modalMappingCategory = undefined;
                this.modalMappingGroup = undefined;
                this.modelImageUrl = "";
                this.modelImageName = "";
                this.notify.success(
                  'Data ' +
                  this.masterDataModel.actionNameModel +
                  'd Successfully.'
                );
                this.isShowModelForm = false;
                this.masterDataModel.actionNameModel = 'Save';
              }
            });
        });

      }
    }
  }

  // common function for edit and delete
  action(actionType: string, id: number, type: string) {
    if (actionType === 'Edit') {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }
    if (type === 'Group') {
      // for group
      if (actionType === 'Edit') {
        // for group edit
        this.isShowGroupForm = true;
        this.adminMasterService
          .getMasterData(`${this.apiPath.groupMaster}${id}`)
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.addGroupResponse = res as AddGroupResponse;
              this.masterDataModel.id = id;
              this.masterDataModel.actionNameGroup = 'Update';
              this.onCheckBoxChange(
                this.masterDataModel.addGroupResponse.add_seo,
                'Group'
              );
              this.addNewGroupForm.setValue({
                groupName: this.masterDataModel.addGroupResponse.name,
                groupDesc: this.masterDataModel.addGroupResponse.description,
                usedEquipment: this.masterDataModel.addGroupResponse.is_used,
                newEquipment: this.masterDataModel.addGroupResponse.is_new,
                usedPriority: this.masterDataModel.addGroupResponse
                  .priority_for_use,
                newPriority: this.masterDataModel.addGroupResponse
                  .priority_for_new,
                seoInfo: this.masterDataModel.addGroupResponse.add_seo,
                gSeoTitle: this.masterDataModel.addGroupResponse.seo_title,
                gSeoMetaKey: this.masterDataModel.addGroupResponse
                  .seo_meta_keywords,
                gSeoMetaDes: this.masterDataModel.addGroupResponse
                  .seo_meta_desc,
              });
              this.groupImageUrl = this.masterDataModel.addGroupResponse.image_url;
            }
          });
      } else if (actionType === 'Delete') {
        // for group delete
        this.dialog
          .open(ConfirmDialogComponent, {
            width: '500px'
          })

          .afterClosed()
          .subscribe((data) => {
            if (data) {
              this.adminMasterService
                .deleteMasterData(`${this.apiPath.groupMaster}${id}`)
                .subscribe((res) => {
                  if (res == null) {
                    this.notify.success('Data Deleted Successfully.');
                    this.getGroupList('ordering=-id', '');
                  }
                });
            }
          });

      }
    } else if (type === 'Category') {
      // for category mapping
      if (actionType === 'Edit') {
        this.isShowCategoryForm = true;
        // for category mapping edit
        this.adminMasterService
          .getMasterData(`${this.apiPath.categoryMappingMaster}${id}`)
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.addCategoryMappingResponse = res as AddCategoryMappingResponse;
              this.masterDataModel.id = id;
              this.masterDataModel.actionNameCategoryMapping = 'Update';
              if (this.masterDataModel.addCategoryMappingResponse.is_used) {
                this.isValuationPartnerGrpIdVisible = true;
              } else {
                this.isValuationPartnerGrpIdVisible = false;
              }
              this.onCheckBoxChange(
                this.masterDataModel.addCategoryMappingResponse.add_seo,
                'Category'
              );

              this.addNewCategoryForm.setValue({
                groupList: this.masterDataModel.addCategoryMappingResponse.group.name,
                categoryName: this.masterDataModel.addCategoryMappingResponse.category.display_name,
                categoryUsedEquip: this.masterDataModel.addCategoryMappingResponse.is_used,
                categoryNewEquip: this.masterDataModel.addCategoryMappingResponse.is_new,
                seo_info_cat: this.masterDataModel.addCategoryMappingResponse.add_seo,
                valuationPartnerGrpId: this.masterDataModel.addCategoryMappingResponse &&
                  this.masterDataModel.addCategoryMappingResponse.valuation_partner_group &&
                  this.masterDataModel.addCategoryMappingResponse.valuation_partner_group.id ?
                  this.masterDataModel.addCategoryMappingResponse.valuation_partner_group.id : '',
                tcs_calculate: this.masterDataModel.addCategoryMappingResponse.calculate_tcs,
                cSeoTitle: this.masterDataModel.addCategoryMappingResponse.seo_title,
                cSeoMetaKey: this.masterDataModel.addCategoryMappingResponse.seo_meta_keywords,
                cSeoMetaDes: this.masterDataModel.addCategoryMappingResponse.seo_meta_desc,
              });
              this.selectedCategoryMapping = this.masterDataModel.addCategoryMappingResponse.category.id;
              this.selectedGroupMapping = this.masterDataModel.addCategoryMappingResponse.group.id;
            }
          });
      } else if (actionType === 'Delete') {
        // for category mapping delete
        this.dialog
          .open(ConfirmDialogComponent, {
            width: '500px'
          })

          .afterClosed()
          .subscribe((data) => {
            if (data) {
              this.adminMasterService
                .deleteMasterData(`${this.apiPath.categoryMappingMaster}${id}`)
                .subscribe((res) => {
                  if (res == null) {
                    this.notify.success('Data Deleted Successfully.');
                    this.getCategoryMappingList('ordering=-id', '');
                  }
                });
            }
          });

      }
    } else if (type === 'Brand') {
      // for brand mapping
      if (actionType === 'Edit') {

        // for brand mapping edit
        this.isShowBrandForm = true;
        this.adminMasterService
          .getMasterData(`${this.apiPath.brandMappingMaster}${id}`)
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.addBrandMappingResponse = res as AddBrandMappingResponse;
              this.masterDataModel.id = id;
              this.masterDataModel.actionNameBrandMapping = 'Update';
              this.onCheckBoxChange(
                this.masterDataModel.addBrandMappingResponse.add_seo,
                'Brand'
              );
              this.addNewBrandForm.setValue({
                brandGroupName: this.masterDataModel.addBrandMappingResponse
                  .group.name,
                brandCategoryName: this.masterDataModel.addBrandMappingResponse
                  .category.display_name,
                brandName: this.masterDataModel.addBrandMappingResponse.brand.display_name,
                brandNewEquip: this.masterDataModel.addBrandMappingResponse
                  .is_new,
                brandUsedEquip: this.masterDataModel.addBrandMappingResponse
                  .is_used,
                usedPriority: this.masterDataModel.addBrandMappingResponse
                  .priority_for_use,
                newPriority: this.masterDataModel.addBrandMappingResponse
                  .priority_for_new,
                seoInfo: this.masterDataModel.addBrandMappingResponse.add_seo,
                bSeoTitle: this.masterDataModel.addBrandMappingResponse
                  .seo_title,
                bSeoMetaKey: this.masterDataModel.addBrandMappingResponse
                  .seo_meta_keywords,
                bSeoMetaDes: this.masterDataModel.addBrandMappingResponse
                  .seo_meta_desc,
              });
              this.selectedBrandMappingCategory = this.masterDataModel.addBrandMappingResponse.category.id;
              this.selectedBrandMappingBrand = this.masterDataModel.addBrandMappingResponse.brand.id;
              this.selectedBrandMappingGroup = this.masterDataModel.addBrandMappingResponse.group.id;

            }
          });

      } else if (actionType === 'Delete') {
        // for brand mapping delete

        this.dialog
          .open(ConfirmDialogComponent, {
            width: '500px'
          })

          .afterClosed()
          .subscribe((data) => {
            if (data) {
              this.adminMasterService
                .deleteMasterData(`${this.apiPath.brandMappingMaster}${id}`)
                .subscribe((res) => {
                  if (res == null) {
                    this.notify.success('Data Deleted Successfully.');
                    this.getBrandMappingList('ordering=-id', '');
                  }
                });
            }
          });

      }
    } else if (type === 'Model') {
      // for adding model
      if (actionType === 'Edit') {
        // for model edit
        this.isShowModelForm = true;
        this.adminMasterService
          .getMasterData(`${this.apiPath.modelMaster}${id}`)
          .subscribe((res) => {
            if (res != null) {
              this.masterDataModel.id = id;
              this.masterDataModel.actionNameModel = 'Update';
              this.masterDataModel.addModelResponse = res as AddModelResponse;
              // if (this.masterDataModel.addModelResponse.is_used) {
              //   this.isAboutModel = true;
              // } else {
              //   this.isAboutModel = false;
              // }
              this.categoryImg = this.masterDataModel.addModelResponse.category.img_url;
              this.modalMappingBrand = this.masterDataModel.addModelResponse.brand.id;
              this.modalMappingCategory = this.masterDataModel.addModelResponse.category.id;
              this.modalMappingGroup = this.masterDataModel.addModelResponse.group.id;
              this.modelImageUrl = this.masterDataModel.addModelResponse.image_url;
              this.yearDiscontinued = this.masterDataModel.addModelResponse.year_discontinued;
              this.yearLaunched = this.masterDataModel.addModelResponse.year_launched;
              this.isRegistrable = this.masterDataModel.addModelResponse.is_registrable;
              this.isAttachmentApplicable = this.masterDataModel.addModelResponse.is_attachment_applicable;
              this.isAttachment = this.masterDataModel.addModelResponse.is_attachment;
              this.isDiscontinued = this.masterDataModel.addModelResponse.is_discontinued == true;
              this.isOffRoad = this.masterDataModel.addModelResponse.is_off_road,
                this.isDiscontinued == true ? this.isShowDate = true : this.isShowDate = false;
              this.addNewModelForm.setValue({
                modelGroupName: this.masterDataModel.addModelResponse.group.name,
                modelCategoryName: this.masterDataModel.addModelResponse.category.display_name,
                modelUsedEquip: this.masterDataModel.addModelResponse.is_used,
                modelNewEquip: this.masterDataModel.addModelResponse.is_new,
                modelBrandName: this.masterDataModel.addModelResponse.brand.display_name,
                modelName: this.masterDataModel.addModelResponse.name,
                aboutModel: "",
                modelStatus: this.masterDataModel.addModelResponse.status,
                capacityGroup: this.masterDataModel.addModelResponse.capacity_group,
                subCategory: this.masterDataModel.addModelResponse.sub_category,
                modelCode: this.masterDataModel.addModelResponse.model_code,
                modelDesc: this.masterDataModel.addModelResponse.model_desc,
                capacity: this.masterDataModel.addModelResponse.capacity,
                capacityUnit: this.masterDataModel.addModelResponse.capacity_unit,
                riskEngineAssetID: this.masterDataModel.addModelResponse.risk_engine_asset_id,
                mfgAssetID: this.masterDataModel.addModelResponse.mfg_asset_id,
                isAttachmentApplicable: this.masterDataModel.addModelResponse.is_attachment_applicable,
                isAttachment: this.masterDataModel.addModelResponse.is_attachment,
                hsnCode: this.masterDataModel.addModelResponse.hsn_code,
                isDiscontinued: this.masterDataModel.addModelResponse.is_discontinued,
                yearLaunched: this.setDate(this.masterDataModel.addModelResponse.year_launched, 'yearLaunched'),
                yearDiscontinued: this.setDate(this.masterDataModel.addModelResponse.year_discontinued, 'yearDiscontinued'),
                //yearLaunched: this.masterDataModel.addModelResponse.year_launched,
                //yearDiscontinued: this.masterDataModel.addModelResponse.year_discontinued,
                isRegistrable: this.masterDataModel.addModelResponse.is_registrable,
                assetRiskRating: this.masterDataModel.addModelResponse.asset_risk_rating,
                environmentalRating: this.masterDataModel.addModelResponse.environmental_rating,
                fuelCapacityLtr: this.masterDataModel.addModelResponse.fuel_capacity_ltr,
                isOffRoad: this.masterDataModel.addModelResponse.is_off_road,
                safetyRating: this.masterDataModel.addModelResponse.safety_rating,
                assetCost: this.masterDataModel.addModelResponse.asset_cost,
                imgURL: "",
              });
            }
          });
      } else if (actionType === 'Delete') {
        // for model delete

        this.dialog
          .open(ConfirmDialogComponent, {
        width: '500px'
      })
      
          .afterClosed()
          .subscribe((data) => {
            if (data) {
              this.adminMasterService
                .deleteMasterData(`${this.apiPath.modelMaster}${id}`)
                .subscribe((res) => {
                  if (res == null) {
                    this.notify.success('Data Deleted Successfully.');
                    this.getModelList('ordering=-id', '');
                  }
                });
            }
          });
      }
    }
  }

  AddForm(type) {
    if (type == 'Group') {
      this.isShowGroupForm = true;
    }
    else if (type == 'Category') {
      this.isShowCategoryForm = true;
    }
    else if (type == 'Brand') {
      this.isShowBrandForm = true;
    }
    else if (type == 'Model') {
      this.isShowModelForm = true;
    }
  }

  setDate(selectedDate, type): any {
    if (type === 'yearLaunched') {
      let date = new Date();
      date.setFullYear(selectedDate);
      //this.addNewModelForm.get('yearLaunched')?.setValue(date);
      return date;
    } else {
      let date = new Date();
      date.setFullYear(selectedDate);
      return date;
      // this.addNewModelForm.get('yearDiscontinued')?.setValue(date);
    }
  }
  //common function for search functionality
  search(event, value: string, type: string) {
    if (type === 'Group') {
      if (event && event.target.value.length <= 0) {
        let pageindex = this.brandMappingPaginator.pageIndex + 1;
        let groupQueryParam = 'page=' + pageindex + '&limit=' + this.groupPageSize + '&ordering=-id&search='
        this.getGroupList(groupQueryParam, '');
      }
      else if (event && event.which == 13) {
        value = value.trim();
        value = value.toLowerCase();
        this.getGroupList('ordering=-id&search=' + value + '', value);
      }
    } else if (type === 'Category') {
      if (event && event.target.value.length <= 0) {
        let pageindex = this.brandMappingPaginator.pageIndex + 1;
        let catQueryParam = 'page=' + pageindex + '&limit=' + this.catPageSize + '&ordering=-id&search='
        this.getCategoryMappingList(catQueryParam, '');
      }
      else if (event && event.which == 13) {
        value = value.trim();
        value = value.toLowerCase();
        this.getCategoryMappingList('ordering=-id&search=' + value + '', value);
      }

    }
    else if (type === 'Brand') {
      if (event && event.target.value.length <= 0) {
        let pageindex = this.brandMappingPaginator.pageIndex + 1;
        let brandQueryParam = 'page=' + pageindex + '&limit=' + this.brandPageSize + '&ordering=-id&search='
        this.getBrandMappingList(brandQueryParam, '');
      }
      else if (event && event.which == 13) {
        value = value.trim();
        value = value.toLowerCase();
        this.getBrandMappingList('ordering=-id&search=' + value + '', value);
      }
    }

    else if (type === 'Model') {
      if (event && event.target.value.length <= 0) {
        let pageindex = this.brandMappingPaginator.pageIndex + 1;
        let modelQueryParam = 'page=' + pageindex + '&limit=' + this.modelPageSize + '&ordering=-id&search='
        this.getModelList(modelQueryParam, '');
      }
      else if (event && event.which == 13) {
        value = value.trim();
        value = value.toLowerCase();
        this.getModelList('ordering=-id&search=' + value + '', value);
      }

    }
  }

  searchOnBlur(value: string, type: string) {
    if (type === 'Group') {
      value = value.trim();
      value = value.toLowerCase();
      this.getGroupList('ordering=-id&search=' + value + '', value);
    }
    else if (type === 'Category') {
      value = value.trim();
      value = value.toLowerCase();
      this.getCategoryMappingList('ordering=-id&search=' + value + '', value);
    }
    else if (type === 'Brand') {
      value = value.trim();
      value = value.toLowerCase();
      this.getBrandMappingList('ordering=-id&search=' + value + '', value);
    }
    else if (type === 'Model') {
      value = value.trim();
      value = value.toLowerCase();
      this.getModelList('ordering=-id&search=' + value + '', value);
    }
  }


  /**upload files and video by this method */
  async imageUpload(frmGroup: any, image: any, tab: string, side: string) {
    this.savedLocation = '';
    const file = image.nativeElement.files[0];
    if (file != null) {
      if (
        (file.type == 'image/png' ||
          file.type == 'image/jpg' ||
          file.type == 'image/jpeg' ||
          file.type == 'image/bmp') &&
        file.size <= 5242880
      ) {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue(reader.result);
        };
        var fileFormat = (file?.name).toString();
        var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

        var image_path =
          environment.bucket_parent_folder +
          '/' +
          tab +
          '/' +
          side +
          '.' +
          fileExt;

        var uploaded_file = await this.s3.prepareFileForUpload(
          file,
          image_path
        );
        if (
          uploaded_file?.Key != undefined &&
          uploaded_file?.Key != '' &&
          uploaded_file?.Key != null
        ) {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side)
            ?.setValue(uploaded_file?.Key);
          this.savedLocation = uploaded_file?.Location;
        } else {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue('');
          this.notify.error('Upload Failed');
        }
      } else {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setValue('');
        frmGroup
          .get(tab)
          ?.get('docImages')
          ?.get(side)
          ?.setErrors({ imgIssue: true });
      }
    }
  }

  // add/update model data
  importBulkModel(bulkModelData: any) {
    if (bulkModelData != null && bulkModelData.length > 0) {
      this.masterDataModel.addModelRequestArr = new Array<AddModelRequest>();
      bulkModelData.forEach((modelData) => {
        this.masterDataModel.addModelRequest = new AddModelRequest();
        let id = modelData['ID*'] != null && modelData['ID*'] > 0 ? modelData['ID*'] : "";
        this.masterDataModel.addModelRequest.id = id;
        if (id > 0) {
          this.masterDataModel.actionNameModel = 'Update';
        } else {
          this.masterDataModel.actionNameModel = 'Save';
        }
        this.masterDataModel.addModelRequest.group = modelData['Product_Group*'];
        this.masterDataModel.addModelRequest.category = modelData['Product_Category*'];
        this.masterDataModel.addModelRequest.is_used = modelData['Used*'] === '' ? false : modelData['Used*'];
        this.masterDataModel.addModelRequest.is_new = modelData['New*'] === '' ? false : modelData['New*'];
        this.masterDataModel.addModelRequest.brand = modelData['Brand_Name*'];
        this.masterDataModel.addModelRequest.name = modelData['Model_Name*'];
        this.masterDataModel.addModelRequest.aboutModel = modelData[''];
        this.masterDataModel.addModelRequest.capacity_group = modelData['Capacity_Group'];
        this.masterDataModel.addModelRequest.sub_category = modelData['Sub_Category'];
        this.masterDataModel.addModelRequest.model_code = modelData['Model_Code'];
        this.masterDataModel.addModelRequest.model_desc = modelData['Model_Descripiton'];
        this.masterDataModel.addModelRequest.capacity = modelData['Capacity_'] === '' ? 0 : modelData['Capacity_'];
        this.masterDataModel.addModelRequest.capacity_unit = modelData['Capacity_Unit'];
        this.masterDataModel.addModelRequest.risk_engine_asset_id = modelData['Risk_Engine_Asset_ID'];
        this.masterDataModel.addModelRequest.mfg_asset_id = modelData['Mfg_Asset_ID'];
        this.masterDataModel.addModelRequest.is_attachment_applicable = modelData['Is_Attachment_Applicable'];
        this.masterDataModel.addModelRequest.is_attachment = modelData['Is_Attachment'];
        this.masterDataModel.addModelRequest.hsn_code = modelData['HSN_Code'];
        this.masterDataModel.addModelRequest.is_discontinued = modelData['Is_Discontinued'];
        this.masterDataModel.addModelRequest.year_launched = modelData['Year_Launched'] === '' ? 0 : modelData['Year_Launched'];
        this.masterDataModel.addModelRequest.year_discontinued = modelData['Year_Discontinued'] === '' ? 0 : modelData['Year_Discontinued'];
        this.masterDataModel.addModelRequest.is_registrable = modelData['Is_Registrable'];
        this.masterDataModel.addModelRequest.asset_risk_rating = modelData['Asset_Risk_Rating'] === '' ? 0 : modelData['Asset_Risk_Rating'];
        this.masterDataModel.addModelRequest.environmental_rating = modelData['Environmental_Rating'] === '' ? 0 : modelData['Environmental_Rating'];
        this.masterDataModel.addModelRequest.fuel_capacity_ltr = modelData['Fuel_Capacity_Ltr'] === '' ? 0 : modelData['Fuel_Capacity_Ltr'];
        this.masterDataModel.addModelRequest.is_off_road = modelData['Is_Off_Road'];
        this.masterDataModel.addModelRequest.safety_rating = modelData['Safety_Rating'] === '' ? 0 : modelData['Safety_Rating'];
        this.masterDataModel.addModelRequest.asset_cost = modelData['Asset_Cost'] === '' ? 0 : modelData['Asset_Cost'];
        this.masterDataModel.addModelRequest.image_url = modelData['Image_URL'];
        //this.masterDataModel.addModelRequest.model_banner_image = '';
        this.masterDataModel.addModelRequest.image_category?.push(
          (new ImageCategory().category_name = modelData['Model_Name*'] === '' ? '' : modelData['Model_Name*'])
        );
        this.masterDataModel.addModelRequestArr?.push(
          this.masterDataModel.addModelRequest
        );
      });
      this.adminMasterService
        .modelBatchUpdate(this.masterDataModel.addModelRequestArr,
          this.apiPath.batchUpdateMaster,
          this.masterDataModel.actionNameModel,
          this.masterDataModel.id
        )
        .subscribe((res: any) => {
          if (res != null) {
            this.errorRecordList = res.data;
            this.fileInputBulkInsert.nativeElement.value = "";
            if (res && res.failed_count > 0) {
              this.isShowInvalidRecords = true;
              // this.isShowModelForm = true;
            }
            this.masterDataModel.addModelResponse = res as AddModelResponse;
            let queryParam = this.masterDataModel.actionNameModel == 'Update' ? `page=${this.modelPaginator.pageIndex + 1}&ordering=-id` : 'ordering=-id';
            this.getModelList(queryParam, '');
            this.addNewModelForm.reset();
            this.masterDataModel.id = 0;
            this.modalMappingBrand = undefined;
            this.modalMappingCategory = undefined;
            this.modalMappingGroup = undefined;
            this.modelImageUrl = "";
            this.modelImageName = "";
            // setTimeout(() => {
            //   this.notify.error(
            //    // res.failed_count + 'out of' + res.total_count + ' records are Failed'
            //   );
            // }, 400);
            this.notify.success(
              res.successful_count + ' out of ' + res.total_count + ' records are processed successfully'
            );
            this.masterDataModel.actionNameModel = 'Save';
          }
        });
    }
  }

  // for uploading excel files
  onFileChange(event: any) {
    /* wire up file reader */

    const target: DataTransfer = <DataTransfer>event.target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
      this.bulkUploadData = data;
      if (data != null && data.length > 0) {
        this.importBulkModel(data); // save uploaded model data
      }
    };
  }

  exportData(type) {
    switch (type) {
      case 'Group': {
        this.getGroupList('page=1&limit=-1&ordering=-id', '', true);
        break;
      }
      case 'Category': {
        this.getCategoryMappingList('page=1&limit=-1&ordering=-id', '', true);
        break;
      }
      case 'Brand': {
        this.getBrandMappingList('page=1&limit=-1&ordering=-id', '', true);
        break;
      }
      case 'Model': {
        this.getModelList('page=1&limit=-1&ordering=-id', '', true);
        break;
      }
    }
  }

  exportCategoryMappingData(data) {
    const excelData = data.map((r: any, index) => {
      return {
        'Sr. No': index + 1,
        'Category Id': r?.id,
        'Product Group': r?.group?.name,
        'Product Category': r?.category?.name,
        Used: r?.is_used ? 'Yes' : 'No',
        New: r?.is_new ? 'Yes' : 'No',
        Both: r?.is_used && r?.is_new ? 'Yes' : 'No',
      };
    });
    ExportExcelUtil.exportArrayToExcel(excelData, 'Product Category Mapping');
  }

  exportGroupData(data) {
    const excelData = data.map((r: any, index) => {
      return {
        'Sr. No': index + 1,
        'Group Id': r?.id,
        'Product Group': r?.name,
        'Description': r?.description,
        Used: r?.is_used ? 'Yes' : 'No',
        New: r?.is_new ? 'Yes' : 'No',
        Both: r?.is_used && r?.is_new ? 'Yes' : 'No',
      };
    });
    ExportExcelUtil.exportArrayToExcel(excelData, 'Product Group');
  }



  exportBrandMappingData(data) {
    const excelData = data.map((r: any, index) => {
      return {
        'Sr. No': index + 1,
        'Brand Id': r?.id,
        'Product Group': r?.group?.name,
        'Product Category': r?.category?.name,
        'Product Brand': r?.brand?.name,
        Used: r?.is_used ? 'Yes' : 'No',
        New: r?.is_new ? 'Yes' : 'No',
        Both: r?.is_used && r?.is_new ? 'Yes' : 'No',
      };
    });
    ExportExcelUtil.exportArrayToExcel(excelData, 'Product Brand Mapping');
  }

  exportModelMappingData(data) {
    const excelData = data.map((r: any) => {
      return {
        ID: r?.id,
        'Product Group': r?.group?.name,
        'Category Id': r?.category?.id,
        'Category Name': r?.category?.name,
        'Brand Id': r?.brand?.id,
        'Brand Name': r?.brand?.name,
        'Model Name': r?.name,
        Used: r?.is_used ? 'Yes' : 'No',
        New: r?.is_new ? 'Yes' : 'No',
        Both: r?.is_used && r?.is_new ? 'Yes' : 'No',
        'Capacity Group': r?.capacity_group,
        'Sub Category': r?.sub_category,
        'Model Code': r?.model_code,
        'Model Description': r?.model_desc,
        Capacity: r?.capacity,
        'Capacity unit': r?.capacity_unit,
        'Risk Engine Asset Id': r?.risk_engine_asset_id,
        'Mfg Asset Id': r?.mfg_asset_id,
        'Is Attachment Applicable': r?.is_attachment_applicable ? 'Yes' : 'No',
        'Is Attachment': r?.is_attachment ? 'Yes' : 'No',
        'HSN Code': r?.hsn_code,
        'Is Discontinued': r?.is_discontinued ? 'Yes' : 'No',
        'Year Launched': r?.year_launched,
        'Year Discontinued': r?.year_discontinued,
        'Is Registrable': r?.is_registrable ? 'Yes' : 'No',
        'Image URL': r?.image_url,
        'Asset Risk Rating': r?.asset_risk_rating,
        'Environmental Rating': r?.environmental_rating,
        'Fuel Capacity Ltr': r?.fuel_capacity_ltr,
        'Is Off Road': r?.is_off_road ? 'Yes' : 'No',
        'Safety Rating': r?.safety_rating,
        'Asset Cost': r?.asset_cost,
      };
    });
    ExportExcelUtil.exportArrayToExcel(excelData, 'Product Model Mapping');
  }

  resetFile(type) {
    if (type == 'fileInputGroupImg') {
      this.groupImageName = "";
      this.fileInputGroupImg.nativeElement.value = "";
    }
    if (type == 'fileInputModelImg')
      this.modelImageName = "";
    this.fileInputModelImg.nativeElement.value = "";
  }

  getFileName(type) {
    if (type == 'fileInputGroupImg') {
      if (this.fileInputGroupImg && this.fileInputGroupImg.nativeElement && this.fileInputGroupImg.nativeElement.files && this.fileInputGroupImg.nativeElement.files[0]) {
        this.groupImageName = this.fileInputGroupImg.nativeElement.files[0].name;
        //  return this.fileInputGroupImg.nativeElement.files[0].name;
      }
      return '';
    }

    if (type == 'fileInputModelImg') {
      if (this.fileInputModelImg && this.fileInputModelImg.nativeElement && this.fileInputModelImg.nativeElement.files && this.fileInputModelImg.nativeElement.files[0]) {
        this.modelImageName = this.fileInputModelImg.nativeElement.files[0].name;
        //return this.fileInputModelImg.nativeElement.files[0].name;
      }
      return '';
    }
    return '';
  }

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.addNewModelForm.get('yearLaunched');
    ctrlValue.setValue(_moment(this.minDate));
    ctrlValue.value.year(normalizedYear.year());
    ctrlValue.value.month(normalizedYear.month());
    ctrlValue.setValue(ctrlValue.value);
    this.addNewModelForm.get('yearLaunched').setValue(ctrlValue.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }
  chosenYearHandlerDate(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.addNewModelForm.get('yearDiscontinued');
    ctrlValue.setValue(_moment(this.minDate));
    ctrlValue.value.year(normalizedYear.year());
    ctrlValue.value.month(normalizedYear.month());
    ctrlValue.setValue(ctrlValue.value);
    this.addNewModelForm.get('yearDiscontinued').setValue(ctrlValue.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }

  deleteImage(url, type) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        if (type == 'GroupImage') {
          this.fileInputGroupImg.nativeElement.value = "";
          this.groupImageUrl = "";
          this.savedLocation = "";
        }
        else {
          this.fileInputModelImg.nativeElement.value = "";
          this.modelImageUrl = "";
          this.savedLocation = "";
        }
      },
      err => {
        this.notify.error(err);
      }
    )
  }

  isDiscontinuedChange(event) {
    if (event.value == "true") {
      this.isShowDate = true;
      this.addNewModelForm.get('yearDiscontinued').setValue('');
      this.addNewModelForm.get('yearLaunched')?.setValidators(Validators.required);
      this.addNewModelForm.get('yearLaunched')?.updateValueAndValidity();
      this.addNewModelForm.get('yearDiscontinued')?.setValidators(Validators.required);
      this.addNewModelForm.get('yearDiscontinued')?.updateValueAndValidity();
    }
    else {
      this.isShowDate = false;
      this.addNewModelForm.get('yearLaunched')?.setValidators(Validators.required);
      this.addNewModelForm.get('yearLaunched')?.updateValueAndValidity();
      this.addNewModelForm.get('yearDiscontinued')?.clearValidators();
      this.addNewModelForm.get('yearDiscontinued')?.updateValueAndValidity();
    }
  }

  viewErrorList() {
    const dialogRef = this.dialog.open(ModelBulkErrorListComponent, {
      width: '700px',
      panelClass: 'custom-modalbox',
      data: {
        errorRecordList: this.errorRecordList,
        isFromModel: true
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  hideErrorList() {
    this.isShowInvalidRecords = false;
  }
  
  // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(this.currentTab == 0){
          if((( this.groupPaginator.length > this.listingData.length) && (this.listingData.length > 0))){
            if(!this.scrolled){
              this.scrolled = true;
            this.groupPaginator.pageIndex++;
            this.getGroupList(this.getQueryParams('Group', this.groupPageSize), '');
            }
          }
          
        }else if(this.currentTab == 1){
          if(((this.categoryMappingPaginator.length > this.listingDataCat.length) && (this.listingDataCat.length > 0))){
            if(!this.scrolledCat){
              this.scrolledCat = true;
              this.categoryMappingPaginator.pageIndex++;
            this.getCategoryMappingList(this.getQueryParams('Category', this.catPageSize), '');     
            }
          }
          
        }else if(this.currentTab == 2){
          if(((this.brandMappingPaginator.length > this.listingDataBrand.length) && (this.listingDataBrand.length > 0))){
            if(!this.scrolledBrand){
              this.scrolledBrand = true;
            this.brandMappingPaginator.pageIndex++;
            this.getBrandMappingList(this.getQueryParams('Brand', this.brandPageSize), '');
          }
        }
        }else{
          if(((this.modelPaginator.length > this.listingDataModel.length) && (this.listingDataModel.length > 0))){
            if(!this.scrolledModel){
              this.scrolledModel = true;
              this.modelPaginator.pageIndex++;
              this.getModelList(this.getQueryParams('Model', this.modelPageSize), '');
          }
        }
        }
    }
  }
}
}

@Component({
  selector: 'error-list-record-dialog.component',
  template: `
  <div class="model-heading modal-popup">
    <h3> Invalid Records List </h3>
    <button class="close-btn">
    <span class="mat-button-wrapper" >
    <img  (click)="close()" alt="" src="../../../../../../assets/images/Close.svg">
    </span>
    </button>
  </div>

    <div class="model-form-body" *ngIf="data.isFromModel">
      <div class="table-responsive">
    <table>
        <tr>
          <th>Name</th>
          <th>Error</th>
        </tr>
        <tr *ngFor="let obj of data.errorRecordList; let i = index">
          <td>{{obj.name ? obj.name : i+1}}</td>
          <td>{{obj.message}}</td>
        </tr>
      </table>
      </div>
    </div>

    <div class="model-form-body" *ngIf="data.isFromTechUsed">
    <div class="table-responsive">
    <table>
        <tr>
          <th>Category</th>
          <th>Brand</th>
          <th>Model</th>
          <th>Error</th>
        </tr>
        <tr *ngFor="let obj of data.errorRecordList; let i = index">
          <td>{{obj.category}}</td>
          <td>{{obj.brand}}</td>
          <td>{{obj.model}}</td>
          <td>{{obj.message}}</td>
        </tr>
      </table>
      </div>
    </div>

    <div class="model-form-body" *ngIf="data.isFromFulfilment">
    <div class="table-responsive">
    <table>
        <tr>
          <th>Auction Id </th>
          <th>lot Number </th>
          <th>Buyer </th>
          <th>Error</th>
        </tr>
        <tr *ngFor="let obj of data.errorRecordList; let i = index">
          <td>{{obj.auction}}</td>
          <td>{{obj.lot_number}}</td>
          <td>{{obj.buyer}}</td>
          <td>{{obj.message}}</td>
        </tr>
      </table>
      </div>
    </div>

   
    `,
  styleUrls: ['./master-data.component.css'],
})
export class ModelBulkErrorListComponent {
  constructor(
    public dialogRef: MatDialogRef<ModelBulkErrorListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}