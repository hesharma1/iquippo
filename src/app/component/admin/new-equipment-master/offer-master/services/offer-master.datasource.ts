import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of } from "rxjs";
import { OfferMaster } from "../model/offer-master";
import { OfferMasterService } from "./offer-master.service";
import { catchError, finalize } from "rxjs/operators";
import { resolve } from "dns";

export class OfferMasterDataSource implements DataSource<OfferMaster> {

    private lessonsSubject = new BehaviorSubject<OfferMaster[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private offerMasterService: OfferMasterService) {}

    loadOfferMasters( pageIndex: number, pageSize: number, sortDirection: string, filter: string ) {
        return new Promise(resolve=>{
            this.loadingSubject.next(true);
            this.offerMasterService.findAllOffers(pageIndex, pageSize, sortDirection, filter)
            .subscribe((results:any) => {
                this.lessonsSubject.next(results)
                resolve(results);
            });
        }).catch(e => console.log(e));
    }

    connect(collectionViewer: CollectionViewer): Observable<OfferMaster[]> {
        return this.lessonsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.lessonsSubject.complete();
        this.loadingSubject.complete();
    }
}
