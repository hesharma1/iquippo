import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from '../../../../../utility/app.refrence';
import { HttpClientService } from '../../../../../utility/http-client.service';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { OfferMaster } from "../model/offer-master";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class OfferMasterService {

    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService, private httpClient: HttpClient) { }
    
    //for post and put use HttpPostRequestCommon and HttpPutRequestCommon functions respectively
    getCategoryMaster() {
        return this.httpClientService.HttpGetRequestCommon(this.apiPath.offerMaster, environment.baseUrl);
    }
    /**
     * Find an Offer
     */
    // findOfferById(courseId: number): Observable<OfferMaster> {
    //     return this.httpClient.get<OfferMaster>(`${environment.baseUrl + this.apiPath.offerMaster}`);
    // }

    // findOfferById(courseId: number): Observable<Object> {
    //     return this.httpClient.get(`${environment.baseUrl + this.apiPath.offerMaster}${courseId}/`);
    // }

    findOfferById(id: any) {
        return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.offerMaster}${id}/`, environment.baseUrl);
    }

    /**
     * Find All Offers
     */
    // findAllOffers(pageNumber = 0, pageSize = 3, sortOrder = 'asc', filter = ''):  Observable<OfferMaster[]> {
    //     return this.httpClient.get(`${environment.baseUrl + this.apiPath.offerMaster}`, {
    //         params: new HttpParams()
    //             .set('page', pageNumber.toString())
    //             .set('pageSize', pageSize.toString())
    //             .set('sortOrder', sortOrder)
    //             .set('filter', filter)
    //     }).pipe(
    //         map((res: any) =>  res.json())
    //     );
    // }  
    
    findAllOffers(pageNumber = 0, pageSize = 3, sortOrder = '', filter = ''):  Observable<Object> {
        return this.httpClient.get(`${environment.baseUrl + this.apiPath.offerMaster}`, {
            params: new HttpParams()
                .set('page', pageNumber.toString())
                .set('limit', pageSize.toString())
                .set('ordering', sortOrder)
                .set('search', filter)
        }).pipe(
            map((res: any) =>  res)
        );
    } 
}
