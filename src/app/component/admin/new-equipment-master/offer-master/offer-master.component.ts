import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { OfferMasterCreateComponent } from "./components";

@Component({
    selector: 'app-offer-master',
    templateUrl: './offer-master.component.html',
    styleUrls: ['./offer-master.component.css']
})
export class OfferMasterComponent implements OnInit {

    showForm: boolean = false;

    constructor(public dialog: MatDialog) 
    { 
        this.showForm = false;        
    }

    GetChildData(data:boolean)
    {
        this.showForm = data;
        //this.createUpdateCategory(1);
        
        //const o = new CreateUpdateCategoryDialogComponen(json);
    }

    openDialog() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.height = '600px';
        dialogConfig.width = '920px';

        const dialogRef = this.dialog.open(OfferMasterCreateComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }
    ngOnInit(): void {}

}
