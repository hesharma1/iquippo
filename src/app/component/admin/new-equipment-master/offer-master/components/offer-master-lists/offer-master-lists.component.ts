import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { merge, fromEvent } from "rxjs";
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";

import { OfferMaster } from "../../model/offer-master";
import { OfferMasterService } from "../../services/offer-master.service";
import { OfferMasterDataSource } from "../../services/offer-master.datasource";
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OfferMasterCreateComponent } from '../offer-master-create/offer-master-create.component';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { string0To255 } from 'aws-sdk/clients/customerprofiles';

@Component({
    selector: 'app-offer-master-lists',
    templateUrl: './offer-master-lists.component.html',
    styleUrls: ['./offer-master-lists.component.css']
})
export class OfferMasterListsComponent implements OnInit, AfterViewInit {
    course!: OfferMaster;
    dataSource1 = new MatTableDataSource<[]>();
    dataSource: any = [];
    displayedColumns = ["seqNo", "CategoryId", "SubCategoryId", "ModelId", "Location", "CashOnPurcahe", "ListInFinance", "Lease", "Status", "Action"];
    @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort!: MatSort;
    @ViewChild('input') input!: ElementRef;
    myPageIndex : number = 1;
    lead : any;
    searchItem : string = '';
    myPageSize : number = 3;

    constructor(private route: ActivatedRoute,
        public dialog: MatDialog,
        private offerMasterService: OfferMasterService) { }

     ngOnInit() {
        // var data = this.dataSource = new OfferMasterDataSource(this.offerMasterService);
        // // this.dataSource.loadOfferMasters(1, 3, 'asc', '');
        // this.lead = await this.dataSource.loadOfferMasters(this.myPageIndex, 3, '', '');
        // this.dataSource1 = this.lead.results;
        this.callOfferMasterDataSource(this.searchItem);
    }

    async callOfferMasterDataSource(search: string0To255){
        var data = this.dataSource = new OfferMasterDataSource(this.offerMasterService);
        // this.dataSource.loadOfferMasters(1, 3, 'asc', '');
        this.lead = await this.dataSource.loadOfferMasters(this.myPageIndex, this.myPageSize, '', search);
        this.dataSource = this.lead.results;
     }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;

                    this.loadOfferMasterPage();
                })
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => this.loadOfferMasterPage())
            )
            .subscribe();
    }
    /**
     * loadOfferMasterPage
     */
    loadOfferMasterPage() {
        //var data = this.dataSource = new OfferMasterDataSource(this.offerMasterService);
        this.dataSource.loadOfferMasters(
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.sort.direction,
            this.input.nativeElement.value);
    }

    /*Previous and Next button functionality*/
     pageEvents(event : any){
        console.log(event.pageIndex);
        console.log(event.pageSize);
        if(event.previousPageIndex > event.pageIndex)
        {
            if(this.myPageIndex != 1)
            {
                this.myPageIndex--;
                this.callOfferMasterDataSource(this.searchItem);
            }   
        }
        else{
            this.myPageIndex++;
            this.callOfferMasterDataSource(this.searchItem);
        }
    }

    /*filter data*/
    applyFilter(searchValue : string){
        searchValue = searchValue.trim();
        searchValue = searchValue.toLowerCase();
        //this.dataSource1.filter = searchValue;
        this.callOfferMasterDataSource(searchValue);
    }

    editOffer(offerId : number){
        console.log(offerId);
        this.findOfferById(offerId);
    }

    openDialog(dataObject : any) {
        var data = dataObject.finance_or_leases;
        var finance : any[] = [];
        var lease : any[] = [];
        var offerType : string;
        for(var i = 0; i < data.length; i++){
            if(data[i].type == 1)
            {
                finance.push(data[i]);
            }
            else if(data[i].type == 2)
            {
                lease.push(data[i]);
            }
        }
        if(dataObject.is_new){
            offerType = "new";
        }else if(!dataObject.is_new && dataObject.is_used){
            offerType = "used";
        }
        else{
            offerType ="both";
        }
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.height = '600px';
        dialogConfig.width = '920px';
        dialogConfig.data = {
            formData :
            {
                category : dataObject.model.category.id,
                brand : dataObject.model.brand.id,
                model : dataObject.model.id,
                country : dataObject.country.id,
                location : dataObject.locations,
                offerType : offerType,
                carPurchase : dataObject.is_cash,
                listFinance : dataObject.is_finance,
                financer : dataObject.finance,
                listLease : dataObject.is_lease,
                leaser : dataObject.lease,
                carPurchaseData : dataObject.cash_purchases,
                listFinanceData : finance,
                listLeaseData : lease
            },
            editId : dataObject.id
            
            
        }

        const dialogRef = this.dialog.open(OfferMasterCreateComponent, dialogConfig);

        dialogRef.afterClosed().subscribe((result:any) => {
            console.log(`Dialog result: ${result}`);
        });
    }


    findOfferById(id: number){
        this.offerMasterService.findOfferById(id).subscribe(res =>{
            var responseData = res as ResponseData;
            this.openDialog(responseData);
        },err=>{
            console.log(err);
        })
    }

    // nextBtn(){
    //     this.myPageIndex++;
    //     this.callOfferMasterDataSource();
    // }

    // prevBtn(){
    //     if(this.myPageIndex != 1)
    //     {
    //         this.myPageIndex--;
    //         this.callOfferMasterDataSource();
    //     }
    // }
}
