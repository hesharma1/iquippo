import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

import { AdminMasterService } from '../../../../../../services/admin-master.service';
import { SaveOfferMaster } from '../../model/save-offer-master.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-offer-master-create',
    templateUrl: './offer-master-create.component.html',
    styleUrls: ['./offer-master-create.component.css']
})
export class OfferMasterCreateComponent implements OnInit {
    @Output() ChdOutput: EventEmitter<string> = new EventEmitter();

    form!: FormGroup;
    PDList!: FormArray;
    FDList!: FormArray;
    LDList!: FormArray;
    submitted = false;
    subsCategory!: Subscription;
    subsBrand!: Subscription;
    subsModel!: Subscription;
    subsCountry!: Subscription;
    subsLocation!: Subscription;
    categoryList = [];
    brandList = [];
    modelList = [];
    countryList = [];
    locationList = [];
    isHiddenCarPurchase = false;
    isHiddenListFinance = false;
    isHiddenListLease = false;
    isEdit = false;
    isEditId: number = 0;
    offerMasterSave?: SaveOfferMaster;
    StrOutputMSG: boolean = false;

    constructor(private fb: FormBuilder,
        private adminMasterService: AdminMasterService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<OfferMasterCreateComponent>) {
        this.form = this.fb.group({
            offerType: ['', Validators.required],
            category: ['', Validators.required],
            brand: ['', Validators.required],
            model: ['', Validators.required],
            country: ['', Validators.required],
            location: ['', Validators.required],
            financer: [''],
            leaser: [''],
            carPurchase: [false],
            listFinance: [false],
            listLease: [false],
            carPurchaseData: this.fb.array([this.createCarPurchase()]),
            listFinanceData: this.fb.array([this.createListFinance()]),
            listLeaseData: this.fb.array([this.createListLease()]),
        });
        if (data) {
            this.isEdit = true;
            this.isHiddenCarPurchase = data.formData.carPurchase ? true : false;
            this.isHiddenListFinance = data.formData.listFinance ? true : false;
            this.isHiddenListLease = data.formData.listLease ? true : false;
            this.isEditId = data.editId;
            this.form.patchValue(data.formData);
        }
    }

    ngOnInit(): void {
        this.offerMasterSave = new SaveOfferMaster;
        // this.form = this.fb.group({
        //     offerType: ['', Validators.required],
        //     category: ['', Validators.required],
        //     brand: ['', Validators.required],
        //     model: ['', Validators.required],
        //     country: ['', Validators.required],
        //     location: ['', Validators.required],
        //     financer: [''],
        //     leaser: [''],
        //     carPurchase: [false],
        //     listFinance: [false],
        //     listLease: [false],
        //     carPurchaseData: this.fb.array([this.createCarPurchase()]),
        //     listFinanceData: this.fb.array([this.createListFinance()]),
        //     listLeaseData: this.fb.array([this.createListLease()]),
        // });
        this.PDList = this.form.get('carPurchaseData') as FormArray;
        this.FDList = this.form.get('listFinanceData') as FormArray;
        this.LDList = this.form.get('listLeaseData') as FormArray;
        // Fetch drop down options
        this.getCategoryMasters();
        this.getCountryMaster();
        this.getLocationMaster();
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }
    get PDFormGroup() { return this.form.get('carPurchaseData') as FormArray; }
    get FDFormGroup() { return this.form.get('listFinanceData') as FormArray; }
    get LDFormGroup() { return this.form.get('listLeaseData') as FormArray; }

    // contact car purchase field group
    createCarPurchase(): FormGroup {
        return this.fb.group({
            price: [''],
            free_of_cost: [''],
        });
    }

    Close() {
        this.ChdOutput.emit();//this.StrOutputMSG);
        this.dialogRef.close();
    }

    // add car purchase field group
    addCarPurchase() {
        this.PDList.push(this.createCarPurchase());
    }

    // remove car purchase field group
    removeCarPurchase(index: number) {
        this.PDList.removeAt(index);
    }

    // create list finance field group
    createListFinance(): FormGroup {
        return this.fb.group({
            type: ['1'],
            name: ['Finance'],
            tenure: [''],
            amount: [''],
            rate: [''],
            margin: [''],
            processing_fee: [''],
            installment: [''],
            free_cost: [''],
        });
    }

    // add list finance field group
    addListFinance() {
        this.FDList.push(this.createListFinance());
    }

    // remove list finance field group
    removeListFinance(index: number) {
        this.FDList.removeAt(index);
    }

    // contact list lease field group
    createListLease(): FormGroup {
        return this.fb.group({
            type: ['2'],
            name: ['Lease'],
            tenure: [''],
            amount: [''],
            rate: [''],
            margin: [''],
            processing_fee: [''],
            installment: [''],
            free_cost: [''],
        });
    }

    // add list lease field group
    addListLease() {
        this.LDList.push(this.createListLease());
    }

    // remove list lease field group
    removeListLease(index: number) {
        this.LDList.removeAt(index);
    }

    //Customize request according to save offer 
    getRequestForSaving() {

        let offerType = this.f.offerType.value;
        let isNew = offerType == 'new' ? true : false;
        let isCash = this.f.carPurchase.value;
        let isFinance = this.f.listFinance.value;
        let isLease = this.f.listLease.value;
        var offerMasterSave = new SaveOfferMaster();
        offerMasterSave.category = this.f.category.value;
        offerMasterSave.brand = this.f.brand.value;
        offerMasterSave.model = this.f.model.value;
        offerMasterSave.country = this.f.country.value;
        const value = this.f.location.value;
        value.forEach((element: string) => {
            offerMasterSave.locations?.push(element);
        });
        offerMasterSave.is_new = isNew;
        offerMasterSave.is_cash = isCash;
        offerMasterSave.is_finance = isFinance;
        offerMasterSave.finance = this.f.financer.value;
        offerMasterSave.is_lease = isLease;
        offerMasterSave.lease = this.f.leaser.value;
        if (isCash) {
            const cashValue = this.PDFormGroup.value;
            cashValue.forEach((element: any) => {
                offerMasterSave.cash_purchases?.push(element);
            });
        }
        if (isFinance) {
            const financeValue = this.FDFormGroup.value;
            financeValue.forEach((element: string) => {
                offerMasterSave.finance_or_leases?.push(element);
            });
        }
        if (isLease) {
            const isLeaseValue = this.LDFormGroup.value;
            isLeaseValue.forEach((element: string) => {
                offerMasterSave.finance_or_leases?.push(element);
            });
        }
        return offerMasterSave;
    }

    // Save on submit
    save() {
        this.submitted = true;
        if (!this.form.invalid) {
            var offerMasterSave = this.getRequestForSaving();
            this.adminMasterService.postOfferMaster(offerMasterSave).subscribe(res => {
                var getResponse = res as ResponseData;
                if (getResponse.statusCode == 201) {
                    if (getResponse.body != null) {
                        console.log(getResponse.body);
                        alert('Your record has been saved successfully');
                        this.dialogRef.close();
                    }
                }
            }, err => {
                console.log(err);
            });

            return;
        }
    }

    update() {
        this.submitted = true;
        if (!this.form.invalid) {
            var offerMasterUpdate = this.getRequestForSaving();
            var id = this.isEditId;
            this.adminMasterService.putOfferMaster(offerMasterUpdate, id).subscribe(res => {
                console.log(res);
                alert('Your record has been updated successfully');
                this.dialogRef.close();
            }, err => {
                console.log(err);
            })
        }
    }

    /**
     * toggleCarPurchase
     */
    toggleCarPurchase() {
        this.isHiddenCarPurchase = !this.isHiddenCarPurchase;
    }

    /**
     * toggleListFinance
     */
    toggleListFinance() {
        this.isHiddenListFinance = !this.isHiddenListFinance;
    }

    /**
     * toggleListLease
     */
    toggleListLease() {
        this.isHiddenListLease = !this.isHiddenListLease;
    }

    /**
     * Get Category Master
     * getCategoryMasters
     */
    getCategoryMasters() {
        this.subsCategory = this.adminMasterService.getCategoryMaster().subscribe(
            (res: any) => {
                this.categoryList = res.results;
            },
            err => {
                console.error(err);
            });
    }

    /**
     * onCountrySelect
     */
    onCategorySelect(countryId: number) {
        this.subsBrand = this.adminMasterService.getBrandMasterByCatId(countryId).subscribe(
            (res: any) => {
                this.brandList = res.results;
            },
            err => {
                console.error(err);
            });
    }

    /**
     * onBrandSelect
     */
    onBrandSelect(brandId: number) {
        this.subsModel = this.adminMasterService.getModelMasterByBrandId(brandId).subscribe(
            (res: any) => {
                this.modelList = res.results;
            },
            err => {
                console.error(err);
            });
    }

    /**
     * getCountryMaster
     */
    getCountryMaster() {
        this.subsCountry = this.adminMasterService.getCountryMaster().subscribe(
            (res: any) => {
                this.countryList = res.results;
            },
            err => {
                console.error(err);
            });
    }

    /**
     * getLocationMaster
     */
    getLocationMaster() {
        this.subsLocation = this.adminMasterService.getLocationMaster().subscribe(
            (res: any) => {
                this.locationList = res.results;
            },
            err => {
                console.error(err);
            });
    }
}

