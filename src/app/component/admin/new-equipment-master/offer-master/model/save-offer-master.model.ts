export class SaveOfferMaster{  
    category?: number;
    brand?: number;
    model?: number;
    country?: number;
    locations?: any[] = [];
    is_new?: boolean;
    is_cash?: boolean;
    is_finance?: boolean;
    finance?: string;
    is_lease?: boolean;
    lease?: string;
    cash_purchases?: any[] = [];
    finance_or_leases?: any[]  = []; 
}
export class SaveOfferMasterResponse{
    is_new?: boolean;
    is_cash?: boolean;
    is_finance?: boolean;
    finance?: string;
    is_lease?: boolean;
    lease?: string;
    cash_purchases?: any[] = [];
    finance_or_leases?: any[] = [];     
}