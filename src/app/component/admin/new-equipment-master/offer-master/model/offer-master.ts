export interface OfferMaster {
    id:number;
    is_new:boolean;
    Category: number;
    Brand: number;
    Model: number;
    country:number;
    locations:[];
    cash_purchase:[];
    finance_lease:[];
    lessonsCount:number;
}