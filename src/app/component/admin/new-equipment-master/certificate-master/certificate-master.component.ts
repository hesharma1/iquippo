import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from '@angular/material/table';
import { CertificateMasterDataModel, AddCertificateRequest } from 'src/app/models/admin/new-equipment-master/certificate-master/certificate-master.model';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs/operators';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { ConfirmDialogComponent } from '../location-master/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-certificate-master',
  templateUrl: './certificate-master.component.html',
  styleUrls: ['./certificate-master.component.css']
})

export class CertificateMasterComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild("fileInputLogo") fileInputLogo!: ElementRef; //file inputs used in certificate
  @ViewChild("fileInputPrimaryImg") fileInputPrimaryImg!: ElementRef;//file inputs used in certificate

  public tblCertificateColumns = ["id", "name", "order", "used_equipment_count", "new_equipment_count", "Actions"];
  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public certificateMasterDataModel: CertificateMasterDataModel;
  public savedLocation = '';
  public searchText!: string;
  public pageSize: number = 10;
  public activePage: any = 1;
  public ordering: string = '-id';
  public showForm: boolean = false;
  public total_count: any;
  public listingData = [];
  // Certificate form controls
  public addCertificateForm: any = this.formbuilder.group({
    // certificateName: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    certificateDesc: new FormControl(''),
    sortBy: new FormControl(''),
    seoInfo: new FormControl(''),
    gSeoTitle: new FormControl(''),
    gSeoMetaKey: new FormControl(''),
    gSeoMetaDes: new FormControl(''),
    certificate_logo_image: new FormControl('')
  });

  constructor(private formbuilder: FormBuilder, private adminMasterService: AdminMasterService, private apiPath: ApiRouteService, 
    public s3: S3UploadDownloadService, private notify: NotificationService, private dialog: MatDialog) {
    this.certificateMasterDataModel = new CertificateMasterDataModel();
  }

  ngOnInit(): void {
    this.getCertificateList(); //for getting Group records
  }

  //for getting certificate data
  getCertificateList() {
    let payload: any = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.adminMasterService.getCertificateMasterList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res != null) {
          this.total_count = res.count;
          // Changes for Card layout on Mobile devices
         if(window.innerWidth > 768){
          this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.activePage == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
        }
      });
  }
  scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
        this.getCertificateList();
        }
      }
    }
  }
}
  //common function for handling different checkbox events
  addSEOInfo(isChecked: boolean) {
    if (isChecked) {
      this.certificateMasterDataModel.addSEOInfoGroup = true;
      this.addCertificateForm.patchValue({
        gSeoTitle: '', gSeoMetaKey: '', gSeoMetaDes: ''
      });
      this.addCertificateForm.get('gSeoTitle').setValidators([Validators.required]);
      this.addCertificateForm.get('gSeoTitle').updateValueAndValidity();
      this.addCertificateForm.get('gSeoMetaKey').setValidators([Validators.required]);
      this.addCertificateForm.get('gSeoMetaKey').updateValueAndValidity();
      this.addCertificateForm.get('gSeoMetaDes').setValidators([Validators.required]);
      this.addCertificateForm.get('gSeoMetaDes').updateValueAndValidity();
    }
    else {
      this.certificateMasterDataModel.addSEOInfoGroup = false;
      this.addCertificateForm.patchValue({
        gSeoTitle: '', gSeoMetaKey: '', gSeoMetaDes: ''
      });
      this.addCertificateForm.get('gSeoTitle').clearValidators();
      this.addCertificateForm.get('gSeoTitle').updateValueAndValidity();
      this.addCertificateForm.get('gSeoMetaKey').clearValidators();
      this.addCertificateForm.get('gSeoMetaKey').updateValueAndValidity();
      this.addCertificateForm.get('gSeoMetaDes').clearValidators();
      this.addCertificateForm.get('gSeoMetaDes').updateValueAndValidity();
    }
  }

  // add/update group data
  addCertificate() {
    this.addCertificateForm.markAllAsTouched();
    if (this.addCertificateForm.valid) {
      const formData = this.addCertificateForm.value;
    
      this.certificateMasterDataModel.addCertificateRequest = new AddCertificateRequest();
      this.certificateMasterDataModel.addCertificateRequest.name = formData.name;
      this.certificateMasterDataModel.addCertificateRequest.order = (formData.sortBy == null || formData.sortBy == '') ? 0 : formData.sortBy;
      this.certificateMasterDataModel.addCertificateRequest.certificate_desc = formData.certificateDesc;
      this.certificateMasterDataModel.addCertificateRequest.add_seo = (formData.seoInfo == null || formData.seoInfo === '') ? false : formData.seoInfo;
      this.certificateMasterDataModel.addCertificateRequest.seo_title = formData.gSeoTitle;
      this.certificateMasterDataModel.addCertificateRequest.seo_meta_keywords = formData.gSeoMetaKey;
      this.certificateMasterDataModel.addCertificateRequest.seo_meta_desc = formData.gSeoMetaDes;
      this.certificateMasterDataModel.addCertificateRequest.certificate_logo_image = formData.certificate_logo_image;
      this.adminMasterService.createUpdateCertificateMasterData(this.certificateMasterDataModel.addCertificateRequest, this.apiPath.certificateMaster, this.certificateMasterDataModel.actionName, this.certificateMasterDataModel.id).subscribe(
        res => {
          if (res != null) {
            this.certificateMasterDataModel.addCertificateResponse = res as any;
            this.getCertificateList();
            this.addCertificateForm.reset();
            this.showForm = false;
            this.certificateMasterDataModel.id = 0;
            this.notify.success('Data ' + this.certificateMasterDataModel.actionName + 'd Successfully.');
            this.certificateMasterDataModel.actionName = "Save";
          }
        });
  }
  }

  // function for edit and delete certificate
  action(actionType: string, id: number) {
    // for certificate edit
    if (actionType === 'Edit') {
      this.adminMasterService.getCertificateMasterData(`${this.apiPath.certificateMaster}${id}/`).subscribe(
        res => {
          if (res != null) {
            this.certificateMasterDataModel.addCertificateResponse = res as any;
            this.showForm = true;
            this.certificateMasterDataModel.id = id;
            this.certificateMasterDataModel.actionName = 'Update';
            this.addSEOInfo(this.certificateMasterDataModel.addCertificateResponse.add_seo);
            this.addCertificateForm.setValue({
              name: this.certificateMasterDataModel.addCertificateResponse.name,
              certificateDesc: this.certificateMasterDataModel.addCertificateResponse.certificate_desc,
              sortBy: this.certificateMasterDataModel.addCertificateResponse.order,
              seoInfo: this.certificateMasterDataModel.addCertificateResponse.add_seo,
              gSeoTitle: this.certificateMasterDataModel.addCertificateResponse.seo_title,
              gSeoMetaKey: this.certificateMasterDataModel.addCertificateResponse.seo_meta_keywords,
              gSeoMetaDes: this.certificateMasterDataModel.addCertificateResponse.seo_meta_desc,
              certificate_logo_image: this.certificateMasterDataModel.addCertificateResponse.certificate_logo_image
            });
          }
        });
    }
    else if (actionType === 'Delete') {// for certificate delete

      this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.adminMasterService.deleteCertificateMasterData(`${this.apiPath.certificateMaster}${id}/`).subscribe(
            res => {
              if (res == null) {
                this.addCertificateForm.reset();
            this.showForm = false;
            this.certificateMasterDataModel.id = 0;
            this.certificateMasterDataModel.actionName = "Save";
                this.notify.success('Data Deleted Successfully.');
                this.getCertificateList();
              }
            });
        }
      });

     
    }

  }

  /**search list  */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.activePage = 1;
      this.getCertificateList();
    }
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getCertificateList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getCertificateList();
  }

  closeForm() {
    this.addCertificateForm.reset();
    this.certificateMasterDataModel.actionName = "Save";
    this.showForm = false;
  }

  export() {
    let payload = {
      limit: 999
    }
    this.adminMasterService.getCertificateMasterList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Certificate Name": r?.name,
            "Order": r?.order,
            "Used Equipment Count": r?.used_equipment_count,
            "New Equipment Count": r?.new_equipment_count,
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Certificate List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  /**upload files and video by this method */
  async handleUpload(event: any, keyName: string) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/marketplace/auction/" + uuidv4() + '_' + keyName + '_' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.addCertificateForm.get(keyName)?.setValue(uploaded_file.Location);
      }
    } else {
      this.notify.error("Invalid Image.")
    }
    this.fileInputLogo.nativeElement.value = '';
  }
  getFileName(keyname, str) {
    return str.split(keyname).pop();
  }
  deleteFile(key, imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.addCertificateForm.get(key)?.setValue('');
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }
}
