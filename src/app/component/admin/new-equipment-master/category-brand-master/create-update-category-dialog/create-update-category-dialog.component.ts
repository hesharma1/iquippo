import { Component, Inject, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { input } from "@aws-amplify/ui";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { UsedEquipmentService } from "src/app/services/used-equipment.service";
import { S3UploadDownloadService } from "src/app/utility/s3-upload-download/s3-upload-download.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-create-update-category-dialog-component',
  templateUrl: "./create-update-category-dialog.component.html",
  styleUrls: ["./create-update-category-dialog.component.css"]
})
export class CreateUpdateCategoryDialogComponent {
  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();
  @ViewChild('fileInputCatImg') fileInputCatImg!: ElementRef; //file inputs used in category
  @ViewChild('fileInputCatVectorImg') fileInputCatVectorImg!: ElementRef; //file inputs used in category
  savedLocation: string = '';
  form: FormGroup;
  StrOutputMSG: boolean = false;
  imgName: string = "";
  imgVectorName: string = "";
  isImgName: boolean = false;
  isImgVectorName: boolean = false;
  data;
  editableData: any;
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  documentName: Array<any> = [];
  @Input() public value: any;
  imageName: any;
  categoryId: number = 0;
  Close() {
    this.ChdOutput.emit(); //this.StrOutputMSG;
  }

  constructor(
    private fb: FormBuilder, private adminMasterService: AdminMasterService, public s3: S3UploadDownloadService,
    private notify: NotificationService, private spinner: NgxSpinnerService, private usedEquipmentService: UsedEquipmentService,
    private notificationservice: NotificationService,) {
    this.form = this.fb.group({
      CategoryName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      CategoryDisplayName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Status: [1, Validators.required],
      // usedPriority: [""],
      // newPriority: [""],
      vector_img: [""],
      priority: ["",Validators.required],
       
    });
  }

  ngOnInit() {
    if (this.value) {
      this.editableData = this.value;
      if (this.editableData && this.editableData.ImageUrl) {
        this.isImgName = true;
      }
      else {
        this.isImgName = false;
      }
      if(this.editableData && this.editableData.ImageUrl){
        this.isImgVectorName = true;
      }else{
        this.isImgVectorName = false;
      }
      this.form = this.fb.group({
        CategoryName: [this.editableData.CategoryName, Validators.compose([Validators.required, Validators.maxLength(50)])],
        CategoryDisplayName: [this.editableData.CategoryDisplayName, Validators.compose([Validators.required, Validators.maxLength(50)])],
        Status: [this.editableData.Status, Validators.required],
        // usedPriority: [this.editableData.priority_for_use],
        // newPriority: [this.editableData.priority_for_new],
        vector_img:[this.editableData.vector_img],
        priority: [this.editableData.priority,Validators.required]

      });
      this.categoryId = this.editableData.CategoryId;
      this.value = '';
    }
    else {
      this.form = this.fb.group({
        CategoryName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        CategoryDisplayName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        Status: [1, Validators.required],
        vector_img: [""],
        // usedPriority: [""],
        // newPriority: [""],
        priority: ["",Validators.required]

      })
    }

    /*var ds = localStorage.getItem("ds");
    var ds = localStorage.getItem("ds");
      if (ds != undefined)
      {
        var JData = eval(ds[0]);
        this.form = this.fb.group({
          CategoryName: [JData.CategoryName, Validators.compose([Validators.required, Validators.maxLength(50)])],
          CategoryDisplayName: [JData.CategoryDisplayName, Validators.compose([Validators.required, Validators.maxLength(50)])],
          Status: [JData.Status, Validators.required]
        })
      }
      else
      {
        this.form = this.fb.group({
          CategoryName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
          CategoryDisplayName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
          Status: [1, Validators.required]
        })
      }*/
  }


  backClick() {
    // this.dialogRef.close();
  }

  okClick() {
    this.data = {};
    const val = {
      ...this.data,
      ...this.form.value
    }

    console.log("val", val);

    if (this.form.value) {
      let fileName: string = val.CategoryName;
      this.imageUpload(
        this.form,
        this.fileInputCatImg,
        'MasterData',
        fileName
      ).then((res) => {
        const postBody = {
          name: val.CategoryName,
          display_name: val.CategoryDisplayName,
          status_id: val.Status,
          img_url: this.savedLocation ? this.savedLocation : this.editableData && this.editableData?.ImageUrl ? this.editableData?.ImageUrl : '',
          // priority_for_use: val.usedPriority,
          // priority_for_new: val.newPriority,
          priority: val.priority,
          vector_img: val.vector_img
          
        }
        console.log("postalbosy", postBody);
        if (this.categoryId) {
          this.adminMasterService.putCategoryMaster(postBody, this.categoryId).subscribe(resp => {
            this.Close();
          })
        } else {
          this.adminMasterService.postCategoryMaster(postBody).subscribe(resp => {
            this.Close();
          });
        }

      });
    }
    /*this.dialogRef.close(val);*/
  }

  getFileName(type) {
    if (type == 'fileInputCatImg') {
      if (this.fileInputCatImg && this.fileInputCatImg.nativeElement && this.fileInputCatImg.nativeElement.files && this.fileInputCatImg.nativeElement.files[0]) {
        this.imgName = this.fileInputCatImg.nativeElement.files[0].name;
        this.isImgName = true;
        // return this.fileInputCatImg.nativeElement.files[0].name;
      }
      return '';
    }
    return '';
  }

  resetFile(type) {
    if (type == 'fileInputCatImg') {
      this.imgName = '';
      this.isImgName = false;
      this.fileInputCatImg.nativeElement.value = "";
    }
    if(type == 'vector_img'){
      this.form.get('vector_img')?.setValue('');
    }
  }

  async imageUpload(frmGroup: any, image: any, tab: string, side: string) {
    this.savedLocation = '';
    const file = image.nativeElement.files[0];
    if (file != null) {
      if (
        (file.type == 'image/png' ||
          file.type == 'image/jpg' ||
          file.type == 'image/jpeg' ||
          file.type == 'image/bmp') &&
        file.size <= 5242880
      ) {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue(reader.result);
        };
        var fileFormat = (file?.name).toString();
        var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

        var image_path =
          environment.bucket_parent_folder +
          '/' +
          tab +
          '/' +
          side +
          '.' +
          fileExt;

        var uploaded_file = await this.s3.prepareFileForUpload(
          file,
          image_path
        );
        if (
          uploaded_file?.Key != undefined &&
          uploaded_file?.Key != '' &&
          uploaded_file?.Key != null
        ) {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side)
            ?.setValue(uploaded_file?.Key);
          this.savedLocation = uploaded_file?.Location;
        } else {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue('');
          this.notify.error('Upload Failed');
        }
      } else {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setValue('');
        frmGroup
          .get(tab)
          ?.get('docImages')
          ?.get(side)
          ?.setErrors({ imgIssue: true });
      }
    }
  }

  delete(url) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    console.log("imgpath", imgPath);
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notificationservice.success('File Successfully deleted!!');
        this.editableData.ImageUrl = "";
        this.savedLocation = "";
        this.isImgName = false;
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }
  async handleVectorUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' +this.form.get('CategoryName')?.value + "_" + "vector_image" + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
    console.log(uploaded_file);
    
    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      this.form.get('vector_img')?.setValue( uploaded_file.Location);
      console.log(this.documentName)
      if (tab == 'vector_img') {
        this.imageName = uploaded_file.Location;
        
      }


      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }
  resetVectorFile() {
    this.documentName = [];
    this.form?.get('vector_img')?.setValue(null);
  }
  getVectorFileName(keyname, str) {
    return str.split(keyname).pop();
  }
}
