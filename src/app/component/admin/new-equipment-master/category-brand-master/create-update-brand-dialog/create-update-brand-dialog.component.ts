import { Component, Inject, Output, EventEmitter, ViewChild, ElementRef, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { UsedEquipmentService } from "src/app/services/used-equipment.service";
import { S3UploadDownloadService } from "src/app/utility/s3-upload-download/s3-upload-download.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-create-update-brand-dialog",
  templateUrl: "./create-update-brand-dialog.component.html",
  styleUrls: ["./create-update-brand-dialog.component.css"]
})
export class CreateUpdateBrandDialogComponent {
  form: FormGroup;

  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();
  @ViewChild('fileInputBrandImg') fileInputBrandImg!: ElementRef; //file inputs used in category
  @ViewChild('fileInputBrandLogoImg') fileInputBrandLogoImg!: ElementRef; //file inputs used in category

  brandImgName: string = '';
  brandLogoImgName: string = '';
  StrOutputMSG: boolean = false;
  brandId;
  savedLocation: string = '';
  @Input() public value: any;
  editableBrandData: any;
  brandImage: string = '';
  isBrandLogoImage: boolean = false;
  isBrandImg: boolean = false;

  Close() {
    this.ChdOutput.emit();//this.StrOutputMSG);
    //this.dialogRef.close();
  }

  constructor(public dialogRef: MatDialogRef<CreateUpdateBrandDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder, private adminMasterService: AdminMasterService, public s3: S3UploadDownloadService,
    private notify: NotificationService, private spinner: NgxSpinnerService, private usedEquipmentService: UsedEquipmentService,
    private notificationservice: NotificationService,) {
    this.form = this.fb.group({
      BrandName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      BrandDisplayName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Status: [1, Validators.required],
      // usedPriority: [""],
      // newPriority: [""],
      priority: ["",Validators.required],
    })

    /* if(data) {
      this.form.patchValue(data);
    } */
    // var BrandData = localStorage.getItem("BrandData");
  }

  ngOnInit() {
    if (this.value) {

      this.editableBrandData = this.value;

      if (this.editableBrandData && this.editableBrandData.BrandImageUrl) {
        this.isBrandImg = true;
      }
      else {
        this.isBrandImg = false;
      }
      if (this.editableBrandData && this.editableBrandData.BrandLogoImageUrl) {
        this.isBrandLogoImage = true;
      }
      else {
        this.isBrandLogoImage = false;
      }

      this.form?.patchValue(this.editableBrandData);
      this.brandId = this.editableBrandData.BrandId;
      this.value = '';
      //localStorage.clear();
      //localStorage.removeItem("BrandData");
    }
    else {
      this.form = this.fb.group({
        BrandName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        BrandDisplayName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        Status: [1, Validators.required],
        // usedPriority: [""],
        //newPriority: [""],
        priority: ["",Validators.required],
      })
    }
  }


  backClick() {
    this.dialogRef.close();
  }

  okClick() {
    const val = {
      ...this.data,
      ...this.form.value
    }
    if (this.form.value) {
      let fileName: string = val.BrandName;

      this.imageUpload(
        this.form,
        this.fileInputBrandImg,
        'MasterData',
        fileName
      ).then((res) => {
        fileName = val.BrandName + '_logo';
        this.brandImage = this.savedLocation;
        this.imageUpload(
          this.form,
          this.fileInputBrandLogoImg,
          'MasterData',
          fileName
        ).then((res) => {
          const postBody = {
            name: val.BrandName,
            display_name: val.BrandDisplayName,
            status_id: val.Status,
            brand_logo_image: this.savedLocation ? this.savedLocation : this.editableBrandData && this.editableBrandData.BrandLogoImageUrl ? this.editableBrandData.BrandLogoImageUrl : '',
            brand_image: this.brandImage ? this.brandImage : this.editableBrandData && this.editableBrandData.BrandImageUrl ? this.editableBrandData.BrandImageUrl : '',
            // priority_for_use: val.usedPriority,
            // priority_for_new: val.newPriority
            priority: val.priority,
          }

          if (this.brandId) {
            this.adminMasterService.putBrandMaster(postBody, this.brandId).subscribe(resp => {
              this.Close();
            });
          } else {
            this.adminMasterService.postBrandMaster(postBody).subscribe(resp => {
              this.Close();
            });
          }
        });
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    );


    }
    //this.dialogRef.close(val);
  }

  resetFile(type) {
    if (type == 'fileInputBrandImg') {
      this.brandImgName = "";
      this.isBrandImg = false;
      this.fileInputBrandImg.nativeElement.value = "";
    }
    if (type == 'fileInputBrandLogoImg') {
      this.brandLogoImgName = "";
      this.isBrandLogoImage = false;
      this.fileInputBrandLogoImg.nativeElement.value = "";
    }
  }

  getFileName(type) {
    if (type == 'fileInputBrandImg') {
      if (this.fileInputBrandImg && this.fileInputBrandImg.nativeElement && this.fileInputBrandImg.nativeElement.files && this.fileInputBrandImg.nativeElement.files[0]) {
        this.brandImgName = this.fileInputBrandImg.nativeElement.files[0].name;
        this.isBrandImg = true;
        //return this.fileInputBrandImg.nativeElement.files[0].name;
      }
      return '';
    }

    if (type == 'fileInputBrandLogoImg') {
      if (this.fileInputBrandLogoImg && this.fileInputBrandLogoImg.nativeElement && this.fileInputBrandLogoImg.nativeElement.files && this.fileInputBrandLogoImg.nativeElement.files[0]) {
        this.brandLogoImgName = this.fileInputBrandLogoImg.nativeElement.files[0].name;
        this.isBrandLogoImage = true;
        return this.fileInputBrandLogoImg.nativeElement.files[0].name;

      }
      return '';
    }
    return '';
  }

  async imageUpload(frmGroup: any, image: any, tab: string, side: string) {
    this.savedLocation = '';
    const file = image.nativeElement.files[0];
    if (file != null) {
      if (
        (file.type == 'image/png' ||
          file.type == 'image/jpg' ||
          file.type == 'image/jpeg' ||
          file.type == 'image/bmp') &&
        file.size <= 5242880
      ) {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue(reader.result);
        };
        var fileFormat = (file?.name).toString();
        var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

        var image_path =
          environment.bucket_parent_folder +
          '/' +
          tab +
          '/' +
          side +
          '.' +
          fileExt;

        var uploaded_file = await this.s3.prepareFileForUpload(
          file,
          image_path
        );
        if (
          uploaded_file?.Key != undefined &&
          uploaded_file?.Key != '' &&
          uploaded_file?.Key != null
        ) {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side)
            ?.setValue(uploaded_file?.Key);
          this.savedLocation = uploaded_file?.Location;
        } else {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue('');
          this.notify.error('Upload Failed');
        }
      } else {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setValue('');
        frmGroup
          .get(tab)
          ?.get('docImages')
          ?.get(side)
          ?.setErrors({ imgIssue: true });
      }
    }
  }

  deleteImage(type, url) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    console.log("imgpath", imgPath);
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notificationservice.success('File Successfully deleted!!');
        if (type == 'BrandImage') {
          this.editableBrandData.BrandImageUrl = "";
          this.brandImage = "";
          this.isBrandImg = false;
        }
        else {
          this.editableBrandData.BrandLogoImageUrl = "";
          this.isBrandLogoImage = false;
          this.savedLocation = "";
        }
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    );
  }

}
