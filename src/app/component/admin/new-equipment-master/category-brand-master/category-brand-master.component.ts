import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ConfirmDialogComponent } from '../location-master/confirm-dialog/confirm-dialog.component';
import { CreateUpdateBrandDialogComponent } from './create-update-brand-dialog/create-update-brand-dialog.component';
import { CreateUpdateCategoryDialogComponent } from './create-update-category-dialog/create-update-category-dialog.component';


@Component({
  templateUrl: './category-brand-master.component.html',
  styleUrls: ['./category-brand-master.component.css']
})
export class CategoryBrandMasterComponent implements OnInit {
  showForm: boolean = false;
  showBrandForm: boolean = false;
  StrParentInput: any;
  ReqJData: any = [];
  brandData: any[] = [];
  categoryData: any[] = [];
  brandDataSource = new MatTableDataSource<any>([]);
  categoryDataSource = new MatTableDataSource<any>([]);
  brandDisplayedColumns = ["id", "brand_image", "brand_logo_image", "name", "display_name", "status", "actions"];
  categoryDisplayedColumns = ["id", "img_url", "name", "display_name", "status", "actions"]
  status: { [key: string]: number } = {
    "Rejected": 0,
    "Active": 1,
    "Inactive": 2
  }
  objCat: any;
  objBrand: any;
  isShowCategory: boolean = true;
  isShowBrand: boolean = true;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public catListingData = [];
  public brandListingData = [];
  public searchText!: string;
  public brandsearchText!: string;
  public currentTab = 0;
  constructor(private dialog: MatDialog, private adminMasterService: AdminMasterService, private apiPath: ApiRouteService,
    private notify: NotificationService) {
    this.showForm = false;
    this.showBrandForm = false;
  }

  ngOnInit() {
    this.getBrandData();
    this.getCategoryData();
  }

  getBrandData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.brandsearchText) {
      payload['search'] = this.brandsearchText;
    }
    this.adminMasterService.getBrandMasterList(payload).subscribe((res: any) => {
      this.brandData = res.results;
      if (!this.brandsearchText) {
        if (this.brandData && this.brandData.length > 0) {
          this.isShowBrand = true;
        }
        else {
          this.isShowBrand = false;
        }
      }
      this.total_count = res.count;
      if (window.innerWidth > 768) {
        this.brandDataSource.data = res.results;
      } else {
        this.scrolledBrand = false;
        if (this.page == 1) {
          this.brandDataSource.data = [];
          this.brandListingData = [];
          this.brandListingData = res.results;
          this.brandDataSource.data = this.brandListingData;
        } else {
          this.brandListingData = this.brandListingData.concat(res.results);
          this.brandDataSource.data = this.brandListingData;
        }
      }
    });
  }

  getCategoryData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.adminMasterService.getCategoryMasterList(payload).subscribe((res: any) => {
      this.categoryData = res.results;
      if (!this.searchText) {
        if (this.categoryData && this.categoryData.length > 0) {
          this.isShowCategory = true;
        }
        else {
          this.isShowCategory = false;
        }
      }

      this.total_count = res.count;
      if (window.innerWidth > 768) {
        this.categoryDataSource.data = res.results;
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.categoryDataSource.data = [];
          this.catListingData = [];
          this.catListingData = res.results;
          this.categoryDataSource.data = this.catListingData;
        } else {
          this.catListingData = this.catListingData.concat(res.results);
          this.categoryDataSource.data = this.catListingData;
        }
      }
    });
  }

  onCatSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getCategoryData();
  }

  onBrandSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getBrandData();
  }

  onPageChange(event: PageEvent, type) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    if (type == 'categoryData') {
      this.getCategoryData();
    }
    else {
      this.getBrandData();
    }
  }

  searchBrand() {
    if (this.brandsearchText === '' || this.brandsearchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getBrandData();
    }
  }

  searchCategory() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getCategoryData();
    }
  }

  exportData(type) {
    if (type === 'brandMaster') {
      ExportExcelUtil.exportArrayToExcel(this.brandDataSource.data, "brand-master");
    }
    else {
      ExportExcelUtil.exportArrayToExcel(this.categoryDataSource.data, "category-master");
    }

  }

  GetChildData(BoolShowForm: boolean, StrAction: string, event, SelectedTblIndex: number, id: number) {
    this.showForm = false;
    this.showBrandForm = false;

    if (StrAction == 'CategoryCreate' || StrAction == 'CategoryEdit') {
      setTimeout(() => {
        this.showForm = BoolShowForm;
      }, 100);
    }
    else {
      setTimeout(() => {
        this.showBrandForm = BoolShowForm;
      }, 100);
    }

    if (StrAction == 'CategoryCreate') {
      this.objCat = '';
      this.getCategoryData();
    }
    else if (StrAction == 'BrandCreate') {
      this.objBrand = '';
      this.getBrandData();
    }

    else if (StrAction == 'CategoryEdit') {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      this.objCat = '';
      let index = this.categoryData.findIndex(e => e.id == id);
      index = index !== -1 ? index : SelectedTblIndex;
      const category = this.categoryData.find(x => x.id == id);
      //const category = this.categoryData[index];
      if (category) {
        this.objCat = {
          CategoryName: category.name,
          CategoryDisplayName: category.display_name,
          Status: category.status_id,
          CategoryId: category.id,
          ImageUrl: category.img_url ? category.img_url : '',
          priority: category.priority,
          vector_img: category.vector_img
        }
        console.log("category", this.objCat);
      }
    }
    else if (StrAction == 'BrandEdit') {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      this.objBrand = '';
      //  const BrandData = this.brandDataSource.filteredData[SelectedTblIndex];
      let index = this.brandData.findIndex(e => e.id == id);
      index = index !== -1 ? index : SelectedTblIndex;
      const BrandData = this.brandData.find(x => x.id == id);
      if (BrandData) {
        this.objBrand = {
          BrandName: BrandData.name,
          BrandDisplayName: BrandData.display_name,
          Status: BrandData.status_id,
          BrandId: BrandData.id,
          BrandImageUrl: BrandData.brand_image ? BrandData.brand_image : '',
          BrandLogoImageUrl: BrandData.brand_logo_image ? BrandData.brand_logo_image : '',
          priority: BrandData.priority
        }
        console.log("objBrand", this.objBrand);
      }
    }

    else if (StrAction == 'CategoryDelete') {

      this.dialog
        .open(ConfirmDialogComponent, {
          width: '500px'
        })

        .afterClosed()
        .subscribe((data) => {
          if (data) {
            this.adminMasterService
              .deleteMasterData(`${this.apiPath.categoryMaster}${id}`)
              .subscribe((res) => {
                if (res == null) {
                  this.notify.success('Data Deleted Successfully.');
                  this.getCategoryData();
                }
              });
          }
        });

    }
    else if (StrAction == 'BrandDelete') {
      this.dialog
        .open(ConfirmDialogComponent, {
          width: '500px'
        }).afterClosed()
        .subscribe((data) => {
          if (data) {
            this.adminMasterService
              .deleteMasterData(`${this.apiPath.brandMaster}${id}`)
              .subscribe((res) => {
                if (res == null) {
                  this.notify.success('Data Deleted Successfully.');
                  this.getBrandData();
                }
              });
          }
        });



    }
  }

  ChdInput() {
    var i = 1;
    let index = this.categoryData.findIndex(e => e.id == i);
    index = index !== -1 ? index : i;
    const category = this.categoryData[index];

    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(CreateUpdateCategoryDialogComponent, {
      width: '500px',
      data: {
        CategoryName: category.name,
        CategoryDisplayName: category.display_name,
        Status: category.status_id
      }
    })
  }

  createUpdateCategory(i?: number) {
    if (i !== undefined) {
      //let index = i - 1;
      // const category = this.categoryData[i];
      let index = this.categoryData.findIndex(e => e.id == i);
      index = index !== -1 ? index : i;
      const category = this.categoryData[index];
      const dialogRef = this.dialog.open(CreateUpdateCategoryDialogComponent, {
        width: '500px',
        data: {
          CategoryName: category.name,
          CategoryDisplayName: category.display_name,
          Status: category.status_id
        }
      })
        .afterClosed()
        .subscribe(data => {
          if (data !== undefined) {
            const putBody = {
              name: data.CategoryName,
              display_name: data.CategoryDisplayName,
              status_id: data.Status
            }
            this.adminMasterService.putCategoryMaster(putBody, category.id).subscribe(resp => {
              this.categoryData[i] = resp;
              this.categoryDataSource = new MatTableDataSource<any>(this.categoryData);
            })
          }
        });
    } else {
      const dialogRef = this.dialog.open(CreateUpdateCategoryDialogComponent)
        .afterClosed()
        .subscribe(data => {
          if (data) {
            const postBody = {
              name: data.CategoryName,
              display_name: data.CategoryDisplayName,
              status_id: data.Status
            }
            this.adminMasterService.postCategoryMaster(postBody).subscribe(resp => {
              this.categoryData.push(resp);
              this.categoryDataSource = new MatTableDataSource<any>(this.categoryData);
            })
          }
        });
    }
  }

  createUpdateBrand(i?: number) {
    if (i !== undefined) {
      let index = this.brandData.findIndex(e => e.id == i);
      index = index !== -1 ? index : i;
      // const brand = this.brandData[i]
      const brand = this.brandData[index];
      const dialogRef = this.dialog.open(CreateUpdateBrandDialogComponent, {
        width: '500px',
        data: {
          BrandName: brand.name,
          BrandDisplayName: brand.display_name,
          Status: brand.status_id
        }
      })
        .afterClosed()
        .subscribe(data => {
          if (data !== undefined) {
            const putBody = {
              name: data.BrandName,
              display_name: data.BrandDisplayName,
              status_id: data.Status
            }
            this.adminMasterService.putBrandMaster(putBody, brand.id).subscribe(resp => {
              this.brandData[i] = resp;
              this.brandDataSource = new MatTableDataSource<any>(this.brandData);
            });
          }
        });
    } else {
      const dialogRef = this.dialog.open(CreateUpdateBrandDialogComponent, {
        width: '500px'
      })

        .afterClosed()
        .subscribe(data => {
          if (data) {
            const postBody = {
              name: data.BrandName,
              display_name: data.BrandDisplayName,
              status_id: data.Status
            }
            this.adminMasterService.postBrandMaster(postBody).subscribe(resp => {
              this.brandData.push(resp);
              this.brandDataSource = new MatTableDataSource<any>(this.brandData);
            });

          }
        });
    }
  }

  onListTabChanged(event) {
    this.currentTab = event?.index;
    console.log(this.currentTab)
  }
  scrolled = false;
  scrolledBrand = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (this.currentTab == 0) {
          if ((this.total_count > this.categoryDataSource.data.length) && (this.categoryDataSource.data.length > 0)) {
            if (!this.scrolled) {
              this.scrolled = true;
              this.page++;
              this.getCategoryData();
            }
          }
        }
        else {
          if ((this.total_count > this.brandDataSource.data.length) && (this.brandDataSource.data.length > 0)) {
            if (!this.scrolledBrand) {
              this.scrolledBrand = true;
              this.page++;
              this.getBrandData();
            }
          }
        }
      }

    }
  }
}


