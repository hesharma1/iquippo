import { OtpSubmitResponse } from 'src/app/models/common/otpcheck.model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationPopupComponent } from '../../confirmation-popup/confirmation-popup.component';
import { AddEditFieldComponent } from './dialogs/add-edit-field/add-edit-field.component';
import { AddEditSpecificationComponent } from './dialogs/add-edit-specification/add-edit-specification.component';
import { CategoryFieldSpec } from 'src/app/component/admin/new-equipment-master/technical-info-master-new/models/category-field.model';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { Category } from 'src/app/component/admin/new-equipment-master/technical-info-master-new/models/category.model';
import { Brand } from 'src/app/component/admin/new-equipment-master/technical-info-master-new/models/brand.model';
import { NewEquipmentMasterService } from 'src/app/services/new-equipment-master.service';
import { ViewBrandSpecDataComponent } from './dialogs/view-brand-spec-data/view-brand-spec-data.component';
import { ConfirmDialogComponent } from '../location-master/confirm-dialog/confirm-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-technical-info-master-new-list',
  templateUrl: './technical-info-master-new-list.component.html',
  styleUrls: ['./technical-info-master-new-list.component.css'],
})
export class TechnicalInformationMasterNewListComponent {
  constructor() {}
}
