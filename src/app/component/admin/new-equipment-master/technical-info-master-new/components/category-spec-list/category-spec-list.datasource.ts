import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';

import { catchError, finalize } from 'rxjs/operators';
import { CategorySpecMasterService } from './category-spec-list.service';

export class CategorySpecMasterDataSource implements DataSource<any> {
  private listSubject = new BehaviorSubject<any[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private categorySpecMasterService: CategorySpecMasterService) { }

  loadCategorySpecMaster(queryparam : string) {
    queryparam = "?" + queryparam;
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.categorySpecMasterService.getCategoryTechSpecificationList(queryparam).subscribe((list) => {
        this.listSubject.next(list);
        resolve(list);
      });
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listSubject.complete();
    this.loadingSubject.complete();
  }
}
