import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NewEquipmentMasterService } from 'src/app/services/new-equipment-master.service';
import { ConfirmDialogComponent } from '../../../location-master/confirm-dialog/confirm-dialog.component';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

@Component({
  selector: 'app-category-spec-list',
  templateUrl: './category-spec-list.component.html',
  styleUrls: ['./category-spec-list.component.css'],
})

export class CategorySpecListComponent implements OnInit {
  showForm: boolean = false;
  fieldArray: Array<any> = [
    {
      'category': 0,
      'field_name': '',
      'field_type': 1,
      'isField': false
    }
  ];
  newAttribute: any = {};
  firstField = true;
  firstFieldName = 'First Item name';
  isEditItems: boolean = true;
  dataSource1 = new MatTableDataSource<any>([]);
  dataSource: any = [];
  displayedColumns = [
    'category__name',
    'field_name',
    'field_type_name',
    'actions',
  ];
  @ViewChild('input') input!: ElementRef;
  categorySpecForm: FormGroup;
  fieldName: string = '';
  categories = [];
  filteredCategories = [];
  specList!: any;
  searchCatValue: any;
  testBool: boolean = false;
  editedFieldName: string = "";
  isEditField: boolean = false;
  editedCatId: any;
  editedFieldType: any;
  editedId: any;
  isFieldAdded: boolean = false;
  searchValue: string = '';
  selectedCat: any;
  selectedCategoryId: any;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;
  public currentTab = 0;

  constructor(private dialog: MatDialog, private fb: FormBuilder, public adminService: AdminMasterService, public newEquipmentService: NewEquipmentMasterService) {
    this.categorySpecForm = this.fb.group({
      Category: [null],
    });
    this.showForm = false;
  }

  ngOnInit() {
    let queryparams = 'limit=-1&ordering=-id';
    this.getCategoryMasterData(queryparams);
    this.getCategoryTechData();
  }

  getCategoryMasterData(queryparams) {
    this.adminService.getCategoryMaster(queryparams).subscribe((response: any) => {
      this.categories = response.results;
      this.filteredCategories = response.results;
    });
  }

  getCategoryTechData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.selectedCategoryId) {
      payload['category__id__in'] = this.selectedCategoryId;
    }
    this.adminService.getCategoryTechSpecificationList(payload).subscribe((res: any) => {
      this.total_count = res.count;
      this.dataSource = res.results;
      if (window.innerWidth > 768) {
        this.dataSource1.data = res.results;
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.dataSource1.data = [];
          this.listingData = [];
          this.listingData = res.results;
          this.dataSource1.data = this.listingData;
        } else {
          this.listingData = this.listingData.concat(res.results);
          this.dataSource1.data = this.listingData;
        }
      }
    });
  }

  filterValuesCategory(search: any) {
    let data = search.target.value;
    let filtered = this.filteredCategories;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        (item: any) => item?.display_name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categories = filteredData
    } else {
      this.categories = []
    }
  }

  addFieldValue(index) {
    if (this.fieldArray.length <= 50) {
      this.fieldArray.push(this.newAttribute);
      this.newAttribute = {};

      this.fieldArray.forEach((data, index) => {
        if (index === this.fieldArray.length - 1) {
          data.isField = false;
        }
        else {
          data.isField = true;
        }
      });
    } else {

    }
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
  }

  onEditCloseItems() {
    this.isEditItems = !this.isEditItems;
  }

  GetChildData(BoolShowForm: boolean, StrAction: string, element: any) //(data:boolean)
  {
    this.isEditField = true;
    if (StrAction == 'Create') {
    }
    else if (StrAction == 'Edit') {
      this.fieldName = element.field_name;
    }
  }

  change(event, id) {
    console.log(" event.source.value", event.source.value)
    console.log("id", id);
    if (event.isUserInput) {
      this.selectedCategoryId = id;
      this.isEditField = false;
      this.searchCatValue = event.source.value;
      let pageIndex = this.paginator.pageIndex + 1;
      this.paginator.pageIndex = 0;
      let queryparam = 'page=' + pageIndex + '&limit=-1&ordering=-id&category__id__in=' + id + '&search=' + this.searchValue;
      this.getCategoryTechData();
    }
  }

  checkField() {
    if (this.fieldArray && this.fieldArray.length > 0) {
      let isField = this.fieldArray.filter(x => x.field_name);
      if (isField && isField.length > 0) {
        this.isFieldAdded = true;
      }
      else {
        this.isFieldAdded = false;
      }
    }
  }


  exportToExcel() {
    let payload = {
      limit: -1,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.selectedCategoryId) {
      payload['category__id__in'] = this.selectedCategoryId;
    }
    this.adminService.getCategoryTechSpecificationList(payload).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "category name": r.category.display_name,
          "Field name": r.field_name,
          "Field type name": r.field_type_name,
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Category-Spec-List");
    },
      err => {
        console.log(err);
      }
    );
  }

  addField() {
    return this.fb.group({
      Field: [null],
    });
  }

  saveCategorySpec(): void {
    this.fieldArray = this.fieldArray.filter(x => x.field_name);
    this.fieldArray.forEach(data => {
      data.category = this.selectedCategoryId;
      data.field_type = 1;
    });
    this.newEquipmentService
      .createCategoryField(this.fieldArray)
      .subscribe((resp: any) => {
        this.searchCatValue = "";
        this.selectedCat = "";
        this.selectedCategoryId = "";
        this.fieldArray = [{ 'category': 0, 'field_name': '', 'field_type': 1, 'isField': false }];
        this.getCategoryTechData();
      });
  }

  editField(element: any): void {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.isEditField = true;
    this.editedFieldName = element.field_name;
    this.editedCatId = element.category.id;
    this.selectedCat = element.category.display_name;
    this.editedFieldType = element.field_type;
    this.editedId = element.id;
  }

  editFieldSubmit() {
    const postBody = {
      category: this.editedCatId,
      field_name: this.editedFieldName,
      field_type: this.editedFieldType,
    };
    this.newEquipmentService
      .updateCategoryField(postBody, this.editedId)
      .subscribe((resp: any) => {
        this.isEditField = false;
        this.editedCatId = '';
        this.editedFieldName = '';
        this.editedFieldType = '';
        this.editedId = '';
        this.selectedCat = ""
        this.selectedCategoryId = '';
        this.getCategoryTechData();
      });
  }

  deleteField(specId: number): void {
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.newEquipmentService
            .deleteCategoryField(specId)
            .subscribe(() => {
              this.getCategoryTechData();
            });
        }
      });
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getCategoryTechData();
  }

  search() {
    console.log("this.searchText", this.searchText);
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getCategoryTechData();
    }
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getCategoryTechData();
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if ((this.total_count > this.dataSource1.data.length) && (this.dataSource1.data.length > 0)) {
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getCategoryTechData();
          }
        }
      }
    }
  }
}
