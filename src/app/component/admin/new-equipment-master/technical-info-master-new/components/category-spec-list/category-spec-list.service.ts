import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Injectable({
  providedIn: 'root',
})
export class CategorySpecMasterService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService,
    private httpClient: HttpClient
  ) { }

  getCategoryTechSpecificationList(queryparam : string , queryParam?: any) {
    let params = new HttpParams();
    params = queryParam;
    return this.httpClient
      .get(environment.baseUrl + this.apiPath.categoryTechSpecification+queryparam, { params: params })
      .pipe(map((res: any) => res));

    // return this.httpClient
    //   .get(
    //     `${
    //       environment.baseUrl + this.apiPath.categoryTechSpecification
    //     }?limit=${pageSize}&page=${pageNumber}`
    //   )
    //   .pipe(map((res: any) => res));
  }
}
