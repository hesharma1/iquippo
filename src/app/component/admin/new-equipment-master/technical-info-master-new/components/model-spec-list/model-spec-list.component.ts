import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { NewEquipmentMasterService } from 'src/app/services/new-equipment-master.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddEditSpecificationComponent } from '../../dialogs/add-edit-specification/add-edit-specification.component';
import { ConfirmDialogComponent } from '../../../location-master/confirm-dialog/confirm-dialog.component';
import { ViewBrandSpecDataComponent } from '../../dialogs/view-brand-spec-data/view-brand-spec-data.component';
import { Brand } from '../../models/brand.model';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-model-spec-list',
  templateUrl: './model-spec-list.component.html',
  styleUrls: ['./model-spec-list.component.css'],
})
export class ModelSpecListComponent implements OnInit {
  dataSource1 = new MatTableDataSource<any>([]);
  dataSource: any = [];
  modelSpecDisplayedColumns = [
    'category_specifications__category__display_name',
    'brand__name',
    'model__name',
    'actions',
  ];

  @ViewChild('input') input!: ElementRef;
  brandSpecForm: FormGroup;
  categories = [];
  filteredCategories = [];
  brands: Brand[] = [];
  models: any[] = [];
  specList!: any;
  isSelectedModel: boolean = false;
  selectedCatdId: any;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;
  constructor(private dialog: MatDialog, private fb: FormBuilder, public adminService: AdminMasterService, public newEquipmentService: NewEquipmentMasterService, private notify: NotificationService,
  ) {
    this.brandSpecForm = this.fb.group({
      Category: ["", Validators.required],
      Brand: ["", Validators.required],
      Model: ["", Validators.required],
    });
  }

  ngOnInit() {
    let queryparams = 'limit=999&ordering=-id';
    this.getCategoryMasterData(queryparams);
    this.getModelSpecTechData();
  }

  getCategoryMasterData(queryparams) {
    this.adminService.getCategoryMaster(queryparams).subscribe((response: any) => {
      this.categories = response.results;
      this.filteredCategories = response.results;
    });
  }

  getModelSpecTechData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }

    this.adminService.getModelTechSpecificationList(payload).subscribe((res: any) => {
      this.total_count = res.count;
      this.dataSource = res.results;
      if (window.innerWidth > 768) {
        this.dataSource1.data = res.results;
        console.log(" this.dataSource1.data", this.dataSource1.data);
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.dataSource1.data = [];
          this.listingData = [];
          this.listingData = res.results;
          this.dataSource1.data = this.listingData;
        } else {
          this.listingData = this.listingData.concat(res.results);
          this.dataSource1.data = this.listingData;
        }
      }
    });
  }

  filterValuesCategory(search: any) {
    let data = search.target.value;
    let filtered = this.filteredCategories;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        (item: any) => item?.display_name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categories = filteredData
    } else {
      this.categories = []
    }
  }

  addModelTechnicalSpecification() {
    this.newEquipmentService
      .getCategoryTechSpecificationList()
      .subscribe((data: any) => {
        const filteredData = data.results.filter(
          (val: any) =>
            val.category.id === this.selectedCatdId
        );
        let fieldsValues: any[] = [];
        filteredData.forEach((val: any) => {
          fieldsValues.push({
            cat_spec_id: val.id,
            field_name: val.field_name,
            field_value: null,
            visible_on_front: null,
            priority: null,
            field_unit: null
          });
        });
        if (fieldsValues && fieldsValues.length > 0) {
          const dialogRef = this.dialog.open(AddEditSpecificationComponent, {
            width: '100%',
            panelClass: 'custom-modalbox',
            data: { isEdit: false, newFieldsValues: fieldsValues },
          });
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              const modelSpecValues: any[] = [];
              result.NewFieldsValuesList.forEach((element: any) => {
                modelSpecValues.push({
                  category_specifications: element.cat_spec_id,
                  brand: this.brandSpecForm.get('Brand')?.value,
                  model: this.brandSpecForm.get('Model')?.value,
                  field_value: element.field_value,
                  visible_on_front:
                    element.visible_on_front == null
                      ? false
                      : element.visible_on_front,
                  priority: element.priority == null ? 0 : element.visible_on_front == false ? 0 : element.priority,
                  field_unit: element.field_unit
                });
              });
              let modelValues = modelSpecValues.filter(x => x.field_value && x.brand && x.model);
              this.saveModelSpecList(modelValues);
            }
          });
        }
        else {
          this.notify.error(
            'No field found with this specific category!'
          );
        }
      });
  }

  saveModelSpecList(modelSpecValues: any[]) {
    this.newEquipmentService
      .createModelTechSpecification(modelSpecValues)
      .subscribe(async (res: any) => {
        if (res && res.length > 0) {
          this.notify.success('Saved successfully');
        }
        this.getModelSpecTechData();
      });
  }

  viewModelTechSpec(catId: number, brandId: number, modelId: number) {
    const fieldsValues: any[] = [];
    this.newEquipmentService
      .getModelSpecByCategoryBrandModel(modelId, brandId, catId)
      .subscribe((resp: any) => {
        (resp as any[]).forEach((val) =>
          fieldsValues.push({
            id: val.id,
            field_name: val.field_name,
            field_value: val.field_value,
            visible_on_front: val.visible_on_front,
            priority: val.priority,
            field_unit: val.field_unit
          })
        );
        const dialogRef = this.dialog.open(ViewBrandSpecDataComponent, {
          width: '50%',
          panelClass: 'custom-modalbox',
          data: { categoryFieldsValues: fieldsValues },
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
          }
        });
      });
  }

  editModelTechSpec(catId: number, brandId: number, modelId: number) {
    let fieldsValues: any[] = [];
    this.newEquipmentService
      .getModelSpecByCategoryBrandModel(modelId, brandId, catId)
      .subscribe((resp: any) => {
        (resp as any[]).forEach((val) =>
          fieldsValues.push({
            id: val.id,
            field_name: val.field_name,
            field_value: val.field_value,
            visible_on_front: val.visible_on_front,
            priority: val.priority,
            field_unit: val.field_unit
          })
        );
        this.newEquipmentService
          .getCategoryTechSpecificationList()
          .subscribe((data: any) => {
            let filteredData = data.results.filter(
              (val: any) => val.category.id === catId
            );
            const filteredData1: any[] = [];
            filteredData.forEach((e: any) => {
              if (!this.isFilled(fieldsValues, e.field_name)) {
                filteredData1.push(e);
              }
            });
            const newfieldsValues: any[] = [];
            filteredData1.forEach((val: any) => {
              newfieldsValues.push({
                cat_spec_id: val.id,
                field_name: val.field_name,
                field_value: null,
                visible_on_front: null,
                priority: null,
                field_unit: null
              });
            });

            const dialogRef = this.dialog.open(AddEditSpecificationComponent, {
              width: '100%',
              panelClass: 'custom-modalbox',
              data: {
                oldFieldsValues: fieldsValues,
                newFieldsValues: newfieldsValues,
              },
            });

            dialogRef.afterClosed().subscribe((result) => {
              if (result) {
                const modelSpecValues: any[] = [];
                result.OldFieldsValuesList.forEach((element: any) => {
                  modelSpecValues.push({
                    id: element.id,
                    brand: brandId,
                    model: modelId,
                    field_value: element.field_value,
                    visible_on_front:
                      element.visible_on_front == null
                        ? false
                        : element.visible_on_front,
                    priority: element.priority == null ? 0 : element.visible_on_front == false ? 0 : element.priority,
                    field_unit: element.field_unit
                  });
                });
                let modelValues = modelSpecValues.filter(x => x.field_value && x.brand && x.model);
                if (modelValues.length > 0) {
                  this.batchUpdateModelSpec(modelValues);
                }

                const catSpecFields: any[] = [];
                result.NewFieldsValuesList.forEach((element: any) => {
                  catSpecFields.push({
                    category_specifications: element.cat_spec_id,
                    brand: brandId,
                    model: modelId,
                    field_name: element.field_name,
                    field_value: element.field_value,
                    visible_on_front:
                      element.visible_on_front == null
                        ? false
                        : element.visible_on_front,
                    priority: element.priority == null ? 0 : element.priority,
                    field_unit: element.field_unit
                  });
                });
                let catFields = catSpecFields.filter(x => x.field_value && x.brand && x.model);
                if (catFields.length > 0) {
                  this.saveModelSpecList(catFields);
                }
              }
            });
          });
      });
  }

  isFilled(fieldsValues: any[], field_name: string) {
    return fieldsValues.filter((fv) => fv.field_name === field_name).length > 0;
  }

  deleteModelTechSpec(catId: number, brandId: number, modelId: number) {
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.newEquipmentService
            .deleteModelTechSpec(catId, brandId, modelId)
            .subscribe(async () => {
              this.getModelSpecTechData();
            });
        }
      });
  }

  createFilledAndUnFilledFieldsArray(
    filledFieldValues: any[],
    catId: number,
    brandId: number,
    modelId: number
  ) {
    let fieldsValues: any[] = [];
    this.newEquipmentService
      .getCategoryTechSpecificationList()
      .subscribe((data: any) => {
        const filteredData = data.results.filter(
          (val: any) => val.category.id === catId
        );

        filteredData.forEach((val: any) => {
          const filledField = filledFieldValues.filter(
            (v: any) => v.field_name === val.field_name
          )[0];
          fieldsValues.push({
            cat_spec_id: val.id,
            field_name: val.field_name,
            field_value: filledField?.field_value,
            visible_on_front: filledField?.visible_on_front,
            priority: filledField?.priority,
            field_unit: filledField?.field_unit
          });
        });
        const dialogRef = this.dialog.open(AddEditSpecificationComponent, {
          width: '40%',
          panelClass: 'custom-modalbox',
          data: { fieldsValues: fieldsValues },
        });

        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            const modelSpecValues: any[] = [];
            result.FieldsValuesList.forEach((element: any) => {
              modelSpecValues.push({
                category_specifications: element.cat_spec_id,
                brand: brandId,
                model: modelId,
                field_value: element.field_value,
                visible_on_front:
                  element.visible_on_front == null
                    ? false
                    : element.visible_on_front,
                priority: element.priority == null ? 0 : element.visible_on_front == false ? 0 : element.priority,
                field_unit: element.field_unit
              });
            });
            let modelValues = modelSpecValues.filter(x => x.field_value && x.brand && x.model);
            this.batchUpdateModelSpec(modelValues);
          }
        });
      });

  }

  batchUpdateModelSpec(modelSpecValues: any[]) {
    this.newEquipmentService
      .batchUpdateModelSpec(modelSpecValues)
      .subscribe(async (res: any) => {
        console.log("res", res);
        this.notify.success('Updated successfully')
        this.getModelSpecTechData();
      });
  }

  exportToExcel() {
    let payload = {
      limit: -1,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.adminService.getModelTechSpecificationList(payload).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "Category name": r.category_specifications__category__display_name,
          "Brand name": r.brand__name,
          "Model name": r.model__name,
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Model-Spec-List");
    },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  loadBrandByCatgeory(event, selecetdCategory: any) {
    if (event.isUserInput) {
      this.selectedCatdId = selecetdCategory;
      this.brands = [];
      this.models = [];
      this.isSelectedModel = false;
      this.adminService
        .getBrandMasterByCatId(selecetdCategory)
        .subscribe((response: any) => {
          this.brands = response.results as Brand[];
        });
    }
  }

  loadModelByBrand(selValue: number) {
    this.adminService
      .getModelMasterByBrandId(selValue)
      .subscribe((response: any) => {
        this.models = response.results as any[];
      });
  }

  loadModel(selValue: number) {
    this.isSelectedModel = true;
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getModelSpecTechData();
  }

  search() {
    console.log("search call..", this.searchText)
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      console.log("true call..")
      this.getModelSpecTechData();
    }
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getModelSpecTechData();
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if ((this.total_count > this.dataSource1.data.length) && (this.dataSource1.data.length > 0)) {
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getModelSpecTechData();
          }
        }
      }
    }
  }
}
