import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';

import { catchError, finalize } from 'rxjs/operators';
import { ModelSpecMasterService } from './model-spec-list.service';

export class ModelSpecMasterDataSource implements DataSource<any> {
  private listSubject = new BehaviorSubject<any[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  modelTechSpecData: any[] = [];

  constructor(private modelSpecMasterService: ModelSpecMasterService) { }

  loadModelSpecMaster(
    pageIndex: number,
    pageSize: number,
    sortDirection: string,
    filter: string
  ) {
    this.loadingSubject.next(true);
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.modelSpecMasterService
        .getModelTechSpecList(pageIndex, pageSize, sortDirection, filter)
        .subscribe((list) => {
          this.listSubject.next(list);
          resolve(list);
        });
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    console.log('Connecting data source');
    return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listSubject.complete();
    this.loadingSubject.complete();
  }
}
