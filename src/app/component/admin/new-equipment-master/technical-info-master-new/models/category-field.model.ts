export class CategoryFieldSpec {
  id?: number;
  field_name?: string;
  field_type?: number;
  field_type_name?: string;
  category_id?: number;
  category_name?: string;
}
