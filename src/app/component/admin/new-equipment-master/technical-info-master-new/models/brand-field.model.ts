export class BrandField {
  field_name?: string;

  field_value?: string;

  visible_on_front?: boolean;

  priority?: number;
}
