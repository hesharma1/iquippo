export class Brand {
  display_name?: string;
  id?: number;
  name?: string;
  status?: string;
}
