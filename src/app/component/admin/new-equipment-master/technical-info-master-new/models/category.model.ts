export class Category {
  display_name?: string;
  id?: number;
  name?: string;
  status?: string;
}
