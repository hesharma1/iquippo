import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
  FormArray,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NewEquipmentMasterService } from '../../../../../../services/new-equipment-master.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-add-edit-specification',
  templateUrl: './add-edit-specification.component.html',
  styleUrls: ['./add-edit-specification.component.css'],
})
export class AddEditSpecificationComponent implements OnInit {
  fieldForm: FormGroup;
  isEdit = false;
  oldfieldsValues: any[] = [];
  newfieldsValues: any[] = [];
  modelSpecification!: FormArray;

  constructor(public dataService: NewEquipmentMasterService, public spinner: NgxSpinnerService,
    public dialogRef: MatDialogRef<AddEditSpecificationComponent>, private notificationservice: NotificationService,
    @Inject(MAT_DIALOG_DATA) data: any,
    private formbulider: FormBuilder
  ) {
    this.fieldForm = this.formbulider.group({
      OldFieldsValuesList: this.formbulider.array([]),
      NewFieldsValuesList: this.formbulider.array([]),
    });

    this.isEdit = data?.isEdit;
    data?.oldFieldsValues?.forEach((value: any) => {
      this.oldfieldsValues.push(value);
    });
    data?.newFieldsValues?.forEach((value: any) => {
      this.newfieldsValues.push(value);
    });
  }

  ngOnInit(): void {
    console.log("OldFieldsValuesList", this.oldfieldsValues);
    if (this.oldfieldsValues && this.oldfieldsValues.length > 0) {
      for (let i = 0; i < this.oldfieldsValues.length; i++) {
        (this.fieldForm.get('OldFieldsValuesList') as FormArray).push(
          this.formbulider.group({
            id: new FormControl(this.oldfieldsValues[i].id, [
              Validators.required,
            ]),
            field_name: new FormControl(this.oldfieldsValues[i].field_name, [
              Validators.required,
            ]),
            field_value: new FormControl(this.oldfieldsValues[i].field_value, [
              Validators.required,
            ]),
            visible_on_front: new FormControl(
              this.oldfieldsValues[i].visible_on_front
            ),
            priority: new FormControl(this.oldfieldsValues[i].priority),
            field_unit: new FormControl(this.oldfieldsValues[i].field_unit,
              [Validators.max(6), Validators.min(2)])
          })
        );
      }
    }

    if (this.newfieldsValues && this.newfieldsValues.length > 0) {
      for (let i = 0; i < this.newfieldsValues.length; i++) {
        (this.fieldForm.get('NewFieldsValuesList') as FormArray).push(
          this.formbulider.group({
            cat_spec_id: new FormControl(this.newfieldsValues[i].cat_spec_id, [
              Validators.required,
            ]),
            field_name: new FormControl(this.newfieldsValues[i].field_name, [
              Validators.required,
            ]),
            field_value: new FormControl(this.newfieldsValues[i].field_value, [
              Validators.required,
            ]),
            visible_on_front: new FormControl(
              this.newfieldsValues[i].visible_on_front
            ),
            priority: new FormControl(this.newfieldsValues[i].priority),
            field_unit: new FormControl(this.newfieldsValues[i].field_unit,
              [Validators.max(6), Validators.min(2)])
          })
        );
      }
    }
  }

  delete(data, id) {
    //this.spinner.show();
    this.dataService.modelTechSpecificationdelete(data.value.id).subscribe(
      data => {
        var i = '' + id + '';
        this.modelSpecification = <FormArray>(
          this.fieldForm.controls['OldFieldsValuesList']
        );
        this.modelSpecification.removeAt(id);

        this.notificationservice.success("Deleted successfully")
      },
      err => {
        console.log(err);
      }
    )
  }
  get oldfieldsValuesList() {
    return this.fieldForm.get('OldFieldsValuesList') as FormArray;
  }

  get newfieldsValuesList() {
    return this.fieldForm.get('NewFieldsValuesList') as FormArray;
  }

  okClick() {
    this.dialogRef.close(this.fieldForm.value);
  }

  close() {
    this.dialogRef.close();
  }
}
