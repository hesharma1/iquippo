import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
} from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { dataBound } from '@syncfusion/ej2-angular-grids';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { CheckPasswordMatch } from 'src/app/utility/custom-validators/forgot-password';
@Component({
  selector: 'app-add-edit-field',
  templateUrl: './add-edit-field.component.html',
  styleUrls: ['./add-edit-field.component.css'],
})
export class AddEditFieldComponent implements OnInit {
  fieldForm: FormGroup;
  isEdit = false;

  constructor(
    //public dialogRef: MatDialogRef<AddEditFieldComponent>,
    //@Inject(MAT_DIALOG_DATA) data: any,
    private formbulider: FormBuilder
  ) {

    this.fieldForm = this.formbulider.group({
      Field: new FormControl("", [Validators.required]),      
    });


    var ele_field_name = localStorage.getItem("ele_field_name");
    if (ele_field_name != undefined)
    {
      //var JData = eval(ds);
      var editableData = JSON.parse(ele_field_name);
      //editableData.forEach(element =>{
        this.fieldForm = this.formbulider.group({
          Field: new FormControl(editableData.field_name, [Validators.required]),          
        });
      //});

      localStorage.clear();
      localStorage.removeItem("ele_field_name");    

    }
    else
    {
      this.fieldForm = this.formbulider.group({
        Field: new FormControl("", [Validators.required]),      
      });
    }

    //this.isEdit = data?.isEdit;
  }

  ngOnInit(): void {}

  okClick() {
    //this.dialogRef.close(this.fieldForm.get('Field')?.value);
  }
}
