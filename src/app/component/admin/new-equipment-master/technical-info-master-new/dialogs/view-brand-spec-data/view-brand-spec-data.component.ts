import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-view-brand-spec-data',
  templateUrl: './view-brand-spec-data.component.html',
  styleUrls: ['./view-brand-spec-data.component.css'],
})
export class ViewBrandSpecDataComponent implements OnInit {
  brandSpecTableHeaders = [
    'Field Name',
    'Field Value',
    'Visible on front',
    'Priority',
  ];
  // brandTechnicalSpecValues: any[] = [
  //   {
  //     category_name: 'Category 1',
  //     category_id: 1,
  //     brand_name: 'Sony',
  //     brand_id: 1,
  //     model_name: 'model1',
  //     model_id: 1,
  //     fields: [
  //       {
  //         field_name: 'field1',
  //         field_value: 'field 1 value',
  //         visible_on_front: true,
  //         priority: 1,
  //       },
  //       {
  //         field_name: 'field11',
  //         field_value: 'field 11 value',
  //         visible_on_front: true,
  //         priority: 11,
  //       },
  //     ],
  //   },
  //   {
  //     category_name: 'Category 2',
  //     category_id: 2,
  //     brand_name: 'Dell',
  //     brand_id: 2,
  //     model_name: 'model2',
  //     model_id: 2,
  //     fields: [
  //       {
  //         field_name: 'field2',
  //         field_value: 'field 2 value',
  //         visible_on_front: true,
  //         priority: 2,
  //       },
  //       {
  //         field_name: 'field22',
  //         field_value: 'field 22 value',
  //         visible_on_front: true,
  //         priority: 22,
  //       },
  //     ],
  //   },
  // ];

  brandTechnicalSpecValues: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<ViewBrandSpecDataComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private formbulider: FormBuilder
  ) {
    this.brandTechnicalSpecValues = data.categoryFieldsValues;
  }

  ngOnInit(): void {}

  okClick() {
    this.dialogRef.close();
  }
}
