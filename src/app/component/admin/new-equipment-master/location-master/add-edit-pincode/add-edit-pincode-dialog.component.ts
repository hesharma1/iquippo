
import { Component, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from "rxjs";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";

@Component({
  selector: "app-add-edit-pincode-dialog",
  templateUrl: "./add-edit-pincode-dialog.component.html",
  styleUrls: ["add-edit-pincode-dialog.component.css"]
})
export class AddEditPinCodeDialogComponent {
  form: FormGroup;
  statesData: any[] = [];
  isEdit: boolean = false;

  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();

  StrOutputMSG: boolean = false;
  countryDataSource: any;
  stateDataSource: any;
  cityDataSource: any;
  Close() {
    this.ChdOutput.emit();
  }

  constructor(
    private fb: FormBuilder, private adminMasterService: AdminMasterService, private notify: NotificationService) {
    this.form = this.fb.group({
      id: [null],
      Country: [null, Validators.required,],
      State: [null, Validators.required,],
      CityName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Pincode: ["", Validators.compose([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)])]
    });

    /* if(data) {
      this.statesData = data.statesData
      if(data.formData) {
        this.form.patchValue(data.formData);
      }
    } */

    var PincodeData = localStorage.getItem("PinCodeData");
    if (PincodeData != undefined) {
      //var JData = eval(ds);
      this.isEdit = true;
      var editableData = JSON.parse(PincodeData);
      this.renderStateDataByStateId(editableData.State)
      this.renderCityDataByCityId(editableData.CityName)
      //this.countryData = editableData.country.name;
      this.form?.patchValue(editableData);
      localStorage.clear();
      localStorage.removeItem("PinCodeData");

    }
    else {
      this.isEdit = false;
      this.form = this.fb.group({
        id: [null],
        Country: [null, Validators.required,],
        State: [null, Validators.required,],
        CityName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        Pincode: ["", Validators.compose([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)])]
      });
    }

  }
  ngOnInit(): void {
    this.renderCountryData();
  }

  backClick() {
    //this.dialogRef.close();
  }

  okClick() {
    let addPincodeRequest = {
      pin_code: this.form.value.Pincode,
      city: this.form.value.CityName,
    }
    // if(this.form.value.Pincode != null && this.form.value.Pincode != "" && this.form.value.Pincode != 0)
    // {
    //   this.adminMasterService.putPincodeMaster(addPincodeRequest,"888888").subscribe((resp) => {
    //      this.ChdOutput.emit();
    //   });
    // }
    // else
    // {
    this.adminMasterService.postPincodeMaster(addPincodeRequest).subscribe((resp) => {
      this.ChdOutput.emit();
      this.notify.success('Pincode added successfully.');
    })
  }

  getCountryData(queryParams: string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams = 'limit=999') {
    this.getCountryData(queryParams).subscribe((data: any) => {
      this.countryDataSource = data.results;
    });
  }

  renderStateData(queryParams: string) {
    this.getStateData(queryParams).subscribe((res: any) => {
      this.stateDataSource = res.results;
    })
  }

  getStateData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getStateMaster(queryParams)
  }

  renderStateDataByStateId(id: any) {
    this.getStateDataByStateId(id).subscribe((res: any) => {
      let stateArray: Array<any>;
      stateArray = new Array<any>();
      stateArray.push(res)
      this.stateDataSource = stateArray;
    })
  }

  getStateDataByStateId(id: any): Observable<Object> {
    return this.adminMasterService
      .getStateMasterByStateid(id)
  }

  renderCityData(queryParams: string) {
    this.getCityData(queryParams).subscribe((res: any) => {
      this.cityDataSource = res.results;
    })
  }

  getCityData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getCityMaster(queryParams)
  }

  renderCityDataByCityId(id: any) {
    this.getCityDataByCityId(id).subscribe((res: any) => {
      let cityArray: Array<any>;
      cityArray = new Array<any>();
      cityArray.push(res)
      this.cityDataSource = cityArray;
    })
  }

  getCityDataByCityId(id: any): Observable<Object> {
    return this.adminMasterService
      .getCityMasterById(id)
  }

  getCities(value: any) {
    this.renderCityData('state__id__in=' + value + '&limit=999')
  }

  getStates(value: any) {
    this.renderStateData('country__id__in=' + value + '&limit=999')
    this.cityDataSource = [];
    let country = this.countryDataSource.filter(x => x.id == value).map(x => x.name)[0];
    if (country != 'India') {
      this.form.get('Pincode')?.clearValidators();
      this.form.get('Pincode')?.updateValueAndValidity();
      this.form.get('Pincode')?.setValidators(Validators.required);
      this.form.get('Pincode')?.updateValueAndValidity();
    }
    else {
      this.form.get('Pincode')?.setValidators([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)]);
      this.form.get('Pincode')?.updateValueAndValidity();
    }
  }
}
