import { Component, Inject, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminMasterService } from "src/app/services/admin-master.service";
import { S3UploadDownloadService } from "src/app/utility/s3-upload-download/s3-upload-download.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-add-edit-country-dialog",
  templateUrl: "./add-edit-country-dialog.component.html",
  styleUrls: ["./add-edit-country-dialog.component.css"]
})
export class AddEditCountryDialogComponent {
  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();
  @ViewChild('fileInputFlagImg') fileInputFlagImg!: ElementRef;
  imgName: string = "";
  isImgName: boolean = false;
  form: FormGroup;
  StrOutputMSG: boolean = false;
  isEdit: boolean = false;
  flagImage: any;
  savedLocation: string = "";

  constructor(private oDialog: MatDialog, private fb: FormBuilder, public s3: S3UploadDownloadService,
    private adminMasterService: AdminMasterService, private notify: NotificationService) {
    this.form = this.fb.group({
      id: [""],
      Name: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Code: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Flag: [""],
      ISDCode: ["", Validators.compose([Validators.required, Validators.maxLength(50)])]
    });

    var CountryData = localStorage.getItem("CountryData");
    if (CountryData != undefined) {
      //var JData = eval(ds);
      var editableData = JSON.parse(CountryData);
      console.log("editableData", editableData);
      this.flagImage = editableData.Flag;
      if (this.flagImage) {
        this.isImgName = true;
      }
      else {
        this.isImgName = false;
      }
      this.isEdit = true;
      this.form?.patchValue(editableData);
      localStorage.clear();
      localStorage.removeItem("CountryData");
    }
    else {
      this.form = this.fb?.group({
        id: [""],
        Name: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        Code: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
        Flag: [""],
        ISDCode: ["", Validators.compose([Validators.required, Validators.maxLength(50)])]
      });
    }
  }

  onInit() {
    //this.ChdInput = 0;
  }

  backClick() {
    //this.dialogRef.close();
  }

  okClick() {
    let fileName: string = this.form.value.Name;
    this.imageUpload(
      this.form,
      this.fileInputFlagImg,
      'MasterData',
      fileName
    ).then((res) => {
      let addCountryRequest = {
        name: this.form.value.Name,
        code: this.form.value.Code,
        flag: this.savedLocation ? this.savedLocation : this.flagImage ? this.flagImage : '',
        isd_code: this.form.value.ISDCode

      }
      console.log("addCountryRequest", addCountryRequest);
      if (this.form.value.id != null && this.form.value.id != "" && this.form.value.id != 0) {
        this.adminMasterService.putCountryMaster(addCountryRequest, this.form.value.id).subscribe((resp) => {
          this.ChdOutput.emit();
          this.notify.success('Country updated successfully.');
        });
      }
      else {
        this.adminMasterService.postCountryMaster(addCountryRequest).subscribe((resp) => {
          this.ChdOutput.emit();
          this.notify.success('Country added successfully.');
        });
      }
    });
  }

  async imageUpload(frmGroup: any, image: any, tab: string, side: string) {
    this.savedLocation = '';
    const file = image.nativeElement.files[0];
    if (file != null) {
      if (
        (file.type == 'image/png' ||
          file.type == 'image/jpg' ||
          file.type == 'image/jpeg' ||
          file.type == 'image/bmp') &&
        file.size <= 5242880
      ) {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setErrors(null);
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue(reader.result);
        };
        var fileFormat = (file?.name).toString();
        var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

        var image_path =
          environment.bucket_parent_folder +
          '/' +
          tab +
          '/' +
          side +
          '.' +
          fileExt;

        var uploaded_file = await this.s3.prepareFileForUpload(
          file,
          image_path
        );
        if (
          uploaded_file?.Key != undefined &&
          uploaded_file?.Key != '' &&
          uploaded_file?.Key != null
        ) {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side)
            ?.setValue(uploaded_file?.Key);
          this.savedLocation = uploaded_file?.Location;
        } else {
          frmGroup
            .get(tab)
            ?.get('docImages')
            ?.get(side + 'View')
            ?.setValue('');
          this.notify.error('Upload Failed');
        }
      } else {
        frmGroup.get(tab)?.get('docImages')?.get(side)?.setValue('');
        frmGroup
          .get(tab)
          ?.get('docImages')
          ?.get(side)
          ?.setErrors({ imgIssue: true });
      }
    }
  }

  getFileName(type) {
    if (type == 'fileInputFlagImg') {
      if (this.fileInputFlagImg && this.fileInputFlagImg.nativeElement && this.fileInputFlagImg.nativeElement.files && this.fileInputFlagImg.nativeElement.files[0]) {
        this.imgName = this.fileInputFlagImg.nativeElement.files[0].name;
        this.isImgName = true;
      }
      return '';
    }
    return '';
  }


  resetFile(type) {
    if (type == 'fileInputFlagImg') {
      this.imgName = '';
      this.isImgName = false;
      this.fileInputFlagImg.nativeElement.value = "";
    }
  }

  deleteFlag(url) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    console.log("imgpath", imgPath);
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.flagImage = "";
        this.isImgName = false;
        this.savedLocation = "";
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }


  Close() {
    this.ChdOutput.emit();//this.StrOutputMSG)
    this.oDialog.closeAll();
  }
}
