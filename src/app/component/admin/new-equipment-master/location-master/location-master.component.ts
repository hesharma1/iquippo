import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';

import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { AddEditCountryDialogComponent } from './add-edit-country/add-edit-country-dialog.component.';
import { AddEditLocationDialogComponent } from './add-edit-city/add-edit-city-dialog.component';
import { AddEditStateDialogComponent } from './add-edit-state/add-edit-state-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ExportPopupComponent } from 'src/app/component/admin/export-popup/export-popup.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'location-master',
  templateUrl: './location-master.component.html',
  styleUrls: ['./location-master.component.css'],
})
export class LocationMasterComponent implements OnInit, AfterViewInit 
{
  countryData: any[] = [];
  allCountries: any[] = [];
  countryDataSource = new MatTableDataSource<any>([]);
  countryDisplayedColumns = ['name', 'code', 'flag', 'isd_code', 'actions']; 
  @ViewChild('countryPaginator') countryPager!: MatPaginator;

  stateData: any[] = [];
  allStates: any[] = [];
  stateDataSource = new MatTableDataSource<any>([]);
  stateDisplayedColumns = ['country_name', 'name', 'actions'];  
  @ViewChild('statePaginator') statePager!: MatPaginator;

  locationData: any[] = [];
  CityDataSource = new MatTableDataSource<any>([]);
  PincodeDataSource = new MatTableDataSource<any>([]);
  CityDisplayedColumns = ['Country','state_name', 'name', 'actions'];
  PincodeDisplayedColumns = ['Country','state_name', 'City', 'pin_code','rm_name', 'actions'];
  @ViewChild('cityPaginator') cityPager!: MatPaginator;
  @ViewChild('pinCodePaginator') pincodePager!: MatPaginator;

  showCountryForm: boolean = false;
  showStateForm: boolean = false;
  showCityForm: boolean = false;
  showPincodeForm: boolean = false;
  currentTab =0;
  FlagPopUp: number = 0;
  listingDataPin:any = [];
  listingData:any = [];
  listingDataState:any = [];
  listingDataCity:any = [];
  constructor(
    //private dialog: MatDialog,
    private adminMasterService: AdminMasterService,private datePipe: DatePipe,private dialog: MatDialog,private notify: NotificationService,private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    if(window.innerWidth > 768){
    this.renderCountryData('');
    this.renderStateData('');
    this.renderCityData('');
    this.renderPincodeData('');
    }
    this.showCountryForm = false;
    this.showStateForm = false;
    this.showCityForm = false;
    this.showPincodeForm = false;
  }


  add(type : string)
  {
   if(type === 'Country')
   {
    this.showCountryForm = true;
   }
   else if(type === 'State')
   {
    this.showStateForm = true;
   }
   else if(type === 'City')
   {
    this.showCityForm = true;
   }
   else if(type === 'Pincode')
   {
    this.showPincodeForm = true;
   }
  }

  GetChildData(StrAction: string , event ,SelectedTblIndex: number)
  {    
    if(StrAction == 'CountryCreate')
    {
      this.showCountryForm = false;
       this.renderCountryData('');
    }
    else if (StrAction == 'StateCreate')
    {
      this.showStateForm = false;
      this.renderStateData('');
    }
    else if (StrAction == 'cityCreate')
    {
      this.showCityForm = false;
      this.renderCityData('');
    }
    else if (StrAction == 'PincodeCreate')
    {
      this.showPincodeForm = false;
      this.renderPincodeData('');
    }
    else if(StrAction == 'CountryEdit')
    {
      //let index = this.countryDataSource.filteredData[SelectedTblIndex]; //.indexOf(SelectedTblIndex);
      //index = index !== -1 ? index : SelectedTblIndex;
      const countryData = this.countryDataSource.filteredData[SelectedTblIndex];
      this.showCountryForm = false;
      this.ref.detectChanges();
      this.showCountryForm = true;
      let obj = {
        id : countryData.id,
        Name : countryData.name,
        Code: countryData.code,
        Flag: countryData.flag,
        ISDCode: countryData.isd_code
      }

      localStorage.removeItem("CountryData");
      localStorage.clear();

      localStorage.setItem("CountryData" , JSON.stringify(obj));
    }
    else if (StrAction == 'StateEdit') // ***
    {
      this.showStateForm = false;
      this.ref.detectChanges();
      this.showStateForm = true;
      const StateData = this.stateDataSource.filteredData[SelectedTblIndex];

      let obj = {
        id: StateData.id,
        Country: StateData.country.id,
        StateName: StateData.name
      }

      localStorage.removeItem("StateData");
      localStorage.clear();

      localStorage.setItem("StateData" , JSON.stringify(obj));
    }
    else if (StrAction == 'CityEdit')
    {
      this.showCityForm = false;
      this.ref.detectChanges();
      this.showCityForm = true;
      const cityDataSource = this.CityDataSource.filteredData[SelectedTblIndex];

      let obj = {
        id : cityDataSource.id,
        Country: cityDataSource.state.country.id,
        State: cityDataSource.state.id,
        cityName: cityDataSource.name        
      }

      localStorage.removeItem("CityData");
      localStorage.clear();

      localStorage.setItem("CityData" , JSON.stringify(obj));
    }
    else if (StrAction == 'PincodeEdit')
    {
      this.showPincodeForm = false;
      this.ref.detectChanges();
      this.showPincodeForm = true;
      const pincodeDataSource = this.PincodeDataSource.filteredData[SelectedTblIndex];

      let obj = {
        id : pincodeDataSource.id,
        Country: pincodeDataSource.city.state.country.id,
        State: pincodeDataSource.city.state.id,
        CityName: pincodeDataSource.city.id,
        Pincode: pincodeDataSource.pin_code
      }

      localStorage.removeItem("PinCodeData");
      localStorage.clear();

      localStorage.setItem("PinCodeData" , JSON.stringify(obj));
    }

  }
  onListTabChanged(event) {
    this.currentTab = event?.index;
    console.log(this.currentTab)
  }
  scrolled = false;
  scrolledState = false;
  scrolledPin =  false;
  scrolledCity = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(this.currentTab == 0){
          if(((this.countryPager.length > this.listingData.length) && (this.listingData.length > 0))){
            if(!this.scrolled){
              this.scrolled = true;
            this.countryPager.pageIndex++;
            this.renderCountryData(this.getQueryParams('Country',10));
            }
          }
          
        }else if(this.currentTab == 1){
          if(((this.statePager.length > this.listingDataState.length) && (this.listingDataState.length > 0))){
            if(!this.scrolledState){
              this.scrolledState = true;
            this.statePager.pageIndex++;
            this.renderStateData(this.getQueryParams('State', 10));     
            }
          }
          
        }else if(this.currentTab == 2){
          if(((this.cityPager.length > this.listingDataCity.length) && (this.listingDataCity.length > 0))){
            if(!this.scrolledCity){
              this.scrolledCity = true;
            this.cityPager.pageIndex++;
            this.renderCityData(this.getQueryParams('City', 10));
          }
        }
        }else{
          if(((this.pincodePager.length > this.listingDataPin.length) && (this.listingDataPin.length > 0))){
            if(!this.scrolledPin){
              this.scrolledPin = true;
            this.pincodePager.pageIndex++;
            this.renderPincodeData(this.getQueryParams('Pincode', 10));
          }
        }
        }
    }
  }
}
  
  ngAfterViewInit() {
    //country
    this.countryDataSource.paginator = this.countryPager;
    this.countryPager.page.subscribe((e: any) => {
      this.renderCountryData(this.getQueryParams('Country', e.pageSize));
    });

    //state
    this.stateDataSource.paginator = this.statePager;
    this.statePager.page.subscribe((e: any) => {
      this.renderStateData(this.getQueryParams('State', e.pageSize));     
    });

    //City
    this.CityDataSource.paginator = this.cityPager;
    this.cityPager.page.subscribe((e: any) => {
      this.renderCityData(this.getQueryParams('City', e.pageSize));
    });

    //Pincode
    this.PincodeDataSource.paginator = this.pincodePager;
    this.pincodePager.page.subscribe((e: any) => {
      this.renderPincodeData(this.getQueryParams('Pincode', e.pageSize));
    });
    if(window.innerWidth < 768){
      this.statePager.pageIndex =0;
      this.countryPager.pageIndex = 0;
      this.cityPager.pageIndex = 0;
      this.pincodePager.pageIndex = 0;
      this.renderCountryData(this.getQueryParams('Country', 10));
    this.renderStateData(this.getQueryParams('State', 10));
    this.renderCityData(this.getQueryParams('City', 10));
    this.renderPincodeData(this.getQueryParams('Pincode', 10));
    }
  }
  getQueryParams(type: string, pageSize: any): string {
     let queryParam = '';
    if (type === 'Country')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.countryPager.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.countryPager.pageIndex + 1}&ordering=-id`;
      }
    else  if (type === 'State')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.statePager.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.statePager.pageIndex + 1}&ordering=-id`;
      }
    else if (type === 'City')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.cityPager.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.cityPager.pageIndex + 1}&ordering=-id`;
      }
    else if (type === 'Pincode')
      if (pageSize) {
        queryParam = `limit=${pageSize}&page=${this.pincodePager.pageIndex + 1}&ordering=-id`;
      }
      else {
        queryParam = `page=${this.pincodePager.pageIndex + 1}&ordering=-id`;
      }

    return queryParam;
  }

  getCountryData(queryParams :string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams : string) {
    this.getCountryData(queryParams).subscribe((data: any) => {
      //this.countryData = data.results;
      this.countryPager.length = data.count;
      // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      this.countryData = data.results;
    }else{
      this.scrolled = false;
        if(this.countryPager.pageIndex == 0){
          this.countryData = [];
          this.listingData = [];
          this.listingData = data.results;
          this.countryData = this.listingData;
        }else{
          this.listingData = this.listingData.concat(data.results);
          this.countryData = this.listingData;
          //this.dataList.results = this.listingData;
        }
      }
      this.countryDataSource = new MatTableDataSource<any>(this.countryData);
    });
  }

  searchCountries(val: string) {
    this.renderCountryData(`search=${val}`);
  }

  
  getStateData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getStateMaster(queryParams)

  }

  exportCountryData(){    
    this.getCountryData(`limit=-1`).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {
         return {
          "Country": r.name, 
          "Code" :  r.code ,
          "ISD Code": r.isd_code
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "country-master");
    })
  }

  exportStateData(){    
    this.getStateData(`limit=-1`).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "Country": r.country.name,
          "State": r.name
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "state-master");
    })
  }

  renderStateData(queryParams :string){
    this.getStateData(queryParams).subscribe((res: any) => {
      //this.stateData = res.results;
      // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      this.stateData = res.results;
    }else{
      this.scrolledState = false;
        if(this.statePager.pageIndex == 0){
          this.stateData = [];
          this.listingDataState = [];
          this.listingDataState = res.results;
          this.stateData = this.listingDataState;
        }else{
          this.listingDataState = this.listingDataState.concat(res.results);
          this.stateData = this.listingDataState;
          //this.dataList.results = this.listingData;
        }
      }
      this.stateDataSource = new MatTableDataSource<any>(this.stateData);
      this.statePager.length = res.count;
    })
  }

  searchStates(val: string) {
    this.renderStateData(`search=${val}`);
  }

  getCityData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getCityMaster(queryParams)
  }

  exportCityData(){
    this.getCityData(`limit=-1`).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {        
        return {
          "Country" : r.state.country.name,
          "State": r.state.name,
          "City": r.name
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "city-master");
    })
  }

  exportPincodeData(){
    const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '500px';
      dialogConfig.data = {
        flag: true
      }
    const dialogRef = this.dialog.open(ExportPopupComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          let payload = {
            
          }
          if(val.fromDate){
            payload['created_at__date__gte'] = this.datePipe.transform(val.fromDate,'yyyy-MM-dd');
          }
          if(val.toDate){
            payload['created_at__date__lte'] = this.datePipe.transform(val.toDate,'yyyy-MM-dd');
          }
    this.getPincodecodeData(`limit=999`,payload).subscribe((res: any) => {
      const excelData = res.results.map((r: any) => {       
        return {
          "Country" : r.city.state.country.name,
          "State": r.city.state.name,
          "City": r.city.name,
          "Pincode": r.pin_code,
          "RM":r.rm_name
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Pincode-master");
    })
  }
});
  }

  renderCityData(queryParams : string){
    this.getCityData(queryParams).subscribe((res: any) => {
      //this.locationData = res.results;     
      // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      this.locationData = res.results;
    }else{
      this.scrolledCity = false;
        if(this.cityPager.pageIndex == 0){
          this.locationData = [];
          this.listingDataCity = [];
          this.listingDataCity = res.results;
          this.locationData = this.listingDataCity;
        }else{
          this.listingDataCity = this.listingDataCity.concat(res.results);
          this.locationData = this.listingDataCity;
          //this.dataList.results = this.listingData;
        }
      }
        this.CityDataSource = new MatTableDataSource<any>(this.locationData);
        this.cityPager.length = res.count;
    })
  }

  getPincodecodeData(queryParams: string,payload?): Observable<Object> {
    return this.adminMasterService.getPincodeMaster(queryParams,payload)
  }

  renderPincodeData(queryParams : string){
    this.getPincodecodeData(queryParams).subscribe((res: any) => {  
      let PincodeData;
       // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      PincodeData = res.results;
    }else{
      this.scrolledPin = false;
        if(this.pincodePager.pageIndex == 0){
          PincodeData = [];
          this.listingDataPin = [];
          this.listingDataPin = res.results;
          PincodeData = this.listingDataPin;
        }else{
          this.listingDataPin = this.listingDataPin.concat(res.results);
          PincodeData = this.listingDataPin;
          //this.dataList.results = this.listingData;
        }
      }    
        this.PincodeDataSource = new MatTableDataSource<any>(PincodeData);
        this.pincodePager.length = res.count;
    })
  }

  
  searchLocations(val: string) {
    this.renderCityData(`search=${val}`);
  }

  

  deleteCountry(id: any) {    
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed()
      .subscribe((data) => {
        if (data) {          
          this.adminMasterService.deleteCountryMaster(id).subscribe(() => {
            this.notify.success('Country deleted successfully.');
            this.renderCountryData('');
          });
        }
      });
  }
  deleteState(id: any) {    
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed()
      .subscribe((data) => {
        if (data) {          
          this.adminMasterService.deleteStateMaster(id).subscribe(() => {
             this.renderStateData('');
             this.notify.success('State deleted successfully.');
           });
        }
      });
  }
  deleteCity(id: any) {    
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })

      .afterClosed()
      .subscribe((data) => {
        if (data) {          
          this.adminMasterService.deleteCityMaster(id).subscribe(() => {
            this.renderCityData('');
            this.notify.success('City deleted successfully.');
          });
        }
      });
  }
  deletePincode(id: any) {   
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })
      
      .afterClosed()
      .subscribe((data) => {
        if (data) {
            this.adminMasterService.deletePincodeMaster(id).subscribe(() => {
            this.renderPincodeData('');
            this.notify.success('Pincode deleted successfully.');
          });
        }
      });
  }
  
  
  search(value : any,type : string)
  {   
   if(type === 'Country')
   {
    this.renderCountryData(`search=${value}`);
   }
   else if(type === 'City')
   {
    this.renderCityData(`search=${value}`);
   }
   else if(type === 'State')
   {
    this.renderStateData(`search=${value}`);
   }
   else if(type === 'Pincode')
   {
    this.renderPincodeData(`search=${value}`);
   }
   
   

  }


}
