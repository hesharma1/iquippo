import { Component } from "@angular/core";
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.css"]
})
export class ConfirmDialogComponent {
  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>){}

  backClick(){
    this.dialogRef.close(false);
  }

  okClick(){
    this.dialogRef.close(true);
  }
}
