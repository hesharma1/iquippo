import { Component, Inject, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from "rxjs";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";

@Component({
  selector: "app-add-edit-state-dialog",
  templateUrl: "./add-edit-state-dialog.component.html",
  styleUrls: ["./add-edit-state-dialog.component.css"]
})
export class AddEditStateDialogComponent {
  form: FormGroup;
  countryData: any[] = [];
  isEdit: boolean = false;

  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();

  StrOutputMSG: boolean = false;
  countryDataSource: any;
  Close() {
    this.ChdOutput.emit();//this.StrOutputMSG)
    //this.dialogRef.close();
  }

  constructor(
    // public dialogRef: MatDialogRef<AddEditStateDialogComponent>,
    // @Inject(MAT_DIALOG_DATA) public data : any, 
    private fb: FormBuilder, private adminMasterService: AdminMasterService, private notify: NotificationService) {
    this.form = this.fb.group({
      id: [null],
      Country: [null, Validators.required,],
      StateName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
    });

    /* if(data) {
      this.countryData = data.countryData
      if(data.formData) {
        this.form.patchValue(data.formData);
      }
    } */

    var StateData = localStorage.getItem("StateData");
    if (StateData != undefined) {
      var editableData = JSON.parse(StateData);
      this.isEdit = true;
      this.form?.patchValue(editableData);
      localStorage.clear();
      localStorage.removeItem("StateData");
    }
    else {
      this.form = this.fb.group({
        id: [null],
        Country: [null, Validators.required,],
        StateName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      });
    }


  }

  ngOnInit(): void {
    this.renderCountryData();
  }

  backClick() {
    // this.dialogRef.close();
  }

  okClick() {
    let addStateRequest = {
      name: this.form.value.StateName,
      country: this.form.value.Country,
    }
    if (this.form.value.id != null && this.form.value.id != "" && this.form.value.id != 0) {
      this.adminMasterService.putStateMaster(addStateRequest, this.form.value.id).subscribe((resp) => {
        this.ChdOutput.emit();
        this.notify.success('State updated successfully.');
      });
    }
    else {
      this.adminMasterService.postStateMaster(addStateRequest).subscribe((resp) => {
        this.ChdOutput.emit();
        this.notify.success('State added successfully.');
      });
    }
  }
  getCountryData(queryParams: string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams = 'limit=999') {
    this.getCountryData(queryParams).subscribe((data: any) => {
      this.countryDataSource = data.results;

    });
  }
}
