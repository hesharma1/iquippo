import { Component, Inject, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from "rxjs";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";

@Component({
  selector: "app-add-edit-city-dialog",
  templateUrl: "./add-edit-city-dialog.component.html",
  styleUrls: ["add-edit-city-dialog.component.css"]
})
export class AddEditLocationDialogComponent {
  form: FormGroup;
  statesData: any[] = [];
  isEdit: boolean = false;

  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();

  StrOutputMSG: boolean = false;
  countryDataSource: any;
  stateDataSource: any;
  Close() {
    this.ChdOutput.emit();//this.StrOutputMSG)
  }

  constructor(
    // public dialogRef: MatDialogRef<AddEditLocationDialogComponent>,
    // @Inject(MAT_DIALOG_DATA) public data : any, 
    private fb: FormBuilder, private adminMasterService: AdminMasterService, private notify: NotificationService) {
    this.form = this.fb.group({
      id: [null],
      Country: [null, Validators.required,],
      State: [null, Validators.required,],
      cityName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])]
    });

    /* if(data) {
      this.statesData = data.statesData
      if(data.formData) {
        this.form.patchValue(data.formData);
      }
    } */

    var LocationData = localStorage.getItem("CityData");
    if (LocationData != undefined) {
      //var JData = eval(ds);
      this.isEdit = true;
      var editableData = JSON.parse(LocationData);
      this.renderStateDataByStateId(editableData.State)
      //this.countryData = editableData.country.name;
      this.form?.patchValue(editableData);
      localStorage.clear();
      localStorage.removeItem("CityData");
    }
    else {
      this.isEdit = false;
      this.form = this.fb.group({
        id: [null],
        Country: [null, Validators.required,],
        State: [null, Validators.required,],
        cityName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])]
      });
    }

  }
  ngOnInit(): void {
    this.renderCountryData();
  }
  backClick() {
    //this.dialogRef.close();
  }

  okClick() {
    let addCityRequest = {
      name: this.form.value.cityName,
      state: this.form.value.State,
    }
    if (this.form.value.id != null && this.form.value.id != "" && this.form.value.id != 0) {
      this.adminMasterService.putCityMaster(addCityRequest, this.form.value.id).subscribe((resp) => {
        this.ChdOutput.emit();
        this.notify.success('City updated successfully.');
      });
    }
    else {
      this.adminMasterService.postCityMaster(addCityRequest).subscribe((resp) => {
        this.ChdOutput.emit();
        this.notify.success('City added successfully.');
      });
    }

    /* const val = {
      ...this.data,
      ...this.form.value
    }
    this.dialogRef.close(val); */
  }

  getCountryData(queryParams: string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams = 'limit=999') {
    this.getCountryData(queryParams).subscribe((data: any) => {
      this.countryDataSource = data.results
    });
  }



  renderStateData(queryParams: string) {
    this.getStateData(queryParams).subscribe((res: any) => {
      this.stateDataSource = res.results;
    })
  }


  getStateData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getStateMaster(queryParams)

  }
  renderStateDataByStateId(id: any) {
    this.getStateDataByStateId(id).subscribe((res: any) => {
      let stateArray: Array<any>;
      stateArray = new Array<any>();
      stateArray.push(res)
      this.stateDataSource = stateArray;
    })
  }
  getStateDataByStateId(id: any): Observable<Object> {
    return this.adminMasterService
      .getStateMasterByStateid(id)

  }

  getStates(value: any) {
    this.renderStateData('country__id__in=' + value + '&limit=999')
  }
}
