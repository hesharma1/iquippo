import { Component, Inject, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: "app-add-edit-location-dialog",
  templateUrl: "./add-edit-location-dialog.component.html",
  styleUrls: ["add-edit-location-dialog.component.css"]
})
export class AddEditLocationDialogComponent {
form: FormGroup;
statesData: any[] = [];

@Output() ChdOutput: EventEmitter<string> = new EventEmitter();

  StrOutputMSG: boolean = false;
  
  Close()
  {
      this.ChdOutput.emit();//this.StrOutputMSG)
  }

constructor(
  // public dialogRef: MatDialogRef<AddEditLocationDialogComponent>,
  // @Inject(MAT_DIALOG_DATA) public data : any, 
  private fb: FormBuilder){
  this.form = this.fb.group({
    State: [null, Validators.required, ],
    LocationName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
    Pincode: ["", Validators.pattern(/^[1-9][0-9]{5}$/)]
  });
  
  /* if(data) {
    this.statesData = data.statesData
    if(data.formData) {
      this.form.patchValue(data.formData);
    }
  } */

  var LocationData = localStorage.getItem("LocationData");
  if (LocationData != undefined)
  {
    //var JData = eval(ds);

    var editableData = JSON.parse(LocationData);

    //this.countryData = editableData.country.name;

    this.form?.patchValue(editableData);

    localStorage.clear();
    localStorage.removeItem("LocationData");    

  }
  else
  {
    this.form = this.fb.group({
      State: [null, Validators.required, ],
      LocationName: ["", Validators.compose([Validators.required, Validators.maxLength(50)])],
      Pincode: ["", Validators.pattern(/^[1-9][0-9]{5}$/)]
    });
  }

}

backClick(){
  //this.dialogRef.close();
}

okClick(){
  /* const val = {
    ...this.data,
    ...this.form.value
  }
  this.dialogRef.close(val); */
}

}
