import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalInfoMasterUsedComponent } from './technical-info-master-used.component';

describe('TechnicalInfoMasterUsedComponent', () => {
  let component: TechnicalInfoMasterUsedComponent;
  let fixture: ComponentFixture<TechnicalInfoMasterUsedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnicalInfoMasterUsedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalInfoMasterUsedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
