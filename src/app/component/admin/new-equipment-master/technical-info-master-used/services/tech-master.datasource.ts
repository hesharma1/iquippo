import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { TechMasterService } from "./tech-master.service";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";

export class TechMasterDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private techMasterService: TechMasterService) { }

    getTechInfoData(payload: any) {
        return new Promise(resolve => {
            this.loadingSubject.next(true);
            this.techMasterService.get('techInfoMasterUsed', null, payload)
                .pipe(
                    catchError(() => of([])),
                    finalize(() => this.loadingSubject.next(false))
                )
                .subscribe((data: any) => {
                    this.dataSubject.next(data)
                    resolve(data)
                });
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete();
        this.loadingSubject.complete();
    }
}
