import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { ApiRouteService } from '../../../../../utility/app.refrence';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class TechMasterService {

  baseUrl:string;

  constructor(public http:HttpClient, public router:Router, private apiPath: ApiRouteService) { 
    this.baseUrl = environment.baseUrl;
  }

  public get(urlKey:string, params?:any, queryParams?:any, baseUrl?:string): Observable<Response>{
    const base = baseUrl ? baseUrl : this.baseUrl;
    var mainUrl = base + this.apiPath[urlKey];
    var x;
    if(params){
      for(x in params){
        mainUrl += params[x] + "/";
      }
    }
    return this.http.get<Response>(`${mainUrl}`, {params : (queryParams ? queryParams : '')})
      .catch((error:HttpErrorResponse) => {
        this.handleError(error);
        return throwError(error);        
      });
  }

  public post(urlKey:string, body?:any, baseUrl?:string): Observable<Response>{
    const base = baseUrl ? baseUrl : this.baseUrl;    
    return this.http.post<Response>(`${base + this.apiPath[urlKey]}`, body)
      .catch((error:HttpErrorResponse) => {
        this.handleError(error);
        return throwError(error);        
      });
  }

  public patch(urlKey:string, body?:any, baseUrl?:string): Observable<Response>{
    const base = baseUrl ? baseUrl : this.baseUrl;    
    return this.http.patch<Response>(`${base + this.apiPath[urlKey]}`, body)
      .catch((error:HttpErrorResponse) => {
        this.handleError(error);
        return throwError(error);        
      });
  }

  public delete(urlKey:string, params?:any, baseUrl?:string): Observable<Response>{
    const base = baseUrl ? baseUrl : this.baseUrl;
    var mainUrl = base + this.apiPath[urlKey];
    var x;
    if(params){
      for(x in params){
        mainUrl += params[x] + "/";
      }
    }    
    return this.http.delete<Response>(`${mainUrl}`)
      .catch((error:HttpErrorResponse) => {
        this.handleError(error);
        return throwError(error);        
      });
  }

  public put(urlKey:string, body?:any, id?:number, baseUrl?:string): Observable<Response>{
    const base = baseUrl ? baseUrl : this.baseUrl;    
    return this.http.put<Response>(`${base + this.apiPath[urlKey] + id + "/"}`, body)
      .catch((error:HttpErrorResponse) => {
        this.handleError(error);
        return throwError(error);        
      });
  }

  private handleError(error){
    if(error.status == 400 || error.status == 500 || error.status == 401){
      console.log(error);
    }
  }
}
