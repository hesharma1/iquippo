import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TechMasterService } from './services/tech-master.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import * as XLSX from 'xlsx';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ModelBulkErrorListComponent } from '../master-data/master-data.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../location-master/confirm-dialog/confirm-dialog.component';
import { AdminMasterService } from 'src/app/services/admin-master.service';

@Component({
  selector: 'app-technical-info-master-used',
  templateUrl: './technical-info-master-used.component.html',
  styleUrls: ['./technical-info-master-used.component.css']
})
export class TechnicalInfoMasterUsedComponent implements OnInit {
  showForm: boolean = false;
  displayedColumns = ["id", "category__display_name", "brand__display_name", "model__name", "gross_Weight", "operating_weight", "bucket_capacity", "engine_power", "lifting_capacity", "action"];
  mainData: Array<any> = [];
  editData: any;
  @ViewChild('input') input!: ElementRef;
  @ViewChild('fileInput') fileInput!: ElementRef;
  errorRecordList: boolean = false;
  isShowInvalidRecords: boolean = false;
  isDisplay: boolean = true;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;
  public currentTab = 0;
  techDataSource = new MatTableDataSource<any>([]);
  constructor(public dataService: TechMasterService, private notify: NotificationService, private dialog: MatDialog,
    private adminMasterService: AdminMasterService) {
    this.showForm = false;
    this.editData = null;
  }

  ngOnInit() {
    this.getTechUsedData();
  }

  getTechUsedData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.adminMasterService.getTechInfoMasterUsedList(payload).subscribe((res: any) => {
      this.total_count = res.count;
      this.mainData = res.results;
      if (!this.searchText) {
        if (this.mainData && this.mainData.length > 0) {
          this.isDisplay = true;
        }
        else {
          this.isDisplay = false;
        }
      }
      if (window.innerWidth > 768) {
        this.techDataSource.data = res.results;
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.techDataSource.data = [];
          this.listingData = [];
          this.listingData = res.results;
          this.techDataSource.data = this.listingData;
        } else {
          this.listingData = this.listingData.concat(res.results);
          this.techDataSource.data = this.listingData;
        }
      }
    });
  }

  formClose(e: any) {
    if (e && e.formsubmission) {
      this.getTechUsedData();
    }
    this.editData = null;
  }

  editTechInfo(data) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.editData = data;
    this.showForm = true;
  }

  removeTechInfo(data) {
    let payload = {
      id: data.id
    }
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.dataService.delete('techInfoMasterUsed', payload).subscribe(
            data => {
              this.getTechUsedData();
            },
            err => {
              console.log(err);
            }
          )
        }
      });
  }

  exportTechInfo() {
    let payload = {
      limit: 999
    }
    this.dataService.get('techInfoMasterUsed', null, payload).subscribe(
      (res: any) => {
        console.log("res", res.results);
        const excelData = res.results.map((r: any) => {
          console.log("r", r);
          return {
            "Category": r?.category?.display_name,
            "Brand": r?.brand?.display_name,
            "Model": r?.model?.name,
            "Gross Weight": r?.gross_Weight,
            "Operating Weight": r?.operating_weight,
            "Bucket Capacity": r?.bucket_capacity,
            "Engine Power": r?.engine_power,
            "Lifting Capacity": r?.lifting_capacity
          }
        });
        console.log("excelData", excelData);
        ExportExcelUtil.exportArrayToExcel(excelData, "Technical info used");
      }
    )
  }


  onFileChange(event: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      const data = XLSX.utils.sheet_to_json(ws);
      if (data != null && data.length > 0) {
        let modifiedData: Array<any> = [];
        modifiedData = data.map((t: any) => {
          t["Id"] = t["Id"] ? t["Id"] : "";
          return {
            "id": t["Id"],
            "category": t["Category"],
            "brand": t["Brand"],
            "model": t["Model"],
            "gross_Weight": t["Gross Weight"],
            "operating_weight": t["Operating Weight"],
            "bucket_capacity": t["Bucket Capacity"],
            "engine_power": t["Engine Power"],
            "lifting_capacity": t["Lifting Capacity"]
          }
        });

        // modifiedData.forEach(data => {
        //   data.category = this.dataSource
        // });
        this.dataService.patch('techInfoBatchUpdate', JSON.stringify(modifiedData)).subscribe(
          (res: any) => {
            this.fileInput.nativeElement.value = "";
            this.errorRecordList = res.data;
            if (res && res.failed_count > 0) {
              this.isShowInvalidRecords = true;
            }
            this.notify.success(
              res.successful_count + ' out of ' + res.total_count + ' records are processed successfully'
            );
            this.getTechUsedData();
          },
          err => {
            console.log(err);
          }
        )
      }
    };
  }

  viewErrorList() {
    const dialogRef = this.dialog.open(ModelBulkErrorListComponent, {
      width: '700px',
      panelClass: 'custom-modalbox',
      data: {
        errorRecordList: this.errorRecordList,
        isFromTechUsed: true
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  hideErrorList() {
    this.isShowInvalidRecords = false;
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getTechUsedData();
  }

  search() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getTechUsedData();
    }
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getTechUsedData();
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (this.currentTab == 0) {
          if ((this.total_count > this.techDataSource.data.length) && (this.techDataSource.data.length > 0)) {
            if(!this.scrolled){
              this.scrolled = true;
            this.page++;
            this.getTechUsedData();
            }
          }
        }
      }
    }
  }
}
