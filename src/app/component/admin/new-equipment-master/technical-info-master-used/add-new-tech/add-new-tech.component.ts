import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { Category } from '../../technical-info-master-new/models/category.model';
import { TechMasterService } from '../services/tech-master.service';

@Component({
  selector: 'app-add-new-tech',
  templateUrl: './add-new-tech.component.html',
  styleUrls: ['./add-new-tech.component.css']
})
export class AddNewTechComponent implements OnInit, OnChanges {
  @Output() closeForm = new EventEmitter();
  @Input() editData: any;

  addNewForm: FormGroup;
  categoryArr: Array<any>;
  brandArr: Array<any>;
  modelArr: Array<any>;
  toEdit: boolean = false;
  techData: any;

  constructor(public fb: FormBuilder, public dataService: TechMasterService, public spinner: NgxSpinnerService,
    public adminService: AdminMasterService) {
    this.categoryArr = [];
    this.brandArr = [];
    this.modelArr = [];

    this.addNewForm = this.fb.group({
      category: ['', Validators.compose([Validators.required])],
      brand: ['', Validators.compose([Validators.required])],
      model: ['', Validators.compose([Validators.required])],
      gross_Weight: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      operating_weight: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      bucket_capacity: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      engine_power: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      lifting_capacity: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])]
    })
  }

  ngOnInit(): void {
    if (!this.toEdit) {
      this.getCategories();
    }
  }

  ngOnChanges(change: any) {
    if (change.editData?.currentValue) {
      this.toEdit = true;
      this.techData = change.editData?.currentValue;
      this.getCategories();
      this.getBrands(this.techData?.category?.id);
      this.getModels(this.techData?.brand?.id);
      this.setData(this.techData);
    }
  }

  getCategories() {
    // this.dataService.get('categoryMaster').pipe(finalize(() => {   })).subscribe(
    //   (data: any) => {
    //     this.categoryArr = data.results;
    //   },
    //   err => {
    //     console.log(err);
    //   }
    // )
    let queryparams = 'limit=999&ordering=-id';
    this.adminService.getCategoryMaster(queryparams).subscribe((response: any) => {
      this.categoryArr = response.results as Category[];
    });

  }

  /*filterBrandByCategory(event) {
    this.categoryId = this.categoryList.filter(x => event.option.value === x.display_name).map(x => x.id).join(",");
    this.commonService.getBrandMasterByCatId(this.categoryId).subscribe(
      (res: any) => {
        this.brandFilterList = res.results;
        this.brandFilterData = res.results;
        this.ModelbrandName = '';
      },
      (err) => {
        console.error(err);
      }
    );
  }*/


  getBrands(e: number) {
    if (e) {
      this.addNewForm.patchValue({
        brand: '',
        model: '',
      })
      this.brandArr = [];
      this.modelArr = [];

      let payload = {
        id: e
      }
      this.dataService.get('brandMasterByCategory', payload).subscribe(
        (data: any) => {
          this.brandArr = data.results;
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  getModels(e: number) {
    if (e) {
      this.addNewForm.patchValue({
        model: '',
      })
      this.modelArr = [];

      let payload = {
        id: e
      }
      this.dataService.get('modelMasterByBrand', payload).subscribe(
        (data: any) => {
          this.modelArr = data.results;
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  close() {
    let obj = {
      close: true,
      formsubmission: false
    }
    this.toEdit = false;
    this.resetForm();
    this.closeForm.emit(obj);
  }

  resetForm() {
    this.categoryArr = [];
    this.brandArr = [];
    this.modelArr = [];
    this.addNewForm.setValue({
      category: '',
      brand: '',
      model: '',
      gross_Weight: null,
      operating_weight: null,
      bucket_capacity: null,
      engine_power: null,
      lifting_capacity: null
    });
  }

  setData(data: any) {
    this.addNewForm.patchValue({
      category: data?.category?.id,
      brand: data?.brand?.id,
      model: data?.model?.id,
      gross_Weight: data?.gross_Weight,
      operating_weight: data?.operating_weight,
      bucket_capacity: data?.bucket_capacity,
      engine_power: data?.engine_power,
      lifting_capacity: data?.lifting_capacity
    });
  }

  saveForm(value: any) {
    let payload = value;
    for (let x in payload) {
      if (payload[x] == '') {
        payload[x] = null;
      }
    }
    if (this.toEdit) {
      this.dataService.put('techInfoMasterUsed', JSON.stringify(payload), this.techData.id).subscribe(
        data => {
          let obj = {
            close: true,
            formsubmission: true
          }
          this.toEdit = false;
          this.closeForm.emit(obj);
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.dataService.post('techInfoMasterUsed', JSON.stringify(payload)).subscribe(
        data => {
          let obj = {
            close: true,
            formsubmission: true
          }
          this.closeForm.emit(obj);
        },
        err => {
          console.log(err);
        }
      )
    }

  }

}
