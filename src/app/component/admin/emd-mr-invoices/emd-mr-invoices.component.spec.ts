import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EMDMRInvoicesComponent } from './emd-mr-invoices.component';

describe('EMDMRInvoicesComponent', () => {
  let component: EMDMRInvoicesComponent;
  let fixture: ComponentFixture<EMDMRInvoicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EMDMRInvoicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EMDMRInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
