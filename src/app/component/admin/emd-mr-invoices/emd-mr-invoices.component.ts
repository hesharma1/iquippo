import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { SharedService } from 'src/app/services/shared-service.service';
import { CommonService } from 'src/app/services/common.service';
import { ConfirmationModalComponent } from 'src/app/component/shared/confirmation-modal/confirmation-modal.component';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { OfflinePaymentComponent } from 'src/app/component/admin/offline-payment/offline-payment.component';
import { SigninComponent } from '../../signin/signin.component';
import { ViewAllIdsComponent } from 'src/app/component/shared/view-all-ids/view-all-ids.component';
import { InvoiceFeeDetailsComponent } from '../enterprise-invoice/invoice-fee-details/invoice-fee-details.component';
import { ViewTransactionsComponent } from '../enterprise-invoice/view-transactions/view-transactions.component';


@Component({
  selector: 'app-emd-mr-invoices',
  templateUrl: './emd-mr-invoices.component.html',
  styleUrls: ['./emd-mr-invoices.component.css']
})

export class EMDMRInvoicesComponent implements OnInit {
  public displayedColumns: string[] = [
    "Asset_Id", "Asset_Name", "Asset_Location", "Buyer_Name", "Seller_Name", "Trade_Type", "Total_Price", "EMD_Amount", "Trade_Amount",
    "Sales_Invoice", "action"
  ];

  public displayedInvoiceColumns: string[] = [
    "Asset_Id", "Asset_Name", "Asset_Location", "Buyer_Name", "Seller_Name", "Trade_Type", "Total_Price", "EMD_Amount", "Trade_Amount",
    "Sales_Invoice", "action"
  ];

  public statusOptions: Array<any> = [
    { name: 'Request In Draft', value: 1 },
    { name: 'Request Initiated', value: 2 },
    { name: 'Request Submitted', value: 3 },
    { name: 'Inspection In Progress', value: 4 },
    { name: 'Request On Hold', value: 5 },
    { name: 'Request Closed', value: 6 },
    { name: 'Inspection Completed', value: 7 },
    { name: 'Report Submitted', value: 8 },
    { name: 'Invoice Generated', value: 9 },
    { name: 'Invoice Modified', value: 10 },
    { name: 'Invoice Cancelled', value: 11 },
    { name: 'Payment Completed', value: 12 },
    { name: 'Request Cancelled', value: 13 },
    { name: 'Request Modified', value: 14 },
  ];

  public enterpriseValuationForm: any = this.formbuilder.group({
    invoice_number: new FormControl(''),
    service_number: new FormControl(''),
    services: new FormControl(''),
    gst_number: new FormControl(''),
    pan_number: new FormControl(''),
    taxable_amount: new FormControl('', Validators.required),
    amount: new FormControl(''),
    po_ref_no: new FormControl(''),
    po_ref_date: new FormControl(''),
    sac_code: new FormControl(null),
    billing_address: new FormControl(''),
    is_gst: new FormControl(false),
    is_self_invoice: new FormControl(false),
    is_cgst: new FormControl(false),
    is_igst: new FormControl(false),
    billed_to: new FormControl(''),
    service_to: new FormControl(''),
    type: new FormControl(null),
    invoice_date: new FormControl(new Date()),
    status: new FormControl(2),
    country: new FormControl('', Validators.required),
    state_name: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    city_name: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    pin_code: new FormControl(null),
    registration_number: new FormControl(''),
    gst_state: new FormControl('', Validators.required),
    gst_registration_number: new FormControl('', Validators.required)
  });

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  activePageInvoice: any = 1;
  pageSizeInvoice: number = 10;
  orderingInvoice: string = "-id"
  searchTextInvoice!: string;
  total_count_invoice: any;
  fromDate;
  requestType;
  checkedAll = false;
  editId = '';
  stateList: any = [];
  country_data: any = [];
  gstDetails: any;
  gstState;
  public showForm: boolean = false;
  editData;
  toDate;
  fromDateInvoice;
  toDateInvoice;
  enterprise;
  enterpriseCustomerList = [];
  public searchText!: string;
  public activePage: any = 1;
  public total_count: any;
  public invoiceGeneratedData!: MatTableDataSource<any>;
  public generatedInvoiceData!: MatTableDataSource<any>;
  public ordering: string = "-id"
  public pageSize: number = 10;
  public filter: any;
  public addRow: any = [];
  todayDate = new Date();
  iquippoGstList;
  stateGST;
  is_gst: boolean = false
  is_cgst: boolean = false
  is_igst: boolean = false
  public cognitoId!: string;
  bidId: any;
  constructor(private dialog: MatDialog,
    public spinner: NgxSpinnerService,
    public valService: ValuationService,
    private masterAdminService: AdminMasterService,
    private formbuilder: FormBuilder,
    private datePipe: DatePipe,
    private agreementServiceService: AgreementServiceService,
    public storage: StorageDataService, public notify: NotificationService,
    private sharedService: SharedService, private router: Router, private commonService: CommonService,
    private appRouteEnum: AppRouteEnum, public changeDetect: ChangeDetectorRef) {
  }

  /**ng on init */
  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.invoiceGeneratedData = new MatTableDataSource();
    this.generatedInvoiceData = new MatTableDataSource();
    this.getToBeGeneratedInvoiceData();
    this.getGeneratedInvoiceData();
    this.getEnterpriseCustomer();
    this.masterAdminService.getCountryList().subscribe((result: any) => {
      this.country_data = result.results;
    });
  }

  /**get list of to be generated invoice from trade bid api **/
  getToBeGeneratedInvoiceData() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
      invoice_trade_listing: 'BP'
    };

    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.fromDate) {
      payload['request_date__gte'] = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
    }
    if (this.toDate) {
      payload['request_date__lte'] = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
    }
    if (this.requestType) {
      payload['request_type__in'] = this.requestType;
    }
    if (this.enterprise) {
      payload['enterprise__id__in'] = this.enterprise;
    }

    this.masterAdminService.getTradeBidRecords(payload).subscribe((res: any) => {
      this.total_count = res.count;
      this.invoiceGeneratedData.data = res.results;
      console.log("invoiceGeneratedData", this.invoiceGeneratedData.data);
    });

  }

  /** Invoice generated **/
  getGeneratedInvoiceData() {
    let payload = {
      page: this.activePageInvoice,
      limit: this.pageSizeInvoice,
      ordering: this.orderingInvoice,
      type__in: 4,
      status__in: "1,2"
    };
    if (this.searchTextInvoice) {
      payload['search'] = this.searchTextInvoice
    }
    if (this.fromDateInvoice) {
      payload['invoice_created_date__gte'] = this.datePipe.transform(this.fromDateInvoice, 'yyyy-MM-dd');
    }
    if (this.toDateInvoice) {
      payload['invoice_created_date__lte'] = this.datePipe.transform(this.toDateInvoice, 'yyyy-MM-dd');
    }
    this.valService.getIndividualInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count_invoice = res.count;
          this.generatedInvoiceData.data = res.results;
          console.log("generatedInvoiceData", this.generatedInvoiceData.data);
        }
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    )
  }

  /**get row status chnage */
  getRowstatus(status) {
    if (status == '1') {
      return "Request in Draft"
    } else if (status == '2') {
      return "Request Submitted"
    } else if (status == '3') {
      return "Payment Done"
    } else if (status == '4') {
      return "Payment Cancelled"
    }
    else if (status == '5') {
      return "Report Generated"
    }
    return "";
  }

  closeForm() {
    this.showForm = false;
    this.enterpriseValuationForm.reset();
  }

  /**search list  */
  searchInList(type) {
    if (this.searchText === '' || this.searchText?.length > 3 || this.searchTextInvoice === '' || this.searchTextInvoice?.length > 3) {
      if (type == 'togenerate') {
        this.activePage = 1;
        this.getToBeGeneratedInvoiceData();
      } else if (type == 'generated') {
        this.activePageInvoice = 1;
        this.getGeneratedInvoiceData();
      }
    }
  }

  countryChange() {

    this.enterpriseValuationForm?.get('pin_code')?.setValue('');
    this.enterpriseValuationForm?.get('state')?.setValue('');
    this.enterpriseValuationForm?.get('city')?.setValue('');
  }

  statusFilter(val) {
    this.filter = val;
    this.activePage = 1;
    this.getToBeGeneratedInvoiceData();
  }

  getLocationData() {

    if (this.enterpriseValuationForm.get('country')?.value == 1) {
      let pincode = this.enterpriseValuationForm.get('pin_code')?.value;
      pincode = pincode.toString();
      if (pincode && pincode.length > 5) {
        let queryParam = `pin_code__contains=${pincode}`;
        this.agreementServiceService.getPinCode(queryParam).subscribe(
          (res: any) => {
            this.stateList = res.results;
            this.enterpriseValuationForm?.get('city_name')?.setValue(this.stateList[0].city?.name);

            this.enterpriseValuationForm?.get('pin_code')?.setValue(this.stateList[0].pin_code);
            this.enterpriseValuationForm?.get('state_name')?.setValue(this.stateList[0].city.state?.name);
            this.enterpriseValuationForm?.get('state')?.setValue(this.stateList[0].city.state?.id);
            this.enterpriseValuationForm?.get('city')?.setValue(this.stateList[0].city.id);
          },
          (err) => {
            console.error(err);
          }
        );
      }
    }
  }

  /**on chnage page from paginations */
  onPageInvoiceChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getGeneratedInvoiceData();
  }

  onSortInvoiceColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getGeneratedInvoiceData();
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.checkedAll = false;
    this.addRow = [];
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getToBeGeneratedInvoiceData();
  }

  onPageChangeInvoice(event: PageEvent) {
    this.activePageInvoice = event.pageIndex + 1;
    this.pageSizeInvoice = event.pageSize;
    this.getGeneratedInvoiceData();
  }

  onSortColumn(event) {
    if (event.active != "check") {
      this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
      this.getToBeGeneratedInvoiceData();
    }
  }

  edit(row) {
    this.enterpriseValuationForm.patchValue({
      invoice_date: row.invoice_date,
      invoice_in_favour_of: row.billed_to,
      po_ref_no: row.po_ref_number,
      po_ref_date: row.po_ref_date,
      sac_code: row.sac_code,
      address: row.billing_address,
      country: row.country,
      pin_code: row.pin_code,
      services: row.service_description,
    })
    this.showForm = true;
    this.getLocationData();
  }

  opencallpopup(row: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = {
      category: row.valuation_asset?.category.display_name,
      brand: row?.valuation_asset?.brand.display_name,
      model: row?.valuation_asset?.model.name,
      ucn: row.unique_control_number,
      mfg_year: row.valuation_asset.mfg_year,
      is_original_invoice: row.valuation_asset.is_original_invoice,
      is_equipment_registered: row.valuation_asset.is_equipment_registered,
      reg_number: row?.valuation_asset?.rc_number,
      asset_condition: row?.valuation_asset?.asset_condition,
      is_equipment_accidentally_damaged: row.valuation_asset.is_equipment_accidentally_damaged
    };

    const dialogRef = this.dialog.open(GenerateReportComponent, dialogConfig);

    //dialogRef.afterClosed().subscribe(result => {
    //    console.log(`Dialog result: ${result}`);
    //});
  }

  /**Export Function */
  export(string) {
    let payload = {
      limit: 999
    }
    if (string != 'all') {
      if (this.filter) {
        payload['status__in'] = this.filter;
      }
    }
    this.valService.getInstaValuation(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "UCN": r?.unique_control_number,
            "Name": r?.valuation_asset?.seller.first_name,
            "Mobile": r?.valuation_asset?.seller.mobile,
            "Email": r?.valuation_asset?.seller.email,
            "Category": r?.valuation_asset?.category.display_name,
            "Brand": r?.brand?.display_name,
            "Model": r?.model?.name,
            "Manufacturing Year": r?.valuation_asset?.mfg_year,
            "Invoice Availability": r?.valuation_asset?.is_original_invoice,
            "Invoice Amount": r?.valuation_asset?.invoice_value,
            "Registration Status": r?.valuation_asset?.is_rc_available,
            "Registration number": r?.valuation_asset?.rc_number,
            "Run": r?.valuation_asset?.hmr_kmr,
            "Accident Update": r?.valuation_asset?.is_equipment_accidentally_damaged,
            "Asset Condition": r?.valuation_asset?.asset_condition,
            "Status": this.getRowstatus(r?.status),
            "Created At": r?.created_at
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Insta Valuation List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  checkEnterpriseRow(e, row) {
    if (e.checked) {
      row.isSelected = true;
      this.changeDetect.detectChanges();
      for (let item of this.addRow) {
        if (item.enterprise_id === row.enterprise_id) {
          item.no_of_records = item.no_of_records + 1;
          item.ucn.push(row.unique_control_number);
          item.description = 'Various Asset'
          return;
        }
      }
      let obj = {
        ucn: [row.unique_control_number],
        request_type: row.request_type,
        enterprise: row.enterprise,
        enterprise_id: row.enterprise_id,
        no_of_records: row.no_of_records,
        description: row.asset_category
      };
      this.addRow.push(obj);
    } else {
      row.isSelected = false;
      this.changeDetect.detectChanges();
      let index = this.addRow.indexOf(row);
      this.addRow.splice(index, 1);
    }
    if (this.addRow && this.addRow?.length == 0) {
      this.checkedAll = false;
      this.changeDetect.detectChanges();
    }
  }

  //to be generetd //
  openGenerateInvoice(data) {
    this.bidId = data?.id;
    let payload: any
    payload = {
      "ucns": data?.id,
      "type": 4
    }
    this.valService.getTotalFee(payload).subscribe((res: any) => {
      console.log("getTotalFee", res);
      this.enterpriseValuationForm.patchValue({
        taxable_amount: res?.total_fees,
        billed_to: res?.enterprise
      });
      if (res?.message) {
        this.notify.error(res?.message, true);
      } else {
        payload = {
          "services_nature__in": 4
        }
        this.valService.getSaacCode(payload).subscribe((res: any) => {
          console.log("getSaacCode", res);
          this.enterpriseValuationForm.patchValue({
            sac_code: res?.results[0]?.sac_code,
            services: res?.results[0]?.description + ' ' + data?.description
          });
          payload = {
            "sac_code__sac_code__in": res?.results[0]?.sac_code
          }
          this.valService.getGstRate(payload).subscribe((res: any) => {
            console.log("getGstRate", res);
            this.gstDetails = res?.results;
          })
          this.getIquippoGst();
          let enterprise = {
            partner__id__in: data?.enterprise_id
          }
          this.valService.getPartnerCorporateAddress(enterprise).subscribe((res: any) => {
            this.enterpriseValuationForm.patchValue({
              billing_address: res?.results[0]?.address,
              country: res?.results[0]?.pin_code?.city?.state?.country?.id,
              pin_code: res?.results[0]?.pin_code?.pin_code
            })
            for (let idProof of res?.results[0]?.identity_proof) {
              if (idProof?.proof_type == 6) {
                this.enterpriseValuationForm.patchValue({
                  registration_number: idProof?.id_number
                });
                break;
              }
            }
            if (!this.enterpriseValuationForm.get('registration_number').value) {
              this.enterpriseValuationForm.patchValue({
                registration_number: res?.results[0]?.tax_refernce_number
              });
            }
            this.getLocationData();
          })

          this.masterAdminService.getCountryList().subscribe((result: any) => {
            this.country_data = result?.results;
          });
        });

        this.showForm = true;
        this.editData = data;
      }
    })
  }

  getIquippoGst() {
    let payload = {
      "doc_type__in": 2
    }
    this.valService.getIquippoGst(payload).subscribe((res: any) => {
      this.iquippoGstList = res.results;
      this.enterpriseValuationForm.patchValue({
        gst_state: this.iquippoGstList[0].state.id
      });
      this.gstStateChange(this.iquippoGstList[0]);
    });
  }

  gstStateChange(e) {
    this.enterpriseValuationForm.get('gst_registration_number').setValue(e.doc_number);
    this.gstState = e.state.id;
  }

  saveData() {
    var modifyInvoiceRequest: any = {

    }
    modifyInvoiceRequest.invoice_number = this.editId;
    // modifyInvoiceRequest.enterprise_valuation = this.editData.ucn;
    modifyInvoiceRequest.service_number = this.bidId;
    modifyInvoiceRequest.billed_to = this.enterpriseValuationForm?.get('billed_to')?.value;
    modifyInvoiceRequest.service_to = this.enterpriseValuationForm?.get('service_to')?.value;
    modifyInvoiceRequest.is_self_invoice = true;
    modifyInvoiceRequest.is_cgst_applicable = this.is_cgst;
    modifyInvoiceRequest.is_sgst_applicable = this.is_gst;
    modifyInvoiceRequest.is_igst_applicable = this.is_igst;
    modifyInvoiceRequest.cgst_rate = this.is_cgst ? this.gstDetails[1].service_tax_rate : null;
    modifyInvoiceRequest.sgst_rate = this.is_gst ? this.gstDetails[0].service_tax_rate : null;
    modifyInvoiceRequest.igst_rate = this.is_igst ? this.gstDetails[2].service_tax_rate : null;
    modifyInvoiceRequest.type = 4;
    modifyInvoiceRequest.bid = this.bidId;
    modifyInvoiceRequest.taxable_amount = this.enterpriseValuationForm?.get('taxable_amount')?.value;
    modifyInvoiceRequest.billing_address = this.enterpriseValuationForm?.get('billing_address')?.value;
    modifyInvoiceRequest.gst_number = this.enterpriseValuationForm?.get('registration_number')?.value;
    modifyInvoiceRequest.po_ref_number = this.enterpriseValuationForm?.get('po_ref_no')?.value;
    modifyInvoiceRequest.po_ref_date = this.datePipe.transform(this.enterpriseValuationForm?.get('po_ref_date')?.value, 'yyyy-MM-dd');
    modifyInvoiceRequest.service_description = this.enterpriseValuationForm?.get('services')?.value;
    modifyInvoiceRequest.sac_code = this.enterpriseValuationForm?.get('sac_code')?.value;
    modifyInvoiceRequest.country = this.enterpriseValuationForm?.get('country')?.value;
    modifyInvoiceRequest.state = this.enterpriseValuationForm?.get('state')?.value;
    modifyInvoiceRequest.city = this.enterpriseValuationForm?.get('city')?.value;
    modifyInvoiceRequest.pin_code = this.enterpriseValuationForm?.get('pin_code')?.value;
    modifyInvoiceRequest.iquippo_gst_state = this.gstState;
    modifyInvoiceRequest.iquippo_gst_number = this.enterpriseValuationForm?.get('gst_registration_number')?.value;

    modifyInvoiceRequest.invoice_created_date = this.datePipe.transform(this.enterpriseValuationForm?.get('po_ref_date')?.value, 'yyyy-MM-dd');
    if (this.editId) {
      this.valService.putInvoice(modifyInvoiceRequest, this.editId).subscribe((res: any) => {
        this.notify.success('Invoice Generated Successfully');
        this.showForm = false;
        this.getToBeGeneratedInvoiceData();
        this.getGeneratedInvoiceData();
        this.editId = '';
      }
        ,
        err => {
          console.log(err);
          this.notify.error(err?.message);
        }
      )
    } else {
      this.valService.postInvoice(modifyInvoiceRequest).subscribe((res: any) => {
        this.notify.success('Invoice Generated Successfully');
        this.showForm = false;
        this.getToBeGeneratedInvoiceData();
        this.getGeneratedInvoiceData();
        this.editId = '';
      }
        ,
        err => {
          console.log(err);
          this.notify.error(err?.message);
        }
      )
    }

  }

  getRequestType(id: any) {
    let returnRequestType;
    switch (id) {
      case 1:
        returnRequestType = "Valuation";
        break;
      case 2:
        returnRequestType = "Inspection";
        break;
      case 3:
        returnRequestType = "GPS Installation";
        break;
      case 4:
        returnRequestType = "Photographs Only";
        break;
    }
    return returnRequestType;
  }

  getInvoiceStatus(id) {
    let status;
    switch (id) {
      case 1:
        status = "Invoice Generated";
        break;
      case 2:
        status = "Invoice Modified";
        break;
      case 3:
        status = "Invoice Cancelled";
        break;
    }
    return status;
  }

  viewDetails() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    let obj = {
      "ucn": this.bidId,
      "isCreditNote": false
    }
    dialogConfig.data = obj;
    const dialogRef = this.dialog.open(InvoiceFeeDetailsComponent, dialogConfig).afterClosed().subscribe((data) => {
      let payload: any
      payload = {
        "ucns": this.bidId,
        "type": 4
      }
      this.valService.getTotalFee(payload).subscribe((res: any) => {
        this.enterpriseValuationForm.patchValue({
          taxable_amount: res.total_fees
        });
      });
    });
  }

  downloadInvoice(url: any) {
    if (url != '') {
      this.sharedService.download(url);
    }
  }

  makePayment(r) {
    if (this.cognitoId) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = r;
      const dialogRef = this.dialog.open(OfflinePaymentComponent, dialogConfig).afterClosed().subscribe((data) => {
        this.getGeneratedInvoiceData();
      });
    }
  }

  checkAuthentication() {
    return new Promise((resolved, reject) => {
      if (this.cognitoId) {
        let rolesArray = this.storage.getStorageData("rolesArray", false);
        rolesArray = rolesArray.split(',');
        if (rolesArray.includes('Customer')) {
          let payload = {
            cognito_id: this.cognitoId,
            role: 'Customer'
          }
          this.commonService.validateSeller(payload).subscribe(
            (res: any) => {
              if (!res.kyc_verified || !res.pan_address_proof) {
                this.notify.warn('Please submit KYC and Address proof', true);
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                  width: '500px',
                  disableClose: true,
                  data: {
                    heading: 'KYC Details Incomplete',
                    message: 'You need to complete your KYC profile with iQuippo to make payments.',
                    submessage: 'Click Yes if you want to proceed or No if you want to do this later. '
                  }
                });

                dialogRef.afterClosed().subscribe(val => {
                  if (val) {
                    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: this.cognitoId } });
                  }
                })
                resolved(false);
                return;
              } else {
                resolved(true);
              }
            },
            err => {
              console.log(err);
              reject(false);
            }
          );
        } else {
          this.notify.error('Please login as Customer to proceed further with asset buy!!');
        }
      }
      else {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'my-class';
        dialogConfig.width = '900px';
        dialogConfig.data = {
          flag: true
        }
        const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
        //dialogRef.afterClosed().subscribe(result => {
        //    console.log(`Dialog result: ${result}`);
        //});


        dialogRef.afterClosed().subscribe(val => {
          if (val) {
            this.cognitoId = this.storage.getStorageData('cognitoId', false);
          }
        })
      }
    });
  }

  filterFromDate() {
    this.getToBeGeneratedInvoiceData();
  }

  getEnterpriseCustomer() {
    let request = {
      partnership_type__in: 5,
      status__in: 2,
      limit: 999
    }
    this.valService.getEnterpriseCustomerList(request).subscribe((res: any) => {
      this.enterpriseCustomerList = res.results;
    })
  }

  viewTransaction(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    dialogConfig.data = row.transaction;
    const dialogRef = this.dialog.open(ViewTransactionsComponent, dialogConfig).afterClosed().subscribe((data) => {
    });
  }

  viewAllUcns(data) {
    const dialogRef = this.dialog.open(ViewAllIdsComponent, {
      width: '500px',
      disableClose: true,
      data: {
        isUCN: true,
        ids: data,
      }
    });
  }

  onSellerSelect(search: any) {
    this.iquippoGstList.filter((res) => {
      if (res.state.id == search?.value) {
        this.gstStateChange(res);
      }
    });
  }

  checkall(e) {
    if (e.checked) {
      this.checkedAll = true;
      for (let row of this.invoiceGeneratedData.data) {
        row.isSelected = true;
        for (let item of this.addRow) {
          if (item.enterprise_id === row.enterprise_id) {
            item.no_of_records = item.no_of_records + 1;
            item.ucn.push(row.unique_control_number);
            item.description = 'Various Asset'
            // return;
          }
        }
        let obj = {
          ucn: [row.unique_control_number],
          request_type: row.request_type,
          enterprise: row.enterprise,
          enterprise_id: row.enterprise_id,
          no_of_records: row.no_of_records,
          description: row.asset_category
        };
        this.addRow.push(obj);
      }
      this.changeDetect.detectChanges();
    } else {
      this.checkedAll = false;
      for (let row of this.invoiceGeneratedData.data) {
        row.isSelected = false;
        let index = this.addRow.indexOf(row);
        this.addRow.splice(index, 1);
      }
      this.changeDetect.detectChanges();
    }
  }

  getCheckedStatus(row) {
    return row.isSelected;
  }

  getTotalAmount() {
    if (this.enterpriseValuationForm?.get('taxable_amount')?.value) {
      let amount = this.enterpriseValuationForm?.get('taxable_amount')?.value;
      let fee = 0;
      if (this.is_gst) {
        fee = fee + this.gstDetails[0].service_tax_rate
      }
      if (this.is_cgst) {
        fee = fee + this.gstDetails[1].service_tax_rate;
      }
      if (this.is_igst) {
        fee = fee + this.gstDetails[2].service_tax_rate;
      }
      amount = amount + amount * fee / 100;
      return amount;
    }
    return '';
  }

  getCheckedAll() {
    return this.checkedAll
  }

}
