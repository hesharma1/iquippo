import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';

import { catchError, finalize } from 'rxjs/operators';
import { BannerSettingsService } from './banner-settings.service';

export class BannerSettingsDataSource implements DataSource<any> {
  private listSubject = new BehaviorSubject<any[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private bannerSettingService: BannerSettingsService) {}

  loadBannerSettingsMaster(
    pageIndex: number,
    pageSize: number,
    sortDirection: string,
    filter: string,
    type : string,
    position__in : string,
    priority__in : string,
  ) {
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.bannerSettingService
        .getBannerSettingsList(
          pageIndex,
          pageSize,
          sortDirection,
          filter,
          type,
          position__in,
          priority__in
        )
        .subscribe((list) => {
          this.listSubject.next(list);
          
          resolve(list);
          // this.listSubject.next(list.results);
        });
    });
  }

  loadHomepageBannerSettings(
    pageIndex: number,
    pageSize: number,
    sortDirection: string,
    filter: string,
    type : string,
    section__in: string
  ) {
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.bannerSettingService
        .getHomePageBannerSettingsList(
          pageIndex,
          pageSize,
          sortDirection,
          filter,
          section__in
        )
        .subscribe((list) => {
          this.listSubject.next(list);
          resolve(list);
          // this.listSubject.next(list.results);
        });
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    console.log('Connecting data source');
    return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listSubject.complete();
    this.loadingSubject.complete();
  }
}
