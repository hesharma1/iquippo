import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Injectable({
  providedIn: 'root',
})
export class BannerSettingsService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService,
    private httpClient: HttpClient
  ) {}

  getBannerSettingsList(
    pageNumber,
    pageSize,
    sortOrder = 'asc',
    filter,
    type,
    position__in,
    priority__in
  ) {
    return this.httpClient
      .get(`${environment.bannersBaseUrl + this.apiPath.bannerSettingsList}`, {
        params: new HttpParams()
          .set('page', pageNumber.toString())
          .set('limit', pageSize.toString())
          .set('ordering', sortOrder)
          .set('search', filter)
          .set('banner_page_type__in',type)
          .set('position__in',position__in? position__in: '')
          .set('priority__in',priority__in? priority__in: '')
      })
      .pipe(map((res: any) => res));

    // return this.httpClient
    //   .get(
    //     `${
    //       environment.baseUrl + this.apiPath.categoryTechSpecification
    //     }?limit=${pageSize}&page=${pageNumber}`
    //   )
    //   .pipe(map((res: any) => res));
  }
  getHomePageBannerSettingsList(
    pageNumber,
    pageSize,
    sortOrder = '-id',
    filter,
    section__in
  ) {
    let object : any = {
      page : pageNumber.toString(),
      limit : pageSize.toString(),
      ordering : sortOrder,
      search : filter,
      section__in : section__in
    }
    let params = new HttpParams();
    params = object;
    // if(pageNumber.toString())
    // params.set('page', pageNumber.toString())
    // if(pageSize.toString())
    // params.set('limit', pageSize.toString())
    // if(sortOrder)
    // params.set('ordering', sortOrder)
    // if(filter)
    // params.set('search', filter)
    return this.httpClient
      .get(`${environment.bannersBaseUrl + this.apiPath.homePageBanner}`,  { params: params })
      .pipe(map((res: any) => res));

    // return this.httpClient
    //   .get(
    //     `${
    //       environment.baseUrl + this.apiPath.categoryTechSpecification
    //     }?limit=${pageSize}&page=${pageNumber}`
    //   )
    //   .pipe(map((res: any) => res));
  }
}
