import { ElementRef, HostListener, ViewChild } from "@angular/core";
import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { BannerSettingsDataSource } from "./services/banner-settings.datasource";
import { BannerSettingsService } from "./services/banner-settings.service";
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay,
} from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component";
import { NewEquipmentMasterService } from "src/app/services/new-equipment-master.service";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { ApiRouteService } from "src/app/utility/app.refrence";
import { S3UploadDownloadService } from "src/app/utility/s3-upload-download/s3-upload-download.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { environment } from "src/environments/environment";
import { FlexOrderDirective } from "@angular/flex-layout";
import { DatePipe } from "@angular/common";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { NgxSpinnerService } from 'ngx-spinner';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';

export enum homepageSectionsEnum {
  Jumbotron = 1,
  Marketing_Banner_Section_1 = 2,
  Marketing_Banner_Section_2 = 3,
  Marketing_Banner_Section_3 = 4,
  Marketing_Banner_Section_4 = 5,
  Marketing_Banner_Section_5 = 6,
  Marketing_Banner_Section_6 = 7,
  Marketing_Banner_Section_7 = 8,
  Marketing_Banner_Section_8 = 9,
}

export enum pageType {
  New = 1,
  Used = 2,
  HomePage = 3,
}
@Component({
  selector: 'app-banner-settings',
  templateUrl: './banner-settings.component.html',
  styleUrls: ['./banner-settings.component.css']
})

export class BannerSettingsComponent implements OnInit {
  startOriginalDate: any;
  bannerImage: string = "";
  documentName1: Array<any> = [];
  documentName2: Array<any> = [];
  documentName3: Array<any> = [];
  selectedValuesId: Array<any> = [];
  homepageSectionsType: typeof
    homepageSectionsEnum = homepageSectionsEnum;
  bannerForm: FormGroup;
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  public pageSize: number = 10;
  dataSource1 = new MatTableDataSource<[]>();
  dataSource: any = [];
  defaultImageUrlForNew: string = '';
  defaultImageUrlForOld: string = '';
  sellerType;
  seller:any = [];
  displayedColumns = [
    "id",
    "image",
    "banner_title",
    "banner_description",
    "position",
    "start_date",
    "end_date",
    "banner_link",
    "is_active",
    'actions'
  ];
  displayedColumns2 = [
    "id",
    "image",
    "banner_title",
    "banner_description",
    "priority",
    "start_date",
    "end_date",
    "banner_link",
    "is_active",
    'actions'
  ];
  displayedColumnHomepage = [
    "id",
    "image",
    "banner_title",
    "banner_description",
    "section",
    "order",
    "start_date",
    "end_date",
    "banner_link",
    "is_active",
    'actions'
  ];
  maxArrayLength = 5;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  listingData = [];
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('input') input!: ElementRef;
  specList!: any;
  showForm: boolean = false;
  public ordering: string = '-id';
  public searchText: string = "";
  bannerType: string = "New";
  public activePage: any = 1;
  positionArray = [{
    Text: "T",
    Name: "Top"
  },
  {
    Text: "L",
    Name: "Left"
  }, {
    Text: "R",
    Name: "Right"
  }, {
    Text: "BL",
    Name: "Bottom Left"
  }, {
    Text: "BR",
    Name: "Bottom Right"
  }];

  priorityArray = [{ value: 1 },
  { value: 2 },
  { value: 3 },
  { value: 4 },
  { value: 5 },
  { value: 6 },
  { value: 7 }
  ];

  homepageSections = [
    {
      id: 1,
      name: "Jumbotron"
    },
    {
      id: 2,
      name: "Marketing Banner Section 1"
    },
    {
      id: 3,
      name: "Marketing Banner Section 2"
    },
    {
      id: 4,
      name: "Marketing Banner Section 3"
    },
    {
      id: 5,
      name: "Marketing Banner Section 4"
    },
    {
      id: 6,
      name: "Marketing Banner Section 5"
    },
    {
      id: 7,
      name: "Marketing Banner Section 6"
    },
    {
      id: 8,
      name: "Marketing Banner Section 7"
    },
    {
      id: 9,
      name: "Marketing Banner Section 8"
    }
  ];
  certificateArray: Array<any> = [];
  columns;
  minDate: any;
  bannerPageType;
  conditionArray: Array<any> = [];
  isActive: any;
  originalPositionArray: any[] = [];
  originalPriorityArray: any[] = [];
  endOriginalDate: any;
  section__in: any = '';
  position__in: any = '';
  priority__in: any = '';
  sellerList;
  constructor(
    private bannerSettingService: BannerSettingsService,
    private dialog: MatDialog,
    public newEquipmentService: NewEquipmentMasterService,
    private notificationservice: NotificationService,
    public s3: S3UploadDownloadService,
    private fb: FormBuilder,
    private equipnmentService: NewEquipmentMasterService,
    private adminMasterService: AdminMasterService,
    private apiPath: ApiRouteService,
    private datePipe: DatePipe,
    private spinner:NgxSpinnerService,
    private usedEquipmentService: UsedEquipmentService
  ) {
    const currentYear = new Date();
    // let disableUntil = { year: currentYear.getFullYear() - 18, month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.showForm = false;
    this.bannerForm = this.fb.group({
      bannerTypeDropdown: ['', Validators.required],
      bannerTitle: [''],
      //  redirectionUrl: ['', Validators.compose([Validators.required, Validators.pattern(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/)])],
      redirectionUrl: ['', Validators.required],
      bannerDescription: [''],
      position: ['', Validators.required],
      priority: [''],
      isActive: ['', Validators.required],
      buttonText: [''],
      // certificate : [null, Validators.compose([ Validators.pattern(/^[0-9]*$/)])],
      imageUrl: [''],
      redirectionLogic: new FormArray([this.createRedirectionRow()]),
      section: [''],
      order: [''],
      redirectTo: [''],
      startDate: [''],
      endDate: [''],
      altImageText: [''],
      mark_for_lead: ['']
    });
  }

  createRedirectionRow() {
    return new FormGroup({
      column: new FormControl(''),
      condition: new FormControl(''),
      value: new FormControl('')
    });
  }

  removeRedirectionRow(i: number) {
    const control = <FormArray>this.bannerForm.controls['redirectionLogic'];
    control.removeAt(i);
  }
  addBannerLogicRow() {
    let redirectionRows = this.bannerForm.controls['redirectionLogic'] as FormArray;
    if (redirectionRows.length < this.maxArrayLength) {
      redirectionRows.push(this.createRedirectionRow());
    }
  }
  getBannersRedirectionLength() {
    let redirectionRows = this.bannerForm.controls['redirectionLogic'] as FormArray;
    return redirectionRows.length;
  }
  trackByFn(index: any, item: any) {
    return index;
  }
 
  listData:any = [];
  categoryList = [];
  brandList = [];
  modelList = [];
  filterValuesCategory(search: any, i) {
    let data = search.target.value
    let filtered = this.listData[i];
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }
  filterValuesBrand(search: any,i) {
    let data = search.target.value
    let filtered = this.listData[i];
    let filteredData: any;
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }
  filterValuesModal(search: any, i) {
    let data = search.target.value
    let filtered = this.listData[i];
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }
  columnsChange(value, i) {
    this.listData[i] = undefined;
    let column = this.columns.find(element => element.name == value)
    this.conditionArray[i] = [];
    this.conditionArray[i] = column.class_lookups;
    if (column.name == 'category') {
      let queryparams = "limit=999&ordering=-id"
      this.adminMasterService.getCategoryMaster(queryparams).subscribe((data: any) => {
        this.listData[i] = data.results
      });
    }
    if (column.name == 'brand') {
      let queryparams = "limit=999&ordering=-id"
      this.adminMasterService.getBrandMaster(queryparams).subscribe(
        (res: any) => {
          this.listData[i] = res.results;
        },
        (err) => {
          this.notificationservice.error(err)

        }
      );
    }
    if (column.name == 'model') {
      let queryparams = "limit=999&ordering=-id"
      this.adminMasterService.getModelMaster(queryparams).subscribe((res: any) => {
        if (res != null) {
          this.listData[i] = res.results;
        }
      })
    }
    if (column.name == 'certificate') {
      let payload = {
        limit:999
      };
      this.adminMasterService.getCertificateMasterList(payload).subscribe((res: any) => {
        if (res != null) {
          this.listData[i] = res.results;
        }
      })
    }
    if (column.name == 'seller (Dealer)') {
      this.sellerType = 'Dealer';
    }
    if (column.name == 'seller (Manufacturer)') {
      this.sellerType = 'Manufacturer';
    }
    if (column.name == 'seller (Customer)') {
      this.sellerType = 'Customer';
    }
  }

  searchSeller(e){
    const pattern = /[0-9\+\-\ ]/;
    //    let inputChar = String.fromCharCode(event.charCode);
    console.log("event.keyCode", e.keyCode);
    this.sellerList = [];

    if (e.keyCode != 8 && !pattern.test(e.key)) {
      e.preventDefault();
    }
    else if (e.target.value.length >=3) {
    if (this.sellerType == 'Dealer') {
      let payload = {
        "number": e.target.value,
        "source": "bulk_upload",
        "role":  'Dealer'
      }
      this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
        (res: any) => {
          this.sellerList = [];
          if(res.data)
          this.sellerList = res.data;
          console.log("sellerList",  this.sellerList)
        },
        (error) => {
          console.log("eror", error);
          this.sellerList = [];
        }
      );
    }
    if (this.sellerType == 'Manufacturer') {
      let payload = {
        "number": e.target.value,
        "source": "bulk_upload",
        "role":  'Manufacturer'
      }
      this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
        (res: any) => {
          this.sellerList = [];
          if(res.data)
          this.sellerList = res.data;
          console.log("sellerList",  this.sellerList)
        },
        (error) => {
          console.log("eror", error);
          this.sellerList = [];
        }
      );
    }
    if ( this.sellerType == 'Customer') {
      let payload = {
        "number": e.target.value,
        "source": "bulk_upload",
        "role":  'Customer'
      }
      this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
        (res: any) => {
          this.sellerList = [];
          if(res.data)
          this.sellerList = res.data;
          console.log("sellerList",  this.sellerList)
        },
        (error) => {
          console.log("eror", error);
          this.sellerList = [];
        }
      );
    }
  }
}


  transformEnum(value: any) {
    if (value) {
      let [first, ...rest] = value.split("_");
      if (rest.length === 0) return first;
      else return `${first} ${rest.join(" ")}`;
    }
  }


  getColumns() {
    if (this.bannerPageType == '1') {
      return this.displayedColumns;
    } else if (this.bannerPageType == '2') {
      return this.displayedColumns2;
    } else {
      return this.displayedColumnHomepage;
    }
  }

  async ngOnInit() {
    this.originalPositionArray = this.positionArray;
    this.originalPriorityArray = this.priorityArray;
    this.dataSource = new BannerSettingsDataSource(
      this.bannerSettingService
    );

    this.specList = await this.dataSource.loadBannerSettingsMaster(1, 10, 'asc', '', '1',this.position__in?this.position__in: '', 
    this.priority__in?this.priority__in: '' );
    this.bannerPageType = '1';
    this.dataSource1 = this.specList.results?.map((value: any) =>
      this.getProperties(value)
    );
    this.getMasterFields();
    this.getCertificateList('', '');
    this.editId = '';
     
  }

  getMasterFields() {
    this.columns = [
      {class_lookups: ["in"],
      name: "category"},
      {class_lookups: ["in"],
      name: "brand"},
      {class_lookups: ["in"],
      name: "model"},
      {class_lookups: ["in"],
      name: "seller (Customer)"},
      {class_lookups: ["in"],
      name: "seller (Dealer)"},
      {class_lookups: ["in"],
      name: "seller (Manufacturer)"},
      {class_lookups: ["in"],
      name: "certificate"}
    ];
    // let string;
    // if (this.bannerPageType == '1') {
    //   string = 'NewEquipment';
    // } else {
    //   string = 'UsedEquipment';
    // }
    // let queryParams = 'app-label=product&model-name=' + string;
    // this.equipnmentService.getMasterFields(queryParams).subscribe((res: any) => {
    //   this.columns = [];
    //   for(let item of res.data){
    //     if(item.name == 'category' ||item.name == 'brand' ||item.name == 'model'){
    //       this.columns.push(item)
    //     }
    //   }
     
    // });
  }

  resetForm() {
    this.bannerForm.reset();
    this.editId = '';
    // this.showForm = false;
    this.documentName1 = [];
    this.documentName2 = [];
    this.documentName3 = [];
    this.bannerImage = "";
  }

  cancelForm() {
    this.bannerForm.reset();
    this.editId = '';
    this.showForm = false;
    this.documentName1 = [];
    this.documentName2 = [];
    this.documentName3 = [];
    this.bannerImage = "";
  }
  onSellerSelect(i,cognito){
    this.seller[i]=cognito;
  }

  checkedLeadCheckbox(e){
    if(e.checked){
      this.bannerForm?.get('redirectionUrl')?.setValue('raise-callback');
    }
  }

  saveForm(form) {

    if (this.startOriginalDate == this.datePipe.transform(form.startDate, 'yyyy-MM-dd')) {
      this.bannerForm.get('startDate')?.clearValidators();
      this.bannerForm.get('startDate')?.updateValueAndValidity();
    }

    if (this.endOriginalDate == this.datePipe.transform(form.endDate, 'yyyy-MM-dd')) {
      console.log("end date true...");
      this.bannerForm.get('endDate')?.clearValidators();
      this.bannerForm.get('endDate')?.updateValueAndValidity();
    }
    let array = form.redirectionLogic;
    let filterdArray = array.filter(x => x.column != null && x.condition && x.value);

    // if (form.redirectionUrl) {
    //   this.bannerForm.get('redirectionUrl')?.setValidators(Validators.pattern(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/));
    //   this.bannerForm.get('redirectionUrl')?.updateValueAndValidity();
    // }
    if (!form.redirectionUrl && (filterdArray && filterdArray.length <= 0)) {
      this.bannerForm.get('redirectionUrl')?.setValidators(Validators.required);
      this.bannerForm.get('redirectionUrl')?.updateValueAndValidity();
    }
    else if (!form.redirectionUrl && (filterdArray && filterdArray.length <= 0)) {
      if (!form.redirectionUrl) {
        this.bannerForm.get('column')?.setValidators(Validators.required);
        this.bannerForm.get('column')?.updateValueAndValidity();
        this.bannerForm.get('condition')?.setValidators(Validators.required);
        this.bannerForm.get('condition')?.updateValueAndValidity();
        this.bannerForm.get('value')?.setValidators(Validators.required);
        this.bannerForm.get('value')?.updateValueAndValidity();
      }
    }
    else {
      this.bannerForm.get('redirectionUrl')?.clearValidators();
      this.bannerForm.get('redirectionUrl')?.updateValueAndValidity();
      this.bannerForm.get('column')?.clearValidators();
      this.bannerForm.get('column')?.updateValueAndValidity();
      this.bannerForm.get('condition')?.clearValidators();
      this.bannerForm.get('condition')?.updateValueAndValidity();
      this.bannerForm.get('value')?.clearValidators();
      this.bannerForm.get('value')?.updateValueAndValidity();
    }

    this.bannerForm.markAllAsTouched();

    if (this.bannerForm.valid) {
      if ((this.bannerPageType == '1' && this.documentName1 && this.documentName1.length <= 0 && !this.bannerImage) ||
        (this.bannerPageType == '2' && this.documentName2 && this.documentName2.length <= 0) && !this.bannerImage ||
        (this.bannerPageType == '3' && this.documentName3 && this.documentName3.length <= 0 && !this.bannerImage)) {
        this.notificationservice.error(
          'Please Upload Banner Image'
        )
        return
      }
      else {
        console.log("valid form..");
        let data: any;
        let string;
        if(this.bannerForm.get('mark_for_lead')?.value == true){
          this.bannerForm.get('redirectionUrl')?.setValue('raise-callback');
        }else if (form.redirectionLogic) {
          let array = form.redirectionLogic;
          let i = 0;
          array.forEach(element => {
            if (element.column) {
              if (element.column == 'category') {
                element.column = 'category__id'
              } else if (element.column == 'brand') {
                element.column = 'brand__id'
              } else if (element.column == 'model') {
                element.column = 'model__id'
              }else if (element.column == 'certificate') {
                element.column = 'certificate__id'
              }else if (element.column == 'seller (Dealer)' ||element.column == 'seller (Manufacturer)' ||element.column == 'seller (Customer)') {
                element.column = 'seller__cognito_id'
              }
              if (string) {
                string = string + element.column
              } else {
                string = '?' + element.column
              }
              if (element.condition) {
                string = string + '__' + element.condition
              }
              if (element.value) {
                if(element.column == 'seller__cognito_id'){
                  string = string + '=' + this.seller[i] + '&'
                }else{
                  if(element.column == 'model__id' || element.column == 'brand__id' || element.column == 'category__id'){
                    string = string + '=' + this.selectedValuesId[i] + '&'
                  }else{
                    string = string + '=' + element.value + '&'
                  }
                }
                i++;
              }
            }
            
          });
        }
        if (this.bannerPageType != '3') {
          data = {
            banner_title: form.bannerTitle,
            button_label: form.buttonText,
            banner_description: form.bannerDescription,
            is_active: form.isActive,
            banner_type: form.bannerTypeDropdown,
            // banner_page_type :  form.bannerPageType,
            // certificate :  form.certificate,
            img_url: form.imageUrl ? form.imageUrl : this.bannerImage ? this.bannerImage : '',
            banner_page_type: this.bannerPageType,
            end_date: this.datePipe.transform(form.endDate, 'yyyy-MM-dd'),
            start_date: this.datePipe.transform(form.startDate, 'yyyy-MM-dd'),
            alt_image_text: form.altImageText

          }
          data.priority = form.priority ? form.priority : '1'
          data.position = form.position ? form.position : 'T'
          if (string) {
            if (this.bannerPageType == '1') {
              data.banner_link = '/product/new-equipment/' + string;
            } else {
              data.banner_link = '/product/used-equipment/' + string;
            }
          } else {
            data.banner_link = form.redirectionUrl;
          }
          console.log("data", data);
          if (this.editId) {
            this.equipnmentService.putBannerSettings(data, this.editId).subscribe(async (res) => {
              this.notificationservice.success("Your record has been successfully updated.")
              this.resetForm();
              this.showForm = false;
              this.specList = await this.dataSource.loadBannerSettingsMaster(1, 10, 'asc', '', this.bannerPageType,this.position__in?this.position__in: '', 
              this.priority__in?this.priority__in: '' );
              this.dataSource1 = this.specList.results.map((value: any) =>
                this.getProperties(value)
              );
            }, err => {
              // this.notificationservice.error(err)
            });
          }
          else {
            this.equipnmentService.postBannerSettings(data).subscribe(async (res:any) => {
              if(!res.message){
                this.notificationservice.success("Your record has been successfully added.")
                this.resetForm();
                this.showForm = false;
                this.section__in = '';
                this.specList = await this.dataSource.loadBannerSettingsMaster(1, 10, 'asc', '', this.bannerPageType,this.position__in?this.position__in: '', 
                this.priority__in?this.priority__in: '' );
                this.dataSource1 = this.specList.results.map((value: any) =>
                  this.getProperties(value));
                  
              }else{
                this.notificationservice.error(res.message);
              }
            }, err => {
              // this.notificationservice.error(err)
            });
          }
        }
        else {
          data = {
            banner_title: form.bannerTitle,
            button_label: form.buttonText,
            banner_description: form.bannerDescription,
            is_active: form.isActive,
            img_url: form.imageUrl ? form.imageUrl : this.bannerImage ? this.bannerImage : '',
            order: form.order,
            section: form.section,
            button_link: form.redirectionUrl,
            end_date: this.datePipe.transform(form.endDate, 'yyyy-MM-dd'),
            start_date: this.datePipe.transform(form.startDate, 'yyyy-MM-dd'),
            alt_image_text: form.altImageText
          }
          if (string) {
            if (form.redirectTo == '2') {
              data.button_link = '/product/used-equipment/' + string;
            } else {
              data.button_link = '/product/new-equipment/' + string;
            }
          } else {
            data.button_link = form.redirectionUrl;
          }
          console.log("data", data);
          if (this.editId) {
            this.equipnmentService.putHomePageBannerSettings(data, this.editId).subscribe(async (res:any) => {
              // this.dialogRef.close();
              if(!res.message){
              this.notificationservice.success("Your record has been successfully updated.")
              this.resetForm();
              this.dataSource = new BannerSettingsDataSource(
                this.bannerSettingService
              );
              this.specList = await this.dataSource.loadHomepageBannerSettings(
                1,
                10,
                '-id',
                '',
                '',
        this.section__in
              );
              this.dataSource1 = this.specList.results?.map((value: any) =>
                this.getPropertiesForHomepage(value)
              );
              this.resetForm();
              this.showForm = false;
              }else{
                this.notificationservice.error(res.message);
              }
            }, err => {
              this.notificationservice.error(err)

            });
          }
          else {
            this.equipnmentService.postHomePageBannerSettings(data).subscribe(async (res) => {
              // this.dialogRef.close();
              this.notificationservice.success("Your record has been successfully added.")
              this.resetForm();
              this.dataSource = new BannerSettingsDataSource(
                this.bannerSettingService
              );
              this.specList = await this.dataSource.loadHomepageBannerSettings(
                1,
                10,
                '-id',
                '',
                '',
        this.section__in
              );
              this.dataSource1 = this.specList.results?.map((value: any) =>
                this.getPropertiesForHomepage(value)
              );
              this.resetForm();
              this.showForm = false;
            }, err => {
              this.notificationservice.error(err)
            });
          }
        }
      }
    } else {
      return
    }
  }

  resetFile1() {
    this.documentName1 = [];
  }
  resetFile2() {
    this.documentName2 = [];
  }
  resetFile3() {
    this.documentName3 = [];
  }

  async handleUpload(event: any, side: string, type: string) {
    let section;
    let sortNo;
    if (this.bannerPageType == '3') {
      section = this.bannerForm.get('section')?.value;
    }
    if (this.bannerPageType == '1') {
      section = "New";
    }
    if (this.bannerPageType == '2') {
      section = "Used";
    }
    if (this.bannerPageType == '3') {
      sortNo = this.bannerForm.get('order')?.value;
    }
    if (this.bannerPageType == '2') {
      sortNo = this.bannerForm.get('priority')?.value;
    }
    if (this.bannerPageType == '1') {
      sortNo = this.bannerForm.get('position')?.value;
    }
    const file = event.target.files[0];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml') && file.size <= 5242880) {
      this.isInvalidfile = true;
      this.uploadingImage = true;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + '/' + side + '_' + section + '_' + sortNo + '.' + fileExt;
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        //this.viewingOptionsForm?.get('GeneralAppearance')?.setValue("");

        if (this.bannerPageType == '1') {
          this.bannerForm.controls['imageUrl'].setValue(uploaded_file.Location);
          this.documentName1.push({ name: fileFormat, url: uploaded_file.Location });
        }
        if (this.bannerPageType == '2') {
          this.bannerForm.controls['imageUrl'].setValue(uploaded_file.Location);
          this.documentName2.push({ name: fileFormat, url: uploaded_file.Location });
        }
        if (this.bannerPageType == '3') {
          this.bannerForm.controls['imageUrl'].setValue(uploaded_file.Location);
          this.documentName3.push({ name: fileFormat, url: uploaded_file.Location });
        }

      } else {
        // this.viewingOptionsForm?.get(tab)?.get('GeneralAppearance')?.get(side + 'View')?.setValue('');
      }
      this.uploadingImage = false;
    } else {
      this.notificationservice.error("Invalid Image.")
    }
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.changePaginationList(this.bannerType);
  }
  scrolled = false;
   // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.specList?.count > this.specList?.results.length) && (this.specList?.results.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
       this.changePaginationList(this.bannerType);
        }
      }
    }
  }
}

  async changePaginationList(type: string) {
    if(this.bannerType == 'Homepage'){
      type = '3'
    }else{
      type = this.bannerType == 'New' ? '1' : '2';
    }
    console.log("type", type);
    if (type != '3') {
      console.log("3 true")
      this.dataSource = new BannerSettingsDataSource(
        this.bannerSettingService
      );
      // Changes for Card layout on Mobile devices
      if(window.innerWidth > 768){
        this.specList = await this.dataSource.loadBannerSettingsMaster(
          this.activePage,
          this.pageSize,
          this.ordering,
          this.searchText,
          type,
          this.position__in?this.position__in: '', 
          this.priority__in?this.priority__in: '' 
        );        
      }else{
        this.scrolled = false;
          if(this.activePage == 1){
            this.specList = [];
          this.listingData = [];
          let results = await this.dataSource.loadBannerSettingsMaster(
            this.activePage,
            this.pageSize,
            this.ordering,
            this.searchText,
            type,
            this.position__in?this.position__in: '', 
            this.priority__in?this.priority__in: '' 
          );
          this.specList = results;
          this.listingData = results.results;
          this.specList.results = this.listingData;
        }else{
          let results = await this.dataSource.loadBannerSettingsMaster(
            this.activePage,
            this.pageSize,
            this.ordering,
            this.searchText,
            type,
            this.position__in?this.position__in: '', 
            this.priority__in?this.priority__in: '' 
          );
          this.specList.results = this.specList.results.concat(results.results);
      //this.dataList.results = this.listingData;
    }
  }
      this.bannerPageType = type;
      this.dataSource1 = this.specList.results?.map((value: any) =>
        this.getProperties(value)
      );
    } else {
      console.log("3 false");
      this.dataSource = new BannerSettingsDataSource(
        this.bannerSettingService
      );
      
      // Changes for Card layout on Mobile devices
      if(window.innerWidth > 768){
        this.specList = await this.dataSource.loadHomepageBannerSettings(
          this.activePage,
          this.pageSize,
          this.ordering,
          this.searchText,
          '',
          this.section__in
        );        
      }else{
          if(this.activePage == 1){
            this.specList = [];
          this.listingData = [];
          let results = await this.dataSource.loadHomepageBannerSettings(
            this.activePage,
            this.pageSize,
            this.ordering,
            this.searchText,
            '',
            this.section__in
          );
          this.specList = results;
          this.listingData = results.results;
          this.specList.results = this.listingData;
        }else{
          let results = await this.dataSource.loadHomepageBannerSettings(
            this.activePage,
            this.pageSize,
            this.ordering,
            this.searchText,
            '',
            this.section__in
          );
          this.specList.results = this.specList.results.concat(results.results);
      //this.dataList.results = this.listingData;
    }
  }
      this.bannerPageType = type;
      this.dataSource1 = this.specList.results?.map((value: any) =>
        this.getPropertiesForHomepage(value)
      );
    }

  }

  /**search list  */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.activePage = 1;
      this.changePaginationList(this.bannerType);
    }
  }

  getCertificateList(queryParams: string, value: string) {
    this.adminMasterService.getCertificateMasterDataList(this.apiPath.certificateMaster, queryParams).subscribe(
      (res: any) => {
        if (res != null) {
          this.certificateArray = this.certificateArray.concat(res.results);
        }
      });
  }

  getProperties(element: any) {
    return {
      id: element.id,
      banner_title: element.banner_title,
      banner_description: element.banner_description,
      position: element.position,
      priority: element.priority,
      is_active: element.is_active,
      start_date: element.start_date,
      end_date: element.end_date,
      img_url: element.img_url,
      banner_link: element.banner_link
    };
  }

  getPropertiesForHomepage(element: any) {
    return {
      id: element.id,
      banner_title: element.banner_title,
      banner_description: element.banner_description,
      is_active: element.is_active,
      button_label: element.button_label,
      button_link: element.button_link,
      img_url: element.img_url,
      order: element.order,
      section: element.section,
      start_date: element.start_date,
      end_date: element.end_date,
      banner_link: element.button_link
    };
  }

  // ngAfterViewInit() {
  //   this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
  //   fromEvent(this.input.nativeElement, 'keyup')
  //     .pipe(
  //       debounceTime(150),
  //       distinctUntilChanged(),
  //       tap(async () => {
  //         this.paginator.pageIndex = 0;
  //         this.specList = await this.loadBannerSettingsPage();
  //         this.dataSource1 = this.specList.results.map((value: any) =>
  //           this.getProperties(value)
  //         );
  //         // this.loadCategorySpecMasterPage();
  //       })
  //     )
  //     .subscribe();

  //   merge(this.sort.sortChange, this.paginator.page)
  //     .pipe(
  //       tap(async () => {
  //         this.specList = await this.loadBannerSettingsPage();
  //         this.dataSource1 = this.specList.results.map((value: any) =>
  //           this.getProperties(value)
  //         );
  //       })
  //     )
  //     .subscribe();

  // }

  loadBannerSettingsPage() {
    return new Promise((resolve) => {
      resolve(
        this.dataSource.loadBannerSettingsMaster(
          this.paginator.pageIndex + 1,
          this.paginator.pageSize,
          this.sort.direction == 'asc'
            ? this.sort.active
            : '-' + this.sort.active,
          this.input.nativeElement.value,
          this.bannerType == 'New' ? '1' : '2',
          this.position__in?this.position__in: '', 
        this.priority__in?this.priority__in: '' 
        )
      );
    });
  }


  editParticularData(dataId: number) {
    this.findDataById(dataId);
  }
  editId;
  findDataById(id: number) {
    this.isActive = undefined;
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    if (this.bannerPageType != '3') {
      this.newEquipmentService.editBannerSettingsField(id).subscribe((res: any) => {
        console.log("res new or used", res);
        if (res && res.banner_page_type == 1) {
          this.bannerType = 'New';
        }
        else {
          this.bannerType = 'Used';
        }
        this.bannerForm.patchValue({
          bannerTitle: res.banner_title,
          bannerLink: res.banner_link,
          redirectionUrl: res.banner_link,
          bannerDescription: res.banner_description,
          position: res.position,
          priority: res.priority,
          isActive: res.is_active,
          bannerPageType: res.banner_page_type,
          bannerTypeDropdown: res.banner_type.toString() != '' ? res.banner_type.toString() : '',
          //bannerType:res.banner_type?res.banner_type.toString():'',
          buttonText: res.button_label,
          imageUrl: res.img_url,
          startDate: this.datePipe.transform(res.start_date, 'yyyy-MM-dd'),
          endDate: this.datePipe.transform(res.end_date, 'yyyy-MM-dd'),
          altImageText: res.alt_image_text
          // certificate: res.certificate.id
        });
        
        if (res.banner_link.includes('/product/used-equipment/')) {
          res.banner_link = res.banner_link.replace("/product/used-equipment/", "")
          this.bannerForm.patchValue({
            redirectionUrl: ''
          })
        }
        if(res.banner_link == 'raise-callback'){
          this.bannerForm.patchValue({
            mark_for_lead : true});
        }else{
        let column = res.banner_link.substring(1,res.banner_link.indexOf('__'));
        res.banner_link = res.banner_link.slice(res.banner_link.indexOf('__'));
        console.log(column);
        let condition = 'in'
        let value = res.banner_link.substring(1,res.banner_link.indexOf('='));
        value = res.banner_link.replace(column, "");
        let redirectionRows = this.bannerForm.controls['redirectionLogic'] as FormArray;
        if (redirectionRows.length < this.maxArrayLength) {
          redirectionRows.removeAt(0);
          let group = new FormGroup({
            column: new FormControl(column),
            condition: new FormControl(condition),
            value: new FormControl(value)
          });
          redirectionRows.push(group);
        }
      }
        // if (form.redirectionLogic) {
        //   let array = form.redirectionLogic;
        //   let i = 0;
        //   array.forEach(element => {
        //     if (element.column) {
        //       if (element.column == 'category') {
        //         element.column = 'category__id'
        //       } else if (element.column == 'brand') {
        //         element.column = 'brand__id'
        //       } else if (element.column == 'model') {
        //         element.column = 'model__id'
        //       }else if (element.column == 'certificate') {
        //         element.column = 'certificate__id'
        //       }else if (element.column == 'seller (Dealer)' ||element.column == 'seller (Manufacturer)' ||element.column == 'seller (Customer)') {
        //         element.column = 'seller__cognito_id'
        //       }
        //       if (string) {
        //         string = string + element.column
        //       } else {
        //         string = '?' + element.column
        //       }
        //     }
        //     if (element.condition) {
        //       string = string + '__' + element.condition
        //     }
        //     if (element.value) {
        //       if(element.column == 'seller__cognito_id'){
        //         string = string + '=' + this.seller[i] + '&'
        //       }else{
        //         string = string + '=' + element.value + '&'
        //       }
        //       i++;
        //     }
        //   });
        // }
        if (res.banner_link.includes('/product/new-equipment/')) {
          res.banner_link = res.banner_link.replace("/product/new-equipment/", "")
          this.bannerForm.patchValue({
            redirectionUrl: '',

          })
        }
        this.isActive = res.is_active;
        this.bannerImage = res.img_url;
        console.log("bannerImage", this.bannerImage);
        this.startOriginalDate = this.datePipe.transform(res.start_date, 'yyyy-MM-dd');
        this.endOriginalDate = this.datePipe.transform(res.end_date, 'yyyy-MM-dd');
        this.editId = res.id;
        this.showForm = true;
      }, err => {
        this.notificationservice.error(err)
      })
    } else {
      this.newEquipmentService.editHomepageBannerSettingsField(id).subscribe((res: any) => {
        console.log("res home", res);
        if (res && res.banner_page_type == 3) {
          this.bannerType = 'Homepage';
        }
        this.bannerForm.patchValue({
          bannerTitle: res.banner_title,
          redirectionUrl: res.button_link,
          bannerDescription: res.banner_description,
          isActive: res.is_active,
          buttonText: res.button_label,
          imageUrl: res.img_url,
          order: res.order,
          section: res.section,
          startDate: this.datePipe.transform(res.start_date, 'yyyy-MM-dd'),
          endDate: this.datePipe.transform(res.end_date, 'yyyy-MM-dd'),
          altImageText: res.alt_image_text
        });
        if(res.button_link == 'raise-callback'){
          this.bannerForm.patchValue({
            mark_for_lead : true});
        }
        this.bannerImage = res.img_url;
        console.log("bannerImage", this.bannerImage);
        this.startOriginalDate = this.datePipe.transform(res.start_date, 'yyyy-MM-dd');
        this.endOriginalDate = this.datePipe.transform(res.end_date, 'yyyy-MM-dd');
        this.isActive = res.is_active;
        this.editId = res.id;
        this.showForm = true;
        // if (res.banner_type == 1) {
        //   this.positionArray = this.originalPositionArray.filter(x => x.Name == 'Top');
        //   this.priorityArray = this.originalPriorityArray.filter(x => x.value == 1);
        // }
        // else {
        //   this.positionArray = this.originalPositionArray.filter(x => x.Name != 'Top');
        //   this.priorityArray = this.originalPriorityArray.filter(x => x.value != 1);
        // }
      }, err => {
        this.notificationservice.error(err)
      })
    }
  }

  deleteBannerImage(url) {
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    console.log("imgpath", imgPath);
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notificationservice.success('File Successfully deleted!!');
        this.bannerImage = "";
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }

  deleteField(specId: number): void {
    // this.newEquipmentService.deleteCategoryField().subscribe();
    if (this.bannerPageType != '3') {
      const dialogRef = this.dialog
        .open(ConfirmDialogComponent, {
          width: '500px'
        })

        .afterClosed()
        .subscribe((data) => {
          if (data) {
            this.newEquipmentService
              .deleteBannerSettingsField(specId)
              .subscribe(async () => {
                this.specList = await this.loadBannerSettingsPage();
                this.dataSource1 = this.specList.results.map((value: any) =>
                  this.getProperties(value)
                );
              });
          }
        });
    } else {
      const dialogRef = this.dialog
        .open(ConfirmDialogComponent, {
        width: '500px'
      })
      
        .afterClosed()
        .subscribe((data) => {
          if (data) {
            this.newEquipmentService
              .deleteHomePageBannerSettingsField(specId)
              .subscribe(async () => {
                this.dataSource = new BannerSettingsDataSource(
                  this.bannerSettingService
                );
                this.specList = await this.dataSource.loadHomepageBannerSettings(
                  '',
                  '999',
                  '-id',
                  '',
                  '',
        this.section__in
                );
                this.dataSource1 = this.specList.results?.map((value: any) =>
                  this.getPropertiesForHomepage(value)
                );
                this.resetForm();
                this.showForm = false;
              });
          }
        });
    }
  }

  changesBannerType(bannerType: number) {
    if (bannerType == 1) {
      this.positionArray = this.originalPositionArray.filter(x => x.Name == 'Top');
      this.priorityArray = this.originalPriorityArray.filter(x => x.value == 1);
    }
    else {
      this.positionArray = this.originalPositionArray.filter(x => x.Name != 'Top');
      this.priorityArray = this.originalPriorityArray.filter(x => x.value != 1);
    }
  }

  changesBannerSetting(bannerType: string) {
    let type;
    this.position__in = ''
    this.activePage = 1;
    if (bannerType == 'New') {
      type = bannerType = '1';
      this.bannerForm.get('bannerTypeDropdown')?.setValidators(Validators.required);
      this.bannerForm.get('bannerTypeDropdown')?.updateValueAndValidity();
      this.bannerForm.get('position')?.setValidators(Validators.required);
      this.bannerForm.get('position')?.updateValueAndValidity();

      this.bannerForm.get('section')?.clearValidators();
      this.bannerForm.get('section')?.updateValueAndValidity();
      this.bannerForm.get('priority')?.clearValidators();
      this.bannerForm.get('priority')?.updateValueAndValidity();
      this.bannerForm.get('order')?.clearValidators();
      this.bannerForm.get('order')?.updateValueAndValidity();
    } else if (bannerType == 'Used') {
      type = bannerType = '2';
      this.bannerForm.get('bannerTypeDropdown')?.setValidators(Validators.required);
      this.bannerForm.get('bannerTypeDropdown')?.updateValueAndValidity();
      this.bannerForm.get('priority')?.setValidators(Validators.required);
      this.bannerForm.get('priority')?.updateValueAndValidity();

      this.bannerForm.get('section')?.clearValidators();
      this.bannerForm.get('section')?.updateValueAndValidity();
      this.bannerForm.get('position')?.clearValidators();
      this.bannerForm.get('position')?.updateValueAndValidity();
      this.bannerForm.get('order')?.clearValidators();
      this.bannerForm.get('order')?.updateValueAndValidity();
    } else if (bannerType == 'Homepage') {
      type = bannerType = '3';
      this.bannerForm.get('bannerTypeDropdown')?.clearValidators();
      this.bannerForm.get('bannerTypeDropdown')?.updateValueAndValidity();
      this.bannerForm.get('position')?.clearValidators();
      this.bannerForm.get('position')?.updateValueAndValidity();
      this.bannerForm.get('priority')?.clearValidators();
      this.bannerForm.get('priority')?.updateValueAndValidity();

      this.bannerForm.get('section')?.setValidators(Validators.required);
      this.bannerForm.get('section')?.updateValueAndValidity();
      this.bannerForm.get('order')?.setValidators(Validators.required);
      this.bannerForm.get('order')?.updateValueAndValidity();
    }
    this.bannerPageType = type;
    //this.showForm = false;
    this.resetForm();
    this.changePaginationList(type);
    this.getMasterFields();
  }

  changesRedirectionColumns(value) {
    let string;
    if (value == '1') {
      string = 'NewEquipment';
    } else {
      string = 'UsedEquipment';
    }
    let queryParams = 'app-label=product&model-name=' + string;
    this.equipnmentService.getMasterFields(queryParams).subscribe((res: any) => {
      this.columns = [];
      for(let item of res.data){
        if(item.name == 'category' ||item.name == 'brand' ||item.name == 'model'){
          this.columns.push(item)
        }
      }
    });
  }

  AddForm() {
    this.resetForm();
    this.showForm = true;
  }
  getConditionName(val) {
    switch (val) {
      case 'gte': { return 'Greater Than Equals To' }
      case 'lt': { return 'Less Than' }
      case 'gt': { return 'Greater Than' }
      case 'lte': { return 'Less Than Equals To' }
      case 'isnull': { return 'Not Having' }
      case 'exact': { return 'Equal To' }
      case 'iexact': { return 'Case insensitive equals' }
      case 'in': { return 'In' }
      case 'contains': { return 'Containing' }
      case 'icontains': { return 'Case insensitive contains' }
      case 'startswith': { return 'Starts with' }
      case 'istartswith': { return 'Case insensitive starts with' }
      case 'endswith': { return 'Ends with' }
      case 'iendswith': { return 'Case insensitive ends with' }
      case 'range': { return 'Min, Max' }
      default: { return '' }
    }
  }

  changePageType(data: any) {
    let type = '';
    switch (data.banner_page_type) {
      case 1:
        type = "New";
        break;
      case 2:
        type = "Used"
        break;
      case 3:
        type = "Homepage"
        break;
    }
    return type;
  }

  getColumnName(val) {
    let frags = val.split('_');
    for (let i = 0; i < frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }

  /**sort list  */
  // onSortColumn(event) {
  //   // this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  //   // this.getEmdList();
  // }

  statusFilter(val) {
    this.section__in = val? val : '';
    this.activePage = 1;
    this.changePaginationList(this.bannerType);
  }

  statusPosFilter(val) {
    this.position__in = val? val : '';
    this.activePage = 1;
    this.changePaginationList(this.bannerType);
  }

  statusSortFilter(val) {
    this.priority__in = val? val : '';
    this.activePage = 1;
    this.changePaginationList(this.bannerType);
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.changePaginationList(this.bannerType);
  }
  onValueSelect(id,i){
    this.selectedValuesId[i] = id;
  }
}