import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerContactListComponent } from './partner-contact-list.component';

describe('PartnerContactListComponent', () => {
  let component: PartnerContactListComponent;
  let fixture: ComponentFixture<PartnerContactListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerContactListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
