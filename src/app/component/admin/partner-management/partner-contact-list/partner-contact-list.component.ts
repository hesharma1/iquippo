import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { FilterSettingsModel } from '@syncfusion/ej2-angular-grids';
import { UserService } from 'src/app/services/user';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { GridComponent } from '@syncfusion/ej2-angular-grids'
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { VerifyuserComponent } from 'src/app/component/add-user/verify-user/verifyuser.component';
import { MatDialog } from '@angular/material/dialog';
import { ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/services/common.service';


export interface PeriodicElement {
  UserName: string;
  firstName: string;
  email: string;
  phoneNumber: string;
  kyc_verified: string;
  state: string;
  city: string;
  pan_address_proof?: string;
  Groups: string[];

}

@Component({
  selector: 'app-partner-contact-list',
  templateUrl: './partner-contact-list.component.html',
  styleUrls: ['./partner-contact-list.component.css']
})

export class PartnerContactListComponent implements OnInit {
  @ViewChild("TableOneSort", { static: true }) tableOneSort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('grid') public grid?: GridComponent;
  public activePage: any = 1;
  public total_count: any;
  public pageNumber: number = 1;
  cognitoId: any;
  pageSize: number = 10;
  dataSource!: MatTableDataSource<any>;
  displayedColumns: string[] = [
    "id", "user__first_name", "created_at", "partnership_type", "user__mobile_number", "user__email", "companyName", "rm_name", "status", "isActive", "Actions"
  ];

  role?: string;
  partnerContactList: any;
  productDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  partnercognitoId: any;
  public listingData = [];

  searchData: any = {
    'source': '',
    'mobileNumber': '',
    'firstName': ''
  };
  filterUserForm: any = this.formbulider.group({
    Mobile: '',
    Name: '',
  });
  public data: PeriodicElement[] = [];
  pageSettings: PageSettingsModel = { pageSize: 10 };
  public toolbarOptions?: ToolbarItems[] = ['ExcelExport'];
  filterOptions: FilterSettingsModel = {
    type: 'Menu'
  };
  ordering: any = '-id';
  scrolled: boolean = false;

  constructor(private partnerDealerRegistrationService: PartnerDealerRegistrationService, private router: Router, public userService: UserService, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private sharedService: SharedService, private storage: StorageDataService, public dialog: MatDialog, private appRouteEnum: AppRouteEnum, private formbulider: FormBuilder,
    private notificationservice: NotificationService, private apiRouteService: ApiRouteService, private commonService: CommonService
    , private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    if (this.storage.getStorageData('cognitoId', false) != null && this.storage.getStorageData('cognitoId', false) != undefined) {
      this.cognitoId = this.storage.getStorageData('cognitoId', false);
    }
    this.storage.clearStorageData('partnerCognitoId');
    // if (this.storage.getStorageData('partnerCognitoId', false) != null && this.storage.getStorageData('partnerCognitoId', false) != undefined) {
    //   this.partnercognitoId = this.storage.getStorageData('partnerCognitoId', false);
    // }
    this.role = this.storage.getStorageData("role", false);
    if (this.role != this.apiRouteService.roles.superadmin) {
      this.toolbarOptions = [];
    }
    // if(this.role==this.apiRouteService.roles.customer){
    //   this.sharedService.redirecteToMP1AfterLogin();
    // }
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['src']) {
        this.storage.clearStorageData("src");
        this.storage.setStorageData("src", params['src'], true);
        // this.sharedService.queryParamsAfterLogin = params;
        // this.storage.clearStorageData("aflparams");
        // this.storage.setStorageData("aflparams", params, true);
      }
      if (params['cognitoId']) {
        this.storage.clearStorageData("cognitoId");
        this.storage.setStorageData("cognitoId", params['cognitoId'].replace(/['"]+/g, ''), true);
      }
      if (params['deviceKey']) {
        this.storage.clearStorageData("deviceKey");
        this.storage.setStorageData("deviceKey", params['deviceKey'], true);
      }
      if (params['refreshToken']) {
        this.storage.clearStorageData("refreshToken");
        this.storage.setStorageData("refreshToken", params['refreshToken'], true);
      }
    });
    //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': ''
    };
    //this.getusersfn(this.searchData);
    this.initializeFilterFormGroup();
    this.getPartnerContactList();
  }
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getPartnerContactList();
  }
  togglestatus(userCog: any, status: any) {
    var payload = {
      "is_active": status == true ? false : true
    }
    this.userService.updateActiveStatus(userCog, payload).subscribe((res: any) => {
      if (res) {
        this.notificationservice.success("Partner status updated successfully.");
        this.getPartnerContactList();
      }
    }, err => {
      console.log(err);
    })
  }
  verifyusernav(id: any) {
    const dialogRef = this.dialog.open(VerifyuserComponent, {
      width: '650px',
      data: { id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      //this.getusersfn(this.searchData);
    });
  }

  toolbarClick(args: ClickEventArgs): void {
    if (args.item.id === 'Grid_excelexport') { // 'Grid_excelexport' -> Grid component id + _ + toolbar item name
      this.grid?.excelExport();
    }
  }

  addusernav() {
    this.router.navigate(['/admin-dashboard/partner-admin/create-partner-contact']);
  }

  editusernav(e: any, userName: any) {
    this.router.navigate([`./` + this.appRouteEnum.Profile], { queryParams: { cognitoId: userName, isCallCenter: 1 } });
    //this.router.navigate([`./`  + this.appRouteEnum.Adduser], { queryParams: { userName: userName } });
  }

  dataBound() {
    this.grid?.autoFitColumns(['email', 'phoneNumber']);
  }

  filterData() {
    //this.spinner.show();
    var formData = this.filterUserForm.value;
    if (formData != undefined) {
      this.searchData = {
        'source': 'filter',
        'mobileNumber': formData.Mobile,
        'firstName': formData.Name
      };
      //&search=
      this.partnerDealerRegistrationService.searchPartnerContact(formData.Mobile, formData.Name)
        .subscribe(
          (res: any) => {

            this.partnerContactList = res.results;
            this.total_count = res.count;
            this.productDataSource.data = res.results;
          },
          (err) => {

            console.error(err);
          }
        );
    }
  }
  /**on chnage pages pagination view */
  onPageChange(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPartnerContactList();
  }
  resetData() {
    // //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': ''
    };
    this.getPartnerContactList();
    this.initializeFilterFormGroup();
  }

  filterNameData() {
    if (this.filterUserForm.get('Name').value == '') {
      this.getPartnerContactList();
    }
  }
  initializeFilterFormGroup() {
    this.filterUserForm.setValue({
      Mobile: '',
      Name: ''
    });
  }

  getPartnerContactList() {
    //this.spinner.show();
    this.partnerDealerRegistrationService.getPartnerContactListWithoutIdordering(this.pageNumber, this.pageSize, this.ordering).subscribe(
      (res: any) => {
        this.partnerContactList = res.results;
        this.total_count = res.count;
        if (window.innerWidth > 768) {
          this.productDataSource.data = res.results;
        } else {
          this.scrolled = false;
          if (this.pageNumber == 1) {
            this.productDataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.productDataSource.data = this.listingData;
          } else {
            this.listingData = this.listingData.concat(res.results);
            this.productDataSource.data = this.listingData;
          }
        }

      },
      (err) => {

        console.error(err);
      }
    );
  }
  edit(row) {
    console.log(row);
    localStorage.setItem('partnerContactRecordById', JSON.stringify(row));
    this.storage.setStorageData('partnerCognitoId', row.user.cognito_id, false)
    // this.storage.getStorageData("partnerCognitoId", true).replace(/['"]+/g, '');
    this.router.navigate(['/admin-dashboard/partner-admin/create-partner-contact']);

  }
  delete(rowdata) {
    const actions = "success!!"
    this.partnerDealerRegistrationService.deleteContactEntity(rowdata.id).subscribe(
      (res: any) => {

        this.notificationservice.success("Entity deleted successfully")
        this.getPartnerContactList();
      },
      (err) => {
        console.error(err);
      }
    );

  }
  approve(rowdata, action) {
    console.log(rowdata)
    const actions = 'Success!!';
    let payload = {
      cognito_id: rowdata.user.cognito_id,
      role: 'PartnerGuest'
    }
    if (action != 3) {
      this.commonService.validateSeller(payload).subscribe((res: any) => {
        if ((!res.kyc_verified && res.kyc_verified != 2) || !res.pan_address_proof) {
          this.notificationservice.warn('Please ask user to upload KYC and Address proof or submit required details.', true);
          //   this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
          // this.router.navigate([`./` + this.appRouteEnum.partnerProfile], { queryParams: { createListing: true,isNew: false } });
          return;
        }
        if (!res.bank_details_exist) {
          this.notificationservice.warn('Please ask user to submit bank details or submit required details.', true);
          //    this.router.navigate([`./` + this.appRouteEnum.partnerBankingDetails], { queryParams: { createListing: true,isNew: false } });
          return;
        }
        // if (!res.allow && rowdata.partnership_type !=3) {
        //   this.notificationservice.warn('Please ask user to submit valid agreement or submit required details for the user.', true);
        //   return;
        //  }
        else {
          let request = {
            status: action,
            partnership_type: rowdata.partnership_type,
            created_by: this.cognitoId,
            user: rowdata.user.cognito_id
          };

          this.partnerDealerRegistrationService.approveRejectContactPartnerEntity(request, rowdata.id).subscribe(
            (res: any) => {

              // this._snackBar.open('Status updated successfully', actions, {
              //   duration: 8000,
              // });
              this.notificationservice.success("Status updated successfully")
              this.getPartnerContactList();
            },
            (err) => {
              console.error(err);
            }
          );

        }


      });
    }
    else {
      // for reject no checking
      let request = {
        status: action,
        partnership_type: rowdata.partnership_type,
        created_by: this.cognitoId,
        user: rowdata.user.cognito_id
      };

      this.partnerDealerRegistrationService.approveRejectContactPartnerEntity(request, rowdata.id).subscribe(
        (res: any) => {

          // this._snackBar.open('Status updated successfully', actions, {
          //   duration: 8000,
          // });
          this.notificationservice.success("Status updated successfully")
          this.getPartnerContactList();
        },
        (err) => {
          console.error(err);
        }
      );

    }
    // //this.spinner.show();

  }
  exportData() {
    let payload = {
      limit: 999,
    }
    this.partnerDealerRegistrationService.exportPartnerContactList(999, 'id').pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Id": r?.id,
            "Partnership type": this.setPartnershipType(r?.partnership_type),
            "Updated at": r?.updated_at,
            "Created at": r?.created_at,
            "Is channel partner?": r?.is_channel_partner == true ? "yes" : "no",
            "Status": this.getStatusType(r?.status),
            "User": r?.user?.first_name,
            "Mobile": r?.user?.mobile_number,
            "Email": r?.user?.email,
            "Company Name": r?.partner_company,
            "RM Name": r?.user?.relationship_manager_name,
            "Created by": r?.created_by,

          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "partner-contact");
      },
      err => {
        console.log(err);
        this.notificationservice.error(err.message);
      }
    );
  }
  setPartnershipType(id: any) {
    if (id != '' && id != undefined)
      return this.apiRouteService.partershipTypes[id - 1]?.value;
  }
  getStatusType(id: any) {
    let returnRequestType;
    switch (id) {
      case 0:
        returnRequestType = "Draft";
        break;
      case 1:
        returnRequestType = "Pending";
        break;
      case 2:
        returnRequestType = "Approved";
        break;
      case 3:
        returnRequestType = "Rejected";
        break;
      default:
        returnRequestType = "Draft";
        break;
    }
    return returnRequestType;
  }

  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.productDataSource.data.length) && (this.productDataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if (!this.scrolled) {
            this.scrolled = true;
            this.pageNumber++;
            this.getPartnerContactList();
          }
        }
      }
    }
  }

}


