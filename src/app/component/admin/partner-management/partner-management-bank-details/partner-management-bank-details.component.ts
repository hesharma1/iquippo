import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AccountService } from 'src/app/services/account';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Bankdetails } from 'src/app/shared/abstractions/user';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-partner-management-bank-details',
  templateUrl: './partner-management-bank-details.component.html',
  styleUrls: ['./partner-management-bank-details.component.css']
})
export class PartnerManagementBankDetailsComponent implements OnInit {
  documentName: Array<any> = [];
  public bankingForm: FormGroup;
  public steps = {
    current: 3,
    total: [
      { title: 'Profile' },
      { title: 'Preference' },
      { title: 'Banking' },
      // { title: 'Terms & Conditions' },
    ]
  }
  public toEdit: any;
  public bankList: Array<{ id: string; bankName: string }> = [];
  public congnitoId: string = 'dc3f1d50-f549-45d6-b5a0-a9f64d9d8fe2';
  objData: any;
  masterData: any;
  partnerEntity: any;
  partnerId: any;
  selectedPartnerCognito: any;
  PartnerType: any;
  entityDetails: any;
  manuFactBrands: any = [];
  isEditEntity = false;
  @ViewChild('cancelledCheque') cancelledCheque!: ElementRef;
  
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public storage: StorageDataService,
    private router: Router, private notify: NotificationService, public _Activatedroute: ActivatedRoute,
    private s3: S3UploadDownloadService, public accountService: AccountService, private agreementServiceService: AgreementServiceService) {

    this.bankingForm = this.fb.group({
      bankDetailArray: this.fb.array([])
    });
  }

  ngOnInit(): void {
    this.toEdit = this._Activatedroute.snapshot.queryParamMap.get('toEdit');
    this._Activatedroute.queryParams.subscribe((params) => {
      if (params['isEditEntity']) {
        if (params['isEditEntity'] == true) {
          this.isEditEntity = true;
        } else {
          this.isEditEntity = false;
        }
      }
    });
    if (this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true) != null && this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true) != undefined) {
      let cognitoId = this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true);
      if (cognitoId?.partner_admin?.cognito_id != undefined || cognitoId?.partner_admin?.cognito_id != null && this.isEditEntity) {
        this.selectedPartnerCognito = cognitoId?.partner_admin?.cognito_id;
        this.PartnerType = cognitoId?.partnership_type;
      }
    }
    if (this.storage.getStorageData('partnerEntityAdmin', true) != null && this.storage.getStorageData('partnerEntityAdmin', true) != undefined) {
      let selectedAdmin = this.storage.getStorageData('partnerEntityAdmin', true)
      let selectedType = this.storage.getStorageData('PARTNERADMINTYPE', true)
      if (selectedAdmin != undefined && selectedAdmin != null && selectedType != undefined && selectedType != null && !this.isEditEntity) {
        this.selectedPartnerCognito = selectedAdmin;
        this.PartnerType = selectedType;
      }
    }
    this._Activatedroute.params.subscribe((params: Params): void => {
      this.partnerId = params['id'];
    });
    this.getBankListfn();
    this.getEntityDetails();
    // this.addBankFormGroup();
    this.congnitoId = this.storage.getStorageData("userData", true)?.username;

    if (this.toEdit) {
    }
    if (
      this.storage.getStorageData('partnerEntityCreate', false) != null &&
      this.storage.getStorageData('partnerEntityCreate', false) != undefined
    ) {
      this.entityDetails = JSON.parse(
        this.storage.getStorageData('partnerEntityCreate', false)
      );
    }

  }

  get bankDetailsArray() {
    return this.bankingForm.controls.bankDetailArray as FormArray;
  }

  getBankListfn() {
    //this.spinner.show();
    this.accountService.getBankListOnboard('?limit=999&ordering=bank_name').pipe((finalize(() => { }))).subscribe(
      (data: any) => {
        this.bankList = data.results;
      }
    )
    this.partnerEntity = JSON.parse(this.storage.getStorageData("partnerEntityCreate", false))
    this.accountService.getPartnerBankDetails(this.partnerId).subscribe((res: any) => {
      console.log(res)
      this.objData = res.results;
      if (this.objData.length > 0) {
        this.objData.forEach((cust: Bankdetails) => {
          this.bankDetailsArray.push(this.createBankFormGroup(cust));
          //console.log(this.customersControls);
        });
      }
      else {
        this.addBankFormGroup();
      }
    })

  }

  getEntityDetails() {
    //this.spinner.show();
    this.agreementServiceService.getPartnerEntityparticular(this.selectedPartnerCognito, this.PartnerType).subscribe(
      (res: any) => {

        this.entityDetails = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  addBankFormGroup() {
    this.bankDetailsArray.push(this.createBankFormGroup());
  }


  createBankFormGroup(bankObj?: any): FormGroup {
    if (bankObj == undefined || bankObj == null) {
      return this.fb.group({
        id: [''],
        bank: ['', Validators.compose([Validators.required])],
        branch: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_holder_name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_number: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
        ifsc_code: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_type: ['', Validators.compose([Validators.required])],
        cancelled_cheque: ['', Validators.compose([Validators.required])],
        partner: this.partnerId,
      })
    }
    else {
      return this.fb.group({
        id: [bankObj.id],
        bank: [bankObj.bank, Validators.compose([Validators.required])],
        branch: [bankObj.branch, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_holder_name: [bankObj.account_holder_name, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_number: [bankObj.account_number, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
        ifsc_code: [bankObj.ifsc_code, Validators.compose([Validators.required, Validators.maxLength(50)])],
        account_type: [bankObj.account_type, Validators.compose([Validators.required])],
        cancelled_cheque: [bankObj.cancelled_cheque, Validators.compose([Validators.required])],
        partner: this.partnerId,
      })
    }
  }


  removeBankFormGroup(id: any) {
    var i = '' + id + '';
    this.bankDetailsArray.removeAt(id);
  }

  async uploadCheque(e: any, index: number, bank: any) {
    const file = e.target.files[0];
    if (file && (file.size <= 5242880) && (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' ||
      file.type == 'image/bmp')) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => { }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path = environment.bucket_parent_folder + "/" + this.congnitoId + '/' + 'partnerDealer-bank-details' + (index + 1) + '.' + fileExt;

      var uploaded_file = await this.s3.prepareFileForUploadWithoutPublicAccess(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        bank.controls.cancelled_cheque.setValue(uploaded_file?.Location);
        this.documentName[index] = { name: fileFormat, url: uploaded_file.Location };
      }
      else {
        bank.controls.cancelled_cheque.setValue('');
        this.notify.error("Upload Failed");
      }
    } else {
      this.notify.error("Invalid image");
      if (bank.controls.cancelled_cheque.value == '') {
        bank.controls.cancelled_cheque.setErrors({
          required: true
        })
      }
    }
  }

  deleteCheque(value,bank,i) {
    this.s3.deleteFile(value).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        bank.controls.cancelled_cheque.setValue('');
        this.documentName[i] = '';
      this.cancelledCheque.nativeElement.value = "";

      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }

  saveBankDetailsfn() {
    //this.spinner.show();
    let payload = this.bankingForm.value.bankDetailArray;
    this.accountService.postPartnerBankDetails(payload).pipe((finalize(() => { }))).subscribe(
      (data: any) => {
        this.notify.success("Bank Details Saved !!");
        this.saveOnSubmitBrands();

      },
      err => {
        console.log(err);
      }
    )
  }


  saveOnSubmitBrands() {
    ////this.spinner.show();
    let object = {};
    if (this.entityDetails && this.entityDetails.length > 0 && this.entityDetails[0].manufacture_brands) {
      this.entityDetails[0].manufacture_brands?.forEach(element => {
        this.manuFactBrands.push(element.id);
      });
    }
    object = {
      entity_type: this.entityDetails[0].entity_type,
      is_msme_number: this.entityDetails[0].is_msme_number,
      msme_number: this.entityDetails[0].msme_number,
      company_name: this.entityDetails[0].company_name,
      incorporation_date: this.entityDetails[0].incorporation_date,
      mobile: this.entityDetails[0].mobile,
      status: 1,
      created_by: this.entityDetails[0].created_by,
      partner_admin: this.selectedPartnerCognito,
      manufacture_brands: this.manuFactBrands,
    };

    this.agreementServiceService
      .putDealerEntityDetails(object, this.partnerId)
      .subscribe((res) => {
        this.router.navigate(['/admin-dashboard/partner-admin/partner-entity-list']);
        this.storage.clearLocalStorageData('partnerEntityCreate');
        this.storage.clearLocalStorageData('partnerEntityAdmin')
        this.storage.clearLocalStorageData('PARTNERADMINTYPE')
        this.storage.clearLocalStorageData('EDIT_PARTNER_ENTITY_ADMIN')
        this.storage.clearLocalStorageData('entityMappingId')
      });
  }


}
