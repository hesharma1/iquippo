import { Component, ElementRef, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { AccountService } from 'src/app/services/account';
import {
  AddressDetails,
  GetUserDto,
  UserProfileDTO,
  UserResponse,
} from 'src/app/shared/abstractions/user';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CountryResponse, ResponseData } from 'src/app/models/common/responsedata.model';
import {
  MatSlideToggle,
  MatSlideToggleChange,
} from '@angular/material/slide-toggle';
import {
  PanRequest,
  PanResponse,
} from 'src/app/models/common/panRequestData.model';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { PartnerDealerRegistrationEntityModel } from '../../../../../models/partner-dealer-management/partner-dealer-registration-data.model';
import { PartnerDealerRegistrationIdentityProofModel } from '../../../../../models/partner-dealer-management/partner-dealer-registration-data.model';
import { PartnerDealerRegistrationTaxModel } from '../../../../../models/partner-dealer-management/partner-dealer-registration-data.model'
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
@Component({
  selector: 'app-create-partner-entity',
  templateUrl: './create-partner-entity.component.html',
  styleUrls: ['./create-partner-entity.component.css'],
})
export class CreatePartnerEntityComponent implements OnInit,OnDestroy {
  partnerDealerRegistrationIdentityProofModel: any;
  partnerDealerRegistrationTaxModel: any;
  base64textString: any;
  corporateProfileForm: FormGroup;
  isInvalidfile: boolean = false;
  pinCodeResponse?: PincodeResponse;
  userprofileData: UserProfileDTO;
  getUser: GetUserDto;
  deletedAddress: string[] = [];
  userDataobj: any;
  pinCodeErrorFlag = false;
  isPanValid = false;
  docImge: any;
  VerifiedDataobj: any;
  countryList?: any;
  selectedCity?: CountryResponse;
  selectedData = [];
  manuFactBrands:any = [];
  isPanExist: boolean = false;
  isPanCheck: boolean = false;
  isShowPanDoc: boolean = false;
  isShowDocProof: boolean = false;
  isKarzaInvoked: boolean = false;
  paramaterarray: any;
  isCallCenter: any;
  AddressFormArray: any;
  initialPan: string = '';
  entityResult: any;
  PartnerType: any;
  minDate: any;
  editPartnerEntity:any;
  partnerDealerRegistrationEntityModel: any;
  proofList = [
    'Voter Id',
    'Aadhar',
    'Driving License',
    'Passport',
    'Ration Card',
    'Bank Statement',
    'Utility Bills',
    'Trade License',
    'GSTN & Autority Certificate',
    'Registration Certificate',
  ];
  PanResponse?: PanResponse;
  initialFirstName: any;
  initialLastName: any;
  isEmailReq?: boolean = false;
  isEditable: boolean = false;
  isPanEditable: boolean = false;
  isEdit: boolean = false;
  cognitoId: any;
  uploadingImage: boolean = false;
  isEntityEdittable: boolean = true;
  hideAppliedForPan: boolean = false;
  isSuperAdmin: boolean = true;
  isAdminAvailable = false;
  role?: string;
  TaxFormArray: any;
  BrandLocationsArray: any;
  isShowNext = false;
  isEditEntity = false;
  stateList: any = [];
  isProofType: any;
  documentProofs: any[] = [];
  partnerEntityList: any[] = [];
  pageNumber: any = 1;
  pageSize: any = 10;
  userId: any;
  partnerEntityTaxList: any = [];
  partnerIdentityProofList: any;
  stateListData: any;
  partnerContactList: any;
  selectedPartnerCognito: any;
  selectedPartnerId: any;
  entityType: any;
  relationEntityType: any;
  constructor(
    private storage: StorageDataService,
    public sharedService: SharedService,
    private router: Router,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private accountService: AccountService,
    private httpClient: HttpClient,
    private notify: NotificationService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private appRouteEnum: AppRouteEnum,
    private s3: S3UploadDownloadService,
    private apiRouteService: ApiRouteService,
    private agreementServiceService: AgreementServiceService,
    private partnerDealerRegistrationService: PartnerDealerRegistrationService
  ) {
    this.corporateProfileForm = this.fb.group({
      Representer: ['2'],
      //Gender:['M'],
      TaxArray: new FormArray([]),
      BrandLocationArray: new FormArray([]),
      RelationshipEntity: [1],
      EntityType: [1, Validators.required],
      partnerContact: ['', Validators.required],
      partnerType: ['', Validators.required],
      // PanCardNo: ['',Validators.required],
      enableMSME: [false],
      MSMENumber: [''],
      documentId: new FormControl(''),
      CompanyName: ['', Validators.required],
      // customFile: ['',Validators.required],
      DateoOfIncorporation: ['', Validators.required],
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl(''),
      Mobile: ['', Validators.required],
      countryCode: ['', Validators.required],
      applied_for_pan: new FormControl(false, [Validators.required]),
      pan: new FormGroup({
        documentId: new FormControl(''),
        id: new FormControl(''),
        docType: new FormControl('pan'),
        docNumber: new FormControl('', Validators.required),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl('', Validators.required),
          frontView: new FormControl(''),
          back: new FormControl(''),
        }),
      }),

      voter: new FormGroup({
        documentId: new FormControl(''),
        id: new FormControl(''),
        docType: new FormControl('voter'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl(''),
        }),
      }),

      uan: new FormGroup({
        documentId: new FormControl(''),
        id: new FormControl(''),
        docType: new FormControl('uan'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl(''),
        }),
      }),
      tradelicense: new FormGroup({
        documentId: new FormControl(''),
        id: new FormControl(''),
        docType: new FormControl('tradelicense'),
        docNumber: new FormControl(''),
        dob: new FormControl(''),
        name: new FormControl(''),
        docImages: new FormGroup({
          front: new FormControl(''),
          frontView: new FormControl(''),
          backView: new FormControl(''),
          back: new FormControl(''),
        }),
      }),
    });
    this.userprofileData = {};
    this.userprofileData.use_pan_name = 'No';
    this.getUser = {};
  }

  test() {
    //.log(this.corporateProfileForm.value);
  }

  getPartnerContactList() {
    this.partnerDealerRegistrationService.getPartnerContacts().subscribe(
      (res: any) => {
         
        this.partnerContactList = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  verifyPanData() {
    ////this.spinner.show();
    var panRequest: any = {};
    if (
      this.corporateProfileForm.get('applied_for_pan')?.value == false &&
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.valid &&
      this.corporateProfileForm.get('pan')?.get('docNumber')?.value != ''
    ) {
      if (
        this.initialPan.toLowerCase() !=
        this.corporateProfileForm
          .get('pan')
          ?.get('docNumber')
          ?.value.toLowerCase()
      ) {
        //this.spinner.show();
        if (
          this.isCallCenter == '1' ||
          this.role == this.apiRouteService.roles.callcenter
        ) {
          panRequest.cognitoId = this.storage.getStorageData(
            'cognitoUserId',
            true
          );
        } else {
          panRequest.cognitoId = this.storage.getStorageData('cognitoId', false);
        }
        panRequest.panCardNo = this.corporateProfileForm
          .get('pan')
          ?.get('docNumber')?.value;
        panRequest.documentId = this.corporateProfileForm
          .get('pan')
          ?.get('documentId')?.value;
        panRequest.represent = '2';
        panRequest.source = 'Partner';
        panRequest.name = this.corporateProfileForm.get('CompanyName')?.value == null ? '' : this.corporateProfileForm.get('CompanyName')?.value;
          this.partnerDealerRegistrationService.verifyPanCardDetails(panRequest).subscribe(
            (resp) => {
              this.PanResponse = resp as PanResponse;
              //console.log(this.PanResponse.body);
              if (this.PanResponse.body?.statusCode == '200') {
                 this.isKarzaInvoked = true;
                const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                  width: '650px',
                  data: {
                    message: 'Do you want to use your PAN Name?',
                  },
                });
                dialogRef.afterClosed().subscribe((result) => {
                  if (result) {
                    //console.log(result);
                    this.replaceVerifyPanName();
                    let msg: any = this.PanResponse?.body?.message;
                    this.notify.success(msg);
                    if (
                      this.corporateProfileForm?.get('EntityType')?.value ==
                      '1'
                    ) {
                      this.corporateProfileForm.get('FirstName')?.disable();
                      this.corporateProfileForm.get('LastName')?.disable();
                      this.corporateProfileForm.get('CompanyName')?.enable();
  
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        this.corporateProfileForm
                          ?.get('AddressArray')
                          ?.get(b)
                          ?.get('GSTNo')
                          ?.setErrors(null);
                      }
                    } else {
                      this.corporateProfileForm.get('CompanyName')?.disable();
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        if (
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')?.value == ''
                        )
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')
                            ?.setErrors({ required: true });
                      }
                    }
                    this.userprofileData.use_pan_name = 'Yes';
                  } else {
                    if (
                      this.corporateProfileForm?.get('EntityType')?.value ==
                      '1'
                    ) {
                      this.corporateProfileForm
                        .get('FirstName')
                        ?.setValue(this.initialFirstName);
                      this.corporateProfileForm
                        .get('LastName')
                        ?.setValue(this.initialLastName);
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        this.corporateProfileForm
                          ?.get('AddressArray')
                          ?.get(b)
                          ?.get('GSTNo')
                          ?.setErrors(null);
                      }
                    } else {
                      this.corporateProfileForm.get('CompanyName')?.enable();
                      //this.corporateProfileForm.get('CompanyName')?.setValue('');
  
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        if (
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')?.value == ''
                        )
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')
                            ?.setErrors({ required: true });
                      }
                    }
                    this.userprofileData.use_pan_name = 'No';
                  }
                });
              } else if (this.PanResponse.body?.statusCode == '508') {
                 
                this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
                // NamemisMatchPopup
                const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
                  width: '650px',
                  data: {
                    message: 'Do you want to use your PAN Name?',
                  },
                });
                dialogRef.afterClosed().subscribe((result) => {
                  if (result) {
                    this.replaceVerifyPanName();
                    if (
                      this.corporateProfileForm?.get('EntityType')?.value ==
                      '1'
                    ) {
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        this.corporateProfileForm
                          ?.get('AddressArray')
                          ?.get(b)
                          ?.get('GSTNo')
                          ?.setErrors(null);
                      }
                    } else {
                      this.corporateProfileForm.get('CompanyName')?.disable();
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        if (
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')?.value == ''
                        )
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')
                            ?.setErrors({ required: true });
                      }
                    }
                    this.userprofileData.use_pan_name = 'Yes';
                  } else {
                    if (
                      this.corporateProfileForm?.get('EntityType')?.value ==
                      '1'
                    ) {
                      this.corporateProfileForm
                        .get('FirstName')
                        ?.setValue(this.initialFirstName);
                      this.corporateProfileForm
                        .get('LastName')
                        ?.setValue(this.initialLastName);
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        this.corporateProfileForm
                          ?.get('AddressArray')
                          ?.get(b)
                          ?.get('GSTNo')
                          ?.setErrors(null);
                      }
                    } else {
                      this.corporateProfileForm.get('CompanyName')?.enable();
                      //this.corporateProfileForm.get('CompanyName')?.setValue('');
  
                      for (
                        let index = 0;
                        index <
                        this.corporateProfileForm?.get('AddressArray')?.value
                          .length;
                        index++
                      ) {
                        var b = '' + index + '';
                        if (
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')?.value == ''
                        )
                          this.corporateProfileForm
                            ?.get('AddressArray')
                            ?.get(b)
                            ?.get('GSTNo')
                            ?.setErrors({ required: true });
                      }
                    }
                    this.userprofileData.use_pan_name = 'No';
                  }
                });
              } else if (this.PanResponse.body?.statusCode == '513') {
                 
                 this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
                let msg: any = this.PanResponse.body.message;
                this.notify.warn(msg);
              } else if (
                this.PanResponse.body?.statusCode == '506' ||
                this.PanResponse.body?.statusCode == '512'
              ) {
                this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
                let msg: any = this.PanResponse.body.message;
                this.notify.warn(msg);
              } else {
                let msg: any = this.PanResponse?.body?.message;
                this.notify.warn(msg);
              }
            },
            (error) => {
              console.log(error);
              this.isKarzaInvoked = false;
              this.notify.warn('There is temporary issue with our document validation services.please provide further details and our team will get in touch with you');
            }
          );
        }
        
      
    }
  }

  replaceVerifyPanName() {
    if (
      this.corporateProfileForm?.get('EntityType')?.value == '1'
    ) {
      let stringToSplit = this.PanResponse?.body?.firstName;
      let x:any;
      x = stringToSplit?.split(" ");
      console.log(x[0]);
      if(x?.length == 2) {
        this.corporateProfileForm
        .get('FirstName')
        ?.setValue(x[0]);
      this.corporateProfileForm
        .get('LastName')
        ?.setValue(x[x.length-1]);
      } else {
        this.corporateProfileForm
        .get('FirstName')
        ?.setValue(x[0] + x[1]);
      this.corporateProfileForm
        .get('LastName')
        ?.setValue(x[x.length-1]);
      }
      
        // this.corporateProfileForm
        // .get('CompanyName')
        // ?.setValue(this.PanResponse?.body?.firstName);
    } else {
      this.corporateProfileForm
        .get('CompanyName')
        ?.setValue(this.PanResponse?.body?.firstName);
    }
   
  }

  


  ngOnInit(): void {
    console.log(this.corporateProfileForm?.get('enableMSME')?.value)
    this.entityType = 1;
    this.relationEntityType = 1;
    this.cognitoId = this.storage.getStorageData('cognitoUserId', false);
    this.role = this.storage.getStorageData('role', false);
    this.userprofileData.corporateDetails = [];
    this.userprofileData.address = [];
    this.selectedData = [];
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['aoc']) {
        if (params['aoc'] == 'qtt') {
          this.isEmailReq = true;
          this.hideAppliedForPan = true;
          this.checkEmailField();
        } else {
          this.isEmailReq = false;
        }
      }
      if (params['isEditEntity']) {
        if (params['isEditEntity'] == 'true') {
          this.isEditEntity = true;
          console.log("isEditEntity", this.isEditEntity);
        } else {
          this.isEditEntity = false;
          console.log("isEditEntity", this.isEditEntity);
        }
      }
      if (params['isEdit']) {
        if (params['isEdit'] == '1') {
          this.isEdit = true;
        } else {
          this.isEdit = false;
        }
      }
    });
    this.bindFlags();
    this.isCallCenter = this.storage.getStorageData('isCallCenter', true);
    if (
      this.isCallCenter == '1' ||
      this.role == this.apiRouteService.roles.callcenter
    ) {
      this.userprofileData.cognitoId = this.storage.getStorageData(
        'cognitoUserId',
        false
      );
      this.userprofileData.callcenterCognito = this.storage.getStorageData(
        'cognitoId',
        false
      );
    } else {
      this.userprofileData.cognitoId = this.storage.getStorageData(
        'cognitoId',
        false
      );
    }
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    const currentYear = new Date();
    let disableUntil = { year: currentYear.getFullYear(), month: currentYear.getMonth(), day: currentYear.getDate() };
    this.minDate = new Date(disableUntil.year, disableUntil.month, disableUntil.day);
    this.AddressFormArray = <FormArray>(
      this.corporateProfileForm.controls['AddressArray']
    );
    this.TaxFormArray = <FormArray>(
      this.corporateProfileForm.controls['TaxArray']
    );
    this.BrandLocationsArray = <FormArray>(
      this.corporateProfileForm.controls['BrandLocationArray']
    );
    if (this.storage.getStorageData('PARTNERADMINTYPE', true) !== null && 
      this.storage.getStorageData('PARTNERADMINTYPE', true) !== undefined
    ) {
      this.PartnerType = this.storage.getStorageData('PARTNERADMINTYPE', true);
      this.corporateProfileForm?.get('partnerType')?.setValue(this.PartnerType);

    }
    this.checkCognitoAndType();
    //this.addNewTax();
    this.getStateData();
    this.getPartnerContactList();
    this.getEntityDetails();
    if (this.storage.getStorageData('partnerEntityCreate', false) != null) {
    }
    this.partnerDealerRegistrationIdentityProofModel = new PartnerDealerRegistrationIdentityProofModel();
    if(!this.corporateProfileForm?.get('enableMSME')?.value) {
      this.corporateProfileForm?.get('MSMENumber')?.disable();
    }
  }

  checkCognitoAndType() {
    if (this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true) !== null &&
      this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true) !== undefined) {
        this.editPartnerEntity = this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true);
      let cognitoId = this.storage.getStorageData('EDIT_PARTNER_ENTITY_ADMIN', true);
      console.log("cognitoId", this.cognitoId);
      if (cognitoId?.partner_admin?.cognito_id != undefined || cognitoId?.partner_admin?.cognito_id != null) {
        this.selectedPartnerCognito = cognitoId?.partner_admin?.cognito_id;
        this.PartnerType = cognitoId?.partnership_type;
        this.isAdminAvailable = true;
        this.corporateProfileForm?.get('partnerType')?.setValue(this.PartnerType);
        this.corporateProfileForm?.get('partnerContact')?.setValue(this.selectedPartnerCognito);
        this.corporateProfileForm?.get('partnerContact')?.clearValidators();
        this.corporateProfileForm?.get('partnerContact')?.updateValueAndValidity();

      }
    }

    if (this.storage.getStorageData('partnerEntityAdmin', true) !== null && this.storage.getStorageData('partnerEntityAdmin', true)
     !== undefined) {
      let selectedAdmin = this.storage.getStorageData('partnerEntityAdmin', true)
      let selectedType = this.storage.getStorageData('PARTNERADMINTYPE', true)
      if (selectedAdmin != undefined && selectedAdmin != null && selectedType != undefined && selectedType != null && !this.isEditEntity) {
        this.selectedPartnerCognito = selectedAdmin;
        this.isAdminAvailable = true;
        this.corporateProfileForm?.get('partnerContact')?.clearValidators();
        this.corporateProfileForm?.get('partnerContact')?.updateValueAndValidity();
        this.PartnerType = selectedType;
        this.corporateProfileForm?.get('partnerType')?.setValue(this.PartnerType);
        this.corporateProfileForm?.get('partnerContact')?.setValue(this.selectedPartnerCognito);
      } else {
        this.corporateProfileForm?.get('partnerType')?.setValue('');
        this.corporateProfileForm?.get('partnerContact')?.setValue('');
      }
    }
  }
  msmeCheckbox(e: MatSlideToggleChange) {

  }

  /**get entity details */
  getEntityDetails() {
    this.partnerEntityList = [];
    if (this.selectedPartnerCognito != undefined && this.selectedPartnerCognito != null && this.PartnerType != undefined && this.PartnerType != null) {
      //this.spinner.show();
      this.agreementServiceService.getPartnerEntityparticular(this.selectedPartnerCognito, this.PartnerType).subscribe(
        (res: any) => {
           
          this.partnerEntityList = res.results;
          console.log("this.partnerEntityList", this.partnerEntityList[0]?.id);
          if (this.partnerEntityList.length > 0) {
            this.patchEntityValues(this.partnerEntityList[0])
            this.getEntityIdentityProofDetails(this.partnerEntityList[0]?.id);
            this.getEntityTaxDetails();
          } else {
            this.addNewTax();
          }
        },
        (err) => {
          console.error(err);
        }
      );
    }
    else {
      this.partnerEntityList = [];
      this.addNewTax();

    }
  }

  patchEntityValues(element) {
    console.log("partnerEntityList", this.partnerEntityList);
    // if (this.isEditEntity) {
       this.entityType = element.entity_type;
       this.relationEntityType = element.relation_with_entity;
      //  this.corporateProfileForm?.get('EntityType')?.setValue(element.entity_type);
      // this.corporateProfileForm?.get('RelationshipEntity')?.setValue(element.relation_with_entity);
      this.corporateProfileForm?.get('enableMSME')?.setValue(element.is_msme_number)
      this.corporateProfileForm?.get('MSMENumber')?.setValue(element.msme_number)
      this.corporateProfileForm?.get('CompanyName')?.setValue(element.company_name)
      this.corporateProfileForm?.get('DateoOfIncorporation')?.setValue(element.incorporation_date)
      this.corporateProfileForm?.get('Mobile')?.setValue(element.mobile)
      this.corporateProfileForm?.get('partnerType')?.setValue(element.partnership_type)
      this.corporateProfileForm?.get('FirstName')?.setValue(element.first_name)
      this.corporateProfileForm?.get('LastName')?.setValue(element.last_name)
      if(element.entity_type != '1') {
        this.corporateProfileForm?.get('FirstName')?.clearValidators();
        this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();

      }
      this.corporateProfileForm?.get('partnerType')?.disable();
      this.corporateProfileForm?.get('EntityType')?.disable();
      this.corporateProfileForm?.get('RelationshipEntity')?.disable();
  }

  /**get entity Tax details */
  getEntityTaxDetails() {
    //this.spinner.show();
    // if (this.selectedPartnerCognito != undefined && this.selectedPartnerCognito != null && this.PartnerType != undefined && 
    // this.PartnerType != null) {
    this.agreementServiceService.getPartnerEntityTaxparticular(this.selectedPartnerCognito, this.PartnerType).subscribe(
      (res: any) => {
         
        this.partnerEntityTaxList = res.results;
        if (this.partnerEntityTaxList && this.partnerEntityTaxList.length > 0) {
          this.partnerEntityTaxList.forEach(cust => {
            this.TaxFormArray.push(this.createTaxFormGroup(cust));
            //console.log(this.customersControls);
          });
        }
        else {
          this.addNewTax()
        }
      },
      (err) => {
        console.error(err);
      }
    );
    //    }
  }

  initializRecords(data?: any) {
    const formArray = new FormArray([]);
    for (let i = 0; i < this.partnerEntityTaxList.length; i++) {
      formArray.push(this.fb.group({
        registrationNo: this.partnerEntityTaxList[i].registration_number,
        State: this.partnerEntityTaxList[i].state.id,
        id: this.partnerEntityTaxList[i].id
      }));
    }
    this.corporateProfileForm.setControl('TaxArray', formArray);
  }

  /**get entity Tax details */
  getEntityIdentityProofDetails(id) {
    //this.spinner.show();
    this.agreementServiceService.getPartnerEntityIdentityparticular(id).subscribe(
      (res: any) => {
         
        this.partnerIdentityProofList = res.results;
        this.initializProofRecords()
      },
      (err) => {
        console.error(err);
      }
    );
  }

  initializProofRecords() {
    this.partnerIdentityProofList.forEach(element => {
      if (element.proof_type == 1) {
        this.corporateProfileForm?.get('pan')?.get('docNumber')?.setValue(element.id_number)
        //this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
        this.corporateProfileForm?.get('pan')?.get('id')?.setValue(element.id)
        this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.setValue(element.document)
      }
      if (element.proof_type == 6) {
        this.corporateProfileForm?.get('voter')?.get('docNumber')?.setValue(element.id_number)
        this.corporateProfileForm?.get('voter')?.get('id')?.setValue(element.id)
        this.corporateProfileForm?.get('voter')?.get('docImages')?.get('front')?.setValue(element.document)
        this.corporateProfileForm?.get('voter')?.get('docImages')?.get('back')?.setValue(element.document_back)
      }
      if (element.proof_type == 7) {
        this.corporateProfileForm?.get('uan')?.get('docNumber')?.setValue(element.id_number)
        this.corporateProfileForm?.get('uan')?.get('id')?.setValue(element.id)
        this.corporateProfileForm?.get('uan')?.get('docImages')?.get('front')?.setValue(element.document)
        this.corporateProfileForm?.get('uan')?.get('docImages')?.get('back')?.setValue(element.document_back)
      }
      if (element.proof_type == 8) {
        this.corporateProfileForm?.get('tradelicense')?.get('docNumber')?.setValue(element.id_number)
        this.corporateProfileForm?.get('tradelicense')?.get('id')?.setValue(element.id)
        this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('front')?.setValue(element.document)
        this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('back')?.setValue(element.document_back)
      }
    });
  }

  getpinCodefn(i: string) {
    var b = '' + i + '';
    var x = this.corporateProfileForm
      ?.get('AddressArray')
      ?.get(b)
      ?.get('PinCode')?.value;
    //console.log(x);
    if (x > 99999 && x < 1000000) {
      this.accountService
        .getPinCode(
          this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('PinCode')
            ?.value
        )
        .subscribe(
          (resp) => {
            //console.log(resp);
            this.pinCodeResponse = resp as PincodeResponse;
            //console.log("my", this.pinCodeResponse);

            if (this.pinCodeResponse.code === 258) {
              this.corporateProfileForm
                ?.get('AddressArray')
                ?.get(b)
                ?.get('pinCodeErrorFlag')
                ?.setValue(false);

              // this.pinCodeErrorFlag = false;
              this.corporateProfileForm
                ?.get('AddressArray')
                ?.get(b)
                ?.get('State')
                ?.setValue(this.pinCodeResponse.pincode?.state);
              this.corporateProfileForm
                ?.get('AddressArray')
                ?.get(b)
                ?.get('City')
                ?.setValue(this.pinCodeResponse.pincode?.city);
            } else if (this.pinCodeResponse.code === 259) {
              // console.log(this.pinCodeResponse.code);
              this.corporateProfileForm
                ?.get('AddressArray')
                ?.get(b)
                ?.get('pinCodeErrorFlag')
                ?.setValue(true);
              //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue('');
              //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue('');
            } else {
              console.log('errorrr');
              this.corporateProfileForm
                ?.get('AddressArray')
                ?.get(b)
                ?.get('pinCodeErrorFlag')
                ?.setValue(true);
              //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('State')?.setValue('');
              //this.corporateProfileForm?.get('AddressArray')?.get(b)?.get('City')?.setValue('');
            }
          },
          (error) => {
            console.log(error);
            this.corporateProfileForm
              ?.get('AddressArray')
              ?.get(b)
              ?.get('pinCodeErrorFlag')
              ?.setValue(true);
            this.corporateProfileForm
              ?.get('AddressArray')
              ?.get(b)
              ?.get('State')
              ?.setValue('');
            this.corporateProfileForm
              ?.get('AddressArray')
              ?.get(b)
              ?.get('City')
              ?.setValue('');
          }
        );
    }
  }

  navselfDetails() {
    if (this.isEditable) return;
    if (this.isEmailReq)
      this.router.navigate([`./` + this.appRouteEnum.Profile], {
        queryParams: { aoc: 'qtt', isEdit: '1' },
      });
    else
      this.router.navigate([`./` + this.appRouteEnum.Profile], {
        queryParams: { isEdit: '1' },
      });
  }

  createAddressFormGroup(cust?: AddressDetails) {
    //console.log("cust",cust);
    if (cust != undefined) {
      if (
        this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
      ) {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(
            cust?.addressLine1,
            Validators.required
          ),
          StreetAddress2: new FormControl(
            cust?.addressLine2,
            Validators.required
          ),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo, Validators.required),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      } else {
        return this.fb.group({
          address: new FormControl(cust?.address, Validators.required),
          addressId: new FormControl(cust?.addressId),
          StreetAddress1: new FormControl(
            cust?.addressLine1,
            Validators.required
          ),
          StreetAddress2: new FormControl(
            cust?.addressLine2,
            Validators.required
          ),
          City: new FormControl(cust?.city, Validators.required),
          createdAt: new FormControl(cust?.createdAt),
          GSTNo: new FormControl(cust?.gstNo),
          id: new FormControl(cust?.id),
          PinCode: new FormControl(cust?.pincode, [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl(cust?.state, Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
    } else {
      if (
        this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
      ) {
        return new FormGroup({
          StreetAddress1: new FormControl('', Validators.required),
          addressId: new FormControl(''),
          StreetAddress2: new FormControl('', Validators.required),
          PinCode: new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl('', Validators.required),
          City: new FormControl('', Validators.required),
          GSTNo: new FormControl('', Validators.required),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
      {
        return new FormGroup({
          StreetAddress1: new FormControl('', Validators.required),
          addressId: new FormControl(''),
          StreetAddress2: new FormControl('', Validators.required),
          PinCode: new FormControl('', [
            Validators.required,
            Validators.pattern(/[1-9]{1}[0-9]{5}$/),
            Validators.minLength(6),
            Validators.maxLength(6),
          ]),
          State: new FormControl('', Validators.required),
          City: new FormControl('', Validators.required),
          GSTNo: new FormControl(''),
          pinCodeErrorFlag: new FormControl(false),
        });
      }
    }
  }

  addNewTax() {
    this.TaxFormArray.push(this.createTaxFormGroup(''));
  }

  createTaxFormGroup(taxObject?: any): FormGroup {
    if (taxObject == undefined || taxObject == null || taxObject == '') {
      return this.fb.group({
        registrationNo: new FormControl('', Validators.required),
        State: new FormControl('', Validators.required),
        id: new FormControl(''),
      });
    } else {
      return this.fb.group({
        registrationNo: [taxObject.registration_number],
        State: [taxObject.state.id, Validators.compose([Validators.required])],
        id: [taxObject.id, Validators.compose([Validators.required, Validators.maxLength(50)])],
      })
    }
  }

  onNext() {
    this.isShowNext = true;
  }

  removeTaxFormGroup(id: any) {
    const arrayControl = <FormArray>this.corporateProfileForm.controls['TaxArray'];
    arrayControl.removeAt(id);
  }

  onEntityTypeChange(value: any, data: any) {
    if (this.isEditable) return;
    this.corporateProfileForm?.get('EntityType')?.setValue(value);
    if (value == 'Proprietorship') {
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('1');
      this.corporateProfileForm?.get('FirstName')?.setValidators(Validators.required);
      this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();
      this.corporateProfileForm?.get('LastName')?.setValidators(Validators.required);
      this.corporateProfileForm?.get('LastName')?.updateValueAndValidity();
    }
      
    if (value == 'Partnership' || value == 'LLP') {
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('2');
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('1');
      this.corporateProfileForm?.get('FirstName')?.clearValidators();
      this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();
      this.corporateProfileForm?.get('LastName')?.clearValidators();
      this.corporateProfileForm?.get('LastName')?.updateValueAndValidity();
    }
     
    if (value == 'HUF') {
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('3');
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('1');
      this.corporateProfileForm?.get('FirstName')?.clearValidators();
      this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();
      this.corporateProfileForm?.get('LastName')?.clearValidators();
      this.corporateProfileForm?.get('LastName')?.updateValueAndValidity();
    }
      
    if (value == 'Private Ltd.Co.' || value == 'Public Ltd.Co.') {
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('4');
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('1');
      this.corporateProfileForm?.get('FirstName')?.clearValidators();
      this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();
      this.corporateProfileForm?.get('LastName')?.clearValidators();
      this.corporateProfileForm?.get('LastName')?.updateValueAndValidity();
    }
      
    if (value == 'Trust' || value == 'Society') {
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('5');
      this.corporateProfileForm?.get('RelationshipEntity')?.setValue('1');
      this.corporateProfileForm?.get('FirstName')?.clearValidators();
      this.corporateProfileForm?.get('FirstName')?.updateValueAndValidity();
      this.corporateProfileForm?.get('LastName')?.clearValidators();
      this.corporateProfileForm?.get('LastName')?.updateValueAndValidity();
    }
    this.checkPan();
  }
  onRelationshipEntityChange(value: any, data: any) {
    if (this.isEditable) return;
    this.corporateProfileForm?.get('RelationshipEntity')?.setValue(data);
  }

  /*checkPan() {
    // this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors( {partnership: null,
    //   wrong: null,llp: null,privateError: null,publicError: null,trust: null,
    //   society: null,huf: null});
    this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    var pan = this.corporateProfileForm.get('pan')?.get('docNumber')?.value?.toLowerCase();
    var entityValue = this.corporateProfileForm.get('EntityType')?.value;
    // if(pan?.length==10 && pan.charAt(3)=='p'){
    //   this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ wrong: true });
    // }
    if (pan == "") {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ required: true });
     // this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
    }
    else if (pan?.length != 10) {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 1 && pan.charAt(3) != 'p') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ proprietorship: true });
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
    }
    else if (pan?.length == 10 && entityValue == 2 && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ partnership: true });
     // this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 4 && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ llp: true });
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 5 && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ privateError: true });
    //  this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 6 && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ publicError: true });
    ///  this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 7 && pan.charAt(3) != 't') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ trust: true });
    //  this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 8 && pan.charAt(3) != 'a') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ society: true });
     // this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else if (pan?.length == 10 && entityValue == 3 && pan.charAt(3) != 'h') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ huf: true });
    //  this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');

    }
    else {
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ partnership: false });
      // this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors( {partnership: null,
      //   wrong: null,llp: null,privateError: null,publicError: null,trust: null,
      //   society: null,huf: null});
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors(null);
      this.verifyPanData();
    }
  }*/

  checkPan() {
    // this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors( {partnership: null,
    //   wrong: null,llp: null,privateError: null,publicError: null,trust: null,
    //   society: null,huf: null});
    this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    var pan = this.corporateProfileForm.get('pan')?.get('docNumber')?.value?.toLowerCase();
    var entityValue = this.corporateProfileForm.get('EntityType')?.value;
    console.log("entityValue", entityValue);
    // if(pan?.length==10 && pan.charAt(3)=='p'){
    //   this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ wrong: true });
    // }
    if (pan == "") {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ required: true });
      //this.corporateProfileForm.get('pan')?.get('docNumber')?.setValue('');
    }
    else if (pan?.length != 10) {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ minlength: true });
    }
    else if (pan?.length == 10 && (entityValue == 1 || entityValue == 'Proprietorship') && pan.charAt(3) != 'p') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ proprietorship: true });
    }
    else if (pan?.length == 10 && (entityValue == 2 || entityValue == 'Partnership') && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ partnership: true });
    }
    else if (pan?.length == 10 && (entityValue == 4 || entityValue == 'LLP') && pan.charAt(3) != 'f') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ llp: true });
    }
    else if (pan?.length == 10 && (entityValue == 5 || entityValue == 'Private Ltd.Co.') && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ privateError: true });
    }
    else if (pan?.length == 10 && (entityValue == 6 || entityValue == 'Public Ltd.Co.') && pan.charAt(3) != 'c') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ publicError: true });
    }
    else if (pan?.length == 10 && (entityValue == 7 || entityValue == 'Trust') && pan.charAt(3) != 't') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ trust: true });
    }
    else if (pan?.length == 10 && (entityValue == 8 || entityValue == 'Society') && pan.charAt(3) != 'a') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ society: true });
    }
    else if (pan?.length == 10 && (entityValue == 3 || entityValue == 'HUF') && pan.charAt(3) != 'h') {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors({ huf: true });
    }
    else {
      this.corporateProfileForm.get('pan')?.get('docNumber')?.setErrors(null);
      this.verifyPanData();
    }
  }

  enableMSME() {
    if (this.isEditable) return;

    if (this.corporateProfileForm?.get('enableMSME')?.value) {
      this.corporateProfileForm?.get('MSMENumber')?.setErrors(null);
      this.corporateProfileForm?.get('MSMENumber')?.setValue('');
      this.corporateProfileForm?.get('MSMENumber')?.disable();
      this.corporateProfileForm?.get('MSMENumber')?.clearValidators();
      this.corporateProfileForm?.get('MSMENumber')?.updateValueAndValidity();
    } else {
      this.corporateProfileForm
        ?.get('MSMENumber')
        ?.setErrors({ required: true });
      this.corporateProfileForm?.get('MSMENumber')?.enable();
      this.corporateProfileForm?.get('MSMENumber')?.setValidators(Validators.required);
      this.corporateProfileForm?.get('MSMENumber')?.updateValueAndValidity();
    }
  }

  panFrontImageError = false;
  checkPanImageOnSubmit() {
    if (
      this.corporateProfileForm.get('pan')?.get('docNumber')?.value != '' &&
      this.corporateProfileForm.get('pan')?.get('docImages')?.get('front')
        ?.value == ''
    ) {
      this.panFrontImageError = true;
      this.notify.error('Please upload pan image.');
      return false;
    } else {
      this.panFrontImageError = false;
      return true;
    }
  }

  confirmSave() {
    if (this.partnerEntityList?.length == 0) {
      this.saveOnSubmit();
    } else {
      this.updateOnSubmit();
    }
  }

  saveOnSubmit() {
    //this.spinner.show();
    this.partnerDealerRegistrationEntityModel = new PartnerDealerRegistrationEntityModel();
    this.partnerDealerRegistrationEntityModel.entity_type = this.corporateProfileForm?.get('EntityType')?.value;
    this.partnerDealerRegistrationEntityModel.is_msme_number = this.corporateProfileForm?.get('enableMSME')?.value;
    this.partnerDealerRegistrationEntityModel.msme_number = this.corporateProfileForm?.get('MSMENumber')?.value;
    this.partnerDealerRegistrationEntityModel.relation_with_entity = this.corporateProfileForm?.get('RelationshipEntity')?.value;
    this.partnerDealerRegistrationEntityModel.company_name = this.corporateProfileForm?.get('CompanyName')?.value;
    this.partnerDealerRegistrationEntityModel.incorporation_date = this.datePipe.transform(this.corporateProfileForm?.get('DateoOfIncorporation')?.value, 'yyyy-MM-dd');
    this.partnerDealerRegistrationEntityModel.mobile = this.corporateProfileForm?.get('Mobile')?.value;
    this.partnerDealerRegistrationEntityModel.partner_admin = this.corporateProfileForm?.get('partnerContact')?.value;
    this.partnerDealerRegistrationEntityModel.created_by = this.corporateProfileForm?.get('partnerContact')?.value;
    this.partnerDealerRegistrationEntityModel.first_name = this.corporateProfileForm?.get('FirstName')?.value;
    this.partnerDealerRegistrationEntityModel.last_name = this.corporateProfileForm?.get('LastName')?.value;
    this.partnerDealerRegistrationEntityModel.gst_number = 0;
    this.partnerDealerRegistrationEntityModel.partnership_type = this.PartnerType;
    this.partnerDealerRegistrationEntityModel.status = 0;
    this.partnerDealerRegistrationEntityModel.manufacture_brands = [];
    this.agreementServiceService
      .postDealerEntityDetails(this.partnerDealerRegistrationEntityModel)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
        this.entityResult = res;
        this.storage.setStorageData('partnerEntityCreate', JSON.stringify(this.entityResult), false);
        this.storage.setStorageData('PARTNERADMINTYPE', this.PartnerType, true);
        this.storage.setStorageData('partnerEntityAdmin', this.selectedPartnerCognito, true)
        this.saveEntityMapping(this.entityResult.id);
        this.loadTaxDetails();
      });
  }


  updateOnSubmit() {
    //this.spinner.show();
    this.partnerDealerRegistrationEntityModel = new PartnerDealerRegistrationEntityModel();
    this.partnerDealerRegistrationEntityModel.entity_type = this.corporateProfileForm?.get('EntityType')?.value;
    this.partnerDealerRegistrationEntityModel.is_msme_number = this.corporateProfileForm?.get('enableMSME')?.value;
    this.partnerDealerRegistrationEntityModel.msme_number = this.corporateProfileForm?.get('MSMENumber')?.value;
    this.partnerDealerRegistrationEntityModel.company_name = this.corporateProfileForm?.get('CompanyName')?.value;
    this.partnerDealerRegistrationEntityModel.incorporation_date = this.datePipe.transform(this.corporateProfileForm?.get('DateoOfIncorporation')?.value, 'yyyy-MM-dd');
    this.partnerDealerRegistrationEntityModel.mobile = this.corporateProfileForm?.get('Mobile')?.value;
    this.partnerDealerRegistrationEntityModel.first_name = this.corporateProfileForm?.get('FirstName')?.value;
    this.partnerDealerRegistrationEntityModel.last_name = this.corporateProfileForm?.get('LastName')?.value;
    this.partnerDealerRegistrationEntityModel.user = this.selectedPartnerCognito;
    this.partnerDealerRegistrationEntityModel.partner_admin = this.selectedPartnerCognito;
    this.partnerDealerRegistrationEntityModel.created_by = this.selectedPartnerCognito;
    this.partnerDealerRegistrationEntityModel.gst_number = 0;
    this.partnerDealerRegistrationEntityModel.partnership_type = this.PartnerType;
    if(this.partnerEntityList[0].manufacture_brands) {
      this.partnerEntityList[0].manufacture_brands?.forEach(element => {
        this.manuFactBrands.push(element.id);
      });
    }
    this.partnerDealerRegistrationEntityModel.manufacture_brands = this.manuFactBrands;
    this.agreementServiceService
      .putDealerEntityDetails(this.partnerDealerRegistrationEntityModel, this.partnerEntityList[0].id)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
        if (this.partnerEntityTaxList.length > 0) {
          this.storage.setStorageData('partnerEntityCreate', JSON.stringify(res), false);
          this.storage.setStorageData('PARTNERADMINTYPE', this.PartnerType, true);
          this.updateTaxDetails();
        } else {
          this.router.navigate(['/admin-dashboard/partner-admin/preference', this.partnerEntityList[0].id]);
        }
      });
  }

  /**select partner type set value in stroge  */
  selectPartnerType(value) {
    this.PartnerType = value;
  }

  selectAdminType(value) {
    this.selectedPartnerCognito = value.user.cognito_id;
    this.selectedPartnerId =  value.id;
  }

  /**load Tax details */
  updateTaxDetails() {
    this.TaxFormArray = <FormArray>(this.corporateProfileForm.controls['TaxArray']);
    console.log(this.TaxFormArray)
    this.partnerDealerRegistrationTaxModel = new PartnerDealerRegistrationTaxModel();
    const request: string[] = [];
    this.TaxFormArray.value.forEach(element => {
      let object: any = {};
      object = {
        id: element.id == '' ? '' : element.id,
        registration_number: element.registrationNo,
        state: element.State,
        partner: this.partnerEntityList[0].id
      }
      request.push(object)
      this.agreementServiceService
        .saveUpdateDealerTaxDetails(object)
        .subscribe((res) => {
          var getResponse = res as ResponseData;
           
          this.updateIdentityProofDetails();        

        });
    });

  }

  
  /**load identity proof data callings */
  updateIdentityProofDetails() {
    this.prepareIdentityProof(this.partnerEntityList[0].id);
    console.log("update request",this.documentProofs)
    this.agreementServiceService
      .postDealerIdentityProofDetails(this.documentProofs)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
        console.log("updated")
        this.router.navigate(['/admin-dashboard/partner-admin/preference', this.partnerEntityList[0].id]);
      });
  }

  

  saveEntityMapping(entityId: any) {
    var payloadEntityMapping: any = {};
    payloadEntityMapping.status = 0;
    payloadEntityMapping.partner = entityId;
    payloadEntityMapping.partner_contact = this.selectedPartnerId;
    this.agreementServiceService.postPartnerEntityMapping(payloadEntityMapping).subscribe((res: any) => {
      console.log(res);
      this.storage.setStorageData("entityMappingId", res.id, false);
       
    }, err => {
      console.log(err);
    })
  }
  /**load identity proof data callings */
  loadIdentityProofDetails() {
    this.prepareIdentityProof('');
    this.agreementServiceService
      .postDealerIdentityProofDetails(this.documentProofs)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
      });
  }

  /**load Tax details */
  loadTaxDetails() {
    this.TaxFormArray = <FormArray>(this.corporateProfileForm.controls['TaxArray']);
    console.log(this.TaxFormArray)
    this.partnerDealerRegistrationTaxModel = new PartnerDealerRegistrationTaxModel();
    const request: string[] = [];
    this.TaxFormArray.value.forEach(element => {
      this.partnerDealerRegistrationTaxModel.registration_number = element.registrationNo;
      this.partnerDealerRegistrationTaxModel.partner = this.entityResult.id;
      this.partnerDealerRegistrationTaxModel.state = element.State;
      request.push(this.partnerDealerRegistrationTaxModel)
    });
    this.agreementServiceService
      .postDealerTaxDetails(request)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
         
        this.loadIdentityProofDetails();
        this.router.navigate(['admin-dashboard/partner-admin/preference', this.entityResult.id])
      });
  }

  /**onChnage tab prepare data in tabs */
  prepareIdentityProof(id) {
    this.documentProofs = []
    console.log(this.corporateProfileForm?.get('pan')?.value)
    if (this.corporateProfileForm?.get('pan')?.get('docNumber')?.value != '') {
      let object1 = {};
      object1 = {
        proof_type: 1,
        id_number: this.corporateProfileForm?.get('pan')?.get('docNumber')?.value,
        status: 1,
        id: this.corporateProfileForm?.get('pan')?.get('id')?.value,
        document: this.corporateProfileForm?.get('pan')?.get('docImages')?.get('front')?.value,
        partner: this.entityResult?.id == undefined || this.entityResult?.id == null? id : this.entityResult?.id
      }
      this.documentProofs.push(object1)
    } if (this.corporateProfileForm?.get('voter')?.get('docNumber')?.value != '') {
      let object2 = {};
      object2 = {
        proof_type: 6,
        id_number: this.corporateProfileForm?.get('voter')?.get('docNumber')?.value,
        id: this.corporateProfileForm?.get('voter')?.get('id')?.value,
        status: 1,
        document: this.corporateProfileForm?.get('voter')?.get('docImages')?.get('front')?.value,
        document_back:this.corporateProfileForm?.get('voter')?.get('docImages')?.get('back')?.value,
        partner: this.entityResult?.id == undefined || this.entityResult?.id == null? id : this.entityResult?.id
      }
      this.documentProofs.push(object2)
    }
    if (this.corporateProfileForm?.get('uan')?.get('docNumber')?.value != '') {
      let object3 = {};
      object3 = {
        proof_type: 7,
        id_number: this.corporateProfileForm?.get('uan')?.get('docNumber')?.value,
        id: this.corporateProfileForm?.get('uan')?.get('id')?.value,
        status: 1,
        document: this.corporateProfileForm?.get('uan')?.get('docImages')?.get('front')?.value,
        document_back:this.corporateProfileForm?.get('uan')?.get('docImages')?.get('back')?.value,
        partner: this.entityResult?.id == undefined || this.entityResult?.id == null? id : this.entityResult?.id
      }
      this.documentProofs.push(object3)
    }
    if (this.corporateProfileForm?.get('tradelicense')?.get('docNumber')?.value != '') {
      let object4 = {};
      object4 = {
        proof_type: 8,
        id_number: this.corporateProfileForm?.get('tradelicense')?.get('docNumber')?.value,
        id: this.corporateProfileForm?.get('tradelicense')?.get('id')?.value,
        status: 1,
        document: this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('front')?.value,
        document_back:this.corporateProfileForm?.get('tradelicense')?.get('docImages')?.get('back')?.value,
        partner: this.entityResult?.id == undefined || this.entityResult?.id == null? id : this.entityResult?.id
      }
      this.documentProofs.push(object4)
    } else { }
    console.log(this.documentProofs)
  }
  /*** get state data*/
  getStateData() {
    this.agreementServiceService.getStateMaster('limit=999').subscribe(
      (res: any) => {
         
        this.stateListData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }
  onSubmit() {
    if (!this.checkEmailField()) {
      this.corporateProfileForm.get('EmailId')?.markAsTouched();
      return;
    }
    // if(this.isPanExist)
    // {
    //   this.docImge="";
    // }
    // if(this.corporateProfileForm.invalid){
    //   return;
    // }
    // else{
    // }
    if (!this.checkPanImageOnSubmit()) {
      return;
    }
    //this.spinner.show();
    if (this.corporateProfileForm.get('applied_for_pan')?.value) {
      if (
        this.corporateProfileForm?.get('aadhar')?.get('docNumber')?.value ==
        '' &&
        this.corporateProfileForm?.get('passport')?.get('docNumber')?.value ==
        '' &&
        this.corporateProfileForm?.get('voter')?.get('docNumber')?.value ==
        '' &&
        this.corporateProfileForm?.get('dl')?.get('docNumber')?.value == '' &&
        this.corporateProfileForm?.get('uan')?.get('docNumber')?.value == '' &&
        this.corporateProfileForm?.get('tradelicense')?.get('docNumber')
          ?.value == ''
      ) {
        this.notify.warn(
          'If you applied for PAN, until then please enter one proof of Identity other than PAN.'
        );
         
        return;
      }
    }
    const value = { ...this.corporateProfileForm.getRawValue() };
    // let docObj={
    //     "pan" : {
    //     "docType" : "pan",
    //     "docNumber" : value?.PanCardNo,
    //     "name":value?.CompanyName,
    //     "docImages":
    //      {"front" :this.docImge,
    //      "back" : "" },
    //   },
    // };
    var aadharCard = this.corporateProfileForm?.get('aadhar')?.value;
    if (aadharCard != null && aadharCard != undefined) {
      if (
        aadharCard?.docNumber != null &&
        aadharCard?.docNumber != '' &&
        aadharCard?.docNumber != undefined
      ) {
        if (
          (aadharCard?.docImages?.front == '' ||
            aadharCard?.docImages?.front?.length < 0) &&
          (aadharCard?.docImages?.back == '' ||
            aadharCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('aadhar')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('aadhar')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('aadhar')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('aadhar')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    var passportCard = this.corporateProfileForm?.get('passport')?.value;
    if (passportCard != null && passportCard != undefined) {
      if (
        passportCard?.docNumber != null &&
        passportCard?.docNumber != '' &&
        passportCard?.docNumber != undefined
      ) {
        if (
          (passportCard?.docImages?.front == '' ||
            passportCard?.docImages?.front?.length < 0) &&
          (passportCard?.docImages?.back == '' ||
            passportCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('passport')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('passport')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('passport')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('passport')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    var voterCard = this.corporateProfileForm?.get('voter')?.value;
    if (voterCard != null && voterCard != undefined) {
      if (
        voterCard?.docNumber != null &&
        voterCard?.docNumber != '' &&
        voterCard?.docNumber != undefined
      ) {
        if (
          (voterCard?.docImages?.front == '' ||
            voterCard?.docImages?.front?.length < 0) &&
          (voterCard?.docImages?.back == '' ||
            voterCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('voter')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('voter')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('voter')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('voter')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    var dlCard = this.corporateProfileForm?.get('dl')?.value;
    if (dlCard != null && dlCard != undefined) {
      if (
        dlCard?.docNumber != null &&
        dlCard?.docNumber != '' &&
        dlCard?.docNumber != undefined
      ) {
        if (
          (dlCard?.docImages?.front == '' ||
            dlCard?.docImages?.front?.length < 0) &&
          (dlCard?.docImages?.back == '' || dlCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('dl')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('dl')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('dl')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('dl')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    var uanCard = this.corporateProfileForm?.get('uan')?.value;
    if (uanCard != null && uanCard != undefined) {
      if (
        uanCard?.docNumber != null &&
        uanCard?.docNumber != '' &&
        uanCard?.docNumber != undefined
      ) {
        if (
          (uanCard?.docImages?.front == '' ||
            uanCard?.docImages?.front?.length < 0) &&
          (uanCard?.docImages?.back == '' ||
            uanCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('uan')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('uan')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('uan')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('uan')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    var tradelicenseCard = this.corporateProfileForm?.get('tradelicense')
      ?.value;
    if (tradelicenseCard != null && tradelicenseCard != undefined) {
      if (
        tradelicenseCard?.docNumber != null &&
        tradelicenseCard?.docNumber != '' &&
        tradelicenseCard?.docNumber != undefined
      ) {
        if (
          (tradelicenseCard?.docImages?.front == '' ||
            tradelicenseCard?.docImages?.front?.length < 0) &&
          (tradelicenseCard?.docImages?.back == '' ||
            tradelicenseCard?.docImages?.back?.length < 0)
        ) {
          this.corporateProfileForm
            ?.get('tradelicense')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors({ required: true });
          this.corporateProfileForm
            ?.get('tradelicense')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors({ required: true });
           
          return;
        } else {
          this.corporateProfileForm
            ?.get('tradelicense')
            ?.get('docImages')
            ?.get('front')
            ?.setErrors(null);
          this.corporateProfileForm
            ?.get('tradelicense')
            ?.get('docImages')
            ?.get('back')
            ?.setErrors(null);
        }
      }
    }
    if (value?.EntityType == 'Proprietorship') {
      this.corporateProfileForm
        ?.get('pan')
        ?.get('name')
        ?.setValue(value?.FirstName.concat(' ' + value?.LastName.toString()));
      value.pan.name = value?.FirstName.concat(
        ' ' + value?.LastName.toString()
      );
    } else {
      this.corporateProfileForm
        ?.get('pan')
        ?.get('name')
        ?.setValue(value?.CompanyName);
      value.pan.name = value?.CompanyName;
    }
    var docObj;
    if (this.corporateProfileForm.get('applied_for_pan')?.value) {
      docObj = {
        aadhar: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('aadhar')?.value)
        ),
        passport: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('passport')?.value)
        ),
        voter: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('voter')?.value)
        ),
        dl: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('dl')?.value)
        ),
        uan: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('uan')?.value)
        ),
        tradelicense: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('tradelicense')?.value)
        ),
      };
    } else {
      docObj = {
        pan: JSON.parse(JSON.stringify(value?.pan)),
        aadhar: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('aadhar')?.value)
        ),
        passport: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('passport')?.value)
        ),
        voter: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('voter')?.value)
        ),
        dl: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('dl')?.value)
        ),
        uan: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('uan')?.value)
        ),
        tradelicense: JSON.parse(
          JSON.stringify(this.corporateProfileForm?.get('tradelicense')?.value)
        ),
      };
    }
    this.userprofileData.doc_details = docObj;
    if (!this.corporateProfileForm.get('applied_for_pan')?.value) {
      this.userprofileData.doc_details.pan.docImages.frontView = '';
    }
    this.userprofileData.doc_details.aadhar.docImages.frontView = '';
    this.userprofileData.doc_details.aadhar.docImages.backView = '';
    this.userprofileData.doc_details.passport.docImages.frontView = '';
    this.userprofileData.doc_details.passport.docImages.backView = '';
    this.userprofileData.doc_details.voter.docImages.frontView = '';
    this.userprofileData.doc_details.voter.docImages.backView = '';
    this.userprofileData.doc_details.dl.docImages.frontView = '';
    this.userprofileData.doc_details.dl.docImages.backView = '';
    this.userprofileData.doc_details.uan.docImages.frontView = '';
    this.userprofileData.doc_details.uan.docImages.backView = '';
    this.userprofileData.doc_details.tradelicense.docImages.frontView = '';
    this.userprofileData.doc_details.tradelicense.docImages.backView = '';

    this.userprofileData.applied_for_pan = value?.applied_for_pan;
    this.userprofileData.represent = value?.Representer;
    this.userprofileData.entityType = value?.EntityType;
    this.userprofileData.relationshipWithEntity = value?.RelationshipEntity;
    this.userprofileData.doc_details = docObj;
    this.userprofileData.deletedAddress = this.deletedAddress;
    // let date = new Date(value?.DateoOfIncorporation);

    // let dd = date.getDate();
    // let mm = date.getMonth() + 1;
    // let yyyy = date.getFullYear();
    // var docDate=dd + "/" + mm + "/" + yyyy;

    // console.log(dd + "/" + mm + "/" + yyyy);
    //this.userprofileData.gstNo=value?.GSTNo;
    var dob = value?.DateoOfIncorporation;
    var docDate = this.datePipe.transform(dob, 'yyyy-MM-dd');
    this.userprofileData.corporateDetails = [];
    this.userprofileData.address = [];

    if (
      this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
    ) {
      this.userprofileData.corporateDetails?.push({
        companyName: value?.CompanyName,
        dateOfIncorporation: docDate?.toString(),
        mobileNumber: value?.Mobile,
        emailId: value?.EmailId,
        msme: value?.MSMENumber,
      });
    } else {
      this.userprofileData.corporateDetails?.push({
        companyName: value?.CompanyName,
        firstName: value?.FirstName,
        lastName: value?.LastName,
        dateOfIncorporation: docDate?.toString(),
        mobileNumber: value?.Mobile,
        emailId: value?.EmailId,
        msme: value?.MSMENumber,
      });
    }
    for (let index = 0; index < value.AddressArray.length; index++) {
      this.userprofileData.address?.push({
        addressId: value.AddressArray[index]?.addressId,
        addressLine1: value.AddressArray[index]?.StreetAddress1,
        addressLine2: value.AddressArray[index]?.StreetAddress2,
        pincode: value.AddressArray[index]?.PinCode,
        state: value.AddressArray[index]?.State,
        city: value.AddressArray[index]?.City,
        gstNo: value.AddressArray[index]?.GSTNo,
      });
    }
    // if(this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!=null ||
    //  this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!="" ||
    //  this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value!=undefined) {
    //   this.userprofileData.address_proof= JSON.parse(JSON.stringify(value.address_proof));
    //  }
    if (
      this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value !=
      null &&
      this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value !=
      '' &&
      this.corporateProfileForm.get('address_proof')?.get('docNumber')?.value !=
      undefined
    ) {
      if (
        this.corporateProfileForm?.get('address_proof')?.get('docImages')
          ?.value == '' ||
        this.corporateProfileForm?.get('address_proof')?.get('docImages')
          ?.value == undefined
      ) {
         
        this.notify.warn('Please upload Address Proof image.');
        return;
      }

      this.userprofileData.address_proof = JSON.parse(
        JSON.stringify(value.address_proof)
      );
    }
    this.httpClient
      .post(
        environment.kycDocumentVerifyUrl,
        JSON.stringify(this.userprofileData)
      )
      .subscribe(
        (resp: any) => {
          //console.log(resp);
          this.VerifiedDataobj = JSON.parse(JSON.stringify(resp.body));

          if (resp?.statusCode == 200) {
            // if (this.VerifiedDataobj?.pan?.isVerified==1 || (this.VerifiedDataobj?.aadhar?.isVerified==1 || this.VerifiedDataobj?.voter?.isVerified==1 || this.VerifiedDataobj?.dl?.isVerified==1 || this.VerifiedDataobj?.passport?.isVerified==1)){
            this.notify.success(
              'Corporate data saved or verified successfully'
            );
            this.router.navigate([`./customer/dashboard/communication`]);
             

            //  }
            //  else{
            //   this.notify.success("User saved or verified successfully");
            //   this.router.navigate([`./communication`]);
            //    
            //  }
          } else {
             
            var msg = JSON.parse(this.VerifiedDataobj);
            this.notify.error(msg?.message);
            this.userprofileData.address = [];
            this.userprofileData.corporateDetails = [];
          }

          // if(this.VerifiedDataobj?.code==200){
          //   if (this.VerifiedDataobj?.pan?.isVerified==1 || (this.VerifiedDataobj?.aadhar?.isVerified==1 || this.VerifiedDataobj?.voter?.isVerified==1 || this.VerifiedDataobj?.dl?.isVerified==1 || this.VerifiedDataobj?.uan?.isVerified==1 || this.VerifiedDataobj?.tradelicense?.isVerified==1 || this.VerifiedDataobj?.passport?.isVerified==1)){
          //     this.notify.success("Corporate user saved or verified successfully");
          //     this.router.navigate([`./communication`]);
          //      

          //   }
          //   else{
          //     this.notify.error("please enter a valid PAN");
          //      
          //   }
          // }

          // else if(this.VerifiedDataobj?.pan?.isVerified==undefined){
          //    
          //   this.notify.error("please enter a valid PAN");
          // }
        },
        (error) => {
           
          console.log(error);
          //   alert(error.message.toSting());
        }
      );
  }

  /**on tav chnage */
  onTabChanged($event) { 
   let value = $event;
   if(value == 1) {
     this.checkPan();
     this.verifyPanData();
   }
  }

  checkaddressFun(ev: KeyboardEvent) {
    let k = ev.keyCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 9 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  toggleEditable(event: any) {
    this.corporateProfileForm.get('applied_for_pan')?.setValue(event.checked);
    if (event.checked) {
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.setErrors(null);
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.setValue('');
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.disable();
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('front')
        ?.setErrors(null);
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('front')
        ?.setValue('');
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('frontView')
        ?.setValue('');
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('front')
        ?.disable();
    } else {
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docNumber')
        ?.setErrors({ required: true });
      this.corporateProfileForm?.get('pan')?.get('docNumber')?.enable();
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('front')
        ?.setErrors({ required: true });
      this.corporateProfileForm
        ?.get('pan')
        ?.get('docImages')
        ?.get('front')
        ?.enable();
    }
    if (
      this.corporateProfileForm?.get('EntityType')?.value != 'Proprietorship'
    ) {
      this.corporateProfileForm.get('CompanyName')?.setValue('');
      this.corporateProfileForm?.get('CompanyName')?.enable();
    }
    // this.corporateProfileForm.get('LastName')?.setValue(this.initialLastName);
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if (
      file.size <= 5242880 &&
      (file.type == 'image/png' ||
        file.type == 'image/jpg' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/bmp')
    ) {
      this.isInvalidfile = true;
      //  this.isShowPanDoc = false;
      this.uploadingImage = true;
      this.corporateProfileForm
        .get(tab)
        ?.get('docImages')
        ?.get(side)
        ?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log('type', file.type);
        this.corporateProfileForm
          .get(tab)
          ?.get('docImages')
          ?.get(side + 'View')
          ?.setValue(reader.result);
      };
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path =
        environment.bucket_parent_folder +
        '/' +
        this.cognitoId +
        '/' +
        tab +
        '/' +
        side +
        '.' +
        fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      console.log(uploaded_file)
      if (
        uploaded_file?.Key != undefined &&
        uploaded_file?.Key != '' &&
        uploaded_file?.Key != null
      ) {
        this.corporateProfileForm
          .get(tab)
          ?.get('docImages')
          ?.get(side)
          ?.setValue(uploaded_file?.Location);
      } else {
        this.corporateProfileForm
          .get(tab)
          ?.get('docImages')
          ?.get(side + 'View')
          ?.setValue('');
        this.notify.error('Upload Failed');
      }
      // controlName=reader.result;
      setTimeout(() => {
        this.checkPanImageOnSubmit();
      }, 0);
      this.uploadingImage = false;
      // };
    } else {
      // this.isShowPanDoc = true;
      this.corporateProfileForm
        .get(tab)
        ?.get('docImages')
        ?.get(side)
        ?.setValue('');
      this.corporateProfileForm
        .get(tab)
        ?.get('docImages')
        ?.get(side)
        ?.setErrors({ imgIssue: true });
    }
  }

  handleUploadAddress(event: any) {
    const file = event.target.files[0];
    if (
      file.size <= 5242880 &&
      (file.type == 'image/png' ||
        file.type == 'image/jpg' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/bmp')
    ) {
      this.isInvalidfile = true;
      this.isShowDocProof = false;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log(reader.result);
        this.corporateProfileForm
          .get('address_proof')
          ?.get('docImages')
          ?.setValue(reader.result);
        // controlName=reader.result;
      };
    } else {
      this.isShowDocProof = true;
      this.corporateProfileForm
        .get('address_proof')
        ?.get('docImages')
        ?.setValue('');
    }
  }

  bindFlags() {
    this.accountService.getCountryDetails().subscribe(
      (response) => {
        var countries = response as any;
        this.countryList = countries.body;
        this.corporateProfileForm
          .get('countryCode')
          ?.setValue(this.countryList[0]);
        this.selectedCity = this.countryList[0];
      },
      (err) => {
        this.notify.error(err);
         
      }
    );
  }

  updateCountryCode(city: any) {
    this.corporateProfileForm.get('countryCode')?.setValue(city);
  }

  checkEmailField() {
    if (this.isEmailReq) {
      let email = this.corporateProfileForm?.get('EmailId')?.value;
      if (!email) {
        this.corporateProfileForm.get('EmailId')?.setErrors({ required: true });
        return false;
      } else if (
        this.corporateProfileForm.get('EmailId')?.hasError('pattern')
      ) {
        this.corporateProfileForm.get('EmailId')?.setErrors(null);
        this.corporateProfileForm.get('EmailId')?.setErrors({ pattern: true });
        return false;
      } else {
        this.corporateProfileForm.get('EmailId')?.setErrors(null);
        return true;
      }
    } else if (this.corporateProfileForm.get('EmailId')?.hasError('pattern')) {
      this.corporateProfileForm.get('EmailId')?.setErrors(null);
      this.corporateProfileForm.get('EmailId')?.setErrors({ pattern: true });
      return false;
    } else {
      this.corporateProfileForm.get('EmailId')?.setErrors(null);
      return true;
    }
  }

  ngOnDestroy() {
        //this.storage.clearLocalStorageData('partnerEntityCreate');
        //this.storage.clearLocalStorageData('partnerEntityAdmin')
        //this.storage.clearLocalStorageData('PARTNERADMINTYPE')
        //this.storage.clearLocalStorageData('EDIT_PARTNER_ENTITY_ADMIN')
        //this.storage.clearLocalStorageData('entityMappingId')
  }
}
