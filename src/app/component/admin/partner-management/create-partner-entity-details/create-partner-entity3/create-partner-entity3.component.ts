import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {

  MatSlideToggleChange,
} from '@angular/material/slide-toggle';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/app/services/shared-service.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { AddressDetails } from 'src/app/shared/abstractions/user';
import { PartnerDealerRegistrationAddress, PartnerDealerRegistrationEntityModel } from 'src/app/models/partner-dealer-management/partner-dealer-registration-data.model';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { AccountService } from 'src/app/services/account';
import { PincodeResponse } from 'src/app/models/common/pinCodeResponse.model';
import { DatePipe } from '@angular/common';
import { Params, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-partner-entity3',
  templateUrl: './create-partner-entity3.component.html',
  styleUrls: ['./create-partner-entity3.component.css'],
})
export class CreatePartnerEntity3Component implements OnInit {
  corporateProfileForm: FormGroup;
  partnerDealerRegistrationEntityModel: any;
  cognitoId: any;
  AddressFormArray: any;
  TaxFormArray: any;
  BrandLocationsArray: any;
  stateList: any = [];
  partnerDealerRegistrationAddress: any;
  pinCodeResponse?: PincodeResponse;
  partnerId: any;
  brandData: any;
  cityData: any;
  modelList: any;
  data: any;
  
  constructor(
    private storage: StorageDataService,
    public sharedService: SharedService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private agreementServiceService: AgreementServiceService,
    private accountService: AccountService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private router: Router
  ) {
    this.corporateProfileForm = this.fb.group({
      //Gender:['M'],
      StreetAddress1: ['', Validators.required],
      PinCode: ['', Validators.required],
      State: ['', Validators.required],
      City: ['', Validators.required],
      Brand: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.data = JSON.parse(this.storage.getStorageData('partnerEntityCreate', false));
    console.log(this.storage.getStorageData('partnerEntityCreate', false))
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getStateData();
    this.getBrandData('');
    this.getCity();
    this.onBrandSelect();
    this.route.params.subscribe((params: Params): void => {
      this.partnerId = params['id'];
    });
  }

  getBrandData(queryparams = "limit=999") {
    this.agreementServiceService.getBrandMaster(queryparams).subscribe((data: any) => {
      this.brandData = data.results;
    });
  }

  getCity() {
    this.agreementServiceService.getMasterCity().subscribe((data: any) => {
      this.cityData = data.results;
    });
  }

  /*** get state data*/
  getStateData() {
    this.agreementServiceService.getStateMaster('limit=999').subscribe(
      (res: any) => {
         
        this.stateList = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /*** get state data*/
  getLocationData() {
    let pincode = this.corporateProfileForm?.get('PinCode')?.value;
    let queryParam = `search=${pincode}`;
    this.agreementServiceService.getPinCode(queryParam).subscribe(
      (res: any) => {
         
        this.stateList = res.results;
        this.corporateProfileForm?.get('City')?.setValue(this.stateList[0].city?.name)
        this.corporateProfileForm?.get('State')?.setValue(this.stateList[0].city.state?.name)
      },
      (err) => {
        console.error(err);
      }
    );
  }

  onSubmit() {
    this.corporateProfileForm?.markAllAsTouched();
    if (this.corporateProfileForm.valid) {
      //this.spinner.show();
      this.AddressFormArray = <FormArray>(this.corporateProfileForm.controls['AddressArray']);
      this.partnerDealerRegistrationAddress = new PartnerDealerRegistrationAddress();
      this.partnerDealerRegistrationAddress.address = this.corporateProfileForm?.get('StreetAddress1')?.value;
      this.partnerDealerRegistrationAddress.state = this.stateList[0].city.state?.id;
      this.partnerDealerRegistrationAddress.partner = parseInt(this.partnerId);
      this.partnerDealerRegistrationAddress.city = this.stateList[0].city?.id;
      this.partnerDealerRegistrationAddress.pin_code = this.stateList[0].id
      this.agreementServiceService
        .postDealerAddressDetails(this.partnerDealerRegistrationAddress)
        .subscribe((res) => {
          this.saveOnSubmitBrands();
          var getResponse = res as ResponseData;
        });
    }
  }

  /**on tav chnage */
  onTabChanged(event: any) { }

  checkaddressFun(ev: KeyboardEvent) {
    let k = ev.keyCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 9 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  /**save partner multi brands */
  saveOnSubmitBrands() {
    let object = {};
    object = {
      entity_type: this.data.entity_type,
      is_msme_number: this.data.is_msme_number,
      msme_number: this.data.msme_number,
      company_name: this.data.company_name,
      incorporation_date: this.data.incorporation_date,
      mobile: this.data.mobile,
      status: this.data.status,
      created_by: this.data.created_by,
      partner_admin: this.data.partner_admin,
      manufacture_brands: this.corporateProfileForm?.get('Brand')?.value
    }
    this.agreementServiceService
      .putDealerEntityDetails(object, this.partnerId)
      .subscribe((res) => {
        var getResponse = res as ResponseData;
         
        this.router.navigate(['/partner-bank-management']);
      });
  }

  onBrandSelect() {
    this.agreementServiceService.getBrandMaster('limit=999').subscribe(
      (res: any) => {
        this.modelList = res.results;
      },
      err => {
        console.error(err);
      });
  }
}
