import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerEntityListComponent } from './partner-entity-list.component';

describe('PartnerEntityListComponent', () => {
  let component: PartnerEntityListComponent;
  let fixture: ComponentFixture<PartnerEntityListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerEntityListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerEntityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
