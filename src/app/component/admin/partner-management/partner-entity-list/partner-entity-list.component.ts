import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageSettingsModel } from '@syncfusion/ej2-angular-grids';
import { FilterSettingsModel } from '@syncfusion/ej2-angular-grids';
import { UserService } from 'src/app/services/user';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { GridComponent } from '@syncfusion/ej2-angular-grids'
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { VerifyuserComponent } from 'src/app/component/add-user/verify-user/verifyuser.component';
import { MatDialog } from '@angular/material/dialog';
import { ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { PartnerDealerRegistrationEntityModel } from '../../../../models/partner-dealer-management/partner-dealer-registration-data.model';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';
import * as _ from 'underscore';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

export interface PeriodicElement {
  UserName: string;
  firstName: string;
  email: string;
  phoneNumber: string;
  kyc_verified: string;
  state: string;
  city: string;
  pan_address_proof?: string;
  Groups: string[];
}

@Component({
  selector: 'app-partner-entity-list',
  templateUrl: './partner-entity-list.component.html',
  styleUrls: ['./partner-entity-list.component.css']
})

export class PartnerEntityListComponent implements OnInit {
  @ViewChild('grid') public grid?: GridComponent;
  @ViewChild("TableOneSort", { static: true }) tableOneSort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public activePage: any = 1;
  public total_count: any;
  public pageNumber: number = 1;
  public cognitoId: any;
  public pager: any = {};
  public norecordvariable = false;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public partnerDealerRegistrationEntityModel: any
  public dataSource!: MatTableDataSource<any>;
  public displayedColumns: string[] = [
    "id", "company_name", "created_at", "incorporation_date", "status", "mobile", "entity_type", "partnership_type", "relation_with_entity", "msme_number", "Actions"
  ];
  public role?: string;
  public partnerEntityList: any;
  public productDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public listingData = [];
  public searchData: any = {
    'source': '',
    'mobileNumber': '',
    'firstName': ''
  };
  public filterUserForm: any = this.formbulider.group({
    Mobile: '',
    Name: '',
    partnerType: '',
    entityType: ''
  });
  public data: PeriodicElement[] = [];
  pageSettings: PageSettingsModel = { pageSize: 10 };
  public toolbarOptions?: ToolbarItems[] = ['ExcelExport'];
  filterOptions: FilterSettingsModel = {
    type: 'Menu'
  };
  scrolled: boolean = false;
  constructor(private datePipe: DatePipe, private _snackBar: MatSnackBar,
    private partnerDealerRegistrationService: PartnerDealerRegistrationService, private router: Router, public userService: UserService, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private sharedService: SharedService, private storage: StorageDataService, public dialog: MatDialog, private appRouteEnum: AppRouteEnum, private formbulider: FormBuilder,
    private notificationservice: NotificationService, private apiRouteService: ApiRouteService) {

  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.role = this.storage.getStorageData("role", false);
    if (this.role != this.apiRouteService.roles.superadmin) {
      this.toolbarOptions = [];
    }
    // if(this.role==this.apiRouteService.roles.customer){
    //   this.sharedService.redirecteToMP1AfterLogin();
    // }
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['src']) {
        this.storage.clearStorageData("src");
        this.storage.setStorageData("src", params['src'], true);
        // this.sharedService.queryParamsAfterLogin = params;
        // this.storage.clearStorageData("aflparams");
        // this.storage.setStorageData("aflparams", params, true);
      }
      if (params['cognitoId']) {
        this.storage.clearStorageData("cognitoId");
        this.storage.setStorageData("cognitoId", params['cognitoId'].replace(/['"]+/g, ''), true);
      }
      if (params['deviceKey']) {
        this.storage.clearStorageData("deviceKey");
        this.storage.setStorageData("deviceKey", params['deviceKey'], true);
      }
      if (params['refreshToken']) {
        this.storage.clearStorageData("refreshToken");
        this.storage.setStorageData("refreshToken", params['refreshToken'], true);
      }
    });
    //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': ''
    };
    this.getusersfn(this.searchData);
    this.initializeFilterFormGroup();
    this.getPartnerEntityList();
  }


  verifyusernav(id: any) {
    const dialogRef = this.dialog.open(VerifyuserComponent, {
      width: '650px',
      data: { id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getusersfn(this.searchData);
    });
  }



  toolbarClick(args: ClickEventArgs): void {
    if (args.item.id === 'Grid_excelexport') { // 'Grid_excelexport' -> Grid component id + _ + toolbar item name
      this.grid?.excelExport();
    }
  }

  addusernav() {
    this.clearPartnerStorage();
    this.router.navigate(['/admin-dashboard/partner-admin/create-partner-entity'], { queryParams: { isEditEntity: false } });
  }

  clearPartnerStorage() {
    this.storage.clearLocalStorageData('partnerEntityCreate');
    this.storage.clearLocalStorageData('partnerEntityAdmin')
    this.storage.clearLocalStorageData('PARTNERADMINTYPE')
    this.storage.clearLocalStorageData('EDIT_PARTNER_ENTITY_ADMIN')
    this.storage.clearLocalStorageData('entityMappingId')
  }

  editusernav(editEntity) {
    this.storage.setStorageData('EDIT_PARTNER_ENTITY_ADMIN', JSON.stringify(editEntity), false);
    this.router.navigate(['/admin-dashboard/partner-admin/create-partner-entity'], { queryParams: { isEditEntity: true } });
  }

  dataBound() {
    this.grid?.autoFitColumns(['email', 'phoneNumber']);
  }

  getusersfn(data: any) {
    this.userService.getDetails(data).subscribe(response => {
      var getResponse = response as ResponseData;
      if (getResponse.statusCode == 200) {
        if (getResponse.body != null) {
          var getResponseBodyJSON = JSON.parse(getResponse.body);
          this.data = getResponseBodyJSON as PeriodicElement[];

        }
      }
      else {
        var getResponseBodyJSON = JSON.parse(getResponse.body);
        this.notificationservice.error(getResponseBodyJSON.message);

      }
    });
  }

  filterData() {
    //this.spinner.show();
    var formData = this.filterUserForm.value;
    if (formData != undefined) {
      this.searchData = {
        'mobileNumber': formData.Mobile,
      };
      this.getFilteredData(formData.Mobile, '', '');
    }
  }

  selectPartnerType() {
    var formData = this.filterUserForm.value;
    this.partnerEntityList = [];
    this.productDataSource.data = [];
    this.total_count = 0;
    this.getFilteredData('', formData.partnerType, '');
  }

  selectEntityType() {
    var formData = this.filterUserForm.value;
    this.partnerEntityList = [];
    this.productDataSource.data = [];
    this.total_count = 0;
    this.getFilteredData('', '', formData.entityType);
  }

  getFilteredData(Mobile, partnerType, entityType) {
    //this.spinner.show();
    this.partnerDealerRegistrationService.getPartnerEntityList(this.pageNumber, this.cognitoId, this.pageSize, Mobile, this.ordering, partnerType, entityType).subscribe(
      (res: any) => {

        this.partnerEntityList = res.results;
        this.total_count = res.count;
        //this.productDataSource = new MatTableDataSource<any>(this.partnerEntityList)
        this.productDataSource.data = res.results;
        console.log(this.productDataSource)
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /**sorting on list */
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getPartnerEntityList();
  }

  resetData() {
    //this.spinner.show();
    this.searchData = {
      'source': '',
      'mobileNumber': '',
      'firstName': '',
    };
    this.initializeFilterFormGroup();
  }

  initializeFilterFormGroup() {
    this.filterUserForm.setValue({
      Mobile: '',
      Name: '',
      partnerType: '',
      entityType: ''
    });
  }

  /**on chnage pages pagination view */
  onPageChange(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPartnerEntityList();
  }


  getPartnerEntityList() {
    //this.spinner.show();
    let partnertype = this.filterUserForm.get('partnerType').value ? this.filterUserForm.get('partnerType').value : '';
    let entitytype = this.filterUserForm.get('entityType').value ? this.filterUserForm.get('entityType').value : '';
    this.partnerDealerRegistrationService.getPartnerEntityList(this.pageNumber, this.cognitoId, this.pageSize, '', this.ordering, partnertype, entitytype).subscribe(
      (res: any) => {

        // this.partnerEntityList = res.results;
        // this.total_count=res.count;
        // this.productDataSource = new MatTableDataSource<any>(this.partnerEntityList)
        // console.log(this.productDataSource)
        this.partnerEntityList = res.results;
        this.total_count = res.count;
        if (window.innerWidth > 768) {
          this.productDataSource.data = res.results;
        } else {
          this.scrolled = false;
          if (this.pageNumber == 1) {
            this.productDataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.productDataSource.data = this.listingData;
          } else {
            this.listingData = this.listingData.concat(res.results);
            this.productDataSource.data = this.listingData;
          }
        }
      },
      (err) => {
        console.error(err);
      }
    );
  }

  exportData() {
    ExportExcelUtil.exportArrayToExcel(this.partnerEntityList, "partner-entity");
  }
  approve(rowdata, action) {
    const actions = 'Success!!';
    //this.spinner.show();
    this.partnerDealerRegistrationEntityModel = new PartnerDealerRegistrationEntityModel();
    this.partnerDealerRegistrationEntityModel.entity_type = rowdata.entity_type;
    this.partnerDealerRegistrationEntityModel.is_msme_number = rowdata.is_msme_number;
    this.partnerDealerRegistrationEntityModel.msme_number = rowdata.msme_number;
    this.partnerDealerRegistrationEntityModel.company_name = rowdata.company_name;
    this.partnerDealerRegistrationEntityModel.incorporation_date = this.datePipe.transform(rowdata.incorporation_date, 'yyyy-MM-dd');
    this.partnerDealerRegistrationEntityModel.mobile = rowdata.mobile;
    this.partnerDealerRegistrationEntityModel.partner_admin = rowdata.partner_admin.cognito_id;
    this.partnerDealerRegistrationEntityModel.created_by = rowdata.created_by;
    this.partnerDealerRegistrationEntityModel.gst_number = 1;
    this.partnerDealerRegistrationEntityModel.status = action;
    this.partnerDealerRegistrationEntityModel.manufacture_brands = [1];
    this.partnerDealerRegistrationService.approveRejectEntity(this.partnerDealerRegistrationEntityModel, rowdata.id).subscribe(
      (res: any) => {

        // this._snackBar.open('Status updated successfully', actions, {
        //   duration: 8000,
        // });
        this.notificationservice.success("Status updated successfully")
      },
      (err) => {
        console.error(err);
      }
    );

  }

  delete(rowdata) {
    const actions = "success!!"
    this.partnerDealerRegistrationService.deleteEntity(rowdata.id).subscribe(
      (res: any) => {

        // this._snackBar.open('Entity deleted successfully', actions, {
        //   duration: 8000,
        // });
        this.notificationservice.success("Deleted successfully")

        this.getPartnerEntityList();
      },
      (err) => {
        console.error(err);
      }
    );

  }


  norecordshow() {
    setTimeout(() => {
      if (this.partnerEntityList.length === 0) {
        this.norecordvariable = true;
      }
      else {
        this.norecordvariable = false;
      }
    }, 2000);
  }

  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if ((this.total_count > this.productDataSource.data.length) && (this.productDataSource.data.length > 0)) {
          if (!this.scrolled) {
            this.scrolled = true;
            this.pageNumber++;
            this.getPartnerEntityList();
          }
        }
      }
    }
  }
}


