import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementNewComponent } from './user-management-new.component';

describe('UserManagementNewComponent', () => {
  let component: UserManagementNewComponent;
  let fixture: ComponentFixture<UserManagementNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserManagementNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagementNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
