import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-transactions',
  templateUrl: './view-transactions.component.html',
  styleUrls: ['./view-transactions.component.css']
})
export class ViewTransactionsComponent implements OnInit {
  transactionData;
  total_count;
  public displayedColumns: string[] = [
    "transaction_id", "payment_mode", "payment_type", "bank_name", "bank_ref_no", "amount", "payment_date", "payment_status"
  ];
  public statusOptions: Array<any> = [
    { name: 'Initiated', value: 1 },
    { name: 'Successful', value: 2 },
    { name: 'Failed', value: 3 },
    { name: 'Cancelled', value: 4 },
    { name: 'To be realised', value: 5 }
  ];
  isFromFulfilment: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ViewTransactionsComponent>) {
    if (data && data?.isFromFulfilment) {
      this.transactionData = data?.element;
      this.total_count = data?.element?.length;
      this.isFromFulfilment = data?.isFromFulfilment;
    }
    else {
      this.transactionData = data;
      this.total_count = data?.length;
    }

  }

  ngOnInit(): void {
  }
  close() {
    this.dialogRef.close()
  }
  getPaymentStatus(status) {
    let name;
    for (let row of this.statusOptions) {
      if (row.value == status) {
        name = row.name;
        break;
      }
    }
    return name;
  }
}
