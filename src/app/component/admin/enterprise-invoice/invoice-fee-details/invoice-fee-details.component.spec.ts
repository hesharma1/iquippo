import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceFeeDetailsComponent } from './invoice-fee-details.component';

describe('InvoiceFeeDetailsComponent', () => {
  let component: InvoiceFeeDetailsComponent;
  let fixture: ComponentFixture<InvoiceFeeDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceFeeDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceFeeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
