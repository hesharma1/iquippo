import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ValuationService } from 'src/app/services/valuation.service';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-invoice-fee-details',
  templateUrl: './invoice-fee-details.component.html',
  styleUrls: ['./invoice-fee-details.component.css']
})
export class InvoiceFeeDetailsComponent implements OnInit {
  ucnList:any = [];
  feeDetails;
  public feeDetailsData!: MatTableDataSource<any>;
  public total_count: any;
  public isCreditNote = false;
  public ucnCreditList: any = [];
  public invoiceNumber;
  public displayedColumns: string[] = [
    "ucn","asset_name","amount","valuation_charge"
  ];
  public displayedCreditNoteColumns: string[] = [
    "check","ucn","asset_name","amount","valuation_charge"
  ];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<InvoiceFeeDetailsComponent>, private valService: ValuationService, private notify:NotificationService) {
    this.ucnList = data.ucn;
    this.isCreditNote = data.isCreditNote;
    if(data.invoice){
      this.invoiceNumber = data.invoice;
    }
   }

  ngOnInit(): void {
    this.feeDetailsData = new MatTableDataSource();
    let payload = {
      "unique_control_number__in": this.ucnList.toString(),
      "limit":999
    }
    this.valService.invoiceFeeDetails(payload).subscribe((res:any)=>{
      this.feeDetails = res.results;
      this.feeDetailsData = this.feeDetails;
      this.total_count = res.results.length;
      console.log(res);
    })
  }

  close(){
    this.dialogRef.close()
  }
  changeFee(e,row){
    let index = this.feeDetails.indexOf(row);
      this.feeDetails[index].amount = e.target.value;
  }
  saveData(){
    console.log(this.feeDetails);
    this.valService.batchUpdateTax(this.feeDetails).subscribe((res:any)=>{
      console.log(res);
      if(this.isCreditNote){
        let payload={
          "ucns":this.ucnCreditList,
          "invoice":this.invoiceNumber
        }
        this.valService.generateCreditNote(payload).subscribe((res:any)=>{
          this.notify.success('Credit Note Generated Successfully');
          this.dialogRef.close();
        });
      }else{
        this.dialogRef.close();
      }
    })
  }
  checkFeeRow(e,row){
    if(e.checked){
      this.ucnCreditList.push(row.unique_control_number);
    }else{
      let index = this.ucnCreditList.indexOf(row.unique_control_number);
      this.ucnCreditList.splice(0,index);
    }
  }
  getDisplayColumns(){
    if(this.isCreditNote){
      return this.displayedCreditNoteColumns;
    }else{
      return this.displayedColumns;
    }
  }
}
