import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseInvoiceComponent } from './enterprise-invoice.component';

describe('EnterpriseInvoiceComponent', () => {
  let component: EnterpriseInvoiceComponent;
  let fixture: ComponentFixture<EnterpriseInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterpriseInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
