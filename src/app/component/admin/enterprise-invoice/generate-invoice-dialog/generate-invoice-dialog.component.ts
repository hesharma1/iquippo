import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ValuationService } from 'src/app/services/valuation.service';
import { MatTableDataSource } from '@angular/material/table';
import { CsvExportService } from 'src/app/services/csv-export.service';
@Component({
  selector: 'app-generate-invoice-dialog',
  templateUrl: './generate-invoice-dialog.component.html',
  styleUrls: ['./generate-invoice-dialog.component.css']
})
export class GenerateInvoiceDialogComponent implements OnInit {
  reportdata;
  public invoiceGeneratedData!: MatTableDataSource<any>;
  public total_count: any = 0;
  public displayedColumns: string[] = [
    "ucn","request_type","enterprise","no_of_records","actions"
  ];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<GenerateInvoiceDialogComponent>, private valService: ValuationService, private csvservice: CsvExportService) {
    this.reportdata = data;
   }

  ngOnInit(): void {
    this.invoiceGeneratedData = new MatTableDataSource();
    this.invoiceGeneratedData = this.reportdata;
    this.total_count = this.reportdata.length;
  }

  generate_annexure(row){
    let payload = {
      ucns: row.ucn
    }

    this.valService.generateAnnexure(payload).subscribe((res:any)=>{
      console.log(res);
      this.csvservice.downloadFile(res,'Annexure')
    })
  }

  generateInvoice(row){
    this.dialogRef.close(row);
  }

  close(){
    this.dialogRef.close();
  }

}
