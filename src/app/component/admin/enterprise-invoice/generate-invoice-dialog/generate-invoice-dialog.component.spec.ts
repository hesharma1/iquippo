import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateInvoiceDialogComponent } from './generate-invoice-dialog.component';

describe('GenerateInvoiceDialogComponent', () => {
  let component: GenerateInvoiceDialogComponent;
  let fixture: ComponentFixture<GenerateInvoiceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerateInvoiceDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateInvoiceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
