import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { MatTableDataSource } from '@angular/material/table';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { EmdMasterService } from './../buy-sell-process-master/components/emd-master/emd-master.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';
import { ValuationService } from 'src/app/services/valuation.service';

@Component({
  selector: 'app-valuation-settings',
  templateUrl: './valuation-settings.component.html',
  styleUrls: ['./valuation-settings.component.css']
})
export class ValuationSettingsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public valuationSettingsForm: any = this.formbuilder.group({
    valuationType: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
    is_registrable: new FormControl(''),
    asset_type: new FormControl(''),
    service: new FormControl(''),
  }); 
  public valuationType = '1';
  public valuationModel: any;
  public statusOptions: Array<any> = [
    { name: 'Insta Valuation', value: 1 },
    { name: 'Advanced Valuation', value: 2 }
  ];
  public isApprovedStatus: number = 2;
  public valuationDataSouce: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public editId;
  public searchText!: string;
  public total_count: any;
  public activePage: any = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public tblColumns = ["id", "asset_type", "category", "fee", "is_registrable", "Actions"];
  public tblColumnsAdv = ["id", "service", "fee", "Actions"];
  public showForm: boolean = false;
  public chargeFilter: any;
  public amountTypeFilter: any;
  categoryList: any = [];
  assetCategoryList: any = [];
  categoryData: any;
  categoryId: any;
  public InstaValuationListingData = [];
  public AdvValuationListingData = [];
  
  constructor(private formbuilder: FormBuilder, private adminMasterService: AdminMasterService, private apiPath: ApiRouteService, private emdMasterService: EmdMasterService, private notify: NotificationService, public productService: ProductlistService, public spinner: NgxSpinnerService,
    private commonService: CommonService,private valuationService: ValuationService) { }

  ngOnInit(): void {
    this.getCategoryMasters();
    this.getInstaValuationList();
    this.getAssetCategoryList();
    this.chargeStatusFilter('1');
  }

  getAssetCategoryList() {
    this.commonService.getAssetCategoryMaster('limit=999').subscribe(
      (res: any) => {
        this.assetCategoryList = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  getCategoryMasters() {
    this.commonService.getCategoryMaster('limit=999').subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  filterValuesCategory(search: any) {
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }

  onCategorySelect(categoryId) {
    if (categoryId.id != this.categoryId) {
      this.valuationSettingsForm.get('category')?.setValue(categoryId.display_name);
      this.categoryId = categoryId.id
    }else{
      this.valuationSettingsForm.get('category')?.setValue(categoryId.display_name);
    }

  }

  getColumns() {
    if(this.chargeFilter == '1'){
      return this.tblColumns;
    } else {
      return this.tblColumnsAdv;
    }
  }

  action(actionType: string, id: number) {
    if (actionType === 'Edit') // for group edit
    {
      if(this.chargeFilter == '1'){
      this.valuationService.getInstaValuationFee(id).subscribe(
        res => {
          if (res != null) {
            this.valuationModel = res as any;
            this.showForm = true;
            this.valuationSettingsForm.patchValue({
              category: this.valuationModel.category.display_name,
              amount: this.valuationModel.fee,
              is_registrable: this.valuationModel.is_registrable,
              valuationType: 1,
              asset_type: this.valuationModel.asset_type,
            });
            this.editId = this.valuationModel.id;
            this.categoryId = this.valuationModel.category.id;
          }
        });
      }else{
        this.valuationService.getAdvValuationFee(id).subscribe(
          res => {
            if (res != null) {
              this.valuationModel = res as any;
              this.showForm = true;
              this.valuationSettingsForm.patchValue({
                amount: this.valuationModel.fee,
                valuationType: 2,
                service:this.valuationModel.service
              });
              this.editId = this.valuationModel.id;
            }
          });
      }
    }
    else if (actionType === 'Delete') // for group delete
    {
      if(this.chargeFilter == '1'){
      this.valuationService.deleteInstaValSettings(id).subscribe(
        res => {
          if (res == null) {
            this.notify.success('Data Deleted Successfully.');
            //this.getGroupList('', '');
            this.getInstaValuationList();
            this.editId = '';
          }
        });
    }else{
      this.valuationService.deleteAdvValSettings(id).subscribe(
        res => {
          if (res == null) {
            this.notify.success('Data Deleted Successfully.');
            //this.getGroupList('', '');
            this.getAdvValuationList();
            this.editId = '';
          }
        });
    }
  }
  }

  /**search list */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      if(this.chargeFilter == '1'){
        this.getInstaValuationList();
      }else{
        this.getAdvValuationList();
      }
    }
  }

  saveData() {
    this.valuationSettingsForm.markAllAsTouched();
    if (this.valuationSettingsForm.valid) {
      const formData = this.valuationSettingsForm.value;
      if(formData.valuationType == '1'){
        this.valuationModel = new Object();
        this.valuationModel.category = this.categoryId;
        this.valuationModel.fee = formData.amount;
        this.valuationModel.is_registrable = formData.is_registrable ? true:false;
        this.valuationModel.asset_type = formData.asset_type 
        this.valuationService.saveInstaValCharges(this.valuationModel).subscribe(
          res => {
            if (res != null) {
              this.valuationModel = res;
              this.valuationSettingsForm.reset();
              this.notify.success('Data Saved Successfully.');
              this.chargeFilter = formData.valuationType;
              this.getInstaValuationList();
              this.editId = '';
              this.valuationType =  '1';
              this.showForm = false;
            }
          });
      }else{
        this.valuationModel = new Object();
        this.valuationModel.fee = formData.amount;
        this.valuationModel.service = formData.service;
        this.valuationService.saveAdvValCharges(this.valuationModel).subscribe(
          res => {
            if (res != null) {
              this.valuationModel = res;
              this.valuationSettingsForm.reset();
              this.notify.success('Data Saved Successfully.');
              this.chargeFilter = formData.valuationType;
              this.getAdvValuationList();
              this.editId = '';
              this.valuationType =  '2';
              this.showForm = false;
            }
          });
      }
      
    }
  }

  updateData() {
    this.valuationSettingsForm.markAllAsTouched();
    if (this.valuationSettingsForm.valid) {
      const formData = this.valuationSettingsForm.value;
      if(formData.valuationType == '1'){
      this.valuationModel = new Object();
      this.valuationModel.category = this.categoryId;
      this.valuationModel.fee = formData.amount;
      this.valuationModel.is_registrable = formData.is_registrable? true:false;
      this.valuationModel.asset_type = formData.asset_type 
      this.valuationService.updateInstaValCharges(this.valuationModel, this.editId).subscribe(
        res => {
          if (res != null) {
            this.valuationModel = res;
            this.valuationSettingsForm.reset();
            this.notify.success('Data Updated Successfully.');
            this.getInstaValuationList();
            this.editId = '';
            this.valuationType =  '1';
            this.showForm = false;
          }
        });
    }else{
      this.valuationModel = new Object();
      this.valuationModel.fee = formData.amount;
      this.valuationModel.service = formData.service;
      this.valuationService.updateAdvValCharges(this.valuationModel, this.editId).subscribe(
        res => {
          if (res != null) {
            this.valuationModel = res;
            this.valuationSettingsForm.reset();
            this.notify.success('Data Updated Successfully.');
            this.getAdvValuationList();
            this.editId = '';
            this.valuationType =  '2';
            this.showForm = false;
          }
        });
  }
}
  }

  getInstaValuationList() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.chargeFilter) {
      payload['type__in'] = this.chargeFilter;
    }
    if (this.amountTypeFilter) {
      payload['amount_type__in'] = this.amountTypeFilter;
    }
    this.valuationService.getInstaValuationFeeList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
       // this.valuationDataSouce.data = res.results;
         // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.valuationDataSouce.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.valuationDataSouce.data = [];
            this.InstaValuationListingData = [];
            this.InstaValuationListingData = res.results;
            this.valuationDataSouce.data = this.InstaValuationListingData;
          }else{
            this.InstaValuationListingData = this.InstaValuationListingData.concat(res.results);
            this.valuationDataSouce.data = this.InstaValuationListingData;
          }
        }
        this.total_count = res.count;
      },
      err => {
        console.log(err);
      }
    )
  }
  scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.valuationDataSouce.data.length) && (this.valuationDataSouce.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
       if(this.chargeFilter == '1'){
        this.getInstaValuationList();
      }else{
        this.getAdvValuationList();
      }
      }
    }
    }
  }
}
  getAdvValuationList() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.chargeFilter) {
      payload['type__in'] = this.chargeFilter;
    }
    if (this.amountTypeFilter) {
      payload['amount_type__in'] = this.amountTypeFilter;
    }
    this.valuationService.getAdvValuationFeeList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
      // Changes for Card layout on Mobile devices
      if(window.innerWidth > 768){
        this.valuationDataSouce.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.valuationDataSouce.data = [];
            this.AdvValuationListingData = [];
            this.AdvValuationListingData = res.results;
            this.valuationDataSouce.data = this.AdvValuationListingData;
          }else{
            this.AdvValuationListingData = this.AdvValuationListingData.concat(res.results);
            this.valuationDataSouce.data = this.AdvValuationListingData;
          }
        }
        this.total_count = res.count;
      },
      err => {
        console.log(err);
      }
    )
  }

  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    if(this.chargeFilter == '1'){
      this.getInstaValuationList();
    }else{
      this.getAdvValuationList();
    }
  }

  /**sort list  */
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    if(this.chargeFilter == '1'){
      this.getInstaValuationList();
    }else{
      this.getAdvValuationList();
    }
  }

  chargeStatusFilter(val) {
    this.chargeFilter = val;
    this.activePage = 1;
    if(val == '1'){
      this.valuationType =  '1';
      this.getInstaValuationList();
    }else{
      this.valuationType =  '2';
      this.getAdvValuationList();
    }
    
  }

  getElementService(service) {
    if (service == '3') {
      return "Inspection"
    } else if (service == '2') {
      return "Valuation (Includes Asset Inspection)"
    }
    return "";
  }

  amountStatusFilter(val) {
    this.amountTypeFilter = val;
    this.activePage = 1;
    this.getInstaValuationList();
  }

  export() {
    let payload = {
      limit: 999
    }
    this.valuationService.getInstaValuationFeeList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Asset Category": this.getAssetCategory(r?.asset_type),
            "Category": r?.category.display_name,
            "Fee": r?.fee,
            "Is Registrable": r?.is_registrable? "Yes":"No"
           
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Valuation Settings List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  closeForm() {
    this.showForm = false;
    this.editId = '';
    this.valuationSettingsForm.reset();
  }

  changeValuationType(val){
    if(val == '1'){
      this.valuationSettingsForm.get('asset_type')?.setValidators(Validators.required);
      this.valuationSettingsForm.get('asset_type')?.updateValueAndValidity();
      this.valuationSettingsForm.get('category')?.setValidators(Validators.required);
      this.valuationSettingsForm.get('category')?.updateValueAndValidity();
      this.valuationSettingsForm.get('amount')?.setValidators(Validators.required);
      this.valuationSettingsForm.get('amount')?.updateValueAndValidity();
      this.valuationSettingsForm.get('service')?.clearValidators();
      this.valuationSettingsForm.get('service')?.updateValueAndValidity();
    }else{
      this.valuationSettingsForm.get('asset_type')?.clearValidators();
      this.valuationSettingsForm.get('asset_type')?.updateValueAndValidity();
      this.valuationSettingsForm.get('category')?.clearValidators();
      this.valuationSettingsForm.get('category')?.updateValueAndValidity();
      this.valuationSettingsForm.get('amount')?.setValidators(Validators.required);
      this.valuationSettingsForm.get('amount')?.updateValueAndValidity();
      this.valuationSettingsForm.get('service')?.setValidators(Validators.required);
      this.valuationSettingsForm.get('service')?.updateValueAndValidity();
    }
  }

  getAssetCategory(data){
    let filtered = this.assetCategoryList.filter(
       item => item?.id  == data
    );
    return filtered[0]?.type;
  }

}

