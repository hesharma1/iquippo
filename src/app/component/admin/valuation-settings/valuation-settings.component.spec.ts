import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuationSettingsComponent } from './valuation-settings.component';

describe('ValuationSettingsComponent', () => {
  let component: ValuationSettingsComponent;
  let fixture: ComponentFixture<ValuationSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValuationSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuationSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
