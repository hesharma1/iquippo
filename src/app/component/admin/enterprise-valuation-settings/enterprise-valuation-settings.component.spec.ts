import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseValuationSettingsComponent } from './enterprise-valuation-settings.component';

describe('EnterpriseValuationSettingsComponent', () => {
  let component: EnterpriseValuationSettingsComponent;
  let fixture: ComponentFixture<EnterpriseValuationSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterpriseValuationSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseValuationSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
