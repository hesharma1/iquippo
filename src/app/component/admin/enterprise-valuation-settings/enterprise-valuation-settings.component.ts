import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { ViewReportComponent } from '../view-report/view-report.component';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-enterprise-valuation-settings',
  templateUrl: './enterprise-valuation-settings.component.html',
  styleUrls: ['./enterprise-valuation-settings.component.css']
})
export class EnterpriseValuationSettingsComponent implements OnInit {@ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
@ViewChild(MatSort, { static: true }) sort!: MatSort;

public searchText!: string;
public activePage: any = 1;
public total_count: any;
public dataSource!: MatTableDataSource<any>;
public ordering: string = "-unique_control_number"
public pageSize: number = 10;
public filter: any;
public showForm:boolean = false;
categoryList: any = [];
brandList: any = [];
modelList: any = [];
editId;
disableEnt:boolean =  false;
enterpriseCustomerList = [];
public displayedColumns: string[] = [
  "request_type_display","enterprise","effective_from","effective_to",
  "charge_basis_display","amount","category",
  "actions"
];
public statusOptions: Array<any> = [
  { name: 'Request in Draft', value: 1 },
  { name: 'Request Submitted', value: 2 },
  { name: 'Payment Done', value: 3 },
  { name: 'Payment Failed', value: 4 },
  { name: 'Report Generated', value: 5 }
];
public listingData = [];

constructor(private dialog: MatDialog,
  public spinner: NgxSpinnerService,
  public valService: ValuationService,
  public storage: StorageDataService, public notify: NotificationService,
  private formbuilder: FormBuilder,private commonService: CommonService,
  private datePipe: DatePipe,) {
}
public enterpriseValuationForm: any = this.formbuilder.group({
  request_type: new FormControl('', Validators.required),
  effective_from: new FormControl('', Validators.required),
  effective_to: new FormControl('', Validators.required),
  enterprise: new FormControl('', Validators.required),
  charge_basis: new FormControl('', Validators.required),
  category: new FormControl('', ),
  charge_type: new FormControl('',),
  brand: new FormControl('', ),
  model: new FormControl('',),
  amount: new FormControl('', [Validators.pattern('[1-9][0-9]+'),Validators.required])
}); 

/**ng on init */
ngOnInit(): void {
  this.dataSource = new MatTableDataSource();
  this.getPage();
  this.getCategoryMasters();
  this.getEnterpriseCustomer();
}
scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
       this.getPage();
        }
      }
    }
  }
}

/**search list  */
searchInList() {
  if (this.searchText === '' || this.searchText?.length > 3) {
    this.activePage = 1;
    this.getPage();
  }
}

statusFilter(val) {
  this.filter = val;
  this.activePage = 1;
  this.getPage();
}

/**get list from api */
getPage() {
  let payload = {
    page: this.activePage,
    limit: this.pageSize,
    ordering: this.ordering,
  };
  if (this.searchText) {
    payload['search'] = this.searchText;
  }
  if (this.filter) {
    payload['status__in'] = this.filter;
  }
  this.valService.getEnterpriseValSettings(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      if (res) {
        this.total_count = res.count;
        // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.dataSource.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.dataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.dataSource.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.dataSource.data = this.listingData;
          }
        }
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

/**on chnage page from paginations */
onPageChange(event: PageEvent) {
  this.activePage = event.pageIndex + 1;
  this.pageSize = event.pageSize;
  this.getPage();
}

onSortColumn(event) {
  this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPage();
}


/**Export Function */
export(string) {
  let payload = {
    limit: 999
  }
  this.valService.getEnterpriseValSettings(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "Request Type": r?.request_type_display,
          "Enterprise Name": r?.enterprise[0]?.company_name,
          "Effective From Date":r?.effective_from,
          "Effective To Date":r?.effective_to,
          "Charge Basis":r?.charge_basis_display,
          "Categories": this.getCategory(r),
          "Amount": r?.amount,
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Enterprise Val Settings");
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}
getCategoryMasters() {
  this.commonService.getCategoryMaster('limit=999').subscribe(
    (res: any) => {
      this.categoryList = res.results;
    },
    (err) => {
      console.error(err);
    }
  );
}
getEnterpriseCustomer(){
  let request = {
    partnership_type__in : 5,
    status__in : 2,
    limit: 999
  }
  this.valService.getEnterpriseCustomerList(request).subscribe((res:any)=>{
   this.enterpriseCustomerList = res.results;
  })
}
saveData(){
  if(!this.enterpriseValuationForm.valid){
    return;
  }else{
    let payload:any 
    payload ={
      request_type: this.enterpriseValuationForm.get('request_type').value,
  effective_from: this.datePipe.transform(this.enterpriseValuationForm.controls['effective_from'].value, 'yyyy-MM-dd'),
  effective_to: this.datePipe.transform(this.enterpriseValuationForm.controls['effective_to'].value, 'yyyy-MM-dd'),
  enterprise: this.enterpriseValuationForm.get('enterprise').value ? this.enterpriseValuationForm.get('enterprise').value: [],
  charge_basis: this.enterpriseValuationForm.get('charge_basis').value,
  category: this.enterpriseValuationForm.get('category').value ? this.enterpriseValuationForm.get('category').value : [],
  brand: this.enterpriseValuationForm.get('brand').value ? this.enterpriseValuationForm.get('brand').value : [],
  model: this.enterpriseValuationForm.get('model').value ? this.enterpriseValuationForm.get('model').value : [],
  amount: this.enterpriseValuationForm.get('amount').value
    }
    if(this.enterpriseValuationForm.get('charge_type').value){
      payload.charge_type = this.enterpriseValuationForm.get('charge_type').value 
    }
    if(!this.editId){
      this.valService.postEnterpriseValSettings(payload).subscribe(res=>{
        console.log(res);
        this.showForm = false;
        this.enterpriseValuationForm.reset();
        this.getPage();
        this.disableEnt = false;
       })
     }else{
      this.valService.putEnterpriseValSettings(this.editId,payload).subscribe(res=>{
        console.log(res);
        this.showForm = false;
        this.enterpriseValuationForm.reset();
        this.getPage();
        this.disableEnt = false;
       })
     }
    }
    
}
action(actionType: string, id: number) {
    if (actionType === 'Edit') // for group edit
    {
      this.valService.getEnterpriseValSettingsById(id).subscribe(
        (res:any) => {
          if (res != null) {
            this.showForm = true;
            let category:any = [];
            for(let cat of res.category){
              category.push(cat.id);
            }
            this.enterpriseValuationForm.patchValue({
              request_type: res.request_type,
              effective_from: res.effective_from,
              effective_to: res.effective_to,
              
              charge_basis: res.charge_basis,
              category: category,
              brand: res.brand,
              model: res.model,
              amount: res.amount
            });
            let entId: Array<any> = [];
            for(let ent of res.enterprise){
              entId.push(ent?.id)
            }
            this.enterpriseValuationForm.patchValue({
              enterprise: entId
            })
            this.disableEnt = true;
            this.editId = res.id;
          }
        });
    }
    else if (actionType === 'Delete') // for group delete
    {
      this.valService.deleteEnterpriseValSettings(id).subscribe(
        res => {
          if (res == null) {
            this.notify.success('Data Deleted Successfully.');
            //this.getGroupList('', '');
            this.activePage = 1;
            this.getPage();
            this.editId = '';
            this.disableEnt = false;
          }
        });
  }
  }
  getCategory(r){
    if(r.category && r.category?.length >0){
      let index = 0;
      let catString = '';
      for(let cat of r.category){
        if(index !=0){
          catString = catString + ',';
        }
        catString = catString + cat.display_name;
        index++;
      }
      return catString;
    }
    return 'All';
  }
  closeForm(){
    this.editId = '';
    this.showForm = false;
    this.disableEnt =  false;
    this.enterpriseValuationForm.reset();
  }
  getCompanyName(enterprise){
    let string= '';
    for(let ent of enterprise){
      if(string){
        string = string + " , "
      }
      string = string + ent?.company_name;
    }
    return string;
  }
  addValidations(val){
    this.enterpriseValuationForm?.get('category')?.setValue('');
    this.enterpriseValuationForm?.get('amount')?.setValue('');
  }
}
