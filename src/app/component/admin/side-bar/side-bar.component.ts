import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AppRouteEnum } from 'src/app/utility/app-constants.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  public storageSub = new Subject<string>();
  cognitoId;
  sidebartgl: boolean = true;
  wrappertgl: boolean = true;
  isLoggedIn: boolean = false;
  userFirstName?: string;
  userLastName?: string;
  userdata: any;
  mp1HomePage?: string;
  addClass1: boolean = false;
  addClass2: boolean = false;
  addClass3: boolean = false;
  addClass4: boolean = false;
  addClass5: boolean = false;
  addClass6: boolean = false;
  addClass7: boolean = false;
  addClass8: boolean = false;
  addClass9: boolean = false;
  addClass10: boolean = false;
  addClass11: boolean = false;
  addClass12: boolean = false;
  addClass13: boolean = false;
  role: any;
  userRole: any;
  rolesArray;
  adminMenu = [
    "Product",
    "Equipment Masters",
    "New Equipment Lead",
    "Bull/Sell Process Master"
  ];
  superAdminMenu = [
    "Product",
    "Equipment Masters",
    "New Equipment Lead",
    "Bull/Sell Process Master"
  ];
  partnerAllDetail: any;
  partnerContactId: any;
  permissionData: any;
  productLength: any;
  isShowProductHeader = true;
  isShowPartnerHeader = true;
  isShowValuationHeader = true;
  isShowBuyAndSellHeader = true;
  isShowMasterSettingHeader = true;
  isShowEquipmentMaster = true;
  partnerLength: any;
  valuationLength: any;
  buySellLength: any;
  masterSettingLength: any;
  EquipmentmasterLength: any;
  constructor(private cookieServ: CookieService, public awsCognito: AwsAuthService,
    public router: Router, private sharedService: SharedService, private storage: StorageDataService,
    public appRouteEnum: AppRouteEnum, public apiRouteService: ApiRouteService, private commonService: CommonService,) {
    this.mp1HomePage = environment.mp1HomePage;
  }

  ngOnInit(): void {
    this.role = this.storage.getStorageData("role", false);
    this.role = this.apiRouteService.roles.superadmin;
    // this.commonService.sendData.subscribe((data) => {
    //   localStorage.setItem("userRole", data);
    // })
    this.rolesArray = this.storage.getStorageData("rolesArray", false);
    this.rolesArray = this.rolesArray.split(',');
    if (localStorage.getItem("userRole")) {
      // if (this.rolesArray.includes('Customer')) {
      //   this.userRole = this.apiRouteService.roles.customer;
      //   localStorage.setItem("userRole", this.userRole);
      // }

      this.userRole = localStorage.getItem("userRole");
      // if (this.userRole == this.apiRouteService.roles.superadmin || this.userRole == this.apiRouteService.roles.admin) {
      //   this.router.navigate(['/admin-dashboard'])
      // }
    }
    if (this.userRole == this.apiRouteService.roles.admin) {
      localStorage.setItem("userRole", this.userRole);
      this.getUserRoles();
    }
    else if (this.userRole == this.apiRouteService.roles.rm) {
      localStorage.setItem("userRole", this.userRole);
      this.getUserRoles();
      this.getPartnerAllInfo();
    }

  }
  agreementLink() {
    if (this.userRole == this.apiRouteService.roles.rm) {
      this.router.navigate(['/admin-dashboard/client-management/agreement'], { queryParams: { isrm: this.partnerContactId } });
    }
    else {
      this.router.navigate(['/admin-dashboard/client-management/agreement']);
    }
  }
  redirection() {
    if (this.userRole == this.apiRouteService.roles.rm) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.router.navigate([`/admin-dashboard/user-management-new`]);
    }
    else if (this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.superadmin) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.router.navigate([`/admin-dashboard/user-management-new`]);
    }
  }
  getPartnerAllInfo() {
    this.sharedService.getAllPartnerInfo().pipe(finalize(() => { })).subscribe(res => {
      console.log(res);
      this.partnerAllDetail = res;
      this.partnerContactId = this.partnerAllDetail?.partner_contact_entity?.id;
    })
  }
  showsubmenu1() {
    this.addClass1 = !this.addClass1;
  }
  showsubmenu2() {
    this.addClass2 = !this.addClass2;
  }
  showsubmenu3() {
    this.addClass3 = !this.addClass3;
  }
  showsubmenu4() {
    this.addClass4 = !this.addClass4;
  }
  showsubmenu5() {
    this.addClass5 = !this.addClass5;
  }
  showsubmenu6() {
    this.addClass6 = !this.addClass6;
  }
  showsubmenu7() {
    this.addClass7 = !this.addClass7;
  }
  showsubmenu8() {
    this.addClass8 = !this.addClass8;
  }
  showsubmenu9() {
    this.addClass9 = !this.addClass9;
  }
  showsubmenu10() {
    this.addClass10 = !this.addClass10;
  }
  showsubmenu11() {
    this.addClass11 = !this.addClass11;
  }
  showsubmenu12() {
    this.addClass12 = !this.addClass12;
  }
  redirecttologin() {
    this.router.navigate([`./` + this.appRouteEnum.Login]);
  }
  showsubmenu13() {
    this.addClass13 = !this.addClass13;
  }
  logout() {
    this.sharedService.logout();
    // this.awsCognito.awsSignOut().then(() => {
    //   // console.log('qsqw');
    //   // this.navCtrl.setDirection('root');
    //   this.storage.cleanAll();
    //   this.sharedService.removeCookie(this.appRouteEnum.globalCookieName);
    //   //this.router.navigateByUrl(this.appRouteEnum.Login);
    //   window.location.href = environment.mp1LogoutUrl;
    // }).catch(e => {
    //   console.log('getting error');
    // });
  }
  sidebartoggle() {
    this.sidebartgl = !this.sidebartgl;
    this.wrappertgl = !this.wrappertgl;
  }
  backTohome() {
    this.sharedService.redirecteToMP1AfterLogin();
  }
  isAccesible(option: string) {
    if (this.role == this.apiRouteService.roles.superadmin) {
      return true;
    }
    if (this.role == this.apiRouteService.roles.admin) {
      if (this.adminMenu.includes(option)) {
        return true;
      }
    }
    return false;
  }
  getUserRoles() {
    if (this.userRole == this.apiRouteService.roles.admin || this.userRole == this.apiRouteService.roles.rm || this.userRole == this.apiRouteService.roles.superadmin) {
      this.cognitoId = this.storage.getStorageData("cognitoId", false);
      this.commonService.getRolesByUser(this.cognitoId).subscribe(res => {
        this.storage.setStorageData("UserInfo", res, true);
        this.permissionData  = this.storage.getStorageData("UserInfo", true);
        this.commonService.shareData(res);
        // this.router.navigate(['/admin-dashboard']);

      })
    }
  }

  redirectToPage() {
    this.cognitoId = this.storage.getStorageData("cognitoId", false);
    this.storage.setStorageData('partnerDealerUserId', this.cognitoId, true)
    this.router.navigate([`./admin-dashboard/partner-dealer/basic-details`]);
  }

  switchRole(role) {
    this.userRole = role;
    localStorage.removeItem("userRole");
    if (this.userRole == this.apiRouteService.roles.superadmin) {
      localStorage.setItem("userRole", role);
      this.getUserRoles();
      //localStorage.removeItem("UserInfo");
      //window.open('/admin-dashboard', '_self');

    } else if (this.userRole == this.apiRouteService.roles.admin) {
      localStorage.setItem("userRole", role);
      this.getUserRoles();
      // window.open('/admin-dashboard', '_self');
    }
    else if (this.userRole == this.apiRouteService.roles.rm) {
      localStorage.setItem("userRole", role);
      this.getUserRoles();
      // window.open('/admin-dashboard', '_self');

    }
    else {
      localStorage.setItem("userRole", role);
      localStorage.removeItem("UserInfo");
      this.router.navigate(['/customer/dashboard/']);
    }
  }


}