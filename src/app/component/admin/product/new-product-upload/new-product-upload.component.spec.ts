import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductUploadComponent } from './new-product-upload.component';

describe('NewProductUploadComponent', () => {
  let component: NewProductUploadComponent;
  let fixture: ComponentFixture<NewProductUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
