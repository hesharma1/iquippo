import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FieldConfig } from 'src/app/shared/abstractions/field.interface';
import {
  FormControl,
  FormArray,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { NewEquipmentOfferMaster } from '../../../../models/common/new-product-upload.model';
import { environment } from 'src/environments/environment';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { StorageDataService } from './../../../../services/storage-data.service';
import { NotificationService } from './../../../../utility/toastr-notification/toastr-notification.service';
import { AdminNewProductUpload } from './../../../../models/common/new-product-upload.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResponseData } from 'src/app/models/common/responsedata.model';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { CommonService } from 'src/app/services/common.service';
import { SelectionModel } from '@angular/cdk/collections';
import { visualData } from 'src/app/models/sellEquipment.model';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { Router, ActivatedRoute } from '@angular/router';
import { visualDataNewEquipment } from 'src/app/models/sellEquipment.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgreementServiceService } from './../../../../services/agreement-service.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { DatePipe } from '@angular/common';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { Moment } from 'moment';
const moment = _moment;
import { finalize } from 'rxjs/operators';
import { Observable } from "rxjs";
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { PartnerDealerRegistrationService } from './../../../../services/partner-dealer-registration-service.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};
@Component({
  selector: 'app-new-product-upload',
  templateUrl: './new-product-upload.component.html',
  styleUrls: ['./new-product-upload.component.css'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },
  //   { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  // ],
})
export class NewProductUploadComponent implements OnInit, OnDestroy {
  @ViewChild('imageInput') imageInput!: ElementRef;
  addNewAsset = false;
  visuals: any;
  userRole: string = "";
  yearOfManufacture: any = [];
  stateDataSource: any;
  isShowOfferTypes = false;
  isOfferCash = false;
  isOfferLease = false;
  isOfferFinance = false;
  isSubmitType = false;
  isTechfieldShow = false;
  documentNameTc: Array<any> = [];
  documentNameOffers: Array<any> = [];
  newEquipmentOfferMaster: any;
  adminNewProductUpload: any;
  public object: any = {};
  offerMaster: any;
  newProductUpload: FormGroup;
  maxOffersArrayLength = 5;
  cognitoId: any;
  sellerList: any = [];
  categoryId: any;
  brandId: any;
  offersMasterResponce: any;
  modalId: any;
  updateOffermaster: any;
  categoryData: any;
  brandData: any;
  modalData: any;
  modelList: any;
  viewingOptionsForm!: FormGroup;
  tabsList: any;
  brandList: any;
  categoryList: any;
  todayDate = new Date();
  docType: { [key: string]: number } = {
    IMAGE: 1,
    VIDEO_LINK: 2,
    VIDEO_UPLOAD: 3,
  };
  selectedCertificate: any = [];
  uploadImagesPath: Array<object> = [];
  uploadVideosPath: Array<object> = [];
  uploadVideoAndImages: Array<object> = [];
  videoLinkPath: Array<object> = [];
  uploadedImagesPath: Array<object> = [];
  uploadingImage: boolean = false;
  isInvalidfile: boolean = false;
  public arrayItems: any = [];
  stateList: any;
  editableRowData: any;
  isEdit: boolean = false;
  isApproved: boolean = false;
  countryDataSource: any;
  cityDataSource: any;
  selection = new SelectionModel<any>(false, []);
  documentName: Array<object> = [];
  videoName: Array<object> = [];
  public offersForm: FormGroup = new FormGroup({
    offersArray: new FormArray([this.createOffer()]),
  });
  maxYear: any;
  offersArray = this.offersForm.get('offersArray') as FormArray;
  years: any = ['2020', '2021', '2022', '2023', '2024', '2025'];
  imageName: any;
  UspAddArray: any;
  assetVisualData: any;
  partnerContactList: any;
  public demoForm!: FormGroup;
  sellerTypeValue: any = 2;
  sellerTypeArray: Array<any> = [
    { typeName: 'Dealer', value: 2 },
    { typeName: 'Manufacturer', value: 3 }
  ];
  public relationshipManager: Array<any> = [
  ];
  certificateList;
  assetId: any;
  addVisual: boolean = false;
  isMobileInvalid = false;

  constructor(
    public s3: S3UploadDownloadService,
    private storage: StorageDataService,
    private notify: NotificationService,
    private spinner: NgxSpinnerService,
    private adminMasterService: AdminMasterService,
    private productUpload: NewEquipmentService,
    private commonService: CommonService,
    private fb: FormBuilder,
    private notificationservice: NotificationService,
    private usedEquipmentService: UsedEquipmentService,
    public newEquipmentService: NewEquipmentService,
    private agreementServiceService: AgreementServiceService,
    private router: Router, private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private sharedService: SharedService,
    private datePipe: DatePipe,
    private partnerDealerRegistrationService: PartnerDealerRegistrationService
  ) {
    this.newProductUpload = this.fb.group({
      seller_type: new FormControl('', Validators.required),
      seller: new FormControl(''),
      firstname: new FormControl(''),
      // individual: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.required),
      email: new FormControl(''),
      yearOfManufacture: new FormControl('', Validators.required),
      isPriceOnRequest: new FormControl('', Validators.required),
      showroomPrice: new FormControl(''),
      // indicativePrice: new FormControl(''),
      brochure: new FormControl(''),
      // video: new FormControl(''),
      // youtubeLink: new FormControl('', Validators.pattern(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/)),
      category: new FormControl('', Validators.required),
      otherCategory: new FormControl(''),
      brand: new FormControl('', Validators.required),
      otherBrand: new FormControl(''),
      otherModel: new FormControl(''),
      modal: new FormControl('', Validators.required),
      // Sellerlocation: new FormControl('', Validators.required),
      certificate: new FormControl(''),
      isActive: new FormControl(''),
      //rateEquipment: new FormControl('', Validators.required),
      Prdoduct_name: new FormControl(''),
      // SellPrice: new FormControl('', Validators.required),
      enginePower: new FormControl(''),
      grossWeight: new FormControl(''),
      operatingWeight: new FormControl(''),
      bucketCapcity: new FormControl(''),
      liftingCapcity: new FormControl(''),
      uspAddArray: new FormArray([]),
      fuelType: new FormControl(''),
      relationship_manager: new FormControl(''),
      //visuals: new FormArray([this.createVisualArray()]),
    }, {
      validator: [numberonly('grossWeight'), numberonly('enginePower'), numberonly('operatingWeight'),
      numberonly('bucketCapcity'), numberonly('liftingCapcity')]
    });
    this.demoForm = this.fb.group({
      demoArray: this.fb.array([])
    });
    let currentDate = new Date();
    for (let i = 0; i < 4; i++) {
      this.yearOfManufacture.push((currentDate.getFullYear() - i).toString());
    }
    this.route.queryParams.subscribe((data) => {
      this.assetId = data.assetId;

    })
    //this.visuals = this.newProductUpload.get('visuals') as FormArray;
    this.getDetails();

    //this.newUspData();
  }

  getDetails() {
    this.editableRowData = localStorage.getItem('newProductRecordById');
    if (!this.editableRowData && this.assetId) {
      this.productUpload.getBasicDetailList(this.assetId).subscribe(
        (res: any) => {
          this.editableRowData = JSON.stringify(res);
          this.renderCountryData();//country code
          // if (this.editableRowData != null && this.editableRowData != undefined) {
          //   this.isEdit = true;
          //   this.sellerTypeValue = this.editableRowData.seller_type;
          //   this.assetId = this.editableRowData?.id;
          //   // this.getVisualsDataById();
          //   if (this.editableRowData.status == 2 || this.editableRowData.status == 3) {
          //     this.isApproved = true;
          //   }
          //   this.addVisual = true;
          //   this.setFormValue(this.editableRowData);
          //   this.getDocuments();
          //   //this.checkBidAvailable();

          // }

        });
    } else {
      this.renderCountryData();//country code
    }
    this.UspAddArray = <FormArray>(
      this.newProductUpload.controls['uspAddArray']
    );

  }

  newUspData() {
    const TaxArray = this.newProductUpload?.get(
      'uspAddArray'
    ) as FormArray;
    this.UspAddArray.push(this.createUspFormGroup());
  }

  removeUspFormGroup(id: any) {
    var i = '' + id + '';
    this.UspAddArray.removeAt(id);
  }
  get demoArray() {
    return this.demoForm.get("demoArray") as FormArray;
  }

  getCertificateMasterList() {
    let payload: any = {
      limit: 999
    }
    this.adminMasterService.getCertificateMasterList(payload).subscribe((res: any) => {
      this.certificateList = res.results;
    })
  }
  createUspFormGroup(uspdata?: any): FormGroup {
    if (uspdata == undefined || uspdata == null || uspdata == '') {
      return this.fb.group({
        id: new FormControl(''),
        Usp: new FormControl(''),
      });
    } else {
      return this.fb.group({
        id: [uspdata.id],
        Usp: [uspdata.val],
      })
    }
  }
  // bindUspDetails() {
  //   let object:any = {};
  //   this.UspAddArray = <FormArray>(this.newProductUpload.controls['uspAddArray']);
  //   // this.UspAddArray.value.forEach(element => {
  //   //   object = {
  //   //     usp:element.Usp,
  //   //     new_equipment: this.customerId,
  //   //   }
  //     this.brandLocation = this.UspAddArray;
  //   // });
  // }

  setFormValue(dataObject: any) {
    let name = dataObject.seller.first_name + " " + dataObject.seller.last_name;
    this.cognitoId = dataObject.seller.cognito_id;
    if (dataObject?.video_link != null && dataObject?.video_link != '' && dataObject?.video_link != undefined) {
      this.videoName.push({ name: dataObject?.video_link, url: dataObject?.video_link });
    }
    this.imageName = dataObject?.document;
    let category = dataObject.category != "" && dataObject.category != null ? dataObject.category : "";
    this.onCategorySelect(category);
    let brand = dataObject.brand != "" && dataObject.brand != null ? dataObject.brand : "";
    this.onBrandSelect(brand);
    let modal = dataObject.model != "" && dataObject.model != null ? dataObject.model : "";
    this.onModalSelect(modal);
    this.newProductUpload.patchValue({
      // individual: dataObject.seller_type != "" && dataObject.seller_type != null ? dataObject.seller_type.toString() : "",
      seller_type: this.sellerTypeValue,
      firstname: name,
      mobile: dataObject.seller.mobile_number,
      email: dataObject.seller.email,
      isd_code: dataObject.seller.pin_code.city.state.country.isd_code,
      alternate_email: dataObject.seller.alt_email_address,
      category: dataObject.category.id != "" && dataObject.category.id != null ? dataObject.category.name : "",
      brand: dataObject.brand.id != "" && dataObject.brand.id != null ? dataObject.brand.name : "",
      modal: dataObject.model.id != "" && dataObject.model.id != null ? dataObject.model.name : "",
      asset_address: dataObject.asset_address,
      is_rc_available: dataObject.is_rc_available ? "true" : "false",
      is_first_owned: dataObject.is_first_owned ? "1" : "2",
      yearOfManufacture: dataObject.mfg_year != "" && dataObject.mfg_year != null ? dataObject.mfg_year.toString() : "2021",
      customer_reference_number: dataObject.customer_reference_number,
      // Sellerlocation: dataObject.seller.pin_code.pin_code,
      country: dataObject.seller.pin_code.city.state.country.isd_code,
      alt_contact_number: dataObject.alt_contact_number,
      email_address: dataObject.email_address,
      alt_email_address: dataObject.alt_email_address,
      asset_description: dataObject.asset_description,
      rateEquipment: dataObject.rate_equipment != null && dataObject.rate_equipment != "" ? dataObject.rate_equipment.toString() : "",
      motor_operating_hours: dataObject.motor_operating_hours,
      mileage: dataObject.mileage,
      registrationNumber: dataObject.rc_number,
      chassisNumber: dataObject.chassis_number,
      engineNumber: dataObject.engine_number,
      machineSerialNumber: dataObject.machine_sr_number,
      grossWeight: dataObject.gross_Weight,
      enginePower: dataObject.engine_power,
      operatingWeight: dataObject.operating_weight,
      bucketCapcity: dataObject.bucket_capacity,
      liftingCapcity: dataObject.lifting_capacity,
      isParkingCharges: dataObject.is_parking_charges_info ? "1" : "0",
      parkedSince: dataObject.parked_since,
      parkingChargePerDay: dataObject.parking_charge_per_day,
      Prdoduct_name: dataObject.product_name,
      featured: dataObject.is_featured ? "1" : "0",
      isActive: dataObject.is_active ? "1" : "0",
      certificate: dataObject.certificate != "" && dataObject.certificate != null ? dataObject.certificate : "",
      // indicativePrice: dataObject.indicative_price,
      // youtubeLink: dataObject.youtube_link,
      //SellPrice: dataObject.selling_price,
      fuelType: dataObject?.fuel_type,
      otherCategory: dataObject.other_category,
      otherBrand: dataObject.other_brand,
      otherModel: dataObject.other_model,
      relationship_manager: dataObject.relationship_manager,
      isPriceOnRequest: dataObject?.is_price_on_request ? '1' : '0',
      showroomPrice: dataObject.selling_price ? dataObject.selling_price : '',
    })
    this.newProductUpload.get('mobile')?.disable();
    this.newProductUpload.get('email')?.disable();
    this.newProductUpload.get('name')?.disable();
    this.newProductUpload.get('seller_type')?.disable();
    this.updatePriceValidation();
    if (dataObject.usp.length > 0) {
      dataObject.usp.forEach((cust) => {
        this.UspAddArray.push(this.createUspFormGroup(cust));
        //console.log(this.customersControls);
      });
    } else {
      this.newUspData();
    }

    if (dataObject?.video_link)
      this.videoName.push({ name: dataObject?.video_link, url: dataObject?.video_link });
    if (dataObject?.tech_fields?.length > 0) {
      //this.isTechfieldShow = true;
      dataObject.tech_fields.map((field: any) => {
        this.arrayItems.push({ title: field.field_name });
        this.demoArray.push(this.fb.control(field.field_value));
      });
    }
    //else {
    //this.isTechfieldShow = false;

    //}
    if (dataObject?.document)
      this.documentName.push({ name: dataObject?.document, url: dataObject?.document });
    this.categoryId = dataObject.category.id;
    this.brandId = dataObject.brand.id;
    this.modalId = dataObject.model.id;
    // if (dataObject?.mfg_year) {
    //   let date = new Date();
    //   date.setFullYear(dataObject?.mfg_year);
    //   this.yearOfManufacture = date;
    //   this.newProductUpload.get('yearOfManufacture')?.setValue(date);
    // } else {
    //   let date = new Date();
    //   date.setFullYear(2021);
    //   this.yearOfManufacture = date;
    //   this.newProductUpload.get('yearOfManufacture')?.setValue(date);
    // }
  }

  getTechSpecifications() {
    let payload = {
      category: this.categoryId,
      brand: this.brandId,
      model: this.modalId
    }
    this.adminMasterService.getTechSpec(payload).subscribe((res: any) => {
      if (res.dynamic_specs.length > 0) {
        this.isTechfieldShow = true;
        res.dynamic_specs.map((field: any) => {
          this.arrayItems.push({ title: field.field_name });
          this.demoArray.push(this.fb.control(field.field_value));
        });
      } else {
        this.isTechfieldShow = false;

      }
    });

  }

  approve() {
    let request: any = {};
    request.status = 2;
    request.category = this.editableRowData.category.id;
    request.brand = this.editableRowData.brand.id;
    request.model = this.editableRowData.model.id;
    request.location = this.editableRowData.location.id;
    request.seller = this.editableRowData.seller.cognito_id;
    this.productUpload.patchNewEquipmentDetails(request, this.editableRowData.id).subscribe(res => {
      this.isApproved = true;
      this.editableRowData = res;
      localStorage.setItem('newProductRecordById', JSON.stringify(this.editableRowData));
      //this.router.navigate(['/admin-dashboard/products/new-products']);
    })
  }

  getStatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    } else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    } else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  reject() {
    let request: any = {};
    request.status = 4;
    request.category = this.editableRowData.category.id;
    request.brand = this.editableRowData.brand.id;
    request.model = this.editableRowData.model.id;
    request.location = this.editableRowData.location.id;
    request.seller = this.editableRowData.seller.cognito_id;
    this.productUpload.patchNewEquipmentDetails(request, this.editableRowData.id).subscribe(res => {
      console.log(res);
      this.isApproved = true;
      this.editableRowData = res;
      localStorage.setItem('newProductRecordById', JSON.stringify(this.editableRowData));

    })
  }

  regConfig: FieldConfig[] = [
    {
      type: 'select',
      label: 'Product Condition',
      inputType: '',
      name: 'productCondition',
      options: ['Good', 'Bad'],
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Name Required',
        },
      ],
    },
    {
      type: 'input',
      label: 'Motor Operating Hours',
      inputType: 'text',
      name: 'motorOperatingHours',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Motor Operating Hours is required',
        },
      ],
    },
    {
      type: 'input',
      label: 'Mileage',
      inputType: 'text',
      name: 'mileage',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Password Required',
        },
      ],
    },
    {
      type: 'radiobutton',
      label:
        'Do you want to get inspection and valuation done by iQuippo team?',
      name: 'inspectionYN',
      options: ['Yes', 'No'],
      value: '',
    },
  ];
  visualsData = {
    visuals: [
      {
        name: 'General Appearance Visuals',
        images: [
          {
            id: '',
            isPrimary: '',
          },
        ],
      },
      {
        name: 'Cabin Photo',
        images: [
          {
            id: '',
            isPrimary: '',
          },
        ],
      },
    ],
    videos: [
      {
        videoUrl: '',
        video: '',
      },
    ],
    roleId: '',
    cognitoId: '',
  };

  ngOnInit() {
    this.userRole = this.storage.getStorageData('userRole', false);
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getCategoryMasters();
    this.getCertificateMasterList();
    //this.cognitoId = this.storage.getStorageData('cognitoId', false);

    this.viewingOptionsForm = this.fb.group({
      imageData: this.fb.array([]),
      videosData: this.fb.array([this.newVideo()]),
    });
    this.getTabsList(); // call getTabsList to get all tabs
    let currentDate = new Date();
    this.maxYear = new Date().toJSON().split('T')[0];
    this.usedEquipmentService.getRM().subscribe((res: any) => {
      this.relationshipManager = res.results;
    })
  }
  /**
   * Get video data array
   */
  get imageFormGroup() {
    return this.viewingOptionsForm?.get('imageData') as FormArray;
  }
  get videosFormGroup() {
    return this.viewingOptionsForm?.get('videosData') as FormArray;
  }
  get sourceVideoFormGroup() {
    return this.videosFormGroup.controls;
  }
  get sourceImgFormGroup() {
    return this.imageFormGroup.controls;
  }
  sourceImgFormGroup1(index) {
    return this.sourceImgFormGroup[index].get('images') as FormArray;
  }

  videosData(): FormArray {
    return this.viewingOptionsForm?.get('videosData') as FormArray;
  }
  sourceImgFormArr(index) {
    return this.sourceImgFormGroup[index].get('images') as FormArray;
  }

  /**
   * New video for group
   */
  newImage(tab): FormGroup {
    return this.fb.group({
      id: [tab.id],
      label: [tab.category_name],
      imageUrl: [''],
      images: this.fb.array([]),
    });
  }
  /**
   * New video for group
   */
  imagesGroup(data: any): FormGroup {
    let isPrimary = data.is_primary ? true : false;
    return this.fb.group({
      image_category: [data.image_category],
      is_primary: [isPrimary],
      url: [data.url],
      visual_type: [data.visual_type],
      img_path: [data.img]
    });
  }


  /**
   * New video for group 
   */
  newVideo(data?: any): FormGroup {
    return this.fb.group({
      // videoLink: [data ? data.videoLink : ''],
      videoFile: [''],
      url: [data ? data.url : ''],
    })
  }



  /**
   * Add new video existing video data
   */
  addNewVideo() {
    this.videosFormGroup.push(this.newVideo());
  }
  /**
   * Remove video existing video data
   */
  removeVideo(index: number) {
    this.videosFormGroup.removeAt(index);
  }


  newQuantity(): FormGroup {
    return this.fb.group({
      videoLink: '',
      videoFile: '',
    });
  }



  async getVisualsDataById() {
    let rowId = this.editableRowData.id;
    await this.newEquipmentService.getNewVisualsByEquipmentId(rowId).subscribe(async (res: any) => {
      var imageData = res.results;
      var image: any;
      var dataArray: any = [];
      var videoData: any = [];
      var videosLink: any = [];
      for (var i = 0; i < imageData.length; i++) {
        if (imageData[i].visual_type == 2) {
          let obj = {
            videoLink: imageData[i].url,
            videoFile: "",
            visual_type: imageData[i].visual_type,
            image_category: imageData[i].image_category,
            used_equipment: imageData[i].used_equipment
          }
          // videoData.videoLink = imageData[i].url,
          // videoData.videoFile ="",
          // videoData.visual_type = imageData[i].visual_type,
          // videoData.image_category = imageData[i].image_category,
          // videoData.used_equipment = imageData[i].used_equipment
          videoData.push(obj);
        }
        else if (imageData[i].visual_type == 3) {
          let obj = {
            videoFile: imageData[i].url,
            videoLink: "",
            visual_type: imageData[i].visual_type,
            image_category: imageData[i].image_category,
            used_equipment: imageData[i].used_equipment
          }
          videoData.push(obj);

          // videoData.videoFile = imageData[i].url,
          // videoData.videoLink = "",
          // videoData.visual_type = imageData[i].visual_type,
          // videoData.image_category = imageData[i].image_category,
          // videoData.used_equipment = imageData[i].used_equipment
        }
        else {
          let obj = {
            url: imageData[i].url,
            visual_type: imageData[i].visual_type,
            image_category: imageData[i].image_category,
            is_primary: imageData[i].is_primary,
            img: await this.sharedService.viewImage(imageData[i].url)
          }
          dataArray.push(obj);
        }
      }
      this.setValueAtEdit(dataArray, videoData);
    })
  }


  setVisualData(data) {
    let videoObj = data.find(v => { return v.visual_type == 3 });
    let existingVideoObj = this.videosFormGroup.value[this.videosFormGroup.value.length - 1];
    if (videoObj && !existingVideoObj.url) {
      this.videosFormGroup.removeAt(this.videosFormGroup.value.length - 1);
    }
    data.forEach(element => {
      if (element.visual_type == 1) {
        let visualTab: any = this.sourceImgFormGroup.find((fg) => {
          return fg.value.id == element.image_category;
        })
        visualTab.get('images').push(this.imagesGroup(element));
      } else {
        if (element.visual_type == 3) {
          if (element?.url) {
            this.videosFormGroup.push(this.newVideo(element));
          }
        }
      }
    });
  }

  delete(file, index: number, imgIndex?: any) {
    let imgPath = file?.url.slice(file?.url.indexOf(environment.bucket_parent_folder));
    // let visualId = this.assetVisualData.find(f => { return file.url == f.url })?.id;
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.newEquipmentService.deleteVisual(file.id).pipe(finalize(() => { })).subscribe(
          res => {
            this.notificationservice.success('File Successfully deleted!!')
            if (file.visual_type == 1) {
              this.sourceImgFormGroup1(index).removeAt(imgIndex);
            } else {
              this.videosFormGroup.removeAt(index);
              if (index == 0) {
                this.addNewVideo();
              }
            }
          },
          err => {
            console.log(err);
            this.notificationservice.error(err.message);
          }
        )
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }

  setValueAtEdit(dataArray, videoData) {
    // if(dataArray.length > 0){
    //     dataArray.forEach(element => {
    //         this.imageFormGroup.push(this.newImage(element));        
    //     });
    // }

    if (videoData.length > 0) {
      let frmArray = this.viewingOptionsForm.get("videosData") as FormArray;
      frmArray.clear();
      videoData.forEach(element => {
        this.videosFormGroup.push(this.editVideo(element));
      });
    }
    if (dataArray.length > 0) {
      dataArray.forEach((element) => {
        let index = element.image_category - 1;
        this.sourceImgFormArr(index).push(this.imagesGroup(element));
      });

    }

  }

  editVideo(val): FormGroup {
    return this.fb.group({
      videoLink: [val.videoLink],
      videoFile: [val.videoFile],
      url: [''],
      visual_type: [val.visual_type],
      image_category: [val.image_category],
      used_equipment: [val.used_equipment],
    })
  }

  addQuantity() {
    this.videosData().push(this.newQuantity());
  }

  makeItPrimary(obj: any, index: number) {
    console.log(obj);
    console.log(index);
  }

  updateSelected(tabKey, ImageKey, conrolArr) {
    Object.keys(conrolArr).forEach((key) => {
      if (key == ImageKey) {
        conrolArr[key].is_primary = true;
      } else {
        conrolArr[key].is_primary = false;
      }
    });
    this.sourceImgFormGroup1(tabKey).setValue(conrolArr);
  }

  getTabsList() {
    this.newEquipmentService.getvisualTabs().subscribe((res: any) => {
      this.tabsList = res.results;
      if (this.tabsList.length) {
        this.tabsList.forEach((tab: any) => {
          this.imageFormGroup.push(this.newImage(tab));
        });
      }
    });
  }
  /**
   * setPrimary
   */
  setPrimary(tabKey, ImageKey, conrolArr) {
    Object.keys(conrolArr).forEach((key) => {
      if (key == ImageKey) {
        conrolArr[key].is_primary = true;
      } else {
        conrolArr[key].is_primary = false;
      }
    });
    this.sourceImgFormArr(tabKey).setValue(conrolArr);
  }

  /**
   * Handle Image Upload
   * @param event
   * @param tab
   * @param side
   * @param type
   */
  async handleUploadforVisuals(
    event: any,
    tab: number,
    side: string,
    type: string,
    index: number
  ) {
    var visualsDataFrom = new visualData();
    const file = event.target.files[0];
    if (
      (file.type == 'image/png' ||
        file.type == 'image/jpg' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/bmp') &&
      file.size <= 5242880
    ) {
      this.isInvalidfile = true;
      this.uploadingImage = true;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '_' + (this.sourceImgFormGroup1(index).length + 1) + '.' + fileExt;
      // var image_path =
      //   environment.bucket_parent_folder +
      //   '/' +
      //   tab +
      //   '/' +
      //   side +
      //   '_' +
      //   Math.random() +
      //   '.' +
      //   fileExt;
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (
        uploaded_file?.Key != undefined &&
        uploaded_file?.Key != '' &&
        uploaded_file?.Key != null
      ) {
        visualsDataFrom.visual_type = this.docType[type];
        visualsDataFrom.url = uploaded_file.Location;
        visualsDataFrom.is_primary = 0;
        visualsDataFrom.img = reader.result;
        if (
          file.type == 'image/png' ||
          file.type == 'image/jpg' ||
          file.type == 'image/jpeg' ||
          file.type == 'image/bmp'
        ) {
          visualsDataFrom.image_category = tab;
        }
        this.uploadImagesPath.push(visualsDataFrom);
        this.sourceImgFormArr(index).push(this.imagesGroup(visualsDataFrom));
      }
      this.uploadingImage = false;
    } else {
      this.notificationservice.error('Invalid Image.');
    }
    this.imageInput.nativeElement.value = '';
  }

  /**
   * Handle video upload
   * @param event
   * @param count
   * @param side
   * @param type
   */

  preview() {
    let state = 'newPreview';
    localStorage.setItem('newPreview', state);
    this.router.navigate(['new-equipment-dashboard/equipment-details', this.editableRowData.id]);
  }
  addOffer() {
    let offers = this.offersForm.get('offersArray') as FormArray;
    if (offers.length < this.maxOffersArrayLength) {
      offers.push(this.createOffer());
    }
  }
  getOffersLength() {
    let offers = this.offersForm.get('offersArray') as FormArray;
    return offers.length;
  }
  createOffer(): FormGroup {
    return this.fb.group({
      typeOfOffer: new FormControl(''),
      // valueOfOffer: new FormControl(''),
      offerTermsAndCond: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
      offerPicture: new FormControl(''),
      dealer: new FormControl(''),
      freeOfCost: new FormControl(''),
      tenure: new FormControl(''),
      amount: new FormControl(''),
      rate: new FormControl(''),
      margin: new FormControl(''),
      processingFee: new FormControl(''),
      installment: new FormControl(''),
      description: new FormControl(''),
      PinCode: new FormControl(''),
      Country: new FormControl(''),
      State: new FormControl(''),
      City: new FormControl(''),
      CityId: new FormControl(''),
      locationId: new FormControl('')
    }, {
      validator: [numberonly('PinCode')]
    });
  }

  removeOffer(i: number) {
    const control = <FormArray>this.offersForm.controls['offersArray']['controls'];
    if (control[i].get('offerId')) {
      this.newEquipmentService
        .deleteOfferMaster(control[i].get('offerId').value).subscribe(res => {
        })
    }
    let controls = <FormArray>this.offersForm.controls['offersArray'];
    controls.removeAt(i);
  }

  saveAssetData(data: any) {
    const msg = 'Product uploaded succesfully!';
    const action = 'Success!!';
    this.newEquipmentService.postVisualDetails(data).subscribe(
      (res: any) => {
        this.approve();
        this.notificationservice.error("Product uploaded succesfully!")
        //this.router.navigate(['/admin-dashboard/products/new-products']);

      },
      (error) => {
        this.notificationservice.error("Somthing went wrong. Please try again!")
      }
    );
  }



  /** get country/state/city code */
  getCountryData(queryParams: string): Observable<any> {
    return this.adminMasterService
      .getCountryMaster(queryParams)
  }

  renderCountryData(queryParams = 'limit=999') {
    this.getCountryData(queryParams).subscribe((data: any) => {
      this.countryDataSource = data.results;
      if (this.editableRowData != null && this.editableRowData != undefined) {
        this.isEdit = true;
        this.editableRowData = JSON.parse(this.editableRowData);
        this.assetId = this.editableRowData?.id;
        this.addVisual = true;
        this.sellerTypeValue = this.editableRowData.seller_type;
        this.getOffersByEquipmentId();
        // this.getVisualsDataById();
        if (this.editableRowData.status == 2 || this.editableRowData.status == 3) {
          this.isApproved = true;
        }
        this.setFormValue(this.editableRowData);

      } else {
        this.newUspData();
      }
    });
  }

  renderStateData(queryParams: string) {
    return new Promise((resolved, reject) => {
      this.getStateData(queryParams).subscribe((res: any) => {
        this.stateDataSource = res.results;
        resolved(true);
      });
    });
  }

  getStateData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getStateMaster(queryParams)

  }

  async getStates(value: any) {
    await this.renderStateData('country__id__in=' + value + '&limit=999')
    //this.cityDataSource = [];
    let country = this.countryDataSource.filter(x => x.id == value).map(x => x.name)[0];
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    // if (country != 'India') {
    //   arrayControl.controls[i]?.get('Pincode') ?.clearValidators();
    //   arrayControl.controls[i]?.get.updateValueAndValidity();
    //   arrayControl.controls[i]?.get.setValidators(Validators.required);
    //   arrayControl.controls[i]?.get.updateValueAndValidity();
    // }
    // else {
    //   arrayControl.controls[i]?.get('Pincode') ?.setValidators([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)]);
    //   arrayControl.controls[i]?.get('Pincode') ?.updateValueAndValidity();
    // }
  }
  renderStateDataByStateId(id: any) {
    this.getStateDataByStateId(id).subscribe((res: any) => {
      let stateArray: Array<any>;
      stateArray = new Array<any>();
      stateArray.push(res)
      this.stateDataSource = stateArray;
    })
  }

  getStateDataByStateId(id: any): Observable<Object> {
    return this.adminMasterService
      .getStateMasterByStateid(id)

  }

  renderCityData(queryParams: string) {
    this.getCityData(queryParams).subscribe((res: any) => {
      this.cityDataSource = []
      this.cityDataSource = res.results;
    })
  }

  getCityData(queryParams: string): Observable<Object> {
    return this.adminMasterService
      .getCityMaster(queryParams)
  }

  renderCityDataByCityId(id: any) {
    this.getCityDataByCityId(id).subscribe((res: any) => {
      let cityArray: Array<any>;
      cityArray = new Array<any>();
      cityArray.push(res)
      this.cityDataSource = cityArray;
    })
  }

  getCityDataByCityId(id: any): Observable<Object> {
    return this.adminMasterService
      .getCityMasterById(id)
  }

  async getCities(value: any) {
    await this.renderCityData('state__id__in=' + value + '&limit=999')
  }
  /** end of these get country/state/city code */
  saveVisuals(id) {
    //this.spinner.show();
    this.videoLinkPath = [];

    var imageData = this.viewingOptionsForm.get('imageData')?.value;
    var videoData = this.viewingOptionsForm.get('videosData')?.value;
    for (let Imdata of imageData) {
      if (Imdata.images.length > 0) {
        for (let image of Imdata.images) {
          image.is_primary = image.is_primary == 1 ? true : false;
          image.new_equipment = id;
          image.url = image.url;
          this.uploadVideoAndImages.push(image);
        }
      }
    }
    for (let data of videoData) {
      if (data.url != '' && data.url != undefined && data.url != null) {
        var visualsDataFrom = new visualDataNewEquipment();
        visualsDataFrom.visual_type = this.docType['VIDEO_UPLOAD'];
        visualsDataFrom.url = data.url;
        visualsDataFrom.image_category = 1;
        visualsDataFrom.new_equipment = id;
        this.uploadVideoAndImages.push(visualsDataFrom);
        //this.saveAssetData(visualsDataFrom);
      }
      if (data.videoLink != '' && data.videoLink != undefined && data.videoLink != null) {
        var visualsDataFrom = new visualDataNewEquipment();
        visualsDataFrom.visual_type = this.docType['VIDEO_LINK'];
        visualsDataFrom.url = data.videoLink;
        visualsDataFrom.new_equipment = id;
        visualsDataFrom.image_category = 1;
        // this.saveAssetData(visualsDataFrom);
        this.uploadVideoAndImages.push(visualsDataFrom);
      }
    }
    //console.log(this.uploadVideoAndImages); return;
    if (this.uploadVideoAndImages.length > 0) {
      this.saveAssetData(this.uploadVideoAndImages);
    }
    //else{
    //   this.saveData();
    // }
  }

  updateAssetData(data: any, id: any) {
    const msg = 'Product uploaded succesfully!';
    const action = 'Success!!';
    if (this.isSubmitType) {
      //this.router.navigate(['/admin-dashboard/products/new-products']);
    } else {
      this.newEquipmentService.updateVisualDetails(data, id).subscribe(
        (res: any) => {
          this.approve();
          this.notificationservice.success("Product uploaded succesfully!")
          //this.router.navigate(['/admin-dashboard/products/new-products']);
        },
        (error) => {
          this.notificationservice.error("Somthing went wrong. Please try again!")
        }
      );
    }
  }

  updateVisuals(id) {
    //this.spinner.show();
    this.videoLinkPath = [];

    var imageData = this.viewingOptionsForm.get('imageData')?.value;
    var videoData = this.viewingOptionsForm.get('videosData')?.value;
    for (let Imdata of imageData) {
      if (Imdata.images.length > 0) {
        for (let image of Imdata.images) {
          image.is_primary = image.is_primary == 1 ? true : false;
          image.new_equipment = id;
          image.url = image.url;
          this.uploadVideoAndImages.push(image);
        }
      }
    }
    for (let data of videoData) {
      if (data.url != '' && data.url != undefined && data.url != null) {
        var visualsDataFrom = new visualDataNewEquipment();
        visualsDataFrom.visual_type = this.docType['VIDEO_UPLOAD'];
        visualsDataFrom.url = data.url;
        visualsDataFrom.image_category = 1;
        visualsDataFrom.new_equipment = id;
        this.uploadVideoAndImages.push(visualsDataFrom);
        //this.saveAssetData(visualsDataFrom);
      }
      if (data.videoLink != '' && data.videoLink != undefined && data.videoLink != null) {
        var visualsDataFrom = new visualDataNewEquipment();
        visualsDataFrom.visual_type = this.docType['VIDEO_LINK'];
        visualsDataFrom.url = data.videoLink;
        visualsDataFrom.new_equipment = id;
        visualsDataFrom.image_category = 1;
        // this.saveAssetData(visualsDataFrom);
        this.uploadVideoAndImages.push(visualsDataFrom);
      }
    }
    //console.log(this.uploadVideoAndImages); return;
    if (this.uploadVideoAndImages.length > 0) {
      this.updateAssetData(this.uploadVideoAndImages, id);
    }
    //else{
    //   this.saveData();
    // }
  }

  submit() { }

  /**create visual dynamic form group */
  createVisuals() {
    this.visuals = this.newProductUpload.get('visuals') as FormArray;
    if (this.visuals.length < this.maxOffersArrayLength) {
      this.visuals.push(this.createVisualArray());
    }
  }

  /**set neww product details to call api*/
  async confirmation() {
    this.addNewAsset = true;
    this.newProductUpload.markAllAsTouched();
    if (this.addVisual && this.assetId && !this.isEdit) {
      let isVisuals = await this.checkVisuals();
      if (!isVisuals) {
        this.notify.error('Please upload atleast one image for the asset!!');
        return;
      }
    }
    if (this.newProductUpload.valid) {
      this.isSubmitType = false;
      this.getNewProductUploadRequest();
      //this.spinner.show();
      this.productUpload.postBasicDetail(this.adminNewProductUpload).subscribe((res: any) => {
        if (res != null) {
          this.editableRowData = res;
          this.assetId = res?.id;
          this.addVisual = true;
          let offers = this.offersForm.get('offersArray') as FormArray;
          let filtered = offers.value.filter(item => item.typeOfOffer == '' && offers.value.length >= 1);
          if (!filtered.length && offers.value.length) {
            this.createRequestOffersMaster(res.id);
          }
        }
      });
    }
  }

  /**prepare request for api call */
  getNewProductUploadRequest() {
    this.adminNewProductUpload = new AdminNewProductUpload();
    this.adminNewProductUpload.certificate = this.newProductUpload.controls['certificate'].value ? this.newProductUpload.controls['certificate'].value : [];
    this.adminNewProductUpload.relationship_manager = this.newProductUpload.controls['relationship_manager'].value ? this.newProductUpload.controls['relationship_manager'].value : '';
    this.adminNewProductUpload.seller = this.cognitoId;
    this.adminNewProductUpload.mfg_year = this.newProductUpload.controls['yearOfManufacture'].value;
    this.adminNewProductUpload.other_category = this.newProductUpload.controls['otherCategory'].value;
    this.adminNewProductUpload.other_brand = this.newProductUpload.controls['otherBrand'].value;
    this.adminNewProductUpload.other_model = this.newProductUpload.controls['otherModel'].value;
    // if (this.newProductUpload.get('yearOfManufacture')?.value != "") {
    //   let date = this.newProductUpload.get('yearOfManufacture')?.value;
    //   this.adminNewProductUpload.mfg_year = this.datePipe.transform(date, 'yyyy');
    // }
    this.adminNewProductUpload.seller_type = this.sellerTypeValue;
    // this.adminNewProductUpload.indicative_price = this.newProductUpload.get(
    //   'indicativePrice'
    // )?.value;
    this.adminNewProductUpload.document = this.newProductUpload.get(
      'brochure'
    )?.value;
    // this.adminNewProductUpload.video_link = this.newProductUpload.get(
    //   'video'
    // )?.value;
    // this.adminNewProductUpload.youtube_link = this.newProductUpload.get(
    //   'youtubeLink'
    // )?.value;
    this.adminNewProductUpload.category = this.categoryId
    this.adminNewProductUpload.brand = this.brandId
    this.adminNewProductUpload.model = this.modalId
    this.adminNewProductUpload.seller = this.cognitoId;
    this.adminNewProductUpload.info_level = 3;
    if (this.editableRowData && this.editableRowData.status) {
      this.adminNewProductUpload.status = this.editableRowData.status
    } else {
      this.adminNewProductUpload.status = 1
    }
    // this.adminNewProductUpload.rate_equipment = this.newProductUpload.controls[
    //   'rateEquipment'
    // ].value;
    this.adminNewProductUpload.is_active =
      this.newProductUpload.controls['isActive'].value == 1 ? 'true' : 'false';
    this.adminNewProductUpload.product_name = this.newProductUpload.controls[
      'Prdoduct_name'
    ].value;
    this.adminNewProductUpload.is_price_on_request = this.newProductUpload.get('isPriceOnRequest')?.value == '1' ? true : false;
    if (!this.adminNewProductUpload.is_price_on_request) {
      this.adminNewProductUpload.selling_price = this.newProductUpload.get('showroomPrice')?.value;
    }
    // this.adminNewProductUpload.selling_price = this.newProductUpload.controls[
    //   'SellPrice'
    // ].value;
    this.adminNewProductUpload.usp = [];
    this.UspAddArray.value.forEach(element => {
      this.adminNewProductUpload.usp.push(element.Usp);
    });
    this.adminNewProductUpload.tech_fields = [];
    //if (this.isTechfieldShow) {
    let dynamicTech_Fields = this.demoForm.get('demoArray') as FormArray;
    for (let i = 0; i < dynamicTech_Fields.value.length; i++) {
      this.object = {};
      this.object = {
        field_name: this.arrayItems[i].title,
        field_type: 1,
        field_value: dynamicTech_Fields.value[i],
        visible_on_front: true,
        priority: 1
      }
      this.adminNewProductUpload.tech_fields.push(this.object);
    }

    //} else {
    if (this.newProductUpload.get('enginePower')?.value) {
      this.adminNewProductUpload.engine_power = this.newProductUpload.get('enginePower')?.value;
    }
    if (this.newProductUpload.get('grossWeight')?.value) {
      this.adminNewProductUpload.gross_Weight = this.newProductUpload.get('grossWeight')?.value;
    }
    if (this.newProductUpload.get('operatingWeight')?.value) {
      this.adminNewProductUpload.operating_weight = this.newProductUpload.get('operatingWeight')?.value;
    }
    if (this.newProductUpload.get('bucketCapcity')?.value) {
      this.adminNewProductUpload.bucket_capacity = this.newProductUpload.get('bucketCapcity')?.value;
    }
    if (this.newProductUpload.get('liftingCapcity')?.value) {
      this.adminNewProductUpload.lifting_capacity = this.newProductUpload.get('liftingCapcity')?.value;
    }
    //}
    this.UspAddArray = <FormArray>(this.newProductUpload.controls['uspAddArray']);
    this.adminNewProductUpload.usp = [];
    this.UspAddArray.value.forEach(element => {
      this.adminNewProductUpload.usp.push(element.Usp);
    });

    //this.adminNewProductUpload.fuel_type = this.newProductUpload.get('fuelType')?.value;
    if (this.newProductUpload.get('fuelType')?.value != "") {
      this.adminNewProductUpload.fuel_type = this.newProductUpload.get('fuelType')?.value;
    }
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  onOfferTypeSelect(event: any) {
    this.isShowOfferTypes = event;
    if (event == 1) {
      this.isOfferCash = false;
      this.isOfferLease = false;
      this.isOfferFinance = true;
    } else if (event == 2) {
      this.isOfferCash = false;
      this.isOfferLease = true;
      this.isOfferFinance = false;
    } else {
      this.isOfferCash = true;
      this.isOfferLease = false;
      this.isOfferFinance = false;
    }
  }

  /**set neww product details to call api*/
  async next() {
    this.newProductUpload.markAllAsTouched();
    this.offersForm.markAllAsTouched();
    if (this.addVisual && this.assetId && !this.isEdit) {
      let isVisuals = await this.checkVisuals();
      if (!isVisuals) {
        this.notify.error('Please upload atleast one image for the asset!!');
        return;
      }
    }
    if (this.newProductUpload.valid) {
      this.getNewProductUploadRequest();
      //this.spinner.show();
      this.productUpload.updateBasicDetail(this.adminNewProductUpload, this.editableRowData.id).subscribe((res: any) => {
        if (res != null) {
          if (this.addNewAsset) {
            this.approve();
          }
          this.isSubmitType = true;
          this.editableRowData = res;
          let offers = this.offersForm.get('offersArray') as FormArray;
          let filtered = offers.value.filter(item => item.typeOfOffer == '' && offers.value.length >= 1);
          if (!filtered.length && offers.value.length) {
            this.createRequestOffersMaster(res.id);
          } else {
            this.notify.success('Asset Updated Successfully');
            this.isEdit = true;
            //this.router.navigate(['/admin-dashboard/products/new-products']);
          }
        }
      });
    }
  }

  checkVisuals() {
    return new Promise((resolved, reject) => {
      this.newEquipmentService.getNewVisualsByEquipmentId(this.assetId, { limit: 999 }).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res?.results?.length) {
            let obj = res?.results.find(f => { return f.visual_type == 1 })
            if (obj) {
              resolved(true);
            } else {
              resolved(false);
            }
          } else {
            resolved(false);
          }
        },
        err => {
          console.log(err?.message);
          reject(err);
        }
      );
    });
  }

  async getOffersByEquipmentId() {
    await this.productUpload.getOffersFromMaster(this.editableRowData.id).subscribe((res: any) => {
      if (res.results.length > 0) {
        let offer: any = {};
        offer.finance_or_leases = [];
        offer.cash_purchases = [];
        const formArray: any = new FormArray([]);
        //let typeOffer = res.offer_master.is_finance ? "1" : res.offer_master.is_cash ? "3" : res.offer_master.is_lease ? "2" : "";
        for (var i = 0; i < res.results.length; i++) {
          if (res.results[i].country && res.results[i].country[0]) {
            this.getStates(res.results[i].country[0])
          }
          if (res.results[i].state && res.results[i].state[0]) {
            this.getCities(res.results[i].state[0].id)
          }
          formArray.push(this.fb.group({
            startDate: res.results[i].start_date,
            endDate: res.results[i].end_date,
            typeOfOffer: res.results[i].type.toString(),
            freeOfCost: res.results[i].free_of_cost,
            description: res.results[i].description,
            // valueOfOffer: res.results[i].amount,
            amount: res.results[i].amount,
            rate: res.results[i].rate,
            tenure: res.results[i].tenure,
            margin: res.results[i].margin,
            processingFee: res.results[i].processing_fee,
            installment: res.results[i].installment,
            Country: res.results[i].country[0] ? res.results[i].country[0] : '',
            State: res.results[i].state[0] ? res.results[i].state[0].id : '',
            City: res.results[i].city[0] ? res.results[i].city[0].id : '',
            // StateId: res.results[i].state[0].id,
            // CityId: res.results[i].city[0].id,
            PinCode: res.results[i].pincode[0] ? res.results[i].pincode[0].pin_code : '',
            //locationId: res.results[i].state[0].id,
            offerTermsAndCond: res.results[i].t_and_c_doc,
            new_equipment: this.editableRowData.id,
            offerId: res.results[i].id
          }));
          this.documentNameTc[i] = { name: res.results[i].t_and_c_doc, url: res.results[i].t_and_c_doc };
          this.documentNameOffers[i] = { name: res.results[i].offer_image, url: res.results[i].offer_image };
        }
        this.offersForm.setControl('offersArray', formArray);

      }
    })
  }

  prepareAndSaveOffersData(id) {
    var arrayControl = this.offersForm.get('offersArray') as FormArray;
    let newEquipmentOfferMaster: Array<any> = [];
    let cash_purchase = {};
    arrayControl.value.forEach((item) => {
      console.log(item);
      cash_purchase = {
        name: "Offer",
        type: item.typeOfOffer,
        tenure: item.tenure,
        rate: item.rate,
        amount: item.amount,
        margin: item.margin,
        processing_fee: item.processingFee,
        installment: item.installment,
        free_of_cost: item.freeOfCost,
        description: item.description,
        state: item.State ? [item.State] : [],
        city: item.City ? [item.City] : [],
        pincode: item.PinCode ? [Number(item.PinCode)] : [],
        country: item.Country ? [item.Country] : [],
        t_and_c_doc: item.offerTermsAndCond,
        start_date: this.datePipe.transform(item.startDate, 'yyyy-MM-dd'),
        end_date: this.datePipe.transform(item.endDate, 'yyyy-MM-dd'),
        new_equipment: this.editableRowData.id
      };
      newEquipmentOfferMaster.push(cash_purchase);
    });

    if (newEquipmentOfferMaster) {
      this.productUpload.postOfferMaster(newEquipmentOfferMaster).subscribe((res: any) => {
        console.log(res);
        let payload: any = [];
        const control = <FormArray>this.offersForm.controls['offersArray'];
        let count = 0;
        let count1 = 0;

      });
    }
  }

  /*** get state data*/
  getLocationData(event, i) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    let data = arrayControl.controls[i]?.get('PinCode')?.value
    if (data && data.length > 5) {
      let queryParam = `search=${data}`;
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {

          this.stateList = res.results;
          if (this.stateList && this.stateList.length > 0) {
            arrayControl.controls[i]?.get('Country')?.setValue(this.stateList[0].city.state?.country?.id);
            arrayControl.controls[i]?.get('State')?.setValue(this.stateList[0].city.state?.id);
            arrayControl.controls[i]?.get('City')?.setValue(this.stateList[0].city?.id);
            if (this.stateList[0].city.state?.country?.id) {
              this.getStates(this.stateList[0].city.state?.country?.id)
            }
            if (this.stateList[0].city.state?.id) {
              this.getCities(this.stateList[0].city.state?.id)
            }
            // arrayControl.controls[i]?.get('locationId')?.setValue(this.stateList[0].city.state?.id),
            //   arrayControl.controls[i]?.get('CityId')?.setValue(this.stateList[0].city.id)
          }
          // if ((this.stateList && this.stateList.length == 0) || data.length < 5) {
          //   arrayControl.controls[i]?.get('State')?.setValue('')
          //   arrayControl.controls[i]?.get('City')?.setValue('')
          // }
        },
        (err) => {
          console.error(err);
        }
      );
    } else {
      // arrayControl.controls[i]?.get('State')?.setValue('')
      // arrayControl.controls[i]?.get('City')?.setValue('')
    }
  }

  createRequestOffersMaster(id) {
    /**---form control bindings--- */
    this.offersForm.markAllAsTouched();
    if (this.offersForm.valid) {
      let locations: any = [];
      let cash_purchases: any = [];
      let cash_purchase = {};
      let leaseAndFinance = {};
      let leaseAndFinances: any = [];
      var arrayControl = this.offersForm.get('offersArray') as FormArray;
      this.newEquipmentOfferMaster = [];
      this.updateOffermaster = [];
      arrayControl.value.forEach((item, i) => {
        cash_purchase = {
          name: "Offer",
          type: item.typeOfOffer,
          tenure: item.tenure,
          rate: item.rate,
          amount: item.amount,
          margin: item.margin,
          processing_fee: item.processingFee,
          installment: item.installment,
          free_of_cost: item.freeOfCost,
          description: item.description,
          state: item.State ? [item.State] : [],
          city: item.City ? [item.City] : [],
          pincode: item.PinCode ? [Number(item.PinCode)] : [],
          country: item.Country ? [item.Country] : [],
          t_and_c_doc: this.documentNameTc[i]?.url == undefined ? '' : this.documentNameTc[i].url,
          offer_image: this.documentNameOffers[i]?.url == undefined ? '' : this.documentNameOffers[i].url,
          start_date: this.datePipe.transform(item.startDate, 'yyyy-MM-dd'),
          end_date: this.datePipe.transform(item.endDate, 'yyyy-MM-dd'),
          new_equipment: id,
          id: item.offerId,
          cognito_id: this.cognitoId

        };

        if (item.offerId) {
          this.updateOffermaster.push(cash_purchase)
        } else {
          this.newEquipmentOfferMaster.push(cash_purchase);
        }
      });
      if (this.offersForm.valid) {
        if (this.newEquipmentOfferMaster) {
          this.newEquipmentService.postOffersToMaster(this.newEquipmentOfferMaster).subscribe((res) => {
            this.getOffersByEquipmentId()

            this.offersMasterResponce = res;
            var getResponse = res as ResponseData;

            if (getResponse.statusCode == 200 || getResponse.statusCode == 201) {
              if (getResponse.body != null) {

              }
            }
            if (getResponse.statusCode == 409) {

              if (!this.updateOffermaster) {
                this.notify.success('Asset Updated Successfully');
                //this.router.navigate(['/admin-dashboard/products/new-products']);
              }
            }
          });
        }
        if (this.updateOffermaster) {
          let index = 0;
          this.updateOffermaster.forEach(element => {
            this.newEquipmentService.updateOfferMaster(element, element.id).subscribe((res) => {

              index++;
              if (index == this.updateOffermaster.length) {
                this.notify.success('Asset Updated Successfully');
                // this.router.navigate(['/admin-dashboard/products/new-products']);
              }
            });
          });
        }
      } else {
        return;
      }
    } else {
      return;
    }
  }

  /**create visual array */
  createVisualArray(): FormGroup {
    return new FormGroup({
      visualAppearance: new FormControl(''),
      visualVideoLink: new FormControl('', Validators.required),
      visualUploadVideo: new FormControl(''),
    });
  }

  /**upload files and video by this method */
  async handleUploadOffers(event: any, tab: string, side: string, index: any) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    const file = event.target.files[0];
    if (
      (file.type == 'image/png' ||
        file.type == 'image/jpg' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/bmp') &&
      file.size <= 5242880
    ) {
      //this.isInvalidfile = true;
      //this.uploadingImage = true;
      //  this.isShowPanDoc = false;
      arrayControl.controls[side]?.get('offerPicture')?.setErrors(null);
      arrayControl.controls[side]?.get('offerTermsAndCond')?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // console.log('type', file.type);
        // this.offersForm
        //   .get(tab)
        //   ?.get(side)
        //   ?.setValue(reader.result);
      };
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path =
        environment.bucket_parent_folder +
        '/' +
        this.cognitoId +
        '/' +
        tab +
        '/' +
        side +
        '.' +
        fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (
        uploaded_file?.Key != undefined &&
        uploaded_file?.Key != '' &&
        uploaded_file?.Key != null
      ) {
        if (tab == 'offerPicture') {
          // arrayControl.controls[index]?.get('offerPicture')?.setValue(uploaded_file.Location);
          this.documentNameOffers[index] = { name: fileFormat, url: uploaded_file.Location };
        } else {
          // arrayControl.controls[index]?.get('offerTermsAndCond')?.setValue(uploaded_file.Location);
          let object = { name: fileFormat, url: uploaded_file.Location };
          this.documentNameTc[index] = object;
          console.log(this.documentNameTc)
        }
      }

    } else {
      // this.isShowPanDoc = true;

      if (tab == 'offerPicture') {
        arrayControl.controls[index]?.get('offerPicture')?.setValue('');
        arrayControl.controls[index]?.get('offerPicture')?.setErrors({ imgIssue: true });
      } else {
        arrayControl.controls[index]?.get('offerTermsAndCond')?.setValue('');
        arrayControl.controls[index]?.get('offerTermsAndCond')?.setErrors({ imgIssue: true });
      }
    }
  }

  resetFileOffers(i) {
    this.documentNameOffers.splice(i, 1);
  }

  resetFileTermCondition(i: any) {
    this.documentNameTc.splice(i, 1);
  }

  resetvideoFile() {
    this.videoName = [];
  }

  updateValidations(i) {
    var arrayControl = this.offersForm?.controls['offersArray'] as FormArray
    if (arrayControl.controls[i]?.get('typeOfOffer')?.value == 3) {
      arrayControl.controls[i]?.get('typeOfOffer')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
      // arrayControl.controls[i]?.get('valueOfOffer')?.setValidators(Validators.required)
      // arrayControl.controls[i]?.get('valueOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('startDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('startDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('endDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('endDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('freeOfCost')?.clearValidators();
      arrayControl.controls[i]?.get('freeOfCost')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('tenure')?.clearValidators();
      arrayControl.controls[i]?.get('tenure')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('amount')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('amount')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('rate')?.clearValidators();
      arrayControl.controls[i]?.get('rate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('margin')?.clearValidators();
      arrayControl.controls[i]?.get('margin')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('installment')?.clearValidators();
      arrayControl.controls[i]?.get('installment')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('processingFee')?.clearValidators();
      arrayControl.controls[i]?.get('processingFee')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('country')?.clearValidators();
      arrayControl.controls[i]?.get('country')?.updateValueAndValidity();
    }
    if (arrayControl.controls[i]?.get('typeOfOffer')?.value == 1 || arrayControl.controls[i]?.get('typeOfOffer')?.value == 2) {
      arrayControl.controls[i]?.get('typeOfOffer')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
      // arrayControl.controls[i]?.get('valueOfOffer')?.setValidators(Validators.required)
      // arrayControl.controls[i]?.get('valueOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('startDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('startDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('endDate')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('endDate')?.updateValueAndValidity();
      // arrayControl.controls[i]?.get('PinCode')?.clearValidators();
      // arrayControl.controls[i]?.get('PinCode')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('freeOfCost')?.clearValidators();
      arrayControl.controls[i]?.get('freeOfCost')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('tenure')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('tenure')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('amount')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('amount')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('country')?.setValidators(Validators.required);
      arrayControl.controls[i]?.get('country')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('rate')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('rate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('margin')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('margin')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('installment')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('installment')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('processingFee')?.setValidators(Validators.required)
      arrayControl.controls[i]?.get('processingFee')?.updateValueAndValidity();
    }
    if (arrayControl.controls[i]?.get('typeOfOffer')?.value == '') {
      arrayControl.controls[i]?.get('typeOfOffer')?.clearValidators();
      arrayControl.controls[i]?.get('typeOfOffer')?.updateValueAndValidity();
      // arrayControl.controls[i]?.get('valueOfOffer')?.setValidators(Validators.required)
      // arrayControl.controls[i]?.get('valueOfOffer')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('startDate')?.clearValidators();
      arrayControl.controls[i]?.get('startDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('endDate')?.clearValidators();
      arrayControl.controls[i]?.get('endDate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('freeOfCost')?.clearValidators();
      arrayControl.controls[i]?.get('freeOfCost')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('tenure')?.clearValidators();
      arrayControl.controls[i]?.get('tenure')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('amount')?.clearValidators();
      arrayControl.controls[i]?.get('amount')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('rate')?.clearValidators();
      arrayControl.controls[i]?.get('rate')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('margin')?.clearValidators();
      arrayControl.controls[i]?.get('margin')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('installment')?.clearValidators();
      arrayControl.controls[i]?.get('installment')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('processingFee')?.clearValidators();
      arrayControl.controls[i]?.get('processingFee')?.updateValueAndValidity();
      arrayControl.controls[i]?.get('country')?.clearValidators();
      arrayControl.controls[i]?.get('country')?.updateValueAndValidity();
    }
    return this.offersForm;
  }

  /**upload files and video by this method */
  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if (
      (file?.type == 'image/png' ||
        file?.type == 'image/jpg' ||
        file?.type == 'image/jpeg' ||
        file?.type == 'image/bmp') &&
      file?.size <= 5242880
    ) {
      //this.isInvalidfile = true;
      //this.uploadingImage = true;
      //  this.isShowPanDoc = false;
      this.newProductUpload
        .get(tab)
        ?.get('docImages')
        ?.get(side)
        ?.setErrors(null);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log('type', file.type);
        this.newProductUpload
          .get(tab)
          ?.get('docImages')
          ?.get(side + 'View')
          ?.setValue(reader.result);
      };
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path =
        environment.bucket_parent_folder +
        '/' +
        this.cognitoId +
        '/' +
        tab +
        '/' +
        side +
        '.' +
        fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      if (
        uploaded_file?.Key != undefined &&
        uploaded_file?.Key != '' &&
        uploaded_file?.Key != null
      ) {
        this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
        this.newProductUpload.get(tab)?.setValue(uploaded_file.Location);
        // this.newProductUpload
        //   .get(tab)
        //   ?.get('docImages')
        //   ?.get(side)
        //   ?.setValue(uploaded_file?.Key);
      } else {
        // this.newProductUpload
        //   .get(tab)
        //   ?.get('docImages')
        //   ?.get(side + 'View')
        //   ?.setValue('');
        this.notify.error('Upload Failed');
      }
      // controlName=reader.result;
      // setTimeout(() => {
      //   this.checkPanImageOnSubmit();
      // }, 0);
      //this.uploadingImage = false;
      //};
    } else {
      // this.isShowPanDoc = true;
      this.newProductUpload.get(tab)?.get('docImages')?.get(side)?.setValue('');
      this.newProductUpload
        .get(tab)
        ?.get('docImages')
        ?.get(side)
        ?.setErrors({ imgIssue: true });
    }
  }

  async handleVideoUpload(event: any, count: any, side: string, type: string) {
    var visualsDataFrom = new visualData();
    const file = event.target.files[0];
    console.log(file.size);
    console.log('type', file.type);
    if (file.type == 'video/mp4') {
      this.isInvalidfile = true;
      this.uploadingImage = true;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log('type', file.type);
        this.viewingOptionsForm.get('videodata')?.get(count)?.get('videoFile')?.setValue(reader.result);
      }
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];

      var image_path = environment.bucket_parent_folder + "/" + count + '/' + side + '_' + Math.round(Math.random()) + '.' + fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      console.log(uploaded_file);
      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.viewingOptionsForm.get('videodata')?.get(count)?.get('videoFile')?.setValue("");

        // visualsDataFrom.visual_type = this.docType[type];
        // visualsDataFrom.used_equipment = this.vaahanDetail.id;
        // visualsDataFrom.url = uploaded_file.Location;
        // visualsDataFrom.image_category = 1;
        this.sourceVideoFormGroup[count].get('url')?.setValue(uploaded_file.Location);
        this.sourceVideoFormGroup[count].get('visual_type')?.setValue(this.docType[type]);

        this.uploadVideosPath.push(visualsDataFrom);
        console.log(this.uploadImagesPath);
      }
      else {
        this.viewingOptionsForm.get('videodata')?.get(count)?.get('videoFile')?.setValue('');

      }

      this.uploadingImage = false;

    } else {

      this.notificationservice.error("Invalid Video File.")
    }
  }

  async handleVideoUploadBasic(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    if (file.type == 'video/mp4' || file.type == 'video/avi' || file.type == 'video/webm' || file.type == 'video/3gpp' || file.type == 'video/mov' || file.type == 'video/wmv') {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
      this.videoName.push({ name: fileFormat, url: uploaded_file.Location });
      if (tab == 'video') {
        this.newProductUpload.get('video')?.setValue(uploaded_file.Location);
      }
    } else {
      this.newProductUpload.get('video')?.setValue('');
      this.notificationservice.error("Invalid Video File.")
    }
  }

  /* searchSeller(e) {
     console.log(e.target.value);
     let name = e.target.value;
     if (name.length >= 3) {
       // let queryParams =
       //   'first_name__icontains=' +
       //   e.target.value +
       //   '&groups__name__iexact=Customer' +
       //   '&limit=999';
       // this.newEquipmentService.getSellerList(queryParams).subscribe(
       //   (res: any) => {
       //     this.sellerList = res.results;
       //   },
       //   (error) => {
       //     const msg = 'Somthing went wrong. Please try again!';
       //     const action = 'Error';
       //     this.notificationservice.error(msg);
       //   }
       // );
 
       this.getPartnerContactList(name);
     }
   }*/

  searchSeller(e) {
    console.log("value", e.target.value);
    let payload = {
      "number": e.target.value,
      "role": this.newProductUpload.get('seller_type')?.value == 2 ? 'Dealer' : 'Manufacturer'
    }
    if (this.userRole == 'ChannelPartner') {
      payload['channel_partner_cognito_id'] = this.cognitoId;
    }
    this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
      (res: any) => {
        this.sellerList = [];
        if (res.data) {
          this.sellerList = res.data;
          this.isMobileInvalid = false;
        } else {
          this.isMobileInvalid = true;
        }

        console.log("sellerList", this.sellerList)
      },
      (error) => {
        console.log("eror", error);
        this.isMobileInvalid = true;
        this.sellerList = [];
      }
    );

  }


  keyPress(event: any) {
    this.sellerList = [];
    if (this.sellerList.length == 0 && this.newProductUpload.controls['mobile'].value.length < 9) {
      this.sellerList = [];
      this.newProductUpload.get('email')?.setValue('');
      this.newProductUpload.get('firstname')?.setValue('');
    }
    else {
      this.searchSeller(event);
    }
  }

  sellerTypeSelect(value) {
    this.sellerList = [];
    this.newProductUpload.get('seller_type')?.setValue(value);
    this.clearSeller();
  }

  getPartnerContactList(value) {
    this.partnerDealerRegistrationService.searchPartnerContact('', value).subscribe(
      (res: any) => {
        this.sellerList = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      if (res.cognito_id == search) {
        this.selectSeller(res);
        this.isMobileInvalid = false;
      } else {
        this.isMobileInvalid = true;
      }
    });
    if (this.newProductUpload.get('email')?.value == '') {
      this.isMobileInvalid = true;
    }
  }

  selectSeller(seller) {
    this.newProductUpload.get('email')?.setValue(seller.email);
    this.cognitoId = seller.cognito_id;
    this.newProductUpload?.get('firstname')?.setValue(seller.first_name);
  }

  clearSeller() {
    this.newProductUpload.get('email')?.setValue('');
    this.newProductUpload?.get('mobile')?.setValue('');
    this.cognitoId = '';
    this.newProductUpload?.get('firstname')?.setValue('');
  }

  getCategoryMasters() {
    this.commonService.getCategoryMaster('limit=999').subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  filterValuesCategory(search: any) {
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }

  filterValuesBrand(search: any) {
    let data = search.target.value
    let filtered = this.brandData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }

  filterValuesModal(search: any) {
    let data = search.target.value
    let filtered = this.modalData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }

  onModalSelect(modal: any) {
    this.modalId = modal.id;
    this.newProductUpload.get('modal')?.setValue(modal.name);
    this.newProductUpload.get('Prdoduct_name')?.setValue(
      (this.newProductUpload?.get('category')?.value == 'Other' ? this.newProductUpload.get('otherCategory')?.value : this.newProductUpload?.get('category')?.value) + ' ' + (this.newProductUpload?.get('brand')?.value == 'Other' ? this.newProductUpload.get('otherBrand')?.value : this.newProductUpload.get('brand')?.value) + ' ' + (this.newProductUpload?.get('modal')?.value == 'Other' ? this.newProductUpload.get('otherModel')?.value : this.newProductUpload.get('modal')?.value));

  }

  onCategorySelect(categoryId) {
    if (categoryId.id != this.categoryId) {
      this.newProductUpload.get('model')?.setValue('');
      this.newProductUpload.get('brand')?.setValue('');
      this.newProductUpload.get('category')?.setValue(categoryId.display_name);
      this.categoryId = categoryId.id
      let payload = {};

      payload = {
        cognito_id: this.cognitoId,
        type: this.newProductUpload.get('seller_type')?.value == 2 ? 'dealer' : 'manufacturer'
      }

      if (this.categoryId != 0) {
        this.commonService.getBrandMasterByCatId(this.categoryId, payload).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.brandList = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.newProductUpload.get('category')?.setValue(categoryId.display_name);
    }

  }

  onBrandSelect(brandId: any) {
    if (brandId.id != this.brandId) {
      this.brandId = brandId.id;
      this.newProductUpload.get('model')?.setValue('');
      this.newProductUpload.get('brand')?.setValue(brandId.display_name);
      if (this.brandId != 0) {
        this.commonService.getModelMasterByBrandId(brandId.id, this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.modelList = res.results;
            this.modalData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.newProductUpload.get('brand')?.setValue(brandId.display_name);
    }
  }

  backToList() {
    localStorage.removeItem('newProductRecordById');
    this.router.navigate(['/admin-dashboard/products/new-products']);
  }

  ngOnDestroy() {
    localStorage.removeItem('newProductRecordById');
  }

  resetFile() {
    this.documentName = [];
  }

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  // chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
  //   const ctrlValue = this.newProductUpload.get('yearOfManufacture');
  //   ctrlValue?.setValue(_moment(this.minDate));
  //   ctrlValue?.value.year(normalizedYear.year());
  //   ctrlValue?.value.month(normalizedYear.month());
  //   ctrlValue?.setValue(ctrlValue.value);
  //   this.newProductUpload?.get('yearOfManufacture')?.setValue(ctrlValue?.value);
  //   //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
  //   datepicker.close();
  // }

  getEndDate(i) {

    let array = this.offersForm?.get('offersArray') as FormArray;

    let date = array.controls[i]?.get('startDate')?.value;
    if (date) {
      date = new Date(date);
      date.setDate(date.getDate() + 1);
      return date;
    }
    else return new Date();
  }
  updatePriceValidation() {
    if (this.newProductUpload?.get('isPriceOnRequest')?.value == '0') {
      this.newProductUpload?.get('showroomPrice')?.setValidators(Validators.required);
      this.newProductUpload?.get('showroomPrice')?.updateValueAndValidity();
    } else {
      this.newProductUpload?.get('showroomPrice')?.setValue('');
      this.newProductUpload?.get('showroomPrice')?.clearValidators();
      this.newProductUpload?.get('showroomPrice')?.updateValueAndValidity();
    }
  }
  updateValidationsBasic() {
    if (this.newProductUpload) {
      if (this.newProductUpload?.get('category')?.value == 'Other') {
        this.newProductUpload.get('otherCategory')?.setValidators(Validators.required);
        this.newProductUpload.get('otherCategory')?.updateValueAndValidity();
      } else {
        this.newProductUpload.get('otherCategory')?.clearValidators();
        this.newProductUpload.get('otherCategory')?.updateValueAndValidity();
        this.newProductUpload.get('otherCategory')?.setValue('');
        this.newProductUpload.get('otherBrand')?.setValue('');
        this.newProductUpload.get('otherModel')?.setValue('');
      }
      if (this.newProductUpload?.get('brand')?.value == 'Other') {
        this.newProductUpload.get('otherBrand')?.setValidators(Validators.required);
        this.newProductUpload.get('otherBrand')?.updateValueAndValidity();
      } else {
        this.newProductUpload.get('otherBrand')?.clearValidators();
        this.newProductUpload.get('otherBrand')?.updateValueAndValidity();
        this.newProductUpload.get('otherBrand')?.setValue('');
        this.newProductUpload.get('otherModel')?.setValue('');
      }
      if (this.newProductUpload?.get('model')?.value == 'Other') {
        this.newProductUpload.get('otherModel')?.setValidators(Validators.required);
        this.newProductUpload.get('otherModel')?.updateValueAndValidity();
      } else {
        this.newProductUpload.get('otherModel')?.clearValidators();
        this.newProductUpload.get('otherModel')?.updateValueAndValidity();
        this.newProductUpload.get('otherModel')?.setValue('');
      }
    }
    // if (this.newProductUpload) {
    //   if (this.newProductUpload.get('category')?.value == '2') {
    //     this.newProductUpload.get('otherCategory')?.setValidators(Validators.required);
    //     this.newProductUpload.get('otherCategory')?.updateValueAndValidity();
    //   } else if (this.newProductUpload.get('category')?.value == '1') {
    //     this.newProductUpload.get('otherCategory')?.clearValidators();
    //     this.newProductUpload.get('otherCategory')?.updateValueAndValidity();
    //   }
    //   if (this.newProductUpload.get('brand')?.value == '1') {
    //     this.newProductUpload.get('otherBrand')?.clearValidators();
    //     this.newProductUpload.get('otherBrand')?.updateValueAndValidity();
    //   } else if (this.newProductUpload.get('brand')?.value == '2') {
    //     this.newProductUpload.get('otherBrand')?.setValidators(Validators.required);
    //     this.newProductUpload.get('otherBrand')?.updateValueAndValidity();
    //   }
    //   if (this.newProductUpload.get('model')?.value == '1') {
    //     this.newProductUpload.get('otherModel')?.clearValidators();
    //     this.newProductUpload.get('otherModel')?.updateValueAndValidity();
    //   } else if (this.newProductUpload.get('model')?.value == '2') {
    //     this.newProductUpload.get('otherModel')?.setValidators(Validators.required);
    //     this.newProductUpload.get('otherModel')?.updateValueAndValidity();
    //   }
    //   if (this.newProductUpload.get('isEquipmentRegistred')?.value == '1') {
    //     this.newProductUpload.get('registationNumber')?.setValidators(Validators.required);
    //     this.newProductUpload.get('registationNumber')?.updateValueAndValidity();
    //   } else if (this.newProductUpload.get('isEquipmentRegistred')?.value == '2') {
    //     this.newProductUpload.get('registationNumber')?.clearValidators();
    //     this.newProductUpload.get('registationNumber')?.updateValueAndValidity();
    //   }
    // }

  }



}
