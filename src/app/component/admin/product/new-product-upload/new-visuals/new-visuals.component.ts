import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormArray, FormGroup, FormBuilder } from "@angular/forms";
import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/services/common.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { finalize } from 'rxjs/operators';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { forkJoin } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';

@Component({
  selector: 'app-new-visuals',
  templateUrl: './new-visuals.component.html',
  styleUrls: ['./new-visuals.component.css']
})
export class NewVisualsComponent implements OnInit {

  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('videoInput') videoInput!: ElementRef;
  @Input() assetId: any;

  public viewingOptionsForm!: FormGroup;
  public tabsList: any;
  public assetVisualData: any;
  public cognitoId!: string;
  public sellerCognitoId!: string;
  public docType: { [key: string]: number } = {
    "IMAGE": 1,
    "VIDEO_LINK": 2,
    "VIDEO_UPLOAD": 3
  }
  public uploadVideoAndImages: Array<object> = [];
  public files: NgxFileDropEntry[] = [];
  public toEdit!: string;
  public firstImage: boolean = false;

  constructor(
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private s3: S3UploadDownloadService,
    private usedEquipmentService: UsedEquipmentService,
    public newEquipmentService: NewEquipmentService,
    private notificationservice: NotificationService,
    public storage: StorageDataService,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (this.storage.getLocalStorageData('newProductRecordById', true)) {
      this.assetId = this.storage.getLocalStorageData('newProductRecordById', true).id;
      this.sellerCognitoId = this.storage.getLocalStorageData('newProductRecordById', true).seller?.cognito_id;
    }
    this.cognitoId = this.storage.getLocalStorageData('cognitoId', false);
    this.viewingOptionsForm = this.fb.group({
      imageData: this.fb.array([]),
      videosData: this.fb.array([this.newVideo()])
    });
    this.route.queryParams.subscribe((data) => {
      this.toEdit = data.toEdit;
    })
    if (this.assetId) {
      this.getDetails(); // call getTabsList to get all tabs
    } else {
      this.getTabDetails();
    }
  }

  /**
   * Get video data array
   */
  get imageFormGroup() { return this.viewingOptionsForm.get("imageData") as FormArray; }
  get videosFormGroup() { return this.viewingOptionsForm.get("videosData") as FormArray; }
  get sourceImgFormGroup() { return this.imageFormGroup.controls; }
  get sourceVideoFormGroup() { return this.videosFormGroup.controls; }
  sourceImgFormGroup1(index) { return this.sourceImgFormGroup[index].get('images') as FormArray; }

  /**
   * New video for group
   */
  newImage(tab): FormGroup {
    return this.fb.group({
      id: [tab.id],
      label: [tab.category_name],
      imageUrl: [''],
      images: this.fb.array([])
    })
  }

  /**
   * New video for group
   */
  imagesGroup(data: any): FormGroup {
    return this.fb.group({
      id: [data.id],
      image_category: [data.image_category],
      is_primary: [data.is_primary],
      url: [data.url],
      new_equipment: [data.new_equipment],
      visual_type: [data.visual_type]
    })
  }

  /**
   * New video for group
   */
  newVideo(data?: any): FormGroup {
    return this.fb.group({
      id: [data ? data.id : ''],
      videoFile: [''],
      url: [data ? data.url : ''],
    })
  }

  getTabDetails() {
    this.newEquipmentService.getvisualTabs().pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.tabsList = res.results;
        if (this.tabsList.length) {
          this.tabsList.forEach((tab: any) => {
            this.imageFormGroup.push(this.newImage(tab));
          });
        }
      },
      err => {
        console.log(err?.message);
      }
    )
  }

  getDetails() {
    forkJoin([this.newEquipmentService.getvisualTabs(), this.newEquipmentService.getNewVisualsByEquipmentId(this.assetId, { limit: 999 })]).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.tabsList = res[0].results;
        this.assetVisualData = res[1].results;
        if (this.tabsList.length) {
          this.tabsList.forEach((tab: any) => {
            this.imageFormGroup.push(this.newImage(tab));
          });
        }
        if (this.assetVisualData.length) {
          this.setVisualData(this.assetVisualData);
        }

      },
      err => {
        console.log(err?.message);
      }
    )
  }

  setVisualData(data) {
    let videoObj = data.find(v => { return v.visual_type == 3 });
    let existingVideoObj = this.videosFormGroup.value[this.videosFormGroup.value.length - 1];
    if (videoObj && !existingVideoObj.url) {
      this.videosFormGroup.removeAt(this.videosFormGroup.value.length - 1);
    }
    data.forEach(element => {
      if (element.visual_type == 1) {
        let visualTab: any = this.sourceImgFormGroup.find((fg) => {
          return fg.value.id == element.image_category;
        })
        visualTab.get('images').push(this.imagesGroup(element));
      } else {
        if (element.visual_type == 3) {
          if (element?.url) {
            this.videosFormGroup.push(this.newVideo(element));
          }
        }
      }
    });
  }

  getVideoName(str) {
    return str.slice(str.lastIndexOf("/") + 3);
  }

  delete(file, index: number, imgIndex?: any) {
    let imgPath = file?.url.slice(file?.url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.newEquipmentService.deleteVisual(file.id).pipe(finalize(() => {   })).subscribe(
          res => {
            this.notificationservice.success('File Successfully deleted!!')
            if (file.visual_type == 1) {
              this.sourceImgFormGroup1(index).removeAt(imgIndex);
            } else {
              this.videosFormGroup.removeAt(index);
              if (index == 0) {
                this.addNewVideo();
              }
            }
          },
          err => {
            console.log(err);
            this.notificationservice.error(err.message);
          }
        )
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }

  /**
   * Add new video existing video data
   */
  addNewVideo() {
    this.videosFormGroup.push(this.newVideo());
  }

  /**
   * setPrimary
   */
  setPrimary(event, img) {
    let existingPrimaryImage: any;
    let primaryImage: any;
    if (event.checked) {
      this.sourceImgFormGroup.forEach((element: any) => {
        element.controls.images.controls.forEach((ele: any) => {
          if (ele.get('is_primary').value == true) {
            existingPrimaryImage = ele;
          }
          if (existingPrimaryImage && (existingPrimaryImage.value.url != img.url)) {
            existingPrimaryImage.patchValue({ is_primary: false })
          }
        })
      })
      primaryImage = this.assetVisualData.find(f => { return f.url == img.url });
    }
    let payload = {
      "equipment_id": JSON.parse(this.assetId),
      "equipment_type": 'new',
      "visual_id": primaryImage.id
    }
    this.usedEquipmentService.setPrimaryVisualUsedEquipment(payload).pipe(finalize(() => {   })).subscribe(
      data => {
        this.notificationservice.success("Image successfully set as Primary Image!!");
      }, err => {
        console.log(err);
        this.notificationservice.error(err?.message);
      }
    )
  }

  /**
   * Handle Image Upload
   * @param event 
   * @param tab 
   * @param side 
   * @param type 
   */
  async handleUpload(files: any, tab: number) {
    let visualsDataFrom: any = {};
    this.uploadVideoAndImages = [];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        var image_path = environment.bucket_parent_folder + "/marketplace/product/" + (this.toEdit ? this.sellerCognitoId : this.cognitoId) + '/' + this.assetId + '/' + tab + '_' + uuidv4() + '_' + file?.name.trim();
        var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

        if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
          visualsDataFrom = {
            visual_type: 1,
            url: uploaded_file.Location,
            new_equipment: this.assetId,
            image_category: tab,
            is_primary: false
          }
          if (this.assetVisualData.length) {
            let obj = this.assetVisualData.find(f => { return f.visual_type == 1 });
            if (obj) {
              this.firstImage = true;
            }
          }
          if (!this.firstImage && i == 0) {
            visualsDataFrom.is_primary = true;
            this.firstImage = true;
          }
          this.uploadVideoAndImages.push(visualsDataFrom);
        }
      } else {
        this.notificationservice.error("Invalid Image.")
      }
    }
    if (this.uploadVideoAndImages.length) {
      this.saveAssetData(this.uploadVideoAndImages);
    }
    this.imageInput.nativeElement.value = '';
  }

  /**
   * Handle video upload
   * @param event 
   * @param count 
   * @param side 
   * @param type 
   */
  async handleVideoUpload(event: any, count: any) {
    let visualsDataFrom: any = {};
    const file = event.target.files[0];
    if (file.type == 'video/mp4' || file.type == 'video/avi' || file.type == 'video/webm' || file.type == 'video/3gpp' || file.type == 'video/mov' || file.type == 'video/wmv') {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      var image_path = environment.bucket_parent_folder + "/marketplace/product/" + (this.toEdit ? this.sellerCognitoId : this.cognitoId) + '/' + this.assetId + '/' + count + uuidv4() + '_' + file?.name.trim();

      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        visualsDataFrom = {
          visual_type: 3,
          url: uploaded_file.Location,
          new_equipment: this.assetId,
          image_category: 1
        }
        this.saveAssetData([visualsDataFrom]);
      }
    } else {
      this.notificationservice.error("Invalid Video File.")
    }
    this.videoInput.nativeElement.value = '';
  }

  public dropped(files: NgxFileDropEntry[], tab: number) {
    this.files = files;
    let dropFiles: File[] = [];
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          dropFiles.push(file);
          if (dropFiles.length == files.length) {
            this.handleUpload(dropFiles, tab);
          }
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  saveAssetData(data: any) {
    this.newEquipmentService.postvisualsDetail(data).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.setVisualData(res);
        this.assetVisualData = [...res];
        this.notificationservice.success('File uploaded successfully!!');
      },
      (err) => {
        this.notificationservice.error(err);
      }
    );
  }

}
