import { Component, OnInit, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { ProductlistService } from 'src/app/services/product-list.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { ApiRouteService } from "../../../../utility/app.refrence"
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportPopupComponent } from '../../export-popup/export-popup.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-new-product-lists',
  templateUrl: './new-product-lists.component.html',
  styleUrls: ['./new-product-lists.component.css']
})

export class NewProductListsComponent implements OnInit {
  @ViewChild("TableOneSort", { static: true }) tableOneSort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public dataSource!: MatTableDataSource<any>;
  public searchText!: string;
  public apiRouteService!: ApiRouteService
  public activePage: any = 1;
  public pageSize: number = 10;
  public pager: any = {};
  public ordering: string = '-id';
  public filter: any;
  public total_count: any;
  public payload: any = {};
  public norecordvariable = false;
  userRole: string = "";
  public cognitoId?: string;
  public listingData = [];
  public displayedColumns: string[] = [
    "id", "category", "brand", "model", "selling_price", "seller", "mfg_year","created_at", "asset_status", "Actions"
  ];
  public statusOptions: Array<any> = [
    { name: 'Draft', value: 0 },
    { name: 'Pending', value: 1 },
    { name: 'Approved', value: 2 },
    { name: 'Sale in progress', value: 3 },
    { name: 'Rejected', value: 4 },
    { name: 'Sold', value: 5 }
  ];

  constructor( private ProductlistService: ProductlistService,
    private dialog: MatDialog,
      private http: HttpClient,
      private productUpload: NewEquipmentService,
      private route: ActivatedRoute,
      private router : Router,
      public notify: NotificationService,
      public spinner: NgxSpinnerService,
      private datePipe: DatePipe,
      public storage: StorageDataService) { }
  
  /**ng on init*/
  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole',false);
    this.getPage()
  }

  /**search from list */
  searchInList() {
    if (this.searchText === '' || this.searchText.length > 3) {
      this.getPage();
    }
  }
  
  /**sorting on list */
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getPage();
  }

  statusFilter(val) {
    this.filter = val;
    this.activePage = 1;
    this.getPage();
  }

  /**get data in table */
  getPage() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if(this.userRole == 'ChannelPartner'){
      payload['role'] = 'ChannelPartner';
      payload['cognito_id']= this.cognitoId;
  }
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
    this.ProductlistService.getNewProductWithList(payload).subscribe((res: any) => {
      if (res) {
        this.total_count = res.count;
        if(window.innerWidth > 768){
          this.dataSource.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.dataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.dataSource.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.dataSource.data = this.listingData;
          }
        }
        this.norecordshow();

      }
    })
  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){
      const triggerAt: number = 900; 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
        this.getPage();
        }
      }
    }
  }
}

  /**if no record found in table */
  norecordshow() {
    setTimeout(() => {
      if (this.dataSource.data.length === 0) {
        this.norecordvariable = true;
      }
      else {
        this.norecordvariable = false;
      }
    }, 2000);
  }

  /**on chnage pages pagination view */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPage();
  }

  /**chnage status of row */
  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    }else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    }else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  /**edit records navigation */
  editRecord(editableRow) {
    localStorage.setItem('newProductRecordById', JSON.stringify(editableRow));
    this.router.navigate(['/admin-dashboard/products/new-products-upload']);
  }

  deleteAsset(id){
   
    this.dialog
    .open(ConfirmDialogComponent)
    .afterClosed()
    .subscribe((data) => {
      if (data) {
        this.ProductlistService.deleteNewAsset(id).subscribe(res=>{
          this.notify.success('Asset Deleted Successfully!!');
          this.getPage();
        });
      }
    });
  }
  
  export() {
    const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '500px';
      dialogConfig.data = {
        flag: true
      }
    const dialogRef = this.dialog.open(ExportPopupComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          let payload = {
            
          }
          if(val.fromDate){
            payload['created_at__date__gte'] = this.datePipe.transform(val.fromDate,'yyyy-MM-dd');
          }
          if(val.toDate){
            payload['created_at__date__lte'] = this.datePipe.transform(val.toDate,'yyyy-MM-dd');
          }
          this.ProductlistService.exportNewProduct().pipe(finalize(() => {   })).subscribe(
            (res: any) => {
              const excelData = res.map((r: any) => {
                return {
                  "Asset ID": r?.id,
                  "Category": r?.category,
                  "Brand": r?.brand,
                  "Model": r?.model,
                  "Price": r?.selling_price,
                  "Seller": r?.seller,
                  "MFG Year": r?.mfg_year,
                  "Status": r?.status
                }
              });
              ExportExcelUtil.exportArrayToExcel(excelData, "New Asset List");
            },
            err => {
              console.log(err);
              this.notify.error(err.message);
            }
          );
        }
      })
    
  }

}
