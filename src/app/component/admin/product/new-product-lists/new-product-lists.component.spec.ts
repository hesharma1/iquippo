import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductListsComponent } from './new-product-lists.component';

describe('NewProductListsComponent', () => {
  let component: NewProductListsComponent;
  let fixture: ComponentFixture<NewProductListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
