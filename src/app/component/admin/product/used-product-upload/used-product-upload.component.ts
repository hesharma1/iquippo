import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/services/common.service';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { UsedProducts } from 'src/app/models/used-product.model';
import { StorageDataService } from './../../../../services/storage-data.service';
import { AgreementServiceService } from './../../../../services/agreement-service.service';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Moment } from 'moment';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
const moment = _moment;
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { documentData } from 'src/app/models/sellEquipment.model';



@Component({
  selector: 'app-used-product-upload',
  templateUrl: './used-product-upload.component.html',
  styleUrls: ['./used-product-upload.component.css'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },
  //   { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  // ],
})

export class UsedProductUploadComponent implements OnInit, OnDestroy {
  sellerTypeArray: Array<any> = [
    { typeName: 'Customer', value: 1 },
    { typeName: 'Dealer', value: 2 }
  ];

  @Output() ChdOutput: EventEmitter<string> = new EventEmitter();
  basicDetailsForm: FormGroup;
  sellerDetailsForm: FormGroup;
  docType: { [key: string]: number } = {
    "EQUIPMENT_DETAIL": 1,
    "SERVICE_LOG": 2,
    "INVOICE": 7
  };
  mfg_year: any
  relationshipManager;
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  isBuyerPremiumApplicable = false;
  submitted: any = false;
  sellerList: any = [];
  categoryList: any = [];
  brandList: any = [];
  modelList: any = [];
  serverError: Array<string> = [];
  sellerResult: any = [];
  cognitoId: any;
  stateList: any;
  editableRowData: any;
  isEdit: boolean = false;
  isApproved: boolean = false;
  categoryData: any;
  brandData: any;
  modalData: any;
  categoryId: any;
  brandId: any;
  modalId: any;
  documentName: Array<any> = [];
  serviceLogName: Array<any> = [];
  invoiceName: Array<any> = [];
  docDataList: Array<any> = [];
  docData;
  isEngineNumber = false;
  certificateList;
  StrOutputMSG: boolean = false;
  addVisual: boolean = false;
  assetId!: number;
  normalMaxDate: Date = new Date();
  normalMinDate: Date = new Date();
  todayDate = new Date();
  isPostFromVaahan: boolean = false;
  isComingSoon = false;
  sellerTypeValue: any;
  sellerRole: string = '';
  documentNameFile: any;
  serviceLogFile: any;
  invoiceFileName: any;
  userRole: string = "";
  isCreated = false;
  isMobileInvalid = false;
  percentPremium = true;
  constructor(
    private router: Router,
    private s3: S3UploadDownloadService,
    private storage: StorageDataService,
    private agreementServiceService: AgreementServiceService,
    private datePipe: DatePipe,
    private commonService: CommonService,
    private usedEquipmentService: UsedEquipmentService,
    private spinner: NgxSpinnerService,
    private adminMasterService: AdminMasterService,
    private notify: NotificationService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.todayDate.setDate(this.todayDate.getDate() + 1);
    this.getCategoryMasters();
    this.userRole = this.storage.getStorageData('userRole', false);
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.basicDetailsForm = this.fb.group({
      seller_type: new FormControl(''),
      name: new FormControl(''),
      mobile: new FormControl('', [Validators.required]),
      selleremail: new FormControl('', [ Validators.email]),
      isd_code: new FormControl('', Validators.required),
      alternate_email: new FormControl('', [
        Validators.email,
      ]),
      relationship_manager: new FormControl(''),
      category: new FormControl('', Validators.required),
      otherCategory: new FormControl(''),
      model: new FormControl('', Validators.required),
      otherModel: new FormControl(''),
      brand: new FormControl('', Validators.required),
      asset_address: new FormControl('', Validators.required),
      is_rc_available: new FormControl('', Validators.required),
      is_first_owned: new FormControl('', Validators.required),
      mfg_year: new FormControl('', Validators.required),
      customer_reference_number: new FormControl(''),
      location: new FormControl('', Validators.required),
      otherBrand: new FormControl(''),
      country: new FormControl('+91'),
      InvoiceNameChangeFee: new FormControl(''),
      premium_value1: new FormControl(''),
      premium_value2: new FormControl(''),
      buyerPremiumType: new FormControl(''),
      fuelType: new FormControl(''),
      alt_contact_number: new FormControl('', [Validators.minLength(10)]),
      asset_description: new FormControl(''),
      //rate_equipment: new FormControl('', Validators.required),
      asset_condition: new FormControl('', Validators.required),
      motor_operating_hours: new FormControl(''),
      mileage: new FormControl('', Validators.required),
      inspectionYN: new FormControl('', Validators.required),
      valuationValue: new FormControl(''),
      isServiceLogs: new FormControl(''),
      uploadServiceLogs: new FormControl(''),
      registrationNumber: new FormControl(''),
      isPriceOnRequest: new FormControl(''),
      chassisNumber: new FormControl(''),
      engineNumber: new FormControl(''),
      machineSerialNumber: new FormControl(''),
      grossWeight: new FormControl(''),
      operationWeight: new FormControl(''),
      bucketCapacity: new FormControl(''),
      liftingCapacity: new FormControl(''),
      //enableCost: new FormControl(''),
      isParkingCharges: new FormControl(''),
      parkedSince: new FormControl(''),
      RMName: new FormControl(''),
      //reservedprice: new FormControl(''),
      parkingChargePerDay: new FormControl('', Validators.compose([Validators.pattern('[1-9][0-9]+'), Validators.maxLength(10)])
      ),
      Product_name: new FormControl(''),
      ReservedPrice: new FormControl(''),
      SellPrice: new FormControl('', Validators.required),
      AvailabilityofOriginalInvoice: new FormControl(''),
      featured: new FormControl(''),
      is_active: new FormControl(''),
      certificate: new FormControl(''),
      is_insta_sale: new FormControl('', Validators.required),
      is_online_bidding: new FormControl(''),
      is_auction: new FormControl(''),
      is_coming_soon: new FormControl(''),
      insta_sale_price: new FormControl(''),
      last_date_insta_payment: new FormControl(''),
      last_date_bidding_payment: new FormControl(''),
      is_equipment_registered: new FormControl('',Validators.required),
      enginePower: new FormControl(''),
      enableCost:new FormControl(true),
    }, {
      validator: [ numberonly('enginePower'),
      numberonly('grossWeight'), numberonly('operationWeight'),
      numberonly('liftingCapacity'), numberonly('alt_contact_number'), numberonly('SellPrice')
        ,numberonly('ReservedPrice'),numberonly('parkingChargePerDay')] //numberonly('bookValue'), 
    });
    this.sellerDetailsForm = new FormGroup({
      individual: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
    });
    this.editableRowData = localStorage.getItem('usedProductRecordById');
    this.route.queryParams.subscribe((data) => {
      this.assetId = data.assetId;

    })

    if (!this.editableRowData && this.assetId) {
      this.getDetail(this.assetId);
    } else {
      if (this.editableRowData != null && this.editableRowData != undefined) {
        this.isEdit = true;
        this.editableRowData = JSON.parse(this.editableRowData);
        this.sellerTypeValue = this.editableRowData.seller_type;
        this.assetId = this.editableRowData?.id;
        // this.getVisualsDataById();
        if (this.editableRowData.status == 2 || this.editableRowData.status == 3) {
          this.isApproved = true;
        }
        this.addVisual = true;
        this.setFormValue(this.editableRowData);
        this.getDocuments();
        //this.checkBidAvailable();

      }

    }



  }

  getDetail(id: number) {
    //this.spinner.show();
    this.usedEquipmentService.getProduct(id).subscribe(
      (res: any) => {
        this.editableRowData = res;
        if (this.editableRowData != null && this.editableRowData != undefined) {
          this.isEdit = true;
          this.sellerTypeValue = this.editableRowData.seller_type;
          this.assetId = this.editableRowData?.id;
          // this.getVisualsDataById();
          if (this.editableRowData.status == 2 || this.editableRowData.status == 3) {
            this.isApproved = true;
          }
          this.addVisual = true;
          this.setFormValue(this.editableRowData);
          this.getDocuments();
          //this.checkBidAvailable();

        }

      });

  }

  ngOnInit(): void {
    this.getCertificateMasterList();
    this.usedEquipmentService.getRM().subscribe((res: any) => {
      this.relationshipManager = res.results;
    })
  }

  get f() {
    return this.basicDetailsForm.controls;
  }



  selectedMonth = new Date();

  chosenMonthHandler(normlizedMonth, datepicker) {
    var tempDate = JSON.stringify(normlizedMonth).replace("\"", '').split("-")
    var month = parseInt(tempDate[1])
    var year = month == 12 ? parseInt(tempDate[0]) + 1 : parseInt(tempDate[0])
    var year = month == 12 ? parseInt(tempDate[0]) + 1 : parseInt(tempDate[0])
    month = month == 12 ? 1 : month + 1;
    this.selectedMonth = new Date(year + "-" + month)
    console.log(year + " " + month)
    datepicker.close();
  }

  getDocuments() {
    this.usedEquipmentService.getDocumentList(this.editableRowData.id).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        var data = res.results;
        console.log("document data", data);
        this.docData = data;
        if (data.length > 0) {
          console.log("document", this.documentName);
          for (let document of data) {
            if (document.doc_type == 1) {
              this.documentName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
              console.log("document", this.documentName);
            } else if (document.doc_type == 2) {
              this.serviceLogName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
            } else {
              this.invoiceName.push({ name: document.doc_url, url: document.doc_url, id: document.id });
            }
          }
        }

      }
    );
  }
  

  setFormValue(dataObject: any) {
    this.todayDate = new Date();
    let name = dataObject.seller.first_name + " " + dataObject.seller.last_name;
    let category = dataObject.category.id != "" && dataObject.category.id != null ? dataObject.category.id.toString() : "";
    this.categoryId = dataObject.category.id;
    this.brandId = dataObject.brand.id;
    this.modalId = dataObject.model.id;
    this.onCategorySelect(dataObject.category);
    let brand = dataObject.brand.id != "" && dataObject.brand.id != null ? dataObject.brand.id.toString() : "";
    this.onBrandSelect(dataObject.brand);
    this.basicDetailsForm.patchValue({
      seller_type: this.sellerTypeValue,
      name: name,
      relationship_manager: dataObject.relationship_manager,
      mobile: dataObject.seller.mobile_number,
      selleremail: dataObject.seller.email,
      isd_code: dataObject?.seller.pin_code?.city?.state?.country?.isd_code,
      alternate_email: dataObject.seller.alt_email_address,
      category: dataObject.category.display_name != "" && dataObject.category.display_name != null ? dataObject.category.display_name.toString() : "",
      brand: dataObject.brand.display_name != "" && dataObject.brand.display_name != null ? dataObject.brand.display_name.toString() : "",
      model: dataObject.model.name != "" && dataObject.model.name != null ? dataObject.model.name.toString() : "",
      asset_address: dataObject.asset_address,
      AvailabilityofOriginalInvoice: dataObject.is_original_invoice ? "1" : "0",
      is_rc_available: dataObject.is_rc_available ? "true" : "false",
      is_first_owned: dataObject.is_first_owned ? "1" : "2",
      // mfg_year: dataObject.mfg_year != "" && dataObject.mfg_year != null ? dataObject.mfg_year : "",
      customer_reference_number: dataObject.customer_reference_number,
      location:  dataObject.pin_code?.pin_code?dataObject.pin_code?.pin_code:'',
      country: '+91',
      alt_contact_number: dataObject.alt_contact_number,
      // email_address : dataObject.email_address,
      // alt_email_address : dataObject.alt_email_address,
      asset_description: dataObject.asset_description,
      //rate_equipment: dataObject.rate_equipment != null && dataObject.rate_equipment != "" ? dataObject.rate_equipment.toString() : "",
      asset_condition: dataObject.asset_condition != null && dataObject.asset_condition != "" ? dataObject.asset_condition.toString() : "",
      motor_operating_hours: dataObject.motor_operating_hours,
      mileage: dataObject.hmr_kmr == '0.00' ? '' : dataObject.hmr_kmr,
      registrationNumber: dataObject.rc_number,
      chassisNumber: dataObject.chassis_number,
      engineNumber: dataObject.engine_number,
      machineSerialNumber: dataObject.machine_sr_number,
      grossWeight: dataObject.gross_Weight,
      operationWeight: dataObject.operating_weight,
      bucketCapacity: dataObject.bucket_capacity,
      liftingCapacity: dataObject.lifting_capacity,
      isParkingCharges: dataObject.is_parking_charges_info ? "1" : "0",
      parkedSince:this.datePipe.transform(dataObject.parked_since, 'yyyy-MM-dd'),
      parkingChargePerDay: dataObject.parking_charge_per_day,
      //bookValue: dataObject.book_value,
      Product_name: dataObject.product_name,
      ReservedPrice: dataObject.reserve_price,
      SellPrice: dataObject.selling_price,
      featured: dataObject.is_featured ? "1" : "0",
      is_active: dataObject.is_active ? "1" : "0",
      certificate: dataObject.certificate != "" && dataObject.certificate != null ? dataObject.certificate : "",
      otherCategory: dataObject.other_category,
      otherBrand: dataObject.other_brand,
      otherModel: dataObject.other_model,
      is_insta_sale: dataObject.is_insta_sale ? "1" : "0",
      is_online_bidding: dataObject.is_online_bidding ? "1" : "0",
      is_auction: dataObject.is_auction ? "1" : "0",
      is_coming_soon: dataObject.is_coming_soon ? "1" : "0",
      insta_sale_price: dataObject.insta_sale_price,
      last_date_insta_payment: dataObject.last_date_insta_payment,
      last_date_bidding_payment:dataObject.last_date_bid_payment,
      is_equipment_registered: dataObject.is_equipment_registered ? 1 : 0,
      valuationValue: dataObject.valuation_amount,
      isServiceLogs: dataObject.is_service_log_available ? '1' : '0',
      inspectionYN: JSON.stringify(dataObject.valuation_request),
      isPriceOnRequest: dataObject.is_price_on_request ? '1' : '0',
      enginePower: dataObject.engine_power,
      buyerPremiumType: dataObject.buyer_premium_type == 2 ? this.percentPremium = true : this.percentPremium = false,
      enableCost:dataObject.buyer_premium_type == 2 ? true : false,
      fuelType:dataObject.fuel_type == '' ? '' : dataObject.fuel_type

    });
    if( this.percentPremium) {
      this.basicDetailsForm.get('premium_value2')?.setValue(dataObject.buyer_premium);
    }
    if(!this.percentPremium) {
      this.basicDetailsForm.get('premium_value1')?.setValue(dataObject.buyer_premium);
    }
    if(dataObject.is_buyer_premium) {
      this.isBuyerPremiumApplicable = true;
    }
    this.checkBiddingPaymentDate(dataObject.is_online_bidding,dataObject.last_date_bid_payment); 
    this.checkInstaPaymentDate(dataObject.is_insta_sale,dataObject.last_date_insta_payment)  
    this.onOtherModelSelect();
    this.basicDetailsForm.get('mobile')?.disable();
    this.basicDetailsForm.get('selleremail')?.disable();
    this.basicDetailsForm.get('name')?.disable();
    this.basicDetailsForm.get('isd_code')?.disable();
    this.basicDetailsForm.get('alternate_email')?.disable();
    this.basicDetailsForm.get('seller_type')?.disable();
    if (!dataObject.is_parking_charges_info) {
      this.basicDetailsForm.get('parkedSince')?.clearValidators();
      this.basicDetailsForm.get('parkedSince')?.updateValueAndValidity();
    }
    if (dataObject.mfg_year) {
      let date = new Date();
      date.setFullYear(dataObject.mfg_year);
      this.mfg_year = date;
      this.basicDetailsForm.get('mfg_year')?.setValue(date);
    }
    this.getLocationData();
    if (dataObject.seller) {
      this.cognitoId = dataObject.seller.cognito_id;
    }
    if (dataObject.seller) {
      this.cognitoId = dataObject.seller.cognito_id;
    }
    if (this.basicDetailsForm.get('location')?.value != null) {
      this.validatePincode()
      this.basicDetailsForm.get('location')?.clearValidators();
      this.basicDetailsForm.get('location')?.updateValueAndValidity();
    }
  }

  
  sellerTypeSelect(value) {
    this.sellerList = [];
    this.basicDetailsForm.get('seller_type')?.setValue(value);
    if (value == 1) {
      this.sellerRole = 'Customer';
    }
    else {
      this.sellerRole = 'Dealer';
    }
    this.clearSeller();
  }


  enterPrice() {
    if( this.basicDetailsForm.get('SellPrice')?.value < 1) {
      this.notify.error('enter a valid selling price',false)
      this.basicDetailsForm.get('SellPrice')?.setValue('');
    }
  }


  Close() {
    this.ChdOutput.emit();//this.StrOutputMSG)
  }

  approve() {
    console.log(this.basicDetailsForm.get('last_date_insta_payment')?.value)
    this.basicDetailsForm.markAllAsTouched()
    if (this.editableRowData.brand && this.basicDetailsForm.valid) {
      this.editableRowData.status = 2;
      this.editableRowData.category = this.categoryId;
      this.editableRowData.brand = this.brandId;
      this.editableRowData.model = this.modalId;
      this.editableRowData.location = this.editableRowData.location.id;
      this.editableRowData.seller = this.editableRowData.seller.cognito_id;
      this.editableRowData.pin_code = this.editableRowData.pin_code.pin_code;
      this.usedEquipmentService.postupdateUsedEquipment(this.editableRowData, this.editableRowData.id).subscribe(res => {
        this.isApproved = true;
        localStorage.setItem('usedProductRecordById', JSON.stringify(this.editableRowData));
        //this.router.navigate(['/admin-dashboard/products/used-products']);
      })
    }else if(this.editableRowData.last_date_insta_payment == null || this.editableRowData.last_date_bid_payment == null) {
      this.notify.error("Please update the last date of payment to proceed further!",false)
    }else {
      this.notify.error("Please update the asset to proceed further!",false)
    }
  }

  reject() {
    this.basicDetailsForm.markAllAsTouched()
    if (this.basicDetailsForm.valid) {
    this.editableRowData.status = 4;
    this.editableRowData.category = this.categoryId;
    this.editableRowData.brand = this.brandId;
    this.editableRowData.model = this.modalId;
    this.editableRowData.location = this.editableRowData.location.id;
    this.editableRowData.seller = this.editableRowData.seller.cognito_id;
    this.editableRowData.pin_code = this.editableRowData.pin_code.pin_code;
    this.usedEquipmentService.postupdateUsedEquipment(this.editableRowData, this.editableRowData.id).subscribe(res => {
      this.isApproved = true;
      localStorage.setItem('usedProductRecordById', JSON.stringify(this.editableRowData));

      //this.router.navigate(['/admin-dashboard/products/used-products']);
    })
  } else if(this.editableRowData.last_date_insta_payment == null || this.editableRowData.last_date_bid_payment == null) {
    this.notify.error("Please update the last date of payment to proceed further!",false)
  } else {
    this.notify.error("Please update the asset to proceed further!",false)
  }
  }
   updateValidations() {
    if(this.basicDetailsForm){
      if(this.basicDetailsForm?.get('category')?.value == 'Other'){
        this.basicDetailsForm.get('otherCategory')?.setValidators(Validators.required);
        this.basicDetailsForm.get('otherCategory')?.updateValueAndValidity();
      }else{
        this.basicDetailsForm.get('otherCategory')?.clearValidators();
        this.basicDetailsForm.get('otherCategory')?.updateValueAndValidity();
        this.basicDetailsForm.get('otherCategory')?.setValue('');
        this.basicDetailsForm.get('otherBrand')?.setValue('');
        this.basicDetailsForm.get('otherModel')?.setValue('');
      }
      if(this.basicDetailsForm?.get('brand')?.value == 'Other'){
        this.basicDetailsForm.get('otherBrand')?.setValidators(Validators.required);
        this.basicDetailsForm.get('otherBrand')?.updateValueAndValidity();
      }else{
        this.basicDetailsForm.get('otherBrand')?.clearValidators();
        this.basicDetailsForm.get('otherBrand')?.updateValueAndValidity();
        this.basicDetailsForm.get('otherBrand')?.setValue('');
        this.basicDetailsForm.get('otherModel')?.setValue('');
      }
      if(this.basicDetailsForm?.get('model')?.value == 'Other'){
        this.basicDetailsForm.get('otherModel')?.setValidators(Validators.required);
        this.basicDetailsForm.get('otherModel')?.updateValueAndValidity();
      }else{
        this.basicDetailsForm.get('otherModel')?.clearValidators();
        this.basicDetailsForm.get('otherModel')?.updateValueAndValidity();
        this.basicDetailsForm.get('otherModel')?.setValue('');
      }
    }
    // if (this.basicDetailsForm) {
    //   if (this.basicDetailsForm.get('category')?.value == '2') {
    //     this.basicDetailsForm.get('otherCategory')?.setValidators(Validators.required);
    //     this.basicDetailsForm.get('otherCategory')?.updateValueAndValidity();
    //   } else if (this.basicDetailsForm.get('category')?.value == '1') {
    //     this.basicDetailsForm.get('otherCategory')?.clearValidators();
    //     this.basicDetailsForm.get('otherCategory')?.updateValueAndValidity();
    //   }
    //   if (this.basicDetailsForm.get('brand')?.value == '1') {
    //     this.basicDetailsForm.get('otherBrand')?.clearValidators();
    //     this.basicDetailsForm.get('otherBrand')?.updateValueAndValidity();
    //   } else if (this.basicDetailsForm.get('brand')?.value == '2') {
    //     this.basicDetailsForm.get('otherBrand')?.setValidators(Validators.required);
    //     this.basicDetailsForm.get('otherBrand')?.updateValueAndValidity();
    //   }
    //   if (this.basicDetailsForm.get('model')?.value == '1') {
    //     this.basicDetailsForm.get('otherModel')?.clearValidators();
    //     this.basicDetailsForm.get('otherModel')?.updateValueAndValidity();
    //   } else if (this.basicDetailsForm.get('model')?.value == '2') {
    //     this.basicDetailsForm.get('otherModel')?.setValidators(Validators.required);
    //     this.basicDetailsForm.get('otherModel')?.updateValueAndValidity();
    //   }
    //   if (this.basicDetailsForm.get('isEquipmentRegistred')?.value == '1') {
    //     this.basicDetailsForm.get('registationNumber')?.setValidators(Validators.required);
    //     this.basicDetailsForm.get('registationNumber')?.updateValueAndValidity();
    //   } else if (this.basicDetailsForm.get('isEquipmentRegistred')?.value == '2') {
    //     this.basicDetailsForm.get('registationNumber')?.clearValidators();
    //     this.basicDetailsForm.get('registationNumber')?.updateValueAndValidity();
    //   }
    // }

  }

  

  searchSeller(e) {
    let payload = {
      "number": e.target.value,
      "source": "bulk_upload",
      //"role": "Manufacturer,Dealer"
      "role": this.sellerRole ? this.sellerRole : 'Customer'
    }
    if (this.userRole == 'ChannelPartner') {
      payload['channel_partner_cognito_id'] = this.cognitoId;
    }
    this.sellerList = [];
    this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
      (res: any) => {
        if(res.data){
          this.sellerList = res.data;
          this.isMobileInvalid = false;
        }else{
          this.isMobileInvalid = true;
        }
        
        console.log(this.sellerList)
        return this.sellerList;
      },
      (error) => {
        console.log("eror", error);
        //  const msg = 'Somthing went wrong. Please try again!';
        //  const action = 'Error';
        this.isMobileInvalid = true;
        this.sellerList = [];
      }
    );
  }


  keyPress(event: any) {
    //    let inputChar = String.fromCharCode(event.charCode);
    this.sellerList = [];
      if(this.sellerList.length == 0 && this.basicDetailsForm.controls['mobile'].value.length <9) {
      this.sellerList = [];
      this.basicDetailsForm.get('isd_code')?.setValue('');
      this.basicDetailsForm.get('selleremail')?.setValue('');
      this.basicDetailsForm.get('alternate_email')?.setValue('');
      this.basicDetailsForm.get('name')?.setValue('');
      this.basicDetailsForm.get('RMName')?.setValue('');
    }
    else {
      this.searchSeller(event);
      
    }
  }

  
  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      if (res.mobile_number == search) {
        this.selectSeller(res);
      }
    });
  }

  selectSeller(seller) {
    //this.basicDetailsForm.get('seller_type')?.setValue(1);
    console.log("seller", seller);
    this.basicDetailsForm.get('isd_code')?.setValue('+91');
    this.basicDetailsForm.get('selleremail')?.setValue(seller.email);
    this.basicDetailsForm?.get('mobile')?.setValue(seller.mobile_number);
    this.basicDetailsForm?.get('mobile')?.clearValidators();
      this.basicDetailsForm?.get('mobile')?.updateValueAndValidity();
    this.cognitoId = seller.cognito_id;
    this.basicDetailsForm.get('name')?.setValue(seller.first_name);
    this.basicDetailsForm.get('RMName')?.setValue(seller.relationship_manager);
    if (!seller.alt_email_address) {
      this.basicDetailsForm.get('alternate_email')?.setValue(seller.email);
    } else {
      this.basicDetailsForm.get('alternate_email')?.setValue(seller.alt_email_address);
    }
    this.isMobileInvalid = false;
    if(this.basicDetailsForm.get('name')?.value == ''){
      this.isMobileInvalid = true;
    }
  }

  /**
   * Submit form
   */
  async onSubmit() {
    this.submitted = true;
    // Check for error validation
    this.basicDetailsForm.markAllAsTouched();
    if (this.basicDetailsForm.invalid) {
      return;
    }
    if (this.basicDetailsForm.get('is_online_bidding')?.value == '0' && this.basicDetailsForm.get('is_insta_sale')?.value == '0' &&
      this.basicDetailsForm.get('is_auction')?.value == '0' && this.basicDetailsForm.get('is_coming_soon')?.value == '0') {
      return;
    }
    if (this.addVisual && this.assetId && !this.isEdit) {
      let isVisuals = await this.checkVisuals();
      if (!isVisuals && this.basicDetailsForm.get('is_auction')?.value == '0') {
        this.notify.error('Please upload atleast one image for the asset!!');
        return;
      }
    }
    if((this.basicDetailsForm.controls['is_equipment_registered']?.value == 0 || this.basicDetailsForm.controls['is_equipment_registered']?.value == null) && 
    (this.basicDetailsForm.controls['chassisNumber'].value == '' || this.basicDetailsForm.controls['chassisNumber'].value == null) &&
       (this.basicDetailsForm.controls['machineSerialNumber'].value == '' || this.basicDetailsForm.controls['machineSerialNumber'].value == null) && 
       (this.basicDetailsForm.controls['engineNumber']?.value == '' || this.basicDetailsForm.controls['engineNumber']?.value == null)) {
         console.log(this.basicDetailsForm.value);
        this.notify.error("Kindly fill atleast one of the three- Engine no, Chassis No and Machine serial no",false);
        return
    } else {
      this.submitUpdateAsset();

    }
    
  }

  submitUpdateAsset() {
    console.log(this.assetId)
    var usedProduct = new UsedProducts();
    usedProduct.seller = this.cognitoId;
    // Load used product model data.
    usedProduct.seller_type = this.sellerTypeValue;
    usedProduct.relationship_manager = this.basicDetailsForm.controls['relationship_manager'].value ? this.basicDetailsForm.controls['relationship_manager'].value : '';
    usedProduct.mobile = this.basicDetailsForm.controls['mobile'].value;
    usedProduct.email = this.basicDetailsForm.controls['selleremail'].value;
    usedProduct.name = this.basicDetailsForm.controls['name'].value;
    usedProduct.isd_code = this.basicDetailsForm.controls['isd_code'].value;
    usedProduct.alternate_email = this.basicDetailsForm.controls['alternate_email'].value;
    usedProduct.category = this.categoryId;
    usedProduct.other_category = this.basicDetailsForm.controls['otherCategory'].value;
    usedProduct.brand = this.brandId;
    usedProduct.other_brand = this.basicDetailsForm.controls['otherBrand'].value;
    usedProduct.model = this.modalId;
    usedProduct.other_model = this.basicDetailsForm.controls['otherModel'].value;
    usedProduct.is_equipment_registered = this.basicDetailsForm.controls['is_equipment_registered']?.value == 1 ? true : false;
    if (usedProduct.is_equipment_registered)
      usedProduct.rc_number = this.basicDetailsForm.controls['registrationNumber'].value;
    usedProduct.is_rc_available = this.basicDetailsForm.controls['is_rc_available'].value;
    usedProduct.is_first_owned = this.basicDetailsForm.controls['is_first_owned'].value == 1 ? true : false;
    if(this.basicDetailsForm.controls['valuationValue'].value)
    usedProduct.valuation_amount = this.basicDetailsForm.controls['valuationValue'].value;

    let date = this.basicDetailsForm.controls['mfg_year'].value
    usedProduct.mfg_year = this.datePipe.transform(date, 'yyyy');
    usedProduct.customer_reference_number = this.basicDetailsForm.controls['customer_reference_number'].value;
    usedProduct.pin_code = this.basicDetailsForm.controls['location'].value;
    usedProduct.asset_address = this.basicDetailsForm.controls['asset_address'].value;
    usedProduct.alt_contact_number = this.basicDetailsForm.controls['alt_contact_number'].value;
    usedProduct.is_service_log_available = this.basicDetailsForm.controls['isServiceLogs'].value == 1 ? true : false;
    // usedProduct.email_address = this.basicDetailsForm.controls[
    //   'email_address'
    // ].value;
    // usedProduct.alt_email_address = this.basicDetailsForm.controls[
    //   'alt_email_address'
    // ].value;
    usedProduct.asset_description = this.basicDetailsForm.controls['asset_description'].value;
    //usedProduct.rate_equipment = this.basicDetailsForm.controls['rate_equipment'].value;
    usedProduct.asset_condition = this.basicDetailsForm.controls['asset_condition'].value;
    if(this.basicDetailsForm.controls['motor_operating_hours'].value)
    usedProduct.motor_operating_hours = this.basicDetailsForm.controls['motor_operating_hours'].value;
    if(this.basicDetailsForm.controls['mileage'].value)
    usedProduct.hmr_kmr = this.basicDetailsForm.controls['mileage'].value;
    usedProduct.valuation_request = this.basicDetailsForm.controls['inspectionYN'].value;
    usedProduct.chassis_number = this.basicDetailsForm.controls['chassisNumber'].value;
    usedProduct.engine_number = this.basicDetailsForm.controls['engineNumber'].value;
    usedProduct.registrationNumber = this.basicDetailsForm.controls['registrationNumber'].value;
    usedProduct.machine_sr_number = this.basicDetailsForm.controls['machineSerialNumber'].value;
    if (this.basicDetailsForm.controls['bucketCapacity'].value != "") {
      usedProduct.bucket_capacity = this.basicDetailsForm.controls['bucketCapacity'].value;
    }
    if (this.basicDetailsForm.controls['liftingCapacity'].value != "") {
      usedProduct.lifting_capacity = this.basicDetailsForm.controls['liftingCapacity'].value;
    }
    if (this.basicDetailsForm.controls['grossWeight'].value != "") {
      usedProduct.gross_Weight = this.basicDetailsForm.controls['grossWeight'].value;
    }
    if (this.basicDetailsForm.controls['operationWeight'].value != "") {
      usedProduct.operating_weight = this.basicDetailsForm.controls['operationWeight'].value;
    }
    if (this.basicDetailsForm.controls['enginePower'].value != "") {
      usedProduct.engine_power = this.basicDetailsForm.controls['enginePower'].value;
    }
    //usedProduct.book_value = this.basicDetailsForm.controls['bookValue'].value;
    if (this.basicDetailsForm.controls['isParkingCharges'].value != "0") {
      usedProduct.parked_since = this.datePipe.transform(this.basicDetailsForm.controls['parkedSince'].value, 'yyyy-MM-dd');
      usedProduct.parking_charge_per_day = this.basicDetailsForm.controls['parkingChargePerDay'].value;
    }
    usedProduct.rm_name = this.basicDetailsForm.controls['RMName'].value;
    usedProduct.product_name = this.basicDetailsForm.controls['Product_name'].value;
    usedProduct.selling_price = this.basicDetailsForm.controls['SellPrice'].value;
    if(this.basicDetailsForm.controls['ReservedPrice'].value)
    usedProduct.reserve_price = this.basicDetailsForm.controls['ReservedPrice'].value;
    usedProduct.is_original_invoice = this.basicDetailsForm.controls['AvailabilityofOriginalInvoice'].value == 1 ? true : false;
    usedProduct.is_featured = this.basicDetailsForm.controls['featured'].value == 1 ? true : false;
    usedProduct.is_active = this.basicDetailsForm.controls['is_active'].value == 1 ? true : false;
    usedProduct.certificate = this.basicDetailsForm.controls['certificate'].value ? this.basicDetailsForm.controls['certificate'].value : [];
    usedProduct.info_level = 3;
    usedProduct.status = this.editableRowData?.status ? this.editableRowData?.status : 1;
    usedProduct.is_insta_sale = this.basicDetailsForm.controls['is_insta_sale'].value == "1" ? true : false,
    usedProduct.is_price_on_request = this.basicDetailsForm.controls['isPriceOnRequest'].value == "1" ? true : false,
    usedProduct.is_online_bidding = this.basicDetailsForm.controls['is_online_bidding'].value == "1" ? true : false,
    usedProduct.is_auction = this.basicDetailsForm.controls['is_auction'].value == "1" ? true : false,
    usedProduct.is_valuation = this.editableRowData ? this.editableRowData.is_valuation : false,
    usedProduct.is_coming_soon = this.basicDetailsForm.controls['is_coming_soon'].value == "1" ? true : false,
    usedProduct.insta_sale_price = this.basicDetailsForm.controls['SellPrice'].value,
    usedProduct.last_date_insta_payment = this.datePipe.transform(this.basicDetailsForm.controls['last_date_insta_payment'].value, 'yyyy-MM-dd');
    usedProduct.last_date_bid_payment = this.datePipe.transform(this.basicDetailsForm.controls['last_date_bidding_payment'].value, 'yyyy-MM-dd');
    if(this.basicDetailsForm.controls['fuelType'].value != '') {
      usedProduct.fuel_type = this.basicDetailsForm.controls['fuelType'].value
    }
    if (this.basicDetailsForm.controls['isParkingCharges'].value != "0") {
      usedProduct.is_parking_charges_info = this.basicDetailsForm.controls['isParkingCharges'].value;
    }
    if (this.percentPremium) {
      usedProduct.buyer_premium = this.basicDetailsForm.controls['premium_value2'].value;
      usedProduct.buyer_premium_type = this.percentPremium ? 2 : 1;
    }
    if (!this.percentPremium) {
      usedProduct.buyer_premium = this.basicDetailsForm.controls['premium_value1'].value;
      usedProduct.buyer_premium_type = this.percentPremium ? 2 : 1;
    }
    // now save equipment data
    if (this.sellerResult) {
      if (this.assetId) {
        this.usedEquipmentService.postupdateUsedEquipment(usedProduct, this.assetId).subscribe(
          (results) => {
            if (results) {
              const equipmentData: any = results;
              if(this.editableRowData == null || this.editableRowData == undefined) {
                this.notify.success('Data has been successfully created');

              } else {
                this.notify.success('Data has been successfully updated');
              }
              this.editableRowData = equipmentData;
              this.editableRowData.id = this.assetId;
              if (this.isPostFromVaahan) {
                this.approve();
              }
              localStorage.removeItem('usedProductRecordById');
             
              //this.router.navigate(['/admin-dashboard/products/used-products']);
            }
          },
          (error) => {
            this.notify.error('Somthing went wrong. Please try again!');
          }
        );
      } else {
        this.usedEquipmentService.postUsedEquipment(usedProduct).subscribe(
          (results: any) => {
            if (results) {
              const equipmentData: any = results;
              this.assetId = results?.id;
              this.addVisual = true
              for (let item of this.docDataList) {
                item.used_equipment = this.assetId;
                this.uploadDocument(item);
              }
              this.notify.success('Data has been successfully saved');
              this.editableRowData = results;
              this.editableRowData.id = this.assetId;
              this.approve();
              // this.router.navigate(['/admin-dashboard/products/used-products']);
            }
          },
          (error) => {
            this.notify.error('Somthing went wrong. Please try again!');
          }
        );
      }
    }
  }
  registrationValidation(value) {
    if (value == 1) {
      this.isEngineNumber = true;
      this.basicDetailsForm?.get('engineNumber')?.clearValidators();
      this.basicDetailsForm?.get('engineNumber')?.updateValueAndValidity();
    } else {
      this.isEngineNumber = false;
      this.basicDetailsForm?.get('engineNumber')?.setValidators(Validators.required);
      this.basicDetailsForm?.get('engineNumber')?.updateValueAndValidity();
    }
  }

  checkVisuals() {
    return new Promise((resolved, reject) => {
      this.usedEquipmentService.getVisualsByEquipmentId(this.assetId, { limit: 999 }).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res?.results?.length) {
            let obj = res?.results.find(f => { return f.visual_type == 1 })
            if (obj) {
              resolved(true);
            } else {
              resolved(false);
            }
          } else {
            resolved(false);
          }
        },
        err => {
          console.log(err?.message);
          reject(err);
        }
      );
    });
  }




  preview() {
    let state = 'preview';
    localStorage.setItem('preview', state);
    localStorage.setItem('redirectFromAdmin', 'true');
    this.router.navigate(['used-equipment-dashboard/equipment-details', this.editableRowData.id]);
  }


  getCategoryMasters() {
    this.commonService.getCategoryMaster('limit=999').subscribe(
      (res: any) => {
        this.categoryList = res.results;
        this.categoryData = res.results;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  /*** get state data*/
  getLocationData() {
    let pincode = this.basicDetailsForm?.get('location')?.value;
    let queryParam = `search=${pincode}`;
    if (pincode && pincode.length > 4) {
      this.agreementServiceService.getPinCode(queryParam).subscribe(
        (res: any) => {
          this.stateList = res.results;
        },
        (err) => {
          console.error(err);
        }
      );
    }
  }

  validatePincode() {
    let pincode = this.basicDetailsForm?.get('location')?.value;
    if (this.stateList) {
      let pinvalid = this.stateList.filter(e => e.pin_code == pincode)
      if (pinvalid?.length > 0) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  filterValuesCategory(search: any) {
    let data = search.target.value
    let filtered = this.categoryData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.categoryList = filteredData
    } else {
      this.categoryList = []
    }
  }

  filterValuesBrand(search: any) {
    let data = search.target.value
    let filtered = this.brandData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.brandList = filteredData
    } else {
      this.brandList = [];
    }
  }

  filterValuesModal(search: any) {
    let data = search.target.value
    let filtered = this.modalData;
    let filteredData: any
    if (filtered && filtered.length > 0) {
      filteredData = filtered.filter(
        item => item.name.toLowerCase().includes(data.toLowerCase())
      );
    }
    if (filteredData && filteredData.length > 0) {
      this.modelList = filteredData
    } else {
      this.modelList = []
    }
  }

  onModalSelect(modal: any) {
    this.modalId = modal.id;
    this.basicDetailsForm.get('model')?.setValue(modal.name);
    this.onOtherModelSelect();
  }

  onOtherModelSelect() {
    this.basicDetailsForm.get('Product_name')?.setValue(
      (this.basicDetailsForm?.get('category')?.value == 'Other' ?
        this.basicDetailsForm.get('otherCategory')?.value : this.basicDetailsForm?.get('category')?.value) + ' ' + (
        this.basicDetailsForm?.get('brand')?.value == 'Other' ? this.basicDetailsForm.get('otherBrand')?.value :
          this.basicDetailsForm.get('brand')?.value) + ' ' + (this.basicDetailsForm?.get('model')?.value == 'Other' ?
            this.basicDetailsForm.get('otherModel')?.value : this.basicDetailsForm.get('model')?.value));
  }

  onCategorySelect(categoryId) {
    if (categoryId.id != this.categoryId) {
      this.basicDetailsForm.get('model')?.setValue('');
      this.basicDetailsForm.get('brand')?.setValue('');
      this.basicDetailsForm.get('category')?.setValue(categoryId.display_name);
      this.categoryId = categoryId.id
      if (this.categoryId != 0) {
        this.commonService.getBrandMasterByCatId(this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.brandList = res.results;
            this.brandData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.basicDetailsForm.get('category')?.setValue(categoryId.display_name);
    }

  }

  onBrandSelect(brandId: any) {
    if (brandId.id != this.brandId) {
      this.brandId = brandId.id;
      this.basicDetailsForm.get('model')?.setValue('');
      this.basicDetailsForm.get('brand')?.setValue(brandId.display_name);
      if (this.brandId != 0) {
        this.commonService.getModelMasterByBrandId(brandId.id, this.categoryId).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            this.modelList = res.results;
            this.modalData = res.results;
          },
          err => {
            console.error(err);
          });
      }
    } else {
      this.basicDetailsForm.get('brand')?.setValue(brandId.display_name);
    }
  }

  backToList() {
    localStorage.removeItem('usedProductRecordById');
    this.router.navigate(['/admin-dashboard/products/used-products']);
  }

  ngOnDestroy() {
    localStorage.removeItem('usedProductRecordById');
  }

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.basicDetailsForm.get('mfg_year');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.basicDetailsForm?.get('mfg_year')?.setValue(ctrlValue?.value);
    datepicker.close();
  }

  chosenParkSinceYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.basicDetailsForm.get('parkedSince');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.basicDetailsForm?.get('parkedSince')?.setValue(ctrlValue?.value);
    datepicker.close();
  }

  makePriceMandatory() {
    if (this.basicDetailsForm.get('is_insta_sale')?.value == '1') {
      this.basicDetailsForm.get('isPriceOnRequest')?.setValue('0');
      this.basicDetailsForm.get('SellPrice')?.setValidators(Validators.required);
      this.basicDetailsForm.get('SellPrice')?.updateValueAndValidity();
      // this.basicDetailsForm.get('bookValue')?.setValidators(Validators.required);
      // this.basicDetailsForm.get('bookValue')?.updateValueAndValidity();
    }
  }

  disableChecks(value) {
    if (value == 1) {
      this.basicDetailsForm.get('is_insta_sale')?.setValue('0');
      this.basicDetailsForm.get('is_auction')?.setValue('0');
      this.basicDetailsForm.get('is_online_bidding')?.setValue('0');
      // this.isComingSoon = true;
      this.basicDetailsForm.get('is_insta_sale')?.clearValidators();
      this.basicDetailsForm.get('is_insta_sale')?.updateValueAndValidity();
      this.basicDetailsForm.get('is_auction')?.clearValidators();
      this.basicDetailsForm.get('is_auction')?.updateValueAndValidity();
      this.basicDetailsForm.get('is_online_bidding')?.clearValidators();
      this.basicDetailsForm.get('is_online_bidding')?.updateValueAndValidity();
      this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
    } else {
      // this.isComingSoon = false;
      this.basicDetailsForm.get('is_insta_sale')?.setValue('1');
      this.basicDetailsForm.get('last_date_insta_payment')?.setValidators(Validators.required);
      this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
      this.basicDetailsForm.get('is_auction')?.setValue('1');
      this.assetId != null && this.assetId != undefined ? this.checkBidAvailable() : this.basicDetailsForm.get('is_online_bidding')?.setValue('0');
      if(this.basicDetailsForm.get('is_online_bidding')?.value == 0) {
        this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
      }
    }
    
     
    }
  
    checkParkingValidation(value) {
      if(value == 1) {
        this.basicDetailsForm.get('parkedSince')?.setValidators(Validators.required);
        this.basicDetailsForm.get('parkedSince')?.updateValueAndValidity();
        this.basicDetailsForm.get('parkingChargePerDay')?.setValidators(Validators.required);
        this.basicDetailsForm.get('parkingChargePerDay')?.updateValueAndValidity();
      } else {
        this.basicDetailsForm.get('parkedSince')?.setValue('');
        this.basicDetailsForm.get('parkedSince')?.clearValidators();
        this.basicDetailsForm.get('parkedSince')?.updateValueAndValidity();
        this.basicDetailsForm.get('parkingChargePerDay')?.setValue('');
        this.basicDetailsForm.get('parkingChargePerDay')?.clearValidators();
        this.basicDetailsForm.get('parkingChargePerDay')?.updateValueAndValidity();
      }
    }

  resetFile(i, url) {
    console.log("url", url);
    let id = this.documentName[i].id;
    this.documentName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(res => {
      this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {
      })
    })
  }

  checkReservePrice(value) {
    this.basicDetailsForm.get('is_auction')?.setValue('0');
    this.basicDetailsForm.get('is_insta_sale')?.setValue('0');
    this.basicDetailsForm.get('is_coming_soon')?.setValue('0');

    this.basicDetailsForm.get('last_date_insta_payment')?.clearValidators();
    this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
    //  if(value == 1) {
    //    this.notify.warn("You need to enter the Selling price before placing an asset for bidding",false);
    // }
  }

  resetServiceFile(i, url) {
    console.log("url", url);
    console.log("i", i);
    let id = this.serviceLogName[i].id;
    this.serviceLogName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(res => {
      this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {

      })
    })
  }

  resetinvoiceFile(i, url) {
    console.log("url", url);
    console.log("i", i);
    let id = this.invoiceName[i].id;
    this.invoiceName.splice(i, 1);
    let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(res => {
      this.usedEquipmentService.deletedocumentUploadUsedEquipment(id).subscribe(res => {

      })
    })
  }

  getCertificateMasterList() {
    let payload: any = {
      limit: 999
    }
    this.adminMasterService.getCertificateMasterList(payload).subscribe((res: any) => {
      this.certificateList = res.results;
    })
  }

  getStatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    } else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    } else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  postVaahanDetails(value) {
    //console.log(this.basicDetailsForm.get('otherCategory')?.value;)
    if (!this.editableRowData) {
      var usedProduct = new UsedProducts();
      usedProduct.seller = this.cognitoId;
      usedProduct.category = this.categoryId;
      usedProduct.brand = this.brandId;
      usedProduct.model = this.modalId;
      usedProduct.seller_type = this.sellerTypeValue;
      if (this.basicDetailsForm.get('otherCategory')?.value) {
        usedProduct.other_category = this.basicDetailsForm.get('otherCategory')?.value;
      }
      if (this.basicDetailsForm.get('otherBrand')?.value) {
        usedProduct.other_brand = this.basicDetailsForm.get('otherBrand')?.value;
      }
      if (this.basicDetailsForm.get('otherModel')?.value) {
        usedProduct.other_model = this.basicDetailsForm.get('otherModel')?.value;
      }
      usedProduct.is_equipment_registered = value == 1 ? true : false;
      if (value == 1)
        usedProduct.rc_number = this.basicDetailsForm.controls['registrationNumber'].value;
      if (usedProduct && usedProduct.seller && usedProduct.category && usedProduct.brand && usedProduct.model && this.basicDetailsForm.get('selleremail')?.value) {
        this.usedEquipmentService.postUsedEquipment(usedProduct).subscribe(
          (response: any) => {
            this.isPostFromVaahan = true;
            this.usedEquipmentService.getProduct(response.id).subscribe(
              (res: any) => {
                this.editableRowData = res;

                this.setFormValue(this.editableRowData);
                this.assetId = this.editableRowData.id;
                this.addVisual = true;
                if(this.editableRowData.is_buyer_premium) {
                  this.isBuyerPremiumApplicable = true;
                }
                localStorage.setItem('usedProductRecordById', JSON.stringify(this.editableRowData));
              });
          });
      }
    }
  }

  


  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    // if (tab == 'EQUIPMENT_DETAIL') {
    //   this.documentNameFile = file?.name;
    // }
    // else if (tab == 'SERVICE_LOG') {
    //   this.serviceLogFile = file?.name;
    // }
    // else {
    //   this.invoiceFileName = file?.name;
    // }
    this.isInvalidfile = true;
    this.uploadingImage = true;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
    }
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;
    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      // this.vehicleDetailsForm.get('uploadDocument')?.setValue('');
      var documentDataRequest = new documentData();
      documentDataRequest.doc_type = this.docType[tab];
      documentDataRequest.doc_url = uploaded_file.Location;
      documentDataRequest.used_equipment = this.editableRowData && this.editableRowData.id ? this.editableRowData.id : ''
      console.log("documentDataRequest", documentDataRequest);
      if (this.editableRowData && this.editableRowData.id) {
        this.uploadDocument(documentDataRequest);
      } else {
        this.docDataList.push(documentDataRequest);
      }
      if (this.docType[tab] == 1) {
        this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      } else if (this.docType[tab] == 2) {
        this.serviceLogName.push({ name: fileFormat, url: uploaded_file.Location });
      } else {
        this.invoiceName.push({ name: fileFormat, url: uploaded_file.Location });
      }
      //this.uploadDocumentPath.push(documentDataRequest);
    }
    this.uploadingImage = false;
  }

  uploadDocument(data: any) {
    //this.spinner.show();
    this.usedEquipmentService.postdocumentUploadUsedEquipment(data).pipe(finalize(() => { })).subscribe(
      (res: any) => {
      });
  }
  checkPrice() {
    if ((this.basicDetailsForm.get('isPriceOnRequest')?.value == '1' || this.basicDetailsForm.get('SellPrice')?.value == '') && !this.isComingSoon) {
      this.basicDetailsForm.get('is_insta_sale')?.setValue('0');
      this.basicDetailsForm.get('last_date_insta_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
     
    }
    this.basicDetailsForm.get('is_online_bidding')?.setValue('0');
    this.basicDetailsForm.get('is_auction')?.setValue('0');
    this.basicDetailsForm.get('is_coming_soon')?.setValue('0');
    this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
    this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
  }

  validateLastDate() {
    if (this.basicDetailsForm.get('is_insta_sale')?.value == 0) {
      this.basicDetailsForm.get('last_date_insta_payment')?.setValidators(Validators.required);
      this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
    } else {
      this.basicDetailsForm.get('last_date_insta_payment')?.setValue('');
      this.basicDetailsForm.get('last_date_insta_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
    }
  }

  validateBidLastDate() {
    if (this.basicDetailsForm.get('is_online_bidding')?.value == 0) {
      this.basicDetailsForm.get('last_date_bidding_payment')?.setValidators(Validators.required);
      this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
    } else {
      this.basicDetailsForm.get('last_date_bidding_payment')?.setValue('');
      this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
    }
  }

  checkAuction() {
    this.basicDetailsForm.get('is_online_bidding')?.setValue('0');
    this.basicDetailsForm.get('is_insta_sale')?.setValue('0');
    this.basicDetailsForm.get('is_coming_soon')?.setValue('0');
    this.basicDetailsForm.get('last_date_insta_payment')?.clearValidators();
    this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
    this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
      this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
  }

  checkBidAvailable() {
    if (this.editableRowData != null && this.editableRowData != undefined) {
      if (this.editableRowData.no_of_active_bids && this.editableRowData.no_of_active_bids != 0) {
        //this.markInstaSale()
        this.basicDetailsForm.get('is_online_bidding')?.setValue('1');
        this.basicDetailsForm.get('is_auction')?.setValue('0');
        this.basicDetailsForm.get('is_insta_sale')?.setValue('0');
        this.basicDetailsForm.get('last_date_insta_payment')?.clearValidators();
        this.basicDetailsForm.get('last_date_insta_payment')?.updateValueAndValidity();
        // this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
        // this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
        this.basicDetailsForm.get('is_coming_soon')?.setValue('0');
        this.notify.warn('There are active bids against this asset so disposal mode cannot be changed, If required active bids can be cancelled then only disposal mode can be updated.');
      } else {
        this.basicDetailsForm.get('is_online_bidding')?.setValue('0');
         this.basicDetailsForm.get('last_date_bidding_payment')?.clearValidators();
        this.basicDetailsForm.get('last_date_bidding_payment')?.updateValueAndValidity();
      }
    }
  }

  markInstaSale() {
    this.notify.error('Bid is proceed for the item please contact for the further asistant');
  }

  checkValuatioAmount() {
    if(this.basicDetailsForm?.get('valuationValue')?.value > 2147483647) {
      this.notify.error("Max valuation amount is 2147483647",false);
      this.basicDetailsForm?.get('valuationValue')?.setValue('')
    } else {
      this.basicDetailsForm?.get('valuationValue')?.clearValidators();
      this.basicDetailsForm?.get('valuationValue')?.updateValueAndValidity();
    }
  }

  parkingCharges() {
    if(this.basicDetailsForm?.get('parkingChargePerDay')?.value > 65535) {
      this.notify.error("Max Parking charge is 65535",false);
      this.basicDetailsForm?.get('parkingChargePerDay')?.setValue('')
      // this.basicDetailsForm?.get('parkingChargePerDay')?.setValidators(Validators.required);
      // this.basicDetailsForm?.get('parkingChargePerDay')?.updateValueAndValidity();
    } else {
      // this.basicDetailsForm?.get('parkingChargePerDay')?.clearValidators();
      // this.basicDetailsForm?.get('parkingChargePerDay')?.updateValueAndValidity();
    }
  }

  clearSeller() {
    this.basicDetailsForm.get('isd_code')?.setValue('');
    this.basicDetailsForm.get('selleremail')?.setValue('');
    this.basicDetailsForm?.get('mobile')?.setValue('');
    this.cognitoId = '';
    this.basicDetailsForm.get('name')?.setValue('');
    this.basicDetailsForm.get('RMName')?.setValue('');
    this.basicDetailsForm.get('alternate_email')?.setValue('');
  }
  closeDatePicker(elem: MatDatepicker<any>, e) {
    let _d  = e;
          this.mfg_year = _d;
    elem.close();
  }


  checkBiddingPaymentDate(isBid,bidDate){
    if(isBid && bidDate == null) {
      this.basicDetailsForm?.get('last_date_bidding_payment')?.setValidators(Validators.required);
      this.basicDetailsForm?.get('last_date_bidding_payment')?.updateValueAndValidity();
    } else {
      this.basicDetailsForm?.get('last_date_bidding_payment')?.clearValidators();
      this.basicDetailsForm?.get('last_date_bidding_payment')?.updateValueAndValidity();
    }
  }
  checkInstaPaymentDate(isBid,instaDate){
    if(isBid && instaDate == null) {
      this.basicDetailsForm?.get('last_date_insta_payment')?.setValidators(Validators.required);
      this.basicDetailsForm?.get('last_date_insta_payment')?.updateValueAndValidity();
    } else {
      this.basicDetailsForm?.get('last_date_insta_payment')?.clearValidators();
      this.basicDetailsForm?.get('last_date_insta_payment')?.updateValueAndValidity();
    }
  }

  enableCost(value) {
    if(value.checked) {
      this.percentPremium = true;
      this.basicDetailsForm.get('premium_value1')?.setValue('');
    } else {
      this.percentPremium = false;
      this.basicDetailsForm.get('premium_value2')?.setValue('');
    }
  }

  onKeydownEvent(event: KeyboardEvent) {
    let value = this.basicDetailsForm?.get('bucketCapacity')?.value;
    if(value?.length>0) {
      let data = value.split(".")
      var preDecimal = data[0];
      var postDecimal = data[1];
  
      if (preDecimal?.length > 3 || postDecimal?.length > 2) {
        this.notify.error("please enter 3 digits before decimal and 2 digits after decimal.");
        this.basicDetailsForm?.get('bucketCapacity')?.setErrors({ required: true });
      } else {
        this.basicDetailsForm?.get('bucketCapacity')?.setErrors(null);
      }
    }
  }
}




