import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedProductUploadComponent } from './used-product-upload.component';

describe('UsedProductUploadComponent', () => {
  let component: UsedProductUploadComponent;
  let fixture: ComponentFixture<UsedProductUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsedProductUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedProductUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
