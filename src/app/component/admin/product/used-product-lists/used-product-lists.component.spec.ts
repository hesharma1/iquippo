import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedProductListsComponent } from './used-product-lists.component';

describe('UsedProductListsComponent', () => {
  let component: UsedProductListsComponent;
  let fixture: ComponentFixture<UsedProductListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsedProductListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedProductListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
