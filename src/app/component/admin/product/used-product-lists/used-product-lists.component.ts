import { Component, OnInit, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationPopupComponent } from './../../confirmation-popup/confirmation-popup.component';
import { ProductlistService } from 'src/app/services/product-list.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ConfirmDialogComponent } from '../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-used-product-lists',
  templateUrl: './used-product-lists.component.html',
  styleUrls: ['./used-product-lists.component.css']
})
export class UsedProductListsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public showForm: boolean = false;
  public searchText!: string;
  public activePage: any = 1;
  public total_count: any;
  public dataSource!: MatTableDataSource<any>;
  public ordering: string = '-id';
  public cognitoId?: string;
  public pageSize: number = 10;
  public filter: any;
  public listingData = [];
  public displayedColumns: string[] = [
    "id", "category__display_name", "brand__display_name", "model__name", "service_type", "selling_price", "seller__first_name", "mfg_year", "location","created_at","customer_reference_number","status", "Actions"
  ];
  public statusOptions: Array<any> = [
    { name: 'Draft', value: 0 },
    { name: 'Pending', value: 1 },
    { name: 'Approved', value: 2 },
    { name: 'Sale in progress', value: 3 },
    { name: 'Rejected', value: 4 },
    { name: 'Sold', value: 5 }
  ];
  noRecordFound: string = "";
  userRole: string = "";
  constructor(private dialog: MatDialog,
    private ProductlistService: ProductlistService,
    private usedUploadService: UsedEquipmentService,
    private route: Router,
    public spinner: NgxSpinnerService,
    public storage: StorageDataService, public notify: NotificationService) {
    this.showForm = false;
  }

  /**ng on init */
  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole',false);
    this.getPage();
  }

  /**get data from parent component */
  GetChildData(data: boolean) {
    this.showForm = data;
  }

  /**open dailog  */
  openDialog() {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      width: '40%',
      panelClass: 'custom-modalbox'
    });

    dialogRef.afterClosed().subscribe(
      res => {
        console.log('The dialog was closed');
      },
      err => {
        console.log(err);
      }
    );
  }

  /**get row status chnage */
  getRowstatus(status) {
    if (status == '0') {
      return "Draft"
    } else if (status == '1') {
      return "Pending"
    } else if (status == '2') {
      return "Approved"
    }else if (status == '3') {
      return "Sale in progress"
    } else if (status == '4') {
      return "Rejected"
    }else if (status == '5') {
      return "Sold"
    }
    return "";
  }

  /**search list  */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.activePage = 1;
      this.getPage();
    }
  }
  scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)){
      const triggerAt: number = 900; 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
        this.getPage();
        }
      }
    }
  }
}
  statusFilter(val) {
    this.filter = val;
    this.activePage = 1;
    this.getPage();
  }

  /**get list from api */
  getPage() {
    let payload = {};
    payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if(this.userRole == 'ChannelPartner'){
        payload['role'] = this.userRole;
        payload['cognito_id']= this.cognitoId;
    }
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
    this.ProductlistService.getUsedProWithList(payload).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if(window.innerWidth > 768){
            this.dataSource.data = res.results;
          }else{
            this.scrolled = false;
            if(this.activePage == 1){
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            }else{
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
          if (this.dataSource.data && this.dataSource.data.length <= 0) {
            this.noRecordFound = 'No records found';
          }
          else {
            this.noRecordFound = '';
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      }
    )
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getPage();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getPage();
  }

  /**chnage status */
  approve(item) {
    item.status = 2;
    item.category = item.category.id;
    item.brand = item.brand.id;
    item.model = item.model.id;
    item.location = item.location.id;
    item.seller = item.seller.cognito_id;
    this.usedUploadService.postupdateUsedEquipment(item, item.id).pipe(finalize(() => {   })).subscribe(
      res => {
        console.log(res);
        this.getPage()
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      })
  }

  /**chnage status */
  reject(item) {
    item.status = 4;
    item.category = item.category.id;
    item.brand = item.brand.id;
    item.model = item.model.id;
    item.location = item.location.id;
    item.seller = item.seller.cognito_id;
    this.usedUploadService.postupdateUsedEquipment(item, item.id).pipe(finalize(() => { })).subscribe(
      res => {
        console.log(res);
        this.getPage()
      },
      err => {
        console.log(err);
        this.notify.error(err?.message);
      })
  }

  /**edit record navigate to page */
  editRecord(editableRow) {
    localStorage.setItem('usedProductRecordById', JSON.stringify(editableRow));
    this.route.navigate(['/admin-dashboard/products/used-products-upload'], { queryParams: { assetId: editableRow?.id, toEdit: true } });
  }

 

  export() {
    let payload = {
      limit: 999
    }
    this.ProductlistService.exportNewProduct().pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        const excelData = res.map((r: any) => {
          return {
            "Asset ID": r?.id,
            "Category": r?.category,
            "Brand": r?.brand,
            "Model": r?.model,
            "Price": r?.selling_price,
            "Seller": r?.seller,
            "MFG Year": r?.mfg_year,
            "Status": r?.status
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Used Asset List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  deleteAsset(id) {
    this.dialog
      .open(ConfirmDialogComponent)
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.ProductlistService.deleteAsset(id).subscribe(res => {
            this.notify.success('Asset Deleted Successfully!!');
            this.getPage();
          });
        }
      });
  }

}
