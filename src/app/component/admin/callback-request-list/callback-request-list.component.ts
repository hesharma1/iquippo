import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/services/common.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

@Component({
  selector: 'app-callback-request-list',
  templateUrl: './callback-request-list.component.html',
  styleUrls: ['./callback-request-list.component.css']
})
export class CallbackRequestListComponent implements OnInit {
  public callBackRequestList: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  displayedColumns = ["id","request_from", "first_name", "contact_number", "email_address", "address", 'created_at', "pin_code__pin_code", "pin_code__city__name", "pin_code__city__state__name", "comment"];
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;
  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.getCallBackRequestList();
  }

  getCallBackRequestList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.commonService.getCallBackRequestList(payload).subscribe((res: any) => {
      this.total_count = res.count;
      if (window.innerWidth > 768) {
        this.callBackRequestList.data = res.results;
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.callBackRequestList.data = [];
          this.listingData = [];
          this.listingData = res.results;
          this.callBackRequestList.data = this.listingData;
        } else {
          this.listingData = this.listingData.concat(res.results);
          this.callBackRequestList.data = this.listingData;
        }
      }
    })
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getCallBackRequestList();
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getCallBackRequestList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getCallBackRequestList();
    }
  }

  exportData() {
    this.commonService.getCallBack().pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Name": r?.first_name + r?.last_name,
            "Contact number": r?.contact_number,
            "Email address": r?.email_address,
            "Address": r?.address,
            "Pincode": r?.user?.pin_code?.pin_code,
            "City": r?.user?.pin_code?.city?.name,
            "State": r?.user?.pin_code?.city?.state?.name,
            "Description": r?.comment,
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "callback-request");
      },
      err => {
        console.log(err);
      }
    );
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.callBackRequestList.data.length) && (this.callBackRequestList.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getCallBackRequestList();
          }
        }
      }
    }
  }
}
