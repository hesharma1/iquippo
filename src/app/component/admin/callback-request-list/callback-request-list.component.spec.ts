import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallbackRequestListComponent } from './callback-request-list.component';

describe('CallbackRequestListComponent', () => {
  let component: CallbackRequestListComponent;
  let fixture: ComponentFixture<CallbackRequestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallbackRequestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallbackRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
