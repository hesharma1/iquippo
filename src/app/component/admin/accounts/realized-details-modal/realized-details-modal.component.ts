import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-realized-details-modal',
  templateUrl: './realized-details-modal.component.html',
  styleUrls: ['./realized-details-modal.component.css']
})
export class RealizedDetailsModalComponent implements OnInit {

  public realizationDetails:any;

  constructor(public dialogRef: MatDialogRef<RealizedDetailsModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.realizationDetails = this.data.realizationDetails;
  }

  backClick() {
    this.dialogRef.close();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

}
