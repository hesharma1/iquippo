import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-realized-response-modal',
  templateUrl: './realized-response-modal.component.html',
  styleUrls: ['./realized-response-modal.component.css']
})
export class RealizedResponseModalComponent implements OnInit {

  public realizationForm: FormGroup;
  public mainData: any;
  public transationType;
  public isMandatory:boolean = true;
  public maxDate = new Date();

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<RealizedResponseModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.mainData = data?.mainData;
    this.transationType = data?.type;
    this.realizationForm = fb.group({
      response: [true, Validators.compose([Validators.required])],
      remarks: [''],
      actual_receipt_date: ['', Validators.required],
      actual_transaction_reference_number: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }

  submit() {
    this.dialogRef.close(this.realizationForm.value);
  }

  updateValidation(val) {
    if (val) {
      this.isMandatory = true;
      this.realizationForm.controls.actual_receipt_date.setValidators([Validators.required]);
      this.realizationForm.controls.actual_receipt_date.updateValueAndValidity();
      this.realizationForm.controls.actual_transaction_reference_number.setValidators([Validators.required]);
      this.realizationForm.controls.actual_transaction_reference_number.updateValueAndValidity();
    } else {
      this.isMandatory = false;
      this.realizationForm.controls.actual_receipt_date.setValidators([]);
      this.realizationForm.controls.actual_receipt_date.updateValueAndValidity();
      this.realizationForm.controls.actual_transaction_reference_number.setValidators([]);
      this.realizationForm.controls.actual_transaction_reference_number.updateValueAndValidity();
    }
  }
}
