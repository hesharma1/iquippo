import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRefundInfoComponent } from './add-refund-info.component';

describe('AddRefundInfoComponent', () => {
  let component: AddRefundInfoComponent;
  let fixture: ComponentFixture<AddRefundInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRefundInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRefundInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
