import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountService } from 'src/app/services/account';
import { AccountsService } from 'src/app/services/accounts.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-add-refund-info',
  templateUrl: './add-refund-info.component.html',
  styleUrls: ['./add-refund-info.component.css']
})
export class AddRefundInfoComponent implements OnInit {
  Amount: any;
  refundId: any;
  AccountNo: any;
  bankName: any;
  Comments: any;
  id: any;
  form: FormGroup;
  refundData: any;

  
  constructor(public accountService: AccountsService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddRefundInfoComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public notify: NotificationService) {
    if (data) {
      this.id = data.id;
      this.refundData = data.refundDetails;
      this.Amount = data.refundDetails ? data.refundDetails.amount : '';
      this.refundId = data.refundDetails ? data.refundDetails.refund_id : '';
      this.AccountNo = data.refundDetails ? data.refundDetails.account_number : '';
      this.bankName = data.refundDetails ? data.refundDetails.bank_name : '';
      this.Comments = data.refundDetails ? data.refundDetails.comments : '';
      console.log("data", data);
    }

    this.form = this.fb.group({
      Amount: ["", Validators.required],
      refundId: ["", Validators.required],
      AccountNo: ["", Validators.required],
      bankName: ["", Validators.required],
      Comments: [""]
    });
  }

  ngOnInit(): void {

  }

  submitRefundForm() {
    const params = {
      amount: this.Amount,
      refund_id: this.refundId,
      bank_name: this.bankName,
      account_number: this.AccountNo,
      comments: this.Comments,
      status: 2
    }
    this.accountService.addRefundInfo(params, this.id).subscribe(
      res => {
        this.dialogRef.close();
        this.notify.success('Details saved successfully');
      });
  }

  close() {
    this.dialogRef.close();
  }

}
