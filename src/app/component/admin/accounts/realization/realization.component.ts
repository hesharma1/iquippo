import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from "src/app/services/storage-data.service";
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { AuctionService } from 'src/app/services/auction.service';
import { AccountsService } from 'src/app/services/accounts.service';
import { RealizedResponseModalComponent } from '../realized-response-modal/realized-response-modal.component';
import { RealizedDetailsModalComponent } from '../realized-details-modal/realized-details-modal.component';
import { PaymentService } from 'src/app/services/payment.service';

@Component({
  selector: 'app-realization',
  templateUrl: './realization.component.html',
  styleUrls: ['./realization.component.css']
})
export class RealizationComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-payment_date';
  public total_count: any;
  public searchText!: string;
  public auctionList: Array<any> = [];
  public transactionType: any = 1;
  public transactionValue: any = 1;
  public auctionValue: any;
  public realizationValue: any = '';
  public displayedColumns = ['transaction_id', 'amount', 'buyer__first_name', 'buyer__mobile_number', 'buyer__email', 'payment_mode', 'payment_date', 'payment_status', 'actions'];
  public displayedColumnsValuation = ['transaction_id', "invoice_number", 'amount', 'buyer__first_name', 'buyer__mobile_number', 'buyer__email', 'payment_mode', 'payment_status', 'actions'];
  public listingData = [];
  public realizationTypes = [
    {typeName:'Realized', value: '2'},
    {typeName:'To be Realized', value: '5'},
    {typeName:'Not Realized', value: '3,4'},
  ]

  constructor(public router: Router, public dialog: MatDialog, public spinner: NgxSpinnerService, public storage: StorageDataService, public notify: NotificationService, private auctionService: AuctionService, public accountService: AccountsService, public paymentService: PaymentService) { }

  ngOnInit(): void {
    // this.getAuctionList();
    this.getRealizationList();
  }

  getAuctionList() {
    let payload = {
      parent_auction__id__isnull: true,
      ordering: '-updated_at'
    }
    if (this.auctionValue) {
      payload['name__icontains'] = this.auctionValue;
    }
    this.auctionService.getAuctionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.auctionList = res.results;
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  getRealizationList(val?) {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      payment_status__in: this.realizationValue ? this.realizationValue : '2,3,4,5',
      type__in: this.transactionValue,
      listing_type: 'offline'
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (val) {
      payload['paymentdetail__emdpayment__auction_emd__auction_id__in'] = val;
      payload['paymentdetail__fulfilment__lot_bid__lot__auction__id__in'] = val;
    }
    this.accountService.getRealizationList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.dataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.dataSource.data = this.listingData;
            } else {
              this.listingData = this.listingData.concat(res.results);
              this.dataSource.data = this.listingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getRealizationList();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getRealizationList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.page = 1;
      this.pageSize = 10;
      this.getRealizationList();
    }
  }

  searchAuction() {
    if (this.auctionValue?.length > 3) {
      this.getAuctionList();
    } else {
      if (this.auctionValue === '') {
        this.auctionList = [];
        this.getRealizationList();
      }
    }
  }

  transactionChange(val) {
    this.page = 1;
    this.pageSize = 10;
    if (val == '2') {
      this.transactionValue = '5,6,9'
    } else if (val == '3') {
      this.transactionValue = '2,3'
    } else {
      this.transactionValue = val;
    }

    this.getRealizationList();
  }

  auctionSelection(val) {
    this.page = 1;
    this.pageSize = 10;
    this.auctionValue = val?.name;
    this.getRealizationList(val?.id);
  }

  realizationSelection(val) {
    this.realizationValue = val;
    this.page = 1;
    this.pageSize = 10;
    this.getRealizationList();
  }

  realizeTransaction(element) {
    const dialogRef = this.dialog.open(RealizedResponseModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        mainData: element,
        type: this.transactionType,
      }
    });

    dialogRef.afterClosed().subscribe(val => {
      if (val != undefined) {
        if (this.transactionValue == '1') {
          let payload = {
            actual_receipt_date: val?.actual_receipt_date ? val?.actual_receipt_date : null,
            actual_transaction_reference_number: val?.actual_transaction_reference_number
          }
          if (!val?.response && val?.remarks) {
            payload['remarks'] = val?.remarks
          }
          this.accountService.realizeTransaction(element?.id, val?.response, payload).subscribe(
            (res: any) => {
              this.notify.success('Action performed successfully!!');
              this.getRealizationList();
            },
            err => {
              console.log(err);
              this.notify.error(err.message);
            }
          );
        } else {
          let payload = {
            payment_status: (val?.response == true) ? 2 : 3,
            actual_receipt_date: val?.actual_receipt_date ? val?.actual_receipt_date : null,
            actual_transaction_reference_number: val?.actual_transaction_reference_number
          }
          if (!val?.response && val?.remarks) {
            payload['remarks'] = val?.remarks
          }
          this.paymentService.updatePaymentStatus(element?.id, payload).subscribe(
            res => {
              this.notify.success('Action performed successfully!!');
              this.getRealizationList();
            },
            err => {
              console.log(err);
              this.notify.error(err.message);
            }
          );
        }
      }
    })
  }

  viewRalization(data) {
    const dialogRef = this.dialog.open(RealizedDetailsModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        realizationDetails: data,
      }
    });
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Not Realized'
        break;
      }
      case 4: {
        return 'Not Realized'  // written in place of cancelled because of Auction api
        break;
      }
      case 5: {
        return 'To be Realized'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  export() {
    let payload = {
      limit: -1,
      payment_status__in: '2,3,4,5',
      type__in: this.transactionValue,
      listing_type: 'offline'
    }
    this.accountService.getRealizationList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Transaction Id": r?.transaction_id,
            "Amoount": r?.amount,
            // "Lot No": r?.lot?.lot_number,
            // "Asset ID": r?.asset?.id,
            "User Name": r?.buyer?.first_name + '' + r?.buyer?.last_name,
            "Mobile": r?.buyer?.mobile_number,
            "Email": r?.buyer?.email,
            "Payment Mode": r?.payment_mode,
            "Request Status": this.getPaymentStatus(r.payment_status)
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Realization List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  getColumns() {
    if (this.transactionType == '2') {
      return this.displayedColumnsValuation;
    } else {
      return this.displayedColumns;
    }

  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if (!this.scrolled) {
            this.scrolled = true;
            this.page++;
            this.getRealizationList();
          }
        }
      }
    }
  }

}
