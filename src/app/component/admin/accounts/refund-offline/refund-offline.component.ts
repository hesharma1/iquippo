import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { AccountsService } from 'src/app/services/accounts.service';
import { AddRefundInfoComponent } from '../add-refund-info/add-refund-info.component';
import { RealizedDetailsModalComponent } from '../realized-details-modal/realized-details-modal.component';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-refund-offline',
  templateUrl: './refund-offline.component.html',
  styleUrls: ['./refund-offline.component.css']
})

export class RefundOfflineComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-created_at';
  public total_count: any;
  public auctionDisplayedColumns: string[] = ['transaction__transaction_id', "Refund_Id", "AuctionId_ID", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "created_at", "action"
  ];

  public walletDisplayedColumns: string[] = ['transaction__transaction_id', "Refund_Id", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "created_at", "action"
  ];

  public valuationDisplayedColumns: string[] = ['transaction__transaction_id', "Refund_Id", "UCN", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "created_at", "action"
  ];

  public tradeDetailsDisplayedColumns: string[] = ['transaction__transaction_id', "Refund_Id", "Asset_Id", "Transaction_Type", "Transaction_Status", "Refund_Status", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "created_at", "action"
  ];

  public refundedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public initiatedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchRefundedText!: string;
  public searchInitiatedText!: string;
  public paymentStatus: any;
  public paymentForType: any;
  public transactionTypeValue: any = '1';
  public transactionType: Array<any> = [
    { name: 'Auction', value: '1' },
    { name: 'Valuation', value: '5,6,9' },
    { name: 'Trade Details', value: '2,3,4' },

  ];
  initiatedTotalCount: any;
  refundedTotalCount: any;
  form: FormGroup;
  public now: Date = new Date();
  public listingData = [];
  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute,
    private router: Router, private ProductlistService: ProductlistService,
    private storage: StorageDataService, public notify: NotificationService,
    private datePipe: DatePipe, private dialog: MatDialog, public accountService: AccountsService, private fb: FormBuilder) {
    this.form = this.fb.group({
      startDate: ["", Validators.required],
      endDate: ["", Validators.required]
    });
  }


  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getOnlineInitiatedList();
  }

  getOnlineInitiatedList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      usage_type__in: 'RF',
      listing_type: 'offline',
      is_wallet: 'False',
      status__in: '2,3,4,5,6',
      type__in: this.transactionTypeValue
    };
    if (this.searchInitiatedText) {
      payload['search'] = this.searchInitiatedText;
    }
    this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.initiatedTotalCount = res.count;
          if (window.innerWidth > 768) {
            this.initiatedDataSource.data = res.results;
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.initiatedDataSource.data = [];
              this.listingData = [];
              this.listingData = res.results;
              this.initiatedDataSource.data = this.listingData;
            } else {
              this.listingData = this.listingData.concat(res.results);
              this.initiatedDataSource.data = this.listingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  transactionChange(val) {
    this.searchInitiatedText = '';
    this.getOnlineInitiatedList();
  }

  getColumns() {
    if (this.transactionTypeValue == '1') {
      return this.auctionDisplayedColumns;
    } else if (this.transactionTypeValue == '5,6,9') {
      return this.valuationDisplayedColumns;
    } else if (this.transactionTypeValue == '2,3,4') {
      return this.tradeDetailsDisplayedColumns;
    } else {
      return this.walletDisplayedColumns;
    }
  }

  onInitiatedPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getOnlineInitiatedList();
  }
  onInitiatedSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getOnlineInitiatedList();
  }

  searchInitiatedList() {
    if (this.searchInitiatedText === '' || this.searchInitiatedText?.length > 3) {
      this.page = 1;
      this.getOnlineInitiatedList();
    }
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getTransactionType(val) {
    switch (val) {
      case 1: {
        return 'Auction'
        break;
      }
      case 2: {
        return 'Insta Sell'
        break;
      }
      case 3: {
        return 'Bid'
        break;
      }
      case 4: {
        return 'Lead'
        break;
      }
      case 5: {
        return 'Advanced Valuation'
        break;
      }
      case 6: {
        return 'Insta Valuation'
        break;
      }
      case 7: {
        return 'Auction EMD'
        break;
      }
      case 8: {
        return 'Auction Fulfillment'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  addRefundInfo(id, data) {
    const dialogRef = this.dialog.open(AddRefundInfoComponent, {
      width: '500px',
      disableClose: true,
      data: {
        id: id,
        refundDetails: data
      }
    });

    dialogRef.afterClosed().subscribe(val => {
      this.getOnlineInitiatedList();
    });

  }

  viewRalization(data) {
    const dialogRef = this.dialog.open(RealizedDetailsModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        realizationDetails: data,
        isFromOfflineRefund: true
      }
    });
  }

  approveRefund(data) {
    let payload = {
      id: data?.id
    }
    this.accountService.approveOfflineRefund(payload, data?.id).subscribe(
      (res: any) => {
        if (res) {
          this.notify.success(res.message);
          this.getOnlineInitiatedList();
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  export() {
    let startDate = this.form?.get('startDate')?.value;
    startDate = new Date(startDate);
    startDate = startDate.setDate(startDate.getDate() - 1);
    let enddate = this.form?.get('endDate')?.value;
    enddate = new Date(enddate);
    enddate = enddate.setDate(enddate.getDate() + 1);
    this.form.markAllAsTouched();
    if (this.form.valid) {
      let payload = {
        page: 1,
        limit: -1,
        ordering: this.ordering,
        usage_type__in: 'RF',
        listing_type: 'offline',
        is_wallet: 'False',
        type__in: this.transactionTypeValue,
        status__in: '2,3,4,5,6',
      };
      if (this.searchInitiatedText) {
        payload['search'] = this.searchInitiatedText;
      }
      if (this.form?.get('startDate')?.value) {
        payload['created_at__date__gte'] = this.datePipe.transform(this.form?.get('startDate')?.value, 'yyyy-MM-dd');
      }
      if (this.form?.get('endDate')?.value) {
        payload['created_at__date__lte'] = this.datePipe.transform(enddate, 'yyyy-MM-dd');
      }
      this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res) {
            const excelData = res.results.map((element: any) => {
              let auctionId = element?.transaction?.emd_details?.filter(x => x.auction?.auction_engine_auction_id).map(x => x.auction?.auction_engine_auction_id).join(", ");
              //valuation columns//
              if (this.transactionTypeValue == '5,6,9') {
                return {
                  'Transaction_id': element?.transaction?.transaction_id,
                  "Refund_Id": element?.refund_id,
                  "UCN": element?.unique_control_number,
                  "Transaction_Type": this.getTransactionType(element?.type),
                  "User_Name": element?.transaction?.buyer?.first_name,
                  "Mobile_No": element?.transaction?.buyer?.mobile_number,
                  "email": element?.transaction?.buyer?.email,
                  "Amount": element?.transaction?.amount,
                  "Refund_Amount": element?.amount,
                  "Refund_Type": element?.refund_type,
                  "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                }
              }
              //Trade columns//
              else if (this.transactionTypeValue == '2,3,4') {
                return {
                  'Transaction_id': element?.transaction?.transaction_id,
                  "Refund_Id": element?.refund_id,
                  "Asset_Id": element?.transaction?.asset_id?.id,
                  "Transaction_Type": this.getTransactionType(element?.type),
                  "Transaction_Status": this.getPaymentStatus(element?.transaction?.payment_status),
                  "Refund_Status": this.getPaymentStatus(element?.status),
                  "User_Name": element?.transaction?.buyer?.first_name,
                  "Mobile_No": element?.transaction?.buyer?.mobile_number,
                  "email": element?.transaction?.buyer?.email,
                  "Amount": element?.transaction?.amount,
                  "Refund_Amount": element?.amount,
                  "Refund_Type": element?.refund_type,
                  "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                }
              }
              //auction columns//
              else {
                return {
                  'Transaction_id': element?.transaction?.transaction_id,
                  "Refund_Id": element?.refund_id,
                  "AuctionId_ID": auctionId,
                  "Transaction_Type": this.getTransactionType(element?.type),
                  "User_Name": element?.transaction?.buyer?.first_name,
                  "Mobile_No": element?.transaction?.buyer?.mobile_number,
                  "email": element?.transaction?.buyer?.email,
                  "Amount": element?.transaction?.amount,
                  "Refund_Amount": element?.amount,
                  "Refund_Type": element?.refund_type,
                  "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                }
              }
            });
            ExportExcelUtil.exportArrayToExcel(excelData, "Offline Refund");
          }
        },
        err => {
          this.notify.error(err.message);
        }
      );
    }
  }

  getMinDate() {
    const date = new Date();
    const month = date.getMonth();
    date.setMonth(date.getMonth() - 1);
    while (date.getMonth() === month) {
      date.setDate(date.getDate() - 1);
    }
    return date;
  }

  getEndDate() {
    let date = this.form?.get('startDate')?.value;
    if (date) {
      date = new Date(date);
      return date;
    }
    // else {
    //   const date = new Date();
    //   const month = date.getMonth();
    //   date.setMonth(date.getMonth() - 1);
    //   while (date.getMonth() === month) {
    //     date.setDate(date.getDate() - 1);
    //   }
    //   return date;
    // }
  }
  scrolled =  false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.initiatedTotalCount > this.initiatedDataSource.data.length) && (this.initiatedDataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getOnlineInitiatedList();
        }
      }
      }
    }
  }

}
