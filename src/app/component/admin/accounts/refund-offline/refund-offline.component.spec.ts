import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundOfflineComponent } from './refund-offline.component';

describe('RefundOfflineComponent', () => {
  let component: RefundOfflineComponent;
  let fixture: ComponentFixture<RefundOfflineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefundOfflineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundOfflineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
