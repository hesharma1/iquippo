import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { MatDialog } from '@angular/material/dialog';
import { AccountsService } from 'src/app/services/accounts.service';
import { RealizedDetailsModalComponent } from '../realized-details-modal/realized-details-modal.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-refund-online',
  templateUrl: './refund-online.component.html',
  styleUrls: ['./refund-online.component.css']
})

export class RefundOnlineComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public selectedTabIndex: number = 0;
  public auctionDisplayedColumns: string[] = ['Transaction_id', "Refund_Id", "AuctionId_ID", "Order_Id", "Payment_Id", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "Request_Date", "action"
  ];

  public walletDisplayedColumns: string[] = ['Transaction_id', "Refund_Id", "Order_Id", "Payment_Id", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "Request_Date", "action"
  ];

  public valuationDisplayedColumns: string[] = ['Transaction_id', "Refund_Id", "UCN", "Order_Id", "Payment_Id", "Transaction_Type", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "Request_Date", "action"
  ];

  public tradeDetailsDisplayedColumns: string[] = ['Transaction_id', "Refund_Id", "Asset_Id", "Order_Id", "Payment_Id", "Transaction_Type", "Transaction_Status", "Refund_Status", "User_Name", "Mobile_No", "email", "Amount", "Refund_Amount", "Refund_Type", "Request_Date", "action"
  ];

  public refundedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public initiatedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchRefundedText!: string;
  public searchInitiatedText!: string;
  public paymentStatus: any;
  public paymentForType: any;
  public transactionTypeValue: any = '1';
  public transactionType: Array<any> = [
    { name: 'Auction', value: '1' },
    { name: 'Valuation', value: '5,6,9' },
    // { name: 'Wallet', value: '0' },
    { name: 'Trade Details', value: '2,3,4' }
  ];
  initiatedTotalCount: any;
  refundedTotalCount: any;
  refundedForm: FormGroup;
  initaitedForm: FormGroup;
  public now: Date = new Date();
  public initiatedListingData = [];
  public refundedListingData = [];
  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute, private router: Router, private ProductlistService: ProductlistService, private storage: StorageDataService, public notify: NotificationService, private datePipe: DatePipe, private dialog: MatDialog, public accountService: AccountsService, private fb: FormBuilder) {
    this.initaitedForm = this.fb.group({
      initaitedStartDate: ["", Validators.required],
      initaitedEndDate: ["", Validators.required]
    });
    this.refundedForm = this.fb.group({
      refundedStartDate: ["", Validators.required],
      refundedEndDate: ["", Validators.required]
    });

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getData();
  }

  getOnlineInitiatedList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      usage_type__in: 'RF',
      listing_type: 'online',
      is_wallet: 'False',
      status__in: 1,  //for ini 1. and for refunded 2
      type__in: this.transactionTypeValue  //transaction type
    };
    if (this.searchInitiatedText) {
      payload['search'] = this.searchInitiatedText;
    }
    this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.initiatedTotalCount = res.count;
          this.initiatedDataSource.data = res.results;
          if (window.innerWidth > 768) {
            this.initiatedDataSource.data = res.results;
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.initiatedDataSource.data = [];
              this.initiatedListingData = [];
              this.initiatedListingData = res.results;
              this.initiatedDataSource.data = this.initiatedListingData;
            } else {
              this.initiatedListingData = this.initiatedListingData.concat(res.results);
              this.initiatedDataSource.data = this.initiatedListingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getOnlineRefundedList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      usage_type__in: 'RF',
      listing_type: 'online',
      is_wallet: 'False',
      status__in: 2, //for ini 1. and for refunded 2
      type__in: this.transactionTypeValue  //transaction type
    };

    if (this.searchRefundedText) {
      payload['search'] = this.searchRefundedText;
    }
    this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.refundedTotalCount = res.count;
          this.refundedDataSource.data = res.results;
          if (window.innerWidth > 768) {
            this.refundedDataSource.data = res.results;
          } else {
            this.scrolledrefunded = false;
            if (this.page == 1) {
              this.refundedDataSource.data = [];
              this.refundedListingData = [];
              this.refundedListingData = res.results;
              this.refundedDataSource.data = this.refundedListingData;
            } else {
              this.refundedListingData = this.refundedListingData.concat(res.results);
              this.refundedDataSource.data = this.refundedListingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  transactionChange(val) {
    this.searchInitiatedText = '';
    this.searchRefundedText = '';
    this.page = 1;
    this.pageSize = 10;
    this.getData();
  }

  getColumns() {
    if (this.transactionTypeValue == '1') {
      return this.auctionDisplayedColumns;
    }
    else if (this.transactionTypeValue == '5,6,9') {
      return this.valuationDisplayedColumns;
    }
    else if (this.transactionTypeValue == '2,3,4') {
      return this.tradeDetailsDisplayedColumns;
    }
    else {
      return this.walletDisplayedColumns;
    }
  }

  onTabChanged(event) {
    this.selectedTabIndex = event.index;
    this.page = 1;
    this.pageSize = 10;
    this.getData();
  }

  getData() {
    switch (this.selectedTabIndex) {
      case 0: {
        this.getOnlineInitiatedList();
        break;
      }
      case 1: {
        this.getOnlineRefundedList();
        break;
      }
      default: {
        this.getOnlineInitiatedList();
        break;
      }
    }
  }

  onInitiatedPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getOnlineInitiatedList();
  }

  onRefundedPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getOnlineRefundedList();
  }

  onRefundedSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getOnlineRefundedList();
  }

  onInitiatedSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getOnlineInitiatedList();
  }

  searchRefundedList() {
    if (this.searchRefundedText === '' || this.searchRefundedText?.length > 3) {
      this.page = 1;
      this.getOnlineRefundedList();
    }
  }

  searchInitiatedList() {
    if (this.searchInitiatedText === '' || this.searchInitiatedText?.length > 3) {
      this.page = 1;
      this.getOnlineInitiatedList();
    }
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getTransactionType(val) {
    switch (val) {
      case 1: {
        return 'Auction'
        break;
      }
      case 2: {
        return 'Insta Sell'
        break;
      }
      case 3: {
        return 'Bid'
        break;
      }
      case 4: {
        return 'Lead'
        break;
      }
      case 5: {
        return 'Advanced Valuation'
        break;
      }
      case 6: {
        return 'Insta Valuation'
        break;
      }
      case 7: {
        return 'Auction EMD'
        break;
      }
      case 8: {
        return 'Auction Fulfillment'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  viewRalization(data) {
    const dialogRef = this.dialog.open(RealizedDetailsModalComponent, {
      width: '500px',
      disableClose: true,
      data: {
        realizationDetails: data,
        isFromOnlineRefund: true
      }
    });
  }

  approveRefund(data) {
    let payload = {
      id: data?.id
    }
    this.accountService.approveOnlineRefund(payload).subscribe(
      (res: any) => {
        if (res) {
          this.notify.success(res.message);
          this.getData();
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  export(type) {

    if (type == 'InitiatdRefund') {
      this.initaitedForm.markAllAsTouched();
      // let date = this.initaitedForm?.get('initaitedEndDate')?.value;
      // date = new Date(date);
      // date = date.setDate(date.getDate() + 1);
      let startDate = this.initaitedForm?.get('initaitedStartDate')?.value;
      startDate = new Date(startDate);
      startDate = startDate.setDate(startDate.getDate() - 1);
      let enddate = this.initaitedForm?.get('initaitedEndDate')?.value;
      enddate = new Date(enddate);
      enddate = enddate.setDate(enddate.getDate() + 1);
      if (this.initaitedForm.valid) {
        let payload = {
          page: 1,
          limit: -1,
          ordering: this.ordering,
          usage_type__in: 'RF',
          listing_type: 'online',
          is_wallet: 'False',
          status__in: 1,  //for ini 1. and for refunded 2
          type__in: this.transactionTypeValue  //transaction type
        };
        if (this.searchInitiatedText) {
          payload['search'] = this.searchInitiatedText;
        }
        if (this.initaitedForm?.get('initaitedStartDate')?.value) {
          payload['created_at__date__gte'] = this.datePipe.transform(this.initaitedForm?.get('initaitedStartDate')?.value, 'yyyy-MM-dd');
        }
        if (this.initaitedForm?.get('initaitedEndDate')?.value) {
          payload['created_at__date__lte'] = this.datePipe.transform(enddate, 'yyyy-MM-dd');
        }
        this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            if (res) {
              const excelData = res.results.map((element: any) => {
                let auctionId = element?.transaction?.emd_details?.filter(x => x.auction?.auction_engine_auction_id).map(x => x.auction?.auction_engine_auction_id).join(", ");
                //valuation columns//
                if (this.transactionTypeValue == '5,6,9') {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "UCN": element?.unique_control_number,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),

                  }
                }
                //Trade columns//
                else if (this.transactionTypeValue == '2,3,4') {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "Asset_Id": element?.transaction?.asset_id?.id,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "Transaction_Status": this.getPaymentStatus(element?.transaction?.payment_status),
                    "Refund_Status": this.getPaymentStatus(element?.status),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                  }
                }
                //auction columns//
                else {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "AuctionId_ID": auctionId,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                  }
                }
              });
              ExportExcelUtil.exportArrayToExcel(excelData, "Online Refund");
            }
          },
          err => {
            this.notify.error(err.message);
          }
        );
      }
    }
    else {
      this.refundedForm.markAllAsTouched();
      if (this.refundedForm.valid) {
        // let date = this.refundedForm?.get('refundedEndDate')?.value;
        // date = new Date(date);
        // date = date.setDate(date.getDate() + 1);
        let startDate = this.refundedForm?.get('refundedStartDate')?.value;
        startDate = new Date(startDate);
        startDate = startDate.setDate(startDate.getDate() - 1);
        let enddate = this.refundedForm?.get('refundedEndDate')?.value;
        enddate = new Date(enddate);
        enddate = enddate.setDate(enddate.getDate() + 1);
        let payload = {
          page: 1,
          limit: -1,
          ordering: this.ordering,
          usage_type__in: 'RF',
          listing_type: 'online',
          is_wallet: 'False',
          status__in: 2, //for ini 1. and for refunded 2
          type__in: this.transactionTypeValue  //transaction type
        };
        if (this.searchRefundedText) {
          payload['search'] = this.searchRefundedText;
        }
        if (this.refundedForm?.get('refundedStartDate')?.value) {
          payload['created_at__date__gte'] = this.datePipe.transform(this.refundedForm?.get('refundedStartDate')?.value, 'yyyy-MM-dd');
        }
        if (this.refundedForm?.get('refundedEndDate')?.value) {
          payload['created_at__date__lte'] = this.datePipe.transform(enddate, 'yyyy-MM-dd');
        }
        this.accountService.OnlineOfflineRefundList(payload).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            if (res) {
              const excelData = res.results.map((element: any) => {
                let auctionId = element?.transaction?.emd_details?.filter(x => x.auction?.auction_engine_auction_id).map(x => x.auction?.auction_engine_auction_id).join(", ");
                //valuation columns//
                if (this.transactionTypeValue == '5,6,9') {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "UCN": element?.unique_control_number,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                  }
                }
                //Trade columns//
                else if (this.transactionTypeValue == '2,3,4') {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "Asset_Id": element?.transaction?.asset_id?.id,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "Transaction_Status": this.getPaymentStatus(element?.transaction?.payment_status),
                    "Refund_Status": this.getPaymentStatus(element?.status),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),
                  }
                }
                //auction columns//
                else {
                  return {
                    'Transaction_id': element?.transaction?.transaction_id,
                    "Refund_Id": element?.refund_id,
                    "AuctionId_ID": auctionId,
                    "Order_Id": element?.payment_gateway_order_id,
                    "Payment_Id": element?.payment_gateway_payment_id,
                    "Transaction_Type": this.getTransactionType(element?.type),
                    "User_Name": element?.transaction?.buyer?.first_name,
                    "Mobile_No": element?.transaction?.buyer?.mobile_number,
                    "email": element?.transaction?.buyer?.email,
                    "Amount": element?.transaction?.amount,
                    "Refund_Amount": element?.amount,
                    "Refund_Type": element?.refund_type,
                    "created_at": this.datePipe.transform(element?.created_at, 'yyyy-MM-dd'),

                  }
                }
              });
              ExportExcelUtil.exportArrayToExcel(excelData, "Online Refund");
            }
          },
          err => {
            this.notify.error(err.message);
          }
        );
      }
    }
  }

  getMinDate() {
    const date = new Date();
    const month = date.getMonth();
    date.setMonth(date.getMonth() - 1);
    while (date.getMonth() === month) {
      date.setDate(date.getDate() - 1);
    }
    return date;
  }

  initaitedGetEndDate() {
    let date = this.initaitedForm?.get('initaitedStartDate')?.value;
    if (date) {
      date = new Date(date);
      return date;
    }
    // else {
    //   const date = new Date();
    //   const month = date.getMonth();
    //   date.setMonth(date.getMonth() - 1);
    //   while (date.getMonth() === month) {
    //     date.setDate(date.getDate() - 1);
    //   }
    //   return date;
    // }
  }

  refundedGetEndDate() {
    let date = this.refundedForm?.get('refundedStartDate')?.value;
    if (date) {
      date = new Date(date);
      return date;
    }
    // else {
    //   const date = new Date();
    //    const month = date.getMonth();
    //   date.setMonth(date.getMonth() - 1);
    //   while (date.getMonth() === month) {
    //     date.setDate(date.getDate() - 1);
    //   }
    //   return date;
    // }
  }
  scrolled = false;
  scrolledrefunded = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (this.selectedTabIndex == 0) {
          if ((this.initiatedTotalCount > this.initiatedDataSource.data.length) && (this.initiatedDataSource.data.length > 0)) {
            if(!this.scrolled){
              this.scrolled = true;
            this.page++;
            this.getOnlineInitiatedList();
            }
          }
        }
        else {
          if ((this.refundedTotalCount > this.refundedDataSource.data.length) && (this.refundedDataSource.data.length > 0)) {
            if(!this.scrolledrefunded){
              this.scrolledrefunded = true;
            this.page++;
            this.getOnlineRefundedList();
            }
          }
        }
      }
    }
  }
}
