import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundOnlineComponent } from './refund-online.component';

describe('RefundOnlineComponent', () => {
  let component: RefundOnlineComponent;
  let fixture: ComponentFixture<RefundOnlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefundOnlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
