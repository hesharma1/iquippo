import { DatePipe } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from 'src/app/services/shared-service.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';
import { ConfirmationDeletePopupComponent } from '../../../client-agreement/confirmation-delete-popup/confirmation-delete-popup.component';
import { BulkUploadService } from 'src/app/component/customer/bulk-upload/bulk-upload.service';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bulk-listing',
  templateUrl: './bulk-listing.component.html',
  styleUrls: ['./bulk-listing.component.css']
})
export class BulkListingComponent implements OnInit {
  form?: FormGroup;
  public listingData:any = [];
  multipleAgreementRef?: FormArray;
  dataSource1 = new MatTableDataSource<[]>();
  res: any;
  showForm: boolean = true;
  displayedColumns = ["id","seller","total_assets","status","created_at","is_discarded","Action"];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('agreement') agreement?: ElementRef;
  @ViewChild('work_order') work_order?: ElementRef;
  @ViewChild('input') input!: ElementRef;
  action: string = "";

  agreementStatus: any;
  agreementTemplateUrl: string = '';
  cognitoId: any;
  role;
  sellerData: any;
  sellerList: any;
  sellerId?: number;
  
  constructor(private fb: FormBuilder,public router: Router, private datePipe: DatePipe, private s3: S3UploadDownloadService, private notify: NotificationService, public agreementService: AgreementServiceService, public sharedService: SharedService, private dialog: MatDialog, public app: ApiRouteService, public spinner: NgxSpinnerService, private commonService: CommonService,public bulkService:BulkUploadService) {
    
    this.agreementStatus = app.agreementStatus;
  }

  

  // getSellerList() {
  //   var queryParams = 'limit=999';
  //   this.agreementService.getSellerList(queryParams).subscribe((res: any) => {
  //     this.sellerList = res.results;
  //   }, err => {

  //   })
  // }
  
  

  createUUID() {
    return uuidv4();
  }

  

  
  ngOnInit(): void {
    // this.getSellerList();
    this.role = localStorage.getItem("userRole");
    this.cognitoId = localStorage.getItem('cognitoId');
    // if(this.role==this.app.roles.admin || this.role==this.app.roles.superadmin){
    //    this.displayedColumns = ["id","seller","total_assets","status","created_at","is_discarded","Action"];
    // }
  //  this.getAgreementTemplateUrl();
    
  }

  ngAfterViewInit() {
    this.getbulkListDetails();
    // this.dataSource1.paginator = this.paginator;
  }

  
  multipleUpload() {
    if(!this.cognitoId){
      this.router.navigate(['login'], { queryParams: { returnUrl: this.router.routerState.snapshot.url }});
      return;
    }
    localStorage.setItem('adminBulkUpload',"true");
    this.router.navigate(['/bulk-upload/upload-files'], { queryParams: { isNew: false } })
  }

  actionPopup(agreementObj: any, action: string) {
    // if (action == "view") {
    //   const dialogRef = this.dialog.open(AgreementDetailsPopupComponent, {
    //     width: "60%",
    //     data: { agreementObj: agreementObj }
    //   }).afterClosed()
    //     .subscribe(data => {
    //     })
    // }
    if (action == "edit") {
      this.router.navigate(['/bulk-upload/bulk-curation'], { queryParams: { id: agreementObj.bulk_transaction_id } });
    }
    else {
      const dialogRef = this.dialog.open(ConfirmationDeletePopupComponent, {
        width: "500px",
        data: {}
      }).afterClosed()
        .subscribe(data => {
          if (data) {
            this.bulkService.deleteBulkUpload(agreementObj.bulk_transaction_id).subscribe(res => {
              //this.agreementService.deletePartnerEntityMapping(agreementObj).subscribe(res=>{
              this.getbulkListDetails();
              //})
            })
          }
        })

    }
    //})
  } 
  scrolled= false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.res.count > this.listingData.length) && (this.listingData.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.paginator.pageIndex++;
        this.getbulkListDetails();
        }
      }
    }
  }
}
  getbulkListDetails() {
    let order = this.sort.direction == "asc" ? this.sort.active : "-" + this.sort.active ;
    let queryparams = "limit=" + this.paginator.pageSize + "&page=" + (this.paginator.pageIndex + 1) + "&ordering=" + order; //+ "&search=" + this.input.nativeElement.value
    if(this.role == this.app.roles.dealer || this.role == this.app.roles.manufacturer || this.role == this.app.roles.customer){
      queryparams += "&seller__cognito_id="+this.cognitoId
    }
    this.bulkService.getBulkUploadList(queryparams).subscribe(res => {
      console.log(res);
      this.res = res as any;
      // Changes for Card layout on Mobile devices
if(window.innerWidth > 768){
  this.res = res as any;
      this.dataSource1 = this.res?.results;
  }else{
    this.scrolled = false;
    if(this.paginator.pageIndex == 1){
      // this.dataSource1 = [];
      this.listingData = [];
      this.listingData = this.res?.results;
      this.dataSource1 = this.listingData;
    }else{
      this.listingData = this.listingData.concat(this.res?.results);
      this.dataSource1 = this.listingData;
    }
  }
    this.dataSource1.paginator = this.paginator;

      console.log("ds", this.dataSource1);
    }, err => {
      console.log(err);
    })
  }

  

  getAgreementTemplateUrl() {
    this.agreementService.getAgreemenTemplateUrl().subscribe((res: any) => {
      console.log(res);
      this.agreementTemplateUrl = res?.value;

    }, err => {
      console.log(err);
    })
  }

  

}
