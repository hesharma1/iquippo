import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkListingComponent } from './bulk-listing.component';

describe('BulkListingComponent', () => {
  let component: BulkListingComponent;
  let fixture: ComponentFixture<BulkListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
