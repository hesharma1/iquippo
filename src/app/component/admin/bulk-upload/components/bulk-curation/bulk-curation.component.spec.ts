import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCurationComponent } from './bulk-curation.component';

describe('BulkCurationComponent', () => {
  let component: BulkCurationComponent;
  let fixture: ComponentFixture<BulkCurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkCurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
