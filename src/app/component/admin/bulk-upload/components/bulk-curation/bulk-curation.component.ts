import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { CommonService } from 'src/app/services/common.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FlexAlignStyleBuilder } from '@angular/flex-layout';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { BulkUploadService } from 'src/app/component/customer/bulk-upload/bulk-upload.service';
import { BrandSelectionComponent } from 'src/app/component/customer/bulk-upload/brand-selection/brand-selection.component';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { v4 as uuidv4 } from 'uuid';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { Gallery } from 'ng-gallery';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { PanVerifyPopupComponent } from 'src/app/component/pan-verify-popup/pan-verify-popup.component';
@Component({
  selector: 'app-bulk-curation',
  templateUrl: './bulk-curation.component.html',
  styleUrls: ['./bulk-curation.component.css'],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden", display: "none" })
      ),
      state("expanded", style({ height: "*", visibility: "visible", display: "table-row" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class BulkCurationComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('imageInput') imageInput!: ElementRef;

  panelOpenState = false;
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-start_date';
  public total_count: any;
  public searchText!: string;
  public disabledButton: boolean = false;
  public steps = {
    current: 2,
    total: [
      { title: 'Upload Files' },
      { title: 'View Details' },
      { title: 'Completed' },
    ]
  }
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  public mainData: Array<any> = [];
  enterprise: boolean = false;
  public displayedColumns: string[] = ["id", "category", "brand", "model", "rc_number", "status", "Action"];
  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);role: string | null;
;
  public dialogData: any;
  mismatchBrand: Array<any> = [];
  mismatchCategory: Array<any> = [];
  mismatchModel: Array<any> = [];
  mismatchLocation: Array<any> = [];
  public uploadVideoAndImages: Array<object> = [];
  bulkTransactionId: string = '';
  public viewingOptionsForm!: FormGroup;
  public tabsList: any;
  public assetVisualData: any;
  public firstImage: boolean = false;
  public assetId: any;
  public cognitoID!: string;
  constructor(public bulkService: BulkUploadService,
    public gallery: Gallery,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe,
    public notify: NotificationService,
    public cmnService: CommonService,
    public router: Router,
    public dialog: MatDialog,
    public appref: ApiRouteService,
    private fb: FormBuilder,
    private usedEquipmentService: UsedEquipmentService,
    private spinner: NgxSpinnerService,
    private notificationservice: NotificationService,
    public s3: S3UploadDownloadService,
    public storage: StorageDataService) { }

  ngOnInit(): void {
    this.cognitoID = this.storage.getLocalStorageData('cognitoId', false);
    this.role = localStorage.getItem("userRole");
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['id'] != '') {
        this.bulkTransactionId = params['id'];
      }
      if (params['enterprise'] != '') {
        this.enterprise = params['enterprise'];
      }
    });
    this.getBulkUploadCuration();
    this.viewingOptionsForm = this.fb.group({
      imageData: this.fb.array([])
    });
    //this.getVisualsTabList(); to be uncomment
    //this.getVisualsTabList();
  }

  get imageFormGroup() { return this.viewingOptionsForm.get("imageData") as FormArray; }
  get sourceImgFormGroup() { return this.imageFormGroup.controls; }
  sourceImgFormGroup1(index) { return this.sourceImgFormGroup[index].get('images') as FormArray; }

  getVisualsTabList() {
    forkJoin([this.usedEquipmentService.getvisualTabs(), this.usedEquipmentService.getTempVisualsByEquipmentId(this.assetId, { limit: 999 })]).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.assetVisualData = [];
        this.tabsList = res[0].results;
        this.assetVisualData = res[1].results;
        if (this.tabsList.length) {
          this.tabsList.forEach((tab: any) => {
            this.imageFormGroup.push(this.newImage(tab));
          });
        }
        for (var i = 0; i < this.tabsList.length; i++) {
          console.log(this.sourceImgFormGroup1(i));
          let length = this.sourceImgFormGroup1(i).length;
          for (var j = 0; j < length; j++) {
            this.sourceImgFormGroup1(i).removeAt(j);
          }
        }
        if (this.assetVisualData.length) {
          this.setVisualData(this.assetVisualData);
        }

      },
      err => {
        console.log(err?.message);
      }
    )


    // this.usedEquipmentService.getvisualTabs().subscribe((res:any) => {
    //   this.tabsList = res.results;
    //   if (this.tabsList.length) {
    //     this.tabsList.forEach((tab: any) => {
    //       this.imageFormGroup.push(this.newImage(tab));
    //     });
    //   }
    // },(err : any) =>{
    //   console.log(err);
    // })
  }

  newImage(tab): FormGroup {
    return this.fb.group({
      id: [tab.id],
      label: [tab.category_name],
      imageUrl: [''],
      images: this.fb.array([]),
    })
  }

  @HostListener('window:beforeunload', ['$event'])
  WindowBeforeUnoad($event: any) {
    $event.returnValue = 'Your data will be lost!';
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   // this.dataSource.sort = this.sort;
  // }
  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getBulkUploadCuration();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getBulkUploadCuration();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 3) {
      this.getBulkUploadCuration();
    }
  }
  expandRow(row: any) {
    this.expandedElement?.id != row.id ? this.expandedElement = row : this.expandedElement = undefined;
    let data = this.sourceImgFormGroup;
    // while (this.sourceImgFormGroup.length !== 0) {
    //   this.sourceImgFormGroup = [];      
    // }
    const control = this.imageFormGroup;
    for (let i = control.length - 1; i >= 0; i--) {
      control.removeAt(i)
    }
    this.editImagesGroup();
    this.getVisualsDetails(row.id);
  }
  addToMaster(type: string, obj: any) {
    let payload: any = {
      "source": type,
      "bulk_transaction_id": this.bulkTransactionId,
      "name": ""
    }
    if (this.enterprise) {
      payload.bulk_source = "Enterprise"
    }
    if (type == "Category") {
      payload.name = obj.category
    }
    else if (type == "Brand") {
      payload.name = obj.brand
    }
    // else if(type="Location"){
    //   payload.name=obj.location
    // }
    else {
      payload.name = obj.model
    }
    this.bulkService.addToMaster(payload).subscribe(res => {
      this.getBulkUploadCuration();
    })
  }
  addToDictionary(type: string, obj: any) {
    // let payload = {
    //   "source":type,
    //   "bulk_transaction_id": this.bulkTransactionId,
    //   "name":""
    // }

    let payload: any = {
      "source": type,
      "bulk_transaction_id": this.bulkTransactionId,
      "master_name": "",
      "alias_name": ""
    }
    if (this.enterprise) {
      payload.bulk_source = "Enterprise"
    }
    if (type == "Category") {
      payload.master_name = obj.matched_category;
      payload.alias_name = obj.category;
    }
    else if (type == "Brand") {
      payload.master_name = obj.matched_brand;
      payload.alias_name = obj.brand;
    }
    // else if(type=="Location"){
    //   payload.master_name=obj.category;
    //   payload.alias_name=obj.category;
    // }
    else {
      payload.master_name = obj.matched_model;
      payload.alias_name = obj.model;
    }
    this.bulkService.addToDictionary(payload).subscribe(res => {
      this.getBulkUploadCuration();
    })
  }
  getBulkUploadCuration() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      is_discarded:false
      // cognito_id: this.cognitoId,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }

    let queryparams = this.bulkTransactionId
    this.bulkService.getBulkUploadCurationById(queryparams, payload, this.enterprise).subscribe((res: any) => {
      this.total_count = res.count;
      // this.dataSource.data = res.results;

      this.mainData = res.all_equipments;
      this.mismatchBrand = res.brand_mismatch;
      this.mismatchCategory = res.category_mismatch;
      this.mismatchLocation = res.location_mismatch;
      this.mismatchModel = res.model_mismatch;
      console.log(this.mainData)
      const rows: Array<any> = [];
      this.mainData.forEach(element => rows.push(element, { detailRow: true, element }));
      this.mainData = rows;
      this.dataSource.data = this.mainData;
      this.checkDataStatus();
    });
  }
  bulkUpdateValues(type: string, obj: any) {
    // let payload = {
    //   "source":type,
    //   "bulk_transaction_id": this.bulkTransactionId,
    //   "name":""
    // }
    let payload: any = {
      "source": type,
      "bulk_transaction_id": this.bulkTransactionId,
      "old_name": "",
      "new_name": ""
    }
    if (this.enterprise) {
      payload.bulk_source = "Enterprise"
    }

    if (type == "Category") {
      payload.old_name = obj.category;
      payload.new_name = obj.matched_category;
    }
    else if (type == "Brand") {
      payload.old_name = obj.brand;
      payload.new_name = obj.matched_brand;
    }
    else if (type == "Location") {
      payload.old_name = obj.pin_code;
      payload.new_name = obj.matched_location;
    }
    else {
      payload.old_name = obj.model;
      payload.new_name = obj.matched_model;
    }
    this.bulkService.UpdateValue(payload).subscribe(res => {
      this.getBulkUploadCuration();
    })
  }
  back() {
    if (this.enterprise){
      if(this.role==this.appref.roles.admin || this.role==this.appref.roles.superadmin)
      this.router.navigate(['/admin-dashboard/bulk-listing-enterprise'])
      else
      this.router.navigate(['/customer/dashboard/bulk-listing'])
    }
    else{
      if(this.role==this.appref.roles.admin || this.role==this.appref.roles.superadmin)
      this.router.navigate(['/admin-dashboard/bulk-listing'])
      else
      this.router.navigate(['/customer/dashboard/bulk-listing'])

    }
  }
  bulkDiscardRecords(type: string, obj: any) {
    // let payload = {
    //   "source":type,
    //   "bulk_transaction_id": this.bulkTransactionId,
    //   "name":""
    // }
    let payload: any = {
      "source": type,
      "bulk_transaction_id": this.bulkTransactionId,
      "name": ""
    }
    if (this.enterprise) {
      payload.bulk_source = "Enterprise"
    }

    if (type == "Category") {
      payload.name = obj.category;
    }
    else if (type == "Brand") {
      payload.name = obj.brand;
    }
    else if (type == "Location") {
      payload.name = obj.pin_code;
    }
    else {
      payload.name = obj.model;
    }
    this.bulkService.discardRecords(payload).subscribe(res => {
      this.getBulkUploadCuration();
    })
  }
  bulkImportData() {
    // let payload = {
    //   "source":type,
    //   "bulk_transaction_id": this.bulkTransactionId,
    //   "name":""
    // }
    const dialogRef = this.dialog.open(PanVerifyPopupComponent, {
      width: '650px',
      data: {
        message: 'Are you sure you want to perform the bulk import of these assets? The assets will be imported and reflected in the portal immediately. Click Yes to proceed with this action or click No to cancel this activity.',
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.disabledButton = true;
        let payload = {
          "bulk_transaction_id": this.bulkTransactionId,
        }

        this.bulkService.bulkImportData(payload, this.enterprise).subscribe(res => {
          if (this.enterprise) {
            this.router.navigate([`./bulk-upload/confirmed`], { queryParams: { enterprise: true } });
          }
          else {
            this.router.navigate([`./bulk-upload/confirmed`]);
          }
          this.getBulkUploadCuration();
        }, err => {
          console.log(err);
          this.disabledButton = true;
        })
      }
      else {

      }
    })
  }
 async checkDataStatus() {
    this.disabledButton = false;
    if (this.mainData) {
      await this.mainData.forEach((x) => {
        if (x.status == 2) {
          this.disabledButton = true;
          return true;
        } else {
         // this.notify.success('You have successfully completed the curation process. Please click on Import to proceed further');
          //this.disabledButton = false;
          return false;
        }
      })
      if(!this.disabledButton){
        if(this.role==this.appref.roles.admin || this.role==this.appref.roles.superadmin)
        this.notify.success('You have successfully completed the curation process. Please click on Import to proceed further');
      }
    }
  }

  getDialogData(val, row) {
    if (!this.dialogData) {
      forkJoin([this.cmnService.getCategoryMaster(), this.cmnService.getBrandMaster(), this.cmnService.getModelMaster()]).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          this.dialogData = res;
          if (val == "Category") {
            this.openDialog(val, row, res[0]);
          } else if (val == "Brand") {
            this.openDialog(val, row, res[1]);
          } else {
            this.openDialog(val, row, res[2]);
          }
        },
        (err: any) => {
          console.log(err);
        }
      )
    } else {
      if (val == "Category") {
        this.openDialog(val, row, this.dialogData[0]);
      } else if (val == "Brand") {
        this.openDialog(val, row, this.dialogData[1]);
      } else {
        this.openDialog(val, row, this.dialogData[2]);
      }
    }
  }

  openDialog(val, row, data) {
    const dialogRef = this.dialog.open(BrandSelectionComponent, {
      width: '500px',
      disableClose: true,
      data: {
        heading: val,
        listing: data.results
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.data) {
        if (val == "Category") {
          row.category = result.data;
          row.is_category_valid = true;
          this.checkRowStatus(row);
        } else if (val == "Brand") {
          row.brand = result.data;
          row.is_brand_valid = true;
          this.checkRowStatus(row);
        } else {
          row.model = result.data;
          row.is_model_valid = true;
          this.checkRowStatus(row);
        }
      }
      console.log(this.mainData);
    });
  }

  checkRowStatus(row) {
    if (row.is_category_valid && row.is_brand_valid && row.is_model_valid && row.status_code == 200) {
      row.is_uploaded = true;
    }
  }

  runImporter() {

    // this.mainData.forEach(element => {
    //   element.service_date = this.datePipe.transform(element.service_date,'yyyy-MM-dd')
    // }); 
    this.cmnService.postBulkUpload(this.mainData).subscribe(
      data => {
        console.log(data);
        this.router.navigate([`bulk-upload/confirmed`])
        this.notify.success("Bulk Upload Successful");
      },
      err => {
        console.log(err);
        this.notify.error("Bulk Upload failed");
      }
    )
  }

  confirmedDialogue() { }

  async handleUpload(files: any, tab: number) {
    let visualsDataFrom: any = {};
    this.uploadVideoAndImages = [];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      var fileFormat = (file?.name).toString();
      var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
      if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        var image_path = environment.bucket_parent_folder + "/marketplace/product/" + this.cognitoID + '/' + this.assetId + '/' + tab + '_' + uuidv4() + '_' + file?.name.trim();
        var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

        if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
          visualsDataFrom = {
            visual_type: 1,
            url: uploaded_file.Location,
            temp_used_equipment: this.assetId,
            image_category: tab,
            is_primary: false
          }
          for (let i = 0; i < this.imageFormGroup.value.length; ++i) {
            if (this.imageFormGroup.value[i].images.length) {
              this.firstImage = true;
              break;
            }
          }
          if (!this.firstImage) {
            visualsDataFrom.is_primary = true;
          }
          this.uploadVideoAndImages.push(visualsDataFrom);
        }
      } else {
        this.notificationservice.error("Invalid Image.")
      }
    }
    if (this.uploadVideoAndImages.length) {
      this.saveAssetData(this.uploadVideoAndImages);
    }
    this.imageInput.nativeElement.value = '';
  }

  getVisualsDetails(id: any) {
    this.assetId = id;
    this.getVisualsTabList();
  }

  saveAssetData(data: any) {
    this.usedEquipmentService.postvisualTempUsedEquipment(data).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.setVisualData(res);
      },
      (err) => {
        this.notificationservice.error(err);
      }
    );
  }

  setVisualData(data) {
    data.forEach(element => {
      if (element.visual_type == 1) {
        let visualTab: any = this.sourceImgFormGroup.find((fg) => {
          return fg.value.id == element.image_category;
        })

        visualTab.get('images').push(this.imagesGroup(element));
      }
    });
  }

  editImagesGroup(): FormGroup {
    return this.fb.group({
      id: [],
      image_category: [],
      is_primary: [],
      url: [],
      used_equipment: [],
      visual_type: [],
      // img_path: [data.url || data.img || data.img_path]
    })
  }

  imagesGroup(data: any): FormGroup {
    return this.fb.group({
      id: [data.id],
      image_category: [data.image_category],
      is_primary: [data.is_primary],
      url: [data.url],
      used_equipment: [data.used_equipment],
      visual_type: [data.visual_type],
      // img_path: [data.url || data.img || data.img_path]
    })
  }

  delete(file, index: number, imgIndex?: any) {
    let imgPath = file?.url.slice(file?.url.indexOf(environment.bucket_parent_folder));
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.usedEquipmentService.deleteTempVisual(file.id).pipe(finalize(() => { })).subscribe(
          res => {
            this.notificationservice.success('File Successfully deleted!!')
            if (file.visual_type == 1) {
              this.sourceImgFormGroup1(index).removeAt(imgIndex);
            }
          },
          err => {
            console.log(err);
            this.notificationservice.error(err.message);
          }
        )
      },
      err => {
        console.log(err);
        this.notificationservice.error(err);
      }
    )
  }

  setPrimary(event, img) {
    let existingPrimaryImage: any;
    let primaryImage: any;
    if (event.checked) {
      this.sourceImgFormGroup.forEach((element: any) => {
        element.controls.images.controls.forEach((ele: any) => {
          if (ele.get('is_primary').value == true) {
            existingPrimaryImage = ele;
          }
          if (existingPrimaryImage && (existingPrimaryImage.value.url != img.url)) {
            existingPrimaryImage.patchValue({ is_primary: false })
          }
        })
      })
      primaryImage = this.assetVisualData.find(f => { return f.url == img.url });
    }
    let payload = {
      "equipment_id": JSON.parse(this.assetId),
      "equipment_type": 'used',
      "visual_id": primaryImage.id
    }
    this.usedEquipmentService.setPrimaryVisualUsedEquipment(payload).pipe(finalize(() => { })).subscribe(
      data => {
        this.notificationservice.success("Image successfully set as Primary Image!!");
      }, err => {
        console.log(err);
        this.notificationservice.error(err?.message);
      }
    )
  }


}