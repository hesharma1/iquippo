import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { PaymentService } from 'src/app/services/payment.service';
import { Router } from '@angular/router';
import { AuctionService } from 'src/app/services/auction.service';

@Component({
  selector: 'app-offline-payment',
  templateUrl: './offline-payment.component.html',
  styleUrls: ['./offline-payment.component.css']
})
export class OfflinePaymentComponent implements OnInit {
  @ViewChild('imageInput') imageInput!: ElementRef;

  public paymentForm!: FormGroup;
  public cognitoId!: any;
  public bidDetails: any;
  public auctionDetails: any;
  public emd_details: any;
  public auctionFullPaymentDetails: any;
  public walletBalance: number = 0;
  public paymentMethod: any = 1;
  public walletValue: any = 0;
  public paymentModeOptions = [
    { name: 'Cash', value: 'Cash' },
    { name: 'Cheque', value: 'Cheque' },
    { name: 'Demand Draft', value: 'Demand Draft' },
    { name: 'RTGS', value: 'RTGS' },
    { name: 'NEFT', value: 'NEFT' }
  ]

  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<OfflinePaymentComponent>, public s3: S3UploadDownloadService, public notify: NotificationService, public storage: StorageDataService, public paymentService: PaymentService, public router: Router, public auctionService: AuctionService) {

    this.data = data;
    if (this.data?.case == 'trade') {
      this.bidDetails = this.data?.bid_details
    } else if (this.data?.case == 'auction') {
      this.auctionDetails = this.data?.auction_details
      this.getAuctionAllfulfilmentPaymentDetails()
    } else if (this.data?.case == 'emdTopUp') {
      this.emd_details = this.data.emd_details;
    }
    this.paymentForm = this.fb.group({
      payment_mode: ['', Validators.required],
      bank_name: ['', Validators.required],
      branch: ['', Validators.required],
      proof_of_payment: ['', Validators.required],
      bank_ref_no: ['', Validators.required],
      dd_in_favour_of: [''],
      amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
    })
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
  }

  saveData() {
    let payload;
    if (this.paymentForm.value.amount == 0) {
      this.notify.error('Paying amount should be more than 0 !!');
      return;
    }
    if (this.data.case == 'trade') {
      if (this.paymentForm.value.amount > this.data?.paymentDetails?.amount_to_be_paid) {
        this.notify.error('Paying amount should be less than Amount to be Paid!!');
        return;
      } else {
        if (!this.bidDetails?.final_payments && ((this.data?.paymentDetails?.buyer_premium + this.data?.paymentDetails?.parking + this.data?.paymentDetails?.invoice_name_change_fee) > this.paymentForm.value.amount)) {
          this.notify.error('Paying amount should be at least ' + (this.data?.paymentDetails?.buyer_premium + this.data?.paymentDetails?.parking + this.data?.paymentDetails?.invoice_name_change_fee));
          return;
        } else {
          payload = {
            type: this.bidDetails?.trade_type == 'BID' ? 3 : 2,
            payment_type: 2,
            role: 1,
            buyer: this.bidDetails?.buyer?.cognito_id,
            asset_type: 2,
            payment_status: 5,
            is_online_payment: false,
            bid: this.bidDetails?.id,
            asset_id: this.bidDetails?.equipment?.id
          }
        }
      }
    } else if (this.data.case == 'auction') {
      payload = {
        type: 1,
        payment_type: 2,
        role: 1,
        buyer: this.auctionDetails?.lot_bid?.user?.cognito_id,
        asset_type: 2,
        payment_status: 5,
        is_online_payment: false,
        fulfilment: this.auctionDetails?.id,
        registered_by: this.cognitoId
      }
    } else if (this.data.case == 'emdTopUp') {
      payload = {
        auction_emd_ids: this.emd_details?.emd_details.map(e => { return e?.auction_emd?.id }),
        role: 1,
        buyer: this.emd_details?.buyer?.cognito_id,
        registered_by: this.cognitoId
      }
    } else {
      payload = {
        type: 9,
        payment_type: 1,
        role: 1,
        buyer: this.cognitoId,
        asset_type: 2,
        payment_status: 5,
        is_online_payment: false,
        invoice: this.data.invoice_number
      }
    }
    payload = { ...payload, ...this.paymentForm.value };

    this.payOffline(payload);
  }

  payOffline(payload) {
    if (this.data.case == 'emdTopUp') {
      this.paymentService.auctionTopupPayment(payload).subscribe(
        (data: any) => {
          this.paymentForm.reset();
          this.notify.success('EMD Topup added successfully !!', true);
          this.dialogRef.close(true);
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.paymentService.generateOrderId(payload).subscribe(
        (data: any) => {
          this.paymentForm.reset();
          if (this.data.case == 'trade') {
            this.router.navigate(['/admin-dashboard/trade/trade-details-dashboard']);
          } else if (this.data.case == 'auction') {
            this.updateFulfillmentStatus(data);
          } else {
            this.notify.success('Payment for the asset is updated successfully !!', true);
          }
          this.dialogRef.close(true);
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  closeForm() {
    this.paymentForm.reset();
    this.dialogRef.close();
  }

  async handleUpload(event: any) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/marketplace/enterprise/payment/" + uuidv4() + '/' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.paymentForm.get('proof_of_payment')?.setValue(uploaded_file.Location);
      }
    } else {
      this.notify.error("Invalid Image.")
    }
    this.imageInput.nativeElement.value = '';
  }

  deleteFile(imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.paymentForm.get('proof_of_payment')?.setValue('');
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }

  updateValidation(val) {
    if (val == 2) {
      this.getWalletBalance();
    } else {
      this.paymentForm = this.fb.group({
        payment_mode: ['', Validators.required],
        bank_name: ['', Validators.required],
        branch: ['', Validators.required],
        proof_of_payment: ['', Validators.required],
        bank_ref_no: ['', Validators.required],
        dd_in_favour_of: [''],
        amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
      })
    }
  }

  onPaymentModeChange(val) {
    if (val == 'Cash') {
      this.paymentForm.controls.bank_name.clearValidators();
      this.paymentForm.controls.bank_name.updateValueAndValidity();
      this.paymentForm.controls.branch.clearValidators();
      this.paymentForm.controls.branch.updateValueAndValidity();
      // this.paymentForm.controls.proof_of_payment.clearValidators();
      // this.paymentForm.controls.proof_of_payment.updateValueAndValidity();
      this.paymentForm.controls.bank_ref_no.clearValidators();
      this.paymentForm.controls.bank_ref_no.updateValueAndValidity();
      this.paymentForm.controls.dd_in_favour_of.clearValidators();
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    } else {
      this.paymentForm.controls.bank_name.setValidators([Validators.required]);
      this.paymentForm.controls.bank_name.updateValueAndValidity();
      this.paymentForm.controls.branch.setValidators([Validators.required]);
      this.paymentForm.controls.branch.updateValueAndValidity();
      this.paymentForm.controls.proof_of_payment.setValidators([Validators.required]);
      this.paymentForm.controls.proof_of_payment.updateValueAndValidity();
      this.paymentForm.controls.bank_ref_no.setValidators([Validators.required]);
      this.paymentForm.controls.bank_ref_no.updateValueAndValidity();
      this.paymentForm.controls.dd_in_favour_of.clearValidators();
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    }

    if (val == 'Demand Draft') {
      this.paymentForm.controls.dd_in_favour_of.setValidators([Validators.required]);
      this.paymentForm.controls.dd_in_favour_of.updateValueAndValidity();
    }
  }

  getFileName(str) {
    return str.split('/').pop();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realised'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

  getAuctionAllfulfilmentPaymentDetails() {
    let payload = {
      fulfilment__in: this.auctionDetails?.id
    }
    this.auctionService.getAuctionAllfulfilmentPaymentDetails(payload).subscribe(
      (res: any) => {
        this.auctionFullPaymentDetails = res?.results;
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    )
  }

  updateFulfillmentStatus(txnDetails: any) {
    let payload = {
      payment: {
        transaction: txnDetails ? txnDetails?.id : null,
      },
      fulfilment: this.auctionDetails?.id
    }
    this.paymentService.updateFulfillmentStatus(payload).subscribe(
      (data: any) => {
        this.notify.success('Auction fulfillment payment updated successfully!!', true);
      },
      err => {
        console.log(err);
      }
    )
  }

  getWalletBalance() {
    if (!this.walletBalance) {
      let walletPayload = {
        user: this.emd_details?.buyer?.cognito_id
      }
      this.paymentService.getWalletBalance(walletPayload).subscribe(
        (data: any) => {
          this.walletBalance = data?.total;
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  walletPayment() {
    if (this.walletValue == 0 || !this.walletValue) {
      this.notify.error('Paying amount should be more than 0 !!');
      return;
    }
    if(this.walletValue > this.walletBalance){
      this.notify.error('User wallet balance is less than payment amount. Please add balance in user wallet to proceed !!');
      return;
    }
    let payload = {
      auction_emd_ids: this.emd_details?.emd_details.map(e => { return e?.auction_emd?.id }),
      buyer: this.emd_details?.buyer?.cognito_id,
      amount: +this.walletValue,
      payment_mode: "Online",
      registered_by: this.cognitoId
    }
    this.paymentService.auctionWalletTopupPayment(payload).subscribe(
      (data: any) => {
        this.notify.success('EMD Topup added successfully !!', true);
        this.dialogRef.close(true);
      },
      err => {
        console.log(err);
      }
    )
  }
}
