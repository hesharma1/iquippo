import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { ViewTransactionsComponent } from '../enterprise-invoice/view-transactions/view-transactions.component';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuctionService } from 'src/app/services/auction.service';

@Component({
  selector: 'app-buyer-premium-invoices',
  templateUrl: './buyer-premium-invoices.component.html',
  styleUrls: ['./buyer-premium-invoices.component.css']
})

export class BuyerPremiumInvoicesComponent implements OnInit {
  public displayedColumns: string[] = [
    "equipment__id", "equipment__product_name", "equipment__asset_address", "buyer__first_name", "equipment__seller__first_name", "trade_type", "equipment__selling_price", "emd_details__amount", "amount", "action"
  ];

  public displayedInvoiceColumns: string[] = [
    "bid__equipment__id", "bid__equipment__product_name", "bid__equipment__asset_address", "bid__buyer__first_name", "bid__equipment__seller__first_name", "bid__trade_type", "bid__equipment__selling_price", "bid__emd_details__amount", "bid__amount",
    "action"
  ];

  public auctionDisplayedColumns: string[] = [
    "equipment__id", "equipment__product_name", "equipment__asset_address", "lot_bid__user__first_name", "equipment__seller__first_name", "trade_type", "lot_bid__amount", "emd_details__amount", "amount", "action"
  ];

  public auctionDisplayedInvoiceColumns: string[] = [
    "bid__equipment__id", "bid__equipment__product_name", "bid__equipment__asset_address", "fulfilment___lot_bid__user__first_name", "bid__equipment__seller__first_name", "bid__trade_type", "fulfilment__lot_bid__amount", "bid__emd_details__amount", "bid__amount",
    "action"
  ];

  public statusOptions: Array<any> = [
    { name: 'Request In Draft', value: 1 },
    { name: 'Request Initiated', value: 2 },
    { name: 'Request Submitted', value: 3 },
    { name: 'Inspection In Progress', value: 4 },
    { name: 'Request On Hold', value: 5 },
    { name: 'Request Closed', value: 6 },
    { name: 'Inspection Completed', value: 7 },
    { name: 'Report Submitted', value: 8 },
    { name: 'Invoice Generated', value: 9 },
    { name: 'Invoice Modified', value: 10 },
    { name: 'Invoice Cancelled', value: 11 },
    { name: 'Payment Completed', value: 12 },
    { name: 'Request Cancelled', value: 13 },
    { name: 'Request Modified', value: 14 },
  ];

  public enterpriseValuationForm: any = this.formbuilder.group({
    invoice_number: new FormControl(''),
    service_number: new FormControl(''),
    services: new FormControl(''),
    gst_number: new FormControl(''),
    pan_number: new FormControl(''),
    taxable_amount: new FormControl('', Validators.required),
    amount: new FormControl(''),
    po_ref_no: new FormControl(''),
    po_ref_date: new FormControl(''),
    sac_code: new FormControl(null),
    billing_address: new FormControl('', Validators.required),
    is_gst: new FormControl(false),
    is_self_invoice: new FormControl(false),
    is_cgst: new FormControl(false),
    is_igst: new FormControl(false),
    billed_to: new FormControl(''),
    service_to: new FormControl(''),
    type: new FormControl(null),
    invoice_date: new FormControl(new Date()),
    status: new FormControl(2),
    country: new FormControl('', Validators.required),
    state_name: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    city_name: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    pin_code: new FormControl('', Validators.required),
    registration_number: new FormControl(''),
    gst_state: new FormControl('', Validators.required),
    gst_registration_number: new FormControl('', Validators.required)
  });

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  activePageInvoice: any = 1;
  pageSizeInvoice: number = 10;
  orderingInvoice: string = "-id"
  searchTextInvoice!: string;

  fromDate;
  checkedAll = false;
  editId = '';
  stateList: any = [];
  country_data: any = [];
  gstDetails: any;
  gstState;
  public showForm: boolean = false;
  editData;
  toDate;
  fromDateInvoice;
  toDateInvoice;

  public searchText!: string;
  public activePage: any = 1;
  public total_count: any;
  public total_count_invoice: any;
  public auctionTotal_count: any;
  public auctionTotal_count_invoice: any;
  public invoiceGeneratedData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public generatedInvoiceData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public auctionInvoiceGeneratedData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public auctionGeneratedInvoiceData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public ordering: string = "-id"
  public pageSize: number = 10;
  public filter: any;
  public addRow: any = [];
  todayDate = new Date();
  iquippoGstList;
  stateGST;
  is_gst: boolean = false
  is_cgst: boolean = false
  is_igst: boolean = false
  public cognitoId!: string;
  bidId: any;
  userDataobj: any;
  public currentTab = 0;
  //public listingData = [];
  public invoiceGeneratedlistingData = [];
  public generatedInvoicelistingData = [];
  public auctionInvoiceGeneratedlistingData = [];
  public auctionGeneratedInvoicelistingData = [];
  transactionType: Array<any> = [
    { name: 'Auction', value: 1 },
    { name: 'Trade', value: 2 },
  ];
  public transactionTypeValue: any = 1;
  isShowAuctionList: boolean = true;
  fulfilmentId: any;
  asset_name: any;
  asset_id: any;
  taxable_amount: any;
  constructor(private dialog: MatDialog,
    public spinner: NgxSpinnerService,
    public valService: ValuationService,
    private masterAdminService: AdminMasterService,
    private formbuilder: FormBuilder,
    private datePipe: DatePipe,
    private agreementServiceService: AgreementServiceService,
    public storage: StorageDataService, public notify: NotificationService,
    private sharedService: SharedService, public changeDetect: ChangeDetectorRef,
    private httpClient: HttpClient, public auctionService: AuctionService) {
  }

  /**ng on init */
  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.getAuctionToBeGeneratedInvoiceData();
    this.getAuctionGeneratedInvoiceData();
    this.masterAdminService.getCountryList().subscribe((result: any) => {
      this.country_data = result.results;
    });
  }

  /**get list of to be generated invoice from trade bid api **/
  getToBeGeneratedInvoiceData() {
    let toDate = this.toDate;
    toDate = new Date(toDate);
    toDate = toDate.setDate(toDate.getDate() + 1);
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
      invoice_trade_listing: 'BP',
      is_invoice_generated: false
    };

    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.fromDate) {
      payload['created_at__gte'] = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    if (this.toDate) {
      payload['created_at__lte'] = this.datePipe.transform(toDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }


    this.masterAdminService.getTradeBidRecords(payload).subscribe((res: any) => {
      this.total_count = res.count;
      if (window.innerWidth > 768) {
        this.invoiceGeneratedData.data = res.results;
      } else {
        this.scrolled = false;
        if (this.activePage == 1) {
          this.invoiceGeneratedData.data = [];
          this.invoiceGeneratedlistingData = [];
          this.invoiceGeneratedlistingData = res.results;
          this.invoiceGeneratedData.data = this.invoiceGeneratedlistingData;
        } else {
          this.invoiceGeneratedlistingData = this.invoiceGeneratedlistingData.concat(res.results);
          this.invoiceGeneratedData.data = this.invoiceGeneratedlistingData;
        }
      }
    });
  }

  /** Invoice generated **/
  getGeneratedInvoiceData() {
    let toDateInvoice = this.toDateInvoice;
    toDateInvoice = new Date(toDateInvoice);
    toDateInvoice = toDateInvoice.setDate(toDateInvoice.getDate() + 1);
    let payload = {
      page: this.activePageInvoice,
      limit: this.pageSizeInvoice,
      ordering: this.orderingInvoice,
      type__in: 4,
      status__in: "1,2",
      bid__is_invoice_generated: true
    };
    if (this.searchTextInvoice) {
      payload['search'] = this.searchTextInvoice
    }
    if (this.fromDateInvoice) {
      payload['invoice_created_date__gte'] = this.datePipe.transform(this.fromDateInvoice, 'yyyy-MM-dd');
    }
    if (this.toDateInvoice) {
      payload['invoice_created_date__lte'] = this.datePipe.transform(toDateInvoice, 'yyyy-MM-dd');
    }
    this.valService.getIndividualInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count_invoice = res.count;
          if (window.innerWidth > 768) {
            this.generatedInvoiceData.data = res.results;
          } else {
            this.scrolledInvoice = false;
            if (this.activePageInvoice == 1) {
              this.generatedInvoiceData.data = [];
              this.generatedInvoicelistingData = [];
              this.generatedInvoicelistingData = res.results;
              this.generatedInvoiceData.data = this.generatedInvoicelistingData;
            } else {
              this.generatedInvoicelistingData = this.generatedInvoicelistingData.concat(res.results);
              this.generatedInvoiceData.data = this.generatedInvoicelistingData;
            }
          }

        }
      },
      err => {
        this.notify.error(err?.message);
      }
    )
  }

  /**get list of to be generated invoice from fulfilment api **/
  getAuctionToBeGeneratedInvoiceData() {
    let toDate = this.toDate;
    toDate = new Date(toDate);
    toDate = toDate.setDate(toDate.getDate() + 1);
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering,
      bid_status__in: 'AC',
      is_buyer_premium_invoice: false
    };

    if (this.searchText) {
      payload['search'] = this.searchText;
    }

    if (this.fromDate) {
      payload['created_at__gte'] = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    if (this.toDate) {
      payload['created_at__lte'] = this.datePipe.transform(toDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {

          this.auctionTotal_count = res.count;
          if (window.innerWidth > 768) {
            this.auctionInvoiceGeneratedData.data = res.results;
          } else {
            this.auctionScrolled = false;
            if (this.activePage == 1) {
              this.auctionInvoiceGeneratedData.data = [];
              this.auctionInvoiceGeneratedlistingData = [];
              this.auctionInvoiceGeneratedlistingData = res.results;
              this.auctionInvoiceGeneratedData.data = this.auctionInvoiceGeneratedlistingData;

            } else {
              this.auctionInvoiceGeneratedlistingData = this.auctionInvoiceGeneratedlistingData.concat(res.results);
              this.auctionInvoiceGeneratedData.data = this.auctionInvoiceGeneratedlistingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  /** Invoice generated **/
  getAuctionGeneratedInvoiceData() {
    let toDateInvoice = this.toDateInvoice;
    toDateInvoice = new Date(toDateInvoice);
    toDateInvoice = toDateInvoice.setDate(toDateInvoice.getDate() + 1);
    let payload = {
      page: this.activePageInvoice,
      limit: this.pageSizeInvoice,
      ordering: this.orderingInvoice,
      type__in: 4,
      status__in: "1,2",
      fulfilment__is_buyer_premium_invoice: true
    };
    if (this.searchTextInvoice) {
      payload['search'] = this.searchTextInvoice
    }
    if (this.fromDateInvoice) {
      payload['invoice_created_date__gte'] = this.datePipe.transform(this.fromDateInvoice, 'yyyy-MM-dd');
    }
    if (this.toDateInvoice) {
      payload['invoice_created_date__lte'] = this.datePipe.transform(toDateInvoice, 'yyyy-MM-dd');
    }
    this.valService.getIndividualInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.auctionTotal_count_invoice = res.count;
          if (window.innerWidth > 768) {
            this.auctionGeneratedInvoiceData.data = res.results;
          } else {
            this.auctionScrolledInvoice = false;
            if (this.activePageInvoice == 1) {
              this.auctionGeneratedInvoiceData.data = [];
              this.auctionGeneratedInvoicelistingData = [];
              this.auctionGeneratedInvoicelistingData = res.results;
              this.auctionGeneratedInvoiceData.data = this.auctionGeneratedInvoicelistingData;
            } else {
              this.auctionGeneratedInvoicelistingData = this.auctionGeneratedInvoicelistingData.concat(res.results);
              this.auctionGeneratedInvoiceData.data = this.auctionGeneratedInvoicelistingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err?.message);
      }
    )
  }

  closeForm() {
    this.showForm = false;
    this.editId = "";
    this.enterpriseValuationForm.reset();
  }

  /**search list  */
  searchInList(type) {
    if (this.searchText === '' || this.searchText?.length > 3 || this.searchTextInvoice === '' || this.searchTextInvoice?.length > 3) {
      if (type == 'togenerate') {
        this.activePage = 1;
        if (this.isShowAuctionList) {
          this.getAuctionToBeGeneratedInvoiceData();
        }
        else {
          this.getToBeGeneratedInvoiceData();
        }

      }
      else if (type == 'generated') {
        this.activePageInvoice = 1;
        if (this.isShowAuctionList) {
          this.getAuctionGeneratedInvoiceData();
        }
        else {
          this.getGeneratedInvoiceData();
        }
      }
    }
  }

  countryChange() {
    this.enterpriseValuationForm?.get('pin_code')?.setValue('');
    this.enterpriseValuationForm?.get('state')?.setValue('');
    this.enterpriseValuationForm?.get('city')?.setValue('');
  }

  getLocationData() {
    if (this.enterpriseValuationForm.get('country')?.value == 1) {
      let pincode = this.enterpriseValuationForm.get('pin_code')?.value;
      pincode = pincode.toString();
      if (pincode && pincode.length > 5) {
        let queryParam = `pin_code__contains=${pincode}`;
        this.agreementServiceService.getPinCode(queryParam).subscribe(
          (res: any) => {
            this.stateList = res.results;
            this.enterpriseValuationForm?.get('city_name')?.setValue(this.stateList[0].city?.name);
            this.enterpriseValuationForm?.get('pin_code')?.setValue(this.stateList[0].pin_code);
            this.enterpriseValuationForm?.get('state_name')?.setValue(this.stateList[0].city.state?.name);
            this.enterpriseValuationForm?.get('state')?.setValue(this.stateList[0].city.state?.id);
            this.enterpriseValuationForm?.get('city')?.setValue(this.stateList[0].city.id);
          },
          (err) => {
            console.error(err);
          }
        );
      }
    }
  }

  /**on chnage page from paginations */
  onPageChange(event: PageEvent) {
    this.checkedAll = false;
    this.addRow = [];
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    if (this.isShowAuctionList) {
      this.getAuctionToBeGeneratedInvoiceData();
    }
    else {
      this.getToBeGeneratedInvoiceData();
    }
  }

  onPageChangeInvoice(event: PageEvent) {
    this.activePageInvoice = event.pageIndex + 1;
    this.pageSizeInvoice = event.pageSize;
    if (this.isShowAuctionList) {
      this.getAuctionGeneratedInvoiceData();
    }
    else {
      this.getGeneratedInvoiceData();
    }
  }

  onSortColumn(event, type) {
    if (type == 'TobeGenerated') {
      if (event.active != "check") {
        this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
        if (this.isShowAuctionList) {
          this.getAuctionToBeGeneratedInvoiceData();
        }
        else {
          this.getToBeGeneratedInvoiceData();
        }
      }
    }
    else {
      if (event.active != "check") {
        this.orderingInvoice = (event.direction == "asc") ? event.active : ("-" + event.active);
        if (this.isShowAuctionList) {
          this.getAuctionGeneratedInvoiceData();
        }
        else {
          this.getGeneratedInvoiceData();
        }
      }
    }
  }

  edit(row) {
    this.enterpriseValuationForm.patchValue({
      invoice_date: row.invoice_date,
      invoice_in_favour_of: row.billed_to,
      po_ref_no: row.po_ref_number,
      po_ref_date: row.po_ref_date,
      sac_code: row.sac_code,
      address: row.billing_address,
      country: row.country,
      pin_code: row.pin_code,
      services: row.service_description,
    })
    this.showForm = true;
    this.getLocationData();
  }

  //to be generetd //
  openGenerateInvoice(data) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    if (this.isShowAuctionList) {
      let payload: any
      this.editId = "";
      this.fulfilmentId = data?.id;
      this.asset_id = data?.asset_ids;
      this.asset_name = data?.asset_names;
      this.taxable_amount = data?.actual_buyers_premium
      this.enterpriseValuationForm.patchValue({
        taxable_amount: data?.actual_buyers_premium,
        billed_to: data?.lot_bid?.user?.first_name + ' ' + data?.lot_bid?.user?.last_name
      });
      payload = {
        "services_nature__in": 4
      }
      this.valService.getSaacCode(payload).subscribe((res: any) => {
        this.enterpriseValuationForm.patchValue({
          sac_code: res?.results[0]?.sac_code,
          services: res?.results[0]?.description + ' lot ' + data?.lot_bid?.lot?.lot_number //query//
        });
        payload = {
          "sac_code__sac_code__in": res?.results[0]?.sac_code
        }
        this.valService.getGstRate(payload).subscribe((res: any) => {
          this.gstDetails = res?.results;
        })
        this.getIquippoGst();
        this.getBuyerInfo(data);
        this.masterAdminService.getCountryList().subscribe((result: any) => {
          this.country_data = result?.results;
        });
      });
      this.showForm = true;
      this.editData = data;
    }
    else {
      this.editId = "";
      this.bidId = data?.id;
      let payload: any
      payload = {
        "type": 4,
        "bid": data?.id
      }
      this.valService.getTotalFee(payload).subscribe((res: any) => {
        this.enterpriseValuationForm.patchValue({
          taxable_amount: res?.total_fees,
          billed_to: data?.buyer?.first_name + ' ' + data?.buyer.last_name
        });
        if (res?.message) {
          this.notify.error(res?.message, true);
        } else {
          payload = {
            "services_nature__in": 4
          }
          this.valService.getSaacCode(payload).subscribe((res: any) => {
            this.enterpriseValuationForm.patchValue({
              sac_code: res?.results[0]?.sac_code,
              services: res?.results[0]?.description + ' ' + data?.equipment?.category?.display_name
            });
            payload = {
              "sac_code__sac_code__in": res?.results[0]?.sac_code
            }
            this.valService.getGstRate(payload).subscribe((res: any) => {
              this.gstDetails = res?.results;
            })
            this.getIquippoGst();
            this.getBuyerInfo(data);
            this.masterAdminService.getCountryList().subscribe((result: any) => {
              this.country_data = result?.results;
            });
          });

          this.showForm = true;
          this.editData = data;
        }
      })
    }
  }

  getBuyerInfo(data) {
    if (this.isShowAuctionList) {
      this.httpClient
        .get(environment.corporateDetails + '?userId=' + data?.lot_bid?.user?.cognito_id)
        .subscribe(
          (res: any) => {
            this.userDataobj = res;
            this.enterpriseValuationForm.patchValue({
              // billing_address: this.userDataobj?.adress_details[0]?.address[0]?.addressLine1 + ' ' + this.userDataobj?.adress_details[0]?.address[0]?.addressLine2,
              //billing_address: this.userDataobj?.adress_details[0]?.address,
              billing_address: this.userDataobj?.adress_details[0]?.addressLine1 + ' ' + this.userDataobj?.adress_details[0]?.addressLine2,
              country: data?.lot_bid?.country, //Query//
              pin_code: this.userDataobj?.basic_user_details?.pinCode
            })

            this.getLocationData();

          },
          (error) => {
            console.log(error);
          }
        );
    }
    else {
      this.httpClient
        .get(environment.corporateDetails + '?userId=' + data?.buyer?.cognito_id)
        .subscribe(
          (res: any) => {
            this.userDataobj = res;
            this.enterpriseValuationForm.patchValue({
              // billing_address: this.userDataobj?.adress_details[0]?.address[0]?.addressLine1 + ' ' + this.userDataobj?.adress_details[0]?.address[0]?.addressLine2,
              //  billing_address: this.userDataobj?.adress_details[0]?.address,
              billing_address: this.userDataobj?.adress_details[0]?.addressLine1 + ' ' + this.userDataobj?.adress_details[0]?.addressLine2,
              country: data?.buyer?.pin_code?.city?.state?.country?.id,
              pin_code: this.userDataobj?.basic_user_details?.pinCode
            })

            this.getLocationData();

          },
          (error) => {
            console.log(error);
          }
        );
    }

  }

  getIquippoGst() {
    let payload = {
      "doc_type__in": 2
    }
    this.valService.getIquippoGst(payload).subscribe((res: any) => {
      this.iquippoGstList = res.results;
      this.enterpriseValuationForm.patchValue({
        gst_state: this.iquippoGstList[0].state.id
      });
      this.gstStateChange(this.iquippoGstList[0]);
    });
  }

  gstStateChange(e) {
    this.enterpriseValuationForm.get('gst_registration_number').setValue(e.doc_number);
    this.gstState = e.state.id;
  }

  saveData() {
    var modifyInvoiceRequest: any = {
    }
    modifyInvoiceRequest.invoice_number = this.editId;
    if (this.isShowAuctionList) {
      modifyInvoiceRequest.service_number = this.fulfilmentId;
      modifyInvoiceRequest.asset_name = this.asset_name?.filter(x => x != null).join(",");
      modifyInvoiceRequest.asset_id = this.asset_id?.filter(x => x != null).join(",");
    }
    else {
      modifyInvoiceRequest.service_number = this.bidId;
    }

    modifyInvoiceRequest.billed_to = this.enterpriseValuationForm?.get('billed_to')?.value;
    modifyInvoiceRequest.service_to = this.enterpriseValuationForm?.get('service_to')?.value;
    modifyInvoiceRequest.is_self_invoice = true;
    modifyInvoiceRequest.is_cgst_applicable = this.is_cgst;
    modifyInvoiceRequest.is_sgst_applicable = this.is_gst;
    modifyInvoiceRequest.is_igst_applicable = this.is_igst;
    modifyInvoiceRequest.cgst_rate = this.is_cgst ? this.gstDetails[1].service_tax_rate : null;
    modifyInvoiceRequest.sgst_rate = this.is_gst ? this.gstDetails[0].service_tax_rate : null;
    modifyInvoiceRequest.igst_rate = this.is_igst ? this.gstDetails[2].service_tax_rate : null;
    modifyInvoiceRequest.type = 4;
    if (this.isShowAuctionList) {
      modifyInvoiceRequest.fulfilment = this.fulfilmentId;
    }
    else {
      modifyInvoiceRequest.bid = this.bidId;
    }

    modifyInvoiceRequest.taxable_amount = this.enterpriseValuationForm?.get('taxable_amount')?.value;
    modifyInvoiceRequest.billing_address = this.enterpriseValuationForm?.get('billing_address')?.value;
    modifyInvoiceRequest.gst_number = this.enterpriseValuationForm?.get('registration_number')?.value;
    modifyInvoiceRequest.po_ref_number = this.enterpriseValuationForm?.get('po_ref_no')?.value;
    modifyInvoiceRequest.po_ref_date = this.datePipe.transform(this.enterpriseValuationForm?.get('po_ref_date')?.value, 'yyyy-MM-dd');
    modifyInvoiceRequest.service_description = this.enterpriseValuationForm?.get('services')?.value;
    modifyInvoiceRequest.sac_code = this.enterpriseValuationForm?.get('sac_code')?.value;
    modifyInvoiceRequest.country = this.enterpriseValuationForm?.get('country')?.value;
    modifyInvoiceRequest.state = this.enterpriseValuationForm?.get('state')?.value;
    modifyInvoiceRequest.city = this.enterpriseValuationForm?.get('city')?.value;
    modifyInvoiceRequest.pin_code = this.enterpriseValuationForm?.get('pin_code')?.value;
    modifyInvoiceRequest.iquippo_gst_state = this.gstState;
    modifyInvoiceRequest.iquippo_gst_number = this.enterpriseValuationForm?.get('gst_registration_number')?.value;

    modifyInvoiceRequest.invoice_created_date = this.datePipe.transform(this.enterpriseValuationForm?.get('po_ref_date')?.value, 'yyyy-MM-dd');
    if (this.editId) {
      this.valService.putInvoice(modifyInvoiceRequest, this.editId).subscribe((res: any) => {
        this.notify.success('Invoice Updated Successfully');
        this.closeForm();
        if (this.isShowAuctionList) {
          this.getAuctionToBeGeneratedInvoiceData();
          this.getAuctionGeneratedInvoiceData();
        }
        else {
          this.getToBeGeneratedInvoiceData();
          this.getGeneratedInvoiceData();
        }

      }
        ,
        err => {
          this.notify.error(err?.message);
        }
      )
    } else {
      this.valService.postInvoice(modifyInvoiceRequest).subscribe((res: any) => {
        this.notify.success('Invoice Generated Successfully');
        this.closeForm();
        if (this.isShowAuctionList) {
          this.getAuctionToBeGeneratedInvoiceData();
          this.getAuctionGeneratedInvoiceData();
        }
        else {
          this.getToBeGeneratedInvoiceData();
          this.getGeneratedInvoiceData();
        }
      }
        ,
        err => {
          this.notify.error(err?.message);
        }
      )
    }

  }

  downloadInvoice(url: any) {
    if (url != '') {
      this.sharedService.download(url);
    }
  }

  filterFromDate() {
    if (this.isShowAuctionList) {
      this.getAuctionToBeGeneratedInvoiceData();
    }
    else {
      this.getToBeGeneratedInvoiceData();
    }
  }

  onSellerSelect(search: any) {
    this.iquippoGstList?.filter((res) => {
      if (res.state.id == search?.value) {
        this.gstStateChange(res);
      }
    });
  }

  getTotalAmount() {
    if (this.enterpriseValuationForm?.get('taxable_amount')?.value) {
      let amount = this.enterpriseValuationForm?.get('taxable_amount')?.value;
      let fee = 0;
      if (this.is_gst) {
        fee = fee + this.gstDetails[0].service_tax_rate
      }
      if (this.is_cgst) {
        fee = fee + this.gstDetails[1].service_tax_rate;
      }
      if (this.is_igst) {
        fee = fee + this.gstDetails[2].service_tax_rate;
      }
      amount = amount + amount * fee / 100;
      return amount;
    }
    return '';
  }

  modifyInvoice(row) {
    this.valService.getInvoice(row.invoice_number).subscribe((res: any) => {
      this.showForm = true;
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      this.editId = row.invoice_number;
      this.bidId = res?.bid?.id;
      this.fulfilmentId = res?.fulfilment?.id;
      this.enterpriseValuationForm.patchValue({
        "billed_to": res.billed_to,
        "service_to": res.service_to,
        "taxable_amount": res.taxable_amount,
        "billing_address": res.billing_address,
        "registration_number": res.gst_number,
        "po_ref_no": res.po_ref_number,
        "po_ref_date": res.po_ref_date,
        "services": res.service_description,
        "sac_code": res.sac_code,
        "country": res.country.id,
        "pin_code": res.pin_code.pin_code,
        "invoice_date": res.invoice_created_date,
        "gst_registration_number": res.iquippo_gst_number,
        "gst_state": res.iquippo_gst_state.id
      });
      this.getLocationData();
      this.is_cgst = res.is_cgst_applicable;
      this.is_gst = res.is_sgst_applicable;
      this.is_igst = res.is_igst_applicable;
      let payload = {
        "sac_code__sac_code__in": res.sac_code
      }
      this.valService.getGstRate(payload).subscribe((res: any) => {
        this.gstDetails = res.results;
      })
      this.getIquippoGst();
      this.masterAdminService.getCountryList().subscribe((result: any) => {
        this.country_data = result.results;
      });
      if (this.iquippoGstList) {
        let gstState = this.iquippoGstList?.filter(e => e.state.id == res.iquippo_gst_state.id);
        this.gstState = gstState[0].state.id;
      }
    });
  }

  export(type) {
    if (type == 'ToBeGenerated') {
      if (this.isShowAuctionList) {
        this.exportAuctionToBeGeneratedData();
      }
      else {
        this.exportTradeToBeGeneratedData();
      }
    }
    else {
      if (this.isShowAuctionList) {
        this.exportAuctionGeneratedInvoiceData();
      }
      else {
        this.exportTradeGeneratedInvoiceData();
      }
    }
  }

  exportTradeToBeGeneratedData() {
    let toDate = this.toDate;
    toDate = new Date(toDate);
    toDate = toDate.setDate(toDate.getDate() + 1);
    let payload = {
      page: 1,
      limit: -1,
      ordering: this.ordering,
      invoice_trade_listing: 'BP',
      is_invoice_generated: false
    };

    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.fromDate) {
      payload['created_at__gte'] = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    if (this.toDate) {
      payload['created_at__lte'] = this.datePipe.transform(toDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }

    this.masterAdminService.getTradeBidRecords(payload).subscribe((res: any) => {
      if (res) {
        const excelData = res.results.map((row: any) => {
          return {
            "Asset ID": row?.equipment?.id,
            "Asset Name": row?.equipment?.product_name,
            "Asset Location": row?.equipment?.asset_address,
            "Buyer Name": row?.buyer?.first_name,
            "Buyer mobile number": row?.buyer?.mobile_number,
            "Seller Name": row?.equipment?.seller?.first_name,
            "Seller mobile number": row?.equipment?.seller?.mobile_number,
            "Trade Type": row?.trade_type_display_name,
            "Total Price": row?.equipment?.selling_price,
            "EMD Amount": row?.emd_details?.amount,
            "Trade Amount ": row?.amount

          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Buyer Commission Invoice");
      }
    });
  }

  exportAuctionToBeGeneratedData() {
    let toDate = this.toDate;
    toDate = new Date(toDate);
    toDate = toDate.setDate(toDate.getDate() + 1);
    let payload = {
      page: 1,
      limit: -1,
      ordering: this.ordering,
      bid_status__in: 'AC',
      is_buyer_premium_invoice: false
    };

    if (this.searchText) {
      payload['search'] = this.searchText;
    }

    if (this.fromDate) {
      payload['created_at__gte'] = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }
    if (this.toDate) {
      payload['created_at__lte'] = this.datePipe.transform(toDate, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          const excelData = res.results.map((row: any) => {
            return {
              "Asset ID": row?.asset_ids.join(", "),
              "Asset Name": row?.asset_names.join(", "),
              "Asset Location": row?.asset_location.join(", "),
              "Buyer Name": row?.lot_bid?.user?.first_name,
              "Buyer mobile number": row?.lot_bid?.user?.mobile_number,
              "Seller Name": row?.asset_seller.join(", "),
              "Trade Type": 'Auction',
              "Total Price": row?.lot_bid?.amount,
              "EMD Amount": row?.emd?.amount,
              "Trade Amount ": row?.lot_bid?.amount

            }
          });
          ExportExcelUtil.exportArrayToExcel(excelData, "Buyer Commission Invoice");
        }
      });
  }

  exportTradeGeneratedInvoiceData() {
    let toDateInvoice = this.toDateInvoice;
    toDateInvoice = new Date(toDateInvoice);
    toDateInvoice = toDateInvoice.setDate(toDateInvoice.getDate() + 1);
    let payload = {
      page: 1,
      limit: -1,
      ordering: this.orderingInvoice,
      type__in: 4,
      status__in: "1,2",
      bid__is_invoice_generated: true
    };
    if (this.searchTextInvoice) {
      payload['search'] = this.searchTextInvoice
    }
    if (this.fromDateInvoice) {
      payload['invoice_created_date__gte'] = this.datePipe.transform(this.fromDateInvoice, 'yyyy-MM-dd');
    }
    if (this.toDateInvoice) {
      payload['invoice_created_date__lte'] = this.datePipe.transform(toDateInvoice, 'yyyy-MM-dd');
    }
    this.valService.getIndividualInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          const excelData = res.results.map((row: any) => {
            return {
              "Asset ID": row?.bid?.equipment?.id,
              "Asset Name": row?.bid?.equipment?.product_name,
              "Asset Location": row?.bid?.equipment?.asset_address,
              "Buyer Name": row?.bid?.buyer?.first_name,
              "Buyer mobile number": row?.bid?.buyer?.mobile_number,
              "Seller Name": row?.bid?.equipment?.seller?.first_name,
              "Seller mobile number": row?.bid?.equipment?.seller?.mobile_number,
              "Trade Type": row?.bid?.trade_type_display_name,
              "Total Price": row?.bid?.equipment?.selling_price,
              "EMD Amount": row?.bid?.emd_details?.amount,
              "Trade Amount ": row?.bid?.amount,

            }
          });
          ExportExcelUtil.exportArrayToExcel(excelData, "Buyer Commission Invoice");
        }
      },
      err => {
        this.notify.error(err?.message);
      });
  }

  exportAuctionGeneratedInvoiceData() {
    let toDateInvoice = this.toDateInvoice;
    toDateInvoice = new Date(toDateInvoice);
    toDateInvoice = toDateInvoice.setDate(toDateInvoice.getDate() + 1);
    let payload = {
      page: 1,
      limit: -1,
      ordering: this.orderingInvoice,
      type__in: 4,
      status__in: "1,2",
      fulfilment__is_buyer_premium_invoice: true
    };
    if (this.searchTextInvoice) {
      payload['search'] = this.searchTextInvoice
    }
    if (this.fromDateInvoice) {
      payload['invoice_created_date__gte'] = this.datePipe.transform(this.fromDateInvoice, 'yyyy-MM-dd');
    }
    if (this.toDateInvoice) {
      payload['invoice_created_date__lte'] = this.datePipe.transform(toDateInvoice, 'yyyy-MM-dd');
    }
    this.valService.getIndividualInvoice(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          const excelData = res.results.map((row: any) => {
            return {
              "Asset ID": row?.fulfilment?.asset_ids.join(", "),
              "Asset Name": row?.fulfilment?.asset_names.join(", "),
              "Asset Location": row?.fulfilment?.asset_location.join(", "),
              "Buyer Name": row?.fulfilment?.lot_bid?.user?.first_name,
              "Buyer mobile number": row?.fulfilment?.lot_bid?.user?.mobile_number,
              "Seller Name": row?.fulfilment?.asset_seller.join(", "),
              "Trade Type": 'Auction',
              "Total Price": row?.fulfilment?.lot_bid?.amount,
              "EMD Amount": row?.fulfilment?.emd?.amount,
              "Trade Amount ": row?.fulfilment?.lot_bid?.amount

            }
          });
          ExportExcelUtil.exportArrayToExcel(excelData, "Buyer Commission Invoice");
        }
      },
      err => {
        this.notify.error(err?.message);
      });
  }

  viewTransaction(element, type) {
    if (type == 'isFromFulfilment') {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        element: element,
        isFromFulfilment: true
      };

      const dialogRef = this.dialog.open(ViewTransactionsComponent, dialogConfig).afterClosed().subscribe((data) => {
      });
    }
    else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = element;
      const dialogRef = this.dialog.open(ViewTransactionsComponent, dialogConfig).afterClosed().subscribe((data) => {
      });
    }

  }

  onListTabChanged(event) {
    this.currentTab = event?.index;
  }

  scrolled = false;
  scrolledInvoice = false;
  auctionScrolled = false;
  auctionScrolledInvoice = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (this.currentTab == 0) {
          if (this.isShowAuctionList) {
            if ((this.auctionTotal_count > this.auctionInvoiceGeneratedData.data.length) && (this.auctionInvoiceGeneratedData.data.length > 0)) {
              if (!this.auctionScrolled) {
                this.auctionScrolled = true;
                this.activePage++;
                this.getAuctionToBeGeneratedInvoiceData();
              }
            }
          }
          else {
            if ((this.total_count > this.invoiceGeneratedData.data.length) && (this.invoiceGeneratedData.data.length > 0)) {
              if (!this.scrolled) {
                this.scrolled = true;
                this.activePage++;
                this.getToBeGeneratedInvoiceData();
              }
            }
          }
        }
        else {
          if (this.isShowAuctionList) {
            if ((this.auctionTotal_count_invoice > this.auctionGeneratedInvoiceData.data.length) && (this.auctionGeneratedInvoiceData.data.length > 0)) {
              if (!this.auctionScrolledInvoice) {
                this.auctionScrolledInvoice = true;
                this.activePageInvoice++;
                this.getAuctionGeneratedInvoiceData();
              }
            }
          }
          else {
            if ((this.total_count_invoice > this.generatedInvoiceData.data.length) && (this.generatedInvoiceData.data.length > 0)) {
              if (!this.scrolledInvoice) {
                this.scrolledInvoice = true;
                this.activePageInvoice++;
                this.getGeneratedInvoiceData();
              }
            }
          }
        }
      }
    }
  }

  transactionChange(val) {
    this.searchText = '';
    this.searchTextInvoice = '';
    this.activePage = 1;
    this.activePageInvoice = 1;
    this.pageSize = 10;
    this.pageSizeInvoice = 10;
    this.closeForm();
    if (val == 1) {
      this.isShowAuctionList = true;
      this.getAuctionToBeGeneratedInvoiceData();
      this.getAuctionGeneratedInvoiceData();
    }
    else {
      this.isShowAuctionList = false;
      this.getToBeGeneratedInvoiceData();
      this.getGeneratedInvoiceData();
    }
  }

}