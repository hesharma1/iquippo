import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerPremiumInvoicesComponent } from './buyer-premium-invoices.component';

describe('BuyerPremiumInvoicesComponent', () => {
  let component: BuyerPremiumInvoicesComponent;
  let fixture: ComponentFixture<BuyerPremiumInvoicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyerPremiumInvoicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerPremiumInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
