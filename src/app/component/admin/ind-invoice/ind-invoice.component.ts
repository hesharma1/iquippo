import { DatePipe } from '@angular/common';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { InfoDialogComponent } from '../trade-details/info-dialog/info-dialog.component';
import { ViewReportComponent } from '../view-report/view-report.component';
import { InvoiceFeeDetailsComponent } from 'src/app/component/admin/enterprise-invoice/invoice-fee-details/invoice-fee-details.component';

@Component({
  selector: 'app-ind-invoice',
  templateUrl: './ind-invoice.component.html',
  styleUrls: ['./ind-invoice.component.css']
})
export class IndInvoiceComponent implements OnInit {@ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
@ViewChild(MatSort, { static: true }) sort!: MatSort;

stateList: any = [];
country_data: any = [];
ucn;
public showForm:boolean = false;
public searchText!: string;
public searchTextInvoice!: string;
public activePage: any = 1;
public total_count: any;
public credit_count: any;
public invoiceGeneratedData!: MatTableDataSource<any>;
public creditNoteData!: MatTableDataSource<any>;
public ordering: string = "-unique_control_number"
public pageSize: number = 10;
public activePageInvoice: any = 1;
public pageSizeInvoice: number = 10;
public orderingInvoice: string = "-invoice_number"
public total_countInvoice: any;
public filter: any;
public displayedColumns: string[] = [
  "invoice_number","unique_control_number","request_type","customer_name",
  "amount","status","invoice_date","actions"
];
public displayedColumns_credit_note: string[] = [
  "credit_number","invoice_number","unique_control_number","request_type","customer_name",
  "amount","status","invoice_date","actions"
];
public statusOptions: Array<any> = [
  { name: 'Request in Draft', value: 1 },
  { name: 'Request Submitted', value: 2 },
  { name: 'Payment Done', value: 3 },
  { name: 'Payment Failed', value: 4 },
  { name: 'Report Generated', value: 5 }
];
todayDate = new Date();
cognitoId: any;
gstDetails: any;
public enterpriseValuationForm: any = this.formbuilder.group({
  invoice_number: new FormControl('',Validators.required),
  service_number: new FormControl('',Validators.required),
  services: new FormControl(''),
  gst_number: new FormControl(''),
  pan_number: new FormControl(''),
  taxable_amount: new FormControl('', Validators.required),
  // amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
  amount: new FormControl(''),
  po_ref_no: new FormControl(''),
  po_ref_date: new FormControl(''),
  sac_code: new FormControl(null),
  billing_address: new FormControl('0'),
  is_gst: new FormControl(false),
  is_self_invoice: new FormControl(false),
  is_cgst: new FormControl(false),
  is_igst: new FormControl(false),
  billed_to: new FormControl(''),
  service_to: new FormControl(''),
  type: new FormControl(null),
  invoice_date: new FormControl(''),
  status: new FormControl(2),
  country: new FormControl('', Validators.required),
  state_name: new FormControl('', Validators.required),
  state: new FormControl('', Validators.required),
  city_name: new FormControl('', Validators.required),
  city: new FormControl('', Validators.required),
  pin_code: new FormControl(null),
  registration_number: new FormControl(''),
  gst_state: new FormControl('', Validators.required),
  gst_registration_number: new FormControl('', Validators.required),
});
is_gst: boolean = false
is_cgst: boolean = false
is_igst: boolean = false
listingData = [];
listingDataInvoice = [];
public currentTab =  0;
constructor(private dialog: MatDialog,
  public spinner: NgxSpinnerService,
  public valService: ValuationService,
  private masterAdminService: AdminMasterService,
  private formbuilder: FormBuilder,
  private datePipe: DatePipe,
  private sharedService: SharedService,
  private agreementServiceService: AgreementServiceService,
  public storage: StorageDataService, public notify: NotificationService) {
}
scrolled = false;
scrolledInvoice = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(this.currentTab == 0){
          if(((this.total_count > this.invoiceGeneratedData.data.length) && (this.invoiceGeneratedData.data.length > 0))){
            if(!this.scrolled){
              this.scrolled = true;
            this.activePage++;
            this.getPage();
            }
          }
        }else{
          if(((this.credit_count > this.creditNoteData.data.length) && (this.creditNoteData.data.length > 0))){
            if(!this.scrolledInvoice){
              this.scrolledInvoice = true;
            this.activePageInvoice++;
            this.getPageInvoice();
            }
          } 
        }
    }
  }
}

onListTabChanged(event) {
  this.currentTab = event?.index;
  console.log(this.currentTab)
}

/**ng on init */
ngOnInit(): void {
  this.cognitoId = localStorage.getItem('cognitoId');
  this.invoiceGeneratedData = new MatTableDataSource();
  this.creditNoteData = new MatTableDataSource();
  this.getPage();
  this.getPageInvoice();
  this.masterAdminService.getCountryList().subscribe((result:any)=>{
    this.country_data = result.results;
  });
}




/**get row status chnage */
getRowstatus(status) {
  if (status == '1') {
    return "Invoice Generated"
  } else if (status == '2') {
    return "Invoice Modified"
  }
  else if (status == '3') {
    return "Invoice Cancelled"
  }
  return "";
}

cancel(invoice_number){
  let payload = {
    invoice_number: invoice_number
  };
  this.valService.postCancelInvoice(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.notify.success('Invoice cancelled');
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}

closeForm(){
  this.showForm = false;
  this.enterpriseValuationForm.reset();
}

editData(){
  let modifyInvoiceRequest: any =  {
  }
  modifyInvoiceRequest.invoice_number = this.enterpriseValuationForm?.get('invoice_number')?.value;
  modifyInvoiceRequest.service_number = this.enterpriseValuationForm?.get('service_number')?.value;
  modifyInvoiceRequest.billed_to = this.enterpriseValuationForm?.get('billed_to')?.value;
  modifyInvoiceRequest.service_to = this.enterpriseValuationForm?.get('service_to')?.value;
  modifyInvoiceRequest.is_self_invoice = this.enterpriseValuationForm?.get('is_self_invoice')?.value;
  modifyInvoiceRequest.is_cgst_applicable = this.is_cgst;
  modifyInvoiceRequest.is_sgst_applicable =  this.is_gst;
  modifyInvoiceRequest.is_igst_applicable = this.is_igst;
  modifyInvoiceRequest.sgst_rate = this.gstDetails[0]?.service_tax_rate;
  modifyInvoiceRequest.cgst_rate = this.gstDetails[1]?.service_tax_rate;
  modifyInvoiceRequest.igst_rate = this.gstDetails[2]?.service_tax_rate;
  modifyInvoiceRequest.type = this.enterpriseValuationForm?.get('type')?.value;
  modifyInvoiceRequest.taxable_amount = this.enterpriseValuationForm?.get('taxable_amount')?.value;
  modifyInvoiceRequest.billing_address = this.enterpriseValuationForm?.get('billing_address')?.value;
  modifyInvoiceRequest.pan_number = this.enterpriseValuationForm?.get('pan_number')?.value;
  modifyInvoiceRequest.gst_number = this.enterpriseValuationForm?.get('registration_number')?.value;
  modifyInvoiceRequest.po_ref_number = this.enterpriseValuationForm?.get('po_ref_no')?.value;
  modifyInvoiceRequest.po_ref_date = this.datePipe.transform(this.enterpriseValuationForm?.get('po_ref_date')?.value, 'yyyy-MM-dd');
  modifyInvoiceRequest.service_description = this.enterpriseValuationForm?.get('services')?.value;
  modifyInvoiceRequest.sac_code = this.enterpriseValuationForm?.get('sac_code')?.value;
  modifyInvoiceRequest.country = this.enterpriseValuationForm?.get('country')?.value;
  modifyInvoiceRequest.state = this.enterpriseValuationForm?.get('state')?.value;
  modifyInvoiceRequest.city = this.enterpriseValuationForm?.get('city')?.value;
  modifyInvoiceRequest.pin_code = this.enterpriseValuationForm?.get('pin_code')?.value;
  modifyInvoiceRequest.updated_by = this.cognitoId;
  this.valService.putModifyInvoice(modifyInvoiceRequest.invoice_number,modifyInvoiceRequest).subscribe((res:any)=>{
    this.notify.success('Invoice Modified Successfully');
    this.showForm = false;
    this.getPage();
  }
  ,
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

getAssetCondition(condition) {
  if (condition == 'GD') {
    return "Good"
  } else if (condition == 'EX') {
    return "Excellent"
  } else if (condition == 'AVG') {
    return "Average"
  } else if (condition == 'SCR') {
    return "Scrap"
  }
    else if (condition == 'POOR'){
      return "Poor"
    }
    else if (condition == 'VGD'){
      return "Very Good"
    }
  return "";
}

/**search list  */
searchInList() {
  if (this.searchText === '' || this.searchText?.length > 3) {
    this.activePage = 1;
    this.getPage();
  }
}
searchInListInvoice() {
  if (this.searchTextInvoice === '' || this.searchTextInvoice?.length > 3) {
    this.activePageInvoice = 1;
    this.getPageInvoice();
  }
}

countryChange(){
  
  this.enterpriseValuationForm?.get('pin_code')?.setValue('');
  this.enterpriseValuationForm?.get('state')?.setValue('');
  this.enterpriseValuationForm?.get('city')?.setValue('');
}

statusFilter(val) {
  this.filter = val;
  this.activePage = 1;
  this.getPage();
}

/**get list from api */
getPage() {
  let payload = {
    page: this.activePage,
    limit: this.pageSize,
    ordering: this.ordering,
    type__in: 1,
    status__in: '1,2'
  };
  if (this.searchText) {
    payload['search'] = this.searchText;
  }
  // if (this.filter) {
  //   payload['status__in'] = this.filter;
  // }
  this.valService.getIndividualInvoice(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      if (res) {
        this.total_count = res.count;
        // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.invoiceGeneratedData.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.invoiceGeneratedData.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.invoiceGeneratedData.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.invoiceGeneratedData.data = this.listingData;
          }
        }
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  );
  
}
getPageInvoice(){
  let payload_credit_note = {
    page: this.activePageInvoice,
    limit: this.pageSizeInvoice,
    ordering: this.orderingInvoice,
    invoice__type__in: 1
  }
  if(this.searchTextInvoice){
    payload_credit_note['search'] = this.searchTextInvoice
  }
  this.valService.getcreditNoteData(payload_credit_note).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      if (res) {
        this.credit_count = res.count;
        
        // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.creditNoteData.data = res.results;
        }else{
          this.scrolledInvoice = false;
          if(this.activePageInvoice == 1){
            this.creditNoteData.data = [];
            this.listingDataInvoice = [];
            this.listingDataInvoice = res.results;
            this.creditNoteData.data = this.listingDataInvoice;
          }else{
            this.listingDataInvoice = this.listingDataInvoice.concat(res.results);
            this.creditNoteData.data = this.listingDataInvoice;
          }
        }
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

/**on chnage page from paginations */
onPageChange(event: PageEvent) {
  this.activePage = event.pageIndex + 1;
  this.pageSize = event.pageSize;
  this.getPage();
}
onPageChangeInvoice(event: PageEvent) {
  this.activePageInvoice = event.pageIndex + 1;
  this.pageSizeInvoice = event.pageSize;
  this.getPageInvoice();
}

onSortColumn(event) {
  this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPage();
}
onSortColumnInvoice(event) {
  this.orderingInvoice = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPageInvoice();
}

downloadInvoice(url : any){
  if(url != ''){
    this.sharedService.download(url);
  }
}

edit(row){
  let date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd')!
  let payload = {
    sac_code__sac_code__in: row.sac_code,
    effective_from__lte: date,
    effective_to__gte: date,
  };
  this.valService.getgstdetails(payload).subscribe((res:any)=>{
    this.gstDetails = res.results;
  })
  this.enterpriseValuationForm.patchValue({
  invoice_number: row.invoice_number,
  invoice_type_display_name: row.invoice_type_display_name,
  service_number: row.service_number,
  billed_to: row.billed_to,
  service_to: row.service_to,
  is_self_invoice: row.is_self_invoice,
  type: row.type,
  taxable_amount: row.taxable_amount,
  is_gst: row.is_sgst_applicable,
  is_cgst: row.is_cgst_applicable,
  is_igst: row.is_igst_applicable,
  pan_number: row.pan_number,
  registration_number: row.gst_number,
  po_ref_no: row.po_ref_number,
  po_ref_date: row.po_ref_date,
  sac_code: row.sac_code,
  gst_state: row.iquippo_gst_state.name,
  gst_registration_number: row.iquippo_gst_number,
  billing_address: row.billing_address,
  country: row.country.id,
  state: row.state.id,
  state_name: row.state.name,
  city: row.city.id,
  city_name: row.city.name,
  pin_code: row.pin_code.pin_code,
  services: row.service_description,
  invoice_date: row.invoice_created_at
})
this.showForm = true;
this.is_gst = row.is_sgst_applicable,
this.is_cgst = row.is_cgst_applicable,
this.is_igst = row.is_igst_applicable
this.ucn = row.service_number
}

/**Export Function */
export(string) {
  let payload = {
    limit: 999
  }
  if(string != 'all'){
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
  }
  this.valService.getInstaValuation(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "UCN": r?.unique_control_number,
          "Name": r?.valuation_asset?.seller.first_name,
          "Mobile":r?.valuation_asset?.seller.mobile,
          "Email":r?.valuation_asset?.seller.email,
          "Category":r?.valuation_asset?.category.display_name,
          "Brand": r?.brand?.display_name,
          "Model": r?.model?.name,
          "Manufacturing Year": r?.valuation_asset?.mfg_year,
          "Invoice Availability":r?.valuation_asset?.is_original_invoice,
          "Invoice Amount":r?.valuation_asset?.invoice_value,
          "Registration Status":r?.valuation_asset?.is_rc_available,
          "Registration number":r?.valuation_asset?.rc_number,
          "Run":r?.valuation_asset?.hmr_kmr,
          "Accident Update":r?.valuation_asset?.is_equipment_accidentally_damaged,
          "Asset Condition":r?.valuation_asset?.asset_condition,
          "Status": this.getRowstatus(r?.status),
          "Created At": r?.created_at
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Insta Valuation List");
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
  }
  viewDetails() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'my-class';
    dialogConfig.width = '900px';
    // for(let row of this.addRow){
    // dialogConfig.data.push(row.unique_control_number);
    // }
    let obj = {
      "ucn": this.ucn,
      "isCreditNote": false
    }
    dialogConfig.data = obj;
    const dialogRef = this.dialog.open(InvoiceFeeDetailsComponent, dialogConfig).afterClosed().subscribe((data) => {
      let payload: any
      payload = {
        "ucns":this.ucn,
        "type": 3
      }
      this.valService.getTotalFee(payload).subscribe((res: any) => {
        this.enterpriseValuationForm.patchValue({
          taxable_amount: res.total_fees
        });
      }); 
    });
  }
  getTotalAmount(){
    if(this.enterpriseValuationForm?.get('taxable_amount')?.value){
      let amount = Number(this.enterpriseValuationForm?.get('taxable_amount')?.value);
      let fee = 0;
      if(this.is_gst){
       fee = fee + this.gstDetails[0].service_tax_rate
      }
      if(this.is_cgst){
        fee = fee + this.gstDetails[1].service_tax_rate;
      }
      if(this.is_igst){
        fee = fee + this.gstDetails[2].service_tax_rate;
      }
      amount = amount + amount * fee/100;
      return amount;
    }
    return '';
  }
}
