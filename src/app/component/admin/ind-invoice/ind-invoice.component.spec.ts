import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndInvoiceComponent } from './ind-invoice.component';

describe('IndInvoiceComponent', () => {
  let component: IndInvoiceComponent;
  let fixture: ComponentFixture<IndInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
