import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { CommonService } from 'src/app/services/common.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
  selector: 'app-view-report',
  templateUrl: './view-report.component.html',
  styleUrls: ['./view-report.component.css']
})
export class ViewReportComponent implements OnInit {
  reportData;
  constructor( @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ViewReportComponent>) {
   this.reportData =data;
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }
  getAssetCondition(){
    switch (this.reportData.asset_condition) {
      case "EX": {
        return '/assets/images/asset-condition-excellent.png';
        break;
      }
      case "VGD": {
        return '/assets/images/asset-condition-verygood.png';
        break;
      }
      case "GD": {
        return '/assets/images/asset-condition-good.png';
        break;
      }
      case "AVG": {
        return '/assets/images/asset-condition-average.png';
        break;
      }
      case "SCR": {
        return '/assets/images/asset-condition-scrap.png';
        break;
      }
      case "POOR": {
        return '/assets/images/asset-condition-poor.png';
        break;
      }
      default: {
        return '';
        break;
      }
  }
}
}
