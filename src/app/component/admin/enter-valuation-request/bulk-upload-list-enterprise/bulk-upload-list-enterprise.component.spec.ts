import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadListEnterpriseComponent } from './bulk-upload-list-enterprise.component';

describe('BulkUploadListEnterpriseComponent', () => {
  let component: BulkUploadListEnterpriseComponent;
  let fixture: ComponentFixture<BulkUploadListEnterpriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkUploadListEnterpriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadListEnterpriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
