import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadEnterpriseComponent } from './bulk-upload-enterprise.component';

describe('BulkUploadEnterpriseComponent', () => {
  let component: BulkUploadEnterpriseComponent;
  let fixture: ComponentFixture<BulkUploadEnterpriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkUploadEnterpriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadEnterpriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
