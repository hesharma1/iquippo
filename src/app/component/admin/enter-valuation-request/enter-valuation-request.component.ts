import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { ViewReportComponent } from '../view-report/view-report.component';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { AgreementServiceService } from 'src/app/services/agreement-service.service';
import { S3UploadDownloadService } from '../../../utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { DatePipe } from '@angular/common';
import { EnterpriseInfoDialogComponent } from '../enter-valuation-request/info-dialog/enterprise-info-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { sellEquipment, visualData } from 'src/app/models/sellEquipment.model';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { ImageCropperModalComponent } from 'src/app/component/shared/image-cropper-modal/image-cropper-modal.component';
import { forkJoin } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { SharedService } from 'src/app/services/shared-service.service';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  },
};
@Component({
  selector: 'app-enter-valuation-request',
  templateUrl: './enter-valuation-request.component.html',
  styleUrls: ['./enter-valuation-request.component.css'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },
  //   { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  // ]
})
export class EnterValuationRequestComponent implements OnInit {@ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
@ViewChild(MatSort, { static: true }) sort!: MatSort;

@ViewChild('imageInput') imageInput!: ElementRef;
@ViewChild('videoInput') videoInput!: ElementRef;
public viewingOptionsForm!: FormGroup;
  public assetId: any;
  public assetDetail: any;
  public tabsList: any;
  public assetVisualData: any;
  public cognitoID!: string;
  yearOfManufacture= '';
  public docType: { [key: string]: number } = {
    "IMAGE": 1,
    "VIDEO_LINK": 2,
    "VIDEO_UPLOAD": 3
  }

  public uploadVideoAndImages: Array<object> = [];
  public files: NgxFileDropEntry[] = [];
  public firstImage: boolean = false;
  public steps = {
    current: 3,
    total: [
      { title: 'Basic Details' },
      { title: 'Vehicle Details' },
      { title: 'Visuals' }
    ]
  }
todayDate = new Date();
public enterpriseValuationForm: any = this.formbuilder.group({
  request_type: new FormControl('', Validators.required),
  purpose: new FormControl('', Validators.required),
  // amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
  original_owner: new FormControl(''),
  customer_seeking_finance: new FormControl(''),
  reference_number: new FormControl(''),
  repo_date: new FormControl(null),
  is_original_invoice: new FormControl('0'),
  invoice_value: new FormControl(null),
  invoice_date: new FormControl(''),
  invoice_document: new FormControl(''),
  category: new FormControl('', Validators.required),
  otherCategory: new FormControl(''),
  brand: new FormControl('', Validators.required),
  otherBrand: new FormControl(''),
  model: new FormControl('', Validators.required),
  otherModel: new FormControl(''),
  asset_name: new FormControl('', Validators.required),
  engine_number: new FormControl(''),
  chassis_number: new FormControl(''),
  is_equipment_registered: new FormControl('0', Validators.required),
  rc_number: new FormControl(''),
  machine_sr_number: new FormControl(''),
  mfg_year: new FormControl(''),
  asset_address: new FormControl('', Validators.required),
  country: new FormControl('', Validators.required),
  state: new FormControl(''),
  city: new FormControl(''),
  pin_code: new FormControl('', Validators.required),
  contact_person: new FormControl('', Validators.required),
  contact_number: new FormControl('', Validators.required),
  distance_to_be_travelled: new FormControl(''),
  enterprise: new FormControl('', Validators.required),
  rc_document: new FormControl(''),
  request_date: new FormControl(this.todayDate),
  created_by: new FormControl(''),
  status: new FormControl(''),
  update_source: new FormControl(''),
}); 
public agencyInfoForm: any = this.formbuilder.group({
  report_number: new FormControl(''),
  report_created_at: new FormControl(''),
  hour_meter_reading: new FormControl(''),
  hmr_kmr: new FormControl('',[Validators.pattern('[1-9][0-9]+'),Validators.minLength(1), Validators.maxLength(15)]),
  valuation_amount: new FormControl(''),
  inspection_by: new FormControl(''),
  asset_condition: new FormControl(''),
  gps_installed: new FormControl(''),
  gps_device_number: new FormControl(''),
  imei_number: new FormControl(''),
  distance_to_be_travelled: new FormControl(''),
  overall_general_condition: new FormControl(''),
  update_source: new FormControl(''),
});
public searchText!: string;
public activePage: any = 1;
public total_count: any;
public dataSource!: MatTableDataSource<any>;
public ordering: string = "-request_date"
public pageSize: number = 10;
public filter: any;
fromDate;
toDate;
documentName: Array<any> = [];
documentRCName: Array<any> = [];
imageName: any;
uploadingImage: boolean = false;
isInvalidfile: boolean = false;
public showForm:boolean = false;
categoryList: any = [];
brandList: any = [];
modelList: any = [];
categoryData: any;
brandData: any;
modalData: any;
categoryId: any;
brandId: any;
modalId: any;
stateList: any;
countryList = [];
states = [];
purposeList = [];
enterpriseCustomerList = [];
editId: any= '';
cognitoId;
isdisabled: boolean = false;
public displayedColumns: string[] = [
  "unique_control_number","request_type","purpose","status","reference_number",
  "enterprise__company_name","request_date","customer_seeking_finance","job_id","valuation_asset__product_name","report_generated","report_created_at",
  "actions"
];
public statusOptions: Array<any> = [
  { name: 'Request In Draft', value: 1 },
  { name: 'Request Initiated', value: 2 },
  { name: 'Request Submitted', value: 3 },
  { name: 'Inspection In Progress', value: 4 },
  { name: 'Request On Hold', value: 5 },
  { name: 'Request Closed', value: 6 },
  { name: 'Inspection Completed', value: 7 },
  { name: 'Report Submitted', value: 8 },
  { name: 'Invoice Generated', value: 9 },
  { name: 'Invoice Modified', value: 10 },
  { name: 'Invoice Cancelled', value: 11 },
  { name: 'Payment Completed', value: 12 },
  { name: 'Request Cancelled', value: 13 },
  { name: 'Request Modified', value: 14 },
];
public listingData = [];

constructor(private dialog: MatDialog,
  public spinner: NgxSpinnerService,
  public valService: ValuationService,
  public storage: StorageDataService, public notify: NotificationService,
  private formbuilder: FormBuilder,
  private commonService: CommonService,
  private agreementServiceService: AgreementServiceService,
  public s3: S3UploadDownloadService,
  private adminMasterService: AdminMasterService,
  private datePipe: DatePipe,private sharedService: SharedService,
  private router: Router,private usedEquipmentService: UsedEquipmentService, 
  private notificationservice: NotificationService, private fb: FormBuilder, 
  public route: ActivatedRoute) {
}

/**ng on init */
ngOnInit(): void {
  this.cognitoId = this.storage.getStorageData('cognitoId', false);
  this.dataSource = new MatTableDataSource();
  this.getPage();
  this.getCategoryMasters();
  this.getCountryMaster(`limit=999`);
  this.getPurposeList();
  this.getEnterpriseCustomer();
}


/**get row status chnage */
getRowstatus(status) {
  let name;
  for(let row of this.statusOptions){
    if(row.value == status){
      name = row.name;
      break;
    }
  }
  return name;
}

getAssetCondition(condition) {
  if (condition == 'GD') {
    return "Good"
  } else if (condition == 'EX') {
    return "Excellent"
  } else if (status == 'AVG') {
    return "Average"
  } else if (condition == 'SCR') {
    return "Scrap"
  }
    else if (condition == 'POOR'){
      return "Poor"
    }
    else if (condition == 'VGD'){
      return "Poor"
    }
  return "";
}

/**search list  */
searchInList() {
  if (this.searchText === '' || this.searchText?.length > 3) {
    this.activePage = 1;
    this.getPage();
  }
}

statusFilter(val) {
  this.filter = val;
  this.activePage = 1;
  this.getPage();
}

/**get list from api */
getPage() {
  let payload = {
    page: this.activePage,
    limit: this.pageSize,
    ordering: this.ordering,
  };
  if (this.searchText) {
    payload['search'] = this.searchText;
  }
  if (this.filter) {
    payload['status__in'] = this.filter;
  }
  if(this.fromDate){
    payload['request_date__gte'] = this.datePipe.transform(this.fromDate,'yyyy-MM-dd');
  }
  if(this.toDate){
    payload['request_date__lte'] = this.datePipe.transform(this.toDate,'yyyy-MM-dd');
  }
  this.valService.getEnterValuation(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      if (res) {
        this.total_count = res.count;
       // Changes for Card layout on Mobile devices
       if(window.innerWidth > 768){
        this.dataSource.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.dataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.dataSource.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.dataSource.data = this.listingData;
          }
        }
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

/**on chnage page from paginations */
onPageChange(event: PageEvent) {
  this.activePage = event.pageIndex + 1;
  this.pageSize = event.pageSize;
  this.getPage();
}
scrolled = false;
// Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if(((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) ){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
          this.getPage();
        }
      }
    }
  }
}
onSortColumn(event) {
  this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPage();
}

/**Export Function */
export(string) {
  let payload = {
    limit: 999
  }
  if(string != 'all'){
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
  }
  this.valService.getEnterValuation(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "UCN": r?.unique_control_number,
          "Request Type": r?.request_type,
          "Purpose":r?.purpose,
          "Reference No":r?.reference_number,
          "Enterprise":r?.enterprise,
          "Customer Seeking Finance":r?.customer_seeking_finance,
          "Job Id": r?.job_id,
          "Asset Name": r?.asset_name,
          "Request Date":this.datePipe.transform(r?.request_date,'dd-MM-yyyy'),
          "Status":r?.status_display_name,
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Enterprise Valuation Requests");
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}

getCategoryMasters() {
  this.commonService.getCategoryMaster('limit=999').subscribe(
    (res: any) => {
      this.categoryList = res.results;
      this.categoryData = res.results;
    },
    (err) => {
      console.error(err);
    }
  );
}

/*** get state data*/
getLocationData() {
  let pincode = this.enterpriseValuationForm?.get('pin_code')?.value;
  let queryParam = `search=${pincode}`;
  pincode = pincode.toString()
  if (pincode && pincode.length > 4) {
   
    this.enterpriseValuationForm
        ?.get('city')
        ?.setValue('');
      this.enterpriseValuationForm
        ?.get('state')
        ?.setValue('');
    this.agreementServiceService.getPinCode(queryParam).subscribe(
      (res: any) => {
        this.stateList = [];
        this.stateList = res.results;
        if( this.stateList){
          this.enterpriseValuationForm
          ?.get('city')
          ?.setValue(this.stateList[0].city?.name);
        this.enterpriseValuationForm
          ?.get('state')
          ?.setValue(this.stateList[0].city.state?.name);
        }
        
      },
      (err) => {
        console.error(err);
      }
    );
  }
}

validatePincode() {
  let pincode = this.enterpriseValuationForm?.get('pin_code')?.value;
  if (this.stateList) {
    pincode = pincode.toString()
    let pinvalid = this.stateList.filter(e => e.pin_code == pincode)
    if (pinvalid?.length > 0) {
      return false;
    } else {
      return true;
    }
  }
  return true;
}

filterValuesCategory(search: any) {
  let data = search.target.value
  let filtered = this.categoryData;
  let filteredData: any
  if (filtered && filtered.length > 0) {
    filteredData = filtered.filter(
      item => item.name.toLowerCase().includes(data.toLowerCase())
    );
  }
  if (filteredData && filteredData.length > 0) {
    this.categoryList = filteredData
  } else {
    this.categoryList = []
  }
}

filterValuesBrand(search: any) {
  let data = search.target.value
  let filtered = this.brandData;
  let filteredData: any
  if (filtered && filtered.length > 0) {
    filteredData = filtered.filter(
      item => item.name.toLowerCase().includes(data.toLowerCase())
    );
  }
  if (filteredData && filteredData.length > 0) {
    this.brandList = filteredData
  } else {
    this.brandList = [];
  }
}

filterValuesModal(search: any) {
  let data = search.target.value
  let filtered = this.modalData;
  let filteredData: any
  if (filtered && filtered.length > 0) {
    filteredData = filtered.filter(
      item => item.name.toLowerCase().includes(data.toLowerCase())
    );
  }
  if (filteredData && filteredData.length > 0) {
    this.modelList = filteredData
  } else {
    this.modelList = []
  }
}

onModalSelect(modal: any) {
  this.modalId = modal.id;
  this.enterpriseValuationForm.get('model')?.setValue(modal.name);
  this.enterpriseValuationForm.get('asset_name')?.setValue(
    this.enterpriseValuationForm.get('category')?.value + ' ' + this.enterpriseValuationForm.get('brand')?.value + ' ' + this.enterpriseValuationForm.get('model')?.value
  )  
}

onCategorySelect(categoryId) {
  this.enterpriseValuationForm.get('category')?.setValue(categoryId.display_name);
  if (categoryId.id != this.categoryId) {
    this.enterpriseValuationForm.get('model')?.setValue('');
    this.enterpriseValuationForm.get('brand')?.setValue('');
    this.enterpriseValuationForm.get('category')?.setValue(categoryId.display_name);
    this.categoryId = categoryId.id
    if (this.categoryId != 0) {
      this.commonService.getBrandMasterByCatId(this.categoryId).pipe(finalize(() => {   })).subscribe(
        (res: any) => {
          this.brandList = res.results;
          this.brandData = res.results;
        },
        err => {
          console.error(err);
        });
    }
  }

}

onBrandSelect(brandId: any) {
  this.enterpriseValuationForm.get('brand')?.setValue(brandId.display_name);
  if (brandId.id != this.brandId) {
    this.brandId = brandId.id;
    this.enterpriseValuationForm.get('model')?.setValue('');
    this.enterpriseValuationForm.get('brand')?.setValue(brandId.display_name);
    if (this.brandId != 0) {
      this.commonService.getModelMasterByBrandId(brandId.id,this.categoryId).pipe(finalize(() => {   })).subscribe(
        (res: any) => {
          this.modelList = res.results;
          this.modalData = res.results;
        },
        err => {
          console.error(err);
        });
    }
  }
}
async handleUploadForm(event: any, tab: string, side: string) {
  const file = event.target.files[0];
  this.isInvalidfile = true;
  this.uploadingImage = true;
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {

  }
  var fileFormat = (file?.name).toString();
  var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
  var image_path = environment.bucket_parent_folder + "/" + tab + '/' + side + '.' + fileExt;
  var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);
  console.log(uploaded_file);
  if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
    if (tab == 'invoice') {
      this.documentName.push({ name: fileFormat, url: uploaded_file.Location });
      console.log(this.documentName)
      this.imageName = uploaded_file.Location;
      this.enterpriseValuationForm.get('invoice_document')?.setValue(uploaded_file.Location);
    }else if(tab == 'rc'){
      this.documentRCName.push({ name: fileFormat, url: uploaded_file.Location });
      console.log(this.documentRCName)
      this.imageName = uploaded_file.Location;
      this.enterpriseValuationForm.get('rc_document')?.setValue(uploaded_file.Location);
    }


    //this.uploadDocumentPath.push(documentDataRequest);
  }
  this.uploadingImage = false;
}
resetFile(i, url) {
  let id = this.documentName[i].id;
  this.documentName.splice(i, 1);
  let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
  this.s3.deleteFile(imgPath).subscribe(res => {
  })
}
resetRCFile(i, url) {
  let id = this.documentRCName[i].id;
  this.documentRCName.splice(i, 1);
  let imgPath = url.slice(url.indexOf(environment.bucket_parent_folder));
  this.s3.deleteFile(imgPath).subscribe(res => {
  })
}

  maxDate = new Date();
  minDate = this.maxDate.setFullYear(this.maxDate.getFullYear() - 15);
  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.enterpriseValuationForm.get('mfg_year');
    ctrlValue?.setValue(_moment(this.minDate));
    ctrlValue?.value.year(normalizedYear.year());
    ctrlValue?.value.month(normalizedYear.month());
    ctrlValue?.setValue(ctrlValue.value);
    this.enterpriseValuationForm?.get('mfg_year')?.setValue(ctrlValue?.value);
    //this.addNewModelForm.get('yearLaunched')?.setValue(this.datePipe.transform(this.addNewModelForm.get('yearLaunched')?.value, 'yyyy'));
    datepicker.close();
  }

  getCountryMaster(queryParams: string) {
    this.adminMasterService.getCountryMaster(queryParams).subscribe(
        (res: any) => {
            this.countryList = res.results;
        },
        err => {
            console.error(err);
        });
 }

 saveData(){
   let count = 0;
   if(this.enterpriseValuationForm.value.rc_number){
    count++;
   }
   if(this.enterpriseValuationForm.value.chassis_number){
     count++;
   }
   if(this.enterpriseValuationForm.value.engine_number){
    count++;
  }
  if(this.enterpriseValuationForm.value.machine_sr_number){
    count++;
  }
  if(count <2){
    this.notify.error("Please enter either two of Engine Number, Chassis Number or Serial Number")
    return;
  }
  // if(!(this.enterpriseValuationForm.value.rc_number || this.enterpriseValuationForm.value.chassis_number || this.enterpriseValuationForm.value.engine_number || this.enterpriseValuationForm.value.machine_sr_number)){
  //   this.notify.error("Please enter either two of Engine Number, Chassis Number or Serial Number")
  //   return;
  // }
  else{
     let request:any ={};
     let date = this.enterpriseValuationForm.controls['mfg_year'].value
     request.request_type =  this.enterpriseValuationForm.get('request_type').value,
     request.purpose = this.enterpriseValuationForm.get('purpose').value,
     request.original_owner = this.enterpriseValuationForm.get('original_owner').value,
     request.customer_seeking_finance= this.enterpriseValuationForm.get('customer_seeking_finance').value,
     request.reference_number= this.enterpriseValuationForm.get('reference_number').value,
     request.is_original_invoice= this.enterpriseValuationForm.get('is_original_invoice').value  == '1'? true:false,
     request.invoice_value= this.enterpriseValuationForm.get('invoice_value').value,
     request.invoice_date= this.datePipe.transform(this.enterpriseValuationForm.controls['invoice_date'].value, 'yyyy-MM-dd'),
     request.invoice_document= this.enterpriseValuationForm.get('invoice_document').value,
     request.category= this.categoryId,
     request.brand= this.brandId,
     request.model= this.modalId,
     request.asset_name= this.enterpriseValuationForm.get('asset_name').value,
     request.engine_number= this.enterpriseValuationForm.get('engine_number').value,
     request.chassis_number=this.enterpriseValuationForm.get('chassis_number').value,
     request.is_equipment_registered=this.enterpriseValuationForm.get('is_equipment_registered').value  == '1'? true:false,
     request.rc_number=this.enterpriseValuationForm.get('rc_number').value,
     request.machine_sr_number= this.enterpriseValuationForm.get('machine_sr_number').value,
     request.mfg_year= this.datePipe.transform(date, 'yyyy'),
     request.asset_address= this.enterpriseValuationForm.get('asset_address').value,
     request.country= this.enterpriseValuationForm.get('country').value,
     request.state= this.enterpriseValuationForm.get('state').value,
     request.city= this.enterpriseValuationForm.get('city').value,
     request.pin_code= this.enterpriseValuationForm.get('pin_code').value,
     request.contact_person= this.enterpriseValuationForm.get('contact_person').value,
     request.contact_number= this.enterpriseValuationForm.get('contact_number').value,
     request.enterprise= this.enterpriseValuationForm.get('enterprise').value,
     request.rc_document= this.enterpriseValuationForm.get('rc_document').value,
     request.request_date= this.datePipe.transform(this.enterpriseValuationForm.controls['request_date'].value, 'yyyy-MM-dd'),
     request.created_by= this.cognitoId,
     request.status= this.enterpriseValuationForm.get('status').value
     if(this.enterpriseValuationForm.controls['repo_date'].value){
      request.repo_date= this.datePipe.transform(this.enterpriseValuationForm.controls['repo_date'].value, 'yyyy-MM-dd')
     }
     if(this.enterpriseValuationForm.get('distance_to_be_travelled').value){
      request.distance_to_be_travelled= this.enterpriseValuationForm.get('distance_to_be_travelled').value;
     }
    if(this.editId){
      request.update_source = true;
      this.valService.putEnterpriseValuation(request,this.editId).subscribe(res=>{
        console.log(res);
        this.showForm = false;
        this.editId = '';
        this.isdisabled = false;
        this.enterpriseValuationForm.reset();
        this.enterpriseValuationForm = this.formbuilder.group({
          request_type: new FormControl('', Validators.required),
          purpose: new FormControl('', Validators.required),
          // amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
          original_owner: new FormControl(''),
          customer_seeking_finance: new FormControl(''),
          reference_number: new FormControl(''),
          repo_date: new FormControl(null),
          is_original_invoice: new FormControl('0'),
          invoice_value: new FormControl(null),
          invoice_date: new FormControl(''),
          invoice_document: new FormControl(''),
          category: new FormControl('', Validators.required),
          otherCategory: new FormControl(''),
          brand: new FormControl('', Validators.required),
          otherBrand: new FormControl(''),
          model: new FormControl('', Validators.required),
          otherModel: new FormControl(''),
          asset_name: new FormControl('', Validators.required),
          engine_number: new FormControl(''),
          chassis_number: new FormControl(''),
          is_equipment_registered: new FormControl('0', Validators.required),
          rc_number: new FormControl(''),
          machine_sr_number: new FormControl(''),
          mfg_year: new FormControl(''),
          asset_address: new FormControl('', Validators.required),
          country: new FormControl('', Validators.required),
          state: new FormControl(''),
          city: new FormControl(''),
          pin_code: new FormControl('', Validators.required),
          contact_person: new FormControl('', Validators.required),
          contact_number: new FormControl('', Validators.required),
          distance_to_be_travelled: new FormControl(''),
          enterprise: new FormControl('', Validators.required),
          rc_document: new FormControl(''),
          request_date: new FormControl(this.todayDate),
          created_by: new FormControl(''),
          status: new FormControl(''),
          update_source: new FormControl(''),
        }); 
        this.getPage();
       })
     }else{
      request.status = 2;
       this.valService.saveEnterpriseValuation(request).subscribe(res=>{
        console.log(res);
        this.showForm = false;
        this.enterpriseValuationForm = this.formbuilder.group({
          request_type: new FormControl('', Validators.required),
          purpose: new FormControl('', Validators.required),
          // amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
          original_owner: new FormControl(''),
          customer_seeking_finance: new FormControl(''),
          reference_number: new FormControl(''),
          repo_date: new FormControl(null),
          is_original_invoice: new FormControl('0'),
          invoice_value: new FormControl(null),
          invoice_date: new FormControl(''),
          invoice_document: new FormControl(''),
          category: new FormControl('', Validators.required),
          otherCategory: new FormControl(''),
          brand: new FormControl('', Validators.required),
          otherBrand: new FormControl(''),
          model: new FormControl('', Validators.required),
          otherModel: new FormControl(''),
          asset_name: new FormControl('', Validators.required),
          engine_number: new FormControl(''),
          chassis_number: new FormControl(''),
          is_equipment_registered: new FormControl('0', Validators.required),
          rc_number: new FormControl(''),
          machine_sr_number: new FormControl(''),
          mfg_year: new FormControl(''),
          asset_address: new FormControl('', Validators.required),
          country: new FormControl('', Validators.required),
          state: new FormControl(''),
          city: new FormControl(''),
          pin_code: new FormControl('', Validators.required),
          contact_person: new FormControl('', Validators.required),
          contact_number: new FormControl('', Validators.required),
          distance_to_be_travelled: new FormControl(''),
          enterprise: new FormControl('', Validators.required),
          rc_document: new FormControl(''),
          request_date: new FormControl(this.todayDate),
          created_by: new FormControl(''),
          status: new FormControl(''),
          update_source: new FormControl(''),
        }); 

        this.editId = '';
        this.enterpriseValuationForm.reset();
        this.getPage();
       })
     }
    
   }
 }

 getPurposeList(){
   let request = {
     limit: 999
   }
   this.valService.getPurposeList(request).subscribe((res:any)=>{
    this.purposeList = res.results;
   })
 }

 getEnterpriseCustomer(){
  let request = {
    partnership_type__in : 5,
    status__in : 2,
    limit: 999
  }
  this.valService.getEnterpriseCustomerList(request).subscribe((res:any)=>{
   this.enterpriseCustomerList = res.results;
  })
}

closeForm(){
  this.showForm = false;
  this.editId = '';
  this.isdisabled = false;
  this.enterpriseValuationForm.reset();
  this.enterpriseValuationForm = this.formbuilder.group({
    request_type: new FormControl('', Validators.required),
    purpose: new FormControl('', Validators.required),
    // amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])),
    original_owner: new FormControl(''),
    customer_seeking_finance: new FormControl(''),
    reference_number: new FormControl(''),
    repo_date: new FormControl(null),
    is_original_invoice: new FormControl('0'),
    invoice_value: new FormControl(null),
    invoice_date: new FormControl(''),
    invoice_document: new FormControl(''),
    category: new FormControl('', Validators.required),
    otherCategory: new FormControl(''),
    brand: new FormControl('', Validators.required),
    otherBrand: new FormControl(''),
    model: new FormControl('', Validators.required),
    otherModel: new FormControl(''),
    asset_name: new FormControl('', Validators.required),
    engine_number: new FormControl(''),
    chassis_number: new FormControl(''),
    is_equipment_registered: new FormControl('0', Validators.required),
    rc_number: new FormControl(''),
    machine_sr_number: new FormControl(''),
    mfg_year: new FormControl(''),
    asset_address: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    state: new FormControl(''),
    city: new FormControl(''),
    pin_code: new FormControl('', Validators.required),
    contact_person: new FormControl('', Validators.required),
    contact_number: new FormControl('', Validators.required),
    distance_to_be_travelled: new FormControl(''),
    enterprise: new FormControl('', Validators.required),
    rc_document: new FormControl(''),
    request_date: new FormControl(this.todayDate),
    created_by: new FormControl(''),
    status: new FormControl(''),
    update_source: new FormControl(''),
  }); 
}

edit(row){
  this.editId = row.unique_control_number;
  this.valService.getEnterpriseValById(this.editId).subscribe(
    (row : any) => {
  this.enterpriseValuationForm.patchValue({
  request_type: row?.request_type,
  purpose: row?.purpose,
  original_owner: row?.original_owner,
  customer_seeking_finance: row?.customer_seeking_finance,
  reference_number: row?.reference_number,
  repo_date: row?.valuation_asset?.repo_date,
  is_original_invoice: row?.valuation_asset?.is_original_invoice?"1":"0",
  invoice_value: row?.valuation_asset?.invoice_value,
  invoice_date: row?.valuation_asset?.invoice_date,
  invoice_document: row?.valuation_asset?.invoice_document,
  category: row?.valuation_asset?.category?.display_name,
  other_category: row?.valuation_asset?.other_category,
  brand: row?.valuation_asset?.brand?.display_name,
  other_brand: row?.valuation_asset?.other_brand,
  model: row?.valuation_asset?.model.name,
  other_model: row?.valuation_asset?.other_model,
  asset_name: row?.asset_name,
  engine_number: row?.valuation_asset?.engine_number,
  chassis_number: row?.valuation_asset?.chassis_number,
  is_equipment_registered: row?.valuation_asset?.is_equipment_registered?"1":"0",
  rc_number: row?.valuation_asset?.rc_number,
  machine_sr_number: row?.valuation_asset?.machine_sr_number,
  mfg_year: row?.valuation_asset?.mfg_year,
  asset_address: row?.valuation_asset?.asset_address,
  country:  row?.valuation_asset?.pin_code?.city?.state?.country?.id,
  state:  row?.valuation_asset?.pin_code?.city?.state?.name,
  city:  row?.valuation_asset?.pin_code?.city?.name,
  pin_code:  row?.valuation_asset?.pin_code?.pin_code,
  contact_person: row?.contact_person,
  contact_number: row?.contact_number,
  distance_to_be_travelled: row?.distance_to_be_travelled,
  enterprise:row?.enterprise.id,
  rc_document: row?.valuation_asset?.rc_document,
  request_date: row?.request_date,
  status: row?.status
})
this.viewingOptionsForm = this.fb.group({
  imageData: this.fb.array([]),
  videosData: this.fb.array([this.newVideo()]),
});
this.agencyInfoForm.patchValue({
  report_number: row?.report_number,
  report_created_at: row?.report_created_at,
  hour_meter_reading: row?.valuation_asset?.hour_meter_reading,
  hmr_kmr: row?.valuation_asset?.hmr_kmr,
  valuation_amount: row?.valuation_asset?.valuation_amount,
  inspection_by: row?.inspection_by,
  asset_condition: row?.valuation_asset?.asset_condition,
  gps_installed: row?.gps_installed? "1":"0",
  gps_device_number: row?.gps_device_number,
  imei_number: row?.imei_number,
  distance_to_be_travelled: row?.distance_to_be_travelled,
  overall_general_condition: row?.valuation_asset?.overall_general_condition
})
this.getLocationData();
this.showForm = true;
this.isdisabled = true;
this.categoryId = row?.valuation_asset?.category?.id;
this.brandId = row?.valuation_asset?.brand?.id;
this.modalId = row?.valuation_asset?.model?.id;

this.cognitoID = this.storage.getLocalStorageData('cognitoId', false);
  this.assetId = row?.valuation_asset?.id;
    this.getDetails();
});
}
// delete(row){
//   this.editId = row.unique_control_number;
//   this.valService.deleteEnterpriseValuation(this.editId).subscribe(res=>{
//     console.log(res);
//     this.showForm = false;
//     this.editId = '';
//     this.enterpriseValuationForm.reset();
//     this.getPage();
//    })
// }
moreInfoDialog(ucn : string){
  const dialogRef = this.dialog.open(EnterpriseInfoDialogComponent, {
    data : {
      uniqueNumber : ucn
    }
  });
}

resumeJob(unique_number : any){
  let payload = {
    unique_control_number: unique_number,
  }
  this.valService.postResumeJob(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.notify.success('Job Resumed');
      this.getPage();
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}
cancel(unique_number : any){
  let payload = {
    unique_control_number: unique_number,
  }
  this.valService.postCancelJob(payload).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.notify.success('Job cancelled');
      this.getPage();
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  ); 
}
editData(){
  if(!this.agencyInfoForm.valid){
    return;
  }else{
    if(this.editId){
      this.agencyInfoForm.get('update_source').setValue(false);
      this.agencyInfoForm.get('gps_installed').value == 1? this.agencyInfoForm.get('gps_installed').setValue(true):this.agencyInfoForm.get('gps_installed').setValue(false);
      this.valService.putEnterpriseValuation(this.agencyInfoForm.value,this.editId).subscribe(res=>{
        console.log(res);
        this.showForm = false;
        this.editId = '';
        this.isdisabled = false;
        this.enterpriseValuationForm.reset();
        this.agencyInfoForm.reset();
        this.getPage();
       })
    }
  }
}
getDetails() {
  forkJoin([this.usedEquipmentService.getBasicDetailList(this.assetId), this.usedEquipmentService.getvisualTabs(), this.usedEquipmentService.getVisualsByEquipmentId(this.assetId, { limit: 999 })]).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.assetDetail = res[0];
      this.tabsList = res[1]?.results;
      this.assetVisualData = res[2]?.results;
      if (this.tabsList.length) {
        this.tabsList.forEach((tab: any) => {
          this.imageFormGroup.push(this.newImage(tab));
        });
      }
      if (this.assetVisualData.length) {
        this.setVisualData(this.assetVisualData);
      }
    },
    err => {
      console.log(err?.message);
    }
  )
}

/**
 * Get video data array
 */
get imageFormGroup() { return this.viewingOptionsForm.get("imageData") as FormArray; }
get videosFormGroup() { return this.viewingOptionsForm.get("videosData") as FormArray; }
get sourceVideoFormGroup() { return this.videosFormGroup.controls; }
get sourceImgFormGroup() { return this.imageFormGroup.controls; }
sourceImgFormGroup1(index) { return this.sourceImgFormGroup[index].get('images') as FormArray; }

newImage(tab): FormGroup {
  return this.fb.group({
    id: [tab.id],
    label: [tab.category_name],
    imageUrl: [''],
    images: this.fb.array([]),
  })
}

imagesGroup(data: any): FormGroup {
  return this.fb.group({
    id: [data.id],
    image_category: [data.image_category],
    is_primary: [data.is_primary],
    url: [data.url],
    used_equipment: [data.used_equipment],
    visual_type: [data.visual_type]
  })
}

/**
 * New video for group 
 */
newVideo(data?: any): FormGroup {
  return this.fb.group({
    // videoLink: [data ? data.videoLink : ''],
    id: [data ? data.id : ''],
    videoFile: [''],
    url: [data ? data.url : ''],
  })
}

/**
 * Add new video existing video data
 */
addNewVideo() {
  this.videosFormGroup.push(this.newVideo());
}

setPrimary(event, img) {
  let existingPrimaryImage: any;
  let primaryImage: any;
  if (event.checked) {
    this.sourceImgFormGroup.forEach((element: any) => {
      element.controls.images.controls.forEach((ele: any) => {
        if (ele.get('is_primary').value == true) {
          existingPrimaryImage = ele;
        }
        if (existingPrimaryImage && (existingPrimaryImage.value.url != img.url)) {
          existingPrimaryImage.patchValue({ is_primary: false })
        }
      })
    })
    primaryImage = this.assetVisualData.find(f => { return f.url == img.url });
  }
  let payload = {
    "equipment_id": JSON.parse(this.assetId),
    "equipment_type": 'used',
    "visual_id": primaryImage.id
  }
  this.usedEquipmentService.setPrimaryVisualUsedEquipment(payload).pipe(finalize(() => {   })).subscribe(
    data => {

      this.notificationservice.success("Image successfully set as Primary Image!!");
    }, err => {
      console.log(err);
      this.notificationservice.error(err?.message);
    }

  )
}


saveAssetData(data: any) {
  this.usedEquipmentService.postvisualUsedEquipment(data).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.setVisualData(res);
      this.assetVisualData = [...res];
      this.notificationservice.success('File uploaded successfully!!');
    },
    (err) => {
      this.notificationservice.error(err);
    }
  );
}

async handleUpload(files: any, tab: number) {
  let visualsDataFrom: any = {};
  this.uploadVideoAndImages = [];
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/marketplace/product/" + this.cognitoID + '/' + this.assetId + '/' + tab + '_' + uuidv4() + '_' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        visualsDataFrom = {
          visual_type: 1,
          url: uploaded_file.Location,
          used_equipment: this.assetDetail.id,
          image_category: tab,
          is_primary: false
        }
        for (let i = 0; i < this.imageFormGroup.value.length; ++i) {
          if (this.imageFormGroup.value[i].images.length) {
            this.firstImage = true;
            break;
          }
        }
        if (!this.firstImage && i == 0) {
          visualsDataFrom.is_primary = true;
        }
        this.uploadVideoAndImages.push(visualsDataFrom);
      }
    } else {
      this.notificationservice.error("Invalid Image.")
    }
  }
  if (this.uploadVideoAndImages.length) {
    this.saveAssetData(this.uploadVideoAndImages);
  }
  this.imageInput.nativeElement.value = '';
}

async handleVideoUpload(event: any, count: any) {
  let visualsDataFrom: any = {};
  const file = event.target.files[0];
  if (file.type == 'video/mp4' || file.type == 'video/avi' || file.type == 'video/webm' || file.type == 'video/3gpp' || file.type == 'video/mov' || file.type == 'video/wmv') {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var image_path = environment.bucket_parent_folder + "/marketplace/product/" + this.cognitoID + '/' + this.assetId + '/' + count + uuidv4() + '_' + file?.name.trim();

    var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

    if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
      visualsDataFrom = {
        visual_type: 3,
        url: uploaded_file.Location,
        used_equipment: this.assetDetail.id,
        image_category: 1
      }
      this.saveAssetData([visualsDataFrom]);
    }
  } else {
    this.notificationservice.error("Invalid Video File.")
  }
  this.videoInput.nativeElement.value = '';
}

public dropped(files: NgxFileDropEntry[], tab: number) {
  this.files = files;
  let dropFiles: File[] = [];
  for (const droppedFile of files) {
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        dropFiles.push(file);
        if (dropFiles.length == files.length) {
          this.handleUpload(dropFiles, tab);
        }
      });
    } else {
      const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
    }
  }
}

cropImage(img, tab: number, index: number, imgIndex: number) {
  const dialogRef = this.dialog.open(ImageCropperModalComponent, {
    width: '500px',
    disableClose: true,
    data: {
      image: img.url,
    }
  });

  dialogRef.afterClosed().subscribe(result => {
    if (result.data) {
      this.sourceImgFormGroup1(index).removeAt(imgIndex);
      const imageName = 'temp_' + tab + index + '.png';
      const imageFile = new File([result.data], imageName, { type: 'image/png' });
      this.handleUpload([imageFile], tab);
    }
  });
}

getVideoName(str) {
  return str.slice(str.lastIndexOf("/") + 3);
}

setVisualData(data) {
  let videoObj = data.find(v => { return v.visual_type == 3 });
  let existingVideoObj = this.videosFormGroup.value[this.videosFormGroup.value.length - 1];
  if (videoObj && !existingVideoObj.url) {
    this.videosFormGroup.removeAt(this.videosFormGroup.value.length - 1);
  }
  data.forEach(element => {
    if (element.visual_type == 1) {
      let visualTab: any = this.sourceImgFormGroup.find((fg) => {
        return fg.value.id == element.image_category;
      })
      visualTab.get('images').push(this.imagesGroup(element));
    } else {
      if (element.visual_type == 3) {
        if (element?.url) {
          this.videosFormGroup.push(this.newVideo(element));
        }
      }
    }
  });
}

delete(file, index: number, imgIndex?: any) {
  let imgPath = file?.url.slice(file?.url.indexOf(environment.bucket_parent_folder));
  this.s3.deleteFile(imgPath).subscribe(
    data => {
      this.usedEquipmentService.deleteVisual(file.id).pipe(finalize(() => {   })).subscribe(
        res => {
          this.notificationservice.success('File Successfully deleted!!')
          if (file.visual_type == 1) {
            this.sourceImgFormGroup1(index).removeAt(imgIndex);
          } else {
            this.videosFormGroup.removeAt(index);
            if (index == 0) {
              this.addNewVideo();
            }
          }
        },
        err => {
          console.log(err);
          this.notificationservice.error(err.message);
        }
      )
    },
    err => {
      console.log(err);
      this.notificationservice.error(err);
    }
  )
}
getPurpose(id){
  let filter:any = this.purposeList.filter((e:any) => e.id == id)
  return filter[0]?.title;
}
getRequestType(id : any)
  {
    let returnRequestType;
    switch(id)
    {
      case 1 : 
        returnRequestType = "Valuation";
        break;
      case 2 :
        returnRequestType = "Inspection";
        break;
        case 3 :
        returnRequestType = "GPS Installation";
        break;
        case 4 :
        returnRequestType = "Photographs Only";
        break;
    }
    return returnRequestType;
  }
  updateRcValidation(){
    if(this.enterpriseValuationForm.get('is_equipment_registered')?.value == '1'){
      this.enterpriseValuationForm.get('rc_number')?.setValidators(Validators.required);
      this.enterpriseValuationForm.get('rc_number')?.updateValueAndValidity();
    }else{
      this.enterpriseValuationForm.get('rc_number')?.setValue('');
      this.enterpriseValuationForm.get('rc_number')?.clearValidators();
      this.enterpriseValuationForm.get('rc_number')?.updateValueAndValidity();
    }
  }
  downloadInvoice(url: any) {
    if (url != '') {
      this.sharedService.download(url);
    }
  }
  getDate(){
    if(this.toDate){
      return this.toDate;
    }else{
      return this.todayDate;
    }
  }
  isDisabled(){
    if(!this.enterpriseValuationForm.valid){
      return true;
    }else{
      if(this.stateList?.length == 0){
        return true;
      }
      if(this.validatePincode()){
        return true;
      }
      return false;
    }
  }
  closeDatePicker(elem: MatDatepicker<any>, e) {
    let _d  = e;
          this.yearOfManufacture = _d;
    elem.close();
  }
}