import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterValuationRequestComponent } from './enter-valuation-request.component';

describe('EnterValuationRequestComponent', () => {
  let component: EnterValuationRequestComponent;
  let fixture: ComponentFixture<EnterValuationRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterValuationRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterValuationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
