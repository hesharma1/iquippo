import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ValuationService } from "src/app/services/valuation.service";
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'enterprise-info-dialog',
    templateUrl: './enterprise-info-dialog.component.html',
    styleUrls: ['./enterprise-info-dialog.component.css']
})
export class EnterpriseInfoDialogComponent implements OnInit{
  public viewingOptionsForm!: FormGroup;
    uniqueControlNumber : string = '';
    tableData: any = {};
    invoiceInFavorOf: any;
    invoiceCountry : any;
    invoiceState: any;
    invoiceLocation: any;
    invoiceAddress: any;
    public assetDetail: any;
    assetId;
    purposeList;
  public tabsList: any;
  public assetVisualData: any;
  visualVideoData;
  public statusOptions: Array<any> = [
    { name: 'Request In Draft', value: 1 },
    { name: 'Request Initiated', value: 2 },
    { name: 'Request Submitted', value: 3 },
    { name: 'Inspection In Progress', value: 4 },
    { name: 'Request On Hold', value: 5 },
    { name: 'Request Closed', value: 6 },
    { name: 'Inspection Completed', value: 7 },
    { name: 'Report Submitted', value: 8 },
    { name: 'Invoice Generated', value: 9 },
    { name: 'Invoice Modified', value: 10 },
    { name: 'Invoice Cancelled', value: 11 },
    { name: 'Payment Completed', value: 12 },
    { name: 'Request Cancelled', value: 13 },
    { name: 'Request Modified', value: 14 },
  ];;
    constructor(
        public dialogRef: MatDialogRef<EnterpriseInfoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data: any,
        public valService: ValuationService,private fb: FormBuilder,private usedEquipmentService: UsedEquipmentService
    ){
        this.uniqueControlNumber = data.uniqueNumber;
        this.getValuationDetailById();
        this.viewingOptionsForm = this.fb.group({
          imageData: this.fb.array([]),
          videosData: this.fb.array([this.newVideo()]),
        });
        this.getPurposeList();
    }


/**
 * Get video data array
 */
get imageFormGroup() { return this.viewingOptionsForm.get("imageData") as FormArray; }
get videosFormGroup() { return this.viewingOptionsForm.get("videosData") as FormArray; }
get sourceVideoFormGroup() { return this.videosFormGroup.controls; }
get sourceImgFormGroup() { return this.imageFormGroup.controls; }
sourceImgFormGroup1(index) { return this.sourceImgFormGroup[index].get('images') as FormArray; }

newImage(tab): FormGroup {
  return this.fb.group({
    id: [tab.id],
    label: [tab.category_name],
    imageUrl: [''],
    images: this.fb.array([]),
  })
}

imagesGroup(data: any): FormGroup {
  return this.fb.group({
    id: [data.id],
    image_category: [data.image_category],
    is_primary: [data.is_primary],
    url: [data.url],
    used_equipment: [data.used_equipment],
    visual_type: [data.visual_type]
  })
}

/**
 * New video for group 
 */
newVideo(data?: any): FormGroup {
  return this.fb.group({
    // videoLink: [data ? data.videoLink : ''],
    id: [data ? data.id : ''],
    videoFile: [''],
    url: [data ? data.url : ''],
  })
}

/**
 * Add new video existing video data
 */
addNewVideo() {
  this.videosFormGroup.push(this.newVideo());
}
getDetails() {
  forkJoin([ this.usedEquipmentService.getvisualTabs(), this.usedEquipmentService.getVisualsByEquipmentId(this.assetId, { limit: 999 })]).pipe(finalize(() => {   })).subscribe(
    (res: any) => {
      this.tabsList = res[0]?.results;
      this.assetVisualData = res[1]?.results;
      if (this.tabsList.length) {
        this.tabsList.forEach((tab: any) => {
          this.imageFormGroup.push(this.newImage(tab));
        });
      }
      if (this.assetVisualData.length) {
        this.setVisualData(this.assetVisualData);
        this.visualVideoData = res[1].results.filter(f => { return f.visual_type == 3 });
      }
    },
    err => {
      console.log(err?.message);
    }
  )
}

setVisualData(data) {
  let videoObj = data.find(v => { return v.visual_type == 3 });
  let existingVideoObj = this.videosFormGroup.value[this.videosFormGroup.value.length - 1];
  if (videoObj && !existingVideoObj.url) {
    this.videosFormGroup.removeAt(this.videosFormGroup.value.length - 1);
  }
  data.forEach(element => {
    if (element.visual_type == 1) {
      let visualTab: any = this.sourceImgFormGroup.find((fg) => {
        return fg.value.id == element.image_category;
      })
      visualTab.get('images').push(this.imagesGroup(element));
    } else {
      if (element.visual_type == 3) {
        if (element?.url) {
          this.videosFormGroup.push(this.newVideo(element));
        }

      }
    }
  });
}



    ngOnInit(){
    }

    backClick(){
        this.dialogRef.close();
    }

    getValuationDetailById(){
        this.valService.getEnterpriseValById(this.uniqueControlNumber).subscribe(
        (res : any) => {
            this.tableData = res;
            this.assetId= res?.valuation_asset?.id;
            this.getDetails();
            this.setInvoiceDetail(this.tableData?.valuation_on_behalf , this.tableData);
        },(err : any) => {
            console.log(err);
        })
    }

    setInvoiceDetail(isSelf : any, objectData : any){
      if(isSelf == true){
        this.invoiceInFavorOf = objectData?.valuation_asset?.seller?.first_name + " " + objectData?.valuation_asset?.seller?.last_name;
        this.invoiceCountry = objectData?.valuation_asset?.seller?.pin_code?.city?.state?.country?.name;
        this.invoiceState = objectData?.valuation_asset?.seller?.pin_code?.city?.state?.name;
        this.invoiceLocation = "N/A";
      }
      else{
        this.invoiceInFavorOf = objectData?.contact_person;
        this.invoiceCountry = objectData?.valuation_asset?.pin_code?.city?.state?.country?.name;
        this.invoiceState = objectData?.valuation_asset?.pin_code?.city?.state?.name;
        this.invoiceLocation = objectData?.valuation_asset?.location?.location;
      }
    }

    setRequestDisplayValue(id : any)
    {
      let returnRequestType;
    switch(id)
    {
      case 1 : 
        returnRequestType = "Valuation";
        break;
      case 2 :
        returnRequestType = "Inspection";
        break;
        case 3 :
        returnRequestType = "GPS Installation";
        break;
        case 4 :
        returnRequestType = "Photographs Only";
        break;
    }
    return returnRequestType;
    }

    isSelf(valuationForSelf : any){
      return valuationForSelf;
    }

    setStatusDisplayName(id : any)
    {
      let statusDisplayName;
      for(var i = 0; i < this.statusOptions.length; i++)
      {
        if(this.statusOptions[i].value == id)
        {
          statusDisplayName = this.statusOptions[i].name;
          break;
        }
      }
      return statusDisplayName;
    }
    getPurposeList(){
      let request = {
        limit: 999
      }
      this.valService.getPurposeList(request).subscribe((res:any)=>{
       this.purposeList = res.results;
      })
    }
    getPurpose(id){
      let filter:any = this.purposeList.filter((e:any) => e.id == id)
      return filter[0]?.title;
    }
}