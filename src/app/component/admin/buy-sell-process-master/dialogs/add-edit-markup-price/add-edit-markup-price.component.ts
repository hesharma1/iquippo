import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-edit-markup-price',
  templateUrl: './add-edit-markup-price.component.html',
  styleUrls: ['./add-edit-markup-price.component.css'],
})
export class AddEditMarkUpPriceComponent implements OnInit {
  isEdit = false;
  form: FormGroup;
  usersList: any[] = [];
  element: any = null;
  constructor(public dialogRef: MatDialogRef<AddEditMarkUpPriceComponent>, @Inject(MAT_DIALOG_DATA) data: any, private formbulider: FormBuilder) {
    this.form = this.formbulider.group({
      'Type': ['', Validators.required],
      'User': [''],
      'PricePercent': ['', Validators.required],
      'MobileNo': [''],
      'Id': [''],
    });

    this.isEdit = data?.isEdit;
    this.element = data?.element;
  }

  ngOnInit(): void {
    console.log("element", this.element);
    if (this.element) {
      // this.form.get('Id')?.setValue(this.element.id);
      // this.form.get('Type')?.setValue(this.element.type.toString());
      // this.form.get('User')?.setValue(this.element.user);
      // this.form.get('PricePercent')?.setValue(this.element.pricePerc);
      // this.form.get('MobileNo')?.setValue(this.element.mobileNumber);
      this.form = this.formbulider.group({
        'Type': [this.element.type.toString(), Validators.required],
        'User': [this.element.user],
        'PricePercent': [this.element.pricePerc, Validators.required],
        'MobileNo': [this.element.mobileNumber],
        'Id': [this.element.id],
      });
    }
    else {
      this.form = this.formbulider.group({
        'Type': ['', Validators.required],
        'User': [''],
        'PricePercent': ['', Validators.required],
        'MobileNo': [''],
        'Id': [''],
      });
    }
  }

  okClick() {
    this.dialogRef.close(this.form.value);
  }

  onTypeChange(event) {
    console.log(event.value);
    if (event.value === '1') {
      // this.form.get('User')?.reset();
      // this.form.get('User')?.setValidators(Validators.required);
      // this.form.get('User')?.updateValueAndValidity();
      // this.form.get('MobileNo')?.reset();
      // this.form.get('MobileNo')?.clearValidators();
      // this.form.get('MobileNo')?.updateValueAndValidity();
    } else if (event.value === '2') {
      // this.form.get('User')?.reset();
      // this.form.get('User')?.clearValidators();
      // this.form.get('User')?.updateValueAndValidity();
      // this.form.get('MobileNo')?.reset();
      // this.form.get('MobileNo')?.setValidators(Validators.required);
      // this.form.get('MobileNo')?.updateValueAndValidity();
    } else {
      // this.form.get('User')?.reset();
      // this.form.get('User')?.clearValidators();
      // this.form.get('User')?.updateValueAndValidity();
      // this.form.get('MobileNo')?.reset();
      // this.form.get('MobileNo')?.clearValidators();
      // this.form.get('MobileNo')?.updateValueAndValidity();
    }
  }
}
