import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
  FormArray,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
@Component({
  selector: 'app-add-edit-asset-sale-charge',
  templateUrl: './add-edit-asset-sale-charge.component.html',
  styleUrls: ['./add-edit-asset-sale-charge.component.css'],
})
export class AddEditAssetSaleChargeComponent implements OnInit {
  isEdit = false;
  form: FormGroup;
  assetCategoryList: any[] = [];
  element: any = null;

  constructor(
    public dialogRef: MatDialogRef<AddEditAssetSaleChargeComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private formbulider: FormBuilder,
    public adminService: AdminMasterService
  ) {
    this.form = this.formbulider.group({
      AssetCategory: [null, Validators.required],
      ChargeBasis: [null, Validators.required],
      Type: [null, Validators.required],
      FromDate: [null, Validators.required],
      ToDate: [null, Validators.required],
      Amount: [null, Validators.required],
      User: [null],
      MobileNo: [null],
      Id: [null],
    });

    this.isEdit = data?.isEdit;
    this.element = data?.element;
  }

  ngOnInit(): void {
    this.getAssetCategoryList();
    if (this.element !== null) {
      this.form.get('Id')?.setValue(this.element.id);
      this.form.get('Type')?.setValue(this.element.type.toString());
      this.form.get('User')?.setValue(this.element.user);
      this.form.get('Amount')?.setValue(this.element.amount);
      this.form.get('MobileNo')?.setValue(this.element.mobileNumber);
      this.form
        .get('ChargeBasis')
        ?.setValue(this.element.charge_basis.toString());
      this.form.get('AssetCategory')?.setValue(this.element.category_id);
      this.form.get('FromDate')?.setValue(this.element.from_date);
      this.form.get('ToDate')?.setValue(this.element.to_date);
    }
  }

  okClick() {
    this.dialogRef.close(this.form.value);
  }

  getAssetCategoryList() {
    this.adminService.getCategoryMaster().subscribe((response: any) => {
      this.assetCategoryList = response.results as any[];
    });
  }

  onTypeChange(event) {
    console.log(event.value);
    if (event.value === '1') {
      this.form.get('User')?.reset();
      this.form.get('User')?.setValidators(Validators.required);
      this.form.get('User')?.updateValueAndValidity();
      this.form.get('MobileNo')?.reset();
      this.form.get('MobileNo')?.clearValidators();
      this.form.get('MobileNo')?.updateValueAndValidity();
    } else if (event.value === '2') {
      this.form.get('User')?.reset();
      this.form.get('User')?.clearValidators();
      this.form.get('User')?.updateValueAndValidity();
      this.form.get('MobileNo')?.reset();
      this.form.get('MobileNo')?.setValidators(Validators.required);
      this.form.get('MobileNo')?.updateValueAndValidity();
    } else {
      this.form.get('User')?.reset();
      this.form.get('User')?.clearValidators();
      this.form.get('User')?.updateValueAndValidity();
      this.form.get('MobileNo')?.reset();
      this.form.get('MobileNo')?.clearValidators();
      this.form.get('MobileNo')?.updateValueAndValidity();
    }
  }
}
