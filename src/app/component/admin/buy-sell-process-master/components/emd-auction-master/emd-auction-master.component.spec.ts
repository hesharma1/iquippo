import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmdAuctionMasterComponent } from './emd-auction-master.component';

describe('EmdAuctionMasterComponent', () => {
  let component: EmdAuctionMasterComponent;
  let fixture: ComponentFixture<EmdAuctionMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmdAuctionMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmdAuctionMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
