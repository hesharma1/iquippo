import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { MatTableDataSource } from '@angular/material/table';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuctionService } from 'src/app/services/auction.service';
import { MatOption } from '@angular/material/core';
import { EmdMasterService } from '../emd-master/emd-master.service';
import { ViewAllIdsComponent } from 'src/app/component/shared/view-all-ids/view-all-ids.component';
import { MatDialog } from '@angular/material/dialog';
import { createFalse } from 'typescript';

@Component({
  selector: 'app-emd-auction-master',
  templateUrl: './emd-auction-master.component.html',
  styleUrls: ['./emd-auction-master.component.css']
})

export class EmdAuctionMasterComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('subAllSelected') subAllSelected !: MatOption;
  @ViewChild('lotAllSelected') lotAllSelected!: MatOption;
  public emdChargeForm: any = this.formbuilder.group({
    emdType: new FormControl(''),
    amountType: new FormControl(''),
    amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]+(.[0-9]{0,2})?$/)])),
    auction: new FormControl('', Validators.required),
    subAuction: new FormControl(''),
    lotAuction: new FormControl(''),
    lotCount: new FormControl('')
  });
  public emdDataModel: any;
  selectedSubAuction: any = 0;
  selectedLotAuction: any = 0;
  public isApprovedStatus: number = 2;
  public emdDataSouce: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public editId;
  public searchText!: string;
  public total_count: any;
  public activePage: any = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  //public tblColumns = ["id", "auction__name", "amount", "emd_type", "Actions"];
  public tblColumns = ["id", "auction_id", "subauction_id", "lot_id", "amount", "emd_type", "Actions"];
  public showForm: boolean = false;
  public chargeFilter: any;
  public amountTypeFilter: any;
  public amountTypeOptions: Array<any> = [
    { name: 'Flat', value: 1 },
  ];
  public chargeOptions: Array<any> = [
    { name: 'Auction', value: 1 },
  ];

  isAuctionShow: boolean = false;
  isSubAuctionShow: boolean = false;
  isLotAuctionShow: boolean = false;
  parentAuctionsData: any;
  subAuctionsData: any;
  lotAuctionsData: any;
  auctionId: any;
  subAuctionId: any;
  lotAuctionId: any;
  emdType: any;
  auction: any;
  emd_subauction: any;
  emd_lots: any;
  isAuctionId: boolean = false;
  isAuctionLot: boolean = false;
  lotCount: any;
  isShowLotCount: boolean = true;
  isEdit: boolean = false;
  public listingData = [];
  constructor(private formbuilder: FormBuilder, private dialog: MatDialog, private notify: NotificationService,
    public productService: ProductlistService, public spinner: NgxSpinnerService, private auctionService: AuctionService) { }

  ngOnInit(): void {
    this.isAuctionShow = true;
    this.getEmdList();
    let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__isnull: true, ends_at__gt: (new Date()).toISOString() };
    this.getAuctionList(payload);
  }

  getAuctionList(payload) {
    this.auctionService.getAuctionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.parentAuctionsData = res.results;
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getSubAuctionListByAuctionId(payload: any) {
    this.auctionService.getAuctionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.subAuctionsData = [];
          this.lotAuctionsData = [];
          this.subAuctionsData = res.results;
          if (this.subAuctionsData && this.subAuctionsData.length <= 0) {
            this.selectedSubAuction = 0;
            this.isAuctionLot = true;
            let params = { page: 1, limit: -1, ordering: '-id', auction__id__in: payload.parent_auction__id__in };
            this.getLotAuctionListBySubAuctionId(params);
          }
          else {
            if (!this.emdChargeForm.controls.subAuction.value) {
              this.selectedSubAuction = undefined;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getLotAuctionListBySubAuctionId(payload) {
    this.auctionService.getAuctionLotList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.lotAuctionsData = [];
          this.lotAuctionsData = res.results;
          if (this.lotAuctionsData && this.lotAuctionsData.length > 0) {
            if (!this.emdChargeForm.controls.lotAuction.value) {
              this.selectedLotAuction = undefined;
            }
          }
          else {
            this.selectedLotAuction = 0;
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  selectAuctionId(auctionId) {
    this.auctionId = auctionId;
    if (auctionId) {
      this.isSubAuctionShow = true;
      let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__in: auctionId };
      //  if (!this.editId) {
      this.selectedSubAuction = 0;
      this.selectedLotAuction = 0;
      //}
      this.getSubAuctionListByAuctionId(payload);
    }
    else {
      this.isSubAuctionShow = false;
      this.isLotAuctionShow = false;
    }
  }

  selectSubAuction(subAuctionId) {
    this.subAuctionId = subAuctionId.filter(x => x != 0 && x != undefined);
    this.subAuctionId = this.subAuctionId.join(",");
    if (this.subAuctionId && this.emdChargeForm.controls.subAuction.value.length == 1) {
      let payload = { page: 1, limit: -1, ordering: '-id', auction__id__in: this.subAuctionId };
      this.getLotAuctionListBySubAuctionId(payload);
      this.isShowLotCount = true;
    }
    else {
      this.lotAuctionsData = [];
      this.lotCount = '';
      this.isShowLotCount = false;
    }
  }

  checkSubAuctionPerOne(all) {
    this.selectSubAuction(this.emdChargeForm.controls.subAuction.value);
    if (this.emdChargeForm.controls.subAuction.value.length <= 0) {
      this.isLotAuctionShow = false;
      this.isShowLotCount = true;
    }
    else {
      this.isLotAuctionShow = true;
    }
    if (this.subAllSelected.selected) {
      this.subAllSelected.deselect();
    }
    if (this.emdChargeForm.controls.subAuction.value.length == this.subAuctionsData.length) {
      this.subAllSelected.select();
      this.isShowLotCount = false;
    }
  }

  toggleSubAllSelection() {
    if (this.subAllSelected.selected) {
      this.isLotAuctionShow = true;
      this.emdChargeForm.controls.subAuction.patchValue([...this.subAuctionsData ? this.subAuctionsData.map(item => item.id) : ""]);
      this.subAllSelected.select();
      if (this.emdChargeForm.controls.subAuction.value != 0 && this.lotAuctionId && this.lotAuctionId.length > 0) {
        this.isShowLotCount = false;
      }
      else {
        this.isShowLotCount = true;
      }
    } else {
      this.isLotAuctionShow = false;
      this.emdChargeForm.controls.subAuction.patchValue([]);
      if (this.emdChargeForm.controls.subAuction.value != 0 && this.lotAuctionId && this.lotAuctionId.length > 0) {
        this.isShowLotCount = false;
      }
      else {
        this.isShowLotCount = true;
      }
    }
  }

  checkLotAuctionPerOne(all) {
    this.lotAuctionId = this.emdChargeForm.controls.lotAuction.value;
    if (this.lotAuctionId && this.lotAuctionId.length > 0) {
      this.isShowLotCount = false;
    }
    else {
      this.isShowLotCount = true;
    }
    if (this.lotAllSelected.selected) {
      this.lotAllSelected.deselect();
    }
    if (this.emdChargeForm.controls.lotAuction.value.length == this.lotAuctionsData.length) {
      this.lotAllSelected.select();
      this.isShowLotCount = false;
    }
  }

  toggleLotAllSelection() {
    if (this.lotAllSelected.selected) {
      this.emdChargeForm.controls.lotAuction.patchValue([... this.lotAuctionsData ? this.lotAuctionsData.map(item => item.id) : ""]);
      this.lotAllSelected.select();
      if (this.emdChargeForm.controls.lotAuction.value != 0) {
        this.isShowLotCount = false;
      }
      else {
        this.isShowLotCount = true;
      }
    } else {
      this.emdChargeForm.controls.lotAuction.patchValue([]);
      this.isShowLotCount = true;
    }
  }

  action(actionType: string, id: number) {
    if (actionType === 'Edit') {
      this.auctionService.getAuctionEmdListById(id).subscribe(
        res => {
          if (res != null) {
            this.isEdit = true;
            this.emdDataModel = [];
            this.emdDataModel = res as any;
            this.showForm = true;
            this.isAuctionShow = true;
            this.isSubAuctionShow = true;
            this.isLotAuctionShow = true;
            this.selectedSubAuction = 0;
            this.selectedLotAuction = 0;
            if (this.emdDataModel.emd_type == 'AU' || this.emdDataModel.emd_type == 'LC') {
              // if parent_auction == null => its self a parent auction 
              if (this.emdDataModel.auction.parent_auction == null) {
                this.auctionId = this.emdDataModel.auction.id;
                this.isLotAuctionShow = false;
                this.isSubAuctionShow = false;
              }
              else {
                this.auctionId = this.emdDataModel.auction.parent_auction;
                let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__in: this.auctionId };
                this.getSubAuctionListByAuctionId(payload);
                this.lotAuctionsData = [];
                this.isLotAuctionShow = true;
                this.isSubAuctionShow = true;
              }
            }
            else if (this.emdDataModel.emd_type == 'SW') {
              if (this.emdDataModel.emd_subauctions && this.emdDataModel.emd_subauctions.length > 0) {
                this.auctionId = this.emdDataModel.auction.id;
                this.isLotAuctionShow = false;
                let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__in: this.emdDataModel.auction.id };
                this.getSubAuctionListByAuctionId(payload);
              }
            }
            else if (this.emdDataModel.emd_type == 'LW') {
              //for get sub_auction data if parent_auction == null then use auction.id=>
              if (this.emdDataModel.auction.parent_auction == null) {
                this.auctionId = this.emdDataModel.auction.id;
                let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__in: this.emdDataModel.auction.id };
                this.getSubAuctionListByAuctionId(payload);
              }
              else {
                //for get sub_auction data if parent_auction != null then use auction.parent_auction 
                this.auctionId = this.emdDataModel.auction.parent_auction;
                let payload = { page: 1, limit: -1, ordering: '-id', parent_auction__id__in: this.emdDataModel.auction.parent_auction };
                this.getSubAuctionListByAuctionId(payload);
              }
              setTimeout(() => {
                if (this.emdDataModel.emd_lots && this.emdDataModel.emd_lots.length > 0) {
                  //for get emd_lots data use emd_lots.lot.auction
                  this.subAuctionId = this.emdDataModel.emd_lots[0].lot.auction;
                  let payload = { page: 1, limit: -1, ordering: '-id', auction__id__in: this.emdDataModel.emd_lots[0].lot.auction };
                  this.getLotAuctionListBySubAuctionId(payload);
                }
              }, 200);
            }

            setTimeout(() => {
              let emd_subauction = this.emdDataModel.emd_subauctions && this.emdDataModel.emd_subauctions.length > 0 ? this.emdDataModel.emd_subauctions.map(x => x.auction.id) : "";
              let emd_lots = this.emdDataModel.emd_lots.map(x => x.lot.id);
              this.emdChargeForm.setValue({
                emdType: 1,
                auction: this.auctionId ? this.auctionId : "",
                subAuction: this.emdDataModel.emd_subauctions && this.emdDataModel.emd_subauctions.length > 0 ? emd_subauction :
                  this.emdDataModel.emd_type == 'AU' && this.emdDataModel.auction.parent_auction != null ? [this.emdDataModel.auction.id]
                    : this.emdDataModel.emd_lots && this.emdDataModel.emd_lots.length > 0 ? [this.emdDataModel.emd_lots[0].lot.auction] : 0,
                lotAuction: this.emdDataModel.emd_lots && this.emdDataModel.emd_lots.length > 0 ? emd_lots :
                  this.emdDataModel.emd_type == 'AU' && this.emdDataModel.auction.parent_auction != null ? 0 : 0,
                amountType: 1,
                amount: this.emdDataModel.amount,
                lotCount: this.emdDataModel.lot_size
              });
            }, 500);
            this.editId = this.emdDataModel.id;
            this.emdType = this.emdDataModel.emd_type;
          }
        });
    }
    else if (actionType === 'Delete') {
      this.auctionService.deleteAuctionEmd(id).subscribe(
        res => {
          if (res == null) {
            this.notify.success('Data Deleted Successfully.');
            this.getEmdList();
            this.editId = '';
          }
        });
    }
  }

  /**search list */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length >= 3) {
      this.activePage = 1;
      this.getEmdList();
    }
  }

  getEmdList() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.chargeFilter) {
      payload['type__in'] = this.chargeFilter;
    }
    if (this.amountTypeFilter) {
      payload['amount_type__in'] = this.amountTypeFilter;
    }
    this.auctionService.getAuctionEmdList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.total_count = res.count;
        if (window.innerWidth > 768) {
          this.emdDataSouce.data = res.results;
        } else {
          this.scrolled = false;
          if (this.activePage == 1) {
            this.emdDataSouce.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.emdDataSouce.data = this.listingData;
          } else {
            this.listingData = this.listingData.concat(res.results);
            this.emdDataSouce.data = this.listingData;
          }
        }
      },
      err => {
      }
    )
  }

  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getEmdList();
  }

  /**sort list  */
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getEmdList();
  }

  chargeStatusFilter(val) {
    this.chargeFilter = val;
    this.activePage = 1;
    this.getEmdList();
  }

  amountStatusFilter(val) {
    this.amountTypeFilter = val;
    this.activePage = 1;
    this.getEmdList();
  }

  export() {
    let payload = {
      limit: -1,
      page: 1,
      ordering: "-id"
    }
    this.auctionService.getAuctionEmdList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          let subAuction = r?.emd_subauctions?.filter(x => x.auction.auction_engine_auction_id).map(x => x.auction.auction_engine_auction_id).join(", ");
          let emd_lots = r?.emd_lots?.filter(x => x.lot.lot_number).map(x => x.lot.lot_number).join(", ");
          return {
            "ID": r?.id,
            "Auction Id": r?.auction?.auction_engine_auction_id,
            "Subauction Id": subAuction,
            "Lot Id": emd_lots,
            "Amount": r?.amount,
            "Applicable on": r?.emd_type == 'AU' ? 'Auction' : r?.emd_type == 'SW' ? 'Subauction' :
              r?.emd_type == 'LC' ? 'Lot Count'
                : 'Lot'
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Emd Master List");
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  closeForm() {
    this.showForm = false;
    this.editId = '';
    this.isEdit = false;
    this.emdChargeForm.reset();
  }

  postAuctionData() {
    this.auction = "";
    this.emd_subauction = [];
    this.emd_lots = [];
    let subAuctionValue = this.emdChargeForm.controls.subAuction.value ? this.emdChargeForm.controls.subAuction.value.filter(x => x != 0 && x != undefined) : [];
    let lotAuctionValue = this.emdChargeForm.controls.lotAuction.value ? this.emdChargeForm.controls.lotAuction.value.filter(x => x != 0 && x != undefined) : [];
    let parentAuctionValue = this.emdChargeForm.controls.auction.value ? this.emdChargeForm.controls.auction.value : "";

    this.emdChargeForm.markAllAsTouched();
    if (this.emdChargeForm.valid) {
      const formData = this.emdChargeForm.value;
      this.emdDataModel = new Object();
      if (!this.editId) {
        this.emdDataModel.emd_lots = lotAuctionValue ? lotAuctionValue : [];
        this.emdDataModel.emd_subauctions = subAuctionValue ? subAuctionValue : [];
        this.emdDataModel.auction = parentAuctionValue ? parentAuctionValue : '';
        this.emdDataModel.amount = formData.amount;
        this.emdDataModel.lot_count = this.lotCount;
        this.auctionService.postAuctionEmd(this.emdDataModel).subscribe(
          res => {
            if (res != null) {
              this.emdDataModel = res;;
              this.emdChargeForm.reset();
              this.notify.success('Data Saved Successfully.');
              this.activePage = 1;
              this.pageSize = 10;
              this.listingData = [];
              this.getEmdList();
              this.editId = '';
              this.auctionId = "";
              this.subAuctionId = "";
              this.isLotAuctionShow = false;
              this.isSubAuctionShow = false;
              this.subAuctionsData = [];
              this.lotAuctionsData = [];
            }
          });
      }
      else {
        this.emdDataModel.emd_type = this.emdType;
        this.emdDataModel.emd_lots = lotAuctionValue ? lotAuctionValue : [];
        this.emdDataModel.emd_subauctions = subAuctionValue ? subAuctionValue : [];
        this.emdDataModel.auction = parentAuctionValue ? parentAuctionValue : '';
        this.emdDataModel.amount = formData.amount;
        this.emdDataModel.lot_count = this.lotCount;
        this.auctionService.updateAuctionEmd(this.emdDataModel, this.editId).subscribe(
          res => {
            if (res != null) {
              this.emdDataModel = res;
              this.emdChargeForm.reset();
              this.notify.success('Data Updated Successfully.');
              this.getEmdList();
              this.editId = '';
              this.isEdit = false;
              this.auctionId = "";
              this.subAuctionId = "";
              this.isLotAuctionShow = false;
              this.isSubAuctionShow = false;
              this.subAuctionsData = [];
              this.lotAuctionsData = [];
            }
          });
      }
    }
  }

  /*postAuctionData() {
    this.auction = "";
    this.emd_subauction = [];
    this.emd_lots = [];
    let subAuctionValue = this.emdChargeForm.controls.subAuction.value ? this.emdChargeForm.controls.subAuction.value.filter(x => x != 0 && x != undefined) : [];
    let lotAuctionValue = this.emdChargeForm.controls.lotAuction.value ? this.emdChargeForm.controls.lotAuction.value.filter(x => x != 0 && x != undefined) : [];
    let parentAuctionValue = this.emdChargeForm.controls.auction.value ? this.emdChargeForm.controls.auction.value : "";
    if (parentAuctionValue && subAuctionValue && subAuctionValue.length <= 0 && lotAuctionValue && lotAuctionValue.length <= 0) {
      console.log("0");
      this.emdType = 'AU';
      this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
    }
    // else if (parentAuctionValue && subAuctionValue && subAuctionValue.length <= 0 && lotAuctionValue && lotAuctionValue.length <= 0) {
    //   console.log("1");
    //   this.emdType = 'AU';
    //   this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
    // }
    else if (parentAuctionValue && (subAuctionValue && subAuctionValue.length <= 0 ||
      subAuctionValue && subAuctionValue.length == this.subAuctionsData.length) &&
      (lotAuctionValue && lotAuctionValue.length <= 0 ||
        lotAuctionValue && lotAuctionValue.length == this.lotAuctionsData.length)) {
      console.log("1");
      this.emdType = 'AU';
      this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
    }
    // else if (parentAuctionValue && (subAuctionValue && subAuctionValue.length == this.subAuctionsData.length)
    //   && !lotAuctionValue) {
    //   console.log("3");
    //   this.emdType = 'AU';
    //   this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
    // }
    else if (parentAuctionValue && (lotAuctionValue && lotAuctionValue.length <= 0 ||
      lotAuctionValue && lotAuctionValue.length == this.lotAuctionsData.length)
      && (subAuctionValue && subAuctionValue.length == 1 && subAuctionValue.length != this.subAuctionsData.length)) {
      console.log("2");
      this.emdType = 'AU';
      this.auction = this.subAuctionId ? this.subAuctionId : subAuctionValue.join(",") ? subAuctionValue.join(",") : "";
    }
    // else if (parentAuctionValue && !lotAuctionValue
    //   && (subAuctionValue && subAuctionValue.length == 1 && subAuctionValue.length != this.subAuctionsData.length)) {
    //   console.log("5");
    //   this.emdType = 'AU';
    //   this.auction = this.subAuctionId ? this.subAuctionId : subAuctionValue.join(",") ? subAuctionValue.join(",") : "";
    //   this.isAuctionId = false;
    // }
    else if (this.emdDataModel && this.emdDataModel.emd_type == 'AU' && this.emdDataModel.auction.parent_auction != null
      && (subAuctionValue && subAuctionValue.length > 0 && subAuctionValue.length != this.subAuctionsData.length) && lotAuctionValue && lotAuctionValue.length <= 0) {
      console.log("3");
      this.emdType = 'AU';
      this.auction = this.subAuctionId ? this.subAuctionId : subAuctionValue.join(",") ? subAuctionValue.join(",") : "";
      this.isAuctionId = false;
    }
    else if (parentAuctionValue && (subAuctionValue && subAuctionValue.length == this.subAuctionsData.length)
      && (lotAuctionValue && lotAuctionValue.length == this.lotAuctionsData.length)) {
      console.log("4");
      this.emdType = 'AU';
      this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
      this.isAuctionId = true;
    }
    else if (parentAuctionValue && subAuctionValue && subAuctionValue.length > 1 && (subAuctionValue.length != this.subAuctionsData.length)
      && lotAuctionValue && lotAuctionValue.length <= 0 || lotAuctionValue.length == this.lotAuctionsData.length) {
      console.log("5");
      this.emdType = 'SW';
      this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
      this.emd_subauction = subAuctionValue;
    }
    // else if (parentAuctionValue && (subAuctionValue && subAuctionValue.length > 1 && subAuctionValue.length != this.subAuctionsData.length)
    //   && (lotAuctionValue.length == this.lotAuctionsData.length)) {
    //   console.log("8");
    //   this.emdType = 'SW';
    //   this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : '';
    //   this.emd_subauction = subAuctionValue;
    // }
    else if (parentAuctionValue && subAuctionValue && subAuctionValue.length == 1 &&
      (lotAuctionValue && lotAuctionValue.length >= 1 && lotAuctionValue.length != this.lotAuctionsData.length)) {
      console.log("6");
      this.emdType = 'LW';
      this.auction = this.subAuctionId ? this.subAuctionId : subAuctionValue ? subAuctionValue : "";
      this.emd_subauction = [];
      this.emd_lots = lotAuctionValue;
    }
    else if (parentAuctionValue && (lotAuctionValue && lotAuctionValue.length >= 1 &&
      lotAuctionValue.length != this.lotAuctionsData.length)) {
      console.log("7");
      this.emdType = 'LW';
      this.auction = this.auctionId ? this.auctionId : parentAuctionValue ? parentAuctionValue : "";
      this.emd_subauction = [];
      this.emd_lots = lotAuctionValue;
    }

    this.emdChargeForm.markAllAsTouched();
    if (this.emdChargeForm.valid) {
      const formData = this.emdChargeForm.value;
      this.emdDataModel = new Object();
      this.emdDataModel.emd_lots = this.emd_lots ? this.emd_lots : [];
      this.emdDataModel.emd_subauctions = this.emd_subauction ? this.emd_subauction : [];
      this.emdDataModel.auction = this.auction ? this.auction : '';
      this.emdDataModel.emd_type = this.lotCount ? 'LC' : this.emdType;
      this.emdDataModel.amount_type = formData.amountType;
      this.emdDataModel.amount = formData.amount;
      this.emdDataModel.lot_size = this.lotCount ? this.lotCount : '';
      if (!this.editId) {
        let params = {
          emd_type: this.lotCount ? 'LC' : this.emdType,
          auction_id: this.auction ? this.auction : '',
          emd_subauctions: this.emd_subauction ? this.emd_subauction.join(",") : '',
          emd_lots: this.emd_lots ? this.emd_lots.join(",") : ''
        };
        console.log("exist emd params", params);
        this.auctionService.emdExistCheck(params).pipe(finalize(() => { })).subscribe(
          (res: any) => {
            console.log("exist emd", res);
            if (res.is_already_exists) {
              this.notify.warn(res.message)
            }
            else {
              this.auctionService.postAuctionEmd(this.emdDataModel).subscribe(
                res => {
                  if (res != null) {
                    this.emdDataModel = res;;
                    this.emdChargeForm.reset();
                    this.notify.success('Data Saved Successfully.');
                    this.getEmdList();
                    this.editId = '';
                    this.auctionId = "";
                    this.subAuctionId = "";
                    this.isLotAuctionShow = false;
                    this.isSubAuctionShow = false;
                    this.subAuctionsData = [];
                    this.lotAuctionsData = [];
                  }
                });
            }
          }
        )
      }
      else {
        this.auctionService.updateAuctionEmd(this.emdDataModel, this.editId).subscribe(
          res => {
            if (res != null) {
              this.emdDataModel = res;
              this.emdChargeForm.reset();
              this.notify.success('Data Updated Successfully.');
              this.getEmdList();
              this.editId = '';
              this.auctionId = "";
              this.subAuctionId = "";
              this.isLotAuctionShow = false;
              this.isSubAuctionShow = false;
              this.subAuctionsData = [];
              this.lotAuctionsData = [];
            }
          });
      }
    }
  }*/

  viewAllIds(data, type) {
    if (type == 'subids') {
      const dialogRef = this.dialog.open(ViewAllIdsComponent, {
        width: '500px',
        disableClose: true,
        data: {
          isSubAuctionIds: true,
          ids: data,
        }
      });
    }
    else {
      const dialogRef = this.dialog.open(ViewAllIdsComponent, {
        width: '500px',
        disableClose: true,
        data: {
          isLotIds: true,
          ids: data
        }
      });
    }
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.emdDataSouce.data.length) && (this.emdDataSouce.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if (!this.scrolled) {
            this.scrolled = true;
            this.activePage++;
            this.getEmdList();
          }

        }
      }
    }
  }

}
