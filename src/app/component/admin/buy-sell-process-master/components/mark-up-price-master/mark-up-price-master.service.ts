import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { HttpClientService } from 'src/app/utility/http-client.service';

@Injectable({
  providedIn: 'root',
})
export class MarkUpPriceMasterService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService
  ) {}

  updateMarkUpPrice(requestModel: any, id: any) {
    return this.httpClientService.HttpPutRequestCommon(
      requestModel,
      `${this.apiPath.markUpPriceMaster}${id}`,
      environment.bannersBaseUrl
    );
  }

  createMarkUpPrice(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(
      requestModel,
      this.apiPath.markUpPriceMaster,
      environment.bannersBaseUrl
    );
  }

  deleteMarkUpPrice(id: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.markUpPriceMaster}${id}/`,
      environment.bannersBaseUrl
    );
  }
}
