import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';

@Injectable({
  providedIn: 'root',
})
export class MarkUpPriceMasterDataService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService,
    private httpClient: HttpClient
  ) {}

  getMarkUpPriceList(
    pageNumber = 0,
    pageSize = 3,
    sortOrder = '-id',
    filter = ''
  ) {
    return this.httpClient
      .get(`${environment.bannersBaseUrl + this.apiPath.markUpPriceMaster}`, {
        params: new HttpParams()
          .set('page', pageNumber.toString())
          .set('limit', pageSize.toString())
          .set('ordering', sortOrder)
          .set('search', filter),
      })
      .pipe(map((res: any) => res));
  }
}
