import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';

import { catchError, finalize } from 'rxjs/operators';
import { MarkUpPriceMasterDataService } from './mark-up-price-master-data.service';

export class MarkUpPriceMasterDataSource implements DataSource<any> {
  private listSubject = new BehaviorSubject<any[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(
    private markUpPriceMasterDataService: MarkUpPriceMasterDataService
  ) {}

  loadMasterData(
    pageIndex: number,
    pageSize: number,
    sortDirection: string,
    filter: string
  ) {
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.markUpPriceMasterDataService
        .getMarkUpPriceList(pageIndex, pageSize, sortDirection, filter)
        .subscribe((list) => {
          this.listSubject.next(list);
          resolve(list);
          // this.listSubject.next(list.results);
        });
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    console.log('Connecting data source');
    return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listSubject.complete();
    this.loadingSubject.complete();
  }
}
