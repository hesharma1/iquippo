import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { merge, fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay,
} from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { NewEquipmentMasterService } from 'src/app/services/new-equipment-master.service';

import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { MarkUpPriceMasterDataSource } from './mark-up-price-master.datasource';
import { ConfirmDialogComponent } from '../../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';

import { MarkUpPriceMasterDataService } from './mark-up-price-master-data.service';
import { MarkUpPriceMasterService } from './mark-up-price-master.service';
import { AddEditMarkUpPriceComponent } from '../../dialogs/add-edit-markup-price/add-edit-markup-price.component';
import { MarkUpPriceMasterData, sellType } from '../sale-master/sale-master-prop.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { SaleMasterService } from '../sale-master/sale-master.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';

@Component({
  selector: 'app-mark-up-price-master',
  templateUrl: './mark-up-price-master.component.html',
  styleUrls: ['./mark-up-price-master.component.css'],
})
export class MarkUpPriceMasterComponent implements OnInit, AfterViewInit {
  dataSource1 = new MatTableDataSource<[]>();
  dataSource: any = [];
  displayedColumns = ['id', 'type', 'user__first_name', 'price_in_per', 'actions'];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  public ordering: string = '-id';
  isEdit = false;
  form: FormGroup;
  dataList: any;
  public UsersJData: any;
  isSellerList: boolean = false;
  userIdHiddenFlag = false;
  partnerHiddenFlag = false;
  mobileHiddenFlag = false;
  listingData = [];
  sellModelData: MarkUpPriceMasterData = new MarkUpPriceMasterData();
  UserRoleTypeArray: Array<any> = [
    { typeName: 'Enterprise', value: 1 },
    { typeName: 'Seller', value: 2 },
    { typeName: 'Default', value: 3 },
  ];
  total_count = 0;
  constructor(
    private markUpPriceMasterDataService: MarkUpPriceMasterDataService,
    private markUpPriceMasterService: MarkUpPriceMasterService, private partnerDealerRegistrationService: PartnerDealerRegistrationService,
    private dialog: MatDialog, private saleMstrService: SaleMasterService,
    private fb: FormBuilder, private notify: NotificationService,
    public adminService: AdminMasterService
  ) {
    this.form = this.fb.group({
      'userRole': ['', Validators.required],
      'User': [''],
      'PricePercent': ['', [Validators.required, Validators.maxLength(100)]],
      'mobile': [''],
      'Id': [''],
    });
  }

  async ngOnInit() {
    this.dataSource = new MarkUpPriceMasterDataSource(
      this.markUpPriceMasterDataService
    );
    this.dataList = await this.dataSource.loadMasterData(1, 3, '-id', '');
    this.total_count = this.dataList.count;
    this.dataSource1 = this.dataList.results?.map((value: any) =>
      this.getProperties(value)
    );
  }

  getProperties(element: any) {
    return {
      id: element.id,
      type: element.type,
      user: element.user,
      pricePerc: element.price_in_per,
      mobileNumber: element.mobile_number,
    };
  }


  ngAfterViewInit(): void {
    //this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    // fromEvent(this.input.nativeElement, 'keyup')
    //   .pipe(
    //     debounceTime(150),
    //     distinctUntilChanged(),
    //     tap(async () => {
    //       this.paginator.pageIndex = 0;
    //       this.dataList = await this.dataSource.loadMasterData(1, 3, '-id', '');
    //       this.dataSource1 = this.dataList.results?.map((value: any) =>
    //         this.getProperties(value)
    //       );
    //     })
    //   )
    //   .subscribe();

    merge(this.paginator.page)
      .pipe(
        tap(async () => {
          this.getData()
        })
      )
      .subscribe();
  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataList.results.length) && (this.dataList.results.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.paginator.pageIndex++;
       this.getData();
        }
      }
    }
  }
}
  async getData() {
    let pageindex = this.paginator.pageIndex + 1;
    // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      this.dataList = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
      }else{
        this.scrolled = false;
        if(pageindex == 1){
          this.dataList = [];
          this.listingData = [];
          let results = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
          this.dataList = results;
          this.listingData = results.results;
          this.dataList.results = this.listingData;
        }else{
          let results = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '')
          this.dataList.results = this.dataList.results.concat(results.results);
          //this.dataList.results = this.listingData;
        }
      }
    //this.dataList = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
    this.dataSource1 = this.dataList.results?.map((value: any) =>
      this.getProperties(value)
    );
  }

  async search(event, value: string) {

    if (event && event.target.value.length <= 0) {
      let pageindex = this.paginator.pageIndex + 1;
      //let groupQueryParam = 'page=' + pageindex + '&limit=' + this.groupPageSize + '&ordering=-id&search='
      this.dataList = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
      this.dataSource1 = this.dataList.results?.map((value: any) =>
        this.getProperties(value)
      );
    }
    else if (event && event.which == 13) {
      value = value.trim();
      value = value.toLowerCase();
      this.dataList = await this.dataSource.loadMasterData(1, '9999', '-id', value);
      this.dataSource1 = this.dataList.results?.map((value: any) =>
        this.getProperties(value)
      );
    }

  }


  GetUsers(queryParam): void {
    this.UsersJData = [];
    this.saleMstrService.getSellerList(queryParam)
      .subscribe(async (resp: any) => {
        this.UsersJData = resp.results;
        // this.sellModelData.user_name = this.UsersJData.filter(x => x.cognito_id == this.sellModelData.user.cognito_id).map(x => x.cognito_id).join(",")
      },
        error => {
          this.notify.error(
            error
          );
        });
  }

  getPartnerContactList(firstName) {
    this.partnerDealerRegistrationService.searchPartnerContact('', firstName).subscribe(
      (res: any) => {
        this.UsersJData = res.results;
        //this.sellModelData.user_name = this.UsersJData.filter(x => x.user.cognito_id == this.sellModelData.user.cognito_id).map(x => x.user.cognito_id).join(",")
      },
      (err) => {
        console.error(err);
      }
    );
  }

  searchUser(e, type) {
    let name = e.target.value;
    if (type == 'user') {
      if (name.length <= 0) {
        this.sellModelData.mobile_number = '';
      }
      else if (name.length >= 3) {
        this.isSellerList = true;
        let queryParams =
          'first_name__icontains=' +
          e.target.value +
          '&groups__name__iexact=Customer' +
          '&limit=999';
        this.GetUsers(queryParams);
      }
    }
    else {
      if (name.length >= 3) {
        this.isSellerList = false;
        this.getPartnerContactList(e.target.value);
      }
    }
  }

  UserRoleChange(userRoleDDLValue) {
    if (userRoleDDLValue === "Enterprise") {
      this.sellModelData.type = sellType.Enterprise.valueOf();
      this.userIdHiddenFlag = false;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = true;
      this.isSellerList = false;
      this.sellModelData.mobile_number = "";
      this.UsersJData = [];
      this.sellModelData.user_name = "";
      //this.getPartnerContactList();
      this.form.get('User')?.setValidators(Validators.required);
      this.form.get('User')?.updateValueAndValidity();
      this.form.get('mobile')?.clearValidators();
      this.form.get('mobile')?.updateValueAndValidity();
    }
    else if (userRoleDDLValue === "Seller") {
      this.form.get('User')?.setValidators(Validators.required);
      this.form.get('User')?.updateValueAndValidity();
      this.form.get('mobile')?.setValidators(Validators.required);
      this.form.get('mobile')?.updateValueAndValidity();

      this.sellModelData.type = sellType.Seller.valueOf();
      this.userIdHiddenFlag = true;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = false;
      this.isSellerList = true;
      this.UsersJData = [];
      this.sellModelData.user_name = "";
      // let queryParam = 'limit=999&ordering=-id&groups__name__iexact=customer';
      // this.GetUsers(queryParam);
    }
    else if (userRoleDDLValue == "Default") {
      this.sellModelData.type = sellType.Default.valueOf();
      this.userIdHiddenFlag = false;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = false;
      this.isSellerList = false;
      this.UsersJData = [];
      this.sellModelData.mobile_number = "";
      this.sellModelData.user_name = "";
      this.sellModelData.user.cognito_id = "";
      this.form.get('mobile')?.clearValidators();
      this.form.get('mobile')?.updateValueAndValidity();
      this.form.get('User')?.clearValidators();
      this.form.get('User')?.updateValueAndValidity();

    }
  }

  async onSortColumn(event) {
    let sorting = (event.direction == "asc") ? event.active : ("-" + event.active);
    // this.ordering = 'page=' + this.paginator.pageIndex + 1 + '&limit=' + this.paginator.pageSize + '&ordering=' + sorting;
    let pageIndex = this.paginator.pageIndex + 1;
    this.dataList = await this.dataSource.loadMasterData(pageIndex, this.paginator.pageSize, sorting, '');
    this.dataSource1 = this.dataList.results?.map((value: any) =>
      this.getProperties(value)
    );
  }

  async exportToExcel() {
    const specList = await this.dataSource.loadMasterData(1, 99999, '-id', '');
    const exportArray = specList.results.map((value: any) => {
      return {
        type: value.type === 1 ? 'Enterprise' : value.type === 2 ? 'Seller' : 'Default',
        user: value.user,
        pricePerc: value.price_in_per,
      };
    });
    ExportExcelUtil.exportArrayToExcel(exportArray, 'markup-price-master');
  }

  /*addMoreField(): void {
    const dialogRef = this.dialog.open(AddEditMarkUpPriceComponent, {
      width: '40%',
      panelClass: 'custom-modalbox',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.saveCategorySpec(result);
      }
    });
  }*/

  editSellData(id) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.isEdit = true;
    this.sellModelData = {
      id: 0,
      type: 0,
      user_role: "",
      user_name: "",
      userData: "",
      mobile_number: '',
      price_in_per: 0,
      user: {
        alt_email_address: "",
        auth_type: 0,
        cognito_id: "",
        email: "",
        first_name: "",
        full_payment_period: 0,
        emd_charge_type: ""
      }
    }
    this.sellModelData = this.dataList.results.find(x => x.id == id);
    if (this.sellModelData) {
      if (this.sellModelData.mobile_number && this.sellModelData.type == sellType.Seller.valueOf()) {
        this.mobileHiddenFlag = true;
      }

      if (this.sellModelData.type == sellType.Enterprise.valueOf()) {
        this.form.get('User')?.setValidators(Validators.required);
        this.form.get('User')?.updateValueAndValidity();
        this.form.get('mobile')?.clearValidators();
        this.form.get('mobile')?.updateValueAndValidity();
        this.sellModelData.user_role = 'Enterprise';
        this.userIdHiddenFlag = false;
        this.mobileHiddenFlag = false;
        this.partnerHiddenFlag = true;
        // this.getPartnerContactList();
      }
      else if (this.sellModelData.type == sellType.Seller.valueOf()) {
        this.form.get('User')?.setValidators(Validators.required);
        this.form.get('User')?.updateValueAndValidity();
        this.form.get('mobile')?.setValidators(Validators.required);
        this.form.get('mobile')?.updateValueAndValidity();
        this.sellModelData.user_role = 'Seller'
        this.userIdHiddenFlag = true;
        this.partnerHiddenFlag = false;
        // let queryParams ='first_name__icontains=' + this.sellModelData.user.first_name + '&groups__name__iexact=Customer' + '&limit=999';
        // this.GetUsers(queryParams);
      }
      else {
        this.form.get('mobile')?.clearValidators();
        this.form.get('mobile')?.updateValueAndValidity();
        this.form.get('User')?.clearValidators();
        this.form.get('User')?.updateValueAndValidity();
        this.sellModelData.user_role = 'Default';
        this.userIdHiddenFlag = false;
        this.partnerHiddenFlag = false;
        this.mobileHiddenFlag = false;
      }
      this.sellModelData.user_name = this.sellModelData.user.first_name
    }
  }

  saveEditCategorySpec(id): void {
    let obj: any = {};
    obj.id = this.sellModelData.id,
      obj.type = this.sellModelData.type,
      obj.user = this.sellModelData.user.cognito_id,
      obj.mobile_number = this.sellModelData.mobile_number,
      obj.price_in_per = this.sellModelData.price_in_per

    let isDefault = this.dataList.results.filter(x => x.emd_charge_type == "Default" && x.id != this.sellModelData.id);

    if (id <= 0) {
      if (isDefault && isDefault.length > 0) {
        this.notify.error(
          'Data already exist!'
        );
      }
      else {
        this.markUpPriceMasterService
          .createMarkUpPrice(obj)
          .subscribe(async (resp: any) => {
            this.notify.success(
              'Data Saved Successfully'
            );
            this.form.reset();
            this.dataList = await this.dataSource.loadMasterData(1, 3, '-id', '');
            this.dataSource1 = this.dataList.results?.map((value: any) =>
              this.getProperties(value)
            );
          });
      }
    }
    else {
      if (isDefault && isDefault.length > 0) {
        this.notify.error(
          'Data already exist!'
        );
      }
      else {
        this.markUpPriceMasterService
          .updateMarkUpPrice(obj, id)
          .subscribe(async (resp: any) => {
            this.isEdit = false;
            this.notify.success(
              'Data Updated Successfully'
            );

            this.form.reset();
            let pageIndex = this.paginator.pageIndex + 1;
            this.dataList = await this.dataSource.loadMasterData(pageIndex, this.paginator.pageSize, '-id', '');
            this.dataSource1 = this.dataList.results?.map((value: any) =>
              this.getProperties(value)
            );
          });
      }
    }
  }

  editField(element: any): void {
    const dialogRef = this.dialog.open(AddEditMarkUpPriceComponent, {
      width: '40%',
      panelClass: 'custom-modalbox',
      data: {
        isEdit: true,
        element: element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // this.newEquipmentService.createCategoryField/updateCategoryField

        const postBody = {
          type: Number(result.Type),
          user: result.User,
          mobile_number: result.MobileNo,
          price_in_per: result.PricePercent,
        };

      }
    });
  }

  deleteField(id: number): void {
    // this.newEquipmentService.deleteCategoryField().subscribe();
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })
      
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.markUpPriceMasterService
            .deleteMarkUpPrice(id)
            .subscribe(async () => {
              this.notify.success('Data Deleted Successfully.');
              this.dataList = await this.dataSource.loadMasterData(1, 3, '-id', '');
              this.dataSource1 = this.dataList.results?.map((value: any) =>
                this.getProperties(value)
              );
            });
        }
      });
  }

  UserChange(StrUser: string, event) {
    if (event.isUserInput) {
      this.UsersJData.filter((res) => {
        if (res.cognito_id == StrUser) {
          this.sellModelData.mobile_number = res.mobile_number;
          this.sellModelData.user.cognito_id = res.cognito_id;
          if (StrUser && this.isSellerList) {
            this.mobileHiddenFlag = true;
          }
        }
      });
      // this.sellModelData.user.cognito_id = this.UsersJData.filter(x => x.first_name == StrUser).map(x => x.cognito_id).join(",")
      // this.sellModelData.mobile_number = this.UsersJData.filter(x => x.first_name == StrUser).map(x => x.mobile_number).join(",")
    }
  }

  partnerChange(StrUser: string, event) {
    if (event.isUserInput) {
      this.UsersJData.filter((res) => {
        if (res.user.cognito_id == StrUser) {
          this.sellModelData.user.cognito_id = res.user.cognito_id;
        }
      });
      // this.sellModelData.user.cognito_id = this.UsersJData.filter(x => x.first_name == StrUser).map(x => x.cognito_id).join(",")
      // this.sellModelData.mobile_number = this.UsersJData.filter(x => x.first_name == StrUser).map(x => x.mobile_number).join(",")
    }
  }
}
