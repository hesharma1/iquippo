import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SaleMasterService {

  constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) {

  }

  getSaleMaster(queryParams: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.SaleMaster}${queryParams ? '?' + queryParams : ''}`,
     environment.bannersBaseUrl);
  }

  getSellerList(queryParams: string) {
    return this.httpClientService.HttpGetRequestCommon(`${this.apiPath.sellerAndCustomerList}?${queryParams}`, environment.partnerBaseUrl)
  }

  getSaleMasterList(payload) {
    return this.httpClientService.HttpGetRequestCommon(this.apiPath.SaleMaster, environment.bannersBaseUrl, payload);
  }

  SaleMstrGetSellerLst() {
    return this.httpClientService.HttpGetRequestCommon(
      this.apiPath.sellerList + "?limit=" + 999,
      environment.bannersBaseUrl
    );
  }

  SaleMstrSave(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(
      requestModel,
      this.apiPath.SaleMaster,
      environment.bannersBaseUrl
    );
  }

  SaleMstrUpdate(requestModel: any, ID: number) {
    return this.httpClientService.HttpPutRequestCommon(
      requestModel,
      this.apiPath.SaleMaster + ID + "/",
      environment.bannersBaseUrl
    );
  }

  SaleMstrDeleteTableRowData(SaleMasterID: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      this.apiPath.SaleMaster + SaleMasterID + "/",
      environment.bannersBaseUrl
    );
  }

  SaleMstrSearch(Txt: any) {
    return this.httpClientService.HttpGetRequestCommon(
      this.apiPath.SaleMaster + '?search=' + Txt,
      environment.bannersBaseUrl
    );
  }

}
