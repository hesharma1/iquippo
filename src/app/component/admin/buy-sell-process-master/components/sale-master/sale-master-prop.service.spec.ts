import { TestBed } from '@angular/core/testing';

import { SaleMasterPropService } from './sale-master-prop.service';

describe('SaleMasterPropService', () => {
  let service: SaleMasterPropService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaleMasterPropService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
