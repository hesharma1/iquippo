import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class SaleMasterPropService {
  //constructor() { }
  id: number = 0;
  type: number = 0;
  user_role: string = '';
  user_name: string = '';
  mobile_number: string = '';
  functionality: string = '';
  buy_now_price_approval: boolean = true;
  negotiated_sale_approval: boolean = true;
  cooling_period_unit: string = '';
  cooling_period_count: number = 0;
  emd_period: number = 0;
  full_payment_period: number = 0;
  emd_charge_type: string = '';
  user: UserData = {
    alt_email_address: "",
    auth_type: 0,
    cognito_id: "",
    email: "",
    first_name: "",
    full_payment_period: 0,
    emd_charge_type: ""
  };
}

export class MarkUpPriceMasterData {
  id: number = 0;
  type: Number = 0;
  user_role: string = "";
  user_name: string = "";
  userData: string = "";
  mobile_number: string = '';
  price_in_per: number = 0;
  user: UserData = {
    alt_email_address: "",
    auth_type: 0,
    cognito_id: "",
    email: "",
    first_name: "",
    full_payment_period: 0,
    emd_charge_type: ""
  };
}

export interface UserData {
  alt_email_address: string
  auth_type: number
  cognito_id: string
  email: string
  first_name: string
  full_payment_period: number;
  emd_charge_type: string;
}


export enum sellType {
  Enterprise = 1,
  Seller = 2,
  Default = 3,
}

export enum sellerType {
  CUSTOMER = 1,
  DEALER = 2
}


