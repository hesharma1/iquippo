import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleMasterComponent } from './sale-master.component';

describe('SaleMasterComponent', () => {
  let component: SaleMasterComponent;
  let fixture: ComponentFixture<SaleMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
