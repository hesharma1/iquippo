import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SaleMasterPropService, sellerType, sellType } from '../sale-master/sale-master-prop.service';
import { SaleMasterService } from '../sale-master/sale-master.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import * as _ from 'underscore';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { ConfirmDialogComponent } from '../../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';

@Component({
  selector: 'app-sale-master',
  templateUrl: './sale-master.component.html',
  styleUrls: ['./sale-master.component.css']
})

export class SaleMasterComponent implements OnInit {
  UserRoleTypeArray: Array<any> = [
    { typeName: 'Enterprise', value: 1 },
    { typeName: 'Seller', value: 2 },
    { typeName: 'Default', value: 3 },
  ];
  buynowArray: Array<any> = [
    { typeName: 'Yes', value: 1, booleanvalue: true },
    { typeName: 'No', value: 0, booleanvalue: false },
  ];
  negotiatedSaleArray: Array<any> = [
    { typeName: 'Yes', value: 1, booleanvalue: true },
    { typeName: 'No', value: 0, booleanvalue: false },
  ];

  sellType: typeof
    sellType = sellType;

  sellerType: typeof
    sellerType = sellerType;

  userRoleTypeValue: number = 0;
  SaleMstrFrmGrp: FormGroup;
  userIdHiddenFlag = false;
  partnerHiddenFlag = false;
  mobileHiddenFlag = false;
  activePage: any = 1;

  Enterprise_Value: number = 1;
  Customer_Value: number = 2;
  Default_Value: number = 3;
  Action: string = "Save";
  ID: number = 0;
  UserRoleType: string = '';
  User: string = '';
  mobile: string = '';
  functionality: string = '';
  buyNowPriceApproval: string = '';
  negotiatedSaleApproval: string = '';
  coolingPeriodIn: string = '';
  coolingPeriod: string = '';
  emdPeriod: string = '';
  fullPaymentPeriod?: string = '';
  norecordvariable: boolean = false;
  SellData = new MatTableDataSource<any>([]);
  public UsersJData: any;
  isSellerList: boolean = false;
  sellerTypeArray: Array<any> = [
    { typeName: 'Customer', value: 1 },
    { typeName: 'Dealer', value: 2 }
  ];
  sellModelData: SaleMasterPropService = new SaleMasterPropService();
  // "buy_now_price_approval", "negotiated_sale_approval"
  sellDisplayedColumns = ["id", "type", "user", "functionality", "cooling_period_unit", "emd_period", "full_payment_period", "actions"];

  // @ViewChild("sellPaginator") set sellPaginator(pager: MatPaginator) {
  //   if (pager) this.SellData.paginator = pager;
  // }

  //@ViewChild(MatSort, { static: true }) sort!: MatSort;
  public ordering: string = '-id';
  objSell: any;
  sellerList: any = [];
  public usedAssetList;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;
  constructor(private formbuilder: FormBuilder, private saleMstrService: SaleMasterService, private notify: NotificationService,
    private usedEquipmentService: UsedEquipmentService,
    private dialog: MatDialog,
    private partnerDealerRegistrationService: PartnerDealerRegistrationService) {
    this.SaleMstrFrmGrp = this.formbuilder.group({
      'userRole': ['', Validators.required],
      'User': [''],
      'mobile': ['', Validators.required],
      'functionality': ['', Validators.required],
      'buyNowPriceApproval': [''],
      'negotiatedSaleApproval': [''],
      'coolingPeriodIn': ['', Validators.required],
      'coolingPeriod': ['', Validators.required],
      'emdPeriod': ['', Validators.required],
      'fullPaymentPeriod': ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.GetTblSaleMstr();
  }

  sellerTypeSelect(value) {
    this.sellModelData.type = value;
    this.SaleMstrFrmGrp?.get('mobile')?.setValue('');
    this.SaleMstrFrmGrp?.get('User')?.setValue('');
    this.sellerList = [];
  }

  GetTblSaleMstr(): void {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.saleMstrService.getSaleMasterList(payload)
      .subscribe(async (res: any) => {
        this.total_count = res.count;
        if (window.innerWidth > 768) {
          this.SellData.data = res.results;
        } else {
          if (this.page == 1) {
            this.SellData.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.SellData.data = this.listingData;
          } else {
            this.listingData = this.listingData.concat(res.results);
            this.SellData.data = this.listingData;
          }
        }
      },
        error => {
          this.notify.error(
            error
          );
        });
  }

  GetUsers(queryParam): void {
    this.saleMstrService.getSellerList(queryParam)
      .subscribe(async (resp: any) => {
        this.UsersJData = resp.results;
        this.sellModelData.user_name = this.UsersJData.filter(x => x.cognito_id == this.sellModelData.user.cognito_id).map(x => x.cognito_id).join(",")
      },
        error => {
          this.notify.error(
            error
          );
        });
  }

  keyPress(event: any) {
    // const pattern = /[0-9\+\-\ ]/;
    // if (event.keyCode != 8 && !pattern.test(event.key)) {
    //   event.preventDefault();
    // }
    // else if (this.SaleMstrFrmGrp.controls['mobile'].value.length >= 3) {
    //   this.searchSeller(event.target.value);
    // }
    this.sellerList = [];
    if (this.sellerList.length == 0 && this.SaleMstrFrmGrp.controls['mobile'].value.length < 9) {
      this.sellerList = [];
    }
    else {
      this.searchSeller(event);
    }
  }

  searchSeller(e) {
    let payload = {
      "number": e.target.value,
      "source": "bulk_upload",
      "role": this.SaleMstrFrmGrp.get('userRole')?.value == 2 ? 'Dealer' : 'Customer'
    }
    this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
      (res: any) => {
        this.sellerList = [];
        if (res.data)
          this.sellerList = res.data;
      },
      (error) => {
        // console.log("eror", error);
        this.sellerList = [];
      }
    );
  }

  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      this.sellModelData.user.cognito_id = res.cognito_id;
      if (res.cognito_id == search) {
        this.selectSeller(res);
      }
    });
  }

  selectSeller(seller) {
    this.sellModelData.mobile_number = seller.mobile_number;
    this.SaleMstrFrmGrp?.get('mobile')?.setValue(parseInt(seller.mobile_number));
    this.SaleMstrFrmGrp?.get('User')?.setValue(seller.first_name);
  }

  getPartnerContactList() {
    this.partnerDealerRegistrationService.getPartnerContactListWithoutId(1, 200).subscribe(
      (res: any) => {
        this.UsersJData = res.results;
        this.sellModelData.user_name = this.UsersJData.filter(x => x.user.cognito_id == this.sellModelData.user.cognito_id).map(x => x.user.cognito_id).join(",")
      },
      (err) => {
        console.error(err);
      }
    );
  }

  UserRoleChange(userRoleDDLValue) {
    if (userRoleDDLValue === "Enterprise") {
      this.sellModelData.type = sellType.Enterprise.valueOf();
      this.userIdHiddenFlag = false;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = true;
      this.isSellerList = false;
      this.UsersJData = [];
      this.sellModelData.mobile_number = "";
      this.getPartnerContactList();
      this.SaleMstrFrmGrp.get('User')?.setValidators(Validators.required);
      this.SaleMstrFrmGrp.get('User')?.updateValueAndValidity();
      this.SaleMstrFrmGrp.get('mobile')?.clearValidators();
      this.SaleMstrFrmGrp.get('mobile')?.updateValueAndValidity();
    }
    else if (userRoleDDLValue === "Seller") {
      this.SaleMstrFrmGrp.get('User')?.setValidators(Validators.required);
      this.SaleMstrFrmGrp.get('User')?.updateValueAndValidity();
      this.SaleMstrFrmGrp.get('mobile')?.setValidators(Validators.required);
      this.SaleMstrFrmGrp.get('mobile')?.updateValueAndValidity();
      this.sellModelData.type = sellType.Seller.valueOf();
      this.userIdHiddenFlag = true;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = false;
      this.isSellerList = true;
      this.UsersJData = [];
      let queryParam = 'limit=100&ordering=-id&groups__name__iexact=customer';
      this.GetUsers(queryParam);
    }
    else if (userRoleDDLValue == "Default") {
      this.sellModelData.type = sellType.Default.valueOf();
      this.userIdHiddenFlag = false;
      this.mobileHiddenFlag = false;
      this.partnerHiddenFlag = false;
      this.isSellerList = false;
      this.sellModelData.mobile_number = "";
      this.UsersJData = [];
      this.SaleMstrFrmGrp.get('mobile')?.clearValidators();
      this.SaleMstrFrmGrp.get('mobile')?.updateValueAndValidity();
      this.SaleMstrFrmGrp.get('User')?.clearValidators();
      this.SaleMstrFrmGrp.get('User')?.updateValueAndValidity();
    }
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.GetTblSaleMstr();
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.GetTblSaleMstr();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.GetTblSaleMstr();
    }
  }

  SaleMstrSave(id) {
    let isDefault = this.SellData.data.filter(x => x.type == sellType.Default.valueOf() && this.sellModelData.type == sellType.Default.valueOf());
    let obj: any = {};
    obj.id = this.sellModelData.id,
      obj.type = this.sellModelData.type,
      obj.user = this.sellModelData.user.cognito_id,
      obj.mobile_number = this.sellModelData.mobile_number,
      obj.functionality = this.sellModelData.functionality,
      // obj.buy_now_price_approval = this.sellModelData.buy_now_price_approval,
      // obj.negotiated_sale_approval = this.sellModelData.negotiated_sale_approval,
      obj.cooling_period_unit = this.sellModelData.cooling_period_unit,
      obj.cooling_period_count = this.sellModelData.cooling_period_count,
      obj.emd_period = this.sellModelData.emd_period,
      obj.full_payment_period = this.sellModelData.full_payment_period,
      obj.emd_charge_type = this.sellModelData.emd_charge_type;
    if (id <= 0) {
      if (isDefault && isDefault.length > 0) {
        this.notify.error(
          'Data already exist!'
        );
      }
      else {
        this.saleMstrService.SaleMstrSave(obj)
          .subscribe(async (resp: any) => {
            this.notify.success(
              'Data Saved Successfully'
            );
            this.SaleMstrFrmGrp.reset();
            this.GetTblSaleMstr();
          });
      }
    }
    else if (id > 0) {
      if (isDefault && isDefault.length > 1) {
        this.notify.error(
          'Data already exist!'
        );
      }
      else {
        this.saleMstrService.SaleMstrUpdate(obj, id)
          .subscribe(async (resp: any) => {
            this.notify.success(
              'Data Updated Successfully'
            );
            this.SaleMstrFrmGrp.reset();
            this.GetTblSaleMstr();
          });
      }
    }
  }

  UserChange(StrUser: string) {
    this.sellModelData.user.cognito_id = StrUser;
    this.sellModelData.mobile_number = this.UsersJData.filter(x => x.cognito_id == StrUser).map(x => x.mobile_number).join(",")
    if (StrUser && this.isSellerList) {
      this.mobileHiddenFlag = true;
    }
  }

  editSellData(id) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.sellModelData = this.SellData.data.find(x => x.id == id);
    this.SaleMstrFrmGrp?.get('mobile')?.setValue(parseInt(this.sellModelData.mobile_number));
    this.SaleMstrFrmGrp?.get('User')?.setValue(this.sellModelData.user.first_name);
    this.SaleMstrFrmGrp?.get('userRole')?.setValue(this.sellModelData.type);
    console.log("seller", this.sellModelData);
  }

  deleteSellData(id) {
    this.dialog
      .open(ConfirmDialogComponent, {
        width: '500px'
      })
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.saleMstrService.SaleMstrDeleteTableRowData(id)
            .subscribe(async (resp: any) => {
              this.notify.success('Data Deleted Successfully.');
              this.GetTblSaleMstr();
            },
              error => {
                this.notify.error(
                  error
                );
              });
        }
      });
  }

  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.SellData.data.length) && (this.SellData.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          this.page++;
          this.GetTblSaleMstr();
        }
      }
    }
  }

}
