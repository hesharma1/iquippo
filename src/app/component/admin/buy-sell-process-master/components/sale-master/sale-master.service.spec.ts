import { TestBed } from '@angular/core/testing';

import { SaleMasterService } from './sale-master.service';

describe('SaleMasterService', () => {
  let service: SaleMasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaleMasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
