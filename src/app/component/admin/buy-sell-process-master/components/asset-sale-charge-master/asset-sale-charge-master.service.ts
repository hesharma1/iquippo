import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { HttpClientService } from 'src/app/utility/http-client.service';

@Injectable({
  providedIn: 'root',
})
export class AssetSaleChargeMasterService {
  constructor(
    private httpClientService: HttpClientService,
    private apiPath: ApiRouteService
  ) {}

  // getModelTechSpecList(queryParams: string) {
  //   return this.httpClientService.HttpGetRequestCommon(
  //     `${this.apiPath.getModelTechSpecification}?${queryParams}`,
  //     environment.baseUrl
  //   );
  // }

  updateAssetSaleCharge(requestModel: any, id: any) {
    return this.httpClientService.HttpPutRequestCommon(
      requestModel,
      `${this.apiPath.assetSaleChargeMaster}${id}/`,
      environment.baseUrl
    );
  }

  createAssetSaleCharge(requestModel: any) {
    return this.httpClientService.HttpPostRequestCommon(
      requestModel,
      this.apiPath.assetSaleChargeMaster,
      environment.baseUrl
    );
  }

  deleteAssetSaleCharge(id: number) {
    return this.httpClientService.HttpDeleteRequestCommon(
      `${this.apiPath.assetSaleChargeMaster}${id}/`,
      environment.baseUrl
    );
  }
}
