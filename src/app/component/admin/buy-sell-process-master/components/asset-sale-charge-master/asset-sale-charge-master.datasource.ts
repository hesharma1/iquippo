import { CollectionViewer, DataSource } from '@angular/cdk/collections';

import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { AssetSaleChargeeMasterDataService } from './asset-sale-charge-master-data.service';

export class AssetSaleChargeMasterDataSource implements DataSource<any> {
  private listSubject = new BehaviorSubject<any[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(
    private assetSaleChargeMasterDataService: AssetSaleChargeeMasterDataService
  ) {}

  loadMasterData(
    pageIndex: number,
    pageSize: number,
    sortDirection: string,
    filter: string
  ) {
    return new Promise((resolve) => {
      this.loadingSubject.next(true);
      this.assetSaleChargeMasterDataService
        .getAssetSaleChargeList(pageIndex, pageSize, sortDirection, filter)
        .subscribe((list) => {
          this.listSubject.next(list);
          resolve(list);
          // this.listSubject.next(list.results);
        });
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    console.log('Connecting data source');
    return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listSubject.complete();
    this.loadingSubject.complete();
  }
}
