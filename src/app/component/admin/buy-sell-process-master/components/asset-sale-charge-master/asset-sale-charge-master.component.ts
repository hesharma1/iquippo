import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { merge, fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay,
} from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

import { ConfirmDialogComponent } from '../../../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';

import { AssetSaleChargeeMasterDataService } from './asset-sale-charge-master-data.service';
import { AssetSaleChargeMasterService } from './asset-sale-charge-master.service';
import { AssetSaleChargeMasterDataSource } from './asset-sale-charge-master.datasource';
import { AddEditAssetSaleChargeComponent } from '../../dialogs/add-edit-asset-sale-charge/add-edit-asset-sale-charge.component';
import { formatDate, DatePipe } from '@angular/common';

@Component({
  selector: 'app-mark-up-price-master',
  templateUrl: './asset-sale-charge-master.component.html',
  styleUrls: ['./asset-sale-charge-master.component.css'],
})
export class AssetSaleChargeMasterComponent implements OnInit, AfterViewInit {
  listingData = [];
  dataSource1 = new MatTableDataSource<[]>();
  dataSource: any = [];
  displayedColumns = [
    'type',
    'user',
    'from_date',
    'to_date',
    'category_name',
    'charge_basis',
    'amount',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('input') input!: ElementRef;

  dataList: any;
  total_count = 0;

  constructor(
    private route: ActivatedRoute,
    private assetSaleChargeMasterDataService: AssetSaleChargeeMasterDataService,
    private assetSaleChargeMasterService: AssetSaleChargeMasterService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    public adminService: AdminMasterService,
    private datePipe: DatePipe
  ) {
    // this.categorySpecForm = this.fb.group({
    //   Category: [null],
    //   CategoryFieldList: this.fb.array([]),
    // });
  }

  async ngOnInit() {
    this.dataSource = new AssetSaleChargeMasterDataSource(
      this.assetSaleChargeMasterDataService
    );
    this.dataList = await this.dataSource.loadMasterData(1, 3, 'asc', '');
    this.dataSource1 = this.dataList.results?.map((value: any) =>
      this.getProperties(value)
    );
  }

  getProperties(element: any) {
    return {
      id: element.id,
      type: element.type,
      user: element.user,
      from_date: element.start_date,
      to_date: element.end_date,
      category_name: element.category.display_name,
      category_id: element.category.id,
      charge_basis: element.charge_basis,
      amount: element.amount,
      mobileNumber: element.mobile_number,
    };
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(async () => {
          this.paginator.pageIndex = 0;
          this.dataList = await this.dataSource.loadMasterData(1, 3, 'asc', '');
          this.dataSource1 = this.dataList.results?.map((value: any) =>
            this.getProperties(value)
          );
        })
      )
      .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(async () => {
          this.getData()
        })
      )
      .subscribe();
  }

  async exportToExcel() {
    const specList = await this.dataSource.loadMasterData(1, 99999, 'asc', '');
    const exportArray = specList.results.map((value: any) => {
      return {
        type:
          value.type === 1
            ? 'Enterprise'
            : value.type === 2
              ? 'Seller'
              : 'Default',
        user: value.user,
        from_date: value.start_date,
        to_date: value.end_date,
        category_name: value.category.display_name,
        charge_basis: value.charge_basis === '1' ? 'Flat' : 'percent',
        amount: value.amount,
      };
    });
    ExportExcelUtil.exportArrayToExcel(exportArray, 'asset-sale-charge-master');
  }

  addMoreField(): void {
    const dialogRef = this.dialog.open(AddEditAssetSaleChargeComponent, {
      width: '40%',
      panelClass: 'custom-modalbox',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.saveCategorySpec(result);
      }
    });
  }

  saveCategorySpec(result): void {
    const postBody = {
      type: Number(result.Type),
      user: result.User,
      mobile_number: result.MobileNo,
      start_date: this.datePipe.transform(result.FromDate, 'yyyy-MM-dd'),
      end_date: this.datePipe.transform(result.ToDate, 'yyyy-MM-dd'),
      category: result.AssetCategory,
      charge_basis: result.ChargeBasis,
      amount: result.Amount,
    };
    this.assetSaleChargeMasterService
      .createAssetSaleCharge(postBody)
      .subscribe(async (resp: any) => {
        this.dataList = await this.dataSource.loadMasterData(1, 3, 'asc', '');
        this.dataSource1 = this.dataList.results?.map((value: any) =>
          this.getProperties(value)
        );
      });
  }

  editField(element: any): void {
    const dialogRef = this.dialog.open(AddEditAssetSaleChargeComponent, {
      width: '40%',
      panelClass: 'custom-modalbox',
      data: {
        isEdit: true,
        element: element,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const postBody = {
          type: Number(result.Type),
          user: result.User,
          mobile_number: result.MobileNo,
          start_date: this.datePipe.transform(result.FromDate, 'yyyy-MM-dd'),
          end_date: this.datePipe.transform(result.ToDate, 'yyyy-MM-dd'),
          category: result.AssetCategory,
          charge_basis: result.ChargeBasis,
          amount: result.Amount,
        };
        this.assetSaleChargeMasterService
          .updateAssetSaleCharge(postBody, result.Id)
          .subscribe(async (resp: any) => {
            this.dataList = await this.dataSource.loadMasterData(
              1,
              3,
              'asc',
              ''
            );
            this.dataSource1 = this.dataList.results?.map((value: any) =>
              this.getProperties(value)
            );
          });
      }
    });
  }

  deleteField(id: number): void {
    // this.newEquipmentService.deleteCategoryField().subscribe();
    const dialogRef = this.dialog
      .open(ConfirmDialogComponent)
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.assetSaleChargeMasterService
            .deleteAssetSaleCharge(id)
            .subscribe(async () => {
              this.dataList = await this.dataSource.loadMasterData(
                1,
                3,
                'asc',
                ''
              );
              this.dataSource1 = this.dataList.results?.map((value: any) =>
                this.getProperties(value)
              );
            });
        }
      });
  }

  scrolled = false;
  // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.dataList.results.length) && (this.dataList.results.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.paginator.pageIndex++;
       this.getData();
        }
      }
    }
  }
}
  async getData() {
    let pageindex = this.paginator.pageIndex + 1;
    // Changes for Card layout on Mobile devices
    if(window.innerWidth > 768){
      this.dataList = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
      }else{
        this.scrolled = false;
        if(pageindex == 1){
          this.dataList = [];
          this.listingData = [];
          let results = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
          this.dataList = results;
          this.listingData = results.results;
          this.dataList.results = this.listingData;
        }else{
          let results = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '')
          this.dataList.results = this.dataList.results.concat(results.results);
          //this.dataList.results = this.listingData;
        }
      }
    //this.dataList = await this.dataSource.loadMasterData(pageindex, this.paginator.pageSize, '-id', '');
    this.dataSource1 = this.dataList.results?.map((value: any) =>
      this.getProperties(value)
    );
  }
}
