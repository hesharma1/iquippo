import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { HttpClientService } from 'src/app/utility/http-client.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root',
  })
  export class EmdMasterService 
  {
    constructor(private httpClientService: HttpClientService, private apiPath: ApiRouteService) {}

    EmdChargeSave(requestModel: any) 
    {
        return this.httpClientService.HttpPostRequestCommon(
          requestModel,
          this.apiPath.EmdChargeMaster,
          environment.bannersBaseUrl
        );
    }

    emdChargeUpdate(requestModel: any,emdMasterID:number){
        return this.httpClientService.HttpPutRequestCommon(requestModel,
            this.apiPath.EmdChargeMaster + emdMasterID, 
            environment.bannersBaseUrl
        );
    }

    EmdChargeGetTable(payload) 
    {
        return this.httpClientService.HttpGetRequestCommon(
            this.apiPath.EmdChargeMaster, 
            environment.bannersBaseUrl,
            payload
        );
    }

    emdChargeGet(EmdMasterID:number) 
    {
        return this.httpClientService.HttpGetRequestCommon(
            this.apiPath.EmdChargeMaster + EmdMasterID, 
            environment.bannersBaseUrl
        );
    }

    EmdChargeDeleteTableRowData(EmdMasterID:number) 
    {
        return this.httpClientService.HttpDeleteRequestCommon(
            this.apiPath.EmdChargeMaster  + EmdMasterID, 
            environment.bannersBaseUrl
        );
    }

    EmdChargeSearch(Txt:any) 
    {
        return this.httpClientService.HttpGetRequestCommon(
            this.apiPath.EmdChargeMaster + '?search=' + Txt,
            environment.baseUrl       
        );
    }


  }