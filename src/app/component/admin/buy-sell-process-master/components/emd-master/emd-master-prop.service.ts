import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmdMasterPropService {

  constructor() 
  { 
    this.userRole = 0;
    this.userId = '';
    this.categoryId = '';
    this.emdCharge = 0

    this.type = '';
    this.emd_charge_type = '';
    this.user = '';
    this.display_name = '';
    this.amount = '';
  }

  userRole: number;
  userId: any;
  categoryId: any;
  emdCharge : any;

  type : string;
  emd_charge_type:string;
  user: string;
  display_name: string;
  amount:string;

}
