import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { MatTableDataSource } from '@angular/material/table';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { EmdMasterService } from './emd-master.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ProductlistService } from 'src/app/services/product-list.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { numberonly } from 'src/app/utility/custom-validators/forgot-password';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';

@Component({
  selector: 'app-emd-master',
  templateUrl: './emd-master.component.html',
  styleUrls: ['./emd-master.component.css']
})
export class EmdMasterComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public emdChargeForm: any = this.formbuilder.group({
    emdType: new FormControl('', Validators.required),
    seller_type: new FormControl('', Validators.required),
    mobile: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    asset: new FormControl('', Validators.required),
    amountType: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)]))
  }, { validator: [numberonly('amount')] });
  public usedAssetList;
  public emdDataModel: any;
  public isApprovedStatus: number = 2;
  public emdDataSouce: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public editId;
  public listingData = [];
  public searchText!: string;
  public total_count: any;
  public activePage: any = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public tblColumns = ["id", "emd_charge_type", "used_equipment.id", "used_equipment.product_name", "emd_amount_type", "amount", "Actions"];
  public showForm: boolean = false;
  public chargeFilter: any;
  public amountTypeFilter: any;
  public amountTypeOptions: Array<any> = [
    { name: 'Flat', value: 1 },
    { name: 'Percentage', value: 2 }
  ];
  public chargeOptions: Array<any> = [
    // { name: 'Auction', value: 1 },
    { name: 'Used Asset', value: 2 },
    { name: 'Default', value: 3 }
  ];
  sellerTypeArray: Array<any> = [
    { typeName: 'Customer', value: 1 },
    { typeName: 'Dealer', value: 2 }
  ];
  sellerList: any = [];
  sellerTypeValue: any = 2;
  amountText: any = 'Amount';
  amountValue: any;
  constructor(private formbuilder: FormBuilder, private adminMasterService: AdminMasterService, private apiPath: ApiRouteService, private emdMasterService: EmdMasterService, private notify: NotificationService, public productService: ProductlistService, public spinner: NgxSpinnerService,
    private usedEquipmentService: UsedEquipmentService,) { }

  ngOnInit(): void {
    //this.getUsedAssetList();
    this.getEmdList();
  }

  getUsedAssetList() {
    this.productService.getAllUsedEquipmentList(this.isApprovedStatus).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.usedAssetList = res.results;
      },
      err => {
        console.log(err);
      }
    );
  }
  sellerTypeSelect(value) {
    this.emdChargeForm.get('seller_type')?.setValue(value)
  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    //    let inputChar = String.fromCharCode(event.charCode);
    console.log("event.keyCode", event.keyCode);
    console.log("mobile value", this.emdChargeForm.controls['mobile'].value.length);
    this.sellerList = [];

    if (event.keyCode != 8 && !pattern.test(event.key)) {
      event.preventDefault();
    }
    else if (this.emdChargeForm.controls['mobile'].value.length >= 3) {
      this.searchSeller(event.target.value);
    }
  }
  searchSeller(e) {
    console.log("value",);
    let payload = {
      "number": e,
      "source": "bulk_upload",
      "role": this.emdChargeForm.get('seller_type')?.value == 2 ? 'Dealer' : 'Customer'
    }
    this.usedEquipmentService.getPartnerSellerList(payload).subscribe(
      (res: any) => {
        this.sellerList = [];
        if (res.data)
          this.sellerList = res.data;
        console.log("sellerList", this.sellerList)
      },
      (error) => {
        console.log("eror", error);
        this.sellerList = [];
      }
    );

  }
  onSellerSelect(search: any) {
    this.sellerList.filter((res) => {
      if (res.cognito_id == search) {
        this.selectSeller(res);
      }
    });
  }
  selectSeller(seller) {
    this.emdChargeForm?.get('mobile')?.setValue(parseInt(seller.mobile_number));
    this.emdChargeForm?.get('firstname')?.setValue(seller.first_name);
    let payload = {
      seller__cognito_id__in: seller.cognito_id,
      limit: '999',
      status__in: 2
    }
    this.usedEquipmentService.getUsedEquipment(payload).subscribe((res: any) => {
      this.usedAssetList = res.results;
    })
  }

  action(actionType: string, id: number) {
    if (actionType === 'Edit') // for group edit
    {
      this.emdMasterService.emdChargeGet(id).subscribe(
        res => {
          if (res != null) {
            this.emdDataModel = res as any;
            this.showForm = true;
            this.emdChargeForm.setValue({
              emdType: this.emdDataModel.type,
              asset: this.emdDataModel.used_equipment.id,
              amountType: this.emdDataModel.amount_type,
              amount: this.emdDataModel.amount,
              seller_type: this.emdDataModel.used_equipment.seller_type,
              mobile: this.emdDataModel.used_equipment.seller.mobile_number,
              firstname: this.emdDataModel.used_equipment.seller.first_name
            });
            this.amountValue = this.amountValue = this.emdChargeForm?.get('amountType')?.value == 2 ? this.emdChargeForm?.get('amount')?.value + ' %'
              : this.emdChargeForm?.get('amount')?.value;
            this.selectSeller(this.emdDataModel.used_equipment.seller);
            this.sellerTypeValue = this.emdDataModel.used_equipment.seller_type;
            this.editId = this.emdDataModel.id;
          }
        });
    }
    else if (actionType === 'Delete') // for group delete
    {
      this.emdMasterService.EmdChargeDeleteTableRowData(id).subscribe(
        res => {
          if (res == null) {
            this.notify.success('Data Deleted Successfully.');
            //this.getGroupList('', '');
            this.getEmdList();
            this.editId = '';
          }
        });
    }
  }

  /**search list */
  searchInList() {
    if (this.searchText === '' || this.searchText?.length >= 3) {
      this.getEmdList();
    }
  }

  saveData() {
    this.emdChargeForm.markAllAsTouched();
    if (this.emdChargeForm.valid) {
      const formData = this.emdChargeForm.value;
      this.emdDataModel = new Object();
      this.emdDataModel.type = formData.emdType;
      this.emdDataModel.used_equipment = formData.asset;
      this.emdDataModel.amount_type = formData.amountType;
      this.emdDataModel.amount = formData.amount;
      this.emdMasterService.EmdChargeSave(this.emdDataModel).subscribe(
        res => {
          if (res != null) {
            this.emdDataModel = res;;
            this.emdChargeForm.reset();
            this.notify.success('Data Saved Successfully.');
            this.getEmdList();
            this.editId = '';
          }
        });
    }
  }

  updateData() {
    this.emdChargeForm.markAllAsTouched();
    if (this.emdChargeForm.valid) {
      const formData = this.emdChargeForm.value;
      this.emdDataModel = new Object();
      this.emdDataModel.type = formData.emdType;
      this.emdDataModel.used_equipment = formData.asset;
      this.emdDataModel.amount_type = formData.amountType;
      this.emdDataModel.amount = formData.amount;
      this.emdMasterService.emdChargeUpdate(this.emdDataModel, this.editId).subscribe(
        res => {
          if (res != null) {
            this.emdDataModel = res;
            this.emdChargeForm.reset();
            this.notify.success('Data Updated Successfully.');
            this.getEmdList();
            this.editId = '';
          }
        });
    }
  }

  getEmdList() {
    let payload = {
      page: this.activePage,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.chargeFilter) {
      payload['type__in'] = this.chargeFilter;
    }
    if (this.amountTypeFilter) {
      payload['amount_type__in'] = this.amountTypeFilter;
    }
    this.emdMasterService.EmdChargeGetTable(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        this.emdDataSouce.data = res.results;
        // Changes for Card layout on Mobile devices
      if(window.innerWidth > 768){
        this.emdDataSouce.data = res.results;
        }else{
          this.scrolled = false;
          if(this.activePage == 1){
            this.emdDataSouce.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.emdDataSouce.data = this.listingData;
          }else{
            this.listingData = this.listingData.concat(res.results);
            this.emdDataSouce.data = this.listingData;
          }
        }
        this.total_count = res.count;

      },
      err => {
        console.log(err);
      }
    )
  }
  scrolled = false;
  // Changes for Card layout on Mobile devices
@HostListener('window:scroll', ['$event'])
recursiveNewsApiHit = (event) => {
  if(window.innerWidth < 768){
    if((this.total_count > this.emdDataSouce.data.length) && (this.emdDataSouce.data.length > 0)){ 
      if(Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight){
        if(!this.scrolled){
          this.scrolled = true;
        this.activePage++;
       this.getEmdList();
        }
      }
    }
  }
}

  onPageChange(event: PageEvent) {
    this.activePage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getEmdList();
  }

  /**sort list  */
  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getEmdList();
  }

  chargeStatusFilter(val) {
    this.chargeFilter = val;
    this.activePage = 1;
    this.getEmdList();
  }

  amountStatusFilter(val) {
    this.amountTypeFilter = val;
    this.activePage = 1;
    this.getEmdList();
  }

  export() {
    let payload = {
      limit: 999
    }
    this.emdMasterService.EmdChargeGetTable(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Charge Type": r?.emd_charge_type,
            "Asset Name": r?.used_equipment?.product_name,
            "Amount Type": r?.emd_amount_type,
            "Amount": r?.amount,
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Emd Master List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  closeForm() {
    this.showForm = false;
    this.editId = '';
    this.emdChargeForm.reset();
  }

  changeAmount(val) {
    this.amountText = val == 2 ? 'Percentage' : 'Amount';
    if (this.emdChargeForm?.get('amount')?.value) {
      this.amountValue = this.emdChargeForm?.get('amountType')?.value == 2 ? this.emdChargeForm?.get('amount')?.value + ' %'
        : this.emdChargeForm?.get('amount')?.value;
    }
  }

  TotalAmount() {
    this.amountValue = this.emdChargeForm?.get('amountType')?.value == 2 ? this.emdChargeForm?.get('amount')?.value + ' %'
      : this.emdChargeForm?.get('amount')?.value;
  }


}
