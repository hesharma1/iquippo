import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerCommisionInvoicesComponent } from './seller-commision-invoices.component';

describe('SellerCommisionInvoicesComponent', () => {
  let component: SellerCommisionInvoicesComponent;
  let fixture: ComponentFixture<SellerCommisionInvoicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellerCommisionInvoicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerCommisionInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
