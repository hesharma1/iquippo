import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-subscribers-list',
  templateUrl: './subscribers-list.component.html',
  styleUrls: ['./subscribers-list.component.css']
})
export class SubscribersListComponent implements OnInit {
  public subscribersData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns = ["id", "email", "created_at"];
  public ordering: string = '-id';
  public page: number = 1;
  public pageSize: number = 10;
  public total_count: any;
  public listingData = [];
  public searchText!: string;


  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.getSubscribersList();
  }

  getSubscribersList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.commonService.getSubscribersList(payload).subscribe((res: any) => {
      this.total_count = res.count;
      if (window.innerWidth > 768) {
        this.subscribersData.data = res.results;
      } else {
        this.scrolled = false;
        if (this.page == 1) {
          this.subscribersData.data = [];
          this.listingData = [];
          this.listingData = res.results;
          this.subscribersData.data = this.listingData;
        } else {
          this.listingData = this.listingData.concat(res.results);
          this.subscribersData.data = this.listingData;
        }
      }
    })
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getSubscribersList();
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getSubscribersList();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 0) {
      this.page = 1;
      this.pageSize = 10;
      this.getSubscribersList();
    }
  }
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.subscribersData.data.length) && (this.subscribersData.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if(!this.scrolled){
            this.scrolled = true;
          this.page++;
          this.getSubscribersList();
          }
        }
      }
    }
  }

}
