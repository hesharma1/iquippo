import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { S3UploadDownloadService } from 'src/app/utility/s3-upload-download/s3-upload-download.service';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';


@Component({
  selector: 'app-bid-action',
  templateUrl: './bid-action.component.html',
  styleUrls: ['./bid-action.component.css']
})
export class BidActionComponent implements OnInit {
  @ViewChild('imageInput') imageInput!: ElementRef;

  type: any;
  bidactionform?: FormGroup;
  invoiceActionForm?: FormGroup;
  doGenerationForm?: FormGroup;
  sellerInvoiceForm?: FormGroup;
  dateOfDeliveryForm?: FormGroup;
  amount?: number;
  equipment?: any;
  buyer?: string;
  tradeId?: number;
  valuationAmount?: any;
  parkingCharges?: any;
  assetId?: string;
  assetName?: any;
  reservePrice?: any;
  yearOfMfg?: any;
  statusType?: any;
  isSelfInvoice: boolean = true;
  firstNameInvoice?: any;
  lastNameInvoice?: any;
  countryInvoice?: any;
  stateInvoice?: any;
  locationInvoice?: any;
  addressInvoice?: any;
  emailInvoice?: any;
  mobileNoInvoice?: any;
  countryList = [];
  locationList: Array<any> = [];
  states = [];
  DO_DocumentName: Array<object> = [];
  isInvalidfile: boolean = false;
  uploadingImage: boolean = false;
  isConfirming: boolean = false;
  uploadDocument: any;
  tradeMainData: any;

  constructor(
    private fb: FormBuilder,
    public router: Router,
    public dialogRef: MatDialogRef<BidActionComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private adminMasterService: AdminMasterService,
    private route: Router,
    public s3: S3UploadDownloadService,
    private datePipe: DatePipe,
    public notify: NotificationService
  ) {
    if (data) {
      this.type = data.type;
      this.tradeMainData = data?.mainData;
      this.amount = data.amount;
      this.equipment = data.equipmentId;
      this.buyer = data.buyer;
      this.tradeId = data.id;
      this.valuationAmount = data.valuationAmount;
      this.reservePrice = data.reservePrice;
      this.assetName = data.assetName;
      this.yearOfMfg = data.yearOfMfg;
      this.parkingCharges = data.parkingCharges;
      this.statusType = data.statusType;
    }

    if(this.type === 'rejectTransaction'){
      this.bidActionForm();
    }else if(this.type === 'Invoice'){
      this.invoiceForm();
    }else if(this.type === 'DO'){
      this.doGeneration();
    }else if(this.type === 'DOD'){
      this.dodForm();
    }else if(this.type === 'SelInvoice'){
      this.sellerInvoice();
    }
  }

  ngOnInit(): void {
    if (this.type == 'rejectTransaction' && this.tradeMainData?.partial_payment?.length) {
      this.bidactionform?.controls.refund_type.setValidators([Validators.required]);
      this.bidactionform?.controls.refund_type.updateValueAndValidity();
    }
  }

  bidActionForm(){
    this.bidactionform = this.fb.group({
      comments: ['', Validators.required],
      reason: ['', Validators.required],
      refund_type: [''],
      refund_amount: [''],
    });
  }

  invoiceForm() {
    this.invoiceActionForm = new FormGroup({
      isSelfInvoice: new FormControl('1'),
      firstNameInvoice: new FormControl(''),
      lastNameInvoice: new FormControl(''),
      countryInvoice: new FormControl(''),
      stateInvoice: new FormControl(''),
      locationInvoice: new FormControl(''),
      addressInvoice: new FormControl(''),
      emailInvoice: new FormControl('', Validators.pattern(/^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)),
      mobileNoInvoice: new FormControl('', Validators.compose([Validators.pattern('[1-9][0-9]+'),
      Validators.maxLength(10), Validators.minLength(10)]))
    });
  }

  sellerInvoice() {
    this.sellerInvoiceForm = new FormGroup({
      uploadDocument: new FormControl('')
    });
  }

  doGeneration() {
    this.doGenerationForm = new FormGroup({
      uploadDocument: new FormControl(''),
      creationDate: new FormControl('')
    })
  }

  dodForm() {
    this.dateOfDeliveryForm = new FormGroup({
      dateOfDelivery: new FormControl('')
    })
  }

  updateValidation(val) {
    if (val == 'PL') {
      this.bidactionform?.controls.refund_amount.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
      this.bidactionform?.controls.refund_amount.updateValueAndValidity();
    } else {
      this.bidactionform?.controls.refund_amount.setValidators([]);
      this.bidactionform?.controls.refund_amount.updateValueAndValidity();
    }
  }

  getAllStatesForCountry(queryParams: string, countryId: number) {
    return this.adminMasterService.getStateMaster(queryParams).subscribe((res: any) => {
      const allStates = res.results;
      this.states = allStates.filter(l => l.country.id === countryId);
    },
      err => {
        console.log(err);
      }
    )
  }

  getAllLocationsForState(queryParams: string, stateId: number) {
    return this.adminMasterService.getLocationMaster(queryParams).subscribe(
      (res: any) => {
        const allLocations = res.results;
        this.locationList = allLocations.filter(l => l.city.state.id === stateId);
      },
      err => {
        console.log(err);
      }
    )
  }

  backClick() {
    this.dialogRef.close();
  }

  approveBid() {
    const putTradeBody = {
      equipment: this.equipment,
      bid_status: 'SP',
      deal_status: 'CP',
      offer_status: 'BA'
    };
    const putUsedBody = {
      status: 3
    }
    forkJoin([
      this.adminMasterService.puttradeBid(putTradeBody, this.tradeId),
      this.adminMasterService.putUsedEquipmentById(putUsedBody, this.equipment)
    ]).subscribe((res: any) => {
      this.dialogRef.close();
      this.notify.success("Bid Approved", true);
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  rejectTransaction() {
    const putRejectTransTradeBody = {
      amount: this.amount,
      equipment: this.equipment,
      bid_status: 'CL',
      deal_status: 'CP',
      offer_status: this.tradeMainData?.trade_type == "BID" ? 'RE' : 'OR',
      rejection_reason: this.bidactionform?.get('reason')?.value,
      rejection_comment: this.bidactionform?.get('comments')?.value
    };
    if (this.tradeMainData?.partial_payment?.length) {
      if (this.bidactionform?.get('refund_type')?.value == '') {
        this.notify.error('Please select valid Refund Type !!');
        return;
      } else {
        putRejectTransTradeBody['refund_type'] = this.bidactionform?.get('refund_type')?.value;
      }
      if (this.bidactionform?.get('refund_type')?.value == 'PL') {
        if (this.bidactionform?.get('refund_amount')?.value > this.tradeMainData?.partial_payment[0]?.amount) {
          this.notify.error('Refund amount should be less than or equal to Partial payment amount!!');
          return;
        } else {
          putRejectTransTradeBody['refund_amount'] = this.bidactionform?.get('refund_amount')?.value;
        }
      }
    }
    this.adminMasterService.puttradeBid(putRejectTransTradeBody, this.tradeId).subscribe((data: any) => {
      this.dialogRef.close();
      this.notify.success("Transaction Rejected", true);
      this.router.navigate(['/admin-dashboard/trade/trade-details-dashboard']);
    });
  }

  async handleUpload(event: any, tab: string, side: string) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileType = fileFormat.split('.')[fileFormat.split('.').length - 1];
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'application/pdf' || fileType == "docx" || fileType == "doc" || fileType == "docx" || fileType == "dot" || fileType == "dotx")) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/" + this.equipment + '/' + tab + '/' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        if (tab == 'Seller_Invoice') {
          this.sellerInvoiceForm?.get('uploadDocument')?.setValue(uploaded_file.Location);
        }
        else {
          this.doGenerationForm?.get('uploadDocument')?.setValue(uploaded_file.Location);
        }

      }
    } else {
      this.notify.error('Selected document should be pdf and word format');
    }
    this.imageInput.nativeElement.value = '';
  }

  uploadSellerInvoice() {
    let uploadDocUrl = this.sellerInvoiceForm?.get('uploadDocument')?.value;
    const putDOUpdate = {
      amount: this.amount,
      equipment: this.equipment,
      seller_invoice: uploadDocUrl,
      offer_status: 'IG'
    };
    this.adminMasterService.puttradeBid(putDOUpdate, this.tradeId).subscribe((res: any) => {
      this.dialogRef.close('Sales Invoice Uploaded');
      this.notify.success('Seller Invoice Uploaded', true);
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  DODetailSubmited() {
    let uploadDocUrl = this.doGenerationForm?.get('uploadDocument')?.value;
    const customeDate = this.datePipe.transform(this.doGenerationForm?.get('creationDate')?.value, 'YYYY-MM-dd');
    const putDOUpdate = {
      amount: this.amount,
      equipment: this.equipment,
      do_document: uploadDocUrl,
      do_created_at: customeDate,
      is_do_generated: true,
      offer_status: 'DG'
    };
    this.adminMasterService.puttradeBid(putDOUpdate, this.tradeId).subscribe((res: any) => {
      this.dialogRef.close("DO Issued");
      this.notify.success("DO Issued", true);
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  addValidations(event: any) {
    let value = event.value;
    if (value == "1") {
      this.invoiceActionForm?.get('firstNameInvoice')?.clearValidators();
      this.invoiceActionForm?.get('firstNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('lastNameInvoice')?.clearValidators();
      this.invoiceActionForm?.get('lastNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('emailInvoice')?.clearValidators();
      this.invoiceActionForm?.get('emailInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('countryInvoice')?.clearValidators();
      this.invoiceActionForm?.get('countryInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('stateInvoice')?.clearValidators();
      this.invoiceActionForm?.get('stateInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('locationInvoice')?.clearValidators();
      this.invoiceActionForm?.get('locationInvoice')?.updateValueAndValidity();
    }
    else {
      this.invoiceActionForm?.get('firstNameInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('firstNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('lastNameInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('lastNameInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('emailInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('emailInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('countryInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('countryInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('stateInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('stateInvoice')?.updateValueAndValidity();
      this.invoiceActionForm?.get('locationInvoice')?.setValidators([Validators.required]);
      this.invoiceActionForm?.get('locationInvoice')?.updateValueAndValidity();
    }
  }

  setLocalStorage() {
    localStorage.setItem('valuationAmount', this.valuationAmount);
    localStorage.setItem('reservePrice', this.reservePrice);
    localStorage.setItem('assetId', this.equipment);
    localStorage.setItem('assetName', this.assetName);
    localStorage.setItem('yearOfMfg', this.yearOfMfg);
    localStorage.setItem('parkingCharges', this.parkingCharges);
    localStorage.setItem('tradeStatusType', this.statusType);
    localStorage.setItem('tradeAssetId', this.equipment);
  }

  rejectDOD() {
    this.dialogRef.close();
  }

  confirmDOD() {
    const customeDOD_Date = this.datePipe.transform(this.dateOfDeliveryForm?.get('dateOfDelivery')?.value, 'YYYY-MM-dd');
    const putDODUpdate = {
      amount: this.amount,
      equipment: this.equipment,
      asset_delivery_date: customeDOD_Date,
      offer_status: 'AD'
    };
    const putAssetUpdate = {
      status: 5
    };
    forkJoin([
      this.adminMasterService.puttradeBid(putDODUpdate, this.tradeId),
      this.adminMasterService.putUsedEquipmentById(putAssetUpdate, this.equipment)
    ]).subscribe((res: any) => {
      this.dialogRef.close();
      this.notify.success("Date of delivery confirm", true);
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  DeliveryDateSubmit() {
    this.isConfirming = true;
  }

  deleteFile(tab, key, imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        if (tab == 'Seller_Invoice') {
          this.sellerInvoiceForm?.get(key)?.setValue('');
        }
        else {
          this.doGenerationForm?.get(key)?.setValue('');
        }
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }

  getDocumentName(str) {
    return str.slice(str.lastIndexOf("/") + 1);
  }

  updateInvoice() {
    this.isSelfInvoice = this.invoiceActionForm?.get('isSelfInvoice')?.value == "1" ? true : false;
    var updateInvoiceDetail;
    if (this.isSelfInvoice) {
      updateInvoiceDetail = {
        amount: this.amount,
        equipment: this.equipment,
        buyer: this.buyer,
        is_self_invoice: this.isSelfInvoice,
        invoice_first_name: null,
        invoice_last_name: null,
        invoice_email: null,
        invoice_address: null,
        invoice_mobile: null,
        invoice_pin_code: null
      }
    }
    else {
      updateInvoiceDetail = {
        amount: this.amount,
        equipment: this.equipment,
        buyer: this.buyer,
        is_self_invoice: this.isSelfInvoice,
        invoice_first_name: this.invoiceActionForm?.get('firstNameInvoice')?.value,
        invoice_last_name: this.invoiceActionForm?.get('lastNameInvoice')?.value,
        invoice_email: this.invoiceActionForm?.get('emailInvoice')?.value,
        invoice_mobile: this.invoiceActionForm?.get('mobileNoInvoice')?.value
      }
    }
    this.adminMasterService.puttradeBid(updateInvoiceDetail, this.tradeId).subscribe((data: any) => {
      this.dialogRef.close('InvoiceUpdated');
      this.notify.success("Invoice Detail Updated", true);
    });
  }
}
