import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BidActionComponent } from './bid-action.component';

describe('BidActionComponent', () => {
  let component: BidActionComponent;
  let fixture: ComponentFixture<BidActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BidActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BidActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
