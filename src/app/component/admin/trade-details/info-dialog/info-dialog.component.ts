import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';

@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.css']
})
export class InfoDialogComponent implements OnInit {
  statusType: any;
  id: any;
  tableData: any = {};
  assetStatus?: string;
  isSelfInvoive: boolean = true;
  assetStatusObject = [{
    "id": 0,
    "display_name": "DRAFT"
  }, {
    "id": 1,
    "display_name": "PENDING"
  }, {
    "id": 2,
    "display_name": "APPROVED"
  }, {
    "id": 3,
    "display_name": "Sale in Progress"
  }, {
    "id": 4,
    "display_name": "REJECTED"
  }, {
    "id": 5,
    "display_name": "SOLD"
  }]

  constructor(
    public dialogRef: MatDialogRef<InfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private adminMasterService: AdminMasterService,
  ) {
    this.statusType = data.statusType;
    this.id = data.id;
    this.getDataByid();
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }

  getDataByid() {
    if (this.statusType === 'closed') {
      this.adminMasterService.getTradeBidDataById(this.id).subscribe((data: any) => {
        this.tableData = data;
        this.setassetStatus(this.tableData.equipment?.status);
        this.checkForSelfInvoice(this.tableData.is_self_invoice);
      });
    } else {
      this.adminMasterService.getTradeAssetDataById(this.id).subscribe((data: any) => {
        this.tableData = data;
        this.setassetStatus(this.tableData.status);
      });
    }
  }

  checkForSelfInvoice(data: any) {
    if (data) {
      this.isSelfInvoive = true;
    }
    else {
      this.isSelfInvoive = false;
    }
  }

  setassetStatus(statusId: any) {
    for (var i = 0; i < this.assetStatusObject.length; i++) {
      if (statusId == this.assetStatusObject[i].id) {
        this.assetStatus = this.assetStatusObject[i].display_name;
        break;
      }
    }
  }

  getFullAmount(data) {
    let amount = 0;
    for (var i = 0; i < data.length; ++i) {
      amount += parseInt(data[i].amount);
    }
    return amount;
  }

  getFileName(url) {
    return url.slice(url.lastIndexOf("/") + 1);
  }
  
  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realised'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }
}
