import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeAssetDetailsComponent } from './trade-asset-details.component';

describe('TradeAssetDetailsComponent', () => {
  let component: TradeAssetDetailsComponent;
  let fixture: ComponentFixture<TradeAssetDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeAssetDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeAssetDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
