import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { BidActionComponent } from '../bid-action/bid-action.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';

@Component({
    selector: 'app-trade-asset-details',
    templateUrl: './trade-asset-details.component.html',
    styleUrls: ['./trade-asset-details.component.css']
})
export class TradeAssetDetailsComponent implements OnInit {

    tradeApprovalData: any[] = [];
    tradeProgressData: any[] = [];
    tradeClosedData: any[] = [];
    assetStatusObject = [{
        "id": 0,
        "display_name": "DRAFT"
    }, {
        "id": 1,
        "display_name": "PENDING"
    }, {
        "id": 2,
        "display_name": "APPROVED"
    }, {
        "id": 3,
        "display_name": "Sale in Progress"
    }, {
        "id": 4,
        "display_name": "REJECTED"
    }, {
        "id": 5,
        "display_name": "SOLD"
    }];

    assetStatus?: string;
    public searchText!: string;
    public ordering: string = '-id';
    tradeApprovalDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeProgressDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    tradeClosedDataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
    public tradeApprovalListingData = [];
    public tradeProgressListingData = [];
    public tradeClosedListingData = [];
    approvalDisplayedColumns = ["id", "product_name", "tradeType", "no_of_bids", "max_bid", "pin_code", "status", "actions"];
    progressDisplayedColumns = ["id", "product_name", "tradeType", "no_of_bids", "max_bid", "pin_code", "status", "actions"];
    closedDisplayedColumns = ["id", "equipment__id", "equipment__product_name", "trade_type", "equipment__no_of_bids", "equipment__pin_code", "buyer__first_name", "buyer__pin_code", "buyer__mobile_number", "buyer__email", "equipment__selling_price", "emd_details__amount", "amount", "offer_status", "bid_status", "deal_status", "equipment__status", "do_document", "seller_invoice", "actions"];
    public selectedTabIndex: number = 0;
    public total_count: any;
    public page: number = 1;
    public pageSize: number = 10;
    public cognitoId?: string;
    userRole: string = "";
    @ViewChild(MatSort) sort!: MatSort;
  
    approvalScrolled: boolean = false;
    progressScrolled: boolean = false;
    closedScrolled: boolean = false;
    constructor(
        private dialog: MatDialog,
        private adminMasterService: AdminMasterService,
        private router: Router,
        public storage: StorageDataService,
        public sharedService: SharedService,
        public notify: NotificationService,
    ) {

    }

    ngOnInit() {
        this.cognitoId = this.storage.getStorageData('cognitoId', false);
        this.userRole = this.storage.getStorageData('userRole', false);
        this.getData();
    }

    getApprovedTradeAssetData() {
        let queryparams = `status__in=2&bid__bid_status__in=PA&bid__trade_type__in=BID`;
        let payloadClosedBuyer = {
            is_sell: true,
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize
        }
        if (this.userRole == 'ChannelPartner') {
            payloadClosedBuyer['role'] = this.userRole;
            payloadClosedBuyer['cognito_id'] = this.cognitoId;
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeApprovalData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeApprovalDataSource.data = data.results;
                } else {
                    this.approvalScrolled = false;
                    if (this.page == 1) {
                        this.tradeApprovalDataSource.data = [];
                        this.tradeApprovalListingData = [];
                        this.tradeApprovalListingData = data.results;
                        this.tradeApprovalDataSource.data = this.tradeApprovalListingData;
                    } else {
                        this.tradeApprovalListingData = this.tradeApprovalListingData.concat(data.results);
                        this.tradeApprovalDataSource.data = this.tradeApprovalListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err.message);
            }
        );
    }

    getProgessedAssetData() {
        let queryparams = `status__in=3`;
        let payloadClosedBuyer = {
            is_sell: true,
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize
        }
        if (this.userRole == 'ChannelPartner') {
            payloadClosedBuyer['role'] = this.userRole;
            payloadClosedBuyer['cognito_id'] = this.cognitoId;
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeProgressData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeProgressDataSource.data = data.results;
                } else {
                    this.progressScrolled = false;
                    if (this.page == 1) {
                        this.tradeProgressDataSource.data = [];
                        this.tradeProgressListingData = [];
                        this.tradeProgressListingData = data.results;
                        this.tradeProgressDataSource.data = this.tradeProgressListingData;
                    } else {
                        this.tradeProgressListingData = this.tradeProgressListingData.concat(data.results);
                        this.tradeProgressDataSource.data = this.tradeProgressListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err.message);
            }
        );
    }

    getClosedTradeAssetData() {
        let queryparams = `bid_status__in=CL&deal_status__in=CP&offer_status=[BR,RE]`;
        let payloadClosedBuyer = {
            ordering: this.ordering,
            page: this.page,
            limit: this.pageSize
        }
        if (this.userRole == 'ChannelPartner') {
            payloadClosedBuyer['role'] = this.userRole;
            payloadClosedBuyer['cognito_id'] = this.cognitoId;
        }
        if (this.searchText) {
            payloadClosedBuyer['search'] = this.searchText;
        }
        this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
            (data: any) => {
                this.tradeClosedData = data.results;
                this.total_count = data.count;
                if (window.innerWidth > 768) {
                    this.tradeClosedDataSource.data = data.results;
                } else {
                    this.closedScrolled = false;
                    if (this.page == 1) {
                        this.tradeClosedDataSource.data = [];
                        this.tradeClosedListingData = [];
                        this.tradeClosedListingData = data.results;
                        this.tradeClosedDataSource.data = this.tradeClosedListingData;
                    } else {
                        this.tradeClosedListingData = this.tradeClosedListingData.concat(data.results);
                        this.tradeClosedDataSource.data = this.tradeClosedListingData;
                    }
                }
            },
            err => {
                console.log(err);
                this.notify.error(err.message);
            }
        );
    }

    setassetStatus(objectStatus) {
        let statusObj = this.assetStatusObject.find(x => { return x.id == objectStatus });
        return statusObj?.display_name;
    }

    onPageChange(event: PageEvent) {
        this.page = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.getData();
    }

    bidAction(type: string, element: any) {
        const dialogRef = this.dialog.open(BidActionComponent, {
            width: '600px',
            data: {
                type: type,
                amount: element.amount,
                equipmentId: element.equipment.id,
                // buyer: this.cognitoId,
                id: element.id,
                valuationAmount: 0,
                reservePrice: 0,
                assetName: 0,
                yearOfMfg: 0,
                parkingCharges: 0,
                statusType: ''
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (type == "Invoice") {
                element.btnClicked = true;
            }
            if (result == "Sales Invoice Uploaded") {
                this.getClosedTradeAssetData();
            }
        });
    }

    onSortColumn(event, type: any) {
        this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
        this.getData();
        // if (type == "pendingSeller") {
        //     this.getApprovedTradeAssetData();
        // } else if (type == "progressSeller") {
        //     this.getProgessedAssetData()
        // } else if (type == "closedSeller") {
        //     this.getClosedTradeAssetData();
        // }
    }

    searchInList(type: string) {
        this.page = 1;
        this.pageSize = 10;
        if (this.searchText === '' || this.searchText.length > 3) {
            this.getData();
            // if (type == "pendingSeller") {
            //     this.getApprovedTradeAssetData();
            // } else if (type == "progressSeller") {
            //     this.getProgessedAssetData()
            // } else if (type == "closedSeller") {
            //     this.getClosedTradeAssetData();
            // }
        }

    }

    moreInfoDialog(statusType: string, id: any) {
        const dialogRef = this.dialog.open(InfoDialogComponent, {
            width: '800px',
            data: {
                statusType: statusType,
                id: id
            }
        });
    }

    viewBidDialog(statusType: string, id: any) {
        if (statusType == 'approved') {
            this.setDataForViewBid(this.tradeApprovalData, id);
        }
        else {
            this.setDataForViewBid(this.tradeProgressData, id);
        }
        localStorage.setItem('tradeStatusType', statusType);
        localStorage.setItem('tradeAssetId', id);
        this.router.navigate(['./admin-dashboard/trade/view-trade-details'])
    }

    getData() {
        switch (this.selectedTabIndex) {
            case 0: {
                this.getApprovedTradeAssetData();
                break;
            }
            case 1: {
                this.getProgessedAssetData();
                break;
            }
            case 2: {
                this.getClosedTradeAssetData();
                break;
            }
            default: {
                this.selectedTabIndex = 0;
                this.getData();
                break;
            }
        }
    }

    changeOnClickStatus(event: MatTabChangeEvent) {
        this.selectedTabIndex = event.index;
        this.ordering = '-id';
        this.page = 1;
        this.pageSize = 10;
        this.searchText = '';
        this.getData();
    }

    exportToExcelAccToPage() {
        this.exportFile(this.selectedTabIndex)
    }

    exportAllFiles() {
        for (var i = 0; i < 3; i++) {
            this.exportFile(i);
        }
    }

    allPaymentExport() {
        this.exportFile(2);
    }

    exportFile(val) {
        if (val == 0) {
            let queryparams = `status__in=2&bid__bid_status__in=PA&bid__trade_type__in=BID`;
            let payloadClosedBuyer = {
                limit: 999,
                is_sell: true
            }
            if (this.userRole == 'ChannelPartner') {
                payloadClosedBuyer['role'] = this.userRole;
                payloadClosedBuyer['cognito_id'] = this.cognitoId;
            }
            this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
                (res: any) => {
                    const excelData = res.results.map((r: any) => {
                        return {
                            "Asset ID": r?.id,
                            "Asset Name": (r?.category?.name == 'Other' ? r?.other_category : r?.category?.display_name) + ' ' + (r?.brand?.name == 'Other' ? r?.other_brand : r?.brand?.display_name) + ' ' + (r?.model?.name == 'Other' ? r?.other_model : r?.model?.name),
                            "Trade Type": r?.is_insta_sale ? 'Insta Sale' : 'Portal Sale',
                            "No. of Bids": r?.no_of_bids,
                            "Highest Bid": r?.max_bid,
                            'Asset Location': r?.pin_code?.city?.name,
                            "Asset Status": this.setassetStatus(r?.status)
                        }
                    });
                    ExportExcelUtil.exportArrayToExcel(excelData, "Pending for Approval Trade List");
                },
                err => {
                    console.log(err);
                    this.notify.error(err.message);
                }
            );
        }
        else if (val == 1) {
            let queryparams = `status__in=3`;
            let payloadClosedBuyer = {
                limit: 999,
                is_sell: true
            }
            if (this.userRole == 'ChannelPartner') {
                payloadClosedBuyer['role'] = this.userRole;
                payloadClosedBuyer['cognito_id'] = this.cognitoId;
            }
            this.adminMasterService.getTradeAssetDataWithPayload(queryparams, payloadClosedBuyer).subscribe(
                (res: any) => {
                    const excelData = res.results.map((r: any) => {
                        return {
                            "Asset ID": r?.id,
                            "Asset Name": (r?.category?.name == 'Other' ? r?.other_category : r?.category?.display_name) + ' ' + (r?.brand?.name == 'Other' ? r?.other_brand : r?.brand?.display_name) + ' ' + (r?.model?.name == 'Other' ? r?.other_model : r?.model?.name),
                            "Trade Type": r?.is_insta_sale ? 'Insta Sale' : 'Portal Sale',
                            "No. of Bids": r?.no_of_bids,
                            "Highest Bid": r?.max_bid,
                            'Asset Location': r?.pin_code?.city?.name,
                            "Asset Status": this.setassetStatus(r?.status)
                        }
                    });
                    ExportExcelUtil.exportArrayToExcel(excelData, "Sale In Progress Trade List");
                },
                err => {
                    console.log(err);
                    this.notify.error(err.message);
                }
            );
        }
        else if (val == 2) {
            let queryparams = `bid_status__in=CL&deal_status__in=CP&offer_status=[BR,RE]`;
            let payloadClosedBuyer = {
                limit: 999
            }
            if (this.userRole == 'ChannelPartner') {
                payloadClosedBuyer['role'] = this.userRole;
                payloadClosedBuyer['cognito_id'] = this.cognitoId;
            }
            this.adminMasterService.getTradeBidData(queryparams, payloadClosedBuyer).subscribe(
                (res: any) => {
                    const excelData = res.results.map((r: any) => {
                        return {
                            "Ticket Id": r?.id,
                            "Asset ID": r?.equipment?.id,
                            "Asset Name": (r?.equipment?.category?.name == 'Other' ? r?.equipment?.other_category : r?.equipment?.category?.display_name) + ' ' + (r?.equipment?.brand?.name == 'Other' ? r?.equipment?.other_brand : r?.equipment?.brand?.display_name) + ' ' + (r?.equipment?.model?.name == 'Other' ? r?.equipment?.other_model : r?.equipment?.model?.name),
                            "Trade Type": r?.trade_type == "BID" ? 'Portal Sale' : 'Insta Sale',
                            "No. of Bids": r?.equipment?.no_of_bids,
                            'Asset Location': r?.equipment?.pin_code?.city?.name,
                            "Buyer Name": r?.buyer?.first_name + ' ' + r?.buyer?.last_name,
                            "Buyer Location": r?.buyer?.pin_code?.city?.state?.country?.name,
                            "Buyer Mobile No.": r?.buyer?.mobile_number,
                            "Buyer Email Id": r?.buyer?.email,
                            "Selling Price": r?.equipment?.selling_price,
                            "EMD Amount": (r?.emd_details == null) ? 0 : r?.emd_details?.amount + (r?.emd_details?.amount_type == 2) ? '%' : '',
                            "Trade Amount": r?.amount,
                            "Offer Status": r?.offer_status_display_name,
                            "Trade Status": r?.bid_status_display_name,
                            "Deal Status": r?.deal_status_display_name,
                            "Asset Status": this.setassetStatus(r?.equipment?.status),
                            "Delivery Order": r?.do_document,
                            "Sales Invoice": r?.seller_invoice
                        }
                    });
                    ExportExcelUtil.exportArrayToExcel(excelData, "Closed Trade List");
                },
                err => {
                    console.log(err);
                    this.notify.error(err.message);
                }
            );
        }
    }

    setDataForViewBid(data: any, id: number) {
        let dataObject = data.filter(e => e.id == id);
        let valuationAmount = dataObject[0].valuation_amount == null ? 0 : dataObject[0].valuation_amount;
        localStorage.setItem('valuationAmount', valuationAmount);
        let reservePrice = dataObject[0].reserve_price == null ? 0 : dataObject[0].reserve_price;
        localStorage.setItem('reservePrice', reservePrice);
        let assetId = dataObject[0].id;
        localStorage.setItem('assetId', assetId);
        let assetName = (dataObject[0]?.category?.name == 'Other' ? dataObject[0]?.other_category : dataObject[0]?.category?.display_name) + ' ' + (dataObject[0]?.brand.name == 'Other' ? dataObject[0]?.other_brand : dataObject[0]?.brand.display_name) + ' ' + (dataObject[0]?.model?.name == 'Other' ? dataObject[0]?.other_model : dataObject[0]?.model?.name);
        localStorage.setItem('assetName', assetName);
        let yearOfMfg = dataObject[0].mfg_year == "" || dataObject[0].mfg_year == null ? 'N/A' : dataObject[0].mfg_year;
        localStorage.setItem('yearOfMfg', yearOfMfg);
        let parkingCharges = dataObject[0].parking_charge_per_day == null ? 'N/A' : dataObject[0].parking_charge_per_day;
        localStorage.setItem('parkingCharges', parkingCharges);
    }

    @HostListener('window:scroll', ['$event'])
    recursiveNewsApiHit = (event) => {
        if (window.innerWidth < 768) {
            if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                if (this.selectedTabIndex == 0) {
                    if ((this.total_count > this.tradeApprovalDataSource.data.length) && (this.tradeApprovalDataSource.data.length > 0)) {
                        if (!this.approvalScrolled) {
                            this.approvalScrolled = true;
                            this.page++;
                            this.getApprovedTradeAssetData();
                        }
                    }
                }
                else if (this.selectedTabIndex == 1) {
                    if ((this.total_count > this.tradeProgressDataSource.data.length) && (this.tradeProgressDataSource.data.length > 0)) {
                        if (!this.progressScrolled) {
                            this.progressScrolled = true;
                            this.page++;
                            this.getProgessedAssetData();
                        }
                    }
                }
                else {
                    if ((this.total_count > this.tradeClosedDataSource.data.length) && (this.tradeClosedDataSource.data.length > 0)) {
                        if (!this.closedScrolled) {
                            this.closedScrolled = true;
                            this.page++;
                            this.getClosedTradeAssetData();
                        }
                    }
                }
            }
        }
    }

}
