import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { AdminMasterService } from "src/app/services/admin-master.service";
import { InfoDialogComponent } from "./info-dialog/info-dialog.component";
import { ViewBidComponent } from "./view-bid/view-bid.component";

@Component({
    selector: 'app-trade-details-dashboard',
    templateUrl: './trade-details-dashboard.component.html',
    styleUrls: ['./trade-details-dashboard.component.css'],
})

export class TradeDetailsDashboardComponent implements OnInit {


    constructor() {

    }

    ngOnInit() {

    }
}