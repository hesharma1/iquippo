import { Component, OnInit, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BidActionComponent } from '../bid-action/bid-action.component';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { OfflinePaymentComponent } from '../../offline-payment/offline-payment.component';

@Component({
  selector: 'app-view-bid',
  templateUrl: './view-bid.component.html',
  styleUrls: ['./view-bid.component.css']
})
export class ViewBidComponent implements OnInit, OnDestroy {

  @ViewChild("approvalPaginator") set approvalPaginator(pager: MatPaginator) {
    if (pager) this.tradeApprovalDataSource.paginator = pager;
  }
  @ViewChild("progressPaginator") set progressPaginator(pager: MatPaginator) {
    if (pager) this.tradeProgressDataSource.paginator = pager;
  }

  @ViewChild(MatSort) sort!: MatSort;

  statusType?: string;
  id?: number;
  tradeApprovalData: any[] = [];
  tradeProgressData: any[] = [];
  showingData: any[] = [];
  tradeApprovalDataSource = new MatTableDataSource<any>([]);
  tradeProgressDataSource = new MatTableDataSource<any>([]);
  assetId?: string;
  assetName?: string;
  reservePrice?: any;
  yearOfMfg?: string;
  ageingOfAsset?: string;
  ageingOfPortal?: string;
  valuationAmount?: any;
  parkingCharges?: any;
  cognitoId?: string;
  isFullPaymentButtonActive: boolean = false;
  showInvoice: boolean = false;
  isPaymentDone: boolean = false;
  isDateOfDelivery: boolean = false;
  approvedBuyerOrdering: string = 'id';
  searchText!: string;
  isBidApproved: boolean = false;
  isSellerInvoice: boolean = true;
  assetStatus?: string;
  tradeClosedData: any[] = [];
  tradeClosedDataSource = new MatTableDataSource<any>([]);
  userRole: any;
  assetStatusObject = [
    {
      "id": 0,
      "display_name": "DRAFT"
    }, 
    {
      "id": 1,
      "display_name": "PENDING"
    }, 
    {
      "id": 2,
      "display_name": "APPROVED"
    }, 
    {
      "id": 3,
      "display_name": "Sale in Progress"
    }, 
    {
      "id": 4,
      "display_name": "REJECTED"
    }, 
    {
      "id": 5,
      "display_name": "SOLD"
    }
  ];

  approvalDisplayedColumns = ["id", "sellerName", "seller_mobile_number", "buyerName", "buyerMobileNo", "buyerEmailId", "bidAmounnt", "tradeType","offer_status", "bidStatus", "dealStatus", "delivery_Order", "asset_Status", "bid_Date", "in_Cooling_Period", "actions"];
  progressDisplayedColumns = ["id", "sellerName", "seller_mobile_number", "buyerName", "buyerMobileNo", "buyerEmailId", "bidAmounnt", "tradeType","offer_status", "bidStatus", "dealStatus", "EMD_Details", "EMD_Due_Date", "full_Payment_due_date", "invoice_In_Favour_Of", "full_Payment_Details", "delivery_Order", "asset_Status", "bid_Date", "actions"];

  constructor(private router: Router, private dialog: MatDialog, public dialogRef: MatDialogRef<InfoDialogComponent>, @Inject(MAT_DIALOG_DATA) data: any, private adminMasterService: AdminMasterService, private route: ActivatedRoute, public apiService: UsedEquipmentService, public storage: StorageDataService, public notify: NotificationService) {

    let cogId = localStorage.getItem('cognitoId');
    if (cogId != '' && cogId != undefined) {
      this.cognitoId = cogId.replace(/['"]+/g, '');
    }
    let status = localStorage.getItem('tradeStatusType');
    let id = localStorage.getItem('tradeAssetId');
    this.valuationAmount = localStorage.getItem('valuationAmount');
    this.reservePrice = localStorage.getItem('reservePrice');
    this.assetId = localStorage.getItem('assetId')?.toString();
    this.assetName = localStorage.getItem('assetName')?.toString();
    this.yearOfMfg = localStorage.getItem('yearOfMfg')?.toString();
    this.parkingCharges = localStorage.getItem('parkingCharges');
    if (status != null) {
      this.statusType = status;
    }
    if (id != null) {
      this.id = parseInt(id);
    }
    this.getBidList();
  }

  ngOnInit(): void {
    this.userRole = this.storage.getStorageData('userRole', false);
  }

  backClick() {
    this.router.navigate(['./admin-dashboard/trade/trade-details-dashboard']);
  }

  moreInfoDialog(statusType: string, id: any) {
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      width: '800px',
      data: {
        statusType: statusType,
        id: id
      }
    });
  }

  bidAction(type: string, element: any) {
    const dialogRef = this.dialog.open(BidActionComponent, {
      width: '600px',
      data: {
        type: type,
        mainData: element,
        amount: element.amount,
        equipmentId: element.equipment.id,
        buyer: this.cognitoId,
        id: element.id,
        valuationAmount: this.valuationAmount,
        reservePrice: this.reservePrice,
        assetName: this.assetName,
        yearOfMfg: this.yearOfMfg,
        parkingCharges: this.parkingCharges,
        statusType: this.statusType
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (type == "Invoice") {
        this.isFullPaymentButtonActive = true;
      }
      if (result == "InvoiceUpdated") {
        element.btnClicked = true;
      }
      if (result == 'DO Issued') {
        this.isSellerInvoice = false;
        this.router.navigate(['/admin-dashboard/trade/trade-details-dashboard']);
      }
      this.getBidList();
    });
  }

  getBidList() {
    if (this.statusType == "approved") {
      this.getApprovedTradeAssetData();
    }
    else if (this.statusType == "progress") {
      this.getProgessedAssetData();
    }
  }

  setassetStatus(Object: any) {
    var statusId = Object.equipment.status;
    for (var i = 0; i < this.assetStatusObject.length; i++) {
      if (statusId == this.assetStatusObject[i].id) {
        this.assetStatus = this.assetStatusObject[i].display_name;
        break;
      }
    }
    return this.assetStatus;
  }

  isBidClosed(object: any) {
    let bidStatus = object.bid_status_display_name;
    let isActionbutton = true;
    if (bidStatus == "Sale in Progress" || bidStatus == "Closed") {
      this.isBidApproved = true;
      isActionbutton = false;
    }
    else if (bidStatus == "Pending for Approval") {
      this.isBidApproved = false;
    }
    return isActionbutton;
  }

  getApprovedTradeAssetData() {
    let queryparams = `equipment=${this.id}`;
    let payloadApprovedBuyer = {
      ordering: this.approvedBuyerOrdering
    }
    if (this.searchText) {
      payloadApprovedBuyer['search'] = this.searchText;
    }
    this.adminMasterService.getTradeBidData(queryparams, payloadApprovedBuyer).subscribe((data: any) => {
      this.tradeApprovalData = data.results;
      this.tradeApprovalDataSource = new MatTableDataSource<any>(this.tradeApprovalData);
    });
  }

  getProgessedAssetData() {
    let queryparams = `equipment=${this.id}&bid_status__in=SP`;
    let payloadApprovedBuyer = {
      ordering: this.approvedBuyerOrdering
    }
    if (this.searchText) {
      payloadApprovedBuyer['search'] = this.searchText;
    }
    this.adminMasterService.getTradeBidData(queryparams, payloadApprovedBuyer).subscribe((data: any) => {
      this.tradeProgressData = data.results;
      this.tradeProgressDataSource = new MatTableDataSource<any>(this.tradeProgressData);
    });
  }

  onSortColumn(event, type: any) {
    this.approvedBuyerOrdering = (event.direction == "asc") ? event.active : ("-" + event.active);
    if (type == "progressSeller") {
      this.getProgessedAssetData()
    }
    else if (type == "pendingSeller") {
      this.getApprovedTradeAssetData();
    }
  }

  searchInList(type: string) {
    if (this.searchText === '' || this.searchText.length > 3) {
      if (type == "progressSeller") {
        this.getProgessedAssetData()
      }
      else if (type == "pendingSeller") {
        this.getApprovedTradeAssetData();
      }
    }
  }

  setInvoicePerson(element: any) {
    let nameShow;
    if (element.is_self_invoice) {
      nameShow = "Self";
    }
    else {
      nameShow = element.invoice_first_name + " " + element.invoice_last_name;
    }
    return nameShow;
  }

  updateFullPayment(element) {
    let payload = {
      bid_id: element?.id
    }
    this.adminMasterService.getTradeBidPaymentDetails(payload).subscribe(
      res => {
        const dialogRef = this.dialog.open(OfflinePaymentComponent, {
          width: '800px',
          disableClose: true,
          autoFocus: true,
          panelClass: 'my-class',
          data: {
            case: 'trade',
            paymentDetails: res,
            bid_details: element
          }
        });
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    )
  }

  ngOnDestroy() {
    localStorage.removeItem('tradeAssetId');
    localStorage.removeItem('tradeStatusType');
    localStorage.removeItem('valuationAmount');
    localStorage.removeItem('reservePrice');
    localStorage.removeItem('assetId');
    localStorage.removeItem('assetName');
    localStorage.removeItem('yearOfMfg');
    localStorage.removeItem('parkingCharges');
  }
}


