import { DatePipe } from '@angular/common';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/services/common.service';
import { SharedService } from 'src/app/services/shared-service.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ValuationService } from 'src/app/services/valuation.service';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { GenerateReportComponent } from '../generate-report/generate-report.component';
import { ViewReportComponent } from '../view-report/view-report.component';

@Component({
  selector: 'app-contact-us-list',
  templateUrl: './contact-us-list.component.html',
  styleUrls: ['./contact-us-list.component.css']
})
export class ContactUsListComponent implements OnInit {@ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
@ViewChild(MatSort, { static: true }) sort!: MatSort;
public listingData = [];
public searchText!: string;
public activePage: any = 1;
public total_count: any;
public dataSource!: MatTableDataSource<any>;
public ordering: string = "-id"
public pageSize: number = 10;
public filter: any;
public displayedColumns: string[] = [
  "id","full_name","email","country_code","mobile_number",
  "location","comments","created_at"
];

constructor(private dialog: MatDialog,
  public spinner: NgxSpinnerService,
  public valService: ValuationService,
  public cmnService: CommonService,
  public storage: StorageDataService, public notify: NotificationService,
  private sharedService: SharedService,
  private datePipe: DatePipe) {
}

/**ng on init */
ngOnInit(): void {
  this.dataSource = new MatTableDataSource();
  this.getPage();
}

getAssetCondition(condition) {
  if (condition == 'GD') {
    return "Good"
  } else if (condition == 'EX') {
    return "Excellent"
  } else if (condition == 'AVG') {
    return "Average"
  } else if (condition == 'SCR') {
    return "Scrap"
  }
    else if (condition == 'POOR'){
      return "Poor"
    }
    else if (condition == 'VGD'){
      return "Very Good"
    }
  return "";
}

/**search list  */
searchInList() {
  if (this.searchText === '' || this.searchText?.length > 3) {
    this.activePage = 1;
    this.getPage();
  }
}

statusFilter(val) {
  this.filter = val;
  this.activePage = 1;
  this.getPage();
}

/**get list from api */
getPage() {
  let payload = {
    page: this.activePage,
    limit: this.pageSize,
    ordering: this.ordering,
  };
  if (this.searchText) {
    payload['search'] = this.searchText.toString();
  }
  if (this.filter) {
    payload['status__in'] = this.filter;
  }
  this.cmnService.getContactUs(payload).pipe(finalize(() => {  })).subscribe(
    (res: any) => {
      if (res) {
        this.total_count = res.count;
        if (window.innerWidth > 768) {
          this.dataSource.data = res.results;
        } else {
          this.scrolled = false;
          if (this.activePage == 1) {
            this.dataSource.data = [];
            this.listingData = [];
            this.listingData = res.results;
            this.dataSource.data = this.listingData;
          } else {
            this.listingData = this.listingData.concat(res.results);
            this.dataSource.data = this.listingData;
          }
        }
      }
    },
    err => {
      console.log(err);
      this.notify.error(err?.message);
    }
  )
}

/**on chnage page from paginations */
onPageChange(event: PageEvent) {
  this.activePage = event.pageIndex + 1;
  this.pageSize = event.pageSize;
  this.getPage();
}

onSortColumn(event) {
  this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
  this.getPage();
}

/**Export Function */
export(string) {
  let payload = {
    limit: 999
  }
  if(string != 'all'){
    if (this.filter) {
      payload['status__in'] = this.filter;
    }
  }
  this.cmnService.getContactUs(payload).pipe(finalize(() => {  })).subscribe(
    (res: any) => {
      const excelData = res.results.map((r: any) => {
        return {
          "ID": r?.id,
          "Full Name": r?.full_name,
          "Country Code":r?.country_code,
          "Mobile Number":r?.mobile_number,
          "Location":r?.location,
          "Comments": r?.comments,
          "Created At": this.datePipe.transform(r?.created_at, 'dd-mm-yyyy, hh:mm a'),
        }
      });
      ExportExcelUtil.exportArrayToExcel(excelData, "Contact Us List");
    },
    err => {
      console.log(err);
      this.notify.error(err.message);
    }
  );
}
scrolled = false;
@HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
          if(!this.scrolled){
            this.scrolled = true;
          this.activePage++;
          this.getPage();
          }
        }
      }
    }
  }

  
}