import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutAdminPageComponent } from './about-admin-page.component';

describe('AboutAdminPageComponent', () => {
  let component: AboutAdminPageComponent;
  let fixture: ComponentFixture<AboutAdminPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutAdminPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
