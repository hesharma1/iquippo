import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { AuctionService } from 'src/app/services/auction.service';
import { forkJoin } from 'rxjs';
import { UsedEquipmentService } from 'src/app/services/used-equipment.service';

@Component({
  selector: 'app-assets-auction',
  templateUrl: './assets-auction.component.html',
  styleUrls: ['./assets-auction.component.css']
})
export class AssetsAuctionComponent implements OnInit {
  @Output() closeForm = new EventEmitter();
  @Input() editData: any;
   
  public auctionList: Array<any> = [];
  public assetsList: Array<any> = [];
  public lotArray: Array<any> = [];
  public lotAssetForm: FormGroup;
  public toEdit: boolean = false;
  public assetLotData: any;

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public auctionService: AuctionService, private notify: NotificationService, private usedProductService: UsedEquipmentService) {
    this.lotAssetForm = this.fb.group({
      lot: ['', Validators.required],
      assets: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails() {
    forkJoin([this.auctionService.getAuctionList({ limit: 999, parent_auction__id__isnull: true, ends_at__gt: (new Date()).toISOString() }), this.usedProductService.getAllProducts("limit=999&status__in=2&is_auction=true&ordering=-created_at")]).subscribe(
      (res: any) => {
        this.auctionList = res[0].results;        
        this.assetsList = res[1].results;
      },
      err => {
        console.log(err);
        this.notify.error(err?.message, true);
      }
    )
  }

  getLotList(id){
    this.lotArray = [];
    this.auctionService.getLotAuctionByIdList(id).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.lotArray = res.results;
      },
      err => {
        console.log(err);
        this.notify.error(err?.message, true);
      }
    )
  }

  submit(value) {
    let payload = Object.assign({}, value);
    this.auctionService.createAssetLot(payload).pipe(finalize(() => {   })).subscribe(
      data => {
        this.notify.success('Assets successfully tagged with lot!!');
        let obj = {
          close: true,
          formsubmission: true
        }
        this.closeForm.emit(obj);
      },
      err => {
        console.log(err);
      }
    )
  }

  setData(data) {
    this.lotAssetForm.setValue({
      lot: data?.lot,
      assets: data?.assets
    })
  }

  close() {
    let obj = {
      close: true,
      formsubmission: false
    }
    this.toEdit = false;
    this.closeForm.emit(obj);
  }

}
