import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-payment-info-dialog',
  templateUrl: './payment-info-dialog.component.html',
  styleUrls: ['./payment-info-dialog.component.css']
})

export class PaymentInfoDialogComponent implements OnInit {

  public realizationDetails: any;

  constructor(public dialogRef: MatDialogRef<PaymentInfoDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.realizationDetails = this.data.realizationDetails;
  }

  backClick() {
    this.dialogRef.close();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      case 6: {
        return 'Registration Cancelled'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }

}