import { Component, ViewChild, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from '@angular/material/dialog';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { NotificationService } from "src/app/utility/toastr-notification/toastr-notification.service";
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { AuctionService } from 'src/app/services/auction.service';

@Component({
  selector: 'app-auction-listing',
  templateUrl: './auction-listing.component.html',
  styleUrls: ['./auction-listing.component.css']
})
export class AuctionListingComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public searchText!: string;
  public selectedTabIndex: number = 0;
  public addNewAssetLot: boolean = false;
  public editAssetLotData: any;
  public displayedColumns = ["id", "name", "auction_engine_auction_id", "starts_at", "ends_at", "city", "auction_type_display_name", "category_type_display_name", "business_vertical", "sale_type", "auction_status_display_name"];
  public auctionLotColumns = ["auction", "parent_auction", "lot_number", "start_price", "reserve_price", "last_minute_bid", "extended_to", "bid_increment_type", "starts_at", "ends_at"];
  public assetAuctionColumns = ["id", "lot__auction__id", "lot__lot_number", "asset__id", "asset__category__display_name", "asset__brand__display_name", "asset__model__name", "asset__asset_description", "lot__date_of_invoice", "asset__rc_number", "asset__engine_number", "asset__location__location", "asset__is_invoice_available", "asset__seller__first_name", "asset__is_sell", "asset__selling_price"];
  public auctionListFilter = {
    sale_type__in: '',
    auction_type__in: '',
    auction_status__in: '',
    business_vertical__in: '',
    category_type__in: ''
  }
  public bidIncrementFilter;
  public saleTypeFilterOptions: Array<any> = [
    { name: 'Online Auction', value: "OL" },
    { name: 'Reverse Auction', value: "RV" },
    { name: 'Live Auction', value: "LV" },
    { name: 'Private Treaty', value: "PT" }
  ];
  public auctionTypeFilterOptions: Array<any> = [
    { name: 'Sequential', value: "SQ" },
    { name: 'Concurrent', value: "CT" }
  ];
  public auctionStatusFilterOptions: Array<any> = [
    { name: 'Private', value: "PR" },
    { name: 'Public', value: "PB" }
  ];
  public buisnessVerticalFilterOptions: Array<any> = [
    { name: 'Industrial', value: "IN" },
    { name: 'Construction Equipment', value: "CE" },
    { name: 'Asset Based Lending', value: "AB" },
    { name: 'Other', value: "OT" }
  ];
  public categoryTypeFilterOptions: Array<any> = [
    { name: 'Normal Auction', value: "NM" },
    { name: 'Bid Limit Auction', value: "BL" }
  ];
  public bidIncrementFilterOptions: Array<any> = [
    { name: 'Static', value: "ST" },
    { name: 'Bid Range', value: "BR" }
  ];
  public auctionListingData = [];
  public lotListingData = [];
  public assetsListingData = [];
  constructor(private route: ActivatedRoute, public router: Router, private auctionService: AuctionService, public dialog: MatDialog, public spinner: NgxSpinnerService, public notify: NotificationService, public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.getData();
  }

  onTabChanged(event) {
    this.selectedTabIndex = event.index;
    this.page = 1;
    this.pageSize = 10;
    this.searchText = '';
    switch (this.selectedTabIndex) {
      case 0: {
        this.ordering = '-id';
        break;
      }
      case 1: {
        this.ordering = '-auction';
        break;
      }
      case 2: {
        this.ordering = '-id';
        break;
      }
      default: {
        this.ordering = '-id';
        break;
      }
    }
    this.dataSource.data = [];
    this.getData();
  }

  onPageChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getData();
  }

  onSortColumn(event) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    this.getData();
  }

  searchInList() {
    if (this.searchText === '' || this.searchText?.length > 1) {
      this.page = 1;
      this.pageSize = 10;
      this.getData();
    }
  }

  getData() {
    switch (this.selectedTabIndex) {
      case 0: {
        this.getAuctionList();
        break;
      }
      case 1: {
        this.getAuctionLotList();
        break;
      }
      case 2: {
        this.getAuctionAssetsList();
        break;
      }
      default: {
        this.getAuctionList();
        break;
      }
    }
  }

  getAuctionList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      parent_auction__id__isnull: true
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    let filterObj = Object.assign({}, this.auctionListFilter);
    for (let x in filterObj) {
      if (filterObj[x] == '') {
        delete filterObj[x];
      }
    }
    payload = { ...payload, ...filterObj };
    this.auctionService.getAuctionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.dataSource.data = [];
              this.auctionListingData = [];
              this.auctionListingData = res.results;
              this.dataSource.data = this.auctionListingData;
            } else {
              this.auctionListingData = this.auctionListingData.concat(res.results);
              this.dataSource.data = this.auctionListingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  getAuctionLotList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    if (this.bidIncrementFilter) {
      payload['lotbidincrement__increment_type__in'] = this.bidIncrementFilter;
    }
    this.auctionService.getAuctionLotList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
          } else {
            this.scrolledLot = false;
            if (this.page == 1) {
              this.dataSource.data = [];
              this.lotListingData = [];
              this.lotListingData = res.results;
              this.dataSource.data = this.lotListingData;
            } else {
              this.lotListingData = this.lotListingData.concat(res.results);
              this.dataSource.data = this.lotListingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  getAuctionAssetsList() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering
    };
    if (this.searchText) {
      payload['search'] = this.searchText;
    }
    this.auctionService.getAuctionAssetsList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
         
          this.total_count = res.count;
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
          } else {
            if (this.page == 1) {
              this.dataSource.data = [];
              this.assetsListingData = [];
              this.assetsListingData = res.results;
              this.dataSource.data = this.assetsListingData;
            } else {
              this.assetsListingData = this.assetsListingData.concat(res.results);
              this.dataSource.data = this.assetsListingData;
            }
          }
        }
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  getSaleType(val) {
    switch (val) {
      case "OL": {
        return 'Online Auction';
        break;
      }
      case "RV": {
        return 'Reverse Auction';
        break;
      }
      case "LV": {
        return 'Live Auction';
        break;
      }
      case "PT": {
        return 'Private Treaty';
        break;
      }
      default: {
        return 'Online Auction';
        break;
      }
    }
  }

  getBuisnessVertical(val) {
    switch (val) {
      case 'IN': {
        return 'Industrial';
        break;
      }
      case 'CE': {
        return 'Construction Equipment';
        break;
      }
      case 'AB': {
        return 'Asset Based Lending';
        break;
      }
      case 'OT': {
        return 'Other';
        break;
      }
      default: {
        return 'Other';
        break;
      }
    }
  }

  applyAuctionFilter(type, val) {
    this.page = 1;
    switch (type) {
      case "sale": {
        this.auctionListFilter.sale_type__in = val;
        this.getAuctionList();
        break;
      }
      case "auction": {
        this.auctionListFilter.auction_type__in = val;
        this.getAuctionList();
        break;
      }
      case "buisness": {
        this.auctionListFilter.business_vertical__in = val;
        this.getAuctionList();
        break;
      }
      case "status": {
        this.auctionListFilter.auction_status__in = val;
        this.getAuctionList();
        break;
      }
      case "category": {
        this.auctionListFilter.category_type__in = val;
        this.getAuctionList();
        break;
      }
      case "bid": {
        this.bidIncrementFilter = val;
        this.getAuctionLotList();
        break;
      }
      default: {
        this.getAuctionList();
        break;
      }
    }
  }

  editAuction(id) {
    this.router.navigate(['/admin-dashboard/auction/create-auction'], { queryParams: { id: id } })
  }

  editLot(id) {
    this.router.navigate(['/admin-dashboard/auction/create-lot'], { queryParams: { id: id } });
  }

  editAssetLot(data) {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.editAssetLotData = data;
    this.addNewAssetLot = true;
  }

  formClose(e: any) {
    if (e && e.formsubmission) {
      this.getData();
    }
    this.editAssetLotData = null;
  }

  exportAuction() {
    let payload = {
      limit: 999
    }
    this.auctionService.getAuctionList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Auction Name": r?.name,
            "Auction Engine Auction Id": r?.auction_engine_auction_id,
            "Auction Start Date & Time": this.datePipe.transform(r?.starts_at, 'dd-MM-yyyy, h:mm a'),
            "Auction End Date & Time": this.datePipe.transform(r?.ends_at, 'dd-MM-yyyy, h:mm a'),
            "Location": r?.city,
            "Auction Type": r?.auction_type_display_name,
            "Category Type": r?.category_type_display_name,
            "Business Vertical": this.getBuisnessVertical(r?.business_vertical),
            "Auction Sale Type": this.getSaleType(r?.sale_type),
            "Auction Status": r?.auction_status_display_name,
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Auction List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  exportAuctionLot() {
    let payload = {
      limit: 999
    }
    this.auctionService.getAuctionLotList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Auction ID": r?.auction,
            "Parent Auction ID": r?.parent_auction,
            "Lot Number": r?.lot_number,
            "Start Price": r?.start_price,
            "Reserve Price": r?.reserve_price,
            "Last Minute Bid": r?.last_minute_bid,
            "Extended To": r?.extended_to,
            "Bid Increment Type": r?.bid_increments ? r?.bid_increments[0]?.increment_type_display_name : '',
            "Start Date": this.datePipe.transform(r?.starts_at, 'dd-MM-yyyy, h:mm a'),
            "End Date": this.datePipe.transform(r?.ends_at, 'dd-MM-yyyy, h:mm a'),
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Auction Lots List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  exportAuctionAssetsLot() {
    let payload = {
      limit: 999
    }
    this.auctionService.getAuctionAssetsList(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "ID": r?.id,
            "Auction ID": r?.lot?.auction?.id,
            "Lot No": r?.lot?.lot_number,
            "Asset ID": r?.asset?.id,
            "Category": r?.asset?.category?.display_name,
            "Brand": r?.asset?.brand?.display_name,
            "Model": r?.asset?.model?.name,
            "Asset Desc": r?.asset?.asset_description,
            "Invoice Date": r?.lot?.date_of_invoice,
            "Asset Registration No": r?.asset?.rc_number,
            "Engine No": r?.asset?.engine_number,
            "Asset Location": r?.asset?.location?.location,
            "Original Invoice": r?.asset?.is_invoice_available ? 'Yes' : 'No',
            "Contact Person": r?.asset?.seller?.first_name,
            "Contact No": r?.asset?.seller?.mobile_number,
            "Sold": r?.asset?.is_sell ? 'Yes' : 'No',
            "Sale Value": r?.asset?.selling_price
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Auction Assets List");
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    );
  }

  deleteAuction(id: number) {
    this.auctionService.deleteAuction(id).pipe(finalize(() => { })).subscribe(
      data => {
        this.notify.success('Auction deleted successfully!!');
        this.getAuctionList();
      },
      err => {
        console.log(err);
      }
    )
  }

  deleteAuctionLot(id: number) {
    this.auctionService.deleteAuctionLot(id).pipe(finalize(() => { })).subscribe(
      data => {
        this.notify.success('Auction Lot deleted successfully!!');
        this.getAuctionLotList();
      },
      err => {
        console.log(err);
      }
    )
  }

  deleteAuctionAssestLot(id: number) {
    this.auctionService.deleteAuctionAssetsLot(id).pipe(finalize(() => { })).subscribe(
      data => {
        this.notify.success('Auction Asset deleted successfully!!');
        this.getAuctionLotList();
      },
      err => {
        console.log(err);
      }
    )
  }
  scrolled = false;
  scrolledLot = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if ((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) {
        if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {

          if (this.selectedTabIndex == 0) {
            if(!this.scrolled){
              this.scrolled = true;
            this.page++;
            this.getAuctionList();
            }
          } else if (this.selectedTabIndex == 1) {
            if(!this.scrolledLot){
              this.scrolledLot = true;
            this.page++;
            this.getAuctionLotList();
            }
          }
          else {
            this.page++;
            this.getAuctionAssetsList();
          }

        }
      }
    }
  }


}
