import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { DatePipe } from '@angular/common';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuctionService } from 'src/app/services/auction.service';
import { SharedService } from 'src/app/services/shared-service.service';
import * as XLSX from 'xlsx';
import { ModelBulkErrorListComponent } from '../../new-equipment-master/master-data/master-data.component';
import { AuctionFulfilmentInfoDialogComponent } from '../auction-fulfilment-info-dialog/auction-fulfilment-info-dialog.component';
import { SigninComponent } from 'src/app/component/signin/signin.component';
import { BidActionComponent } from 'src/app/component/customer/dashboard/my-trade/seller/bid-action/bid-action.component';
import { ViewAllIdsComponent } from 'src/app/component/shared/view-all-ids/view-all-ids.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OfflinePaymentComponent } from '../../offline-payment/offline-payment.component';

@Component({
  selector: 'app-auction-fulfilment',
  templateUrl: './auction-fulfilment.component.html',
  styleUrls: ['./auction-fulfilment.component.css']
})

export class AuctionFulfilmentComponent implements OnInit {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('fileInput') fileInput!: ElementRef;

  public page: number = 1;
  public pageSize: number = 10;
  public ordering: string = '-id';
  public total_count: any;
  public displayedColumns: string[] = ["select",
    "id",
    "lot_bid__lot__auction",
    "parent_lot_bid__lot__auction",
    "lot_bid__lot__lot_number",
    "lot_bid__lot__description",
    "lot_bid__user__first_name",
    "lot_bid__user__mobile_number",
    "lot_bid__user__email",
    "lot_bid__amount",
    "lot_bid__lot__auction__buyers_premium",
    "lot_bid__status",
    "lot_bid__deal_status",
    "lot_bid__lot__auction__payment_date",
    "created_at",
    "lot_bid__user__relationship_manager__user__first_name",
    "Asset_Id",
    "Full_Payment_Details",
    "Invoice_in_Favour_of",
    "Delivery_Order",
    "action"
  ];
  public CloseBidDisplayedColumns: string[] = [
    "id",
    "lot_bid__lot__auction",
    "parent_lot_bid__lot__auction",
    "lot_bid__lot__lot_number",
    "lot_bid__lot__description",
    "lot_bid__user__first_name",
    "lot_bid__user__mobile_number",
    "lot_bid__user__email",
    "lot_bid__amount",
    "lot_bid__lot__auction__buyers_premium",
    "lot_bid__status",
    "lot_bid__deal_status",
    "lot_bid__lot__auction__payment_date",
    "created_at",
    "lot_bid__user__relationship_manager__user__first_name",
    "Asset_Id",
    "Full_Payment_Details",
    "Delivery_Order",
    "invoice_url",
    "Delivery_date",
    "action"
  ];
  public BidStatus: Array<any> = [
    { name: 'ACCEPTED', value: 'AC' },
    { name: 'REJECTED', value: 'CL' },
  ];
  public DealStatus: Array<any> = [
    { name: 'EMD Received', value: 'ER' },
    { name: 'DO Issued', value: 'DI' },
    { name: 'Closed', value: "CL" },
    { name: 'Payment Received', value: 'PR' },
    { name: 'Asset Delivered', value: 'AD' }
  ];

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public closeBidData: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public userId!: string;
  public userRole!: string;
  public searchApprovedBidText!: string;
  public approvedBidStatusFilter: any;
  public approvedDealStatusFilter: any;
  parentAuctionsData: any;
  closedParentAuctionsData: any;
  fulfilmentPaymentsData: any;

  LOATemplateTypes: Array<any> = [
    { typeName: 'Default', value: 'default' },
    { typeName: 'LOA Format DFSI', value: 'default' },
    { typeName: 'LOA Format HDB', value: 'default' },
    { typeName: 'LOA Format HDFC', value: 'hdfc' },
    { typeName: 'LOA Format HLFL', value: 'hlfl' },
    { typeName: 'LOA Format IBL', value: 'ibl' },
    { typeName: 'LOA Format TCL', value: 'tcl' },
    { typeName: 'LOA Format EU', value: 'EU' },
    { typeName: 'LOA Format TIL', value: 'transstroy' },
    { typeName: 'Lanco', value: 'lanco' }
  ];

  selectedAuctionId: any[] = [];
  selectedLOATemplate: any;
  normalMinDate: Date = new Date();
  cognitoId!: string;
  bulkUploadData: any[] = [];
  auctionFilterValue: any;
  reviseDate: any;
  isShowReviseDateDiv: boolean = false;
  errorRecordList: boolean = false;
  isShowInvalidRecords: boolean = false;
  fulfilmentId: any;
  Type: any;
  closedBid_total_count: any;
  ClosedBidAuctionFilterValue: any;
  searchClosedBidText: any;
  closedDealStatusFilter: any;
  closedBidStatusFilter: any;
  isDODate: boolean = false;
  startDate: any;
  endDate: any;
  form: FormGroup;
  now: Date = new Date();
  auctionValue: any;
  closeBidAuctionValue: any;
  public currentTab = 0;
  public approvedListingData = [];
  public closedListingData = [];
  skipNotification: boolean = false;
  uploadedExcelData: any;
  corrctFormat: boolean = false;
  constructor(public spinner: NgxSpinnerService, public route: ActivatedRoute, private router: Router, private storage: StorageDataService, private datePipe: DatePipe, private fb: FormBuilder, public notify: NotificationService, private dialog: MatDialog, public auctionService: AuctionService, public sharedService: SharedService) {
    this.form = this.fb.group({
      startDate: ["", Validators.required],
      endDate: ["", Validators.required]
    });
  }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.route.queryParams.subscribe((data) => {
      if (data.reload) {
        let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
        this.router.navigateByUrl(url);
      }
    })
    this.userId = this.storage.getStorageData('cognitoId', false);
    this.userRole = this.storage.getStorageData('userRole', false);
    this.getfulfilmentPayments();
    this.getCloseBidData();
  }


  getAuctionList() {
    let payload = {
      page: 1,
      limit: -1,
      ordering: '-id',

    }
    if (this.auctionValue) {
      payload['search'] = this.auctionValue;
      payload['context'] = 'active-fulfilment'
    }

    // if (this.closeBidAuctionValue) {
    //   payload['search'] = this.closeBidAuctionValue;
    //   payload['context'] = 'closed-fulfilment'
    // }

    this.auctionService.auctionDropdown(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.parentAuctionsData = res.results;
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getClosedAuctionList() {
    let payload = {
      page: 1,
      limit: -1,
      ordering: '-id',

    }

    if (this.closeBidAuctionValue) {
      payload['search'] = this.closeBidAuctionValue;
      payload['context'] = 'closed-fulfilment'
    }

    this.auctionService.auctionDropdown(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.closedParentAuctionsData = res.results;
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getfulfilmentPayments() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      role: this.userRole,
      bid_status__in: 'AC'
    };

    if (this.searchApprovedBidText) {
      payload['search'] = this.searchApprovedBidText;
    }

    // if (this.approvedBidStatusFilter) {
    //   payload['bid_status__in'] = this.approvedBidStatusFilter;
    // }
    // else {
    //   payload['bid_status__in'] = 'AC';
    // }

    if (this.approvedDealStatusFilter) {
      payload['deal_status__in'] = this.approvedDealStatusFilter;
    }

    if (this.auctionFilterValue) {
      // payload['lot_bid__lot__auction__id__in'] = this.auctionFilterValue;
      payload['auction_engine_auction_id'] = this.auctionFilterValue;
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.total_count = res.count;
          if (window.innerWidth > 768) {
            this.dataSource.data = res.results;
            this.dataSource.data.forEach(element => {
              let filteredData = this.selectedAuctionId.filter(x => x?.id == element?.id);
              if (filteredData && filteredData.length > 0) {
                element.selectedAuction = true;
              }
              else {
                element.selectedAuction = false;
              }
            });
          } else {
            this.scrolled = false;
            if (this.page == 1) {
              this.dataSource.data = [];
              this.approvedListingData = [];
              this.approvedListingData = res.results;
              this.dataSource.data = this.approvedListingData;
            } else {
              this.approvedListingData = this.approvedListingData.concat(res.results);
              this.dataSource.data = this.approvedListingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  getCloseBidData() {
    let payload = {
      page: this.page,
      limit: this.pageSize,
      ordering: this.ordering,
      role: this.userRole,
      bid_status__in: 'CL'
    };

    if (this.searchClosedBidText) {
      payload['search'] = this.searchClosedBidText;
    }

    // if (this.closedBidStatusFilter) {
    //   payload['bid_status__in'] = this.closedBidStatusFilter;
    // }
    // else {
    //   payload['bid_status__in'] = 'CL';
    // }

    if (this.closedDealStatusFilter) {
      payload['deal_status__in'] = this.closedDealStatusFilter;
    }

    if (this.ClosedBidAuctionFilterValue) {
      // payload['lot_bid__lot__auction__id__in'] = this.ClosedBidAuctionFilterValue;
      payload['auction_engine_auction_id'] = this.ClosedBidAuctionFilterValue;
    }

    this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        if (res) {
          this.closedBid_total_count = res.count;
          if (window.innerWidth > 768) {
            this.closeBidData.data = res.results;
          } else {
            this.scrolledBid = false;
            if (this.page == 1) {
              this.closeBidData.data = [];
              this.closedListingData = [];
              this.closedListingData = res.results;
              this.closeBidData.data = this.closedListingData;
            } else {
              this.closedListingData = this.closedListingData.concat(res.results);
              this.closeBidData.data = this.closedListingData;
            }
          }
        }
      },
      err => {
        this.notify.error(err.message);
      }
    );
  }

  selectLOATemplateValue(val) {
    this.selectedLOATemplate = val;
  }

  searchByBidStatus(val, type) {
    if (type == 'ApprovedBid') {
      this.approvedBidStatusFilter = val;
      this.page = 1;
      this.pageSize = 10;
      this.getfulfilmentPayments();
    }
    else {
      this.closedBidStatusFilter = val;
      this.page = 1;
      this.pageSize = 10;
      this.getCloseBidData();
    }
  }

  searchByDealStatus(val, type) {
    if (type == 'ApprovedBid') {
      this.approvedDealStatusFilter = val;
      this.page = 1;
      this.pageSize = 10;
      this.getfulfilmentPayments();
    }
    else {
      this.closedDealStatusFilter = val;
      this.page = 1;
      this.pageSize = 10;
      this.getCloseBidData();
    }
  }

  selectAuctionId(value) {
    // this.searchApprovedBidText = '';
    this.selectedAuctionId = [];
    this.auctionFilterValue = value;
    this.page = 1;
    this.pageSize = 10;
    this.getfulfilmentPayments();
  }

  selectClosedBidAuctionId(value) {
    //this.searchClosedBidText = '';
    this.ClosedBidAuctionFilterValue = value;
    this.page = 1;
    this.pageSize = 10;
    this.getCloseBidData();
  }

  GenerateLOA() {
    let selectedAuctionIds = this.selectedAuctionId.filter(x => x.id).map(x => x.id);
    if (selectedAuctionIds && selectedAuctionIds.length <= 0) {
      this.notify.error("Please select at least one auction")
    }
    else if (!this.selectedLOATemplate) {
      this.notify.error("Please select LOA template")
    }
    else {
      let params = {
        ids: selectedAuctionIds,
        template: this.selectedLOATemplate,
        additional_parameters: {},
        skip_notification: this.skipNotification
      }
      this.auctionService.generateLOA(params).subscribe(
        res => {
          this.notify.success("LOA generated successfully");
          this.getfulfilmentPayments();
        });
    }
  }

  onChange(checked, item) {
    if (checked.checked == true) {
      this.selectedAuctionId.push(item);
    }
    else {
      this.selectedAuctionId.splice(this.selectedAuctionId.indexOf(item), 1);
    }
  }

  onPageChange(event: PageEvent, type) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    if (type == 'ApprovedBid') {
      //  this.selectedAuctionId = [];
      this.getfulfilmentPayments();
    }
    else {
      this.getCloseBidData();
    }
  }

  onSortColumn(event, type) {
    this.ordering = (event.direction == "asc") ? event.active : ("-" + event.active);
    if (type == 'ApprovedBid') {
      this.getfulfilmentPayments();
    }
    else {
      this.getCloseBidData();
    }
  }

  searchInList(type) {
    if (type == 'ApprovedBid') {
      if (this.searchApprovedBidText === '' || this.searchApprovedBidText?.length > 0) {
        this.page = 1;
        this.pageSize = 10;
        this.getfulfilmentPayments();
      }
    }
    else {
      if (this.searchClosedBidText === '' || this.searchClosedBidText?.length > 0) {
        this.page = 1;
        this.pageSize = 10;
        this.getCloseBidData();
      }
    }
  }

  ProceedFullPayment(data) {
    if (this.cognitoId) {
      this.auctionService.getfulfilmentPaymentDetails(data?.id).subscribe(
        res => {
          const dialogRef = this.dialog.open(OfflinePaymentComponent, {
            width: '800px',
            disableClose: true,
            autoFocus: true,
            panelClass: 'my-class',
            data: {
              case: 'auction',
              paymentDetails: res,
              auction_details: data
            }
          });
          dialogRef.afterClosed().subscribe(val => {
            if (val) {
              this.getfulfilmentPayments();
            }
          })
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      )
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.panelClass = 'my-class';
      dialogConfig.width = '900px';
      dialogConfig.data = {
        flag: true
      }
      const dialogRef = this.dialog.open(SigninComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(val => {
        if (val) {
          this.cognitoId = this.storage.getStorageData('cognitoId', false);
        }
      })
    }
  }

  hideErrorList() {
    this.isShowInvalidRecords = false;
  }

  viewErrorList() {
    const dialogRef = this.dialog.open(ModelBulkErrorListComponent, {
      width: '700px',
      panelClass: 'custom-modalbox',
      data: {
        errorRecordList: this.errorRecordList,
        isFromFulfilment: true
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  onFileChange(event: any, type) {
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_json(ws);
      if (data != null && data.length > 0) {
        let modifiedData: Array<any> = [];
        modifiedData = data.map((t: any) => {
          t = JSON.parse(JSON.stringify(t).replace(/\s/g, ''));
          // this.uploadedExcelData = t;
          // console.log("this.uploadedExcelData", this.uploadedExcelData);
          // if (!this.uploadedExcelData.AuctionId) {
          //   this.notify.error("Please fill the data in Auction Id field and upload the excel again.");
          //   this.fileInput.nativeElement.value = "";
          //   this.corrctFormat = false;
          //   return false;
          // }
          // else if (!this.uploadedExcelData.lotNumber) {
          //   this.notify.error("Please fill the data in lot Number field and upload the excel again.");
          //   this.fileInput.nativeElement.value = "";
          //   this.corrctFormat = false;
          //   return false;
          // }
          // else if (!this.uploadedExcelData.Buyer) {
          //   this.notify.error("Please fill the data in Buyer field and upload the excel again.");
          //   this.fileInput.nativeElement.value = "";
          //   this.corrctFormat = false;
          //   return false;
          // }
          // else if (!this.uploadedExcelData.BidAmount) {
          //   this.notify.error("Please fill the data in Bid Amount field and upload the excel again.");
          //   this.fileInput.nativeElement.value = "";
          //   this.corrctFormat = false;
          //   return false;
          // }
          // else if (!this.uploadedExcelData.BuyersPremium) {
          //   this.notify.error("Please fill the data in Buyers Premium field and upload the excel again.");
          //   this.fileInput.nativeElement.value = "";
          //   this.corrctFormat = false;
          //   return false;
          // }

          return {
            "auction": t.AuctionId ? t.AuctionId : null,
            "lot_number": t.lotNumber ? t.lotNumber : null,
            "buyer": t.Buyer ? t.Buyer : null,
            "bid_amount": t.BidAmount ? t.BidAmount : null,
            "buyers_premium": t.BuyersPremium ? t.BuyersPremium : null,
          }
        });

        if (type == 'Upload') {
          this.auctionService.uploadBulkFulfilment(modifiedData).subscribe(
            (res: any) => {
              this.fileInput.nativeElement.value = "";
              this.errorRecordList = res.data;
              if (res && res.failed_count > 0) {
                this.isShowInvalidRecords = true;
              }
              this.notify.success(
                res.success_count + ' out of ' + res.total_count + ' records are processed successfully'
              );
              this.getfulfilmentPayments();
            }, (err: any) => {
              this.fileInput.nativeElement.value = "";
            });
        }
        else {
          this.auctionService.updateBulkFulfilment(modifiedData).subscribe(
            (res: any) => {
              this.fileInput.nativeElement.value = "";
              this.errorRecordList = res.data;
              if (res && res.failed_count > 0) {
                this.isShowInvalidRecords = true;
              }
              this.notify.success(
                res.success_count + ' out of ' + res.total_count + ' records are processed successfully'
              );
              this.getfulfilmentPayments();
            }, (err: any) => {
              this.fileInput.nativeElement.value = "";
            });

        }
      }
    };
  }

  rejectBid(id, reason, comment) {

    const params = {
      reject_reason: reason,
      reject_comment: comment
    }
    this.auctionService.rejectFulfilment(params, id).subscribe(
      res => {
        this.notify.success("Transaction Rejected", true);
        this.searchApprovedBidText= '';
        this.searchClosedBidText= '';
        this.getfulfilmentPayments();
        this.getCloseBidData();
      });


  }

  approvedBid(id) {
    const params = {
      bid_status: "AC",
    }
    this.auctionService.UpdateFulfilmentPatchReq(params, id).subscribe((res: any) => {
      this.getfulfilmentPayments();
      this.getCloseBidData();
    }, (err: any) => {
      console.log(err?.message);
    });
  }

  ShowChangeReviseDiv(id, type) {
    this.isShowReviseDateDiv = true;
    this.reviseDate = '';
    this.fulfilmentId = id;
    this.Type = type;
  }

  ChangeReviseDate() {
    if (this.Type == 'FulfilmentWise') {
      const params = {
        payment_due_date: this.reviseDate,
      }
      this.auctionService.UpdateFulfilmentPatchReq(params, this.fulfilmentId).subscribe(
        res => {
          this.isShowReviseDateDiv = false;
          this.reviseDate = '';
          this.notify.success('Date updated successfully');
          this.getfulfilmentPayments();
        });
    }
    else {
      let multiFulfilmentId = this.dataSource.data.filter(x => x.lot_bid.lot.auction.id == this.auctionFilterValue).map(x => x.id);
      const params = {
        payment_due_date: this.reviseDate,
        fulfilments: multiFulfilmentId
      }
      this.auctionService.bulkUpdateReviseDate(params, this.fulfilmentId).subscribe(
        res => {
          this.isShowReviseDateDiv = false;
          this.reviseDate = '';
          this.notify.success('Date updated successfully');
          this.getfulfilmentPayments();
        });
    }
  }

  moreInfoDialog(element) {
    const dialogRef = this.dialog.open(AuctionFulfilmentInfoDialogComponent, {
      data: element
    });
  }

  bidAction(type: string, element: any) {
    const dialogRef = this.dialog.open(BidActionComponent, {
      width: '600px',
      data: {
        type: type,
        mainData: element,
        isFromFulfilment: true
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result?.rejectTransaction) {
        let reason = result?.reject_reason;
        let comment = result?.reject_comment;
        this.rejectBid(element?.id, reason, comment);
      }
      else {
        this.getfulfilmentPayments();
        this.getCloseBidData();
      }
    });
  }

  setInvoicePerson(element: any) {
    let nameShow;
    if (element?.invoice_in_favour_of) {
      nameShow = element?.invoice_in_favour_of?.first_name + " " + element?.invoice_in_favour_of?.last_name;
      return nameShow;
    }
  }

  viewAllIds(data) {
    const dialogRef = this.dialog.open(ViewAllIdsComponent, {
      width: '500px',
      disableClose: true,
      data: {
        isFromFulfilment: true,
        ids: data,
      }
    });
  }

  checkedDOIssueDate(checked) {
    if (checked.checked == true) {
      this.isDODate = true;
    }
    else {
      this.isDODate = false;
    }
  }

  export() {
    let date = this.form?.get('endDate')?.value;
    date = new Date(date);
    date = date.setDate(date.getDate() + 1);
    this.form.markAllAsTouched();
    if (this.form.valid) {
      let payload = {
        page: 1,
        limit: -1,
        ordering: this.ordering,
        role: this.userRole
      };


      if (this.isDODate) {
        if (this.form?.get('startDate')?.value) {
          payload['delivery_order_issue_date__gte'] = this.datePipe.transform(this.form?.get('startDate')?.value, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
        if (this.form?.get('endDate')?.value) {
          payload['delivery_order_issue_date__lt'] = this.datePipe.transform(date, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
      }
      else {
        if (this.form?.get('startDate')?.value) {
          payload['created_at__gte'] = this.datePipe.transform(this.form?.get('startDate')?.value, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
        if (this.form?.get('endDate')?.value) {
          payload['created_at__lt'] = this.datePipe.transform(date, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }
      }

      this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res) {
            const excelData = res.results.map((r: any) => {
              return {
                "ID": r?.id,
                "Auction ID": r?.lot_bid?.lot?.auction?.auction_engine_auction_id,
                "Lot Number": r?.lot_bid?.lot?.lot_number,
                "Lot Description": r?.lot_bid?.lot?.description,
                "Asset Id": r?.asset_ids.join(", "),
                "Buyer Name": r?.lot_bid?.user?.first_name,
                "Buyer Mobile No ": r?.lot_bid?.user?.mobile_number,
                "Email": r?.lot_bid?.user?.email,
                "Bid Amount": r?.lot_bid?.amount,
                "Buyers Premium": r?.actual_buyers_premium,
                "Bid Status": r?.bid_status_display_name,
                "Deal Status": r?.deal_status_display_name,
                "Full Payment Due Date": r?.payment_due_date ? (this.datePipe.transform(r?.payment_due_date, 'yyyy-MM-dd')) :
                  r?.lot_bid?.lot?.auction?.payment_date
                    ? r?.lot_bid?.lot?.auction?.payment_date : '',
                "Request Date": r?.created_at ? (this.datePipe.transform(r?.created_at, 'yyyy-MM-dd')) : '',
                "RM Name": r?.lot_bid?.user_relationship_manager_full_name,
                "Invoice_in_Favour_of": this.setInvoicePerson(r),
                "LOA Generated": r?.loa_url ? 'Yes' : 'No',
              }
            });
            ExportExcelUtil.exportArrayToExcel(excelData, "Fulfilment");
          }
        },
        err => {
          this.notify.error(err.message);
        }
      );

    }

  }

  exportSelectedAuctionId(type) {

    if (type == 'ApprovedBid') {

      let payload = {
        page: 1,
        limit: -1,
        ordering: this.ordering,
        role: this.userRole,
        bid_status__in: 'AC'
      };

      if (this.searchApprovedBidText) {
        payload['search'] = this.searchApprovedBidText;
      }

      if (this.approvedDealStatusFilter) {
        payload['deal_status__in'] = this.approvedDealStatusFilter;
      }

      if (this.auctionFilterValue) {
        //payload['lot_bid__lot__auction__id__in'] = this.auctionFilterValue;
        payload['auction_engine_auction_id'] = this.auctionFilterValue;
      }

      this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res) {
            const excelData = res.results.map((r: any) => {
              return {
                "ID": r?.id,
                "Auction ID": r?.lot_bid?.lot?.auction?.auction_engine_auction_id,
                "Lot Number": r?.lot_bid?.lot?.lot_number,
                "Lot Description": r?.lot_bid?.lot?.description,
                "Asset Id": r?.asset_ids.join(", "),
                "Buyer Name": r?.lot_bid?.user?.first_name,
                "Buyer Mobile No ": r?.lot_bid?.user?.mobile_number,
                "Email": r?.lot_bid?.user?.email,
                "Bid Amount": r?.lot_bid?.amount,
                "Buyers Premium": r?.actual_buyers_premium,
                "Bid Status": r?.bid_status_display_name,
                "Deal Status": r?.deal_status_display_name,
                "Full Payment Due Date": r?.payment_due_date ? (this.datePipe.transform(r?.payment_due_date, 'yyyy-MM-dd')) :
                  r?.lot_bid?.lot?.auction?.payment_date
                    ? r?.lot_bid?.lot?.auction?.payment_date : '',
                "Request Date": r?.created_at ? (this.datePipe.transform(r?.created_at, 'yyyy-MM-dd')) : '',
                "RM Name": r?.lot_bid?.user_relationship_manager_full_name,
                "Invoice_in_Favour_of": this.setInvoicePerson(r),
                "LOA Generated": r?.loa_url ? 'Yes' : 'No',
              }
            });
            ExportExcelUtil.exportArrayToExcel(excelData, "Fulfilment");
          }
        },
        err => {
          this.notify.error('Data does not exist!');
        }
      );

    }
    else {
      let payload = {
        page: 1,
        limit: -1,
        ordering: this.ordering,
        role: this.userRole,
        bid_status__in: 'CL'
      };

      if (this.searchClosedBidText) {
        payload['search'] = this.searchClosedBidText;
      }

      if (this.closedDealStatusFilter) {
        payload['deal_status__in'] = this.closedDealStatusFilter;
      }

      if (this.ClosedBidAuctionFilterValue) {
        // payload['lot_bid__lot__auction__id__in'] = this.ClosedBidAuctionFilterValue;
        payload['auction_engine_auction_id'] = this.ClosedBidAuctionFilterValue;
      }

      this.auctionService.getfulfilmentPayments(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          if (res) {
            const excelData = res.results.map((r: any) => {
              return {
                "ID": r?.id,
                "Auction ID": r?.lot_bid?.lot?.auction?.auction_engine_auction_id,
                "Lot Number": r?.lot_bid?.lot?.lot_number,
                "Lot Description": r?.lot_bid?.lot?.description,
                "Asset Id": r?.asset_ids.join(", "),
                "Buyer Name": r?.lot_bid?.user?.first_name,
                "Buyer Mobile No ": r?.lot_bid?.user?.mobile_number,
                "Email": r?.lot_bid?.user?.email,
                "Bid Amount": r?.lot_bid?.amount,
                "Buyers Premium": r?.actual_buyers_premium,
                "Bid Status": r?.bid_status_display_name,
                "Deal Status": r?.deal_status_display_name,
                "Full Payment Due Date": r?.payment_due_date ? (this.datePipe.transform(r?.payment_due_date, 'yyyy-MM-dd')) :
                  r?.lot_bid?.lot?.auction?.payment_date
                    ? r?.lot_bid?.lot?.auction?.payment_date : '',
                "Request Date": r?.created_at ? (this.datePipe.transform(r?.created_at, 'yyyy-MM-dd')) : '',
                "RM Name": r?.lot_bid?.user_relationship_manager_full_name,
                "Invoice_in_Favour_of": this.setInvoicePerson(r),
                "LOA Generated": r?.loa_url ? 'Yes' : 'No',
              }
            });
            ExportExcelUtil.exportArrayToExcel(excelData, "Fulfilment");
          }
        },
        err => {
          this.notify.error(err.message);
        }
      );

    }
  }

  getMinDate() {
    const date = new Date();
    const month = date.getMonth();
    date.setMonth(date.getMonth() - 1);
    while (date.getMonth() === month) {
      date.setDate(date.getDate() - 1);
    }
    return date;
  }

  getEndDate() {
    let date = this.form?.get('startDate')?.value;
    if (date) {
      date = new Date(date);
      return date;
    }
    //   else {
    //     const date = new Date();
    //     const month = date.getMonth();
    //     date.setMonth(date.getMonth() - 1);
    //     while (date.getMonth() === month) {
    //       date.setDate(date.getDate() - 1);
    //     }
    //     return date;
    //  }
  }

  searchAuction() {
    //  this.closeBidAuctionValue = "";

    if (this.auctionValue?.length >= 3) {
      this.getAuctionList();
    }
    else {
      if (this.auctionValue === '') {
        this.parentAuctionsData = [];
        this.auctionFilterValue = "";
        this.getfulfilmentPayments();
      }
    }
  }

  searchCloseBidAuction() {
    // this.auctionValue = '';
    if (this.closeBidAuctionValue?.length >= 3) {
      this.getClosedAuctionList();
    }
    else {
      if (this.closeBidAuctionValue === '') {
        this.closedParentAuctionsData = [];
        this.ClosedBidAuctionFilterValue = "";
        this.getCloseBidData();
      }
    }
  }

  onListTabChanged(event) {
    this.currentTab = event?.index;
  }
  scrolledBid = false;
  scrolled = false;
  @HostListener('window:scroll', ['$event'])
  recursiveNewsApiHit = (event) => {
    if (window.innerWidth < 768) {
      if (Math.round(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (this.currentTab == 0) {
          if ((this.total_count > this.dataSource.data.length) && (this.dataSource.data.length > 0)) {
            if (!this.scrolled) {
              this.scrolled = true;
              this.page++;
              this.getfulfilmentPayments();
            }
          }
        }
        else {
          if ((this.closedBid_total_count > this.closeBidData.data.length) && (this.closeBidData.data.length > 0)) {
            if (!this.scrolledBid) {
              this.scrolledBid = true;
              this.page++;
              this.getCloseBidData();
            }
          }
        }
      }

    }
  }

  onSkipNotificationCheck(event) {
    this.skipNotification = event.checked;
  }

}
