import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionFulfilmentComponent } from './auction-fulfilment.component';

describe('AuctionFulfilmentComponent', () => {
  let component: AuctionFulfilmentComponent;
  let fixture: ComponentFixture<AuctionFulfilmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionFulfilmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionFulfilmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
