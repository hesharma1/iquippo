import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionPaymentHistoryForAdminComponent } from './auction-payment-history-for-admin.component';

describe('AuctionPaymentHistoryForAdminComponent', () => {
  let component: AuctionPaymentHistoryForAdminComponent;
  let fixture: ComponentFixture<AuctionPaymentHistoryForAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionPaymentHistoryForAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionPaymentHistoryForAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
