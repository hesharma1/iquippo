import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { DatePipe } from "@angular/common";
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { AuctionService } from 'src/app/services/auction.service';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';

export const MY_CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY',
  fullPickerInput: 'DD-MM-YYYY hh:mm a',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'hh:mm a',
  monthYearLabel: 'MMM-YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM-YYYY'
};

@Component({
  selector: 'app-create-lot',
  templateUrl: './create-lot.component.html',
  styleUrls: ['./create-lot.component.css'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ]
})
export class CreateLotComponent implements OnInit {
  public auctionLotForm: FormGroup;
  public lotId: any;
  public auctionList: Array<any> = [];
  public categoryData: Array<any> = [];
  public subauctionList: Array<any> = [];
  public assetList: Array<any> = [];
  public minDate = new Date();
  public toEdit: boolean = false;
  public lotDetails: any;
  // public locationOptions: Array<any> = [];
  public statusOptions: Array<any> = [
    { name: 'Open', value: "OP" },
    { name: 'Closed', value: "CL" }
  ];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public datePipe: DatePipe, private notify: NotificationService, public s3: S3UploadDownloadService, private auctionService: AuctionService, private adminMasterService: AdminMasterService, public router: Router, public route: ActivatedRoute, private newEquipmentService: NewEquipmentService) {

    this.auctionLotForm = this.fb.group({
      parent_auction: ['', Validators.compose([Validators.required])],
      auction: [''],
      lot_number: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      title: ['', Validators.compose([Validators.required])],
      description: [''],
      serial_no: [''],
      // invoice_date: [''],
      measurement_unit: ['', Validators.required],
      quantity: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      location: [''],
      facility_name: [''],
      additional_info: [''],
      start_price: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      reserve_price: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      starts_at: ['', Validators.compose([Validators.required])],
      ends_at: ['', Validators.compose([Validators.required])],
      last_minute_bid: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      extended_to: [null, Validators.compose([Validators.pattern(/^[0-9]*$/)])],
      bid_increment_type: ['ST', Validators.compose([Validators.required])],
      category: [''],
      amount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      bid_increments: this.fb.array([])
    })
  }

  ngOnInit(): void {
    this.getDetails();
    this.route.queryParams.subscribe((data) => {
      this.lotId = data.id;
      if (this.lotId) {
        this.toEdit = true;
      }
    })
  }

  getDetails() {
    forkJoin([this.auctionService.getAuctionList({ limit: 999, parent_auction__id__isnull: true, ends_at__gt: (new Date()).toISOString() }), this.adminMasterService.getCategoryMaster("limit=999")]).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.auctionList = res[0].results;
        this.categoryData = res[1].results;
        if (this.toEdit) {
          this.getLotDetails();
        }
      },
      err => {
        console.log(err);
        this.notify.error(err?.message, true);
        this.router.navigate(['/admin-dashboard/auction/auction-list']);
      }
    )
  }

  getSubaucitonList(id) {
    this.auctionLotForm.patchValue({
      auction: ''
    });
    this.subauctionList = [];
    if(id){
      this.auctionService.getAuctionList({ limit: 999, parent_auction__id__in: id }).pipe(finalize(() => {   })).subscribe(
        (res: any) => {
          this.subauctionList = res?.results;
          if (this.subauctionList.length) {
            this.auctionLotForm.get('auction')!.setValidators([Validators.required]);
            this.auctionLotForm.get('auction')!.updateValueAndValidity();
          } else {
            this.auctionLotForm.get('auction')!.setValidators([]);
            this.auctionLotForm.get('auction')!.updateValueAndValidity();
          }
  
          if (this.toEdit) {
            this.auctionLotForm.patchValue({
              auction: this.lotDetails?.auction.toString()
            });
            this.toEdit = false;
          }
        },
        err => {
          console.log(err);
          this.notify.error(err?.message, true);
          this.router.navigate(['/admin-dashboard/auction/auction-list']);
        }
      )
    }
  }

  getLotDetails() {
    this.auctionService.getAuctionLotDetails(this.lotId).pipe(finalize(() => {   })).subscribe(
      (res: any) => {
        this.lotDetails = res;
        if (this.lotDetails?.parent_auction) {
          this.getSubaucitonList(this.lotDetails?.parent_auction);
        }
        this.setLotDetails();
      },
      err => {
        console.log(err);
        this.notify.error(err?.message, true);
        this.router.navigate(['/admin-dashboard/auction/auction-list']);
      }
    )
  }

  get bidArray() {
    return this.auctionLotForm.get("bid_increments") as FormArray;
  }

  addBidArray() {
    this.bidArray.push(this.newBidDetails());
  }

  newBidDetails(data?): FormGroup {
    return this.fb.group({
      increment_type: 'BR',
      bid_start_amount: [data ? data?.bid_start_amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      bid_end_amount: [data ? data?.bid_end_amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      amount: [data ? data?.amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
    })
  }

  removeBidDetails(i) {
    this.bidArray.removeAt(i);
  }

  updateValidation(val) {
    if (val == 'ST') {
      const control = this.bidArray;
      for (let i = control.length - 1; i >= 0; i--) {
        control.removeAt(i)
      }
      this.auctionLotForm.controls.amount.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
      this.auctionLotForm.controls.amount.updateValueAndValidity();
    }
    else {
      this.auctionLotForm.controls.amount.setValidators([]);
      this.auctionLotForm.controls.amount.updateValueAndValidity();
      this.addBidArray();
    }
  }

  // getLocation(pin) {
  //   if (pin && pin.length > 4) {
  //     this.newEquipmentService.getPincodeMaster({ pin_code__contains: pin, limit: 999 }).pipe(finalize(() => {   })).subscribe(
  //       (data: any) => {
  //         if (data.results.length) {
  //           this.locationOptions = data.results;
  //         } else {
  //           this.locationOptions = [];
  //           this.auctionLotForm.get('pin_code')?.setErrors({ wrong: true });
  //         }
  //       },
  //       err => {
  //         console.log(err);
  //       }
  //     );
  //   }
  // }

  submitForm(value) {
    console.log(this.auctionLotForm.value)
    let payload = Object.assign({}, value);
    if (!payload?.auction) {
      payload.auction = payload.parent_auction
    }
    if (value.bid_increment_type == "ST") {
      payload.bid_increments = [{
        increment_type: 'ST',
        amount: value.amount
      }]
    }
    delete payload['parent_auction'];
    delete payload['bid_increment_type'];
    delete payload['amount'];
    // payload['invoice_date'] = payload.invoice_date ? (new Date(payload.invoice_date)).toISOString() : '';
    payload['starts_at'] = payload.starts_at ? (new Date(payload.starts_at)).toISOString() : '';
    payload['ends_at'] = payload.ends_at ? (new Date(payload.ends_at)).toISOString() : '';

    if (this.lotId) {
      this.auctionService.updateAuctionLot(payload, this.lotId).pipe(finalize(() => {   })).subscribe(
        (res: any) => {
          this.notify.success('Auction Lot updated successfully!!', true);
          this.router.navigate(['/admin-dashboard/auction/auction-list']);
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      );
    } else {
      this.auctionService.createAuctionLot(payload).pipe(finalize(() => {   })).subscribe(
        (res: any) => {
          this.notify.success('Auction Lot created successfully!!', true);
          this.router.navigate(['/admin-dashboard/auction/auction-list']);
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      );
    }
  }

  setLotDetails() {
    this.auctionLotForm.patchValue({
      parent_auction: (this.lotDetails?.parent_auction != null) ? JSON.stringify(this.lotDetails?.parent_auction) : JSON.stringify(this.lotDetails?.auction),
      lot_number: this.lotDetails?.lot_number,
      title: this.lotDetails?.title,
      description: this.lotDetails?.description,
      serial_no: this.lotDetails?.serial_no,
      // invoice_date: this.datePipe.transform(this.lotDetails?.invoice_date, 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
      measurement_unit: this.lotDetails?.measurement_unit,
      quantity: this.lotDetails?.quantity,
      location: this.lotDetails?.location,
      facility_name: this.lotDetails?.facility_name,
      additional_info: this.lotDetails?.additional_info,
      start_price: this.lotDetails?.start_price,
      reserve_price: this.lotDetails?.reserve_price,
      starts_at: this.lotDetails?.starts_at,
      ends_at: this.lotDetails?.ends_at,
      last_minute_bid: this.lotDetails?.last_minute_bid,
      extended_to: this.lotDetails?.extended_to,
      bid_increment_type: this.lotDetails?.bid_increments[0]?.increment_type,
      category: JSON.stringify(this.lotDetails?.category)
    })
    if (this.lotDetails?.bid_increments[0]?.increment_type == "BR") {
      this.auctionLotForm.controls.amount.setValidators([]);
      this.auctionLotForm.controls.amount.updateValueAndValidity();
      this.lotDetails?.bid_increments.forEach(element => {
        this.bidArray.push(this.newBidDetails(element));
      });
    } else {
      this.auctionLotForm.patchValue({
        amount: this.lotDetails?.bid_increments[0]?.amount
      })
    }
  }

}
