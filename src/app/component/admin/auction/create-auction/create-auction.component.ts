import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from "rxjs/operators";
import { DatePipe } from "@angular/common";
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { environment } from 'src/environments/environment';
import { v4 as uuidv4 } from 'uuid';
import { S3UploadDownloadService } from './../../../../utility/s3-upload-download/s3-upload-download.service';
import { AuctionService } from 'src/app/services/auction.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';

export const MY_CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY',
  fullPickerInput: 'DD-MM-YYYY hh:mm a',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'hh:mm a',
  monthYearLabel: 'MMM-YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM-YYYY'
};

@Component({
  selector: 'app-create-auction',
  templateUrl: './create-auction.component.html',
  styleUrls: ['./create-auction.component.css'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ]
})
export class CreateAuctionComponent implements OnInit {
  @ViewChild('imageInput') imageInput!: ElementRef;

  public auctionForm: FormGroup;
  public addInfo: boolean = false;
  public minDate = new Date();
  public auctionSelectedContact: any;
  public customerArray: Array<any> = [];
  public toEdit: boolean = false;
  public auctionId: any;
  public addInfoTriggerArray: any = [];
  // public locationOptions: Array<any> = [];
  public auctionDetails: any;
  public saleTypeFilterOptions: Array<any> = [
    { name: 'Online Auction', value: "OL" },
    { name: 'Reverse Auction', value: "RV" },
    { name: 'Live Auction', value: "LV" },
    { name: 'Private Treaty', value: "PT" }
  ];
  public auctionTypeFilterOptions: Array<any> = [
    { name: 'Sequential', value: "SQ" },
    { name: 'Concurrent', value: "CT" }
  ];
  public auctionStatusFilterOptions: Array<any> = [
    { name: 'Private', value: "PR" },
    { name: 'Public', value: "PB" }
  ];
  public buisnessVerticalOptions: Array<any> = [
    { name: 'Industrial', value: "IN" },
    { name: 'Construction Equipment', value: "CE" },
    { name: 'Asset Based Lending', value: "AB" },
    { name: 'Other', value: "OT" }
  ];
  public categoryTypeFilterOptions: Array<any> = [
    { name: 'Normal Auction', value: "NM" },
    { name: 'Bid Limit Auction', value: "BL" }
  ];

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public auctionService: AuctionService, public datePipe: DatePipe, private notify: NotificationService, public s3: S3UploadDownloadService, public router: Router, private newEquipmentService: NewEquipmentService, public route: ActivatedRoute) {
    this.auctionForm = fb.group({
      has_subauctions: ['false'],
      sale_type: ['', Validators.required],
      auction_type: ['', Validators.required],
      auction_status: ['', Validators.required],
      business_vertical: [''],
      name: ['', Validators.required],
      starts_at: ['', Validators.required],
      ends_at: ['', Validators.required],
      inspection_starts_at: [''],
      inspection_ends_at: [''],
      manager: ['', Validators.required],
      city: ['', Validators.compose([Validators.required])],
      address: [''],
      credit_limit_multiplier: [null, Validators.pattern(/^[0-9]*$/)],
      is_tax_included: ['false'],
      last_minute_bid: [null],
      extended_to: [null],
      terms: [''],
      category_type: [''],
      catalog_url: [''],
      // bidder_undertaking_url: [''],
      img_url: [''],
      registration_form_url: [''],
      undertaking_form_url: [''],
      is_active: [false],
      is_otp_enabled: [false],
      is_offline_emd_available: [false],
      is_wallet_emd_available: [false],
      is_online_emd_available: [false],
      on_behalf_of: [''],
      buyers_premium: [''],
      tax: [''],
      payment_date: [''],
      asset_removal_date: [''],
      additional_info: [''],
      disclaimer: [''],
      contacts: this.fb.array([this.newAuctionContactDetails()]),
      bid_increment_type: ['ST'],
      amount: [null, Validators.required],
      bid_increments: this.fb.array([]),
      subauctions: this.fb.array([])
    })
  }

  ngOnInit(): void {
    this.getCustomerList();

    this.route.queryParams.subscribe((data) => {
      this.auctionId = data.id;
    })
  }

  getCustomerList() {
    let payload = {
      partnership_type__in: 3,
      limit: 999
    }
    this.auctionService.getCustomerList(payload).pipe(finalize(() => { })).subscribe(
      (data: any) => {
        this.customerArray = data.results;
        if (this.auctionId) {
          this.toEdit = true;
          this.getAuctionDetails();
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getAuctionDetails() {
    this.auctionService.getAuctionByID(this.auctionId).pipe(finalize(() => { })).subscribe(
      (res: any) => {
        console.log(res);
        this.auctionDetails = res;
        this.setAuctionDetails(this.auctionDetails);
      },
      err => {
        console.log(err);
        this.notify.error(err?.message, true);
        this.router.navigate(['/admin-dashboard/auction/auction-list']);
      }
    )
  }

  async handleUpload(event: any, keyName: string) {
    const file = event.target.files[0];
    var fileFormat = (file?.name).toString();
    var fileExt = fileFormat?.split('.')[fileFormat?.split('.').length - 1];
    if ((file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/bmp' || file.type == 'image/svg+xml' || fileExt == 'svg') && file.size <= 5242880) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      var image_path = environment.bucket_parent_folder + "/marketplace/auction/" + uuidv4() + '_' + keyName + '_' + file?.name.trim();
      var uploaded_file = await this.s3.prepareFileForUpload(file, image_path);

      if (uploaded_file?.Key != undefined && uploaded_file?.Key != '' && uploaded_file?.Key != null) {
        this.auctionForm.get(keyName)?.setValue(uploaded_file.Location);
      }
    } else {
      this.notify.error("Invalid Image.")
    }
    this.imageInput.nativeElement.value = '';
  }

  getFileName(keyname, str) {
    return str.split(keyname).pop();
  }

  deleteFile(key, imgPath) {
    this.s3.deleteFile(imgPath).subscribe(
      data => {
        this.notify.success('File Successfully deleted!!');
        this.auctionForm.get(key)?.setValue('');
      },
      err => {
        console.log(err);
        this.notify.error(err);
      }
    )
  }

  get bidArray() {
    return this.auctionForm.get("bid_increments") as FormArray;
  }

  addBidArray() {
    this.bidArray.push(this.newBidDetails());
  }

  newBidDetails(data?): FormGroup {
    return this.fb.group({
      id: data ? data?.id : null,
      increment_type: 'BR',
      bid_start_amount: [data ? data?.bid_start_amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      bid_end_amount: [data ? data?.bid_end_amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      amount: [data ? data?.amount : '', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])]
    })
  }

  removeBidDetails(i) {
    this.bidArray.removeAt(i);
  }

  checkSubauction(val) {
    if (val == 'false') {
      const control = this.subAuctions;
      if (control.length) {
        for (let i = control.length - 1; i >= 0; i--) {
          control.removeAt(i)
        }
      }
    }
  }

  updateValidation(val) {
    if (val == 'ST') {
      const control = this.bidArray;
      if (control.length) {
        for (let i = control.length - 1; i >= 0; i--) {
          control.removeAt(i)
        }
        this.auctionForm.controls.amount.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
        this.auctionForm.controls.amount.updateValueAndValidity();
      }
    }
    else {
      this.auctionForm.controls.amount.setValidators([]);
      this.auctionForm.controls.amount.updateValueAndValidity();
      this.addBidArray();
    }
  }

  get auctionContact() {
    return this.auctionForm.get("contacts") as FormArray;
  }

  addAuctionContact() {
    this.auctionContact.push(this.newAuctionContactDetails());
  }

  newAuctionContactDetails(data?, obj?): FormGroup {
    return this.fb.group({
      id: [data ? data?.id : null],
      user: [data ? data?.user : ''],
      name: [obj ? obj?.user?.first_name + obj?.user?.last_name : ''],
      mobile_no: [obj ? obj?.user?.mobile_number : ''],
      email: [obj ? obj?.user?.email : ''],
      location: [obj ? obj?.user?.pin_code?.city?.name : '']
    })
  }

  removeAuctionContact(i) {
    this.auctionContact.removeAt(i);
    if (i == 0 && !this.auctionContact.length) {
      this.addAuctionContact();
    }
  }

  onCustomerSelection(forType, formObj, value) {
    let obj = this.customerArray.find(f => { return f.user?.cognito_id == value });
    switch (forType) {
      case 'auction': {
        formObj.patchValue({
          name: [obj?.user?.first_name + obj?.user?.last_name],
          mobile_no: [obj?.user?.mobile_number],
          email: [obj?.user?.email],
          location: [obj?.user?.pin_code?.city?.name]
        });
        break;
      }
      case 'subauction': {
        formObj.patchValue({
          name: [obj?.user?.first_name + obj?.user?.last_name],
          mobile_no: [obj?.user?.mobile_number],
          email: [obj?.user?.email],
          location: [obj?.user?.pin_code?.city?.name]
        });
        break;
      }
      default: {
        break;
      }
    }
  }

  /**
   * Sub - Auctions form function
   */

  get subAuctions() {
    return this.auctionForm.get("subauctions") as FormArray;
  }

  addSubauction() {
    this.subAuctions.push(this.newSubAuction());
  }

  newSubAuction(data?): FormGroup {
    console.log("cal..")
    return this.fb.group({
      id: [data ? data?.id : null],
      name: [data ? data?.name : '', Validators.required],
      starts_at: [data ? data?.starts_at : '', Validators.required],
      ends_at: [data ? data?.ends_at : '', Validators.required],
      inspection_starts_at: [data ? data?.inspection_starts_at : ''],
      inspection_ends_at: [data ? data?.inspection_ends_at : ''],
      manager: [(data && data?.manager) ? data?.manager.toString() : '', Validators.required],
      is_proxy: [data ? data?.is_proxy.toString() : 'false'],
      category_type: [(data && data?.category_type) ? data?.category_type.toString() : ''],
      city: [data ? data?.city : '', Validators.compose([Validators.required])],
      credit_limit_multiplier: [data ? data?.credit_limit_multiplier : null, Validators.pattern(/^[0-9]*$/)],
      is_tax_included: [data ? data?.is_tax_included.toString() : 'false'],
      last_minute_bid: [data ? data?.last_minute_bid : null],
      extended_to: [data ? data?.extended_to : null],
      terms: [data ? data?.terms : ''],
      on_behalf_of: [data ? data?.on_behalf_of : ''],
      buyers_premium: [data ? data?.buyers_premium : ''],
      tax: [data ? data?.tax : ''],
      payment_date: [data ? data?.payment_date : ''],
      asset_removal_date: [data ? data?.asset_removal_date : ''],
      additional_info: [data ? data?.additional_info : ''],
      disclaimer: [data ? data?.disclaimer : ''],
      contacts: this.fb.array([this.newAuctionContactDetails()]),
      bid_increment_type: [data ? data?.bid_increments[0]?.increment_type : 'ST'],
      amount: [null, Validators.required],
      bid_increments: this.fb.array([])
    })
  }

  removeSubauction(i) {
    this.subAuctions.removeAt(i);
  }

  get subAuctionControl() {
    return this.subAuctions.controls;
  }

  subAuctionBidArray(i) {
    return this.subAuctionControl[i].get('bid_increments') as FormArray;
  }

  addSubBidArray(i) {
    this.subAuctionBidArray(i).push(this.newBidDetails());
  }

  removeSubBidDetails(index, bidDetailIndex) {
    this.subAuctionBidArray(index).removeAt(bidDetailIndex);
  }

  updateSubauctionValidation(val, i) {
    let subAuction = this.subAuctionControl[i] as FormGroup;
    if (val == 'ST') {
      const control = this.subAuctionBidArray(i);
      for (let i = control.length - 1; i >= 0; i--) {
        control.removeAt(i)
      }
      subAuction.get('amount')!.setValidators([Validators.required, Validators.pattern(/^[0-9]*$/)]);
      subAuction.get('amount')!.updateValueAndValidity();
    }
    else {
      subAuction.get('amount')!.setValidators([]);
      subAuction.get('amount')!.updateValueAndValidity();
      this.addSubBidArray(i);
    }
  }

  subAuctionContact(i) {
    return this.subAuctionControl[i].get("contacts") as FormArray;
  }

  addSubAuctionContact(i) {
    this.subAuctionContact(i).push(this.newAuctionContactDetails());
  }

  removeSubAuctionContact(index, contactIndex) {
    this.subAuctionContact(index).removeAt(contactIndex);
    if (contactIndex == 0 && !this.subAuctionContact(index).length) {
      this.addSubAuctionContact(index);
    }
  }

  addInfoTriggerFun(index) {
    if (this.addInfoTriggerArray && (this.addInfoTriggerArray.indexOf(index) == -1)) {
      this.addInfoTriggerArray.push(index);
    } else {
      this.addInfoTriggerArray.splice(this.addInfoTriggerArray.indexOf(index), 1);
    }
  }

  // getLocation(formGroup, pin) {
  //   if (pin && pin.length > 4) {
  //     this.newEquipmentService.getPincodeMaster({ pin_code__contains: pin, limit: 999 }).pipe(finalize(() => {   })).subscribe(
  //       (data: any) => {
  //         if (data.results.length) {
  //           this.locationOptions = data.results;
  //         } else {
  //           this.locationOptions = [];
  //           formGroup.get('pin_code')?.setErrors({ wrong: true });
  //         }
  //       },
  //       err => {
  //         console.log(err);
  //       }
  //     );
  //   }
  // }

  submitForm(value) {
    if (value.bid_increment_type == "ST") {
      if (this.toEdit && (value.bid_increment_type == this.auctionDetails.bid_increment_type)) {
        value.bid_increments = [{
          // id: this.auctionDetails?.bid_increments[0]?.id,
          increment_type: 'ST',
          amount: value.amount
        }]
      } else {
        value.bid_increments = [{
          increment_type: 'ST',
          amount: value.amount
        }]
      }
    } else {
      if (value.bid_increment_type == "BR") {
        value.bid_increments.forEach(element => {
          if (!element.id) {
            delete element['id'];
          }
        });
      }
    }

    if (value?.contacts && value?.contacts.length) {
      let tempContactArray: Array<any> = [];
      value?.contacts.forEach((element: any) => {
        if (element?.user) {
          let tempContactObj: any = {
            user: element?.user
          }
          if (element?.id) {
            tempContactObj['id'] = element?.id;
          } else {
            delete element['id']
          }
          tempContactArray.push(tempContactObj);
        }
      });
      value.contacts = [];
      value.contacts = [...tempContactArray];
    }

    delete value['bid_increment_type'];
    delete value['amount'];
    value['starts_at'] = (new Date(value.starts_at)).toISOString();
    value['ends_at'] = (new Date(value.ends_at)).toISOString();
    value['inspection_starts_at'] = value.inspection_starts_at ? (new Date(value.inspection_starts_at)).toISOString() : '';
    value['inspection_ends_at'] = value.inspection_ends_at ? (new Date(value.inspection_ends_at)).toISOString() : '';
    value['payment_date'] = value.payment_date ? (new Date(value.payment_date)).toISOString() : '';
    value['asset_removal_date'] = value.asset_removal_date ? (new Date(value.asset_removal_date)).toISOString() : '';

    // For Subauction Changes
    if (value?.has_subauctions && value?.subauctions && value?.subauctions?.length) {
      value?.subauctions.forEach((element, i) => {
        if (!element?.id) {
          delete element['id'];
        }
        if (element.bid_increment_type == "ST") {
          if (this.toEdit && (element.bid_increment_type == this.auctionDetails.subauctions[i]?.bid_increment_type)) {
            element.bid_increments = [{
              // id: this.auctionDetails?.bid_increments[0]?.id,
              increment_type: 'ST',
              amount: element.amount
            }]
          } else {
            element.bid_increments = [{
              increment_type: 'ST',
              amount: element.amount
            }]
          }
        } else {
          if (element.bid_increment_type == "BR") {
            element.bid_increments.forEach(bid => {
              if (!bid.id) {
                delete bid['id'];
              }
            });
          }
        }

        if (element?.contacts && element?.contacts.length) {
          let tempContactArray: Array<any> = [];
          element?.contacts.forEach((contact: any) => {
            if (contact?.user) {
              let tempSubContactObj: any = {
                user: contact?.user
              }
              if (contact?.id) {
                tempSubContactObj['id'] = contact?.id;
              } else {
                delete contact['id']
              }
              tempContactArray.push(tempSubContactObj);
            }
          });
          element.contacts = [];
          element.contacts = [...tempContactArray];
        }
        delete element['bid_increment_type'];
        delete element['amount'];
        element['starts_at'] = (new Date(element.starts_at)).toISOString();
        element['ends_at'] = (new Date(element.ends_at)).toISOString();
        element['inspection_starts_at'] = element.inspection_starts_at ? (new Date(element.inspection_starts_at)).toISOString() : '';
        element['inspection_ends_at'] = element.inspection_ends_at ? (new Date(element.inspection_ends_at)).toISOString() : '';
        element['payment_date'] = element.payment_date ? (new Date(element.payment_date)).toISOString() : '';
        element['asset_removal_date'] = element.asset_removal_date ? (new Date(element.asset_removal_date)).toISOString() : '';
        element['auction_type'] = value.auction_type;
        element['auction_status'] = value.auction_status;

        for (let y in element) {
          if (element[y] === '' || element[y] === null) {
            delete element[y];
          }
        }
      });
    } else {
      if ((value?.has_subauctions == 'true') && value?.subauctions && !value?.subauctions?.length) {
        this.notify.error('Please either provide subauctions or Change response to "No" for Has Sub-Auctions!!')
        return;
      }
    }

    for (let x in value) {
      if (value[x] === '' || value[x] === null) {
        delete value[x];
      }
    }
    let payload = Object.assign({}, value);
    if (this.toEdit) {
      this.auctionService.updateAuction(payload, this.auctionId).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          // if (res.status == 201) {
          this.notify.success('Auction updated successfully!!', true);
          this.router.navigate(['/admin-dashboard/auction/auction-list']);
          // }
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      );
    } else {
      this.auctionService.createAuction(payload).pipe(finalize(() => { })).subscribe(
        (res: any) => {
          // if (res.status == 201) {
          this.notify.success('Auction ID: ' + res?.id + ' created successfully!!', true);
          this.router.navigate(['/admin-dashboard/auction/auction-list']);
          // }
        },
        err => {
          console.log(err);
          this.notify.error(err.message);
        }
      );
    }
  }

  setAuctionDetails(data) {
    this.auctionForm.patchValue({
      has_subauctions: data?.has_subauctions.toString(),
      sale_type: data?.sale_type,
      auction_type: data?.auction_type,
      auction_status: data?.auction_status,
      business_vertical: data?.business_vertical,
      name: data?.name,
      starts_at: data?.starts_at,
      ends_at: data?.ends_at,
      inspection_starts_at: data?.inspection_starts_at,
      inspection_ends_at: data?.inspection_ends_at,
      manager: data?.manager?.cognito_id.toString(),
      city: data?.city,
      address: data?.address,
      credit_limit_multiplier: data?.credit_limit_multiplier,
      is_tax_included: data?.is_tax_included.toString(),
      last_minute_bid: data?.last_minute_bid,
      extended_to: data?.extended_to,
      terms: data?.terms,
      category_type: data?.category_type,
      catalog_url: data?.catalog_url,
      // bidder_undertaking_url: data?.bidder_undertaking_url,
      img_url: data?.img_url,
      registration_form_url: data?.registration_form_url,
      undertaking_form_url: data?.undertaking_form_url,
      is_active: data?.is_active,
      is_otp_enabled: data?.is_otp_enabled,
      is_offline_emd_available: data?.is_offline_emd_available,
      is_wallet_emd_available: data?.is_wallet_emd_available,
      is_online_emd_available: data?.is_online_emd_available,
      on_behalf_of: data?.on_behalf_of,
      buyers_premium: data?.buyers_premium,
      tax: data?.tax,
      payment_date: data?.payment_date,
      asset_removal_date: data?.asset_removal_date,
      additional_info: data?.additional_info,
      disclaimer: data?.disclaimer,
      bid_increment_type: data?.bid_increments[0]?.increment_type
    })
    if (data?.bid_increments[0]?.increment_type == "BR") {
      this.auctionForm.controls.amount.setValidators([]);
      this.auctionForm.controls.amount.updateValueAndValidity();
      data?.bid_increments.forEach(element => {
        this.bidArray.push(this.newBidDetails(element));
      });
    } else {
      this.auctionForm.patchValue({
        amount: data?.bid_increments[0]?.amount
      })
    }

    if (data?.contacts && data?.contacts.length) {
      this.removeAuctionContact(0);
      data?.contacts.forEach(element => {
        let obj = this.customerArray.find(f => { return f.user?.cognito_id == element?.user });
        this.auctionContact.push(this.newAuctionContactDetails(element, obj));
      });
    }

    if (data?.subauctions && data?.subauctions.length) {
      data?.subauctions.forEach((element, i) => {
        this.subAuctions.push(this.newSubAuction(element));

        if (element?.bid_increments[0]?.increment_type == "BR") {
          this.subAuctionControl[i].get('amount')?.setValidators([]);
          this.subAuctionControl[i].get('amount')?.updateValueAndValidity();
          element?.bid_increments.forEach(bid => {
            this.subAuctionBidArray(i).push(this.newBidDetails(bid));
          });
        } else {
          this.subAuctionControl[i].patchValue({
            amount: element?.bid_increments[0]?.amount
          })
        }

        if (element?.contacts && element?.contacts.length) {
          this.removeSubAuctionContact(i, 0);
          element?.contacts.forEach(ele => {
            let obj = this.customerArray.find(f => { return f.user?.cognito_id == ele?.user });
            this.subAuctionContact(i).push(this.newAuctionContactDetails(ele, obj));
          });
        }

      });
    }
  }

  getEndDate() {
    if (!this.toEdit) {
      let date = this.auctionForm?.get('starts_at')?.value;
      if (date) {
        date = new Date(date);
        // date.setDate(date.getDate() + 1);
        return date;
      }
      else return new Date();
    }
  }

  getAuctionStartDate() {
    if (!this.toEdit) {
      let date = this.auctionForm?.get('starts_at')?.value;
      if (date) {
        date = new Date(date);
        // date.setDate(date.getDate());
        return date;
      }
      else return new Date();
    }
  }

  getSubAuctionStartDate(i) {
    let subAuction = this.subAuctionControl[i] as FormGroup;
    let date = subAuction.get('starts_at')?.value;
    if (date) {
      date = new Date(date);
      date.setDate(date.getDate());
      return date;
    }
    else return new Date();
  }

  getAuctionEndDate() {
    if (!this.toEdit) {
      let date = this.auctionForm?.get('ends_at')?.value;
      if (date) {
        date = new Date(date);
        date.setDate(date.getDate());
        return date;
      }
      else return new Date();
    }
  }

}
