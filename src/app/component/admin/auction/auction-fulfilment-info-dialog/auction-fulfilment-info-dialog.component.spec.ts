import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionFulfilmentInfoDialogComponent } from './auction-fulfilment-info-dialog.component';

describe('AuctionFulfilmentInfoDialogComponent', () => {
  let component: AuctionFulfilmentInfoDialogComponent;
  let fixture: ComponentFixture<AuctionFulfilmentInfoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuctionFulfilmentInfoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionFulfilmentInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
