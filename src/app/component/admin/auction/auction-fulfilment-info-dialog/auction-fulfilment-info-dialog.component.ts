
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminMasterService } from 'src/app/services/admin-master.service';
import { SharedService } from 'src/app/services/shared-service.service';


@Component({
  selector: 'app-auction-fulfilment-info-dialog',
  templateUrl: './auction-fulfilment-info-dialog.component.html',
  styleUrls: ['./auction-fulfilment-info-dialog.component.css']
})

export class AuctionFulfilmentInfoDialogComponent implements OnInit {
  statusType: any;
  id: any;
  tableData: any = {};
  assetStatus?: string;
  isSelfInvoive: boolean = true;
  assetStatusObject = [{
    "id": 0,
    "display_name": "DRAFT"
  }, {
    "id": 1,
    "display_name": "PENDING"
  }, {
    "id": 2,
    "display_name": "APPROVED"
  }, {
    "id": 3,
    "display_name": "Sale in Progress"
  }, {
    "id": 4,
    "display_name": "REJECTED"
  }, {
    "id": 4,
    "display_name": "SOLD"
  }]
  element: any;

  constructor(public dialogRef: MatDialogRef<AuctionFulfilmentInfoDialogComponent>, @Inject(MAT_DIALOG_DATA) data: any, public sharedService: SharedService) {
    this.element = data;
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }

  getPaymentStatus(val) {
    switch (val) {
      case 1: {
        return 'Initiated'
        break;
      }
      case 2: {
        return 'Success'
        break;
      }
      case 3: {
        return 'Failure'
        break;
      }
      case 4: {
        return 'Cancelled'
        break;
      }
      case 5: {
        return 'To be realized'
        break;
      }
      case 6: {
        return 'Registration Cancelled'
        break;
      }
      default: {
        return ''
        break;
      }
    }
  }


}

