import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { NewEquipmentService } from 'src/app/services/customer-new-equipments.service';

@Component({
  selector: 'app-lead-view',
  templateUrl: './lead-view.component.html',
  styleUrls: ['./lead-view.component.css']
})
export class LeadViewComponent implements OnInit {

  public cognitoId!: any;
  public enquiryId: any;
  public enquiryDetails: any;
  public chatDetails!: Array<any>;
  public enquiryCurrentStatus: any;
  public enquiryStatusOptions: Array<any> = [
    { name: 'Open', value: 1 },
    { name: 'In Progress', value: 2 },
    { name: 'Deal Submited', value: 3 },
    { name: 'Deal in Progress', value: 4 },
    { name: 'Purchase Completed', value: 5 },
    { name: 'Rejected', value: 6 },
    { name: 'Rejected by Buyer', value: 7 }
  ];

  constructor(public route: ActivatedRoute, public router: Router, public notify: NotificationService, public spinner: NgxSpinnerService, public storage: StorageDataService, private newEquipmentService: NewEquipmentService) { }

  ngOnInit(): void {
    this.cognitoId = this.storage.getStorageData('cognitoId', false);
    this.route.queryParams.subscribe((data) => {
      this.enquiryId = data.id;
      if (this.enquiryId) {
        this.getEnquiryDetails();
        this.getChatDetails();
      } else {
        this.router.navigate(['/admin-dashboard/new-equipment-lead']);
      }
    })
  }

  getEnquiryDetails() {
    this.newEquipmentService.getEnquiryDetails(this.enquiryId).pipe(finalize(() => {   })).subscribe(
      (data: any) => {
        this.enquiryDetails = data;
        this.enquiryCurrentStatus = JSON.stringify(data.status);
      },
      err => {
        console.log(err);
      }
    )
  }

  getChatDetails() {
    let chatPayload = {
      limit: 999,
      enquiry_id__in: this.enquiryId
    }
    this.newEquipmentService.getEnquiryChatDetails(chatPayload).pipe(finalize(() => {   })).subscribe(
      (data: any) => {
        this.chatDetails = data.results;
      },
      err => {
        console.log(err);
      }
    )
  }

  getEnquiryStatus(val) {
    let Obj = this.enquiryStatusOptions.find((enq) => { return enq.value == val });
    return Obj?.name;
  }

  getEquipmentRequiredTime(val) {
    switch (val) {
      case 1: {
        return 'Immediate'
        break;
      }
      case 2: {
        return 'Within next 60 Days'
        break;
      }
      case 3: {
        return 'Not Sure'
        break;
      }
      default: {
        return 'Not Sure'
        break;
      }
    }
  }

}
