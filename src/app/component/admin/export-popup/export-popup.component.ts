import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-export-popup',
  templateUrl: './export-popup.component.html',
  styleUrls: ['./export-popup.component.css']
})
export class ExportPopupComponent implements OnInit {
  toDate;
  fromDate;
  todayDate = new Date();
  constructor( @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ExportPopupComponent>) {
  }

  ngOnInit(): void {
  }

  backClick() {
    this.dialogRef.close();
  }
  export(){
    let data = {
      "fromDate": this.fromDate,
      "toDate": this.toDate
    }
    this.dialogRef.close(data);
  }
  getDate(){
    if(this.toDate){
      return this.toDate;
    }else{
      return this.todayDate;
    }
  }
  getMaxDate(){
    if(this.fromDate){
      const date = new Date(this.fromDate);
      date.setMonth(this.fromDate.getMonth()+3);
      if(date > this.todayDate){
        return this.todayDate;
      }
      return date;
    }
    return this.todayDate;
  }
}
