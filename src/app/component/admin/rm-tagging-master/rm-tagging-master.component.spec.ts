import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RmTaggingMasterComponent } from './rm-tagging-master.component';

describe('RmTaggingMasterComponent', () => {
  let component: RmTaggingMasterComponent;
  let fixture: ComponentFixture<RmTaggingMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RmTaggingMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RmTaggingMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
