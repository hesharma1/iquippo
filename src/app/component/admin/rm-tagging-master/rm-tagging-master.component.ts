import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';

import { AdminMasterService } from 'src/app/services/admin-master.service';
import { PartnerDealerRegistrationService } from 'src/app/services/partner-dealer-registration-service.service';
import { ApiRouteService } from 'src/app/utility/app.refrence';
import { ExportExcelUtil } from 'src/app/utility/export-excel/export-excel-util';

import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { ConfirmDialogComponent } from '../new-equipment-master/location-master/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'app-rm-tagging-master',
  templateUrl: './rm-tagging-master.component.html',
  styleUrls: ['./rm-tagging-master.component.css']
})
export class RmTaggingMasterComponent implements OnInit,AfterViewInit
  {
     countryData: any[] = [];
    // allCountries: any[] = [];
     countryDataSource = new MatTableDataSource<any>([]);
    // countryDisplayedColumns = ['name', 'code', 'flag', 'isd_code', 'actions']; 
    // @ViewChild('countryPaginator') countryPager!: MatPaginator;
  
     stateData: any[] = [];
    // allStates: any[] = [];
     stateDataSource = new MatTableDataSource<any>([]);
    // stateDisplayedColumns = ['country_name', 'name', 'actions'];  
    // @ViewChild('statePaginator') statePager!: MatPaginator;
    form: FormGroup;
    locationData: any[] = [];
    RmList: any;
      CityDataSource = new MatTableDataSource<any>([]);
    PincodeDataSource = new MatTableDataSource<any>([]);
    // CityDisplayedColumns = ['Country','state_name', 'name', 'actions'];
    PincodeDisplayedColumns = ['name','Country','states', 'cities','actions'];
    // @ViewChild('cityPaginator') cityPager!: MatPaginator;
    //  @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('pincodePager') pincodePager!: MatPaginator;
    total_count:any = 0;
    showCountryForm: boolean = false;
    showStateForm: boolean = false;
    showCityForm: boolean = false;
    showPincodeForm: boolean = false;
  
    FlagPopUp: number = 0;
  multipleCities: any = [];
  isEdit: boolean = false;
  activePage: number = 1;
  pageSize: number = 10;
  rmRecord: any;
  selectedTblIndex: number = 0;
  isReassign: boolean = false;
  
    constructor(
      //private dialog: MatDialog,
      private fb: FormBuilder,private adminMasterService: AdminMasterService,private apiRouteService:ApiRouteService,private partnerDealerRegService:PartnerDealerRegistrationService,private dialog: MatDialog,private notify: NotificationService,private ref: ChangeDetectorRef
    ) {
      this.form = this.fb.group({
        id: [null],
        Country: [null,Validators.required],
        State: [0],
        rm: [null, Validators.required,],
        newrm: [null],
        CityName: ["", Validators.compose([ Validators.maxLength(50)])],
        //Pincode: ["", Validators.compose([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)])]
      });
    }
  
    ngOnInit(): void {
       this.renderCountryData('');
       //this.renderStateData('');
       //this.renderCityData('');
      this.renderPincodeData('');
      this.getRmList();
     // this.getStates('1');
      this.showCountryForm = false;
      this.showStateForm = false;
      this.showCityForm = false;
      this.showPincodeForm = false;
    }
    getRmList() {
      this.partnerDealerRegService.getRmList(this.apiRouteService.partnerContactRegistered).subscribe(
        (res: any) => {
          console.log(res)
          this.RmList = res.results
        })
    }
    onPageChange(event: PageEvent) {
      this.activePage = event.pageIndex + 1;
      this.pageSize = event.pageSize;
      this.renderPincodeData('');
     // this.getPage();
    }
  
    okClick() {
      let addPincodeRequest = {
        country:this.form.value.Country,
        state: this.form.value.State,
        cities: this.form.value.CityName,
      }
      if(this.isEdit){
      this.deletePincode(this.rmRecord,this.rmRecord.user)
      }
      else{
      this.adminMasterService.postRmTaggingMaster(addPincodeRequest,this.form.get('rm')?.value,this.isEdit).subscribe((resp:any) => {
        // this.ChdOutput.emit();
        if(resp.Error!=undefined && resp.Error !=''){
          this.notify.error('RM Cannot belong to multiple countries');
        }
        else{
        this.notify.success('RM tagged successfully.');
        //if(this.isEdit){
          this.showPincodeForm = false;
      //}
        this.renderPincodeData('');
        this.form.reset();
        this.multipleCities=[];
        this.isEdit = false;
        }
      })
      }
    }
    getStates(value: any) {
      this.renderStateData('country__id__in=' + value + '&limit=999')
      //this.cityDataSource = [];
      let country = this.countryData.filter(x => x.id == value).map(x => x.name)[0];
      if (country != 'India') {
        this.form.get('Pincode')?.clearValidators();
        this.form.get('Pincode')?.updateValueAndValidity();
        this.form.get('Pincode')?.setValidators(Validators.required);
        this.form.get('Pincode')?.updateValueAndValidity();
      }
      else {
        this.form.get('Pincode')?.setValidators([Validators.required, Validators.pattern(/^[1-9][0-9]{5}$/)]);
        this.form.get('Pincode')?.updateValueAndValidity();
      }
    }

    add(type : string)
    {
     if(type === 'Country')
     {
      this.showCountryForm = true;
     }
     else if(type === 'State')
     {
      this.showStateForm = true;
     }
     else if(type === 'City')
     {
      this.showCityForm = true;
     }
     else if(type === 'Pincode')
     {
      this.showPincodeForm = true;
     }
    }
  
    async GetChildData(StrAction: string , event ,SelectedTblIndex: number,element:any,reassign:boolean)
    {   
      this.rmRecord = element; 
      this.isReassign = reassign;
      // if(StrAction == 'CountryCreate')
      // {
      //   this.showCountryForm = false;
      //    this.renderCountryData('');
      // }
      // else if (StrAction == 'StateCreate')
      // {
      //   this.showStateForm = false;
      //   this.renderStateData('');
      // }
      // else if (StrAction == 'cityCreate')
      // {
      //   this.showCityForm = false;
      //   this.renderCityData('');
      // }
      if (StrAction == 'PincodeCreate')
      {
        this.showPincodeForm = false;
        this.renderPincodeData('');
      }
      // else if(StrAction == 'CountryEdit')
      // {
      //   //let index = this.countryDataSource.filteredData[SelectedTblIndex]; //.indexOf(SelectedTblIndex);
      //   //index = index !== -1 ? index : SelectedTblIndex;
      //   const countryData = this.countryDataSource.filteredData[SelectedTblIndex];
      //   this.showCountryForm = false;
      //   this.ref.detectChanges();
      //   this.showCountryForm = true;
      //   let obj = {
      //     id : countryData.id,
      //     Name : countryData.name,
      //     Code: countryData.code,
      //     Flag: countryData.flag,
      //     ISDCode: countryData.isd_code
      //   }
  
      //   localStorage.removeItem("CountryData");
      //   localStorage.clear();
  
      //   localStorage.setItem("CountryData" , JSON.stringify(obj));
      // }
      // else if (StrAction == 'StateEdit') // ***
      // {
      //   this.showStateForm = false;
      //   this.ref.detectChanges();
      //   this.showStateForm = true;
      //   const StateData = this.stateDataSource.filteredData[SelectedTblIndex];
  
      //   let obj = {
      //     id: StateData.id,
      //     Country: StateData.country.id,
      //     StateName: StateData.name
      //   }
  
      //   localStorage.removeItem("StateData");
      //   localStorage.clear();
  
      //   localStorage.setItem("StateData" , JSON.stringify(obj));
      // }
      // else if (StrAction == 'CityEdit')
      // {
      //   this.showCityForm = false;
      //   this.ref.detectChanges();
      //   this.showCityForm = true;
      //   const cityDataSource = this.CityDataSource.filteredData[SelectedTblIndex];
  
      //   let obj = {
      //     id : cityDataSource.id,
      //     Country: cityDataSource.state.country.id,
      //     State: cityDataSource.state.id,
      //     cityName: cityDataSource.name        
      //   }
  
      //   localStorage.removeItem("CityData");
      //   localStorage.clear();
  
      //   localStorage.setItem("CityData" , JSON.stringify(obj));
      // }
      else if (StrAction == 'PincodeEdit')
      {
        this.isEdit = true;
        this.showPincodeForm = true;
        this.ref.detectChanges();
       // this.showPincodeForm = true;
        const pincodeDataSource = this.PincodeDataSource.filteredData[SelectedTblIndex];
        this.selectedTblIndex = SelectedTblIndex;
  
        // let obj = {
        //   id : pincodeDataSource.id,
        //   Country: pincodeDataSource.city.state.country.id,
        //   State: pincodeDataSource.city.state.id,
        //   CityName: pincodeDataSource.city.id,
        //   Pincode: pincodeDataSource.pin_code
        // }
        if(pincodeDataSource.states.length>0 || (pincodeDataSource.states.id!=undefined && pincodeDataSource.states.id !=0)){
        await this.getStates(pincodeDataSource.states.country_id);
        this.form.get('State')?.patchValue(pincodeDataSource.states.id);
        this.form.get('Country')?.patchValue(pincodeDataSource.states.country_id);
        await this.getCities(pincodeDataSource.states.id);
        pincodeDataSource.states?.cities?.forEach(element => {
          this.multipleCities.push(element.id);
        });
        }
        else{
          this.form.get('Country')?.patchValue(pincodeDataSource.country.id);
          await this.getStates(pincodeDataSource.country.id);
        }
        //this.form.get('cities')?.patchValue(pincodeDataSource.states?.cities);
        this.form.get('rm')?.patchValue(pincodeDataSource.user);
        // localStorage.removeItem("PinCodeData");
        // localStorage.clear();
  
        // localStorage.setItem("PinCodeData" , JSON.stringify(obj));
      }
  
    }
    
    ngAfterViewInit() {
        this.PincodeDataSource.paginator = this.pincodePager;//this.paginator;
      //country
      // this.countryDataSource.paginator = this.countryPager;
      // this.countryPager.page.subscribe((e: any) => {
      //   this.renderCountryData(this.getQueryParams('Country', e.pageSize));
      // });
  
      //state
      // this.stateDataSource.paginator = this.statePager;
      // this.statePager.page.subscribe((e: any) => {
      //   this.renderStateData(this.getQueryParams('State', e.pageSize));     
      // });
  
      //City
      // this.CityDataSource.paginator = this.cityPager;
      // this.cityPager.page.subscribe((e: any) => {
      //   this.renderCityData(this.getQueryParams('City', e.pageSize));
      // });
  
      //Pincode
      // this.PincodeDataSource.paginator = this.pincodePager;
      // this.pincodePager.page.subscribe((e: any) => {
      //   this.renderPincodeData(this.getQueryParams('Pincode', e.pageSize));
      // });
    }
    getQueryParams(type: string, pageSize: any): string {
       let queryParam = '';
      // if (type === 'Country')
      //   if (pageSize) {
      //     queryParam = `limit=${pageSize}&page=${this.countryPager.pageIndex + 1}&ordering=-id`;
      //   }
      //   else {
      //     queryParam = `page=${this.countryPager.pageIndex + 1}&ordering=-id`;
      //   }
      // else  if (type === 'State')
      //   if (pageSize) {
      //     queryParam = `limit=${pageSize}&page=${this.statePager.pageIndex + 1}&ordering=-id`;
      //   }
      //   else {
      //     queryParam = `page=${this.statePager.pageIndex + 1}&ordering=-id`;
      //   }
      // else if (type === 'City')
      //   if (pageSize) {
      //     queryParam = `limit=${pageSize}&page=${this.cityPager.pageIndex + 1}&ordering=-id`;
      //   }
      //   else {
      //     queryParam = `page=${this.cityPager.pageIndex + 1}&ordering=-id`;
      //   }
      // else if (type === 'Pincode')
        if (pageSize) {
          queryParam = `limit=${pageSize}&page=${this.pincodePager.pageIndex + 1}&ordering=-id`;
        }
        else {
          queryParam = `page=${this.pincodePager.pageIndex + 1}&ordering=-id`;
        }
  
      return queryParam;
    }
  
    getCountryData(queryParams :string): Observable<any> {
      return this.adminMasterService
        .getCountryMaster(queryParams)
    }
  
     renderCountryData(queryParams : string) {
       this.getCountryData(queryParams).subscribe((data: any) => {
         this.countryData = data.results;
         this.countryDataSource = new MatTableDataSource<any>(this.countryData);
    //     this.countryPager.length = data.count;
       });
     }
  
    // searchCountries(val: string) {
    //   this.renderCountryData(`search=${val}`);
    // }
  
    
    getStateData(queryParams: string): Observable<Object> {
      return this.adminMasterService
        .getStateMaster(queryParams)
  
    }
  
    exportCountryData(){    
      this.getCountryData(`limit=-1`).subscribe((res: any) => {
        const excelData = res.results.map((r: any) => {
           return {
            "Country": r.name, 
            "Code" :  r.code ,
            "ISD Code": r.isd_code
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "country-master");
      })
    }
  
    exportStateData(){    
      this.getStateData(`limit=-1`).subscribe((res: any) => {
        const excelData = res.results.map((r: any) => {
          return {
            "Country": r.country.name,
            "State": r.name
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "state-master");
      })
    }
  
    renderStateData(queryParams :string){
      this.getStateData(queryParams).subscribe((res: any) => {
        this.stateData = res.results;
        this.stateDataSource = new MatTableDataSource<any>(this.stateData);
     //   this.statePager.length = res.count;
      })
    }
  
    // searchStates(val: string) {
    //   this.renderStateData(`search=${val}`);
    // }
  
    getCityData(queryParams: string): Observable<Object> {
      return this.adminMasterService
        .getCityMaster(queryParams)
    }
  
    exportCityData(){
      this.getCityData(`limit=-1`).subscribe((res: any) => {
        const excelData = res.results.map((r: any) => {        
          return {
            "Country" : r.state.country.name,
            "State": r.state.name,
            "City": r.name
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "city-master");
      })
    }
  
    exportPincodeData(){
      this.getRmTaggingMaster(`limit=-1`).subscribe((res: any) => {
        const excelData = res.results.map((r: any) => {       
          return {
            "Country" : r.city.state.country.name,
            "State": r.city.state.name,
            "City": r.city.name,
            "Pincode": r.pin_code,
            "RM":r.rm_name
          }
        });
        ExportExcelUtil.exportArrayToExcel(excelData, "Pincode-master");
      })
    }
  
    renderCityData(queryParams : string){
      this.getCityData(queryParams).subscribe((res: any) => {
        this.locationData = res.results;     
          this.CityDataSource = new MatTableDataSource<any>(this.locationData);
       //   this.cityPager.length = res.count;
      })
    }
    getCities(value: any) {
      this.renderCityData('state__id__in=' + value + '&limit=999')
    }
  
    getRmTaggingMaster(queryParams: string,payload?:any): Observable<Object> {
      return this.adminMasterService.getRmTaggingMaster(queryParams,payload)
    }
  Close(){
    this.showPincodeForm = false;
    this.form.reset();
    this.isReassign = false;
    this.multipleCities=[];
  }
    renderPincodeData(queryParams : string){
      let payload ={};
      payload = {
        page: this.activePage,
       limit: 999
      };
      this.getRmTaggingMaster(queryParams,payload).subscribe((res: any) => {    
         let rmTaggingData = res.results;
          let newRmTaggingData:any = [];
          rmTaggingData.forEach(outerelement => {
           
            outerelement.states.forEach(element => {
                 var newObj:any = {};
            newObj.first_name = outerelement.first_name;
            newObj.last_name = outerelement.last_name;
            newObj.user = outerelement.user;  
            newObj.mobile_number = outerelement.mobile_number;
                newObj.states = element;
                newRmTaggingData.push(newObj); 
              });
              outerelement.countries.forEach(element => {
                var newObj:any = {};
           newObj.first_name = outerelement.first_name;
           newObj.last_name = outerelement.last_name;
           newObj.user = outerelement.user;  
           newObj.mobile_number = outerelement.mobile_number;
               newObj.states = [];
               newObj.country = element;
               newRmTaggingData.push(newObj); 
             });
          });    
          
          console.log("rmtaggingfinaldata",newRmTaggingData);     
          this.PincodeDataSource = new MatTableDataSource<any>(newRmTaggingData);
          this.pincodePager.length = newRmTaggingData.length;
          this.PincodeDataSource.paginator = this.pincodePager;
         // this.pincodePager.pageSize = 10;
          //this.total_count =  newRmTaggingData.length;
      })
    }
  
    
    // searchLocations(val: string) {
    //   this.renderCityData(`search=${val}`);
    // }
  
    
  
    // deleteCountry(id: any) {    
    //   this.dialog
    //     .open(ConfirmDialogComponent, {
    //       width: '500px'
    //     })
  
    //     .afterClosed()
    //     .subscribe((data) => {
    //       if (data) {          
    //         this.adminMasterService.deleteCountryMaster(id).subscribe(() => {
    //           this.notify.success('Country deleted successfully.');
    //           this.renderCountryData('');
    //         });
    //       }
    //     });
    // }
    // deleteState(id: any) {    
    //   this.dialog
    //     .open(ConfirmDialogComponent, {
    //       width: '500px'
    //     })
  
    //     .afterClosed()
    //     .subscribe((data) => {
    //       if (data) {          
    //         this.adminMasterService.deleteStateMaster(id).subscribe(() => {
    //            this.renderStateData('');
    //            this.notify.success('State deleted successfully.');
    //          });
    //       }
    //     });
    // }
    // deleteCity(id: any) {    
    //   this.dialog
    //     .open(ConfirmDialogComponent, {
    //       width: '500px'
    //     })
  
    //     .afterClosed()
    //     .subscribe((data) => {
    //       if (data) {          
    //         this.adminMasterService.deleteCityMaster(id).subscribe(() => {
    //           this.renderCityData('');
    //           this.notify.success('City deleted successfully.');
    //         });
    //       }
    //     });
    // }
    deletePincode(obj: any,id:any) {  
      let addPincodeRequest = {
        country:this.form.value.Country,
        state: this.form.value.State,
        cities: this.form.value.CityName,
      } 
      let Request:any = {};
      if( obj.states?.id !=undefined)
      Request.state = obj.states.id
      else{
      Request.state = 0;
      }
      Request.cities=[];
      if( obj.states?.id !=undefined)
      obj.states?.cities.forEach(element => {
        Request.cities.push(element.id)
      });
      else{
        Request = {};
        Request.country = obj.country.id;
      }
      this.adminMasterService.deleteRmTaggingMaster(Request,id).subscribe(() => {
        this.adminMasterService.postRmTaggingMaster(addPincodeRequest,this.form.get('rm')?.value,this.isEdit).subscribe((resp:any) => {
          // this.ChdOutput.emit();
          if(resp.Error!=undefined && resp.Error !=''){
            this.notify.error('RM Cannot belong to multiple countries');
          }
          else{
          this.notify.success('RM tagged successfully.');
          //if(this.isEdit){
            this.showPincodeForm = false;
        //}
          this.renderPincodeData('');
          this.form.reset();
          this.multipleCities=[];
          this.isEdit = false;
          }
        })
        //this.renderPincodeData('');
        //this.notify.success('RM deleted successfully.');
      });
      // this.dialog
      //   .open(ConfirmDialogComponent, {
      //     width: '500px'
      //   })
        
      //   .afterClosed()
      //   .subscribe((data) => {
      //     if (data) {
      //         this.adminMasterService.deleteRmTaggingMaster(Request,id).subscribe(() => {
      //         this.renderPincodeData('');
      //         this.notify.success('RM deleted successfully.');
      //       });
      //     }
      //   });
    }
    
    reassignUser() {  
      // if(this.form.value.newrm!=undefined && this.form.value.newrm !=''){
      //   this.notify.warn('Please select new RM.');
      // }
      let reassignRequest = {
       from:this.form.value.rm,
       to:this.form.value.newrm
      } 
     
      this.adminMasterService.reassignRM(reassignRequest).subscribe(() => {
        this.Close();
        this.renderPincodeData('');
        this.notify.success('Reassignment done successfully.');
      });
    
    }
    search(value : any,type : string)
    {   
    //  if(type === 'Country')
    //  {
    //   this.renderCountryData(`search=${value}`);
    //  }
    //  else if(type === 'City')
    //  {
    //   this.renderCityData(`search=${value}`);
    //  }
    //  else if(type === 'State')
    //  {
    //   this.renderStateData(`search=${value}`);
    //  }
     if(type === 'Pincode')
     {
      this.renderPincodeData(`search=${value}`);
     }
     
     
  
    }
  
  
  }
  