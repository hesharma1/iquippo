import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { AwsAuthService } from 'src/app/auhtentication/aws-auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NotificationService } from 'src/app/utility/toastr-notification/toastr-notification.service';
import { CheckPasswordMatch } from '../../utility/custom-validators/forgot-password';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  public changePswd_Form: FormGroup;
  public userDetails:any;

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService, public storage: StorageDataService, private router: Router, private notify: NotificationService, private awsConfig: AwsAuthService) {

    this.changePswd_Form = this.fb.group({
      old_password: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)])],
      new_password: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)])],
      cnf_password: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)])],
    }, {
      validator: [CheckPasswordMatch('new_password', 'cnf_password')]
    })
  }


  ngOnInit(): void {
    this.userDetails = this.storage.getLocalStorageData('userData',true);
  }

  changePassword() {
    //this.spinner.show();
    let payload = this.changePswd_Form.value;
    this.awsConfig.awsChangePassword(this.userDetails.attributes.preferred_username, payload).pipe(finalize(() => {   })).subscribe(
      data => {
         
        this.notify.success('Password change successfully!!');
        this.router.navigate(['/dashboard']);
      },
      err => {
        console.log(err);
        this.notify.error(err.message);
      }
    )
  }

}
