import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewEquipmentLeadComponent } from '../component/admin/new-equipment-lead/new-equipment-lead.component';
const routes: Routes = [
  {path:'new-equipment-lead',component:NewEquipmentLeadComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewEquipmentLeadRoutingModule { }
