import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkCurationComponent } from '../component/admin/bulk-upload/components/bulk-curation/bulk-curation.component';
import { BulkListingComponent } from '../component/admin/bulk-upload/components/bulk-listing/bulk-listing.component';
import { BulkUploadEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-enterprise/bulk-upload-enterprise.component';
import { ConfirmedPopupComponent } from '../component/customer/bulk-upload/confirmed-popup/confirmed-popup.component';
import { ProductDetailsComponent } from '../component/customer/bulk-upload/product-details/product-details.component';
import { ReviewAssetsComponent } from '../component/customer/bulk-upload/review-assets/review-assets.component';
import { UploadFilesComponent } from '../component/customer/bulk-upload/upload-files/upload-files.component';

const routes: Routes = [
  {path:'upload-files', component:UploadFilesComponent},
  {path:'bulk-upload-enterprise', component:BulkUploadEnterpriseComponent},
  {path:'product-details', component:ProductDetailsComponent},
  {path:'review-assets', component:ReviewAssetsComponent},
  {path:'confirmed', component:ConfirmedPopupComponent},
  {path:'bulk-curation', component:BulkCurationComponent}
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BulkUploadRoutingModule { }
