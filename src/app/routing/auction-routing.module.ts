import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuctionListingComponent } from '../component/admin/auction/auction-listing/auction-listing.component';
import { CreateAuctionComponent } from '../component/admin/auction/create-auction/create-auction.component';
import { CreateLotComponent } from '../component/admin/auction/create-lot/create-lot.component';
import { AssetsAuctionComponent } from '../component/admin/auction/assets-auction/assets-auction.component';
import { AuctionFulfilmentComponent } from '../component/admin/auction/auction-fulfilment/auction-fulfilment.component';
import { AuctionPaymentHistoryForAdminComponent } from '../component/admin/auction/auction-payment-history-for-admin/auction-payment-history-for-admin.component';

const routes: Routes = [
  {path:'auction-list', component:AuctionListingComponent},
  {path:'create-auction', component:CreateAuctionComponent},
  {path:'create-lot', component:CreateLotComponent},
  {path:'assets-auction', component:AssetsAuctionComponent},
  {path:'auction-fulfilment', component:AuctionFulfilmentComponent},
  {path:'auction-payment-history', component:AuctionPaymentHistoryForAdminComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuctionRoutingModule { }
