
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradeAssetDetailsComponent } from '../component/customer/dashboard/my-trade/seller/trade-asset-detail/trade-asset-details.component';
import { TradeDetailsDashboardComponent } from '../component/customer/dashboard/my-trade/trade-details-dashboard.component';
import { ViewBidComponent } from '../component/customer/dashboard/my-trade/seller/view-bids/view-bid.component';

const routes: Routes = [
  {
    path: '',
    component: TradeDetailsDashboardComponent,
    children: [
      {
        path: 'trade-details-dashboard',
        component: TradeAssetDetailsComponent
      },
      {
        path: 'view-trade-details',
        component: ViewBidComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerTradeDetailsRoutingModule { }
