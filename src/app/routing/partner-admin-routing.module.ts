import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePartnerContactDetailsComponent } from './../component/admin/partner-management/create-partner-contact-details/create-partner-contact-details.component';
import { PartnerContactListComponent } from '../component/admin/partner-management/partner-contact-list/partner-contact-list.component';
import { PartnerEntityListComponent } from '../component/admin/partner-management/partner-entity-list/partner-entity-list.component';
import { CreatePartnerEntityComponent } from './../component/admin/partner-management/create-partner-entity-details/create-partner-entity/create-partner-entity.component';
import { CreatePartnerEntity2Component } from './../component/admin/partner-management/create-partner-entity-details/create-partner-entity2/create-partner-entity2.component';
import { PartnerManagementBankDetailsComponent } from './../component/admin/partner-management/partner-management-bank-details/partner-management-bank-details.component';
const routes: Routes = [

  {
    path: 'partner-entity-list',
    component:PartnerEntityListComponent
  },
  {
    path: 'partner-bank-management/:id',
    component:PartnerManagementBankDetailsComponent
  },
  {
    path: 'partner-contact-list',
    component:PartnerContactListComponent
  },
  {
    path: 'create-partner-contact',
    component:CreatePartnerContactDetailsComponent
  },
  {
    path: 'create-partner-entity',
    component:CreatePartnerEntityComponent
  },
  {
    path: 'preference/:id',
    component:CreatePartnerEntity2Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class PartnerAdminRoutingModule { }
