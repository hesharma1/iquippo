import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RealizationComponent } from '../component/admin/accounts/realization/realization.component';
import { RefundOfflineComponent } from '../component/admin/accounts/refund-offline/refund-offline.component';
import { RefundOnlineComponent } from '../component/admin/accounts/refund-online/refund-online.component';

const routes: Routes = [
  {path:'realization', component:RealizationComponent},
  {path:'online-refund', component:RefundOnlineComponent},
  {path:'offline-refund', component:RefundOfflineComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
