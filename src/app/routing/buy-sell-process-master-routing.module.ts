import { AssetSaleChargeMasterComponent } from './../component/admin/buy-sell-process-master/components/asset-sale-charge-master/asset-sale-charge-master.component';
import { MarkUpPriceMasterComponent } from './../component/admin/buy-sell-process-master/components/mark-up-price-master/mark-up-price-master.component';
import { EmdMasterComponent } from '../component/admin/buy-sell-process-master/components/emd-master/emd-master.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaleMasterComponent } from '../component/admin/buy-sell-process-master/components/sale-master/sale-master.component';
import { EmdAuctionMasterComponent } from '../component/admin/buy-sell-process-master/components/emd-auction-master/emd-auction-master.component';

const routes: Routes = [
  {path: 'emd-configuration-master' , component: EmdMasterComponent },
  {path: 'emd-auction-master' , component: EmdAuctionMasterComponent },
  { path: 'markup-price', component: MarkUpPriceMasterComponent },
  { path: 'asset-sale-charge', component: AssetSaleChargeMasterComponent },
  { path: 'sell-process', component: SaleMasterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuySellProcessMasterRoutingModule {}
