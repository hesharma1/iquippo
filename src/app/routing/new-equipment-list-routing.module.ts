import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewEquipmentDetailComponent } from '../component/customer/new-equipment-list/new-equipment-detail/new-equipment-detail.component';
import { NewEquipmentListingComponent } from '../component/customer/new-equipment-list/new-equipment-listing/new-equipment-listing.component';
import { NewEquipmentComponent } from '../component/customer/new-equipment-list/new-equipment/new-equipment.component';
import { CompareComponent } from '../component/shared/compare/compare.component';

const routes: Routes = [
  {path:'dashboard',component:NewEquipmentComponent},
  {path:'equipment-list',component:NewEquipmentListingComponent},
  {path:'equipment-details/:id',component:NewEquipmentDetailComponent},
  {path:'compare', component:CompareComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewEquipmentListRoutingModule { }
