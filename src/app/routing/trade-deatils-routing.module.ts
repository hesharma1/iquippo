
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradeAssetDetailsComponent } from '../component/admin/trade-details/trade-asset-details/trade-asset-details.component';
import { TradeDetailsDashboardComponent } from '../component/admin/trade-details/trade-details-dashboard.component';
import { ViewBidComponent } from '../component/admin/trade-details/view-bid/view-bid.component';

const routes: Routes = [
  {
    path: '',
    component: TradeDetailsDashboardComponent,
    children: [
      {
        path: 'trade-details-dashboard',
        component: TradeAssetDetailsComponent
      },
      {
        path: 'view-trade-details',
        component: ViewBidComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TradeDetailsRoutingModule { }
