import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkUploadEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-enterprise/bulk-upload-enterprise.component';
import { AssetvaluatedComponent } from '../component/valuation/assetvaluated/assetvaluated.component';
import { EquipvalmadeeasyComponent } from '../component/valuation/equipvalmadeeasy/equipvalmadeeasy.component';
import { OurvaluationservicesComponent } from '../component/valuation/ourvaluationservices/ourvaluationservices.component';
import { RaiseinstavaluationenquiryComponent } from '../component/valuation/raiseinstavaluationenquiry/raiseinstavaluationenquiry.component';
import { ValuationdetailsComponent } from '../component/valuation/valuationdetails/valuationdetails.component';

const routes: Routes = [
  { path:'asset-valuated', component:AssetvaluatedComponent },
  { path:'equipval', component:EquipvalmadeeasyComponent },
  { path:'our-val', component:OurvaluationservicesComponent },
  { path:'raise-insta-val/:unique_control_number', component:RaiseinstavaluationenquiryComponent },
  { path:'val-details/:unique_control_number', component:ValuationdetailsComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ValuationRoutingModule { }
