import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicalInfoMasterUsedComponent } from '../component/admin/new-equipment-master/technical-info-master-used/technical-info-master-used.component';
import { BasicDetailsComponent } from '../component/customer/new-equipment/basic-details/basic-details.component';
import { OffersComponent } from '../component/customer/new-equipment/offers/offers.component';
import { ProductsListingComponent } from '../component/customer/new-equipment/products-listing/products-listing.component';
import { SellEquipmentComponent } from '../component/customer/new-equipment/sell-equipment/sell-equipment.component';
import { VisualsComponent } from '../component/customer/new-equipment/visuals/visuals.component';

const routes: Routes = [
  { path:'sell-equipment', component:SellEquipmentComponent},
  { path:'basic-details/:id', component:BasicDetailsComponent},
  { path:'offers/:id', component:OffersComponent},
  { path:'visuals/:id', component:VisualsComponent},
  { path:'products', component:ProductsListingComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewEquipmentRoutingModule { }
