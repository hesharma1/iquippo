import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OldEquipmentDetailsComponent } from '../component/customer/used-equipment-list/old-equipment-details/old-equipment-details.component';
import { OldEquipmentComponent } from '../component/customer/used-equipment-list/old-equipment/old-equipment.component';
import { UsedEquipmentListingComponent } from '../component/customer/used-equipment-list/used-equipment-listing/used-equipment-listing.component';
import { CompareComponent } from '../component/shared/compare/compare.component';

const routes: Routes = [
 {path:'dashboard',component:OldEquipmentComponent},
 {path:'equipment-list',component:UsedEquipmentListingComponent},
 {path:'equipment-details/:id',component:OldEquipmentDetailsComponent},
 {path:'compare', component:CompareComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsedEquipmentListRoutingModule { }
