import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewProductListsComponent } from '../component/admin/product/new-product-lists/new-product-lists.component';
import { UsedProductListsComponent } from '../component/admin/product/used-product-lists/used-product-lists.component';
import { UsedProductUploadComponent } from './../component/admin/product/used-product-upload/used-product-upload.component';
import { NewProductUploadComponent } from './../component/admin/product/new-product-upload/new-product-upload.component';
const routes: Routes = [
  {path:'new-products', component:NewProductListsComponent},
  {path:'used-products', component:UsedProductListsComponent},
  {path:'used-products-upload', component:UsedProductUploadComponent},
  {path:'new-products-upload', component:NewProductUploadComponent},

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminProductRoutingModule { }