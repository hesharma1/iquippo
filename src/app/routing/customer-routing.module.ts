import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../component/customer/dashboard/dashboard/dashboard.component';
import { MyWalletComponent } from '../component/customer/dashboard/my-wallet/my-wallet.component';
import { PaymentComponent } from '../component/customer/payment/payment.component';
import { MyDemoComponent } from '../component/customer/dashboard/my-demo/my-demo.component';
import { MyTransactionComponent } from '../component/customer/dashboard/my-transaction/my-transaction.component';
import { ProductListingComponent } from '../component/customer/dashboard/product-listing/product-listing.component';
import {AssetMasterComponent} from '../component/customer/dashboard/asset-master/asset-master.component';
import { MyEnquiriesComponent } from '../component/customer/dashboard/my-enquiries/my-enquiries.component';
import { MyAgreementsComponent } from '../component/customer/dashboard/my-agreements/my-agreements.component';
import { EnquiryChatComponent } from '../component/customer/dashboard/enquiry-chat/enquiry-chat.component';
import { ChangePasswordComponent } from '../component/change-password/change-password.component';
import { MyInstaValuationRequestComponent } from '../component/customer/dashboard/my-insta-valuation-request/my-insta-valuation-request.component';
import { UserManagementComponent } from '../component/user-management/user-management.component';
import { WatchlistComponent } from '../component/customer/watchlist/watchlist.component';
import { AdvancedValuationRequestComponent } from '../component/customer/dashboard/my-advanced-valuation-request/my-advanced-valuation-request.component';
import { ESignTemplateComponent } from '../component/customer/e-sign-template/e-sign-template.component';
import { AuctionPaymentHistoryComponent } from '../component/customer/dashboard/auction-payment-history/auction-payment-history.component';
import { AuctionFulfilmentForUserComponent } from '../component/customer/auction-fulfilment-for-user/auction-fulfilment-for-user.component';
import { MyselfdetailsComponent } from '../component/myselfdetails/myselfdetails.component';
import { AuthGuard } from '../guards/auth.guard';
import { CorporatedetailsComponent } from '../component/corporatedetails/corporatedetails.component';
import { CompreferenceComponent } from '../component/compreference/compreference.component';
import { BankingComponent } from '../component/banking/banking.component';
import { MyEnterpriseValuationComponent } from '../component/customer/dashboard/my-enterprise-valuation/my-enterprise-valuation.component';
import { BulkUploadListEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-list-enterprise/bulk-upload-list-enterprise.component';
import { BulkListingComponent } from '../component/admin/bulk-upload/components/bulk-listing/bulk-listing.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'wallet',
        component:MyWalletComponent
      },
      {
        path : 'my-demo',
        component : MyDemoComponent
      },
      {
        path :'my-transaction',
        component :MyTransactionComponent
      },
      {
        path :'auction-payment-history',
        component :AuctionPaymentHistoryComponent
      },
      {
        path :'auction-fulfilment-list',
        component :AuctionFulfilmentForUserComponent
      },
      {
        path: 'used-products',
        component: ProductListingComponent
      },
      {
        path: 'MyAssets',
        component:AssetMasterComponent
      },
      {
        path:'my-enquiries',
        component:MyEnquiriesComponent
      },
      {
        path:'my-agreements',
        component:MyAgreementsComponent
      },
      {
        path: 'enquiry-chat',
        component: EnquiryChatComponent,
      },
      {
        path: 'profile',
        component: MyselfdetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'corporatedetails',
        component: CorporatedetailsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'communication',
        component: CompreferenceComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'bankingdetails',
        component: BankingComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'my-trade',
        loadChildren: () =>
          import('../modules/customer-trade-details.module').then((m) => m.TradeDetailsModule),
      }
      ,
      {
        path: 'partner-dealer',
        loadChildren: () =>
          import('../modules/partner-dealer-registration.module').then((m) => m.PartnerDealerRegistrationModule),
      }
      ,
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'insta-valuation-request',
        component: MyInstaValuationRequestComponent
      },
      {
        path: 'advanced-valuation-request',
        component: AdvancedValuationRequestComponent
      },
      
      {
        path: 'customer-management',
        component: UserManagementComponent
      },
      {
        path: 'watchlist',
        component: WatchlistComponent,
      },
      {
        path: 'e-sign-template/:id',
        component: ESignTemplateComponent,
      },
      {
        path: 'my-enetrprise-valuation-request',
        component: MyEnterpriseValuationComponent,
      },
      { path: 'bulk-listing', component: BulkListingComponent },
      { path: 'bulk-listing-enterprise', component: BulkUploadListEnterpriseComponent },
    
    ],
  },
  {
    path: 'payment',
    component: PaymentComponent,
  },
 
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
