import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientAgreementComponent } from '../component/admin/client-agreement/client-agreement.component';

const routes: Routes = [
  { path: 'agreement', component: ClientAgreementComponent },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ClientManagementRoutingModule {
 
 }
