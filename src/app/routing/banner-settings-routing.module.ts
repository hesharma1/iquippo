import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BannerSettingsComponent } from "../component/admin/banner-settings/banner-settings.component";

const routes : Routes = [
    {path : 'banner-settings', component: BannerSettingsComponent},
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class BannerSettingsRoutingModule { }