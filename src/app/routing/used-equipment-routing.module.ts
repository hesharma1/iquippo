import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellEquipmentComponent } from 'src/app/component/customer/used-equipment/sell-equipment/sell-equipment.component';
import { BasicDetailsComponent } from 'src/app/component/customer/used-equipment/basic-details/basic-details.component';
import { VehicleDetailsComponent } from 'src/app/component/customer/used-equipment/vehicle-details/vehicle-details.component';
import { VisualsComponent } from 'src/app/component/customer/used-equipment/visuals/visuals.component';

const routes: Routes = [
  { path:'sell-equipment', component:SellEquipmentComponent},
  { path:'basic-details/:id', component:BasicDetailsComponent},
  { path:'vehicle-details/:id', component:VehicleDetailsComponent},
  { path:'visuals/:id', component:VisualsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsedEquipmentRoutingModule { }
