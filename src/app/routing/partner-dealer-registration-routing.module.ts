import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgreementDetailsComponent } from '../component/partner-dealer-management/agreement-details/agreement-details.component';
import { PartnerDealerAgreementComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-agreement/partner-dealer-agreement.component';
import { PartnerDealerBankDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-bank-details/partner-dealer-bank-details.component';
import { PartnerDealerBasicDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-basic-details/partner-dealer-basic-details.component';
import { PartnerDealerCorporateAddressComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-address/partner-dealer-corporate-address.component';
import { PartnerDealerCorporateDetailsComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-details/partner-dealer-corporate-details.component';
import { PartnerDealerLinkOrganizationComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-link-organization/partner-dealer-link-organization.component';
import { PartnerDealerRegistrationComponent } from '../component/partner-dealer-management/partner-dealer-registration/partner-dealer-registration.component';
import { PartnerDealerCorporateNextDetailsComponent } from './../component/partner-dealer-management/partner-dealer-registration/partner-dealer-corporate-next-details/partner-dealer-corporate-next-details.component';
import { RequestInProgressComponent } from './../component/partner-dealer-management/partner-dealer-registration/request-in-progress/request-in-progress.component';
const routes: Routes = [

  { path: 'registration', component: PartnerDealerRegistrationComponent },
  { path: 'agreement-details', component: AgreementDetailsComponent },
  { path: 'corporate-details', component: PartnerDealerCorporateDetailsComponent },
  { path: 'preference/:id', component: PartnerDealerCorporateNextDetailsComponent },
  { path: 'basic-details', component: PartnerDealerBasicDetailsComponent },
  { path: 'bank-details', component: PartnerDealerBankDetailsComponent },
  { path: 'agreement', component: PartnerDealerAgreementComponent },
  { path: 'corporate-address', component: PartnerDealerCorporateAddressComponent },
  { path: 'link-organization', component: PartnerDealerLinkOrganizationComponent },
  { path: 'partner-request', component: RequestInProgressComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class PartnerDealerRegistrationRoutingModule { }
