import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterDataComponent } from '../component/admin/new-equipment-master/master-data/master-data.component';

import { CategoryBrandMasterComponent } from '../component/admin/new-equipment-master/category-brand-master/category-brand-master.component';
import { LocationMasterComponent } from '../component/admin/new-equipment-master/location-master/location-master.component';
import { OfferMasterComponent } from '../component/admin/new-equipment-master/offer-master/offer-master.component';
import { TechnicalInformationMasterNewListComponent } from '../component/admin/new-equipment-master/technical-info-master-new/technical-info-master-new-list.component';
import { TechnicalInfoMasterUsedComponent } from '../component/admin/new-equipment-master/technical-info-master-used/technical-info-master-used.component';
import { CertificateMasterComponent } from '../component/admin/new-equipment-master/certificate-master/certificate-master.component';
import { RmTaggingMasterComponent } from '../component/admin/rm-tagging-master/rm-tagging-master.component';

const routes: Routes = [
  {
    path: 'technical-information-master-new-list',
    component: TechnicalInformationMasterNewListComponent,
  },

  { path: 'category-brand-master', component: CategoryBrandMasterComponent },
  { path: 'location-master', component: LocationMasterComponent },
  { path: 'offer-master', component: OfferMasterComponent },
  { path: 'masterdata', component: MasterDataComponent },
  { path: 'tech-info-used', component: TechnicalInfoMasterUsedComponent },
  { path: 'offer-master', component: OfferMasterComponent },
  { path: 'masterdata', component: MasterDataComponent },
  { path: 'tech-info-used', component: TechnicalInfoMasterUsedComponent },
  { path: 'certificate-master', component: CertificateMasterComponent },
  { path:'rm-tagging', component:RmTaggingMasterComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewEquipmentMasterRoutingModule {}
