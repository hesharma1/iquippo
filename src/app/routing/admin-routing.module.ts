import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../component/admin/dashboard/dashboard.component';
import { MyDemoComponent } from '../component/admin/my-demo/my-demo.component';
import { UserManagementComponent } from '../component/user-management/user-management.component';
import { AddUserComponent } from '../component/add-user/add-user.component';
import { TransactionComponent } from '../component/admin/transaction/transaction.component';
import { NewEquipmentLeadComponent } from '../component/admin/new-equipment-lead/new-equipment-lead.component';
import { LeadViewComponent } from '../component/admin/lead-view/lead-view.component';
import { BulkListingComponent } from '../component/admin/bulk-upload/components/bulk-listing/bulk-listing.component';
import { ValuationRequestComponent } from '../component/admin/valuation-request/valuation-request.component';
import { AdvancedValuationRequestComponent } from '../component/admin/advanced-valuation-request/advanced-valuation-request.component';
import { ValuationSettingsComponent } from '../component/admin/valuation-settings/valuation-settings.component';
import { EnterValuationRequestComponent } from '../component/admin/enter-valuation-request/enter-valuation-request.component';
import { CallbackRequestListComponent } from '../component/admin/callback-request-list/callback-request-list.component';
import { SubscribersListComponent } from '../component/admin/subscribers-list/subscribers-list.component';
import { EnterpriseValuationSettingsComponent } from '../component/admin/enterprise-valuation-settings/enterprise-valuation-settings.component';
import { BulkUploadListEnterpriseComponent } from '../component/admin/enter-valuation-request/bulk-upload-list-enterprise/bulk-upload-list-enterprise.component';
import { IndInvoiceComponent } from '../component/admin/ind-invoice/ind-invoice.component';
import { EnterpriseInvoiceComponent } from '../component/admin/enterprise-invoice/enterprise-invoice.component';
import { ContactUsListComponent } from '../component/admin/contact-us-list/contact-us-list.component';
import { BankingComponent } from '../component/banking/banking.component';
import { CompreferenceComponent } from '../component/compreference/compreference.component';
import { CorporatedetailsComponent } from '../component/corporatedetails/corporatedetails.component';
import { MyselfdetailsComponent } from '../component/myselfdetails/myselfdetails.component';
import { AuthGuard } from '../guards/auth.guard';
import { UserManagementNewComponent } from '../component/admin/user-management-new/user-management-new.component';
import { AboutAdminPageComponent } from '../component/admin/about-admin-page/about-admin-page.component';
import { MyAgreementsComponent } from '../component/customer/dashboard/my-agreements/my-agreements.component';
import { ChangePasswordComponent } from '../component/change-password/change-password.component';
import { BuyerPremiumInvoicesComponent } from '../component/admin/buyer-premium-invoices/buyer-premium-invoices.component';
import { SellerCommisionInvoicesComponent } from '../component/admin/seller-commision-invoices/seller-commision-invoices.component';
import { EMDMRInvoicesComponent } from '../component/admin/emd-mr-invoices/emd-mr-invoices.component';
import { EMDForfeitureInvoicesComponent } from '../component/admin/emd-forfeiture-invoices/emd-forfeiture-invoices.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'products',
        loadChildren: () =>
          import('./../modules/admin-product.module').then(
            (m) => m.AdminProductModule
          ),
      },
      {
        path: 'partner-admin',
        loadChildren: () =>
          import('../modules/partner-admin.module').then((m) => m.PartnerAdminModule),
      },
      {
        path: 'profile',
        component: MyselfdetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'user-management-new',
        component: UserManagementNewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'corporatedetails',
        component: CorporatedetailsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'communication',
        component: CompreferenceComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'bankingdetails',
        component: BankingComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'my-agreements',
        component: MyAgreementsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'new-equipment-lead',
        component: NewEquipmentLeadComponent
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'lead-view',
        component: LeadViewComponent
      },
      {
        path: 'new-equipment-master',
        loadChildren: () =>
          import('./../modules/new-equipment-master.module').then(
            (m) => m.NewEquipmentMasterModule
          ),
      },
      {
        path: 'partner-dealer',
        loadChildren: () =>
          import('./../modules/partner-dealer-registration.module').then(
            (m) => m.PartnerDealerRegistrationModule
          ),
      },
      {
        path: 'buy-sell-process-master',
        loadChildren: () =>
          import('./../modules/buy-sell-process-master.module').then(
            (m) => m.BuySellProcessMasterModule
          ),
      },
      {
        path: 'banner-settings',
        loadChildren: () =>
          import('./../modules/banner-settings.module').then(
            (m) => m.BannerSettingsModule
          ),
      },
      {
        path: 'client-management',
        loadChildren: () =>
          import('../modules/client-management.module').then((m) => m.ClientManagementModule),
      },
      {
        path: 'trade',
        loadChildren: () =>
          import('../modules/trade-details.module').then((m) => m.TradeDetailsModule),
      },
      {
        path: 'my-demo',
        component: MyDemoComponent
      },
      {
        path: 'customer-management',
        component: UserManagementComponent
      },
      {
        path: 'valuation-settings',
        component: ValuationSettingsComponent
      },
      { path: 'add-user', component: AddUserComponent },
      { path: 'transactions', component: TransactionComponent },
      { path: 'val-request', component: ValuationRequestComponent },
      { path: 'adv-val-request', component: AdvancedValuationRequestComponent },
      { path: 'enterprise-val-request', component: EnterValuationRequestComponent },
      { path: 'ind-invoice', component: IndInvoiceComponent },
      { path: 'bulk-listing', component: BulkListingComponent },
      { path: 'bulk-listing-enterprise', component: BulkUploadListEnterpriseComponent },
      { path: 'callback-request', component: CallbackRequestListComponent },
      { path: 'subscribers', component: SubscribersListComponent },
      { path: 'enterprise-valuation-settings', component: EnterpriseValuationSettingsComponent },
      { path: 'enterprise-invoice', component: EnterpriseInvoiceComponent },
      { path: 'buyer-premium-invoice', component: BuyerPremiumInvoicesComponent },
      { path: 'seller-commision-invoice', component: SellerCommisionInvoicesComponent },
      { path: 'emd-mr-invoice', component: EMDMRInvoicesComponent },
      { path: 'emd-forfeiture-invoice', component: EMDForfeitureInvoicesComponent },
      { path: 'contact-us-list', component: ContactUsListComponent },
      { path: 'about-us', component: AboutAdminPageComponent },
      {
        path: 'auction',
        loadChildren: () =>
          import('./../modules/auction.module').then(
            (m) => m.AuctionModule
          ),
      },
      {
        path: 'accounts',
        loadChildren: () =>
          import('./../modules/accounts.module').then(
            (m) => m.AccountsModule
          ),
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
