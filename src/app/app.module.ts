import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './component/layout/header/header.component';
import { SigninComponent } from './component/signin/signin.component';
import { SignupComponent } from './component/signup/signup.component';
import { MatSliderModule } from '@angular/material/slider';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { CoderesetpasswordComponent } from './component/coderesetpassword/coderesetpassword.component';
import { PwdresetdialogComponent } from './component/pwdresetdialog/pwdresetdialog.component';
import { BasicinformationComponent } from './component/basicinformation/basicinformation.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { TermsAndConditionsComponent } from './component/signup/terms-and-conditions/terms-and-conditions.component';
import { SharedService } from './services/shared-service.service';
import { UtilService } from './services/util.service';
import { MoveCursorToNextDirective } from './utility/custom-validators/move-cursor-to-next.directive';
import { DatePipe } from '@angular/common';
import { LeftMenuComponent } from './component/layout/left-menu/left-menu.component';
import { environment } from 'src/environments/environment';
import { FooterComponent } from './component/layout/footer/footer.component';
import { GridModule, ToolbarService, ExcelExportService } from '@syncfusion/ej2-angular-grids';
import { PageService, SortService, FilterService } from '@syncfusion/ej2-angular-grids';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NotificationModule } from './utility/toastr-notification/notification-service.module';
import { UserService } from './services/user';
import { ConfirmAdduserComponent } from './component/add-user/confirm-adduser/confirm-adduser.component';
import { VerifyuserComponent } from './component/add-user/verify-user/verifyuser.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddressValidationDirective } from './utility/validations/address-validation.directive';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { PanVerifyPopupComponent } from './component/pan-verify-popup/pan-verify-popup.component';
import { HttpHeadersInterceptor } from './utility/http-interceptors/http-headers-interceptor';
import { HttpErrorInterceptor } from './utility/http-interceptors/http-error-interceptor';
import { CompleteNewPasswordComponent } from './component/signin/complete-new-password/complete-new-password.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';

import { AdminModule } from './modules/admin.module';
import { S3UploadDownloadService } from './utility/s3-upload-download/s3-upload-download.service';
import { NewEquipmentListModule } from './modules/new-equipment-list.module';
import { UsedEquipmentListModule } from './modules/used-equipment-list.module';
import { AddEditFieldComponent } from './component/admin/new-equipment-master/technical-info-master-new/dialogs/add-edit-field/add-edit-field.component';
import { AddEditSpecificationComponent } from './component/admin/new-equipment-master/technical-info-master-new/dialogs/add-edit-specification/add-edit-specification.component';
import { AdminMasterService } from './services/admin-master.service';
import { NewEquipmentMasterService } from './services/new-equipment-master.service';
import { ViewBrandSpecDataComponent } from './component/admin/new-equipment-master/technical-info-master-new/dialogs/view-brand-spec-data/view-brand-spec-data.component';
import { BulkUploadModule } from './modules/bulk-upload.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { AddEditMarkUpPriceComponent } from './component/admin/buy-sell-process-master/dialogs/add-edit-markup-price/add-edit-markup-price.component';
import { NgxCarouselModule } from 'ngx-light-carousel'
import { PartnerDealerRegistrationModule } from './modules/partner-dealer-registration.module';
import { EmdMasterComponent } from './component/admin/buy-sell-process-master/components/emd-master/emd-master.component';

import { CustomerModule } from './modules/customer.module';
import { MyTransactionComponent } from './component/customer/dashboard/my-transaction/my-transaction.component';
import { LandingPageDashboardComponent } from './component/landing-page-dashboard/landing-page-dashboard.component';
import { PartnerContactListComponent } from './component/admin/partner-management/partner-contact-list/partner-contact-list.component';
import { PartnerEntityListComponent } from './component/admin/partner-management/partner-entity-list/partner-entity-list.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MakeACallComponent } from './component/make-a-call/make-a-call.component';
import { ValuationModule } from './modules/valuation.module';
import { AddEditAssetSaleChargeComponent } from './component/admin/buy-sell-process-master/dialogs/add-edit-asset-sale-charge/add-edit-asset-sale-charge.component';
import { AboutUsComponent } from './component/about-us/about-us.component';
import { ContactUsComponent } from './component/contact-us/contact-us.component';
import { TermsOfUseComponent } from './component/terms-of-use/terms-of-use.component';
import { PrivacyPolicyComponent } from './component/privacy-policy/privacy-policy.component';
import { ValuationRequestComponent } from './component/admin/valuation-request/valuation-request.component';
import { AdvancedValuationRequestComponent } from './component/admin/advanced-valuation-request/advanced-valuation-request.component';
import { GenerateReportComponent } from './component/admin/generate-report/generate-report.component';
import { ViewReportComponent } from './component/admin/view-report/view-report.component';
import { AdvancedInfoDialogComponent } from './component/admin/advanced-valuation-request/info-dialog/advanced-info-dialog.component';
import { AdvancedValuationActionComponent } from './component/admin/advanced-valuation-request/action-dialog/advanced-valuation-action-request.component';
import { IndInvoiceComponent } from './component/admin/ind-invoice/ind-invoice.component';
import { EmdAuctionMasterComponent } from './component/admin/buy-sell-process-master/components/emd-auction-master/emd-auction-master.component';
import { AuctionPaymentHistoryComponent } from './component/customer/dashboard/auction-payment-history/auction-payment-history.component';
import { GenerateInvoiceDialogComponent } from './component/admin/enterprise-invoice/generate-invoice-dialog/generate-invoice-dialog.component';
import { ConfirmationModalComponent } from './component/shared/confirmation-modal/confirmation-modal.component';
import { DisclaimerModalComponent } from './component/shared/disclaimer-modal/disclaimer-modal.component';
import { AuctionFulfilmentForUserComponent } from './component/customer/auction-fulfilment-for-user/auction-fulfilment-for-user.component';
import { AuctionFulfilmentInfoDialogComponent } from './component/admin/auction/auction-fulfilment-info-dialog/auction-fulfilment-info-dialog.component';
import { LoaderInterceptor } from 'src/app/utility/http-interceptors/loader.interceptor';
import { LoaderService } from './services/loader.service';
import { ContactUsListComponent } from './component/admin/contact-us-list/contact-us-list.component';
import { UserManagementNewComponent } from './component/admin/user-management-new/user-management-new.component';
import { AboutAdminPageComponent } from './component/admin/about-admin-page/about-admin-page.component';
import { ImageLoader } from './utility/validations/image-loader';
import { LazyImgDirective } from './utility/validations/lazy-Img';
import { SharedModule } from './modules/shared.module';
import { ViewAllIdsComponent } from './component/shared/view-all-ids/view-all-ids.component';
import { documentVerificationComponent } from './component/customer/document-verification-confirmation/document-verification-confirmation.component';
import { CustomerSearchModalComponent } from './component/shared/customer-search-modal/customer-search-modal.component';
import { ExportPopupComponent } from './component/admin/export-popup/export-popup.component';
import { OurValuedPartnersComponent } from './component/our-valued-partners/our-valued-partners.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SigninComponent,
    SignupComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    CoderesetpasswordComponent,
    PwdresetdialogComponent,
    BasicinformationComponent,
    TermsAndConditionsComponent,
    MoveCursorToNextDirective,
    LeftMenuComponent,    
    FooterComponent,   
    VerifyuserComponent,
    AddressValidationDirective,
    PanVerifyPopupComponent,
    CompleteNewPasswordComponent,
    CompleteNewPasswordComponent,
    DashboardComponent,
    DashboardComponent,
    EmdMasterComponent,
    MyTransactionComponent,
    LandingPageDashboardComponent,
    PartnerContactListComponent,
    PartnerEntityListComponent,
    MakeACallComponent,
    AboutUsComponent,
    ContactUsComponent,
    TermsOfUseComponent,
    PrivacyPolicyComponent,
    ValuationRequestComponent,
    AdvancedValuationRequestComponent,
    GenerateReportComponent,
    ViewReportComponent,
    AdvancedValuationActionComponent,
    AdvancedInfoDialogComponent,
    IndInvoiceComponent,
    EmdAuctionMasterComponent,        
    AuctionPaymentHistoryComponent,        
    ConfirmationModalComponent, 
    DisclaimerModalComponent,
    AuctionFulfilmentForUserComponent,
    AuctionFulfilmentInfoDialogComponent,
    ContactUsListComponent,
    AboutAdminPageComponent,
    ViewAllIdsComponent,
    documentVerificationComponent,
    CustomerSearchModalComponent,
    ExportPopupComponent,
    OurValuedPartnersComponent
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    AngularMaterialModule,
    FlexLayoutModule,
    MatSliderModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocialLoginModule,
    GridModule,
    NgxSpinnerModule,
    NotificationModule,
    AdminModule,
    NewEquipmentListModule,
    UsedEquipmentListModule,
    BulkUploadModule,
    ScrollingModule,
    MatCardModule,
    NgxCarouselModule,
    PartnerDealerRegistrationModule,
    CustomerModule,
    MatDialogModule,
    FormsModule,
    ValuationModule,
    SharedModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleProviderId),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookProviderId),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpHeadersInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    SharedService,
    UtilService,
    DatePipe,
    PageService,
    SortService,
    FilterService,
    UserService,
    ExcelExportService,
    ToolbarService,
    S3UploadDownloadService,
    AdminMasterService,
    NewEquipmentMasterService,
    LoaderService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    PwdresetdialogComponent,
    TermsAndConditionsComponent,
    ConfirmAdduserComponent,
    VerifyuserComponent,
    PanVerifyPopupComponent,
    CompleteNewPasswordComponent,
    AddEditFieldComponent,
    AddEditSpecificationComponent,
    ViewBrandSpecDataComponent,
    AddEditAssetSaleChargeComponent,
    AddEditMarkUpPriceComponent,
    MakeACallComponent,
    GenerateReportComponent,
    ViewReportComponent,
    AdvancedInfoDialogComponent,
    AdvancedValuationActionComponent,
    ConfirmationModalComponent,
    DisclaimerModalComponent,
    AuctionFulfilmentInfoDialogComponent,
    ViewAllIdsComponent,
    documentVerificationComponent,
    CustomerSearchModalComponent,
    ExportPopupComponent
  ],
  exports: [
    FooterComponent
  ]
})
export class AppModule { }
