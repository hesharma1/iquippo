import { Interface } from "readline";

interface CustomerDetail {
    firstName?: string;
    lastName?: string;
    dob?: string;
    mobileNumber?: string;
    relationship_manager?: string;
    emailId?: string;
    alternateEmailId?: string;
}
// interface Docdetail{

//       	pan : {
// 			docType : string,
// 			docNumber : string,
// 			name:string,
// 			docImages:{ front : string }

// 		},


//     };

interface CorporateDetail {
    msme?: string;
    companyName?: string;
    firstName?: string;
    lastName?: string;
    dateOfIncorporation?: string;
    mobileNumber?: string;
    emailId?: string;
}

interface Address {
    addressLine1?: string;
    addressLine2?: string;
    pincode?: string;
    state?: string;
    city?: string;
    gstNo?: string;
    addressId?: string;

}

export class DocumentDetails {
    docType?: string
    docNumber?: string
    dob?: string
    name?: string
    docUrl?: DocImages
    karzaRes?: any
    documentId?: string
}

export class DocImages {
    front?: string;
    back?: string;
}

interface Pan {
    docType?: string;
    docNumber?: string;
    name?: string;
    docImages?: DocImages;
}

interface DocImages2 {
    front?: string;
    back?: string;
}

interface Dl {
    docType?: string;
    docNumber?: string;
    dob?: string;
    docImages?: DocImages2;
}

interface DocImages3 {
    front?: string;
    back?: string;
}

interface Voter {
    docType?: string;
    docNumber?: string;
    docImages?: DocImages3;
}

interface DocDetails {
    pan?: Pan;
    dl?: Dl;
    voter?: Voter;
}

export interface GetUserDto {
    userId?: string;
}

export interface UserProfileDTO {
    cognitoId?: string;
    represent?: string;
    gender?: string;
    entityType?: string;
    relationshipWithEntity?: string;
    customerDetails?: CustomerDetail[];
    corporateDetails?: CorporateDetail[];
    address?: Address[];
    doc_details?: any;
    applied_for_pan?: boolean;
    callcenterCognito?: string;
    address_proof?: AddressProof
    deletedAddress?: string[];
    use_pan_name?: string;
}

export class AddressProof {
    documentId?: string
    docType?: string
    docNumber?: string
    docImages?: string
    karza_response?: any
}

export class UserResponse {
    basic_user_details?: BasicUserDetails;
    user_details?: UserDetails;
    adress_details?: AddressDetails[];
    doc_details?: DocumentDetails[];
    statusCode?: any;
    body?: any;
    address_proof?: AddressProof
    // applied_for_pan?:boolean;
}

export class BasicUserDetails {
    authType?: string
    cognitoId?: string
    countryCode?: string
    createdAt?: string
    dateOfBirth?: string
    email?: string
    firstName?: string
    flag?: string
    kyc_verified?: boolean
    lastName?: string
    mobileNumber?: string
    pinCode?: string
    prefixCode?: string
    applied_for_pan?: string
    relationship_manager?:string
}

export class AddressDetails {
    address?: string
    addressId?: string
    addressLine1?: string
    addressLine2?: string
    city?: string
    createdAt?: string
    gstNo?: string
    id?: string
    pincode?: string
    state?: string
}

export class UserDetails {
    alternateEmailId?: string
    cognitoId?: string
    createdAt?: string
    gender?: string
    represent?: string
}

export interface SaveBankDetails {
    username?: string,
    bankdetails?: Bankdetails[];
    callcenterCognito?: string,
    deletedItem?: string[];
}

export interface BankListData {
    username?: string,
    cognitoId?: string;
    getData?: string
    callcenterCognito?: string,
}

export interface MasterData {
    username?: string,
    getData?: string
}

export interface Bankdetails {
    bankName?: string,
    bankId?: string,
    branch?: string,
    accountHolder?: string,
    accountNumber?: string,
    ifsc?: string,
    accountType?: string,
    otherBank?: string,
    cancelled_cheque?:string,
    userBankId?: string,
}

interface communicationPrefMetaData {
    service?: boolean,
    promotion?: boolean
}

interface communicationPrefData {
    email?: communicationPrefMetaData,
    sms?: communicationPrefMetaData,
    whatsapp?: communicationPrefMetaData,
    voiceCall?: communicationPrefMetaData
}

export interface CommunicationPrefrencesDTO {
    cognitoId?: string;
    username?: string;
    communicationPref?: communicationPrefData,
    industry?: string[],
    assetClass?: string[],
    services?: string[],
    actAs?: string[],
    callcenterCognito?: string;
}





